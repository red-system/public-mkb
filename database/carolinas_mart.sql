/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : carolinas_mart

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 22/11/2019 15:11:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for arus_stock_produk
-- ----------------------------
DROP TABLE IF EXISTS `arus_stock_produk`;
CREATE TABLE `arus_stock_produk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `penjualan_produk_id` int(11) DEFAULT NULL,
  `distribusi_id` int(11) DEFAULT NULL,
  `distribusi_detail_id` int(11) DEFAULT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `progress_produksi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `history_penyesuaian_produk_id` int(11) DEFAULT NULL,
  `stock_in` float DEFAULT NULL,
  `stock_out` float DEFAULT NULL,
  `last_stock` float DEFAULT NULL,
  `last_stock_total` float DEFAULT NULL,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of arus_stock_produk
-- ----------------------------
BEGIN;
INSERT INTO `arus_stock_produk` VALUES (1, '2019-11-01', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 200, 200, 'insert', 'Insert stok produk', '2019-11-01 12:24:50', '2019-11-01 12:24:50');
INSERT INTO `arus_stock_produk` VALUES (2, '2019-11-01', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 199, 199, 'delete', 'Penjualan produk', '2019-11-01 12:53:14', '2019-11-01 12:53:14');
COMMIT;

-- ----------------------------
-- Table structure for assembly
-- ----------------------------
DROP TABLE IF EXISTS `assembly`;
CREATE TABLE `assembly` (
  `assembly_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`assembly_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for assembly_item
-- ----------------------------
DROP TABLE IF EXISTS `assembly_item`;
CREATE TABLE `assembly_item` (
  `assembly_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `assembly_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`assembly_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bahan
-- ----------------------------
DROP TABLE IF EXISTS `bahan`;
CREATE TABLE `bahan` (
  `bahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `bahan_jenis_id` int(11) DEFAULT NULL,
  `bahan_satuan_id` int(11) DEFAULT NULL,
  `bahan_suplier_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(10) DEFAULT NULL,
  `bahan_nama` varchar(50) DEFAULT NULL,
  `bahan_minimal_stock` int(11) DEFAULT NULL,
  `bahan_qty` bigint(20) DEFAULT '0',
  `bahan_last_stock` bigint(20) DEFAULT NULL,
  `bahan_harga` bigint(20) DEFAULT NULL,
  `bahan_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`bahan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for forget_request
-- ----------------------------
DROP TABLE IF EXISTS `forget_request`;
CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  PRIMARY KEY (`forget_request_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for guest
-- ----------------------------
DROP TABLE IF EXISTS `guest`;
CREATE TABLE `guest` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_nama` varchar(50) DEFAULT NULL,
  `guest_alamat` varchar(50) DEFAULT NULL,
  `guest_telepon` varchar(50) DEFAULT NULL,
  `kewarganegaraan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`guest_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for harga_produk
-- ----------------------------
DROP TABLE IF EXISTS `harga_produk`;
CREATE TABLE `harga_produk` (
  `harga_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `minimal_pembelian` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`harga_produk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for history_penyesuaian_produk
-- ----------------------------
DROP TABLE IF EXISTS `history_penyesuaian_produk`;
CREATE TABLE `history_penyesuaian_produk` (
  `history_penyesuaian_produk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_produk_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` bigint(20) DEFAULT NULL,
  `qty_akhir` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_penyesuaian_produk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for history_transfer_produk
-- ----------------------------
DROP TABLE IF EXISTS `history_transfer_produk`;
CREATE TABLE `history_transfer_produk` (
  `history_transfer_produk_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` text COLLATE utf8mb4_unicode_ci,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_transfer_qty` bigint(20) DEFAULT NULL,
  `qty_terima` bigint(20) DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_transfer_produk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for hutang
-- ----------------------------
DROP TABLE IF EXISTS `hutang`;
CREATE TABLE `hutang` (
  `hutang_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_bahan_id` int(11) DEFAULT NULL,
  `tenggat_pelunasan` date DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`hutang_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for jenis_produk
-- ----------------------------
DROP TABLE IF EXISTS `jenis_produk`;
CREATE TABLE `jenis_produk` (
  `jenis_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_produk_kode` varchar(10) DEFAULT NULL,
  `jenis_produk_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jenis_produk_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_produk
-- ----------------------------
BEGIN;
INSERT INTO `jenis_produk` VALUES (1, '1', 'Organik', '2019-11-01 12:23:34', '2019-11-01 12:23:34');
INSERT INTO `jenis_produk` VALUES (2, '2', 'An-Organik', '2019-11-01 12:23:47', '2019-11-01 12:23:47');
COMMIT;

-- ----------------------------
-- Table structure for konversi_satuan
-- ----------------------------
DROP TABLE IF EXISTS `konversi_satuan`;
CREATE TABLE `konversi_satuan` (
  `konversi_satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_satuan_awal` int(11) DEFAULT NULL,
  `id_satuan_akhir` int(11) DEFAULT NULL,
  `jumlah_konversi` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`konversi_satuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konversi_satuan
-- ----------------------------
BEGIN;
INSERT INTO `konversi_satuan` VALUES (9, 4, 1, 100, '2019-11-05 17:43:45', '2019-11-05 17:44:56');
COMMIT;

-- ----------------------------
-- Table structure for log_aktivitas
-- ----------------------------
DROP TABLE IF EXISTS `log_aktivitas`;
CREATE TABLE `log_aktivitas` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `nama_menu` varchar(0) DEFAULT NULL,
  `aktivitas` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for log_kasir
-- ----------------------------
DROP TABLE IF EXISTS `log_kasir`;
CREATE TABLE `log_kasir` (
  `log_kasir_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `waktu_buka` datetime DEFAULT NULL,
  `waktu_tutup` datetime DEFAULT NULL,
  `kas_awal` bigint(20) DEFAULT NULL,
  `kas_akhir` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_kasir_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_kasir
-- ----------------------------
BEGIN;
INSERT INTO `log_kasir` VALUES (1, 1, '2019-11-01 12:52:37', NULL, 200000, NULL, '2019-11-01 12:52:37', NULL);
INSERT INTO `log_kasir` VALUES (2, 1, '2019-11-06 11:01:36', NULL, 300000, NULL, '2019-11-06 11:01:36', NULL);
COMMIT;

-- ----------------------------
-- Table structure for lokasi
-- ----------------------------
DROP TABLE IF EXISTS `lokasi`;
CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi_kode` varchar(10) DEFAULT NULL,
  `lokasi_nama` varchar(50) DEFAULT NULL,
  `lokasi_tipe` varchar(20) DEFAULT NULL,
  `lokasi_alamat` text,
  `lokasi_telepon` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lokasi_id`) USING BTREE,
  UNIQUE KEY `lokasi_kode` (`lokasi_kode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lokasi
-- ----------------------------
BEGIN;
INSERT INTO `lokasi` VALUES (1, '02', 'Gudang', 'Gudang', 'Gang nusa kambanagan', '0361 223456', '2019-05-05 15:58:24', '2019-08-12 17:29:36');
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_kode` varchar(50) DEFAULT NULL,
  `menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` VALUES (1, 'pos', 'POS', NULL, 'pos', NULL, 'cart', 1, '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES (2, 'master_data', 'Master Data', NULL, '#', NULL, 'master', 3, '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES (3, 'guest', 'Guest', 'list|add|edit|delete', 'guest', NULL, 'address-book', 4, '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES (4, 'inventori', 'Inventori', '', '#', NULL, 'commode', 5, '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES (5, 'produksi', 'produksi', '', '#', NULL, 'hammers', 7, '2019-06-18 03:13:50', '2019-10-10 16:00:10');
INSERT INTO `menu` VALUES (6, 'laporan', 'Laporan', NULL, '#', NULL, 'clipboards', 9, '2019-06-18 03:13:50', '2019-06-18 03:13:50');
INSERT INTO `menu` VALUES (7, 'dashboard', 'Dashboard', NULL, 'home', NULL, 'layers', 2, '2019-06-18 04:12:56', '2019-06-18 04:12:56');
INSERT INTO `menu` VALUES (8, 'order_bahan', 'Order Bahan', 'list|add|edit|delete|view', 'po-bahan', NULL, 'order', 6, '2019-06-29 11:14:43', '2019-06-29 11:14:43');
INSERT INTO `menu` VALUES (9, 'hutang_piutang', 'Hutang/Piutang', NULL, '#', NULL, 'debt', 8, '2019-07-08 10:58:16', '2019-07-08 10:58:45');
COMMIT;

-- ----------------------------
-- Table structure for pembayaran_hutang
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_hutang`;
CREATE TABLE `pembayaran_hutang` (
  `pembayaran_hutang_id` int(11) NOT NULL AUTO_INCREMENT,
  `hutang_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pembayaran_hutang_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pembayaran_piutang
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_piutang`;
CREATE TABLE `pembayaran_piutang` (
  `pembayaran_piutang_id` int(11) NOT NULL AUTO_INCREMENT,
  `piutang_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pembayaran_piutang_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for penjualan
-- ----------------------------
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan` (
  `penjualan_id` int(11) NOT NULL AUTO_INCREMENT,
  `ekspedisi_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `log_kasir_id` int(11) DEFAULT NULL,
  `nama_pelanggan` varchar(50) DEFAULT NULL,
  `alamat_pelanggan` varchar(100) DEFAULT NULL,
  `telepon_pelanggan` varchar(20) DEFAULT NULL,
  `urutan` bigint(20) DEFAULT NULL,
  `no_faktur` varchar(100) DEFAULT NULL,
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `status_distribusi` enum('not_yet','done') DEFAULT 'not_yet',
  `tanggal` date DEFAULT NULL,
  `jenis_transaksi` enum('retail','distributor') DEFAULT 'distributor',
  `pengiriman` enum('ya','tidak') DEFAULT NULL,
  `total` float DEFAULT NULL,
  `biaya_tambahan` float DEFAULT NULL,
  `potongan_akhir` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `terbayar` float DEFAULT NULL,
  `sisa_pembayaran` float DEFAULT NULL,
  `kembalian` float DEFAULT NULL,
  `additional_no` varchar(50) DEFAULT NULL,
  `additional_tanggal` date DEFAULT NULL,
  `additional_keterangan` text,
  `status_distribusi_done_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`penjualan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan
-- ----------------------------
BEGIN;
INSERT INTO `penjualan` VALUES (1, NULL, 1, 6, 1, 'Guest', '', '', 1, 'PL-191101/00/1', 'Lunas', 'not_yet', '2019-11-01', 'distributor', 'tidak', 20000, 0, 0, 20000, 20000, 0, 0, '', '0000-00-00', '', NULL, '2019-11-01 12:53:14', '2019-11-01 12:53:14');
COMMIT;

-- ----------------------------
-- Table structure for penjualan_produk
-- ----------------------------
DROP TABLE IF EXISTS `penjualan_produk`;
CREATE TABLE `penjualan_produk` (
  `penjualan_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `produk_custom_id` int(11) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `hpp` float DEFAULT NULL,
  `potongan` float DEFAULT NULL,
  `ppn_persen` float DEFAULT NULL,
  `ppn_nominal` float DEFAULT NULL,
  `harga_net` float DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`penjualan_produk_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan_produk
-- ----------------------------
BEGIN;
INSERT INTO `penjualan_produk` VALUES (1, 1, 1, NULL, 1, 20000, 18000, NULL, 0.1, NULL, NULL, 1, 20000, '2019-11-01 12:53:14', '2019-11-01 12:53:14');
COMMIT;

-- ----------------------------
-- Table structure for piutang
-- ----------------------------
DROP TABLE IF EXISTS `piutang`;
CREATE TABLE `piutang` (
  `piutang_id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_id` int(11) DEFAULT NULL,
  `tenggat_pelunasan` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`piutang_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_nama` varchar(100) DEFAULT NULL,
  `produk_satuan_id` int(11) DEFAULT NULL,
  `produk_jenis_id` int(11) NOT NULL,
  `produk_kode` varchar(100) NOT NULL,
  `fabric_id` int(11) DEFAULT NULL,
  `patern_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `bed_size_id` int(11) DEFAULT NULL,
  `good_size_id` int(11) DEFAULT NULL,
  `weight_pc` int(11) DEFAULT NULL,
  `weight_m` int(11) DEFAULT NULL,
  `style` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `harga_eceran` double DEFAULT '0',
  `harga_grosir` double DEFAULT '0',
  `harga_konsinyasi` double DEFAULT '0',
  `produk_minimal_stock` bigint(20) NOT NULL,
  `disc_persen` double NOT NULL DEFAULT '0',
  `disc_nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`produk_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
BEGIN;
INSERT INTO `produk` VALUES (1, 'Pupuk B-26', 1, 2, '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20000, 0, 0, 100, 0, 0, '2019-11-01 12:24:18', '2019-11-06 15:22:04');
COMMIT;

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_kode` varchar(10) DEFAULT NULL,
  `satuan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`satuan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of satuan
-- ----------------------------
BEGIN;
INSERT INTO `satuan` VALUES (1, 'G', 'Gram', '2019-11-01 12:19:23', '2019-11-01 12:19:23');
INSERT INTO `satuan` VALUES (2, 'KG', 'Kilo Gram', '2019-11-01 12:19:36', '2019-11-01 12:19:36');
INSERT INTO `satuan` VALUES (3, 'SK', 'Sak', '2019-11-01 12:19:48', '2019-11-01 12:19:48');
INSERT INTO `satuan` VALUES (4, 'PCS', 'Pieces', '2019-11-01 12:22:42', '2019-11-01 12:22:42');
COMMIT;

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
BEGIN;
INSERT INTO `staff` VALUES (1, '1909807889237', 'Astradanta', 'Denpasar, 13 Februari 1997', '1970-01-01', 'Gang nuri 2 no 9 Banjarangkan Klungkung', 'Staff', 'Laki-laki', '2019-04-18', 'astra.danta@gmail.com', '085792078364', '', '2019-04-30 14:29:14', '2019-07-26 10:53:15');
COMMIT;

-- ----------------------------
-- Table structure for stock_produk
-- ----------------------------
DROP TABLE IF EXISTS `stock_produk`;
CREATE TABLE `stock_produk` (
  `stock_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `stock_produk_lokasi_id` int(11) NOT NULL,
  `month` char(4) DEFAULT NULL,
  `year` char(4) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `stock_produk_seri` varchar(100) NOT NULL,
  `stock_produk_qty` bigint(20) NOT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `last_stok` bigint(20) DEFAULT NULL,
  `stock_produk_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stock_produk_id`) USING BTREE,
  KEY `stock_produk_constraint` (`produk_id`) USING BTREE,
  CONSTRAINT `stock_produk_ibfk_1` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stock_produk
-- ----------------------------
BEGIN;
INSERT INTO `stock_produk` VALUES (1, 1, 1, '11', '19', 1, '11192021', 199, 18000, NULL, '', 0, '2019-11-01 12:24:50', '2019-11-01 12:53:14');
COMMIT;

-- ----------------------------
-- Table structure for sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `sub_menu`;
CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `sub_menu_kode` varchar(50) DEFAULT NULL,
  `sub_menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `data` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sub_menu
-- ----------------------------
BEGIN;
INSERT INTO `sub_menu` VALUES (1, 1, 'penjualan', 'POS Reguler', 'delete|view', NULL, 'pos', NULL, '2019-06-18 03:17:08', '2019-10-26 11:57:03');
INSERT INTO `sub_menu` VALUES (3, 2, 'suplier', 'Suplier', 'list|add|edit|delete|view', NULL, 'suplier', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (4, 2, 'satuan', 'Satuan', 'list|add|edit|delete|view|konversi_satuan', NULL, 'satuan', NULL, '2019-06-18 03:25:43', '2019-11-05 14:58:08');
INSERT INTO `sub_menu` VALUES (5, 2, 'jenis-produk', 'Jenis Produk', 'list|add|edit|delete|view', NULL, 'jenis-produk', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (7, 2, 'lokasi', 'Lokasi', 'list|add|edit|delete|view', NULL, 'lokasi', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (8, 2, 'user', 'User', 'list|add|edit|delete|view', NULL, 'user', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (9, 2, 'user-role', 'User Role', '', NULL, 'user-role', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (10, 2, 'tipe-pembayaran', 'Tipe Pembayaran', 'list|add|edit|delete|view', NULL, 'tipe-pembayaran', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (11, 2, 'staff', 'Staf', 'list|add|edit|delete|view', NULL, 'staff', NULL, '2019-06-18 03:25:43', '2019-06-18 03:25:43');
INSERT INTO `sub_menu` VALUES (13, 4, 'produk', 'Produk', 'list|add|edit|delete|view|stock|price|unit|assembly', 'hpp', 'produk', NULL, '2019-06-18 03:51:52', '2019-11-08 11:06:58');
INSERT INTO `sub_menu` VALUES (14, 6, 'produk-by-location', 'Produk per Lokasi', 'list', NULL, 'produk-by-location', NULL, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES (16, 6, 'bahan-by-location', 'Bahan per Lokasi', 'list', NULL, 'bahan-by-location', NULL, '2019-06-28 15:26:43', '2019-06-28 15:26:43');
INSERT INTO `sub_menu` VALUES (18, 4, 'transfer-produk', 'Transfer Stok Produk', 'list|transfer_produk|konfirmasi_transfer_produk|history_transfer_produk', NULL, 'transfer-produk', NULL, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES (20, 4, 'penyesuaian-produk', 'Penyesuaian Stok Produk', 'list|pesnyesuaian_produk|history_penyesuaian_produk', NULL, 'penyesuaian-produk', NULL, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES (22, 4, 'low-stock-produk', 'Low Stock Produk', 'list', NULL, 'low-stock-produk', NULL, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES (24, 4, 'kartu-stock-produk', 'Kartu Stok Produk', 'list', NULL, 'kartu-stock-produk', NULL, '2019-06-18 03:51:52', '2019-06-18 03:51:52');
INSERT INTO `sub_menu` VALUES (26, 6, 'laporan-stock-produk', 'Stok Produk', 'list', NULL, 'laporan/laporan-stock-produk', NULL, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES (28, 6, 'laporan-penjualan', 'Penjualan', 'list', NULL, 'laporan/laporan-penjualan', NULL, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES (29, 6, 'laporan-produksi', 'Produksi', 'list', NULL, 'laporan/laporan-produksi', NULL, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES (30, 6, 'laporan-hutang', 'Hutang', 'list', NULL, 'laporan/laporan-hutang', NULL, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES (31, 6, 'laporan-piutang', 'Piutang', 'list', NULL, 'laporan/laporan-piutang', NULL, '2019-06-18 04:00:08', '2019-06-18 04:00:08');
INSERT INTO `sub_menu` VALUES (34, 9, 'hutang', 'Hutang', '', NULL, 'hutang', NULL, '2019-07-08 11:19:09', '2019-07-08 11:19:09');
INSERT INTO `sub_menu` VALUES (35, 9, 'piutang', 'Piutang', '', NULL, 'piutang', NULL, '2019-07-08 11:19:18', '2019-07-08 11:19:18');
INSERT INTO `sub_menu` VALUES (36, 6, 'laporan-staff', 'Staff', 'list', NULL, 'laporan/laporan-staff', NULL, '2019-07-18 16:18:06', '2019-07-18 16:18:06');
INSERT INTO `sub_menu` VALUES (37, 6, 'laporan-customer', 'Customer', 'list', NULL, 'laporan/laporan-customer', NULL, '2019-07-18 16:18:42', '2019-07-18 16:18:42');
COMMIT;

-- ----------------------------
-- Table structure for suplier
-- ----------------------------
DROP TABLE IF EXISTS `suplier`;
CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `suplier_kode` varchar(10) DEFAULT NULL,
  `suplier_nama` varchar(100) DEFAULT NULL,
  `suplier_alamat` text,
  `suplier_telepon` varchar(20) DEFAULT NULL,
  `suplier_email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`suplier_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tipe_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `tipe_pembayaran`;
CREATE TABLE `tipe_pembayaran` (
  `tipe_pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe_pembayaran_kode` varchar(20) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `no_akun` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `additional` tinyint(1) DEFAULT NULL,
  `kembalian` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tipe_pembayaran_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipe_pembayaran
-- ----------------------------
BEGIN;
INSERT INTO `tipe_pembayaran` VALUES (6, '01', 'Tunai', '', 'kas', NULL, 1, '2019-10-03 12:02:07', '2019-10-03 12:02:07');
INSERT INTO `tipe_pembayaran` VALUES (7, '02', 'Visa', '', 'kas', 1, 0, '2019-10-20 15:30:11', '2019-10-20 16:14:35');
INSERT INTO `tipe_pembayaran` VALUES (8, '03', 'Master Card', '', 'kas', 1, 0, '2019-10-20 15:30:22', '2019-10-20 16:14:43');
INSERT INTO `tipe_pembayaran` VALUES (9, '04', 'American Expres (Amex)', '', 'kas', 1, 0, '2019-10-20 15:30:47', '2019-10-20 16:14:52');
INSERT INTO `tipe_pembayaran` VALUES (10, '05', 'Debet', '', 'kas', 1, 0, '2019-10-20 15:31:45', '2019-10-20 16:14:55');
INSERT INTO `tipe_pembayaran` VALUES (11, '06', 'Cek', '', 'kas', 1, 0, '2019-10-20 15:32:13', '2019-10-20 16:15:06');
INSERT INTO `tipe_pembayaran` VALUES (12, '07', 'Bilyet Giro (BG)', '', 'kas', 1, 0, '2019-10-20 15:32:43', '2019-10-20 16:18:21');
INSERT INTO `tipe_pembayaran` VALUES (13, '08', 'Piutang', '', 'kredit', NULL, 0, '2019-10-20 15:51:54', '2019-10-20 15:51:58');
COMMIT;

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `jumlah_satuan_unit` int(11) DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `harga_jual` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of unit
-- ----------------------------
BEGIN;
INSERT INTO `unit` VALUES (1, 1, 2, 1000, '11022345', 100000, '2019-11-06 14:44:21', '2019-11-06 16:47:15');
INSERT INTO `unit` VALUES (4, 1, 3, 100000, '12122', 120000, '2019-11-06 16:40:41', '2019-11-06 16:47:23');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 1, '$2y$10$sIfZ5LEefpTjmSN9B0Wl6.gZ44N.km9zp34lIaTt8CIdIBs4g4uAS', 1, 'assets/media/users/1556714265JXOGF.jpg', NULL, '2019-05-01 18:06:31', '2019-05-01 20:37:45');
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_role_akses` text,
  `keterangan` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES (1, 'Super Admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"konversi_satuan\":true},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":true},\"tipe-pembayaran\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false},\"inventori\":{\"akses_menu\":true,\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true,\"price\":true,\"unit\":true,\"assembly\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_bahan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"produksi\":{\"akses_menu\":false},\"hutang_piutang\":{\"akses_menu\":true,\"hutang\":{\"akses_menu\":true},\"piutang\":{\"akses_menu\":true}},\"laporan\":{\"akses_menu\":true,\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"bahan-by-location\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true},\"laporan-staff\":{\"akses_menu\":true,\"list\":true},\"laporan-customer\":{\"akses_menu\":true,\"list\":true}}}', NULL, '2019-04-26 00:00:00', '2019-11-08 11:08:54');
INSERT INTO `user_role` VALUES (2, 'admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"konversi_satuan\":false},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true,\"price\":true,\"unit\":true,\"assembly\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_bahan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"produksi\":{\"akses_menu\":true},\"hutang_piutang\":{\"akses_menu\":true,\"hutang\":{\"akses_menu\":true},\"piutang\":{\"akses_menu\":true}},\"laporan\":{\"akses_menu\":true,\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"bahan-by-location\":{\"akses_menu\":true,\"list\":true},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true},\"laporan-staff\":{\"akses_menu\":true,\"list\":true},\"laporan-customer\":{\"akses_menu\":true,\"list\":true}}}', '', '2019-11-01 12:05:04', '2019-11-08 11:09:12');
COMMIT;

-- ----------------------------
-- View structure for display_stock_bahan
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_bahan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_bahan` AS select `carolinas_mart`.`bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`carolinas_mart`.`stock_bahan`.`stock_bahan_qty`)),0,sum(`carolinas_mart`.`stock_bahan`.`stock_bahan_qty`)) AS `jumlah` from (`bahan` left join `stock_bahan` on((`carolinas_mart`.`bahan`.`bahan_id` = `carolinas_mart`.`stock_bahan`.`bahan_id`))) where (`carolinas_mart`.`stock_bahan`.`delete_flag` = 0) group by `carolinas_mart`.`bahan`.`bahan_id`;

-- ----------------------------
-- View structure for display_stock_bahan_lokasi
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_bahan_lokasi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_bahan_lokasi` AS select `carolinas_mart`.`bahan`.`bahan_id` AS `bahan_id`,if(isnull(sum(`carolinas_mart`.`stock_bahan`.`stock_bahan_qty`)),0,sum(`carolinas_mart`.`stock_bahan`.`stock_bahan_qty`)) AS `jumlah`,`carolinas_mart`.`stock_bahan`.`stock_bahan_lokasi_id` AS `stock_bahan_lokasi_id` from (`bahan` left join `stock_bahan` on((`carolinas_mart`.`bahan`.`bahan_id` = `carolinas_mart`.`stock_bahan`.`bahan_id`))) where (`carolinas_mart`.`stock_bahan`.`delete_flag` = 0) group by `carolinas_mart`.`stock_bahan`.`stock_bahan_lokasi_id`,`carolinas_mart`.`stock_bahan`.`bahan_id`;

-- ----------------------------
-- View structure for display_stock_produk
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_produk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_produk` AS select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where (`stock_produk`.`delete_flag` = 0) group by `produk`.`produk_id`;

-- ----------------------------
-- View structure for display_stock_produk_lokasi
-- ----------------------------
DROP VIEW IF EXISTS `display_stock_produk_lokasi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `display_stock_produk_lokasi` AS select `produk`.`produk_id` AS `produk_id`,if(isnull(sum(`stock_produk`.`stock_produk_qty`)),0,sum(`stock_produk`.`stock_produk_qty`)) AS `jumlah`,`stock_produk`.`stock_produk_lokasi_id` AS `stock_produk_lokasi_id` from (`produk` left join `stock_produk` on((`produk`.`produk_id` = `stock_produk`.`produk_id`))) where (`stock_produk`.`delete_flag` = 0) group by `stock_produk`.`stock_produk_lokasi_id`,`stock_produk`.`produk_id`;

-- ----------------------------
-- View structure for staff_lama_kerja
-- ----------------------------
DROP VIEW IF EXISTS `staff_lama_kerja`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_lama_kerja` AS select `staff`.`staff_id` AS `staff_id`,if(((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12) >= 1),concat(round((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) / 12),1),' Tahun'),if((timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()) = 0),'Kurang dari sebulan',concat(timestampdiff(MONTH,`staff`.`mulai_bekerja`,curdate()),' Bulan'))) AS `lama_bekerja` from `staff`;

SET FOREIGN_KEY_CHECKS = 1;
