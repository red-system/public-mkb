<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	var $table_name = '';
	public function __construct()
	{
		parent::__construct();
		if(isset($_SESSION['redpos_login']['db_name'])){
            $config =  array(
                'dsn'	=> '',
                'hostname' => $this->db->hostname,
                'username' => $this->db->username,
                'password' => $this->db->password,
                'database' => $_SESSION['redpos_login']['db_name'],
                'dbdriver' => 'mysqli',
                'dbprefix' => '',
                'pconnect' => FALSE,
                'db_debug' => (ENVIRONMENT !== 'production'),
                'cache_on' => FALSE,
                'cachedir' => '',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_general_ci',
                'swap_pre' => '',
                'encrypt' => FALSE,
                'compress' => FALSE,
                'stricton' => FALSE,
                'failover' => array(),
                'save_queries' => TRUE
            );
            $this->db = $this->load->database($config,true);
        } else {
            $config =  array(
                'dsn'	=> '',
                'hostname' => $this->db->hostname,
                'username' => $this->db->username,
                'password' => $this->db->password,
                'database' => "mykindofbeauty_kemiri",
                'dbdriver' => 'mysqli',
                'dbprefix' => '',
                'pconnect' => FALSE,
                'db_debug' => (ENVIRONMENT !== 'production'),
                'cache_on' => FALSE,
                'cachedir' => '',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_general_ci',
                'swap_pre' => '',
                'encrypt' => FALSE,
                'compress' => FALSE,
                'stricton' => FALSE,
                'failover' => array(),
                'save_queries' => TRUE
            );
            $this->db = $this->load->database($config,true);
        }
		
	}
	function delete_by_id($field_name,$field_id,$reason='Deleted as usual'){
        $staff_id = $_SESSION["redpos_login"]["staff_id"];
		$this->db->where($field_name, $field_id);
		$temp = $this->db->get($this->table_name)->result();
		$data['table_name'] = $this->table_name;
        $data['staff_deleted'] = $staff_id;
        $data['property'] = json_encode($temp);
        $data['reason'] = $reason;
        $data['time'] = date('Y-m-d H:i:s');
        $this->db->insert('deleted_record',$data);
        $this->db->where($field_name, $field_id);
		return $this->db->delete($this->table_name);
	}
	function delete_all(){
		return $this->db->empty_table($this->table_name);
	}
    function insert_batch($data){
	    return $this->db->insert_batch($this->table_name,$data);
    }
	function insert($data){
	    $staff_id = isset($_SESSION["redpos_login"]["staff_id"]) ? $_SESSION["redpos_login"]["staff_id"] :0;
	    $data['created_at'] = date('Y-m-d H:i:s');
        $data['staff_created'] = $staff_id;
		return $this->db->insert($this->table_name, $data);
	}
	function last_id(){
		return $this->db->insert_id();
	}
	function update_by_id($field_name,$field_id,$data){
        $staff_id = isset($_SESSION["redpos_login"]["staff_id"]) ? $_SESSION["redpos_login"]["staff_id"] :0;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['staff_updated'] = $staff_id;
		$this->db->where($field_name, $field_id);
		return $this->db->update($this->table_name, $data);
	}
	function detail($field_name,$field_id){
		$this->db->where($field_name, $field_id);
		return $this->db->get($this->table_name);		
	}
	function all_list(){
		return $this->db->get($this->table_name)->result();
	}
	function row_by_id($id){
		$this->db->where($this->table_name."_id", $id);
		return $this->db->get($this->table_name)->row();
	}
    function row_by_field($field_name,$value){
        $this->db->where($field_name, $value);
        return $this->db->get($this->table_name)->row();
    }
	function string_to_number($string){
		$temp = str_replace(".", "", $string);
		$temp = str_replace(",", "", $temp);
		return $temp;
	}
	function string_to_decimal($string){
		$temp = str_replace(",", "", $string);
		return $temp;
	}
	function start_trans(){
		$this->db->trans_begin();		
	}
	function result_trans(){
		if ($this->db->trans_status() === FALSE){
            $this->db->transRollback();
			return FALSE;
        }
		$this->db->trans_commit();
		return TRUE;		
	}
	function get_exists($field_name,$field_id){
        $this->db->from($this->table_name);
		$this->db->where($field_name, $field_id);
		$query = $this->db->count_all_results();
		if($query>0) {
	      	$exist = 1;
	    } else {
	      	$exist = 0;
	    }
	    return $exist;
	}
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
