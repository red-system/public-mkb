<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';

//$route['default_controller'] = 'UnderMaintainceController';

$route['maintaince'] = 'UnderMaintainceController';
$route['home/country'] = "Home/country";
$route['home/most'] = "Home/most";
$route['home/bonus'] = "Home/bonus";
$route['home/bonus-admin'] = "Home/bonus_admin";
$route['home/po'] = "Home/po";
$route['home/group'] = "Home/group";
$route['home/least'] = "Home/least";
$route['home/sales'] = "Home/sales";
$route['home/request-agen'] = "Home/request_agen";
$route['home/request-super'] = "Home/request_super";
$route['home/piutang-akan'] = "Home/piutang_akan";
$route['home/piutang-jatuh-tempo'] = "Home/piutang_jatuh";
$route['home/hutang-akan'] = "Home/hutang_akan";
$route['home/rekap-bulanan'] = "Home/rekap_bulanan";
$route['home/reward'] = "Home/reward";
$route['home/rekap-leader'] = "Home/rekap_omset_leader";
$route['home/leader-board'] = "Home/leader_board";
$route['home/reseller-leader-board'] = "Home/reseller_leader_board";
$route['home/hutang-jatuh-tempo'] = "Home/hutang_jatuh";

$route['home/cek-order-reseller'] = "Home/order_reseller";

$route['list-superreseller'] = "SuperResellerController";
$route['list-superreseller/list'] = "SuperResellerController/list";

$route['404_override'] = 'page_not_found';
$route['translate_uri_dashes'] = FALSE;
$route['login/auth'] = "Login/auth";
$route['logout'] = "Login/logout";
$route['forget'] = "Login/forget";
$route['forget/:any/:any'] = 'Login/forget_index';
$route['reset-password'] = "Login/reset_password";

$route['profile'] = "Profile";
$route['profile/personal-info'] = "Profile/personal_info";
$route['profile/change-password'] = "Profile/change_password";
$route['profile/change-avatar'] = "Profile/change_avatar";
$route['profile/upload-npwp'] = "Profile/upload_npwp";
$route['profile/change-npwp'] = "Profile/update_npwp";
// $route['profile/upgrade'] = "Profile/upgrade_user";
$route['maps'] = "MapsController/index";
$route['maps/update'] = "MapsController/update_koordinat";

$route['user'] = "UserController";
$route['user/list'] = "UserController/list";
$route['user/add'] = "UserController/add";
$route['user/edit'] = "UserController/edit";
$route['user/delete'] = "UserController/delete";

$route['user-role'] = "UserRoleController";
$route['user-role/list'] = "UserRoleController/list";
$route['user-role/add'] = "UserRoleController/add";
$route['user-role/edit'] = "UserRoleController/edit";
$route['user-role/delete'] = "UserRoleController/delete";
$route['user-role/access/:any'] = "RoleAksesController";
$route['user-role/access/edit/:num'] = "RoleAksesController/edit";

$route['staff'] = "StaffController";
$route['staff/list'] = "StaffController/list";
$route['staff/add'] = "StaffController/add";
$route['staff/edit'] = "StaffController/edit";
$route['staff/delete'] = "StaffController/delete";

$route['konversi-bahan'] = "KonversiBahanController";
$route['konversi-bahan/list'] = "KonversiBahanController/list";
$route['konversi-bahan/add'] = "KonversiBahanController/add";
$route['konversi-bahan/edit'] = "KonversiBahanController/edit";
$route['konversi-bahan/delete'] = "KonversiBahanController/delete";

$route['lokasi'] = "LokasiController";
$route['lokasi/list'] = "LokasiController/list";
$route['lokasi/add'] = "LokasiController/add";
$route['lokasi/edit'] = "LokasiController/edit";
$route['lokasi/delete'] = "LokasiController/delete";

$route['assembly/add'] = "AssemblyController/add_index";
$route['assembly/add/save'] = "AssemblyController/add_save";
$route['assembly/edit-assembly/:any'] = "AssemblyController/edit_index";
$route['assembly/edit/save'] = "AssemblyController/edit_save";
$route['assembly/delete-assembly'] = "AssemblyController/delete_assembly_produk";
$route['assembly/delete-stock-perlokasi/:num'] = "AssemblyController/delete_stock_perlokasi";
$route['assembly/produk-list'] = "AssemblyController/produk_list";
$route['assembly/get-unit-satuan'] = "AssemblyController/get_unit_satuan";
$route['assembly/detail-by-id'] = "AssemblyController/assembly_detail_by_id";

$route['konversi-satuan/:any'] = "KonversiSatuanController";
$route['konversi-satuan-list/:any'] = "KonversiSatuanController/list";
$route['konversi-satuan-add'] = "KonversiSatuanController/add";
$route['konversi-satuan-edit'] = "KonversiSatuanController/edit";
$route['konversi-satuan-delete'] = "KonversiSatuanController/delete";

$route['unit/:any'] = "UnitController";
$route['unit-list/:any'] = "UnitController/list";
$route['unit-add'] = "UnitController/add";
$route['unit-edit'] = "UnitController/edit";
$route['unit-delete'] = "UnitController/delete";

$route['bahan-unit/:any'] = "BahanUnitController";
$route['bahan-unit-list/:any'] = "BahanUnitController/list";
$route['bahan-unit-add'] = "BahanUnitController/add";
$route['bahan-unit-edit'] = "BahanUnitController/edit";
$route['bahan-unit-delete'] = "BahanUnitController/delete";

$route['tipe-pembayaran'] = "TipePembayaranController";
$route['tipe-pembayaran/list'] = "TipePembayaranController/list";
$route['tipe-pembayaran/add'] = "TipePembayaranController/add";
$route['tipe-pembayaran/edit'] = "TipePembayaranController/edit";
$route['tipe-pembayaran/delete'] = "TipePembayaranController/delete";

$route['jenis-bahan'] = "JenisBahanController";
$route['jenis-bahan/list'] = "JenisBahanController/list";
$route['jenis-bahan/add'] = "JenisBahanController/add";
$route['jenis-bahan/edit'] = "JenisBahanController/edit";
$route['jenis-bahan/delete'] = "JenisBahanController/delete";

$route['jenis-produk'] = "JenisProdukController";
$route['jenis-produk/list'] = "JenisProdukController/list";
$route['jenis-produk/add'] = "JenisProdukController/add";
$route['jenis-produk/edit'] = "JenisProdukController/edit";
$route['jenis-produk/delete'] = "JenisProdukController/delete";
$route['jenis-produk/options'] = "JenisProdukController/options";

$route['satuan'] = "SatuanController";
$route['satuan/list'] = "SatuanController/list";
$route['satuan/add'] = "SatuanController/add";
$route['satuan/edit'] = "SatuanController/edit";
$route['satuan/delete'] = "SatuanController/delete";

$route['suplier'] = "SuplierController";
$route['suplier/list'] = "SuplierController/list";
$route['suplier/add'] = "SuplierController/add";
$route['suplier/edit'] = "SuplierController/edit";
$route['suplier/delete'] = "SuplierController/delete";
$route['suplier/option'] = "SuplierController/options";
$route['suplier/import'] = "SuplierController/import_form";
$route['suplier/import-save'] = "SuplierController/import_save";
$route['suplier/pdf'] = "SuplierController/pdf";
$route['suplier/excel'] = "SuplierController/excel";

$route['produksi'] = "ProduksiController";
$route['produksi/list'] = "ProduksiController/list";
$route['produksi/add'] = "ProduksiController/add";
$route['produksi/utility/:any'] = "ProduksiController/utility";
$route['produksi/save-add'] = "ProduksiController/save_add";
$route['produksi/edit/:any'] = "ProduksiController/edit";
$route['produksi/detail'] = "ProduksiController/detail";
$route['produksi/save-edit'] = "ProduksiController/save_edit";
$route['produksi/delete'] = "ProduksiController/delete";
$route['produksi/produk-list'] = "ProduksiController/produk_list";
$route['produksi/selesai-produksi'] = "ProduksiController/selesai_produksi";
$route['produksi/penerimaan-produksi'] = "ProduksiController/penerimaan_produksi";
$route['history-produksi'] = "ProduksiController/history_index";
$route['history-produksi/list'] = "ProduksiController/history_list";

$route['produksi/accept-stock'] = "ProduksiController/accept_stock";
$route['produksi/penerimaan/:any'] = "ProduksiController/penerimaan_page";
$route['produksi/penerimaan/list/:num'] = "ProduksiController/penerimaan_list";
$route['produksi/penerimaan/add/:num'] = "ProduksiController/add_penerimaan";
$route['produksi/penerimaan/edit/:num'] = "ProduksiController/edit_penerimaan";
$route['produksi/penerimaan/delete/:num'] = "ProduksiController/delete_penerimaan";
$route['produksi/penerimaan/detail/:num'] = "ProduksiController/detail_penerimaan";

$route['custom-produksi'] = "CustomProduksiController";
$route['custom-produksi/list'] = "CustomProduksiController/list";
$route['custom-produksi/start/:any'] = "CustomProduksiController/start";
$route['custom-produksi/utility/:any'] = "CustomProduksiController/utility";
$route['custom-produksi/save-start'] = "CustomProduksiController/save_start";
$route['custom-produksi/edit/:any'] = "CustomProduksiController/edit";
$route['custom-produksi/detail'] = "CustomProduksiController/detail";
$route['custom-produksi/save-edit'] = "CustomProduksiController/save_edit";
$route['custom-produksi/delete'] = "CustomProduksiController/delete";
$route['custom-produksi/selesai-produksi'] = "CustomProduksiController/selesai_produksi";
$route['custom-produksi/penerimaan-produksi'] = "CustomProduksiController/penerimaan_produksi";
$route['custom-history-produksi'] = "CustomProduksiController/history_index";
$route['custom-history-produksi/list'] = "CustomProduksiController/history_list";


$route['po-produk'] = "PoProdukController";
$route['po-produk/list'] = "PoProdukController/list";
$route['po-produk/add'] = "PoProdukController/add";
$route['po-produk/utility/:any'] = "PoProdukController/utility";
$route['po-produk/save-add'] = "PoProdukController/save_add";
$route['po-produk/save-start'] = "PoProdukController/save_start";
$route['po-produk/edit/:any'] = "PoProdukController/edit";
$route['po-produk/detail'] = "PoProdukController/detail";
$route['po-produk/save-edit'] = "PoProdukController/save_edit";
$route['po-produk/delete'] = "PoProdukController/delete";
$route['po-produk/pay'] = "PoProdukController/pay";
$route['po-produk/detail-pembayaran'] = "PoProdukController/detail_pembayaran";
$route['po-produk/ulangi-pembayaran'] = "PoProdukController/ulangi_pembayaran";
$route['po-produk/response-payment'] = "PoProdukController/response_payment_save";
$route['po-produk/selesai-po-produk'] = "PoProdukController/selesai_po-produk";
$route['po-produk/penerimaan-po-produk'] = "PoProdukController/penerimaan_po_produk";
$route['po-produk/print/:num'] = "PoProdukController/print";
$route['history-po-produk'] = "PoProdukController/history";
$route['history-po-produk/list'] = "PoProdukController/history_list";

$route['po-bahan'] = "PoBahanController";
$route['po-bahan/list'] = "PoBahanController/list";
$route['po-bahan/add'] = "PoBahanController/add";
$route['po-bahan/utility/:any'] = "PoBahanController/utility";
$route['po-bahan/save-add'] = "PoBahanController/save_add";
$route['po-bahan/save-start'] = "PoBahanController/save_start";
$route['po-bahan/edit/:any'] = "PoBahanController/edit";
$route['po-bahan/detail'] = "PoBahanController/detail";
$route['po-bahan/save-edit'] = "PoBahanController/save_edit";
$route['po-bahan/delete'] = "PoBahanController/delete";
$route['po-bahan/selesai-po-bahan'] = "PoBahanController/selesai_po-bahan";
$route['po-bahan/penerimaan-po-bahan'] = "PoBahanController/penerimaan_po_bahan";
$route['po-bahan/print/:num'] = "PoBahanController/print";
$route['history-po-bahan'] = "PoBahanController/history";
$route['history-po-bahan/list'] = "PoBahanController/history_list";

$route['po-bahan/accept-stock'] = "PoBahanController/accept_stock";
$route['po-bahan/penerimaan/:any'] = "PoBahanController/penerimaan_page";
$route['po-bahan/penerimaan/list/:num'] = "PoBahanController/penerimaan_list";
$route['po-bahan/penerimaan/add/:num'] = "PoBahanController/add_penerimaan";
$route['po-bahan/penerimaan/edit/:num'] = "PoBahanController/edit_penerimaan";
$route['po-bahan/penerimaan/delete/:num'] = "PoBahanController/delete_penerimaan";
$route['po-bahan/penerimaan/detail/:num'] = "PoBahanController/detail_penerimaan";

$route['member'] = "GuestController";
$route['member/view'] = "GuestController/view";
$route['member/list'] = "GuestController/list";
$route['member/add'] = "GuestController/add";
$route['member/edit'] = "GuestController/edit";
$route['member/delete'] = "GuestController/delete";

$route['bahan'] = "BahanController";
$route['bahan/list'] = "BahanController/list";
$route['bahan/add'] = "BahanController/add";
$route['bahan/edit'] = "BahanController/edit";
$route['bahan/delete'] = "BahanController/delete";

$route['stock-bahan/:any'] = "StockBahanController";
$route['stock-bahan/list/:num'] = "StockBahanController/list";
$route['stock-bahan/list-stock/:num'] = "StockBahanController/list_stock";
$route['stock-bahan/add/:num'] = "StockBahanController/add";
$route['stock-bahan/edit/:num'] = "StockBahanController/edit";
$route['stock-bahan/delete/:num'] = "StockBahanController/delete";

$route['harga-produk/:any'] = "HargaProdukController";
$route['harga-produk/list/:num'] = "HargaProdukController/list";
$route['harga-produk/add/:num'] = "HargaProdukController/add";
$route['harga-produk/edit/:num'] = "HargaProdukController/edit";
$route['harga-produk/delete/:num'] = "HargaProdukController/delete";

$route['produk'] = "ProdukController";
$route['produk/list'] = "ProdukController/list";
$route['produk/add'] = "ProdukController/add";
$route['produk/edit'] = "ProdukController/edit";
$route['produk/harga/:num'] = "ProdukController/harga";
$route['produk/item_produk/:num'] = "ProdukController/item_produk";
$route['produk/delete'] = "ProdukController/delete";
$route['produk/import'] = "ProdukController/import_form";
$route['produk/import-save'] = "ProdukController/import_stock";

$route['resto-produk'] = "resto/RestoProdukController";
$route['resto-produk/list'] = "resto/RestoProdukController/list";
$route['resto-produk/add'] = "resto/RestoProdukController/add";
$route['resto-produk/edit'] = "resto/RestoProdukController/edit";
$route['resto-produk/harga/:num'] = "resto/RestoProdukController/harga";
$route['resto-produk/item_produk/:num'] = "resto/RestoProdukController/item_produk";
$route['resto-produk/delete'] = "resto/RestoProdukController/delete";

$route['stock-produk/:any'] = "StockProdukController";
$route['stock-produk/list/:num'] = "StockProdukController/list";
$route['stock-produk/list-stock/:num'] = "StockProdukController/list_stock";
$route['stock-produk/add/:num'] = "StockProdukController/add";
$route['stock-produk/edit/:num'] = "StockProdukController/edit";
$route['stock-produk/delete/:num'] = "StockProdukController/delete";
$route['stock-produk/barcode/:num'] = "StockProdukController/barcode";
$route['stock-produk/print-barcode/:any/:num/:num'] = "StockProdukController/print_barcode";

$route['transfer-bahan'] = "TransferBahanController";
$route['transfer-bahan/list'] = "TransferBahanController/list";
$route['transfer-bahan/stock/:any'] = "TransferBahanController/stock_index";
$route['transfer-bahan/stock/list/:num'] = "TransferBahanController/stock_list";
$route['transfer-bahan/stock/transfer/:num'] = "TransferBahanController/transfer";
$route['konfirmasi-transfer-bahan'] = "KonfirmasiTransferBahanController";
$route['konfirmasi-transfer-bahan/list'] = "KonfirmasiTransferBahanController/list";
$route['konfirmasi-transfer-bahan/confirm'] = "KonfirmasiTransferBahanController/confirm";
$route['history-transfer-bahan'] = "HistoryTransferBahanController";
$route['history-transfer-bahan/list'] = "HistoryTransferBahanController/list";
$route['history-transfer-bahan/pdf'] = "HistoryTransferBahanController/pdf";
$route['history-transfer-bahan/excel'] = "HistoryTransferBahanController/excel";

$route['transfer-produk'] = "TransferProdukController";
$route['transfer-produk/list'] = "TransferProdukController/list";
$route['transfer-produk/stock/:any'] = "TransferProdukController/stock_index";
$route['transfer-produk/stock/list/:num'] = "TransferProdukController/stock_list";
$route['transfer-produk/stock/transfer/:num'] = "TransferProdukController/transfer";
$route['konfirmasi-transfer-produk'] = "KonfirmasiTransferProdukController";
$route['konfirmasi-transfer-produk/list'] = "KonfirmasiTransferProdukController/list";
$route['konfirmasi-transfer-produk/confirm'] = "KonfirmasiTransferProdukController/confirm";
$route['history-transfer-produk'] = "HistoryTransferProdukController";
$route['history-transfer-produk/list'] = "HistoryTransferProdukController/list";
$route['history-transfer-produk/pdf'] = "HistoryTransferProdukController/pdf";
$route['history-transfer-produk/excel'] = "HistoryTransferProdukController/excel";

$route['penyesuaian-bahan'] = "PenyesuaianStockBahanController";
$route['penyesuaian-bahan/list'] = "PenyesuaianStockBahanController/list";
$route['penyesuaian-bahan/stock/adjust/:num'] = "PenyesuaianStockBahanController/adjust";
$route['penyesuaian-bahan/stock/:any'] = "PenyesuaianStockBahanController/stock_index";
$route['history-penyesuaian-bahan'] = "HistoryPenyesuaianBahanController";
$route['history-penyesuaian-bahan/list'] = "HistoryPenyesuaianBahanController/list";
$route['history-penyesuaian-bahan/pdf'] = "HistoryPenyesuaianBahanController/pdf";
$route['history-penyesuaian-bahan/excel'] = "HistoryPenyesuaianBahanController/excel";

$route['penyesuaian-produk'] = "PenyesuaianStockProdukController";
$route['penyesuaian-produk/list'] = "PenyesuaianStockProdukController/list";
$route['penyesuaian-produk/stock/:any'] = "PenyesuaianStockProdukController/stock_index";
$route['penyesuaian-produk/stock/adjust/:num'] = "PenyesuaianStockProdukController/adjust";
$route['history-penyesuaian-produk'] = "HistoryPenyesuaianProdukController";
$route['history-penyesuaian-produk/list'] = "HistoryPenyesuaianProdukController/list";
$route['history-penyesuaian-produk/pdf'] = "HistoryPenyesuaianProdukController/pdf";
$route['history-penyesuaian-produk/excel'] = "HistoryPenyesuaianProdukController/excel";

$route['low-stock-bahan'] = "LowStockBahanController";
$route['low-stock-bahan/list'] = "LowStockBahanController/list";

$route['low-stock-produk'] = "LowStockProdukController";
$route['low-stock-produk/list'] = "LowStockProdukController/list";
$route['low-stock-produk/pdf'] = "LowStockProdukController/pdf";
$route['low-stock-produk/excel'] = "LowStockProdukController/excel";

$route['table'] = "TableController";
$route['table/list'] = "TableController/list";
$route['table/add'] = "TableController/add";
$route['table/edit'] = "TableController/edit";
$route['table/delete'] = "TableController/delete";

$route['komposisi-produk'] = "KomposisiProdukController";
$route['komposisi-produk/list'] = "KomposisiProdukController/list";
$route['komposisi-produk/komposisi/:any'] = "KomposisiProdukController/komposisi_index";
$route['komposisi-produk/komposisi/add/:num'] = "KomposisiProdukController/add";
$route['komposisi-produk/komposisi/edit/:num'] = "KomposisiProdukController/edit";
$route['komposisi-produk/komposisi/delete/:num'] = "KomposisiProdukController/delete";
$route['komposisi-produk/komposisi/list/:num'] = "KomposisiProdukController/komposisi_list";

$route['quality-control'] = 'QualityControlController';
$route['quality-control/list'] = 'QualityControlController/list';
$route['quality-control/retur-form/:any'] = 'QualityControlController/retur_form';
$route['quality-control/retur'] = 'QualityControlController/retur';
$route['quality-control/rugi'] = 'QualityControlController/rugi';
$route['quality-control/rugi-form/:any'] = 'QualityControlController/rugi_form';
$route['quality-control/stock/:num/:num'] = 'QualityControlController/stock_list';
$route['quality-control/po/:num/:num'] = 'QualityControlController/po_list';

$route['retur-produk'] = 'ReturController';
$route['retur-produk/list'] = 'ReturController/list';
$route['retur-produk/retur-form/:any'] = 'ReturController/retur_form';
$route['retur-produk/retur-form'] = 'ReturController/retur_form';
$route['retur-produk/save'] = 'ReturController/save';
$route['retur-produk/delete'] = 'ReturController/delete';
$route['retur-produk/posting/:any'] = 'ReturController/posting';
$route['retur-produk/detail/:num'] = 'ReturController/detail';
$route['retur-produk/list-po'] = 'ReturController/list_po';
$route['retur-produk/change-location'] = 'ReturController/change_location';

$route['retur-gantung'] = 'ReturGantungController';
$route['retur-gantung/list'] = 'ReturGantungController/list';
$route['retur-gantung/retur-form/:any'] = 'ReturGantungController/retur_form';
$route['retur-gantung/retur-form'] = 'ReturGantungController/retur_form';
$route['retur-gantung/save'] = 'ReturGantungController/save';
$route['retur-gantung/delete'] = 'ReturGantungController/delete';
$route['retur-gantung/posting/:any'] = 'ReturGantungController/posting';
$route['retur-gantung/detail/:num'] = 'ReturGantungController/detail';
$route['retur-gantung/list-po'] = 'ReturGantungController/list_po';
$route['retur-gantung/change-location'] = 'ReturGantungController/change_location';
$route['retur-gantung/potong'] = 'ReturGantungController/potong';
$route['retur-gantung/po-hutang/:num'] = 'ReturGantungController/po_hutang';

$route['produk-by-location'] = "ProdukByLocation";
$route['produk-by-location/list'] = "ProdukByLocation/list";
$route['produk-by-location/pdf'] = "ProdukByLocation/pdf";
$route['produk-by-location/excel'] = "ProdukByLocation/excel";

$route['produk-recipe'] = "ProdukRecipeController";
$route['produk-recipe/list'] = "ProdukRecipeController/list";
$route['produk-recipe/form'] = "ProdukRecipeController/form";
$route['produk-recipe/form/:any'] = "ProdukRecipeController/form";
$route['produk-recipe/save'] = "ProdukRecipeController/save";
$route['produk-recipe/delete'] = "ProdukRecipeController/delete";
$route['produk-recipe/list-produk'] = "ProdukRecipeController/list_produk";
$route['produk-recipe/list-bahan'] = "ProdukRecipeController/list_bahan";

$route['komposisi-produk-bahan'] = "KomposisiProdukBahanController";
$route['komposisi-produk-bahan/list'] = "KomposisiProdukBahanController/list";
$route['komposisi-produk-bahan/detail'] = "KomposisiProdukBahanController/detail";
$route['komposisi-produk-bahan/form'] = "KomposisiProdukBahanController/form";
$route['komposisi-produk-bahan/form/:any'] = "KomposisiProdukBahanController/form";
$route['komposisi-produk-bahan/save'] = "KomposisiProdukBahanController/save";
$route['komposisi-produk-bahan/delete'] = "KomposisiProdukBahanController/delete";
$route['komposisi-produk-bahan/list-produk'] = "KomposisiProdukBahanController/list_produk";
$route['komposisi-produk-bahan/list-bahan'] = "KomposisiProdukBahanController/list_bahan";

$route['bahan-by-location'] = "BahanByLocation";
$route['bahan-by-location/list'] = "BahanByLocation/list";
$route['bahan-by-location/pdf'] = "BahanByLocation/pdf";
$route['bahan-by-location/excel'] = "BahanByLocation/excel";

$route['kartu-stock-bahan'] = "KartuStockBahanController";
$route['kartu-stock-bahan/list'] = "KartuStockBahanController/list";
$route['kartu-stock-bahan/pdf'] = "KartuStockBahanController/pdf";
$route['kartu-stock-bahan/excel'] = "KartuStockBahanController/excel";

$route['kartu-stock-produk'] = "KartuStockProdukController";
$route['kartu-stock-produk/list'] = "KartuStockProdukController/list";
$route['kartu-stock-produk/pdf'] = "KartuStockProdukController/pdf";
$route['kartu-stock-produk/excel'] = "KartuStockProdukController/excel";

$route['pos'] = "POSController";
$route['pos/guest'] = "POSController/guest_list";
$route['pos/utility/:any'] = "POSController/utility";
$route['pos/produk'] = "POSController/produk_list";
$route['pos/delete'] = "RekapitulasiPosController/delete";
$route['pos/save'] = "POSController/save";
$route['pos/save-preaty-cash'] = "POSController/save_preaty_cash";
$route['pos/closing'] = "POSController/closing";
$route['pos/print/:num'] = "POSController/print";
$route['pos/pdf/:num'] = "POSController/pos_pdf";
$route['pos/get-unit-satuan'] = "POSController/get_unit_satuan";
$route['pos/info-closing'] = "POSController/info_closing";
$route['pos/print-closing/:num'] = "POSController/print_closing";
$route['pos/normalisasi'] = "POSController/normalisasi";
$route['rekapitulasi-pos'] = "RekapitulasiPosController";
$route['rekapitulasi-pos/list'] = "RekapitulasiPosController/list";
$route['rekapitulasi-pos/detail/:num'] = "RekapitulasiPosController/detail";

$route['pos-agen'] = "POSAgenController";
$route['pos-agen/guest'] = "POSAgenController/guest_list";
$route['pos-agen/utility/:any'] = "POSAgenController/utility";
$route['pos-agen/produk'] = "POSAgenController/produk_list";
$route['pos-agen/save'] = "POSAgenController/save";
$route['pos-agen/save-preaty-cash'] = "POSAgenController/save_preaty_cash";
$route['pos-agen/closing'] = "POSAgenController/closing";
$route['pos-agen/print/:num'] = "POSAgenController/print";
$route['pos-agen/get-unit-satuan'] = "POSAgenController/get_unit_satuan";
$route['pos-agen/info-closing'] = "POSAgenController/info_closing";
$route['pos-agen/print-closing/:num'] = "POSAgenController/print_closing";
$route['pos-agen/normalisasi'] = "POSAgenController/normalisasi";

$route['penukaran-po'] = "POSAgenController/penukaran";
$route['penukaran-po/check'] = "POSAgenController/check";
$route['penukaran-po-save'] = "POSAgenController/penukaran_save";

$route['history-penukaran-po'] = "POSAgenController/history_penukaran";
$route['history-penukaran-po/list'] = "POSAgenController/history_penukaran_list";

$route['resto-pos'] = "resto/RestoPOSController";
$route['resto-pos/add-item/:any'] = "resto/RestoPOSController";
$route['resto-pos/guest'] = "resto/RestoPOSController/guest_list";
$route['resto-pos/utility/:any'] = "resto/RestoPOSController/utility";
$route['resto-pos/produk'] = "resto/RestoPOSController/produk_list";
$route['resto-pos/delete'] = "resto/RestoRekapitulasiPosController/delete";
$route['resto-pos/save'] = "resto/RestoPOSController/save";
$route['resto-pos/save-order'] = "resto/RestoPOSController/save_order";
$route['resto-pos/save-preaty-cash'] = "resto/RestoPOSController/save_preaty_cash";
$route['resto-pos/closing'] = "resto/RestoPOSController/closing";
$route['resto-pos/print/:num'] = "resto/RestoPOSController/print";
$route['resto-pos/get-unit-satuan'] = "resto/RestoPOSController/get_unit_satuan";
$route['resto-pos/info-closing'] = "resto/RestoPOSController/info_closing";
$route['resto-pos/print-closing/:num'] = "resto/RestoPOSController/print_closing";
$route['resto-pos/normalisasi'] = "resto/RestoPOSController/normalisasi";
$route['rekapitulasi-resto-pos'] = "resto/RestoRekapitulasiPosController";
$route['rekapitulasi-resto-pos/list'] = "resto/RestoRekapitulasiPosController/list";
$route['rekapitulasi-resto-pos/detail/:num'] = "resto/RestoRekapitulasiPosController/detail";
$route['order-resto-pos'] = "resto/RestoOrderPOSController";
$route['order-resto-pos/get-item'] = "resto/RestoOrderPOSController/get_item";
$route['order-resto-pos/process'] = "resto/RestoOrderPOSController/process";
$route['order-resto-pos/list'] = "resto/RestoOrderPOSController/list";
$route['order-resto-pos-detail/:any'] = "resto/RestoOrderPOSController/detail";
$route['order-resto-pos-cancel'] = "resto/RestoOrderPOSController/cancel";
$route['resto-pos/print-order/:any'] = "resto/RestoOrderPOSController/print";
$route['resto-pos/get-promo'] = "resto/RestoPOSController/get_promo";

$route['custom-pos'] = "CustomPOSController";
$route['custom-pos/save'] = "CustomPOSController/save";

$route['hutang'] = "HutangController";
$route['hutang/list'] = "HutangController/list";
$route['hutang/pay/:any'] = "HutangController/pay";
$route['hutang/pay/list/:num'] = "HutangController/pay_list";
$route['hutang/pay/add/:num'] = "HutangController/add";
$route['hutang/pay/edit/:num'] = "HutangController/edit";
$route['hutang/pay/delete/:num'] = "HutangController/delete";
$route['hutang/detail/:num'] = "HutangController/detail";
$route['hutang/posting/:any'] = "HutangController/posting";
$route['history-hutang'] = "HistoryHutangController";
$route['history-hutang/list'] = "HistoryHutangController/list";

$route['piutang'] = "PiutangController";
$route['piutang/list'] = "PiutangController/list";
$route['piutang/pay/:any'] = "PiutangController/pay";
$route['piutang/pay/list/:num'] = "PiutangController/pay_list";
$route['piutang/pay/add/:num'] = "PiutangController/add";
$route['piutang/pay/edit/:num'] = "PiutangController/edit";
$route['piutang/pay/delete/:num'] = "PiutangController/delete";
$route['piutang/detail/:num'] = "PiutangController/detail";
$route['piutang/posting/:any'] = "PiutangController/posting";
$route['history-piutang'] = "HistoryPiutangController";
$route['history-piutang/list'] = "HistoryPiutangController/list";

$route['laporan/:any'] = "LaporanController";
$route['laporan/:any/:any'] = "LaporanController";
$route['laporan/:any/:any/:any'] = "LaporanController";

$route['accounting/:any'] = "AccountingController";
$route['accounting/:any/:any'] = "AccountingController";
$route['accounting/:any/:any/:any'] = "AccountingController";

$route['size'] = "SizeController";
$route['size/list'] = "SizeController/list";
$route['size/add'] = "SizeController/add";
$route['size/edit'] = "SizeController/edit";
$route['size/delete'] = "SizeController/delete";
$route['size/options'] = "SizeController/options";

$route['color'] = "ColorController";
$route['color/list'] = "ColorController/list";
$route['color/add'] = "ColorController/add";
$route['color/edit'] = "ColorController/edit";
$route['color/delete'] = "ColorController/delete";
$route['color/options'] = "ColorController/options";

$route['fabric'] = "FabricController";
$route['fabric/list'] = "FabricController/list";
$route['fabric/add'] = "FabricController/add";
$route['fabric/edit'] = "FabricController/edit";
$route['fabric/delete'] = "FabricController/delete";
$route['fabric/options'] = "FabricController/options";

$route['patern'] = "PaternController";
$route['patern/list'] = "PaternController/list";
$route['patern/add'] = "PaternController/add";
$route['patern/edit'] = "PaternController/edit";
$route['patern/delete'] = "PaternController/delete";
$route['patern/options'] = "PaternController/options";
$route['patern/test'] = "PaternController/test";

$route['registrasi'] = "RegisterController";
$route['registrasi-v1/:any'] = "RegisterController/v_satu";
$route['registrasi/:any'] = "RegisterController";
$route['registrasi-reseller/:any'] = "RegisterController/agen";
$route['registrasi-reseller'] = "RegisterController/agen";
$route['registrasi-superreseller/:any'] = "RegisterController/superagen";
$route['registrasi-superreseller'] = "RegisterController/superagen";
$route['registrasi-save'] = "RegisterController/save";
$route['registrasi-upload'] = "RegisterController/upload";
$route['registrasi-city'] = "RegisterController/city";
$route['registrasi-success'] = "RegisterController/success";
$route['registrasi-subdistrict'] = "RegisterController/subdistrict";

$route['super-agen'] = "SuperAgentController";
$route['super-agen/list'] = "SuperAgentController/list";
$route['aktif-super-agen'] = "SuperAgentController/aktif";
$route['aktif-super-agen/list'] = "SuperAgentController/aktif_list";
$route['pasif-super-agen'] = "SuperAgentController/pasif";
$route['pasif-super-agen/list'] = "SuperAgentController/pasif_list";
$route['banned-super-agen'] = "SuperAgentController/banned";
$route['banned-super-agen/list'] = "SuperAgentController/banned_list";
$route['accepted-super-agen'] = "SuperAgentController/accepted";
$route['accepted-super-agen/list'] = "SuperAgentController/accepted_list";

$route['pemetaan-super-reseller'] = "PemetaanSuperAgentController";
$route['pemetaan-super-reseller/list'] = "PemetaanSuperAgentController/list";

$route['pemetaan-reseller'] = "PemetaanAgentController";
$route['pemetaan-reseller/list'] = "PemetaanAgentController/list";

$route['redgroup'] = "RedGroupController";
$route['redgroup/list'] = "RedGroupController/list";
$route['aktif-redgroup'] = "RedGroupController/aktif";
$route['aktif-redgroup/list'] = "RedGroupController/aktif_list";
$route['banned-redgroup'] = "RedGroupController/banned";
$route['banned-redgroup/list'] = "RedGroupController/banned_list";

$route['prospective-reseller'] = "ProspectiveReseller";
$route['prospective/maps_save'] = "ProspectiveReseller/maps_save";
$route['prospective-reseller/list'] = "ProspectiveReseller/list";
$route['aktif-agen'] = "ProspectiveReseller/aktif";
$route['aktif-agen/list'] = "ProspectiveReseller/aktif_list";
$route['pasif-agen'] = "ProspectiveReseller/pasif";
$route['pasif-agen/list'] = "ProspectiveReseller/pasif_list";
$route['prospective-reseller/accept'] = "ProspectiveReseller/accept";
$route['prospective-reseller/refuse'] = "ProspectiveReseller/refuse";
$route['banned-agen'] = "ProspectiveReseller/banned";
$route['banned-agen/list'] = "ProspectiveReseller/banned_list";
$route['prospective-reseller/banned-save'] = "ProspectiveReseller/banned_save";
$route['prospective-reseller/aktif-save'] = "ProspectiveReseller/aktif_save";
$route['prospective-reseller/update-save'] = "ProspectiveReseller/update_save";
$route['prospective-reseller/resend'] = "ProspectiveReseller/resend";
$route['accepted-agen'] = "ProspectiveReseller/accepted";
$route['accepted-agen/list'] = "ProspectiveReseller/accepted_list";

$route['deposit-super-reseller'] = 'DepositSuperResellerController';
$route['deposit-super-reseller/list'] = 'DepositSuperResellerController/list';

$route['reseller-find/city'] = "ResellerFindController/getCity";
$route['reseller-find/subdistrict'] = "ResellerFindController/getSubdistrict";

$route['registrasi-redgroup'] = "RegisterRedController";
$route['registrasi-redgroup-save'] = "RegisterRedController/save";

$route['activation/:any'] = "Activation";
$route['activation-cek/:any'] = "Activation/cek";
$route['activation-save'] = "Activation/save";

$route['discount'] = "DiscountController";
$route['discount/list'] = "DiscountController/list";
$route['discount/add'] = "DiscountController/add";
$route['discount/edit'] = "DiscountController/edit";
$route['discount/delete'] = "DiscountController/delete";
$route['discount/produk/add'] = "DiscountController/produk_add";
$route['discount/produk/detail'] = "DiscountController/produk_detail";
$route['discount/produk/detail/set-id'] = "DiscountController/produk_detail_set_id";
$route['discount/all-produk'] = "DiscountController/produk_all";
$route['discount/set-id'] = "DiscountController/set_id";
$route['discount/unset-id'] = "DiscountController/unset_id";

$route['master/login'] = "MasterLoginController";
$route['master/login/auth'] = "MasterLoginController/auth";
$route['master/logout'] = "MasterLoginController/logout";

$route['master'] = "MasterHomeController";

$route['master/company'] = "MasterCompanyController";
$route['master/company/list'] = "MasterCompanyController/list";
$route['master/company/add'] = "MasterCompanyController/add";
$route['master/company/edit'] = "MasterCompanyController/edit";
$route['master/company/delete'] = "MasterCompanyController/delete";
$route['master/company/enter'] = "MasterCompanyController/enter";

$route['master/type'] = "MasterTypeController";
$route['master/type/list'] = "MasterTypeController/list";
$route['master/type/add'] = "MasterTypeController/add";
$route['master/type/edit'] = "MasterTypeController/edit";
$route['master/type/delete'] = "MasterTypeController/delete";
$route['master/type/access/:any'] = "MasterAksesController";
$route['master/type/access/edit/:num'] = "MasterAksesController/edit";

$route['master/menu'] = "MasterMenuController";
$route['master/menu/list'] = "MasterMenuController/list";
$route['master/menu/add'] = "MasterMenuController/add";
$route['master/menu/edit'] = "MasterMenuController/edit";
$route['master/menu/delete'] = "MasterMenuController/delete";

$route['master/submenu/:any'] = "MasterSubmenuController";
$route['master/submenu-list/:num'] = "MasterSubmenuController/list";
$route['master/submenu-add'] = "MasterSubmenuController/add";
$route['master/submenu-edit'] = "MasterSubmenuController/edit";
$route['master/submenu-delete'] = "MasterSubmenuController/delete";

$route['master/user'] = "MasterUserController";
$route['master/user/list'] = "MasterUserController/list";
$route['master/user/login'] = "MasterUserController/login";
$route['master/user/enter/(:any)'] = "MasterUserController/enter/$1";

$route['pay/notify'] = "NotifyController";
$route['pay/notify_manual'] = "NotifyController/manual";
$route['pay/test'] = "NotifyController/test";
$route['test-last-deposit'] = "NotifyController/test_last_deposit";

$route['payment-gateway-transaction'] = 'PaymentGatewayTransaction';
$route['payment-gateway-transaction/list'] = 'PaymentGatewayTransaction/list';

$route['bonus'] = "BonusController";
$route['bonus/list'] = "BonusController/list";
$route['bonus/pembayaran'] = "BonusController/pembayaran";
$route['test/:num'] = "PaternController/test";

$route['order-super-agen'] = "PoSaController";
$route['order-super-agen/list'] = "PoSaController/list";
$route['order-super-agen/accept'] = "PoSaController/accept";
$route['order-super-agen/refuse'] = "PoSaController/refuse";
$route['order-super-agen/accept-stock'] = "PoSaController/accept_stock";
$route['order-super-agen/penerimaan/:any'] = "PoSaController/penerimaan";
$route['order-super-agen/penerimaan/list/:num'] = "PoSaController/penerimaan_list";
$route['order-super-agen/penerimaan/add/:num'] = "PoSaController/add";
$route['order-super-agen/penerimaan/edit/:num'] = "PoSaController/edit";
$route['order-super-agen/penerimaan/delete/:num'] = "PoSaController/delete";
$route['order-super-agen/penerimaan/detail/:num'] = "PoSaController/detail";

$route['history-order-super-agen'] = "PoSaController/history";
$route['history-order-super-agen/list'] = "PoSaController/history_list";


$route['voucher'] = "VoucherController";
$route['voucher/list'] = "VoucherController/list";
$route['voucher/detail'] = "VoucherController/detail";
$route['voucher/print/:num'] = "VoucherController/print";

$route['voucher-retur'] = "VoucherReturController";
$route['voucher-retur/list'] = "VoucherReturController/list";
$route['voucher-retur/detail'] = "VoucherReturController/detail";
$route['voucher-retur/print/:num'] = "VoucherReturController/print";

$route['piutang-produk'] = "PiutangProdukController";
$route['piutang-produk/list'] = "PiutangProdukController/list";
$route['hutang-produk'] = "HutangProdukController";
$route['hutang-produk/list'] = "HutangProdukController/list";

$route['voucher'] = "VoucherController";
$route['voucher/list'] = "VoucherController/list";

$route['pembayaran-hutang-produk'] = "PembayaranHutangProdukController";
$route['pembayaran-hutang-produk/list'] = "PembayaranHutangProdukController/list";
$route['pembayaran-hutang-produk/add'] = "PembayaranHutangProdukController/add";
$route['pembayaran-hutang-produk/utility/:any'] = "PembayaranHutangProdukController/utility";
$route['pembayaran-hutang-produk/save-add'] = "PembayaranHutangProdukController/save_add";
$route['pembayaran-hutang-produk/save-start'] = "PembayaranHutangProdukController/save_start";
$route['pembayaran-hutang-produk/edit/:any'] = "PembayaranHutangProdukController/edit";
$route['pembayaran-hutang-produk/detail'] = "PembayaranHutangProdukController/detail";
$route['pembayaran-hutang-produk/save-edit'] = "PembayaranHutangProdukController/save_edit";
$route['pembayaran-hutang-produk/delete'] = "PembayaranHutangProdukController/delete";
$route['pembayaran-hutang-produk/approve'] = "PembayaranHutangProdukController/approve";
$route['pembayaran-hutang-produk/refuse'] = "PembayaranHutangProdukController/refuse";
$route['pembayaran-hutang-produk/selesai-pembayaran-hutang-produk'] = "PembayaranHutangProdukController/selesai_pembayaran-hutang-produk";
$route['pembayaran-hutang-produk/penerimaan-pembayaran-hutang-produk'] = "PembayaranHutangProdukController/penerimaan_po_produk";
$route['pembayaran-hutang-produk/print/:num'] = "PembayaranHutangProdukController/print";
$route['pembayaran-hutang-produk/approve-page'] = "PembayaranHutangProdukController/approve_page";
$route['pembayaran-hutang-produk/approve-list'] = "PembayaranHutangProdukController/approve_list";

$route['pembayaran-hutang-produk/refuse-page'] = "PembayaranHutangProdukController/refuse_page";
$route['pembayaran-hutang-produk/refuse-list'] = "PembayaranHutangProdukController/refuse_list";

$route['pembayaran-hutang-produk/accept-stock'] = "PembayaranHutangProdukController/accept_stock";
$route['pembayaran-hutang-produk/pengiriman/:any'] = "PembayaranHutangProdukController/pengiriman_page";
$route['pembayaran-hutang-produk/pengiriman/list/:num'] = "PembayaranHutangProdukController/pengiriman_list";
$route['pembayaran-hutang-produk/pengiriman/add/:num'] = "PembayaranHutangProdukController/add_pengiriman";
$route['pembayaran-hutang-produk/pengiriman/edit/:num'] = "PembayaranHutangProdukController/edit_pengiriman";
$route['pembayaran-hutang-produk/pengiriman/delete/:num'] = "PembayaranHutangProdukController/delete_pengiriman";
$route['pembayaran-hutang-produk/pengiriman/detail/:num'] = "PembayaranHutangProdukController/detail_pengiriman";

$route['pembayaran-withdraw-cash'] = "PembayaranWithdrawCashController";
$route['pembayaran-withdraw-cash/list'] = "PembayaranWithdrawCashController/list";
$route['pembayaran-withdraw-cash/add'] = "PembayaranWithdrawCashController/add";
$route['pembayaran-withdraw-cash/utility/:any'] = "PembayaranWithdrawCashController/utility";
$route['pembayaran-withdraw-cash/save-add'] = "PembayaranWithdrawCashController/save_add";
$route['pembayaran-withdraw-cash/save-start'] = "PembayaranWithdrawCashController/save_start";
$route['pembayaran-withdraw-cash/edit/:any'] = "PembayaranWithdrawCashController/edit";
$route['pembayaran-withdraw-cash/detail'] = "PembayaranWithdrawCashController/detail";
$route['pembayaran-withdraw-cash/save-edit'] = "PembayaranWithdrawCashController/save_edit";
$route['pembayaran-withdraw-cash/delete'] = "PembayaranWithdrawCashController/delete";
$route['pembayaran-withdraw-cash/approve'] = "PembayaranWithdrawCashController/approve";
$route['pembayaran-withdraw-cash/refuse'] = "PembayaranWithdrawCashController/refuse";
$route['pembayaran-withdraw-cash/selesai-pembayaran-withdraw-cash'] = "PembayaranWithdrawCashController/selesai_pembayaran-withdraw-cash";
$route['pembayaran-withdraw-cash/penerimaan-pembayaran-withdraw-cash'] = "PembayaranWithdrawCashController/penerimaan_po_produk";
$route['pembayaran-withdraw-cash/print/:num'] = "PembayaranWithdrawCashController/print";
$route['pembayaran-withdraw-cash/approve-page'] = "PembayaranWithdrawCashController/approve_page";
$route['pembayaran-withdraw-cash/approve-list'] = "PembayaranWithdrawCashController/approve_list";

$route['pembayaran-withdraw-cash/refuse-page'] = "PembayaranWithdrawCashController/refuse_page";
$route['pembayaran-withdraw-cash/refuse-list'] = "PembayaranWithdrawCashController/refuse_list";

$route['order-produsen'] = "PoKutusKutusController";
$route['order-produsen/list'] = "PoKutusKutusController/list";
$route['order-produsen/add'] = "PoKutusKutusController/add";
$route['order-produsen/utility/:any'] = "PoKutusKutusController/utility";
$route['order-produsen/save-add'] = "PoKutusKutusController/save_add";
$route['order-produsen/save-start'] = "PoKutusKutusController/save_start";
$route['order-produsen/edit/:any'] = "PoKutusKutusController/edit";
$route['order-produsen/detail'] = "PoKutusKutusController/detail";
$route['order-produsen/save-edit'] = "PoKutusKutusController/save_edit";
$route['order-produsen/delete'] = "PoKutusKutusController/delete";
$route['order-produsen/selesai-order-produsen'] = "PoKutusKutusController/selesai_order_kutus_kutus";
$route['order-produsen/penerimaan-order-produsen'] = "PoKutusKutusController/penerimaan_po_kutus_kutus";
$route['order-produsen/print/:num'] = "PoKutusKutusController/print";
$route['history-order-produsen'] = "PoKutusKutusController/history";
$route['history-order-produsen/list'] = "PoKutusKutusController/history_list";

$route['order-produsen/accept-stock'] = "PoKutusKutusController/accept_stock";
$route['order-produsen/penerimaan/:any'] = "PoKutusKutusController/penerimaan_page";
$route['order-produsen/penerimaan/list/:num'] = "PoKutusKutusController/penerimaan_list";
$route['order-produsen/penerimaan/add/:num'] = "PoKutusKutusController/add_penerimaan";
$route['order-produsen/penerimaan/edit/:num'] = "PoKutusKutusController/edit_penerimaan";
$route['order-produsen/penerimaan/delete/:num'] = "PoKutusKutusController/delete_penerimaan";
$route['order-produsen/penerimaan/detail/:num'] = "PoKutusKutusController/detail_penerimaan";

$route['group'] = "GroupController";
$route['group/list'] = "GroupController/list";
$route['detail-member/:any'] = "MemberDetail";
$route['detail-member/list/:num'] = "MemberDetail/table_list";


$route['crown'] = "CrownController";
$route['crown/email-bonus'] = "CrownController/email_bonus";
$route['crown-activasi'] = "CrownActivasiController";
$route['cron/omset-hangus'] = "CrownActivasiController/omset_hangus";
$route['test-cron'] = "CrownActivasiController";
$route['test-cron2'] = "CrownActivasiController/cron2";
$route['warning-deposit'] = "CrownActivasiController/warningDeposit";

$route['manage-bonus'] = "ManagementBonusController";
$route['manage-bonus/list'] = "ManagementBonusController/list";
$route['manage-bonus-success'] = "ManagementBonusController/success_page";
$route['manage-bonus-success/list'] = "ManagementBonusController/success_list";
$route['manage-bonus-success/pdf'] = "ManagementBonusController/success_pdf";
$route['manage-bonus-success/excel'] = "ManagementBonusController/success_excel";
$route['manage-bonus-hold'] = "ManagementBonusController/hold";
$route['manage-bonus-hold/list'] = "ManagementBonusController/hold_list";
$route['manage-bonus-hold/pdf'] = "ManagementBonusController/hold_pdf";
$route['manage-bonus-hold/excel'] = "ManagementBonusController/hold_excel";
$route['manage-bonus/processing'] = "ManagementBonusController/processing";
$route['manage-bonus/fail'] = "ManagementBonusController/fail";
$route['manage-bonus/success'] = "ManagementBonusController/success_process";
$route['manage-bonus/success-date'] = "ManagementBonusController/success_process_date";
$route['manage-bonus/turnback'] = "ManagementBonusController/turnback";
$route['manage-bonus/print'] = "ManagementBonusController/print";
$route['manage-bonus-proses'] = "ManagementBonusController/porcess_data";
$route['manage-bonus-proses/list'] = "ManagementBonusController/porcess_list";
$route['manage-bonus/cancel'] = "ManagementBonusController/cancel";
$route['manage-bonus-terkirim'] = "ManagementBonusController/terkirim";
$route['manage-bonus-terkirim/list'] = "ManagementBonusController/terkirim_list";
$route['manage-bonus-proses/detail'] = "ManagementBonusController/proses_detail";
$route['manage-bonus-harian'] = "ManagementBonusHarianController";
$route['manage-bonus-harian/list'] = "ManagementBonusHarianController/list";


$route['pengiriman-withdraw-cash'] = "PaymentWithdrawCashController";
$route['pengiriman-withdraw-cash/list'] = "PaymentWithdrawCashController/list";
$route['pengiriman-withdraw-cash-success'] = "PaymentWithdrawCashController/success_page";
$route['pengiriman-withdraw-cash-success/list'] = "PaymentWithdrawCashController/success_list";
$route['pengiriman-withdraw-cash-hold'] = "PaymentWithdrawCashController/hold";
$route['pengiriman-withdraw-cash-hold/list'] = "PaymentWithdrawCashController/hold_list";
$route['pengiriman-withdraw-cash/processing'] = "PaymentWithdrawCashController/processing";
$route['pengiriman-withdraw-cash/fail'] = "PaymentWithdrawCashController/fail";
$route['pengiriman-withdraw-cash/success'] = "PaymentWithdrawCashController/success_process";
$route['pengiriman-withdraw-cash/turnback'] = "PaymentWithdrawCashController/turnback";
$route['pengiriman-withdraw-cash/print'] = "PaymentWithdrawCashController/print";

$route['money-back'] = "MoneyBackController";
$route['money-back/submit'] = "MoneyBackController/submit";
$route['money-back/batal'] = "MoneyBackController/batal";
$route['money-back/print'] = "MoneyBackController/print";

$route['manage-money-back'] = "ManageMoneyBackController";
$route['manage-money-back/list'] = "ManageMoneyBackController/list";
$route['manage-money-back/approve'] = "ManageMoneyBackController/approve";
$route['manage-money-back/penerimaan'] = "ManageMoneyBackController/penerimaan";
$route['manage-money-back/refuse']= "ManageMoneyBackController/refuse";
$route['manage-money-back/approve-page']= "ManageMoneyBackController/approve_page";
$route['manage-money-back/approve-list']= "ManageMoneyBackController/approve_list";

$route['retur'] = 'ReturAgentController';
$route['retur/list'] = 'ReturAgentController/list';
$route['retur/add'] = 'ReturAgentController/add';
$route['retur/save-add'] = 'ReturAgentController/save_add';
$route['retur/edit/:any'] = 'ReturAgentController/edit';
$route['retur/delete'] = 'ReturAgentController/delete';
$route['retur/detail'] = 'ReturAgentController/detail';
$route['retur/save-edit'] = 'ReturAgentController/save_edit';
$route['retur/submit'] = 'ReturAgentController/submit';
$route['retur/list-produk'] = 'ReturAgentController/list_produk';

$route['manage-retur'] = 'ManageReturController';
$route['manage-retur/list'] = 'ManageReturController/list';
$route['manage-retur/approve'] = 'ManageReturController/approve';
$route['manage-retur/refuse'] = 'ManageReturController/refuse';
$route['manage-retur/approve-page'] = 'ManageReturController/approve_page';
$route['manage-retur/refuse-page'] = 'ManageReturController/refuse_page';
$route['manage-retur/approve-list'] = 'ManageReturController/approve_list';
$route['manage-retur/refuse-list'] = 'ManageReturController/refuse_list';

$route['penukaran-retur-agen'] = 'PenukaranReturAgen';
$route['penukaran-retur-agen/check'] = "PenukaranReturAgen/check";
$route['penukaran-retur-agen-save'] = "PenukaranReturAgen/penukaran_save";

$route['piutang-retur'] = "PiutangReturController";
$route['piutang-retur/list'] = "PiutangReturController/list";
$route['hutang-retur'] = "HutangReturController";
$route['hutang-retur/list'] = "HutangReturController/list";

$route['pembayaran-hutang-retur'] = "PembayaranHutangReturController";
$route['pembayaran-hutang-retur/list'] = "PembayaranHutangReturController/list";
$route['pembayaran-hutang-retur/add'] = "PembayaranHutangReturController/add";
$route['pembayaran-hutang-retur/utility/:any'] = "PembayaranHutangReturController/utility";
$route['pembayaran-hutang-retur/save-add'] = "PembayaranHutangReturController/save_add";
$route['pembayaran-hutang-retur/save-start'] = "PembayaranHutangReturController/save_start";
$route['pembayaran-hutang-retur/edit/:any'] = "PembayaranHutangReturController/edit";
$route['pembayaran-hutang-retur/detail'] = "PembayaranHutangReturController/detail";
$route['pembayaran-hutang-retur/save-edit'] = "PembayaranHutangReturController/save_edit";
$route['pembayaran-hutang-retur/delete'] = "PembayaranHutangReturController/delete";
$route['pembayaran-hutang-retur/approve'] = "PembayaranHutangReturController/approve";
$route['pembayaran-hutang-retur/refuse'] = "PembayaranHutangReturController/refuse";
$route['pembayaran-hutang-retur/selesai-pembayaran-hutang-retur'] = "PembayaranHutangReturController/selesai_pembayaran-hutang-retur";
$route['pembayaran-hutang-retur/penerimaan-pembayaran-hutang-retur'] = "PembayaranHutangReturController/penerimaan_po_produk";
$route['pembayaran-hutang-retur/print/:num'] = "PembayaranHutangReturController/print";
$route['pembayaran-hutang-retur/approve-page'] = "PembayaranHutangReturController/approve_page";
$route['pembayaran-hutang-retur/approve-list'] = "PembayaranHutangReturController/approve_list";

$route['pembayaran-hutang-retur/selesai-pembayaran-hutang-retur'] = "PembayaranHutangReturController/selesai_pembayaran-hutang-retur";
$route['pembayaran-hutang-retur/penerimaan-pembayaran-hutang-retur'] = "PembayaranHutangReturController/penerimaan_po_produk";
$route['pembayaran-hutang-retur/print/:num'] = "PembayaranHutangReturController/print";

$route['request-pembayaran-piutang'] = "RequestPembayaranPiutangController";
$route['request-pembayaran-piutang/list'] = "RequestPembayaranPiutangController/list";
$route['request-pembayaran-piutang/add'] = "RequestPembayaranPiutangController/add";
$route['request-pembayaran-piutang/list-hutang'] = "RequestPembayaranPiutangController/list_hutang";
$route['request-pembayaran-piutang/utility/:any'] = "RequestPembayaranPiutangController/utility";
$route['request-pembayaran-piutang/save-add'] = "RequestPembayaranPiutangController/save_add";
$route['request-pembayaran-piutang/save-start'] = "RequestPembayaranPiutangController/save_start";
$route['request-pembayaran-piutang/edit/:any'] = "RequestPembayaranPiutangController/edit";
$route['request-pembayaran-piutang/detail'] = "RequestPembayaranPiutangController/detail";
$route['request-pembayaran-piutang/save-edit'] = "RequestPembayaranPiutangController/save_edit";
$route['request-pembayaran-piutang/delete'] = "RequestPembayaranPiutangController/delete";
$route['request-pembayaran-piutang/submit'] = "RequestPembayaranPiutangController/submit";
$route['request-pembayaran-piutang/print/:num'] = "RequestPembayaranPiutangController/print";

$route['withdraw-cash'] = "WithdrawCashController";
$route['withdraw-cash/list'] = "WithdrawCashController/list";
$route['withdraw-cash/add'] = "WithdrawCashController/add";
$route['withdraw-cash/list-hutang'] = "WithdrawCashController/list_hutang";
$route['withdraw-cash/utility/:any'] = "WithdrawCashController/utility";
$route['withdraw-cash/save-add'] = "WithdrawCashController/save_add";
$route['withdraw-cash/save-start'] = "WithdrawCashController/save_start";
$route['withdraw-cash/edit/:any'] = "WithdrawCashController/edit";
$route['withdraw-cash/detail'] = "WithdrawCashController/detail";
$route['withdraw-cash/save-edit'] = "WithdrawCashController/save_edit";
$route['withdraw-cash/delete'] = "WithdrawCashController/delete";
$route['withdraw-cash/submit'] = "WithdrawCashController/submit";
$route['withdraw-cash/print/:num'] = "WithdrawCashController/print";

$route['request-pembayaran-piutang-retur'] = "RequestPembayaranPiutangReturController";
$route['request-pembayaran-piutang-retur/list'] = "RequestPembayaranPiutangReturController/list";
$route['request-pembayaran-piutang-retur/add'] = "RequestPembayaranPiutangReturController/add";
$route['request-pembayaran-piutang-retur/list-hutang'] = "RequestPembayaranPiutangReturController/list_hutang";
$route['request-pembayaran-piutang-retur/utility/:any'] = "RequestPembayaranPiutangReturController/utility";
$route['request-pembayaran-piutang-retur/save-add'] = "RequestPembayaranPiutangReturController/save_add";
$route['request-pembayaran-piutang-retur/save-start'] = "RequestPembayaranPiutangReturController/save_start";
$route['request-pembayaran-piutang-retur/edit/:any'] = "RequestPembayaranPiutangReturController/edit";
$route['request-pembayaran-piutang-retur/detail'] = "RequestPembayaranPiutangReturController/detail";
$route['request-pembayaran-piutang-retur/save-edit'] = "RequestPembayaranPiutangReturController/save_edit";
$route['request-pembayaran-piutang-retur/delete'] = "RequestPembayaranPiutangReturController/delete";
$route['request-pembayaran-piutang-retur/submit'] = "RequestPembayaranPiutangReturController/submit";
$route['request-pembayaran-piutang-retur/print/:num'] = "RequestPembayaranPiutangReturController/print";

$route['npwp-reseller'] = 'NpwpResellerController';
$route['npwp-reseller/list'] = 'NpwpResellerController/list';
$route['npwp-reseller/approved'] = 'NpwpResellerController/approved';
$route['npwp-reseller/refuse'] = 'NpwpResellerController/refuse';

$route['npwp-reseller-approve'] = 'NpwpResellerController/approved_page';
$route['npwp-reseller-approve/list'] = 'NpwpResellerController/approved_list';
$route['npwp-reseller-approve/add'] = 'NpwpResellerController/approved_add';
$route['npwp-reseller-approve/edit'] = 'NpwpResellerController/approved_edit';
$route['npwp-reseller-approve/delete'] = 'NpwpResellerController/approved_delete';
$route['npwp-reseller-approve/list-reseller'] = 'NpwpResellerController/list_reseller';
$route['npwp-home-save'] = 'NpwpResellerController/save_home';

$route['reward'] = "RewardController";
$route['reward/list'] = "RewardController/list";
$route['reward/claim'] = "RewardController/claim";
$route['claimed-reward'] = "ClaimedRewardController";
$route['claimed-reward/list'] = "ClaimedRewardController/list";


$route['reward-reseller'] = "RewardResellerController/index";
$route['reward-reseller/list-tambahan'] = "RewardResellerController/list_tambahan";
$route['reward-reseller/claim-tambahan'] = "RewardResellerController/claim_tambahan";
$route['reward-reseller/claim'] = "RewardResellerController/claim";
$route['reward-reseller/list'] = "RewardResellerController/list";
$route['reward-reseller/claim-data'] = "RewardResellerController/claim_data";

$route['management-reward-reseller'] = "ManagementRewardResellerController/index";
$route['management-reward-reseller/list'] = "ManagementRewardResellerController/list";
$route['management-reward-reseller/approve'] = "ManagementRewardResellerController/approve";
$route['management-reward-reseller/refuse'] = "ManagementRewardResellerController/refuse";




$route['website'] = "WebsiteController";

$route['404_override'] = 'not_found';

$route['user-guide'] = 'UserGuideController';
$route['pengiriman-hadiah-event'] = 'PengirimanHadiahEventController';
$route['pengiriman-hadiah-event/list'] = "PengirimanHadiahEventController/list";
$route['pengiriman-hadiah-event-success'] = "PengirimanHadiahEventController/success_page";
$route['pengiriman-hadiah-event-success/list'] = "PengirimanHadiahEventController/success_list";
$route['pengiriman-hadiah-event-hold'] = "PengirimanHadiahEventController/hold";
$route['pengiriman-hadiah-event-hold/list'] = "PengirimanHadiahEventController/hold_list";
$route['pengiriman-hadiah-event/processing'] = "PengirimanHadiahEventController/processing";
$route['pengiriman-hadiah-event/fail'] = "PengirimanHadiahEventController/fail";
$route['pengiriman-hadiah-event/success'] = "PengirimanHadiahEventController/success_process";
$route['pengiriman-hadiah-event/turnback'] = "PengirimanHadiahEventController/turnback";
$route['pengiriman-hadiah-event/print'] = "PengirimanHadiahEventController/print";
$route['pengiriman-hadiah-event-proses'] = "PengirimanHadiahEventController/porcess_data";
$route['pengiriman-hadiah-event-proses/list'] = "PengirimanHadiahEventController/porcess_list";
$route['pengiriman-hadiah-event/cancel'] = "PengirimanHadiahEventController/cancel";
$route['pengiriman-hadiah-event-terkirim'] = "PengirimanHadiahEventController/terkirim";
$route['pengiriman-hadiah-event-terkirim/list'] = "PengirimanHadiahEventController/terkirim_list";
$route['pengiriman-hadiah-event-proses/detail'] = "PengirimanHadiahEventController/proses_detail";


$route['pengiriman-reward-reseller'] = 'PengirimanRewardResellerController';
$route['pengiriman-reward-reseller/list'] = 'PengirimanRewardResellerController/list';
$route['pengiriman-reward-reseller/processing'] = 'PengirimanRewardResellerController/processing';
$route['pengiriman-reward-reseller/print'] = 'PengirimanRewardResellerController/print';
$route['pengiriman-reward-reseller-proses'] = "PengirimanRewardResellerController/porcess_data";
$route['pengiriman-reward-reseller-proses/list'] = "PengirimanRewardResellerController/porcess_list";
$route['pengiriman-reward-reseller/success'] = "PengirimanRewardResellerController/success_process_date";
$route['pengiriman-reward-reseller/cancel'] = "PengirimanRewardResellerController/cancel";
$route['pengiriman-reward-reseller-terkirim'] = "PengirimanRewardResellerController/terkirim";
$route['pengiriman-reward-reseller-terkirim/list'] = "PengirimanRewardResellerController/terkirim_list";


$route['hadiah-event'] = 'HadiahEventController';
$route['hadiah-event/list'] = "HadiahEventController/list";

$route['event'] = 'EventController';
$route['event/list'] = "EventController/list";

$route['management-reward'] = "ManagementRewardController";
$route['management-reward/list'] = "ManagementRewardController/list";
$route['management-reward/approve'] = "ManagementRewardController/approve";
$route['management-reward/refuse'] = "ManagementRewardController/refuse";
$route['management-reward/approve-page'] = "ManagementRewardController/approve_page";
$route['management-reward/approve-list'] = "ManagementRewardController/approve_list";
$route['management-reward/refuse-page'] = "ManagementRewardController/refuse_page";
$route['management-reward/refuse-list'] = "ManagementRewardController/refuse_list";

$route['notification'] = "NotifController";
$route['notification/list'] = "NotifController/list";
$route['notification/add'] = "NotifController/add";
$route['notification/save-add'] = "NotifController/save_add";
$route['notification/edit/:any'] = "NotifController/edit";
$route['notification/detail'] = "NotifController/detail";
$route['notification/detail-notification/:any'] = "NotifController/detail_notif";
$route['notification/save-edit'] = "NotifController/save_edit";
$route['notification/delete'] = "NotifController/delete";


$route['upgrade-super-reseller/check_user'] = "UpgradeSuperResellerController/check_user";
$route['upgrade-super-reseller/upgrade/:any'] = "UpgradeSuperResellerController/upgrade_user";
$route['upgrade-super-reseller/:any'] = "UpgradeSuperResellerController/index";

$route['meeting'] = "MeetingController";
$route['meeting/list'] = "MeetingController/list";
$route['meeting/detail'] = "MeetingController/detail";

$route['meeting/kehadiran/add'] = "MeetingController/save_kehadiran_add";

$route['manage-voucher'] = "ManageVoucherController";
$route['manage-voucher/list'] = "ManageVoucherController/list";
$route['manage-voucher-terpakai'] = "ManageVoucherController/terpakai_page";
$route['manage-voucher-terpakai/list'] = "ManageVoucherController/terpakai_list";

$route['home/performa_group'] = 'Home/performa_group';
$route['home/performa_super'] = 'Home/performa_super';
$route['home/po_perprovinsi'] = 'Home/po_perprovinsi';

$route['hasil-survey'] = "HasilSurveyController";
$route['hasil-survey/list'] = "HasilSurveyController/list";

$route['kontes'] = 'TestController';
$route['kontes/supres'] = 'TestController/supres';
$route['date-test'] = 'TestController/date_test';
$route['test-function'] = 'TestController/test_function';
$route['test-tanggal-withdraw'] = 'TestController/test_tanggal_withdraw';



// cronjob
$route['omset-hangus'] = 'OmsetController/updateStatusOmset';
$route['cron-super-reseller'] = 'CronSuperReseller';
$route['cron-super-reseller/bintang-atas'] = 'CronSuperReseller/bintang_atas';


$route['backup'] = 'BackupController';
$route['backup-side'] = 'BackupController/side';

$route['test-email'] = 'TestController/email';
$route['reward-thailand'] = 'RewardThailandController';
$route['spesial-thailand'] = 'RewardThailandController/special_ticket';
$route['reward-thailand/excel'] = 'RewardThailandController/excel';

$route['simpan-survey'] = "SurveyController/simpan_survey";

