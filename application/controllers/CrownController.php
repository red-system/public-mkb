<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class CrownController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('midtrans_response','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('bonus','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('reseller','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('response_payment','',true);
        $this->load->model('cron_history','',true);
        $this->load->library('main');
        $this->load->helper('string');
    }
    public function index()
    {
        $date = date("Y-m-d");
        $this->db->where('first_limit_date <= ',$date);
        $this->db->where('is_red',0);
        $reseller = array();
        $reseller['is_pass_first_limit'] = 1;
        $this->db->update('mykindofbeauty_kemiri.reseller',$reseller);
        $this->db->where('next_limit_date <= ',$date);
        $this->db->where('is_pass_first_limit',1);
        $next =  $this->db->get('mykindofbeauty_kemiri.reseller')->result();
        foreach ($next as $item){
            $count = $item->skip_deposit_count + 1;
            $next_date = new DateTime(date("Y-m-d",strtotime($item->next_limit_date)));
            $next_date->modify('+1 month');
            $next_date_label = $next_date->format("Y-m-d");
            $next_data = array();
            $next_data['next_limit_date'] = $next_date_label;
            $next_data['skip_deposit_count'] = $count;
            $this->db->where('reseller_id',$item->reseller_id);
            $this->db->update('mykindofbeauty_kemiri.reseller',$next_data);
        }
        $this->db->where('skip_deposit_count >=',1);
        $reseller['is_frezze'] = 1;
        $this->db->update('mykindofbeauty_kemiri.reseller',$reseller);
    }
    function email_bonus(){
        $bonus_hari_ini = $this->bonus->bonus_hari_ini();
        $html = '<html><head>
                        <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
                        <style>
                            .content {
                                max-width: 800px;
                                margin: auto;
                            }
                            .title{
                                width: 60%;
                            }
                            .data,.data th,.data td {
                              border: 1px solid black;
                            }

                        </style></head>

                        <body>
                            <div class="content">
            <div bgcolor="#0aa89e">
                <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

                    <tbody><tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#bfbfbf" style="padding:10px 15px;font-size:14px">
                                <tbody><tr>
                                    <td width="60%" align="left" style="padding:5px 0 0">
                                        <img src="http://app.support88.id/assets/media/logo2.png" width="100px">
                                    </td>
                                    <td width="40%" align="right" style="padding:5px 0 0">
                                        <span style="font-size:18px;font-weight:300;color:#ffffff">
                                            Daftar Bonus Hari Ini
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                    <tr>
                        <table class="data" align="center" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
                        <thead>
                        <tr role="row">
                            <th>No</th>
                            <th >Tanggal</th>
                            <th >Penerima Bonus</th>
                            <th >Total</th>
                            <th >Persentase</th>
                            <th >Total Bonus</th>

                        </thead>
                        <tbody id="child_data_ajax">
                        ';
                        $i = 1;
                        $total = 0;
                        foreach ($bonus_hari_ini as $item){
                         $html .= ' <tr role="row" class="odd">
                        <td style="text-align: center">'.$i.'</td>
                        <td style="text-align: center">'.$item->tanggal.'</td>
                        <td style="text-align: center">'.$item->nama_penerima.'</td>
                        <td style="text-align: right">'.number_format($item->jumlah_deposit).'</td>
                        <td style="text-align: center">'.$item->persentase.'%</td>
                        <td style="text-align: right">'.number_format($item->jumlah_bonus).'</td>
                        </tr>';
                            $i++;
                            $total +=$item->jumlah_bonus;
                        }
                        $html .='<tr>
                            <td role="row" colspan="5" style="text-align: center">Total</td>
                            <td style="text-align: right">'.number_format($total).'</td>
                        </tr>
                        </tbody>
                    </table>
                    </tr>
                </tbody></table>
            <p>&nbsp;<br></p>
            </div>

                                        </div>

            </body></html>';
        $this->db->select("mykindofbeauty_kemiri.staff.*");
        $this->db->join("mykindofbeauty_kemiri.staff","mykindofbeauty_master.login.user_staff_id = mykindofbeauty_kemiri.staff.staff_id");
        $this->db->where("mykindofbeauty_master.login.company_id",3);
        $this->db->where("mykindofbeauty_master.login.user_role_id",2);
        $login = $this->db->get("mykindofbeauty_master.login")->result();

        foreach ($login as $item){
            $this->main->mailer_auth('Daftar Bonus Harian Tanggal '.date("Y-m-d"), $item->staff_email,$item->staff_nama, $html);
        }

    }
    function test(){
        $waktu = date("Y-m-d H:i:s");
        $data['waktu'] = $waktu;
        $this->db->insert('mykindofbeauty_kemiri.testCron',$data);
    }
    function omset_hangus(){
//        $reseller = $this->reseller->super_reseller_active();
//        $reseller = $this->reseller->list_reseller_hangus();
//        foreach ($reseller as $key) {
//            $id_reseller = $key->reseller_id;
//            $next_deposit_date = $key->next_deposit_date;
//            $before_deposit_date = date('Y-m-d', strtotime($next_deposit_date. ' -2 months'));
//            $today = date('Y-m-d');
//            if ($today > $next_deposit_date) {
//                $get_last_deposit_po = $this->po_produk->get_last_deposit_po($id_reseller);
//                if ($get_last_deposit_po->tanggal_penerimaan > $next_deposit_date) {
//                    $data['tidak_diakui'] = 1;
//                    $update = $this->po_produk->update_omset_hangus($id_reseller, $before_deposit_date, $next_deposit_date, $data);
//                }
//            }
//        }
//        $cron['cron_des'] = 'omset_hangus';
//        $cron['time'] = date("Y-m-d H:i:s");
//        $this->cron_history->insert($cron);

    }
}

/* End of file ProdukByLocation.php */
/* Location: ./application/controllers/ProdukByLocation.php */