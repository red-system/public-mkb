<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebsiteController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        array_push($this->css, "vendors/general/sweetalert2/dist/sweetalert2.css");
        array_push($this->js, "vendors/general/clipboard2/clipboard.min.js");
        array_push($this->js, "script/admin/website.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $data['web_referensi'] = 'https://mykindofbeauty.co.id/gabung/'.$reseller->referal_code;
        $data['web_retail'] = 'https://mykindofbeauty.co.id/reseller/'.$reseller->referal_code;
        $data["meta_title"] = "Website < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "marketing-tools";
        $data['page'] = $this->uri->segment(1);
        $data['reseller'] = $reseller;
        $action = array();

        $data['action'] = json_encode($action);

        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/website/index');
        $this->load->view('admin/static/footer');
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
