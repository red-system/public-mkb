<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PoBahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('po_bahan','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('suplier','',true);
        $this->load->model('bahan','',true);
		$this->load->model('tipe_pembayaran','',true);
		$this->load->model('penerimaan_bahan','',true);
		$this->load->model('piutang_bahan_diterima','',true);
		$this->load->model('history_hpp_bahan','',true);
	}
	
	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/po_bahan.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Order Bahan < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "order_bahan";
		$data['page'] = "order_bahan";
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"po_bahan_no"));
		array_push($column, array("data"=>"tanggal_pemesanan"));
		array_push($column, array("data"=>"suplier_nama"));
		array_push($column, array("data"=>"grand_total"));
		array_push($column, array("data"=>"jenis_pembayaran"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['order_bahan'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/po_bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_bahan->po_bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->po_bahan->po_bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_bahan->po_bahan_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
			$key->grand_total = number_format($key->grand_total);
			$key->penerimaan_status_btn = true;
			$key->edit_url = base_url().'po-bahan/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_bahan_id));
			$key->no = $i;
			if($key->jenis_pembayaran == "kas"){
				$key->tipe_pembayaran_nama = $key->kas_nama." ".$key->no_akun;
			}
			$i++;
			$key->delete_url = base_url().'po-bahan/delete/';
			$key->enc_row_id = str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_bahan_id));
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
		array_push($this->js, "script/admin/po_bahan.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Order Bahan < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "order_bahan";
		$data['page'] = $this->uri->segment(1);	
		$data['lokasi'] = $this->lokasi->all_list();
		$data['suplier'] = $this->suplier->all_list();	
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
		$data['po_bahan_no'] = 	$this->po_bahan->get_kode_po_bahan();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/add_po_bahan');
		$this->load->view('admin/static/footer');
		unset($_SESSION['po_bahan']);
		$_SESSION['po_bahan']['lokasi'] = 1;
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key == "get-po-no"){
			$code = $this->input->post('suplier_kode');
			echo $this->po_bahan->get_kode_po_bahan($code);
		}
		if($key=="list-bahan"){
			$this->list_bahan();
		}
		if($key=="sess-bahan-add"){
			$no = $this->input->post('key');
			$bahan_id = $this->input->post('bahan_id');
			$_SESSION['po_bahan']['bahan']['con_'.$no]['bahan_id'] = $bahan_id;
			$_SESSION['po_bahan']['bahan']['con_'.$no]['value'] = 0;
		}
		if($key=="sess-bahan-change"){		
			$jumlah = $this->input->post('jumlah');
			$no = $this->input->post('key');
			if(isset($_SESSION['po_bahan']['bahan']['con_'.$no]['bahan_id'])){
				$_SESSION['po_bahan']['bahan']['con_'.$no]['value'] = $jumlah;
			}
		}
		if($key=="sess-bahan-delete"){
			$no = $this->input->post('key');
			unset($_SESSION['po_bahan']['bahan']['con_'.$no]);
		}
		if($key=="sess-bahan-reset"){
			unset($_SESSION['po_bahan']['bahan']);
		}
		if($key=="sess-lokasi"){
			$_SESSION['po_bahan']['lokasi'] = $this->input->post('lokasi_id');
		}
		if($key == "get-bahan-detail"){
			$code = $this->input->post('po_bahan_detail_id');
			echo json_encode($this->penerimaan_bahan->get_po_bahan_detail($code));
		}			
	}
	function list_bahan(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bahan->bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->bahan->bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bahan->bahan_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'bahan/delete/';
			$key->action =null;
			$key->row_id = $key->bahan_id;
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save_add(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));		
		$insert = $this->po_bahan->insert_po_bahan();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);
	}
	function edit(){
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$po_bahan = $this->po_bahan->po_bahan_by_id($id);
		if ($po_bahan != null) {
			$data['po_bahan'] = $po_bahan;
			$data['po_bahan_detail']=$this->po_bahan->po_bahan_detail_by_id($id);
			array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
			array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
			array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
			array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
			array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
			array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
			array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
			array_push($this->js, "script/admin/po_bahan.js");
			$data['lokasi'] = $this->lokasi->all_list();
			$data['suplier'] = $this->suplier->all_list();
			$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();	
			$data["css"] = $this->css;
			$data["js"] = $this->js;
			$column = array();
			$data["meta_title"] = "Produksi < ".$_SESSION["redpos_company"]['company_name'];;
			$data['parrent'] = "master_data";
			$data['page'] = $this->uri->segment(1);		
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/edit_po_bahan');
			$this->load->view('admin/static/footer');			
		}else {

			redirect('404_override','refresh');
		}
	}
	function save_edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));		
		$insert = $this->po_bahan->edit_po_bahan();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);		
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->po_bahan->delete_by_id("po_bahan_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		$id = $this->input->post('po_bahan_id');
		$temp = $this->po_bahan->po_bahan_by_id($id);
		$no = 0;
		$no_bahan = 0;
		$post = array();
		$temp->item = $this->po_bahan->po_bahan_detail_by_id($id);
		$temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
		if($temp->tanggal_pemesanan != null){
			$x = strtotime($temp->tanggal_pemesanan);
			$temp->tanggal_pemesanan = date("Y-m-d",$x);
		}
		if($temp->tanggal_penerimaan != null){
			$x = strtotime($temp->tanggal_penerimaan);
			$temp->tanggal_penerimaan = date("Y-m-d",$x);
		}
		if($temp->created_at != null){
			$time = strtotime($temp->created_at);
			$temp->created_at = date('d-m-Y H:i:s',$time);
		}
		if($temp->updated_at != null){
			$time = strtotime($temp->updated_at);
			$temp->updated_at = date('d-m-Y H:i:s',$time);
		}
		foreach ($temp->item as $key) {
			$key->harga = number_format($key->harga);
			$key->jumlah = number_format($key->jumlah);
			$key->sub_total = number_format($key->sub_total);
		}
			$temp->total = number_format($temp->total);
			$temp->tambahan = number_format($temp->tambahan);
			$temp->potongan = number_format($temp->potongan);
			$temp->grand_total = number_format($temp->grand_total);		
		echo json_encode($temp);
	}
	
	function history(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/po_bahan.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "History Order Bahan < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "order_bahan";
		$data['page'] = "order_bahan";
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"po_bahan_no"));
		array_push($column, array("data"=>"tanggal_pemesanan"));
		array_push($column, array("data"=>"suplier_nama"));
		array_push($column, array("data"=>"grand_total"));
		array_push($column, array("data"=>"jenis_pembayaran"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
		$data['lokasi'] = $this->lokasi->all_list();
		$action = array("view"=>true);
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/po_bahan');
		$this->load->view('admin/static/footer');		
	}
	function history_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_bahan->po_bahan_count_all_history();
		$result['iTotalDisplayRecords'] = $this->po_bahan->po_bahan_count_filter_history($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_bahan->po_bahan_list_history($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
			$key->grand_total = number_format($key->grand_total);
			
			$key->edit_url = base_url().'po-bahan/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_bahan_id));
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'po-bahan/delete/';
			$key->row_id = $key->po_bahan_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
    function print(){
        $id = $this->uri->segment(3);
        $temp = $this->po_bahan->po_bahan_by_id($id);
        $no = 0;
        $no_bahan = 0;
        $post = array();
        $temp->item = $this->po_bahan->po_bahan_detail_by_id($id);
        $temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga);
            $key->jumlah = number_format($key->jumlah);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        $data['po_produk'] = $temp;
        $this->load->view('admin/po-bahan/print',$data);
    }

    function penerimaan_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/penerimaan_bahan.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Order Bahan < Penerimaan < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_bahan";
        $data['page'] = 'order_bahan';
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $penerimaan = $this->penerimaan_bahan->penerimaan_by_id($id);
        $data['id'] = $id;
        if ($id != null) {
            $data['penerimaan'] = $penerimaan;
            array_push($column, array("data"=>"no"));
            array_push($column, array("data"=>"tanggal_penerimaan"));
            array_push($column, array("data"=>"bahan_nama"));
            array_push($column, array("data"=>"total_qty_penerimaan"));
            array_push($column, array("data"=>"status_kirim_lbl"));
            array_push($column, array("data"=>"keterangan_penerimaan"));
            $data['sumColumn'] = json_encode(array(3));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className"=>"text__center","targets"=>array(0,3)));
            $data["action"] = json_encode(array("view"=>false, "edit"=>true,"delete"=>true, "submit" => true), true);
            
            $data['lokasi'] = $this->lokasi->all_gudang();
            $data['bahan'] = $this->penerimaan_bahan->get_bahan_detail($id);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/po-bahan/penerimaan');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function penerimaan_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->penerimaan_bahan->penerimaan_bahan_count($this->uri->segment(4));
        $result['iTotalDisplayRecords'] = $this->penerimaan_bahan->penerimaan_bahan_filter($this->uri->segment(4),$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->penerimaan_bahan->penerimaan_bahan_list($start,$length,$query,$this->uri->segment(4));
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan_label = date('d-m-Y',$time);
            }
            $key->no = $i;
            $key->delete_url = base_url().'po-bahan/penerimaan/delete/'.$key->penerimaan_bahan_id;
            // $key->submit_url = base_url().'order-super-agen/penerimaan/accept-stock/'.$key->penerimaan_id;
            $key->row_id = $key->penerimaan_bahan_id;
            $key->total_qty_penerimaan = number_format($key->total_qty_penerimaan);
            $key->status_kirim_lbl = 'Belum submit';
            if ($key->status_penerimaan == 1) {
                $key->deny_edit = 'true';
                $key->deny_delete = 'true';
                $key->deny_submit = 'true';
                $key->status_kirim_lbl = 'Submit';
            }
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail_penerimaan(){
        $po_bahan_id = $this->uri->segment(4);
        $data["result"] = $this->penerimaan_bahan->penerimaan_by_id($po_bahan_id);
        echo json_encode($data);
    }

    function add_penerimaan(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['po_bahan_id'] = $this->input->post('po_bahan_id');
        $data['po_bahan_detail_id'] = $this->input->post('po_bahan_detail_id');
        $data['lokasi_penerimaan_id'] = $this->input->post('lokasi_penerimaan_id');
        $data['keterangan_penerimaan'] = $this->input->post('keterangan');
        $data['total_qty_penerimaan'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal_penerimaan'] = date("Y-m-d",$temp);
        $data['status_penerimaan'] = 0;
        $sisa = $this->string_to_number($this->input->post('sisa'));

        if($sisa >= $data['total_qty_penerimaan']){
            $insert = $this->penerimaan_bahan->insert($data);

            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        } else {
            $result['message'] = "Penerimaan Produk melebihi sisa order";
        }
        echo json_encode($result);
    }


    function edit_penerimaan(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['keterangan_penerimaan'] = $this->input->post('keterangan_penerimaan');
        $po_bahan_id = $this->input->post('po_bahan_id');
		$data['po_bahan_detail_id'] = $this->input->post('po_bahan_detail_id');
        $data['lokasi_penerimaan_id'] = $this->input->post('lokasi_penerimaan_id');
        $data['total_qty_penerimaan'] = $this->string_to_number($this->input->post('total_qty_penerimaan'));
        $temp = strtotime($this->input->post('tanggal_penerimaan_label'));
        $data['tanggal_penerimaan'] = date("Y-m-d",$temp);
        $penerimaan_bahan_id = $this->input->post('penerimaan_bahan_id');
        $sisa = $this->string_to_number($this->input->post('sisa'));
        // $terkirim = $this->string_to_number($this->input->post('terkirim'));
        // $old_jumlah = $this->string_to_number($this->input->post('old_jumlah'));
        // $new_terkirim = $terkirim - $old_jumlah + $data['total_qty_penerimaan'];

        if($data['total_qty_penerimaan'] <= $sisa){
            $edit = $this->penerimaan_bahan->update_by_id('penerimaan_bahan_id',$penerimaan_bahan_id,$data);

            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        } else {
            $result['message'] = "Penerimaan Produk melebihi sisa order";
        }

        echo json_encode($result);
    }

    function delete_penerimaan(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->penerimaan_bahan->delete_penerimaan($id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

    function accept_stock(){

        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";

        $penerimaan_bahan_id = $this->input->post("id");

        $this->po_bahan->start_trans();

		// get data penerimaan
        $data_penerimaan = $this->penerimaan_bahan->detail_penerimaan($penerimaan_bahan_id);
        $po_bahan_id = $data_penerimaan->po_bahan_id;
        $po_bahan_detail_id = $data_penerimaan->po_bahan_detail_id;
        $lokasi_id = $data_penerimaan->lokasi_penerimaan_id;
        $total_qty_penerimaan = $data_penerimaan->total_qty_penerimaan;
        $sisa = $data_penerimaan->sisa;
		$hasiL_sisa = $sisa - $total_qty_penerimaan;

        // update penerimaan status
        $penerimaan['status_penerimaan'] = 1;

		$update_sisa_po_bahan = $this->db->query("UPDATE mykindofbeauty_kemiri.po_bahan_detail SET sisa = $hasiL_sisa  WHERE po_bahan_detail_id=$po_bahan_detail_id ");

		// insert ke piutang bahan diterima
		$piutang_bahan = array();
		$piutang_bahan["penerimaan_bahan_id"] = $penerimaan_bahan_id;
		$piutang_bahan["bahan_id"] = $data_penerimaan->bahan_id;
		$piutang_bahan["tanggal"] = date("Y-m-d");
		$piutang_bahan["stock_in"] = $total_qty_penerimaan;
		$piutang_bahan['sisa_piutang'] = $hasiL_sisa;
		$piutang_bahan_diterima = $this->piutang_bahan_diterima->insert($piutang_bahan);

        $cek_sisa = $this->penerimaan_bahan->count_total_sisa($po_bahan_id);
        if ($cek_sisa->sisa > 0) {
            $data_po["status_penerimaan"] = "Sebagian";
        }else{
            $data_po["status_penerimaan"] = "Diterima";
        }

        $data_po["tanggal_penerimaan"] = date("Y-m-d");
        $data_po['lokasi_penerimaan_id'] = $lokasi_id;

		$update_penerimaan = $this->penerimaan_bahan->update_by_id('penerimaan_bahan_id',$penerimaan_bahan_id,$penerimaan);
		
		$update_po_bahan = $this->po_bahan->update_by_id('po_bahan_id',$po_bahan_id,$data_po);

		if($update_po_bahan){
			// $po_bahan_item = $this->po_bahan->po_bahan_detail_by_id($po_bahan_id);
			// foreach ($po_bahan_item as $key) {
				// $bahan_lokasi = $this->stock_bahan->bahan_by_lokasi($lokasi_id,$data_penerimaan->bahan_id);
                    $data = array();
                    $data["stock_bahan_qty"] = $total_qty_penerimaan;
                    $data["bahan_id"] = $data_penerimaan->bahan_id;
                    $data["hpp"] = $data_penerimaan->harga;
                    $data["stock_bahan_lokasi_id"] = $lokasi_id;
                    $insert = $this->stock_bahan->insert($data);
                    $arus["stock_bahan_id"] = $this->stock_bahan->last_id();
                    $arus["method"] = "insert";

					$arus["tanggal"] = date("Y-m-d");
					$arus["table_name"] = "stock_bahan";
					$arus["bahan_id"] = $data_penerimaan->bahan_id;
					$arus["stock_out"] = 0;
					$arus["stock_in"] = $total_qty_penerimaan;
					$data_last_stock = $this->stock_bahan->last_stock($data_penerimaan->bahan_id);
					$arus["last_stock"] = $data_last_stock->result;
					$data_total_stock = $this->stock_bahan->stock_total();
					$arus["last_stock_total"] = $data_total_stock->result;
					$arus["keterangan"] = "Order Bahan";
					$this->stock_bahan->arus_stock_bahan($arus);

					$stock_gudang = $this->history_hpp_bahan->get_total_stock_bahan_last($data_penerimaan->bahan_id);
					$total_stok_gudang = $stock_gudang->total_stock_gudang - $total_qty_penerimaan;

					$data_bahan = $this->bahan->row_by_id($data_penerimaan->bahan_id);
					$harga_hpp_asli = $data_bahan->bahan_harga;

					$hpp_stock_old = $total_stok_gudang * $harga_hpp_asli;
					$hpp_stock_new = $total_qty_penerimaan * $data_penerimaan->harga;
					$total_hpp = $hpp_stock_old + $hpp_stock_new;
					$total_stock_bahan = $total_stok_gudang + $total_qty_penerimaan;

					$new_hpp_bahan = $total_hpp / $total_stock_bahan;

					// $new_hpp_bahan = (($total_stok_gudang * $harga_hpp_asli) + ($total_qty_penerimaan * $data_penerimaan->harga)) / ($total_stok_gudang + $total_qty_penerimaan);

					// $harga_barang = $this->stock_bahan->update_hpp_bahan($data_penerimaan->bahan_id);
					// $bahan = array();
					// $hpp_bahan = $harga_barang->total_harga / $harga_barang->total_qty;
					$bahan['bahan_harga'] = $new_hpp_bahan;
					$update_bahan = $this->bahan->update_by_id('bahan_id',$data_penerimaan->bahan_id,$bahan);

					$histori_bahan = array();
					$histori_bahan["bahan_id"] = $data_penerimaan->bahan_id;
					$histori_bahan["tanggal"] = date("Y-m-d");
					$histori_bahan["bahan_qty"] = $total_qty_penerimaan;
					$histori_bahan['hpp'] = $new_hpp_bahan;
					$history_hpp_bahan = $this->history_hpp_bahan->insert($histori_bahan);
			// }
		}
		if($this->po_bahan->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
			// $result['total_qty_penerimaan'] = $total_qty_penerimaan;
			// $result['total_stok_gudang'] = $total_stok_gudang;
			// $result['harga_hpp_asli'] = $data_bahan->bahan_harga;
			// $result['harga_beli'] = $data_penerimaan->harga;
			// $result['hpp_stock_old'] = $hpp_stock_old;
			// $result['hpp_stock_new'] = $hpp_stock_new;
			// $result['total_hpp'] = $total_hpp;
			// $result['total_stock_bahan'] = $total_stock_bahan;
			// $result['new_hpp_bahan'] = $new_hpp_bahan;
		}

        echo json_encode($result);
    }

	function penerimaan_po_bahan(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$po_bahan_id = $this->input->post('po_bahan_id');
		$data["status_penerimaan"] = "Diterima";
		$data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
		$lokasi_id = $this->input->post('lokasi_penerimaan_id');
		$data['lokasi_penerimaan_id'] = $lokasi_id;
		$this->po_bahan->start_trans();
		$update_po_bahan = $this->po_bahan->update_by_id('po_bahan_id',$po_bahan_id,$data);
		if($update_po_bahan){
			$po_bahan_item = $this->po_bahan->po_bahan_detail_by_id($po_bahan_id);
			foreach ($po_bahan_item as $key) {
				$bahan_lokasi = $this->stock_bahan->bahan_by_lokasi($lokasi_id,$key->bahan_id);
                    $data = array();
                    $data["stock_bahan_qty"] = $key->jumlah;
                    $data["bahan_id"] = $key->bahan_id;
                    $data["hpp"] = $key->harga;
                    $data["stock_bahan_lokasi_id"] = $lokasi_id;
                    $insert = $this->stock_bahan->insert($data);
                    $arus["stock_bahan_id"] = $this->stock_bahan->last_id();
                    $arus["method"] = "insert";

					$arus["tanggal"] = date("Y-m-d");
					$arus["table_name"] = "stock_bahan";
					$arus["bahan_id"] = $key->bahan_id;
					$arus["stock_out"] = 0;
					$arus["stock_in"] = $key->jumlah;
					$arus["last_stock"] = $this->stock_bahan->last_stock($key->bahan_id)->result;
					$arus["last_stock_total"] = $this->stock_bahan->stock_total()->result;
					$arus["keterangan"] = "Order Bahan";
					$this->stock_bahan->arus_stock_bahan($arus);

					$harga_barang = $this->stock_bahan->update_hpp_bahan($key->bahan_id);
					$bahan = array();
					$bahan['bahan_harga'] = $harga_barang->total_harga / $harga_barang->total_qty;
					$this->bahan->update_by_id('bahan_id',$key->bahan_id,$bahan);
			}
		}
		if($this->po_bahan->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}
		echo json_encode($result);
	}

}

/* End of file PoBahanController.php */
/* Location: ./application/controllers/PoBahanController.php */