<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class RegisterRedController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->model('master_login','',true);
        $this->load->helper('string');

        $this->load->model('province');
        $this->load->model('city');
        $this->load->model('subdistrict');
        $this->load->model('reseller');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');
        $this->load->helper('string');
    }

    public function index()
    {
        $uri = $this->uri->segment(2);
        $data['referal_code'] = "";
        if($uri!=""){
            $data['referal_code'] = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
            $data['referal_code'] = $this->encryption->decrypt($data['referal_code']);
        }
        $province = $this->province->all_list();
        $data["bank"] = $this->db->get("mykindofbeauty_kemiri.bank")->result();
        $data['province'] = $province;
        $data["type"] = "super agen";
        $this->load->view('admin/register/register_red',$data);
    }
    function city(){
        $province_id = $this->input->post('province_id');
        $city = $this->city->geProvinceCity($province_id);
        echo json_encode(array("data"=>$city));
    }
    function subdistrict(){
        $city_id = $this->input->post('city_id');
        $subdistrict = $this->subdistrict->getCitySubdistrict($city_id);
        echo json_encode(array("data"=>$subdistrict));
    }
    function save(){
        $result['success'] = false;
        $result['message'] = "Parameter tidak sesuai";
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[reseller.email]',array(
            'is_unique'     => 'This %s already exists.'
        ));
        $this->form_validation->set_rules('phone', 'No Hp', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat Tinggal', 'required');
        $this->form_validation->set_rules('bank_id', 'Bank', 'required');
        $this->form_validation->set_rules('bank_rekening', 'Akun Bank', 'required');
        $this->form_validation->set_rules('bank_atas_nama', 'Atas Nama Akun Bank', 'required');
        $this->form_validation->set_rules('divisi', 'Divisi', 'required');

        $this->form_validation->set_error_delimiters('<span class="error-message" style="color:red">', '</span>');
        if ($this->form_validation->run() === TRUE) {
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $nama = $this->input->post('nama');
            $type = "redgroup";
            $alamat = $this->input->post('alamat');
            $divisi = $this->input->post('divisi');
            $bank_id = $this->input->post('bank_id');
            $bank_rekening = $this->input->post('bank_rekening');
            $bank_atas_nama = $this->input->post('bank_atas_nama');

            $data = array(
                "email"=>$email,
                "phone"=>$phone,
                "nama"=>$nama,
                "type"=>$type,
                "alamat"=>$alamat,
                "divisi"=>$divisi,
                "bank_id"=>$bank_id,
                "bank_rekening"=>$bank_rekening,
                "bank_atas_nama"=>$bank_atas_nama,
                "is_red"=>1,
            );
            $referal_code = $this->input->post('referal_code');
            if($referal_code!=""){
                $reseler = $this->reseller->resseler_by_referal_code($referal_code);
                $data["referal_id"] = $reseler->reseller_id;
            }
            $insert = $this->reseller->insert($data);
            if($insert){
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
                $reseller_id = $this->reseller->last_id();
                $prefix = random_string('alnum', 5);
                $referal_code = $prefix."-".$reseller_id;
                $data = array("referal_code"=>$referal_code);
                $this->reseller->update_by_id('reseller_id',$reseller_id,$data);
            }

            echo json_encode($result);

        }else{
            echo json_encode(
                array(
                    'status' => 'error',
                    'message' => 'Fill form completly',
                    'errors' => array(
                        'email' => form_error('email'),
                        'phone' => form_error('phone'),
                        'nama' => form_error('nama'),
                        'alamat' => form_error('alamat'),
                    )
                )
            );
        }

    }
    function uploadImage($file,$url){
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $date = new DateTime();
        $file_name = $date->getTimestamp().random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/".$file_name.".".$ext, $_FILES[$file]['tmp_name'],NET_SFTP_LOCAL_FILE);
        if (!$output)
        {
            $url = "failed";
            return $url;
        }
        else
        {
            $url = $file_name.'.'.$ext;
        }
        return $url;
    }
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
