<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PemetaanSuperAgentController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reseller','',true);
        $this->load->library('main');

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        // $jumlah = $this->reseller->super_maps();
        // $data['empty_maps'] = $jumlah;
        $column = array();
        $data["meta_title"] = "Pemetaan Super Reseller < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"provinsi"));
        array_push($column, array("data"=>"kabupaten"));
        array_push($column, array("data"=>"kecamatan"));
        array_push($column, array("data"=>"jumlah"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        // $akses_menu = json_decode($this->menu_akses,true);
        // $action = array();
        // foreach ($akses_menu['reseller']['super-agen'] as $key => $value) {
        //     if($key != "list" && $key != "akses_menu"&& $key != "activate"&& $key != "banned" ){
        //         $action[$key] = $value;
        //     }
        // }
        // $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pemetaan-super-agen/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reseller->pemetaan_super_all();
        $result['iTotalDisplayRecords'] = $this->reseller->pemetaan_super_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->pemetaan_super_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            // if($key->created_at != null){
            //     $time = strtotime($key->created_at);
            //     $key->created_at = date('d-m-Y H:i:s',$time);
            // }
            // if($key->updated_at != null){
            //     $time = strtotime($key->updated_at);
            //     $key->updated_at = date('d-m-Y H:i:s',$time);
            // }
            $key->no = $i;
            // $key->link_foto_profile = $this->config->item('dev_storage_image').$key->foto_profile;
            // $key->link_foto_ktp = $this->config->item('dev_storage_image').$key->foto_ktp;
            // $key->link_selfi_ktp = $this->config->item('dev_storage_image').$key->selfi_ktp;
            $i++;
            // $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */