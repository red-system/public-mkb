<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterUserController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_user','',true);
        $this->load->library('master');
        if(!isset($_SESSION['master'])){
            redirect(base_url().'master/login');
        }

    }
    public function index()
    {
        array_push($this->master->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->master->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->master->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->master->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->master->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->master->js, "script/app2.js");
        array_push($this->master->js, "script/master/master_user.js");

        $data["css"] = $this->master->css;
        $data["js"] = $this->master->js;
        $column = array();
        $data["meta_title"] = "Type < Master < Redpos";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"username"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $action = array("enter"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('master/static/header',$data);
        $this->load->view('master/static/sidebar');
        $this->load->view('master/static/topbar');
        $this->load->view('master/user/index');
        $this->load->view('master/static/footer');
    }

    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->master_user->count_all();
        $result['iTotalDisplayRecords'] = $this->master_user->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->master_user->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function enter($username){
        $this->load->model("master_login",'',true);
        $this->load->model("reseller",'',true);
        $this->load->model("user_role",'',true);
        $auth = $this->master_login->auth_login($username);
        $reseller_id = $auth->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $user_role = $this->user_role->row_by_id($auth->user_role_id);
        if($reseller==null){
            $result["success"] = true;
            $result["message"] = "Login success";
            $_SESSION["status_notif"] = false;
            $_SESSION["meeting_notif"] = false;
            $_SESSION["redpos_login"]["user_id"] = $auth->id;
            $_SESSION["redpos_login"]["user_role_id"] = $auth->user_role_id;
            $_SESSION["redpos_login"]["avatar"] = $auth->avatar;
            $_SESSION["redpos_login"]["staff_id"] = $auth->user_staff_id;
            $_SESSION["redpos_login"]["db_name"] = $auth->database_name;
            $_SESSION["redpos_login"]["company_id"] = $auth->company_id;
            $_SESSION["redpos_login"]["pay"] = $auth->midtrans;
            $_SESSION["redpos_login"]["user_role_name"] = $user_role->user_role_name;
            $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_prod;
            $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_prod;
            $_SESSION["redpos_login"]["pay-prod"] = true;
            if($auth->production==0){
                $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_sands;
                $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_sands;
                $_SESSION["redpos_login"]["pay-prod"] = false;
            }
            $this->load->model('staff','',true);
            $staff = $this->staff->row_by_id($auth->user_staff_id);
            $_SESSION["redpos_login"]["user_name"] = $staff->staff_nama;
            $_SESSION["redpos_login"]["email"] = $staff->staff_email;
            $_SESSION["redpos_company"]["company_name"] = $auth->company_name;
            $_SESSION["redpos_company"]["company_full_name"] = $auth->company_full_name;
            $_SESSION["redpos_company"]["company_email"] = $auth->company_email;
            $_SESSION["redpos_company"]["company_address"] = $auth->company_address;
            $_SESSION["redpos_company"]["company_phone"] = $auth->company_phone;
            $_SESSION["redpos_company"]["max_user"] = $auth->max_user;
            $_SESSION["redpos_company"]["max_outlet"] = $auth->max_user;

            $_SESSION["redpos_company"]["database"] = $auth->database_name;
            if($auth->lokasi_id != null){
                $_SESSION["redpos_login"]["lokasi_id"] = $auth->lokasi_id;
                $this->load->model('lokasi','',true);
                $lokasi = $this->lokasi->row_by_id($auth->lokasi_id);
                $_SESSION["redpos_login"]["lokasi_nama"] = $lokasi->lokasi_nama;
            }
        }
        else if($reseller->status!="banned"){
            $result["success"] = true;
            $result["message"] = "Login success";
            $_SESSION["status_notif"] = true;
            $_SESSION["redpos_login"]["user_id"] = $auth->id;
            $_SESSION["redpos_login"]["user_role_id"] = $auth->user_role_id;
            $_SESSION["redpos_login"]["reseller_id"] = $reseller_id;
            $_SESSION["redpos_login"]["avatar"] = $auth->avatar;
            $_SESSION["redpos_login"]["staff_id"] = $auth->user_staff_id;
            $_SESSION["redpos_login"]["db_name"] = $auth->database_name;
            $_SESSION["redpos_login"]["company_id"] = $auth->company_id;
            $_SESSION["redpos_login"]["user_role_name"] = $user_role->user_role_name;
            $_SESSION["redpos_login"]["pay"] = $auth->midtrans;
            $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_prod;
            $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_prod;
            $_SESSION["redpos_login"]["pay-prod"] = true;
            if($auth->production==0){
                $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_sands;
                $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_sands;
                $_SESSION["redpos_login"]["pay-prod"] = false;
            }
            $this->load->model('staff','',true);
            $staff = $this->staff->row_by_id($auth->user_staff_id);
            $_SESSION["redpos_login"]["user_name"] = $staff->staff_nama;
            $_SESSION["redpos_login"]["email"] = $staff->staff_email;
            $_SESSION["redpos_company"]["company_name"] = $auth->company_name;
            $_SESSION["redpos_company"]["company_full_name"] = $auth->company_full_name;
            $_SESSION["redpos_company"]["company_email"] = $auth->company_email;
            $_SESSION["redpos_company"]["company_address"] = $auth->company_address;
            $_SESSION["redpos_company"]["company_phone"] = $auth->company_phone;
            $_SESSION["redpos_company"]["max_user"] = $auth->max_user;
            $_SESSION["redpos_company"]["max_outlet"] = $auth->max_user;
            $_SESSION["redpos_company"]["database"] = $auth->database_name;
            if($auth->lokasi_id != null){
                $_SESSION["redpos_login"]["lokasi_id"] = $auth->lokasi_id;
                $this->load->model('lokasi','',true);
                $lokasi = $this->lokasi->row_by_id($auth->lokasi_id);
                $_SESSION["redpos_login"]["lokasi_nama"] = $lokasi->lokasi_nama;
            }

            $this->load->model('meeting','',true);
            $check_meeting = $this->meeting->meeting_notif($reseller_id);
            if ($check_meeting == true) {
                $_SESSION["meeting_notif"] = true;
            }else{
                $_SESSION["meeting_notif"] = false;
            }

        } else {
            $result["message"] = "Akun anda telah di banned";
        }
        echo json_encode($result);

    }

}

/* End of file SizeController.php */
/* Location: ./application/controllers/SizeController.php */
