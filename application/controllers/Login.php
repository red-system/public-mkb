<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

        $this->load->model('master_login','',true);
		$this->load->helper('string');
        $this->load->library('main');
        if(isset($_SESSION['redpos_login'])){
            $this->load->model('user','',true);
            $this->load->model('staff','',true);
            $this->load->model('lokasi','',true);
            
        }
	}

	public function index()
	{
		if(isset($_SESSION["redpos_login"])){
			redirect(base_url(),'refresh');
		} else {
			$this->load->view('admin/login2');
		}
	}
	function auth(){
        $token = $this->input->post('token');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $this->config->item('recaptcha_secret_key'), 'response' => $token)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $arrResponse = json_decode($response, true);
        if($arrResponse['success']==1){
            $result['success'] = false;
            $result['message'] = "missing parameter";
            $_SESSION["meeting_notif"] = false;
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $this->load->model('reseller','',true);
            $this->load->model('user_role','',true);
            $auth = $this->master_login->auth_login($username);
            if($auth != null){
                if (password_verify($password, $auth->password)){
                    $reseller_id = $auth->reseller_id;
                    $reseller = $this->reseller->row_by_id($reseller_id);
                    $user_role = $this->user_role->row_by_id($auth->user_role_id);
                    if($reseller==null){
                        $result["success"] = true;
                        $result["message"] = "Login success";
                        $_SESSION["status_notif"] = false;
                        $_SESSION["meeting_notif"] = false;
                        $_SESSION["redpos_login"]["user_id"] = $auth->id;
                        $_SESSION["redpos_login"]["user_role_id"] = $auth->user_role_id;
                        $_SESSION["redpos_login"]["avatar"] = $auth->avatar;
                        $_SESSION["redpos_login"]["staff_id"] = $auth->user_staff_id;
                        $_SESSION["redpos_login"]["db_name"] = $auth->database_name;
                        $_SESSION["redpos_login"]["company_id"] = $auth->company_id;
                        $_SESSION["redpos_login"]["pay"] = $auth->midtrans;
                        $_SESSION["redpos_login"]["user_role_name"] = $user_role->user_role_name;
                        $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_prod;
                        $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_prod;
                        $_SESSION["redpos_login"]["pay-prod"] = true;
                        if($auth->production==0){
                            $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_sands;
                            $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_sands;
                            $_SESSION["redpos_login"]["pay-prod"] = false;
                        }
                        $this->load->model('staff','',true);
                        $staff = $this->staff->row_by_id($auth->user_staff_id);
                        $_SESSION["redpos_login"]["user_name"] = $staff->staff_nama;
                        $_SESSION["redpos_login"]["email"] = $staff->staff_email;
                        $_SESSION["redpos_company"]["company_name"] = $auth->company_name;
                        $_SESSION["redpos_company"]["company_full_name"] = $auth->company_full_name;
                        $_SESSION["redpos_company"]["company_email"] = $auth->company_email;
                        $_SESSION["redpos_company"]["company_address"] = $auth->company_address;
                        $_SESSION["redpos_company"]["company_phone"] = $auth->company_phone;
                        $_SESSION["redpos_company"]["max_user"] = $auth->max_user;
                        $_SESSION["redpos_company"]["max_outlet"] = $auth->max_user;

                        $_SESSION["redpos_company"]["database"] = $auth->database_name;
                        if($auth->lokasi_id != null){
                            $_SESSION["redpos_login"]["lokasi_id"] = $auth->lokasi_id;
                            $this->load->model('lokasi','',true);
                            $lokasi = $this->lokasi->row_by_id($auth->lokasi_id);
                            $_SESSION["redpos_login"]["lokasi_nama"] = $lokasi->lokasi_nama;
                        }
                    }
                    else if($reseller->status!="banned"){
                        $result["success"] = true;
                        $result["message"] = "Login success";
                        $_SESSION["status_notif"] = true;
                        $_SESSION["redpos_login"]["user_id"] = $auth->id;
                        $_SESSION["redpos_login"]["user_role_id"] = $auth->user_role_id;
                        $_SESSION["redpos_login"]["reseller_id"] = $reseller_id;
                        $_SESSION["redpos_login"]["avatar"] = $auth->avatar;
                        $_SESSION["redpos_login"]["staff_id"] = $auth->user_staff_id;
                        $_SESSION["redpos_login"]["db_name"] = $auth->database_name;
                        $_SESSION["redpos_login"]["company_id"] = $auth->company_id;
                        $_SESSION["redpos_login"]["user_role_name"] = $user_role->user_role_name;
                        $_SESSION["redpos_login"]["pay"] = $auth->midtrans;
                        $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_prod;
                        $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_prod;
                        $_SESSION["redpos_login"]["pay-prod"] = true;
                        if($auth->production==0){
                            $_SESSION["redpos_login"]["pay-c"] = $auth->client_key_sands;
                            $_SESSION["redpos_login"]["pay-s"] = $auth->server_key_sands;
                            $_SESSION["redpos_login"]["pay-prod"] = false;
                        }
                        $this->load->model('staff','',true);
                        $staff = $this->staff->row_by_id($auth->user_staff_id);
                        $_SESSION["redpos_login"]["user_name"] = $staff->staff_nama;
                        $_SESSION["redpos_login"]["email"] = $staff->staff_email;
                        $_SESSION["redpos_company"]["company_name"] = $auth->company_name;
                        $_SESSION["redpos_company"]["company_full_name"] = $auth->company_full_name;
                        $_SESSION["redpos_company"]["company_email"] = $auth->company_email;
                        $_SESSION["redpos_company"]["company_address"] = $auth->company_address;
                        $_SESSION["redpos_company"]["company_phone"] = $auth->company_phone;
                        $_SESSION["redpos_company"]["max_user"] = $auth->max_user;
                        $_SESSION["redpos_company"]["max_outlet"] = $auth->max_user;
                        $_SESSION["redpos_company"]["database"] = $auth->database_name;
                        if($auth->lokasi_id != null){
                            $_SESSION["redpos_login"]["lokasi_id"] = $auth->lokasi_id;
                            $this->load->model('lokasi','',true);
                            $lokasi = $this->lokasi->row_by_id($auth->lokasi_id);
                            $_SESSION["redpos_login"]["lokasi_nama"] = $lokasi->lokasi_nama;
                        }

                        $this->load->model('meeting','',true);
                        $check_meeting = $this->meeting->meeting_notif($reseller_id);
                        if ($check_meeting == true) {
                            $_SESSION["meeting_notif"] = true;
                        }else{
                            $_SESSION["meeting_notif"] = false;
                        }

                    } else {
                        $result["message"] = "Akun anda telah di banned";
                    }

                } else {
                    $result["message"] = "Wrong password";
                }
            } else {
                $result["message"] = "Email not registered";
            }
            echo json_encode($result);
        }else{
            $result['success'] = false;
            $result['message'] = "Gagal mengirim pesan";
            echo json_encode($result);
        }
	}
	function logout(){
		$result['success'] = false;
		$result['message'] = 'Gagal logout, silakan lakukan closing pada POS terlebih dahulu';
		if(!isset($_SESSION['log_pos']['log_kasir_id'])){
			unset($_SESSION["redpos_login"]);
            unset($_SESSION["redpos_company"]);
			$result['success'] = true;
			$result['message'] = 'Berhasil logout';
		}
		echo json_encode($result);
	}
	function forget(){
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
        $this->load->model('user','',true);
		$auth = $this->master_login->auth_login_forget($email);
        $reseller = $auth;
		if($auth != null){
			$timeout = time()+(24*3600);
			$code = random_string('alnum', 25);
			$session = $this->user->forgot_request($email,$code,$timeout);
			$url = base_url().'forget/'.$email.'/'.$code;
			if($session){
                $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-danger {
            color: #fff;
            background-color: #82070B;
            border-color: #82070B;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 450px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo '.$reseller->nama.',</h4>
                        <h3 style="text-align: left;font-weight: 600">Konfirmasi email Anda.</h3>
                        <span>Permintaan Anda untuk mengganti kata sandi telah disetujui, klik link dibawah untuk lanjut.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <a href="'.$url.'" class="btn btn-danger btn-pill" style="color: #fff">Ganti Kata Sandi</a>
                        </div>
                        <span>
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>

                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                        <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
				$this->load->library('email');
                $this->main->mailer_auth('Request forget password', $email, $auth->nama, $mailContentAdmin);
					$result["success"] = true;
					$result["message"] = "Your request has been accepted please check your email";
			}

		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}
	public function forget_index(){
		$email = $this->uri->segment(2);
		$code = $this->uri->segment(3);
		$check = $this->master_login->forget_session_check($email,$code);
		if(sizeof($check)){
			$data["email"]  = $email;
			$data["code"]  = $code;
			$this->load->view('admin/reset_password');
		} else {
			redirect(base_url()."404_override",'refresh');
		}
	}
	function reset_password(){
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$code = $this->input->post('code');
		$new_password = $this->input->post('new_pass');
		$check = $this->master_login->forget_session_check($email,$code);
		if(sizeof($check)){
			$password = password_hash($new_password, PASSWORD_BCRYPT);
            $auth = $this->master_login->auth_login_forget($email);
			$reseller_id = $auth->reseller_id;
			$login_id = $this->master_login->get_id_by_reseller($reseller_id)->id;

			$change = $this->master_login->change_password($login_id,$password);
			if ($change) {
				$result["success"] = true;
				$result["message"] = "Your password successfully changed";
			} else {
				$result["message"] = "Failed to change password";
			}
		}
		echo json_encode($result);
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
