<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class NotifyController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('midtrans_response','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('produk','',true);
        $this->load->model('rekap_bulanan','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('bonus','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('reseller','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('response_payment','',true);
        $this->load->model('master_login','',true);
        $this->load->model('fullpayment','',true);
        $this->load->library('main');
        $this->load->helper('string');
        $this->load->model('claim_reward','',true);
    }

    public function index()
    {

        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);
        if($result->order_id=="POP/0223/1106/1-WE02x"||$result->order_id=="POP/0223/514/11-ioQU0"||$result->order_id=="POP/0223/514/10-MBbiu"||$result->order_id=="POP/0223/421/2-uVmb3"){
            return;
        }
        $dataMidtrans = array();
        $dataMidtrans['json'] = $json_result;
        $dataMidtrans['transaction_status'] = $result->transaction_status;
        $dataMidtrans['order_id'] = $result->order_id;
        $this->midtrans_response->insert($dataMidtrans);
        $result = json_decode($json_result);
        $paramPengiriman = "13:00:00";
        $paramBonus = "16:00:00";
        $estimasiBonus = $this->main->tanggal_kerja($paramBonus);
        if($result){
            $order_id = $result->order_id;
            $arrOrder = explode('-',$order_id);
            $statusPembayaran = "Pending";
            switch ($result->transaction_status){
                case "settlement":
                    $statusPembayaran = "Lunas" ;
                    $status = "success";
                    break;
                case "capture":
                    $statusPembayaran = "Lunas" ;
                    $status = "success";
                    break;
                case "pending":
                    $statusPembayaran = "Pending" ;
                    $status = "pending";
                    break;
                case "cancel":
                    $statusPembayaran = "Batal" ;
                    $status = "error";
                    break;
                case "deny":
                    $statusPembayaran = "Gagal" ;
                    $status = "error";
                    break;
                case "expire":
                    $statusPembayaran = "Gagal" ;
                    $status = "error";
                    break;
                default:
                    $statusPembayaran = "Gagal" ;
                    $status = "error";
                    break;
            }
            $po_produk_no = $arrOrder[0];
            $po_produk = $this->po_produk->row_by_field('po_produk_no',$po_produk_no);
            $lokasi_id = $po_produk->lokasi_reseller;
            $po_produk_id = $po_produk->po_produk_id;
            $cek = $this->response_payment->row_by_field('po_produk_id',$po_produk_id);
            if($cek==null){
                $data = array();
                $data["status"] = $status;
                $data["json"] = $json_result;
                $this->response_payment->insert($data);
            }else{
                $data = array();
                $data["status"] = $status;
                $data["json"] = $json_result;
                $this->response_payment->update_by_id('po_produk_id',$po_produk_id,$data);
            }
            $pembayaran = array();
            $pembayaran["status_pembayaran"] = $statusPembayaran;
            if(isset($result->settlement_time)){
                $pembayaran["tanggal_uang_diterima"] = $result->settlement_time;
            }
            $po_data = $this->po_produk->row_by_id($po_produk_id);
            $login = $this->db->where("lokasi_id",$po_data->lokasi_reseller)->get("mykindofbeauty_master.login")->row();
            $reseller_id = $login->reseller_id;
            $reseller = $this->reseller->row_by_id($reseller_id);
            $data_user = $this->master_login->row_by_field('reseller_id', $reseller_id);
            $username = $data_user->username;
            $referal_id = $reseller->referal_id;
            $deposit_anak = $this->po_produk->total_deposit_reseller($po_data->lokasi_reseller);
            $deposit_anak = $deposit_anak == "" ? 0 : $deposit_anak;
            $anak_full_payment = $this->fullpayment->is_fullpayment($reseller_id);
            $minimal_po_anak = $this->po_produk->minimal_po_cek($reseller_id);
            $indikator_anak = 0;
            if($deposit_anak<$minimal_po_anak){
                $indikator_anak = 1;
            }
            $login_induk = $this->db->where("reseller_id",$referal_id)->get("mykindofbeauty_master.login")->row();

            $lokasi_induk = $login_induk->lokasi_id;
            $deposit_induk = $this->po_produk->total_deposit_reseller($lokasi_induk);
            $deposit_induk = $deposit_induk == "" ? 0 : $deposit_induk;
            $induk_full_payment = $this->fullpayment->is_fullpayment($referal_id);
            $minimal_po_induk = $this->po_produk->minimal_po_cek($referal_id);
            $indikator_induk = 0;
            if($deposit_induk<$minimal_po_induk){
                $indikator_induk = 1;
            }
            $tanggal_rekap = date("Y-m-d");
            if($status=="success"){

                if($reseller->leader!=null){
                    $leader = $this->reseller->row_by_id($reseller->leader);
                    $currentTime = (date("Y-m-d"));
                    $lastDepositTime = ($leader->next_deposit_date);
                    if(($currentTime<=$lastDepositTime)){
                        $pembayaran['om_leader'] = $reseller->leader;
                    }
                }
                if($po_data->voucher_code==null){
                    $voucher_code = time() . $reseller_id . random_string('numeric', 5);
                }else{
                    $voucher_code = $po_data->voucher_code;
                }
                $voucher_text = '';
                $tes = null;
                $po_detail = $this->db->select('po_produk_detail.*,produk.produk_nama')->join('mykindofbeauty_kemiri.produk as produk','produk.produk_id = mykindofbeauty_kemiri.po_produk_detail.produk_id')->where('po_produk_id',$po_produk_id)->get('mykindofbeauty_kemiri.po_produk_detail')->result();
                if ( $reseller->type=="agent"){
                    $voucher_text = "";
                    foreach ($po_detail as $voucher_produk_detail){
                        $voucher_text = $voucher_text.'<tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Kode Voucher '.$voucher_produk_detail->produk_nama.'</td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$voucher_produk_detail->voucher_detail.'</td>
                                    </tr>';
                        $tes = $voucher_produk_detail->voucher_detail;
                    }

                }
                $referal_link = $this->config->item('url-landing').$reseller->referal_code;
                $message_tanda_terima = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 420;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h3 style="text-align: center;font-weight: 500">Tanda Terima Pembayaran</h3>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table width="100%">
                                <tbody>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">No PO </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$po_data->po_produk_no.'</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Tanggal </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.date("Y-m-d").'</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Kepada </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$reseller->nama.' ('.$username.')</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Total </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.number_format($po_data->grand_total).'</td>
                                    </tr>
                                    '.$voucher_text.'
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Mohon tanda terima pembayaran ini disimpan dengan baik sebagai bukti pembayaran yang valid.
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                       <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                       <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>
                </td>
            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>
</div>
</body></html>';
                $this->main->mailer_auth('Bukti Pembayaran', $reseller->email, $reseller->nama, $message_tanda_terima,$tes);
                $estimasi = $this->main->tanggal_kerja($paramBonus);
                $pembayaran['estimasi_pengiriman'] =$estimasi;

                $reseller_penerima = $this->reseller->row_by_id($referal_id);
                $data_user_penerima = $this->master_login->row_by_field('reseller_id', $referal_id);
                $username_penerima = $data_user_penerima->username;

                $resellerData["first_deposit"] = 1;
                $deposit = $this->po_produk->total_deposit_reseller($po_data->lokasi_reseller);
                $deposit = $deposit == "" ? 0 : $deposit;
                $minimal_po = $this->po_produk->minimal_po_cek($reseller_id);
                // $this->po_produk->rekap_bulanan($reseller_id,$tanggal_rekap,$lokasi_id,'"total_po"');

                if($reseller->first_deposit==0){
                    $first_deposit_date = new DateTime(date("Y-m-d"));
                    $money_date = new DateTime(date("Y-m-d"));
                    $resellerData["first_deposit_date"] = $first_deposit_date->format("Y-m-d");
                    $first_deposit_date->modify("+6 month");
                    $resellerData["first_limit_date"] = $first_deposit_date->format("Y-m-d");
                    $resellerData["next_limit_date"] = $first_deposit_date->format("Y-m-d");
                    $money_date->modify("+2 year");
                    $resellerData["money_back_guarantee"] = $money_date->format("Y-m-d");
                }
                $current_date = date("Y-m-d");
                $last_date = date("Y-m-25",strtotime($current_date));
                $custom_date = new DateTime($last_date);
                $paramDeposit = 2;
                if($reseller->type=="super agen"){
                    $bintang = $this->claim_reward->get_bintang($reseller_id);
                    if($bintang>2){
                        $paramDeposit = 1;
                    }
                }
                $custom_date->modify('+'.$paramDeposit.' month');
                $added_date = $custom_date->format('Y-m-d');
                $next_deposit_date = date("Y-m-25",strtotime($added_date));
                $resellerData["next_deposit_date"] = $next_deposit_date;
                $resellerData["status"] = "active";
                $resellerData["last_deposit"] = date("Y-m-d");
                $this->reseller->update_by_id("reseller_id",$reseller_id,$resellerData);
                $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
                $this->db->select("mykindofbeauty_kemiri.staff.*");
                $this->db->join("mykindofbeauty_kemiri.staff","mykindofbeauty_master.login.user_staff_id = mykindofbeauty_kemiri.staff.staff_id");
                $this->db->where("mykindofbeauty_master.login.company_id",3);
                $this->db->where("mykindofbeauty_master.login.user_role_id",2);
                $login = $this->db->get("mykindofbeauty_master.login")->result();
                $reseller_bank = $this->db->where("bank_id",$reseller->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
                if ( $reseller->type=="super agen"){
                } else {
                    $po["status_penerimaan"] = "Diterima";
                    $po["tanggal_penerimaan"] = date("Y-m-d");
                    $po['lokasi_penerimaan_id'] = $po_data->lokasi_reseller;
                    $this->po_produk->update_by_id("po_produk_id", $po_produk_id, $po);
                    foreach ($po_detail as $itemPoDetail){
//                        $checkVoucher = $this->voucher_produk->cekVoucher($po_produk_id, $reseller_id);
                        if ($result->payment_type!="credit_card"){
                            $voucher = array();
                            $voucher["reseller_pemilik"] = $reseller_id;
                            $voucher["voucher_code"] = $itemPoDetail->voucher_detail==null?$po_produk->voucher_produk:$itemPoDetail->voucher_detail;
                            $voucher["po_produk_id"] = $po_produk_id;
                            $voucher["po_produk_detail_id"] = $itemPoDetail->po_produk_detail_id;
                            $voucher["created_at"] = date("Y-m-d H:i:s");
                            $this->db->insert('mykindofbeauty_kemiri.voucher_produk',$voucher);
                        }
                    }

                }
                $withdraw_cash = $reseller->withdraw;
                $sisa_deposit_withdraw = ($reseller->sisa_deposit_withdraw-$po_data->grand_total) <= 0 ? 0 : abs($reseller->sisa_deposit_withdraw-$po_data->grand_total);
                $loyalti_withdraw = ($reseller->loyalti_withdraw-$po_data->grand_total) <= 0 ? 0 : abs($reseller->loyalti_withdraw-$po_data->grand_total);
                $sisa = $withdraw_cash - $po_data->grand_total;
                $sisa_deposit = $sisa <= 0 ? abs($sisa) : 0;

                $withdraw_reseller = $sisa <= 0 ? 0 : abs($sisa);
                $resellerUpdate = array();
                $resellerUpdate['withdraw'] = $withdraw_reseller;
                $resellerUpdate['sisa_deposit_withdraw'] = $sisa_deposit_withdraw;
                $resellerUpdate['loyalti_withdraw'] = $loyalti_withdraw;
                $this->reseller->update_by_id('reseller_id',$reseller_id,$resellerUpdate);

                if($referal_id!=null){

                    // $this->po_produk->rekap_bulanan($reseller_id,$tanggal_rekap,$lokasi_id,'"omzet"');
                    //
                    $withdraw_penerima = $reseller_penerima->sisa_deposit_withdraw;
                    $sisa_penerima = $withdraw_penerima - $po_data->grand_total;
                    $withdraw_penerima_ganti = $sisa_penerima <= 0 ? 0 : abs($sisa_penerima);
                    $resellerUpdate = array();
                    $resellerUpdate['sisa_deposit_withdraw'] = $withdraw_penerima_ganti;
                    $this->reseller->update_by_id('reseller_id',$referal_id,$resellerUpdate);

                    //
                    if($withdraw_reseller>0){
                        $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                        $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Pemberitahuan</h3>
                        <span>Super Reseller referensi anda atas nama '.$reseller->nama.' melakukan redeposit dengan detail sebagai berikut : 
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Super Reseller </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . ' </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->po_total) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Total Withdraw Cash </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($withdraw_cash) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Sisa Withdraw Cash </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($withdraw_reseller) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span style="color: red">
                          Note : anda belum bisa mendapakat bonus referensi dari '.$reseller_penerima->nama.' ('.$username_penerima.') sebelum super reseller tersebut melakukan redeposit sejumlah total sisa withdraw cashnya.
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                        $this->main->mailer_auth('Pemberitahuan Deposit Reseller Referensi', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                    }
                    if($sisa_deposit>0){
                        if(!($reseller_penerima->type=="agent" && $reseller->type=="super agen")){
                            if($reseller_penerima->type=="super agen" && $reseller->type=="super agen"){
                                if($this->bonus->cekBonus($po_produk_id,$reseller_id,"loyalty bonus")) {
//                                $status_bonus_reseller_loyal = (($deposit_induk < $minimal_po_induk)||$withdraw_penerima>0) ? "Hold" : "On Process";
                                    $status_bonus_reseller_loyal = (!$induk_full_payment||$withdraw_penerima>0) ? "Hold" : "On Process";
                                    $bonus = array();
                                    $bonus["tanggal"] = $estimasiBonus;
                                    $bonus["from_reseller_id"] = $reseller->reseller_id;
                                    $bonus["type"] = "loyalty bonus";
                                    $bonus["jumlah_deposit"] = $sisa_deposit;
                                    $bonus["persentase"] = 5;
                                    $bonus["jumlah_bonus"] = $sisa_deposit * 0.05;
                                    $bonus["to_reseller_id"] = $referal_id;
                                    $bonus["po_produk_id"] = $po_produk_id;
                                    $bonus["status"] = $status_bonus_reseller_loyal;

                                    $kelebihan = $reseller_penerima->kelebihan;
                                    $potongan = 0;
                                    $text_kelebihan = '';
                                    if($kelebihan>0){
                                        if($kelebihan<$bonus["jumlah_bonus"]){
                                            $bonus["potongan"] = $kelebihan;
                                            $potongan = $kelebihan;
                                            $bonus["jumlah_bonus"] = $bonus["jumlah_bonus"] - $kelebihan;
                                            $kelebihan = 0;
                                        }else{
                                            $bonus["potongan"] = $bonus["jumlah_bonus"];
                                            $potongan = $bonus["jumlah_bonus"];
                                            $kelebihan = $kelebihan - $bonus["jumlah_bonus"];
                                            $bonus["jumlah_bonus"] = 0;

                                        }
                                        $total_bonus = $bonus["jumlah_bonus"];
                                        $text_kelebihan = '<tr style="height: 25px">
                                                            <td style="text-align: left">Potongan </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($potongan). ' </td>
                                                        </tr>
                                                        <tr style="height: 25px">
                                                            <td style="text-align: left">Total Bonus Didapat </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($total_bonus). ' </td>
                                                        </tr>';
                                    }
                                    $this->bonus->insert($bonus);

                                    $dataKelebihan['kelebihan'] = $kelebihan;
                                    $this->reseller->update_by_id('reseller_id',$reseller_penerima->reseller_id,$dataKelebihan);
                                    $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                                    if (!$induk_full_payment){
                                        $status_message = "Bonus anda akan di hold karena anda belum memenuhi minimal deposit. Silakan lakukan deposit untuk mencairkan bonus.";
                                    }
                                    $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                                    $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS LOYALTI yang akan langsung di transfer ke rekening MKB Anda.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Pemilik Voucher </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah/Sisa Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">5%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit * 0.05) . '</td>
                                    </tr>
                                    '.$text_kelebihan.'
                                </tbody>
                            </table>
                        </div>
                        <span>
                           '.$status_message.'
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                                    $this->main->mailer_auth('Bonus Loyalti', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                                }
                            }
                            if($this->bonus->cekBonus($po_produk_id,$referal_id)) {
                                $bonus = array();
                                $bank = $this->db->where("bank_id", $reseller_penerima->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
//                            $status_bonus_reseller = (($indikator_induk == 1 && $reseller_penerima->type == "agent")||$withdraw_penerima>0) ? "Hold" : "On Process";
                                $status_bonus_reseller = ((!$induk_full_payment && $reseller_penerima->type == "agent")||$withdraw_penerima>0) ? "Hold" : "On Process";
                                $bonus["tanggal"] = $estimasiBonus;
                                $bonus["from_reseller_id"] = $reseller_id;
                                $bonus["type"] = "referal bonus";
                                $bonus["jumlah_deposit"] = $sisa_deposit;
                                $bonus["persentase"] = 10;
                                $bonus["jumlah_bonus"] = $sisa_deposit * 0.1;
                                $bonus["status"] = $status_bonus_reseller;
                                $bonus["to_reseller_id"] = $referal_id;
                                $bonus["po_produk_id"] = $po_produk_id;
                                $kelebihan = $reseller_penerima->kelebihan;
                                $potongan = 0;
                                $text_kelebihan = '';
                                if($kelebihan>0){
                                    if($kelebihan<$bonus["jumlah_bonus"]){
                                        $bonus["potongan"] = $kelebihan;
                                        $potongan = $kelebihan;
                                        $bonus["jumlah_bonus"] = $bonus["jumlah_bonus"] - $kelebihan;
                                        $kelebihan = 0;
                                    }else{
                                        $bonus["potongan"] = $bonus["jumlah_bonus"];
                                        $potongan = $bonus["jumlah_bonus"];
                                        $kelebihan = $kelebihan - $bonus["jumlah_bonus"];
                                        $bonus["jumlah_bonus"] = 0;

                                    }
                                    $total_bonus = $bonus["jumlah_bonus"];
                                    $text_kelebihan = '<tr style="height: 25px">
                                                            <td style="text-align: left">Potongan </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($potongan). ' </td>
                                                        </tr>
                                                        <tr style="height: 25px">
                                                            <td style="text-align: left">Total Bonus Didapat </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($total_bonus). ' </td>
                                                        </tr>';
                                }

                                $this->bonus->insert($bonus);
                                $dataKelebihan['kelebihan'] = $kelebihan;
                                $this->reseller->update_by_id('reseller_id',$reseller_penerima->reseller_id,$dataKelebihan);
                                $status_bonus = "On Process";
                                $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                                $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                                //here
                                if (!$induk_full_payment && $reseller_penerima->type == "agent") {
                                    $status_bonus = "Hold";
                                    $status_message = "Bonus anda akan di hold karena akun anda termasuk kedalam akun frezze. Silakan lakukan deposit untuk mencairkan bonus.";
                                }
                                $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 500px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS REFERENSI dari pendaftaran Reseller/Super Reseller MKB a/n ' . $reseller->nama . '
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah/Sisa Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">10%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit * 0.1) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Status </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $status_bonus . '</td>
                                    </tr>
                                    '.$text_kelebihan.'
                                </tbody>
                            </table>
                        </div>
                        <span>
                            ' . $status_message . '
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                     <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                                $this->main->mailer_auth('Bonus Referensi', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                            }
                        }
                    }

                }
                if((($deposit+$po_produk_item[0]->po_total)>=$minimal_po_anak)&&($withdraw_reseller==0)){
                    $this->bonus->change_hold_bonus($reseller_id,$estimasiBonus);
                }
                if($reseller_id->is_pass_first_limit==1){
                    $next_date = new DateTime(date("Y-m-d"));
                    $next_date->modify('+1 month');
                    $next_date_label = $next_date->format("Y-m-d");
                    $next_data = array();
                    $next_data['next_limit_date'] = $next_date_label;
                    $next_data['skip_deposit_count'] = 0;
                    $next_data['is_frezze'] = 0;
                    $this->reseller->update_by_id('reseller_id',$reseller_id,$next_data);
                }
            }
            $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$pembayaran);
            $this->rekap_bulanan->get_redeposit($reseller_id,$po_data->grand_total,$po_produk_id);
            if($reseller->leader!=null){
                if($reseller->reseller_id == $reseller->leader){
                    $this->rekap_bulanan->total_po($reseller->leader,$tanggal_rekap);
                } else {
                    $this->rekap_bulanan->total_omzet_reseller($reseller->leader,$tanggal_rekap);
                }
            }

        }
        error_log(print_r($result,TRUE));
    }
    public function manual(){
        $json_result = file_get_contents('php://input');
        $json_result = '{
  "va_numbers": [
    {
      "va_number": "30294036549",
      "bank": "bca"
    }
  ],
  "transaction_time": "2023-02-16 05:07:32",
  "transaction_status": "settlement",
  "transaction_id": "058df40b-3b99-4067-9668-080fa65f38b6",
  "status_message": "midtrans payment notification",
  "status_code": "200",
  "signature_key": "37b13fa6bb0e954e0b10f775e889afac076d2b1d7ff5011682a5b755e58dee731c64f67727207bd7a359cb406befd22c501a1fc3f619a3286f973998cbef9ec7",
  "settlement_time": "2023-02-16 05:07:53",
  "payment_type": "bank_transfer",
  "payment_amounts": [],
  "order_id": "POP/0623/1149/1-UjeOL",
  "merchant_id": "G317630294",
  "gross_amount": "1180000.00",
  "fraud_status": "accept",
  "expiry_time": "2023-02-17 05:07:28",
  "currency": "IDR"
}';

        $result = json_decode($json_result);
        if($result->order_id=="POP/0223/1106/1-WE02x"||$result->order_id=="POP/0223/514/11-ioQU0"||$result->order_id=="POP/0223/514/10-MBbiu"||$result->order_id=="POP/0223/421/2-uVmb3"){
            return;
        }
        $dataMidtrans = array();
        $dataMidtrans['json'] = $json_result;
        $dataMidtrans['transaction_status'] = $result->transaction_status;
        $dataMidtrans['order_id'] = $result->order_id;
        $this->midtrans_response->insert($dataMidtrans);
        $result = json_decode($json_result);
        $paramPengiriman = "13:00:00";
        $paramBonus = "16:00:00";
        $estimasiBonus = $this->main->tanggal_kerja($paramBonus);
        if($result){
            $order_id = $result->order_id;
            $arrOrder = explode('-',$order_id);
            $statusPembayaran = "Pending";
            switch ($result->transaction_status){
                case "settlement":
                    $statusPembayaran = "Lunas" ;
                    $status = "success";
                    break;
                case "capture":
                    $statusPembayaran = "Lunas" ;
                    $status = "success";
                    break;
                case "pending":
                    $statusPembayaran = "Pending" ;
                    $status = "pending";
                    break;
                case "cancel":
                    $statusPembayaran = "Batal" ;
                    $status = "error";
                    break;
                case "deny":
                    $statusPembayaran = "Gagal" ;
                    $status = "error";
                    break;
                case "expire":
                    $statusPembayaran = "Gagal" ;
                    $status = "error";
                    break;
                default:
                    $statusPembayaran = "Gagal" ;
                    $status = "error";
                    break;
            }
            $po_produk_no = $arrOrder[0];
            $po_produk = $this->po_produk->row_by_field('po_produk_no',$po_produk_no);
            $lokasi_id = $po_produk->lokasi_reseller;
            $po_produk_id = $po_produk->po_produk_id;
            $cek = $this->response_payment->row_by_field('po_produk_id',$po_produk_id);
            if($cek==null){
                $data = array();
                $data["status"] = $status;
                $data["json"] = $json_result;
                $this->response_payment->insert($data);
            }else{
                $data = array();
                $data["status"] = $status;
                $data["json"] = $json_result;
                $this->response_payment->update_by_id('po_produk_id',$po_produk_id,$data);
            }
            $pembayaran = array();
            $pembayaran["status_pembayaran"] = $statusPembayaran;
            if(isset($result->settlement_time)){
                $pembayaran["tanggal_uang_diterima"] = $result->settlement_time;
            }
            $po_data = $this->po_produk->row_by_id($po_produk_id);
            $login = $this->db->where("lokasi_id",$po_data->lokasi_reseller)->get("mykindofbeauty_master.login")->row();
            $reseller_id = $login->reseller_id;
            $reseller = $this->reseller->row_by_id($reseller_id);
            $data_user = $this->master_login->row_by_field('reseller_id', $reseller_id);
            $username = $data_user->username;
            $referal_id = $reseller->referal_id;
            $deposit_anak = $this->po_produk->total_deposit_reseller($po_data->lokasi_reseller);
            $deposit_anak = $deposit_anak == "" ? 0 : $deposit_anak;
            $anak_full_payment = $this->fullpayment->is_fullpayment($reseller_id);
            $minimal_po_anak = $this->po_produk->minimal_po_cek($reseller_id);
            $indikator_anak = 0;
            if($deposit_anak<$minimal_po_anak){
                $indikator_anak = 1;
            }
            $login_induk = $this->db->where("reseller_id",$referal_id)->get("mykindofbeauty_master.login")->row();

            $lokasi_induk = $login_induk->lokasi_id;
            $deposit_induk = $this->po_produk->total_deposit_reseller($lokasi_induk);
            $deposit_induk = $deposit_induk == "" ? 0 : $deposit_induk;
            $induk_full_payment = $this->fullpayment->is_fullpayment($referal_id);
            $minimal_po_induk = $this->po_produk->minimal_po_cek($referal_id);
            $indikator_induk = 0;
            if($deposit_induk<$minimal_po_induk){
                $indikator_induk = 1;
            }
            $tanggal_rekap = date("Y-m-d");
            if($status=="success"){

                if($reseller->leader!=null){
                    $leader = $this->reseller->row_by_id($reseller->leader);
                    $currentTime = (date("Y-m-d"));
                    $lastDepositTime = ($leader->next_deposit_date);
                    if(($currentTime<=$lastDepositTime)){
                        $pembayaran['om_leader'] = $reseller->leader;
                    }
                }
                if($po_data->voucher_code==null){
                    $voucher_code = time() . $reseller_id . random_string('numeric', 5);
                }else{
                    $voucher_code = $po_data->voucher_code;
                }
                $voucher_text = '';
                $tes = null;
                $po_detail = $this->db->select('po_produk_detail.*,produk.produk_nama')->join('mykindofbeauty_kemiri.produk as produk','produk.produk_id = mykindofbeauty_kemiri.po_produk_detail.produk_id')->where('po_produk_id',$po_produk_id)->get('mykindofbeauty_kemiri.po_produk_detail')->result();
                if ( $reseller->type=="agent"){
                    $voucher_text = "";
                    foreach ($po_detail as $voucher_produk_detail){
                        $voucher_text = $voucher_text.'<tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Kode Voucher '.$voucher_produk_detail->produk_nama.'</td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$voucher_produk_detail->voucher_detail.'</td>
                                    </tr>';
                        $tes = $voucher_produk_detail->voucher_detail;
                    }

                }
                $referal_link = $this->config->item('url-landing').$reseller->referal_code;
                $message_tanda_terima = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 420;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h3 style="text-align: center;font-weight: 500">Tanda Terima Pembayaran</h3>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table width="100%">
                                <tbody>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">No PO </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$po_data->po_produk_no.'</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Tanggal </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.date("Y-m-d").'</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Kepada </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$reseller->nama.' ('.$username.')</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Total </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.number_format($po_data->grand_total).'</td>
                                    </tr>
                                    '.$voucher_text.'
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Mohon tanda terima pembayaran ini disimpan dengan baik sebagai bukti pembayaran yang valid.
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                       <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                       <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>
                </td>
            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>
</div>
</body></html>';
                $this->main->mailer_auth('Bukti Pembayaran', $reseller->email, $reseller->nama, $message_tanda_terima,$tes);
                $estimasi = $this->main->tanggal_kerja($paramBonus);
                $pembayaran['estimasi_pengiriman'] =$estimasi;

                $reseller_penerima = $this->reseller->row_by_id($referal_id);
                $data_user_penerima = $this->master_login->row_by_field('reseller_id', $referal_id);
                $username_penerima = $data_user_penerima->username;

                $resellerData["first_deposit"] = 1;
                $deposit = $this->po_produk->total_deposit_reseller($po_data->lokasi_reseller);
                $deposit = $deposit == "" ? 0 : $deposit;
                $minimal_po = $this->po_produk->minimal_po_cek($reseller_id);
                // $this->po_produk->rekap_bulanan($reseller_id,$tanggal_rekap,$lokasi_id,'"total_po"');

                if($reseller->first_deposit==0){
                    $first_deposit_date = new DateTime(date("Y-m-d"));
                    $money_date = new DateTime(date("Y-m-d"));
                    $resellerData["first_deposit_date"] = $first_deposit_date->format("Y-m-d");
                    $first_deposit_date->modify("+6 month");
                    $resellerData["first_limit_date"] = $first_deposit_date->format("Y-m-d");
                    $resellerData["next_limit_date"] = $first_deposit_date->format("Y-m-d");
                    $money_date->modify("+2 year");
                    $resellerData["money_back_guarantee"] = $money_date->format("Y-m-d");
                }
                $current_date = date("Y-m-d");
                $last_date = date("Y-m-25",strtotime($current_date));
                $custom_date = new DateTime($last_date);
                $paramDeposit = 2;
                if($reseller->type=="super agen"){
                    $bintang = $this->claim_reward->get_bintang($reseller_id);
                    if($bintang>2){
                        $paramDeposit = 1;
                    }
                }
                $custom_date->modify('+'.$paramDeposit.' month');
                $added_date = $custom_date->format('Y-m-d');
                $next_deposit_date = date("Y-m-25",strtotime($added_date));
                $resellerData["next_deposit_date"] = $next_deposit_date;
                $resellerData["status"] = "active";
                $resellerData["last_deposit"] = date("Y-m-d");
                $this->reseller->update_by_id("reseller_id",$reseller_id,$resellerData);
                $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
                $this->db->select("mykindofbeauty_kemiri.staff.*");
                $this->db->join("mykindofbeauty_kemiri.staff","mykindofbeauty_master.login.user_staff_id = mykindofbeauty_kemiri.staff.staff_id");
                $this->db->where("mykindofbeauty_master.login.company_id",3);
                $this->db->where("mykindofbeauty_master.login.user_role_id",2);
                $login = $this->db->get("mykindofbeauty_master.login")->result();
                $reseller_bank = $this->db->where("bank_id",$reseller->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
                if ( $reseller->type=="super agen"){
                } else {
                    $po["status_penerimaan"] = "Diterima";
                    $po["tanggal_penerimaan"] = date("Y-m-d");
                    $po['lokasi_penerimaan_id'] = $po_data->lokasi_reseller;
                    $this->po_produk->update_by_id("po_produk_id", $po_produk_id, $po);
                    foreach ($po_detail as $itemPoDetail){
//                        $checkVoucher = $this->voucher_produk->cekVoucher($po_produk_id, $reseller_id);
                        if ($result->payment_type!="credit_card"){
                            $voucher = array();
                            $voucher["reseller_pemilik"] = $reseller_id;
                            $voucher["voucher_code"] = $itemPoDetail->voucher_detail==null?$po_produk->voucher_produk:$itemPoDetail->voucher_detail;
                            $voucher["po_produk_id"] = $po_produk_id;
                            $voucher["po_produk_detail_id"] = $itemPoDetail->po_produk_detail_id;
                            $voucher["created_at"] = date("Y-m-d H:i:s");
                            $this->db->insert('mykindofbeauty_kemiri.voucher_produk',$voucher);
                        }
                    }

                }
                $withdraw_cash = $reseller->withdraw;
                $sisa_deposit_withdraw = ($reseller->sisa_deposit_withdraw-$po_data->grand_total) <= 0 ? 0 : abs($reseller->sisa_deposit_withdraw-$po_data->grand_total);
                $loyalti_withdraw = ($reseller->loyalti_withdraw-$po_data->grand_total) <= 0 ? 0 : abs($reseller->loyalti_withdraw-$po_data->grand_total);
                $sisa = $withdraw_cash - $po_data->grand_total;
                $sisa_deposit = $sisa <= 0 ? abs($sisa) : 0;

                $withdraw_reseller = $sisa <= 0 ? 0 : abs($sisa);
                $resellerUpdate = array();
                $resellerUpdate['withdraw'] = $withdraw_reseller;
                $resellerUpdate['sisa_deposit_withdraw'] = $sisa_deposit_withdraw;
                $resellerUpdate['loyalti_withdraw'] = $loyalti_withdraw;
                $this->reseller->update_by_id('reseller_id',$reseller_id,$resellerUpdate);

                if($referal_id!=null){

                    // $this->po_produk->rekap_bulanan($reseller_id,$tanggal_rekap,$lokasi_id,'"omzet"');
                    //
                    $withdraw_penerima = $reseller_penerima->sisa_deposit_withdraw;
                    $sisa_penerima = $withdraw_penerima - $po_data->grand_total;
                    $withdraw_penerima_ganti = $sisa_penerima <= 0 ? 0 : abs($sisa_penerima);
                    $resellerUpdate = array();
                    $resellerUpdate['sisa_deposit_withdraw'] = $withdraw_penerima_ganti;
                    $this->reseller->update_by_id('reseller_id',$referal_id,$resellerUpdate);

                    //
                    if($withdraw_reseller>0){
                        $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                        $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Pemberitahuan</h3>
                        <span>Super Reseller referensi anda atas nama '.$reseller->nama.' melakukan redeposit dengan detail sebagai berikut : 
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Super Reseller </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . ' </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->po_total) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Total Withdraw Cash </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($withdraw_cash) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Sisa Withdraw Cash </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($withdraw_reseller) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span style="color: red">
                          Note : anda belum bisa mendapakat bonus referensi dari '.$reseller_penerima->nama.' ('.$username_penerima.') sebelum super reseller tersebut melakukan redeposit sejumlah total sisa withdraw cashnya.
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                        $this->main->mailer_auth('Pemberitahuan Deposit Reseller Referensi', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                    }
                    if($sisa_deposit>0){
                        if(!($reseller_penerima->type=="agent" && $reseller->type=="super agen")){
                            if($reseller_penerima->type=="super agen" && $reseller->type=="super agen"){
                                if($this->bonus->cekBonus($po_produk_id,$reseller_id,"loyalty bonus")) {
//                                $status_bonus_reseller_loyal = (($deposit_induk < $minimal_po_induk)||$withdraw_penerima>0) ? "Hold" : "On Process";
                                    $status_bonus_reseller_loyal = (!$induk_full_payment||$withdraw_penerima>0) ? "Hold" : "On Process";
                                    $bonus = array();
                                    $bonus["tanggal"] = $estimasiBonus;
                                    $bonus["from_reseller_id"] = $reseller->reseller_id;
                                    $bonus["type"] = "loyalty bonus";
                                    $bonus["jumlah_deposit"] = $sisa_deposit;
                                    $bonus["persentase"] = 5;
                                    $bonus["jumlah_bonus"] = $sisa_deposit * 0.05;
                                    $bonus["to_reseller_id"] = $referal_id;
                                    $bonus["po_produk_id"] = $po_produk_id;
                                    $bonus["status"] = $status_bonus_reseller_loyal;

                                    $kelebihan = $reseller_penerima->kelebihan;
                                    $potongan = 0;
                                    $text_kelebihan = '';
                                    if($kelebihan>0){
                                        if($kelebihan<$bonus["jumlah_bonus"]){
                                            $bonus["potongan"] = $kelebihan;
                                            $potongan = $kelebihan;
                                            $bonus["jumlah_bonus"] = $bonus["jumlah_bonus"] - $kelebihan;
                                            $kelebihan = 0;
                                        }else{
                                            $bonus["potongan"] = $bonus["jumlah_bonus"];
                                            $potongan = $bonus["jumlah_bonus"];
                                            $kelebihan = $kelebihan - $bonus["jumlah_bonus"];
                                            $bonus["jumlah_bonus"] = 0;

                                        }
                                        $total_bonus = $bonus["jumlah_bonus"];
                                        $text_kelebihan = '<tr style="height: 25px">
                                                            <td style="text-align: left">Potongan </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($potongan). ' </td>
                                                        </tr>
                                                        <tr style="height: 25px">
                                                            <td style="text-align: left">Total Bonus Didapat </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($total_bonus). ' </td>
                                                        </tr>';
                                    }
                                    $this->bonus->insert($bonus);

                                    $dataKelebihan['kelebihan'] = $kelebihan;
                                    $this->reseller->update_by_id('reseller_id',$reseller_penerima->reseller_id,$dataKelebihan);
                                    $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                                    if (!$induk_full_payment){
                                        $status_message = "Bonus anda akan di hold karena anda belum memenuhi minimal deposit. Silakan lakukan deposit untuk mencairkan bonus.";
                                    }
                                    $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                                    $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS LOYALTI yang akan langsung di transfer ke rekening MKB Anda.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Pemilik Voucher </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah/Sisa Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">5%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit * 0.05) . '</td>
                                    </tr>
                                    '.$text_kelebihan.'
                                </tbody>
                            </table>
                        </div>
                        <span>
                           '.$status_message.'
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                                    $this->main->mailer_auth('Bonus Loyalti', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                                }
                            }
                            if($this->bonus->cekBonus($po_produk_id,$referal_id)) {
                                $bonus = array();
                                $bank = $this->db->where("bank_id", $reseller_penerima->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
//                            $status_bonus_reseller = (($indikator_induk == 1 && $reseller_penerima->type == "agent")||$withdraw_penerima>0) ? "Hold" : "On Process";
                                $status_bonus_reseller = ((!$induk_full_payment && $reseller_penerima->type == "agent")||$withdraw_penerima>0) ? "Hold" : "On Process";
                                $bonus["tanggal"] = $estimasiBonus;
                                $bonus["from_reseller_id"] = $reseller_id;
                                $bonus["type"] = "referal bonus";
                                $bonus["jumlah_deposit"] = $sisa_deposit;
                                $bonus["persentase"] = 10;
                                $bonus["jumlah_bonus"] = $sisa_deposit * 0.1;
                                $bonus["status"] = $status_bonus_reseller;
                                $bonus["to_reseller_id"] = $referal_id;
                                $bonus["po_produk_id"] = $po_produk_id;
                                $kelebihan = $reseller_penerima->kelebihan;
                                $potongan = 0;
                                $text_kelebihan = '';
                                if($kelebihan>0){
                                    if($kelebihan<$bonus["jumlah_bonus"]){
                                        $bonus["potongan"] = $kelebihan;
                                        $potongan = $kelebihan;
                                        $bonus["jumlah_bonus"] = $bonus["jumlah_bonus"] - $kelebihan;
                                        $kelebihan = 0;
                                    }else{
                                        $bonus["potongan"] = $bonus["jumlah_bonus"];
                                        $potongan = $bonus["jumlah_bonus"];
                                        $kelebihan = $kelebihan - $bonus["jumlah_bonus"];
                                        $bonus["jumlah_bonus"] = 0;

                                    }
                                    $total_bonus = $bonus["jumlah_bonus"];
                                    $text_kelebihan = '<tr style="height: 25px">
                                                            <td style="text-align: left">Potongan </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($potongan). ' </td>
                                                        </tr>
                                                        <tr style="height: 25px">
                                                            <td style="text-align: left">Total Bonus Didapat </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($total_bonus). ' </td>
                                                        </tr>';
                                }

                                $this->bonus->insert($bonus);
                                $dataKelebihan['kelebihan'] = $kelebihan;
                                $this->reseller->update_by_id('reseller_id',$reseller_penerima->reseller_id,$dataKelebihan);
                                $status_bonus = "On Process";
                                $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                                $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                                //here
                                if (!$induk_full_payment && $reseller_penerima->type == "agent") {
                                    $status_bonus = "Hold";
                                    $status_message = "Bonus anda akan di hold karena akun anda termasuk kedalam akun frezze. Silakan lakukan deposit untuk mencairkan bonus.";
                                }
                                $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 500px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS REFERENSI dari pendaftaran Reseller/Super Reseller MKB a/n ' . $reseller->nama . '
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah/Sisa Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">10%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit * 0.1) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Status </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $status_bonus . '</td>
                                    </tr>
                                    '.$text_kelebihan.'
                                </tbody>
                            </table>
                        </div>
                        <span>
                            ' . $status_message . '
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                     <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                                $this->main->mailer_auth('Bonus Referensi', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                            }
                        }
                    }

                }
                if((($deposit+$po_produk_item[0]->po_total)>=$minimal_po_anak)&&($withdraw_reseller==0)){
                    $this->bonus->change_hold_bonus($reseller_id,$estimasiBonus);
                }
                if($reseller_id->is_pass_first_limit==1){
                    $next_date = new DateTime(date("Y-m-d"));
                    $next_date->modify('+1 month');
                    $next_date_label = $next_date->format("Y-m-d");
                    $next_data = array();
                    $next_data['next_limit_date'] = $next_date_label;
                    $next_data['skip_deposit_count'] = 0;
                    $next_data['is_frezze'] = 0;
                    $this->reseller->update_by_id('reseller_id',$reseller_id,$next_data);
                }
            }
            $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$pembayaran);
            $this->rekap_bulanan->get_redeposit($reseller_id,$po_data->grand_total,$po_produk_id);
            if($reseller->leader!=null){
                if($reseller->reseller_id == $reseller->leader){
                    $this->rekap_bulanan->total_po($reseller->leader,$tanggal_rekap);
                } else {
                    $this->rekap_bulanan->total_omzet_reseller($reseller->leader,$tanggal_rekap);
                }
            }

        }
        error_log(print_r($result,TRUE));
    }

}

/* End of file ProdukByLocation.php */
/* Location: ./application/controllers/ProdukByLocation.php */