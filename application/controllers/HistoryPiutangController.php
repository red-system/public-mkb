<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class HistoryPiutangController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('piutang','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/piutang.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Piutang < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = $this->uri->segment(1);
        $target = array(0,3,4,5);
        $sumColumn = array(3,4,5);

        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama_pelanggan"));
        array_push($column, array("data"=>"no_faktur"));
        array_push($column, array("data"=>"grand_total"));
        array_push($column, array("data"=>"terbayar"));
        array_push($column, array("data"=>"sisa"));
        array_push($column, array("data"=>"status_piutang"));
        array_push($column, array("data"=>"tenggat_pelunasan"));
        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/histori_piutang');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->piutang->histori_piutang_count_all();
        $result['iTotalDisplayRecords'] = $this->piutang->histori_piutang_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->piutang->histori_piutang_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->piutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'piutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
            $key->posting_url = base_url().'piutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }


}

/* End of file HistoryTransferBahanController.php */
/* Location: ./application/controllers/HistoryTransferBahanController.php */