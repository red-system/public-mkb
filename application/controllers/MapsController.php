<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/phpseclib/Net/SFTP.php';

class MapsController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user', '', true);
        $this->load->model('master_login', '', true);
        $this->load->model('staff', '', true);
        $this->load->model('reseller', '', true);
        $this->load->helper('string');
        $this->load->library('upload');
        $this->load->library('image_lib');
    }

    public function index()
    {
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/general/sweetalert2/dist/sweetalert2.min.js");
        array_push($this->js, "vendors/custom/components/vendors/sweetalert2/init.js");
        array_push($this->js, "script/admin/maps.js");
        array_push($this->js, "script/admin/gmaps_custom.js");
        array_push($this->external_js, "https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCdIEp0TwCHgZBZWFqZ7NTjvPLrsobEKUU&callback=initMap");
        array_push($this->css, "vendors/general/sweetalert2/dist/sweetalert2.css");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data["external_js"] = $this->external_js;
        $user = $this->master_login->user_by_id($_SESSION["redpos_login"]['user_id']);
        $data['user'] = $user;
        $login = $this->db->where("lokasi_id", @$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();

        $reseller_id = $login->reseller_id;

        $reseller = $this->reseller->row_by_id($reseller_id);
        $data['alamat_outlet'] = $reseller->outlet_alamat;
        $data['reseller_id'] = $reseller_id;
        $data['old_lat'] = ($reseller->latitude == null) ? '-' : $reseller->latitude;
        $data['old_lon'] = ($reseller->longitude == null) ? '-' : $reseller->longitude;
        $data['reseller_id'] = $reseller_id;
        
        $data['reseller'] = $reseller;
        $data["meta_title"] = "User Profile < " . $_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "";
        $data['page'] = $this->uri->segment(1);

        $this->load->view('admin/static/header', $data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/maps');
        $this->load->view('admin/static/footer');

    }

    function update_koordinat()
    {

        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['latitude'] = $this->input->post('lat_outlet');
        $data['longitude'] = $this->input->post('lon_outlet');
        $reseller_id = $this->input->post('reseller_id');
        
        $update = $this->reseller->update_by_id('reseller_id',$reseller_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
        
        // $this->form_validation->set_rules('lat_outlet', '', 'required');
        // $this->form_validation->set_rules('lon_outlet', '', 'required');
        // $result['success'] = false;
        // $result['message'] = "missing parameter";
        // if ($this->form_validation->run() == TRUE) {
        //     $lat_outlet = $this->input->post('lat_outlet');
        //     $lon_outlet = $this->input->post('lon_outlet');
        //     $reseller_id = $this->input->post('reseller_id');
            
        //     $data = array(
        //         "latitude" => $lat_outlet,
        //         "longitude" => $lon_outlet,
        //     );

        //     $update = $this->reseller->update_by_id("reseller_id", $reseller_id, $data);

        //     if ($update) {
        //         $result['success'] = true;
        //         $result['message'] = "Sukses update koordinat outlet";
        //     }

        // }
        // echo json_encode($result);
    }

}