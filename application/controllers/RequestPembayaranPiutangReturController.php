<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestPembayaranPiutangReturController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembayaran_hutang_retur','',true);
    }
    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/request_pem_retur.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Request Pembayaran Piutang Produk< ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "request-pembayaran-retur";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"status"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['request-pembayaran-retur'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/request-pembayaran-piutang-retur/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->pembayaran_hutang_retur->_all_res($reseller_id);
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_retur->_filter_res($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_retur->_list_res($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            $key->edit_url = base_url().'request-pembayaran-piutang-retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_retur_id));
            $key->no = $i;
            $i++;
            if($key->status!="preparing"){
                $key->deny_edit = true;
                $key->deny_delete = true;
                $key->deny_submit = true;
            }
            $key->print_url = base_url()."request-pembayaran-piutang-retur/print/".$key->pembayaran_hutang_retur_id;
            $key->delete_url = base_url().'request-pembayaran-piutang-retur/delete/';
            $key->row_id = $key->pembayaran_hutang_retur_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/admin/request_pem_retur.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Request Pembayaran Piutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "request-pembayaran-retur";

        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/request-pembayaran-piutang-retur/add_pembayaran');
        $this->load->view('admin/static/footer');
        unset($_SESSION['po_produk']);
        $_SESSION['po_produk']['lokasi'] = 1;
    }
    function list_hutang(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->pembayaran_hutang_retur->_hutang_all_res($reseller_id);
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_retur->_hutang_filter_res($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_retur->_hutang_list_res($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pelunasan != null){
                $time = strtotime($key->tanggal_pelunasan);
                $key->tanggal_pelunasan = date('d-m-Y',$time);
            }

            $key->row_id = $key->hutang_retur_id;
            $key->jumlah = number_format($key->jumlah);
            $key->no = $i;

            $key->action = null;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function submit(){
        $result["success"] = false;
        $result["message"] = "Gagal menyimpan data";
        $pembayaran['status'] = 'waiting';
        $pembayaran_hutang_produk_id = $this->input->post('pembayaran_hutang_produk_id');
        $edit = $this->pembayaran_hutang_retur->update_by_id('pembayaran_hutang_retur_id',$pembayaran_hutang_produk_id,$pembayaran);
        if($edit){
            $result["success"] = true;
            $result["message"] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function save_add(){
        $result["success"] = false;
        $result["message"] = "Gagal menyimpan data";
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $insert = $this->pembayaran_hutang_retur->insert_data($reseller_id);
        if($insert){
            $result["success"] = true;
            $result["message"] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function save_start(){

    }
    function edit(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $pembayaran_hutang_produk = $this->pembayaran_hutang_retur->row_by_id($id);

        unset($_SESSION['po_produk']);
        if ($pembayaran_hutang_produk != null) {
            $pembayaran_hutang_produk_detail  = $this->pembayaran_hutang_retur->pembayaran_hutang_retur_detail($id);
            $data["pembayaran_hutang_produk_detail"] = $pembayaran_hutang_produk_detail;
            $data["size"] = sizeof($pembayaran_hutang_produk_detail);
            $_SESSION['po_produk']["reseller_id"] = $pembayaran_hutang_produk_detail[0]->reseller_id;
            $_SESSION['po_produk']["pembayaran_hutang_produk"] = $id;
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "script/admin/request_pem_retur.js");
            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
            $data['parrent'] = "hutang_piutang";
            $data['page'] = "request-pembayaran-piutang";

            $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
            $reseller_id = $login->reseller_id;
            $reseller = $this->reseller->row_by_id($reseller_id);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/request-pembayaran-piutang-retur/edit_pembayaran');
            $this->load->view('admin/static/footer');
        }else {
            redirect('404_override','refresh');
        }
    }
    function detail(){

    }
    function save_edit(){
        $result["success"] = false;
        $result["message"] = "Gagal menyimpan data";
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $edit = $this->pembayaran_hutang_retur->edit_data($reseller_id);
        if($edit){
            $result["success"] = true;
            $result["message"] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->pembayaran_hutang_retur->delete_by_id("pembayaran_hutang_retur_id",$id);
            $this->pembayaran_hutang_retur->setPembayaranNull($id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function print(){

    }

}