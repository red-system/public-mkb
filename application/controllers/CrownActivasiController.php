<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class CrownActivasiController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
        $this->load->helper('string');

    }
    public function index()
    {
//        $headers = apache_request_headers();
//        if ($headers['Authentication'] != 'Mj%Ph%M!-q_W6Huh82P@dqvM_zG*racs' && !is_cli()) {
//            redirect('404_override');
//        } else {
////            $current_time = date("Y-m-d H:i:s");
////            $this->db->where('mykindofbeauty_master.login.active',0);
////            $this->db->where('mykindofbeauty_master.login.expired_time < ',$current_time);
////            $list = $this->db->get('mykindofbeauty_master.login')->result();
////            if($list != null){
////                foreach ($list as $item){
////                    $lokasi_id = $item->lokasi_id;
////                    $reseler_id = $item->reseller_id;
////                    $login_id = $item->id;
////
////                    $this->db->where('mykindofbeauty_master.login.id',$login_id);
////                    $this->db->delete('mykindofbeauty_master.login');
////
////                    $this->db->where('mykindofbeauty_kemiri.reseller.reseller_id',$reseler_id);
////                    $this->db->delete('mykindofbeauty_kemiri.reseller');
////
////                    $this->db->where('mykindofbeauty_kemiri.lokasi.lokasi_id',$lokasi_id);
////                    $this->db->delete('mykindofbeauty_kemiri.lokasi');
////                }
////            }
//            $data['waktu'] = date("Y-m-d");
//
//            $this->db->insert('testCron',$data);
//        }
        echo 123;
        $data['waktu'] = date("Y-m-d H:i:s");

        $this->db->insert('mykindofbeauty_kemiri.testCron',$data);

    }
    public function cron2()
    {
        $data['waktu'] = date("Y-m-d H:i:s");
        $data['description'] = 'Sample waktu pasti';
        $this->db->insert('mykindofbeauty_kemiri.testCron',$data);

    }
    public function warningDeposit(){
        

        $this->db->select('reseller.*,datediff(next_deposit_date,CURRENT_DATE()) as selisih');
        $this->db->where('type','super agen');
        $this->db->where('demo','0');
        $this->db->where('status','active');
        $this->db->having('selisih >',-1);
        $this->db->having('selisih <',8);
        $data = $this->db->get('mykindofbeauty_kemiri.reseller')->result();
        $count = 0;
        foreach ($data as $reseller){
            $warning_redeposit = true;
            $date1 = date_create(date("Y-m-d"));
            $date2 = date_create($reseller->next_deposit_date);
            $diff=date_diff($date1,$date2);
            $sisa_tanggal = $diff->format("%R")=="-"?$diff->format("%R%a"):$diff->format("%a");
            $warning_message = "Halooo Kak ".$reseller->nama.", ".$sisa_tanggal." hari lagi loh waktu untuk redeposit. Ayo segera sebelum
                    omsetnya hangus";
            if(($sisa_tanggal > 0) && ($sisa_tanggal <=7)){
                $warning_message = "Halooo Kak ".$reseller->nama.", ".$sisa_tanggal." hari lagi loh waktu untuk redeposit. Ayo segera sebelum
                    omsetnya hangus";
            } else if($sisa_tanggal == 0){
                $warning_message = "Halooo Kak ".$reseller->nama.",  hari ini terakhir waktu untuk redeposit. Ayo segera sebelum
                    omsetnya hangus";
            } else if ($sisa_tanggal < 0){
                $warning_message = "Halooo Kak ".$reseller->nama.", karena kakak belum melakukan deposit di tanggal ".$reseller->next_deposit_date." atau sebelumnya, maka omset kakak yang sebelumnya dihanguskan ";
            }else {
                $warning_redeposit = false;
            }
            $referal_link = $this->config->item('url-landing').$reseller->referal_code;
            $message_tanda_terima = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h3 style="text-align: center;font-weight: 500">PEMBERITAHUAN</h3>
                        <div style="width: 100%;margin-top: 30px;margin-bottom: 30px">
                            <p>'.$warning_message.'</p>
                        </div>
                       
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                       <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                       <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>
                </td>
            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>
</div>
</body></html>';
            $this->main->mailer_auth('Pemberitahuan Batas Deposit', $reseller->email, $reseller->nama, $message_tanda_terima);
            $count++;
        }
        $dataCron['waktu'] = date("Y-m-d H:i:s");
        $dataCron['description'] = 'cron send email warning jumlah looping '.$count;
        $this->db->insert('mykindofbeauty_kemiri.testCron',$dataCron);

    }
    public function omset_hangus(){
        $date = date("Y-m-d",strtotime(date("Y-m-d")." -1 days"));
        $this->load->model("po_produk",'',true);
        $this->load->model("reseller",'',true);
        $this->load->model("rekap_bulanan",'',true);
        $this->load->model("omset_hangus",'',true);
        $reseller = $this->reseller->list_reseller_hangus($date);
        $startDate = date("Y-m-d 00:00:00",strtotime($date." -2 months"));
        foreach ($reseller as $key) {
//            cek_tanggal po terakhir
            $lokasi_id = $key->lokasi_id;
            $reseller_id = $key->reseller_id;
            $last_po_date = $this->po_produk->get_reseller_last_po($reseller_id)->tanggal;
            $date_limit = date("Y-m-d 00:00:00");
            $date_start = $startDate>$last_po_date?$startDate:$last_po_date;

            // cek total omset reseller dari tanggal po terakhir
            $total_omset = $this->po_produk->total_omset_reseller_date_range($reseller_id,$date_start,$date_limit)->total;

            // sisa omset group
            $this->load->model('claim_reward','',true);
            $curMonth = number_format(date('m'));
            $waktu_end = date("Y-m-t");
            $first_date = date("Y-m-01");
            $datetime_start = new DateTime($first_date);
            $datetime_start->modify("-2 month");
            $waktu_start = $datetime_start->format("Y-m-d");
            $base = $this->claim_reward->cek_base($reseller_id);
            if($base==0){
                if($this->po_produk->is_deposit_in_april($lokasi_id)){
                    $waktu_start = $this->config->item('reward-start-if-deposit-april');
                } else{
                    $waktu_start = $this->config->item('start-count-reward');
                }
            }else{
                if($this->po_produk->is_deposit_in_april($lokasi_id)){
                    $tanggal_claim = $this->claim_reward->get_base_date($reseller_id);

                    $datetime_start = new DateTime(date("Y-m-01",strtotime($tanggal_claim)));
                    if($tanggal_claim<=$this->config->item('old-reward-date')){
                        $waktu_start = '2021-07-01';
                    }else{
                        $datetime_start->modify("-2 month");
                        $waktu_start = $datetime_start->format("Y-m-d");
                    }
                } else{
                    $waktu_start = $this->config->item('start-count-reward');
                }
            }
            $omset_group = $this->rekap_bulanan->omset_group($reseller_id,$waktu_start,$waktu_end);

            // perbandingan omset group dan omset reseller
            $omset_hangus = $omset_group>$total_omset?$total_omset:$omset_group;
            $data_hangus = array();
            $data_hangus['reseller_id'] = $reseller_id;
            $data_hangus['tanggal'] = date("Y-m-d");
            $data_hangus['jumlah'] = $omset_hangus;
            $this->omset_hangus->insert($data_hangus);
        }
        $dataCron['waktu'] = date("Y-m-d H:i:s");
        $dataCron['description'] = "omset hangus";
        $this->db->insert('mykindofbeauty_kemiri.testCron',$dataCron);

    }

}

/* End of file ProdukByLocation.php */
/* Location: ./application/controllers/ProdukByLocation.php */