<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LokasiController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('lokasi','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Lokasi < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"lokasi_kode"));
		array_push($column, array("data"=>"lokasi_nama"));
		array_push($column, array("data"=>"lokasi_alamat"));
		array_push($column, array("data"=>"lokasi_tipe"));
		array_push($column, array("data"=>"lokasi_telepon"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['lokasi'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/lokasi');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->lokasi->lokasi_count_all();
		$result['iTotalDisplayRecords'] = $this->lokasi->lokasi_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->lokasi->lokasi_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'lokasi/delete/';
			$key->row_id = $key->lokasi_id;
		}
		$result['aaData'] = $data;	
		echo json_encode($result);		
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
		$this->form_validation->set_rules('lokasi_kode', '', 'required|is_unique[lokasi.lokasi_kode]');
        $current_sum = $this->lokasi->lokasi_count_all();
        if($current_sum>=$_SESSION["redpos_company"]["max_outlet"]){
            $result['message'] = "Sudah mencapai batas maksimal user";
        }else{
            if ($this->form_validation->run() == TRUE) {
                $lokasi_kode = $this->input->post('lokasi_kode');
                $lokasi_nama = $this->input->post('lokasi_nama');
                $lokasi_tipe = $this->input->post('lokasi_tipe');
                $lokasi_alamat = $this->input->post('lokasi_alamat');
                $lokasi_telepon = $this->input->post('lokasi_telepon');
                $data = array("lokasi_kode"=>$lokasi_kode,"lokasi_nama"=>$lokasi_nama,"lokasi_tipe"=>$lokasi_tipe,"lokasi_alamat"=>$lokasi_alamat,"lokasi_telepon"=>$lokasi_telepon);
                $insert = $this->lokasi->insert($data);
                if($insert){
                    $result['success'] = true;
                    $result['message'] = "Data berhasil disimpan";
                } else {
                    $result['message'] = "Gagal menyimpan data";
                }
            }
        }
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["lokasi_nama"] = $this->input->post('lokasi_nama');
		$data["lokasi_tipe"] = $this->input->post('lokasi_tipe');
		$data["lokasi_alamat"] = $this->input->post('lokasi_alamat');
		$data["lokasi_telepon"] = $this->input->post('lokasi_telepon');
		$lokasi_id = $this->input->post('lokasi_id');

		if ($this->lokasi->is_ready_kode($lokasi_id,$this->input->post('lokasi_kode'))){
			$data['lokasi_kode'] = $this->input->post('lokasi_kode');
			$update = $this->lokasi->update_by_id('lokasi_id',$lokasi_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->lokasi->delete_by_id("lokasi_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file Lokasi.php */
/* Location: ./application/controllers/Lokasi.php */