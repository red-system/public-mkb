<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManagementRewardController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('claim_reward','',true);
        $this->load->model('pengiriman_reward','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');
    }
    function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/management-reward.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Management Reward < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = "pembayaran-withdraw-cash";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"reward_nama"));
        array_push($column, array("data"=>"tanggal_claim"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hadiah']['management-reward'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $action['approve'] = true;
        $action['refuse'] = true;
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-reward/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->claim_reward->request_all();
        $result['iTotalDisplayRecords'] = $this->claim_reward->request_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->claim_reward->request_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->reward_nama = "Reward ".$key->rank_id;
            $key->row_id = $key->claim_reward_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function approve(){
        $claim_reward_id = $this->input->post('claim_reward_id');
        $data_claim_reward = array();
        $data_claim_reward['status'] = 'approve';
        $this->claim_reward->update_by_id('claim_reward_id',$claim_reward_id,$data_claim_reward);

        $claim_reward = $this->claim_reward->row_by_id($claim_reward_id);
        $reward_id = $claim_reward->reward_id;
        $this->load->model('reward');
        $reward = $this->reward->row_by_id($reward_id);

        $data_pengiriman_claim_reward = array();
        $data_pengiriman_claim_reward['claim_reward_id'] = $claim_reward_id;
        $data_pengiriman_claim_reward['tanggal'] = date('Y-m-d');
        $data_pengiriman_claim_reward['jumlah'] = $reward->budget_reward;
        $data_pengiriman_claim_reward['to_reseller_id'] = $claim_reward->reseller_id;

        $this->load->model('pengiriman_reward_reseller','',true);
        $insert = $this->pengiriman_reward_reseller->insert($data_pengiriman_claim_reward);
        $result['success'] = false;
        $result['message'] = 'Gagal memproses data';
        if($insert){
            $result['success'] = true;
            $result['message'] = 'Berhasil memproses data';
        }
        echo json_encode($result);
    }
    function refuse(){

    }
    function approve_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Management Reward < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"reward_nama"));
        array_push($column, array("data"=>"tanggal_claim"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-reward/index_approve');
        $this->load->view('admin/static/footer');
    }
    function approve_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->claim_reward->approve_all();
        $result['iTotalDisplayRecords'] = $this->claim_reward->approve_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->claim_reward->approve_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->reward_nama = "Reward ".$key->rank_id;
            $key->row_id = $key->claim_reward_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function refuse_page(){

    }
    function refuse_list(){

    }
}