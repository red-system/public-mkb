<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperResellerController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reseller','',true);
        $this->load->library('main');

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $jumlah = $this->reseller->super_maps();
        $data['empty_maps'] = $jumlah;
        $column = array();
        $data["meta_title"] = "Prospective Reseller < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"phone"));
        array_push($column, array("data"=>"email"));
        array_push($column, array("data"=>"alamat"));
        array_push($column, array("data"=>"outlet_subdistrict_name"));
        array_push($column, array("data"=>"outlet_alamat"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/partner/superreseller');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "active";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->link_foto_profile = $this->config->item('dev_storage_image').$key->foto_profile;
            $key->link_foto_ktp = $this->config->item('dev_storage_image').$key->foto_ktp;
            $key->link_selfi_ktp = $this->config->item('dev_storage_image').$key->selfi_ktp;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */