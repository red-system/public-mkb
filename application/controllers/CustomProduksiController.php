<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomProduksiController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produksi','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('custom_produksi','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/produksi_custom.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Custom Produksi < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "produksi";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produksi_kode"));
		array_push($column, array("data"=>"jumlah_pesan"));
		array_push($column, array("data"=>"tanggal_mulai"));
		array_push($column, array("data"=>"estimasi_selesai"));
		array_push($column, array("data"=>"status_produksi"));
		array_push($column, array("data"=>"tanggal_selesai"));
		array_push($column, array("data"=>"deskripsi"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['produksi']['custom-produksi'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$data['produksi_kode'] = $this->produksi->get_kode_produksi();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/custom_produksi');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->custom_produksi->produksi_custom_count_all();
		$result['iTotalDisplayRecords'] = $this->custom_produksi->produksi_custom_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->custom_produksi->produksi_custom_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);

			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_mulai != null){
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y',$time);
			}
			if($key->tanggal_selesai != null){
				$time = strtotime($key->tanggal_selesai);
				$key->tanggal_selesai = date('d-m-Y',$time);
			}
			if($key->estimasi_selesai != null){
				$time = strtotime($key->estimasi_selesai);
				$key->estimasi_selesai = date('d-m-Y',$time);
			}
			if($key->status_produksi == "waiting"){
				$key->deny_edit = true;
				$key->deny_delete = true;
				$key->start_produksi_btn = true;
			} else if ($key->status_produksi == "progress") {
				$key->produksi_status_btn = true;
			} else {
				$key->deny_edit = true;
				$key->deny_delete = true;
				$key->penerimaan_status_btn = true;
			}
			$key->edit_url = base_url().'custom-produksi/start/'.$this->encrypt_id($key->produk_custom_id);
			$key->start_url = base_url().'custom-produksi/start/'.$this->encrypt_id($key->produk_custom_id);
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produksi/delete/';
			$key->row_id = $key->produk_custom_id;
			$key->produksi_kode = "PC-".$key->produk_custom_id;
			$key->action = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
	function start(){
		$id = $this->dencrypt_id($this->uri->segment(3));
		$produksi_custom = $this->custom_produksi->row_by_id($id);
		if ($produksi_custom != null) {
			unset($_SESSION['custom-produksi']);
			$_SESSION['custom-produksi']['lokasi'] = $produksi_custom->lokasi_bahan_id == null ? 1 : $produksi_custom->lokasi_bahan_id;
			$data['lokasi_bahan_id'] = $produksi_custom->lokasi_bahan_id == null ? "" : $produksi_custom->lokasi_bahan_id;;
			$data['no_produk'] = 0;
			$data['no_bahan'] = 0;
			$produksi_custom->tanggal_mulai = $produksi_custom->tanggal_mulai == null ? "" : $this->date_database_format($produksi_custom->tanggal_mulai);
			$produksi_custom->estimasi_selesai = $produksi_custom->estimasi_selesai == null ? "" : $this->date_database_format($produksi_custom->estimasi_selesai);
			$data['produk_custom'] = $produksi_custom;
			$data['id'] = $id;
			$item = $this->custom_produksi->produksi_custom_bahan_by_id($id);
			$i = 1;
			$temp = array();
			foreach ($item as $key){
				$_SESSION['con_'.$i] = array("bahan_id"=>$key->bahan_id,"value"=>$key->jumlah);
				$temp["bahan_".$i] = array("bahan_id"=>$key->bahan_id,"jumlah"=>$key->jumlah,"nama"=>$key->bahan_nama);
				$data['no_bahan'] = $i;
				$i++;
			}
			$data['item'] = $temp;
			array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
			array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
			array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
			array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
			array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
			array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
			array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			array_push($this->js, "script/admin/produksi_custom.js");
			$data['lokasi'] = $this->lokasi->all_list();
			$data["css"] = $this->css;
			$data["js"] = $this->js;
			$column = array();
			$data["meta_title"] = "Produksi Custom< ".$_SESSION["redpos_company"]['company_name'];;
			$data['parrent'] = "produksi";
			$data['page'] = $this->uri->segment(1);
			$data['produksi_kode'] = $this->produksi->get_kode_produksi();
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/start_custom_produksi');
			$this->load->view('admin/static/footer');
		}else {

			redirect('404_override','refresh');
		}
	}
	function save_start(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$save = $this->custom_produksi->produksi_custom_save();
		if($save){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}
		echo json_encode($result);
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key=="list-bahan"){
			$this->list_bahan();
		}
		if($key=="sess-bahan-add"){
			$no = $this->input->post('key');
			$bahan_id = $this->input->post('bahan_id');
			$_SESSION['custom-produksi']['bahan']['con_'.$no]['bahan_id'] = $bahan_id;
			$_SESSION['custom-produksi']['bahan']['con_'.$no]['value'] = 0;
			var_dump($_SESSION['custom-produksi']);
		}
		if($key=="sess-bahan-change"){
			$jumlah = $this->input->post('jumlah');
			$no = $this->input->post('key');
			if(isset($_SESSION['custom-produksi']['bahan']['con_'.$no]['bahan_id'])){
				$_SESSION['custom-produksi']['bahan']['con_'.$no]['value'] = $jumlah;
				var_dump($_SESSION['custom-produksi']);
			}
		}
		if($key=="sess-bahan-delete"){
			$no = $this->input->post('key');
			unset($_SESSION['custom-produksi']['bahan']['con_'.$no]);
			var_dump($_SESSION['custom-produksi']);
		}
		if($key=="sess-bahan-reset"){
			unset($_SESSION['custom-produksi']['bahan']);
		}
		if($key=="sess-lokasi"){
			$_SESSION['custom-produksi']['lokasi'] = $this->input->post('lokasi_id');
		}
	}
	function list_bahan(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bahan->custom_bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->bahan->custom_bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bahan->custom_bahan_list($start,$length,$query);
		$tempBahan = array();
		if(isset($_SESSION['custom-produksi']['bahan'])){
			foreach ($_SESSION['custom-produksi']['bahan'] as $key) {
				if(!isset($tempBahan["bahan_".$key["bahan_id"]])){
					$tempBahan["bahan_".$key["bahan_id"]] = 0;
				}
				$tempBahan["bahan_".$key["bahan_id"]] += $key["value"];

			}
		}

		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if(isset($tempBahan['bahan_'.$key->bahan_id])){
				$key->jumlah_lokasi = $key->jumlah_lokasi - $tempBahan['bahan_'.$key->bahan_id];
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'bahan/delete/';
			$key->action =null;
			$key->row_id = $key->bahan_id;
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
			$key->jumlah_lokasi = number_format($key->jumlah_lokasi);
			if(isset($_SESSION["redpos_login"]['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);
			}
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
	function selesai_produksi(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data['tanggal_selesai'] =date("Y-m-d",strtotime($this->input->post('tanggal_selesai')));
		$data['status_produksi'] = "finish";
		$produk_custom_id = $this->input->post('produk_custom_id');
		$this->produksi->start_trans();

		$update = $this->custom_produksi->update_by_id('produk_custom_id',$produk_custom_id,$data)
		;
		if($update){
			$lokasi_bahan_id = $this->input->post('lokasi_bahan_id');
			$produksi_item_bahan = $this->custom_produksi->produksi_custom_bahan_by_id($produk_custom_id);
			foreach ($produksi_item_bahan as $key) {
				$row_stock = $this->stock_bahan->stock_by_location_bahan_id($lokasi_bahan_id,$key->bahan_id);

				$jumlah_bahan = $key->jumlah;
				foreach ($row_stock as $row) {
					$data = array();
					$jumlah = $row->stock_bahan_qty - $jumlah_bahan;
					$stock_out = $jumlah_bahan;
					if($row->stock_bahan_qty < $jumlah_bahan){
						$jumlah = 0;
						$stock_out = $row->stock_bahan_qty;
					}
					$jumlah_bahan =  abs($jumlah_bahan-$row->stock_bahan_qty);
					$data["stock_bahan_qty"] = $jumlah;
					$stock_bahan_id = $row->stock_bahan_id;
					$updateStok = $this->stock_bahan->update_by_id('stock_bahan_id',$stock_bahan_id,$data);
					if($updateStok){
						$data = array();
						$data["tanggal"] = date("Y-m-d");
						$data["table_name"] = "stock_bahan";
						$data["stock_bahan_id"] = $row->stock_bahan_id;
						$data["bahan_id"] = $key->bahan_id;
						$data["stock_out"] = $stock_out;
						$data["stock_in"] = 0;
						$data["last_stock"] = $this->stock_bahan->last_stock($key->bahan_id)->result;
						$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
						$data["keterangan"] = "Produksi produk";
						$data["method"] = "update";
						$this->stock_bahan->arus_stock_bahan($data);
					}
					if($jumlah_bahan == 0){
						break;
					}

				}
			}
		}
		if($this->produksi->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}
		echo json_encode($result);
	}
	function penerimaan_produksi(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$produksi_id = $this->input->post('produk_custom_id');
		$data["status_penerimaan"] = "Diterima";
		$data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
		$lokasi_id = $this->input->post('lokasi_penerimaan_id');
		$hpp = $this->input->post('hpp');
		$data['hpp'] = $this->string_to_number($hpp);
		$data['lokasi_penerimaan_id'] = $lokasi_id;
		$update_produksi = $this->custom_produksi->update_by_id('produk_custom_id',$produksi_id,$data);
		if($update_produksi){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}
		echo json_encode($result);
	}
	function history_index(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/produksi.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produksi Custom < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "produksi";
		$data['page'] = 'custom-produksi';
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produksi_kode"));
		array_push($column, array("data"=>"jumlah_item"));
		array_push($column, array("data"=>"tanggal_mulai"));
		array_push($column, array("data"=>"estimasi_selesai"));
		array_push($column, array("data"=>"status_produksi"));
		array_push($column, array("data"=>"tanggal_selesai"));
		array_push($column, array("data"=>"keterangan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['produksi']['custom-produksi'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$data['produksi_kode'] = $this->produksi->get_kode_produksi();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/custom_history_produksi');
		$this->load->view('admin/static/footer');
	}
	function history_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->custom_produksi->history_produksi_count_all();
		$result['iTotalDisplayRecords'] = $this->custom_produksi->history_produksi_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->custom_produksi->history_produksi_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_mulai != null){
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y',$time);
			}
			if($key->tanggal_selesai != null){
				$time = strtotime($key->tanggal_selesai);
				$key->tanggal_selesai = date('d-m-Y',$time);
			}
			if($key->estimasi_selesai != null){
				$time = strtotime($key->estimasi_selesai);
				$key->estimasi_selesai = date('d-m-Y',$time);
			}
			if($key->status_produksi == "Selesai"){
				$key->deny_edit = true;
				$key->deny_delete = true;
				if($key->status_penerimaan == "Belum Diterima")	{
					$key->penerimaan_status_btn = true;
				}
			} else{
				$key->produksi_status_btn = true;
			}
			$key->edit_url = base_url().'produksi/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_custom_id));
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produksi/delete/';
			$key->row_id = $key->produk_custom_id;
			$key->produksi_kode = "PC-".$key->produk_custom_id;
			$key->jumlah_item = $key->jumlah_pesan;
			$key->keterangan = $key->note;
			$key->tanggal_penerimaan = $this->date_database_format($key->tanggal_penerimaan);
			$key->action = null;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}

}

/* End of file ProduksiController.php */
/* Location: ./application/controllers/ProduksiController.php */
