<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReturAgentController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('produk','',true);
        $this->load->model('user','',true);
        $this->load->model('staff','',true);
        $this->load->model('arus_stock_produk','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('hutang','',true);
        $this->load->model('retur_agen','',true);
        $this->load->model('retur_agen_detail','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/retur_agen.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Retur < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "retur";
        $data['page'] = $this->uri->segment(1);
        $sumColumn = array(4);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"no_retur"));
        array_push($column, array("data"=>"status"));

        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['retur'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/retur-agen/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->retur_agen->retur_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->retur_agen->retur_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->retur_agen->retur_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            if($key->status!="preparing"){
                $key->deny_edit = true;
                $key->deny_delete = true;
                $key->deny_submit = true;
            }
            $key->delete_url = base_url().'retur/delete/';
            $key->edit_url = base_url().'retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->retur_agen_id));
            $key->row_id = $key->retur_agen_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/admin/retur_agen.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Retur < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "retur";
        $data['page'] = $this->uri->segment(1);
        $data['lokasi'] = $this->lokasi->all_list();

        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $data['reseller'] = $reseller;
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/retur-agen/add');
        $this->load->view('admin/static/footer');
        unset($_SESSION['po_produk']);
        $_SESSION['po_produk']['lokasi'] = 1;
    }
    function save_add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $insert = $this->retur_agen->insertData($reseller_id);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/admin/retur_agen.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Retur < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "retur";
        $data['page'] = $this->uri->segment(1);
        $data['lokasi'] = $this->lokasi->all_list();

        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $data['reseller'] = $reseller;
        $id = $this->uri->segment(3);
        $id = $id == '' ? '' : str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $id == '' ? '' : $this->encryption->decrypt($id);

        $retur = $this->retur_agen->row_by_id($id);
        if($retur==null){
            redirect('404_override','refresh');
        }else{
            $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
            $retur_produk = $this->retur_agen->row_by_id($id);
            $retur_produk_detail = $this->retur_agen_detail->data_by_id($id);
            foreach ($retur_produk_detail as $item){
                $item->jumlah_stok = $this->retur_agen_detail->stock_produk_by_id($lokasi_id,$item->produk_id);
            }
            $data['retur_agen_detail'] = $retur_produk_detail;
            $index_no = $retur_produk_detail == null ? 0 :sizeof($retur_produk_detail);
            $data['index_no'] = $index_no;
            $data['retur_agen_id'] = $id;
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/retur-agen/edit');
            $this->load->view('admin/static/footer');
            unset($_SESSION['po_produk']);
            $_SESSION['po_produk']['lokasi'] = 1;
        }
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->retur_agen->delete_by_id("retur_agen_id",$id);
            $this->retur_agen_detail->delete_by_id("retur_agen_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function detail(){
        $retur_agen_id = $this->input->post('retur_agen_id');
        $retur_agen_detail = $this->retur_agen_detail->data_by_id($retur_agen_id);
        echo json_encode($retur_agen_detail);
    }
    function save_edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $edit = $this->retur_agen->editData();
        if($edit){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function submit(){
        $retur_agen_id = $this->input->post('retur_agen_id');
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data ["status"] = "waiting";
        $edit = $this->retur_agen->update_by_id('retur_agen_id',$retur_agen_id,$data);
        if($edit){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function list_produk(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
        $result['iTotalRecords'] = $this->produk->produk_count_all_money_back($lokasi_id);
        $result['iTotalDisplayRecords'] = $this->produk->produk_count_filter_money_back($query,$lokasi_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk->produk_list_money_back_display($start,$length,$query,$lokasi_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'produk/delete/';
            $key->action =null;
            $key->last_hpp = $this->stock_produk->last_hpp($key->produk_id);
            $key->last_hpp = $key->last_hpp == 0 ? $key->hpp_global : $key->last_hpp;
            $key->row_id = $key->produk_id;
            $key->produk_minimal_stock = number_format($key->produk_minimal_stock);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }


}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */