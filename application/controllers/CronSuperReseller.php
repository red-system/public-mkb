<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class CronSuperReseller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
        $this->load->helper('string');
    }
    public function index()
    {
        $sql1 = '(select reseller_id,max(reward_id) as rangking from mykindofbeauty_kemiri.claim_reward where status = "approve" GROUP BY reseller_id)';
        $date = date("Y-m-d");
        $datetime = date("Y-m-d H:i:s");
        $hasil_reseller =  $this->db->select('reseller.reseller_id,nama,next_deposit_date,tanggal_withdraw.tanggal,DATEDIFF(next_deposit_date,"'.$date.'") as rentang_deposit,DATEDIFF("'.$date.'",if(tanggal_withdraw.tanggal is null,reseller.last_deposit,tanggal_withdraw.tanggal)) as rentang_wd,if(b.rangking is null,0,b.rangking) as rangking')
            ->join('mykindofbeauty_kemiri.tanggal_withdraw','mykindofbeauty_kemiri.reseller.reseller_id = mykindofbeauty_kemiri.tanggal_withdraw.reseller_id','left')
            ->join($sql1.' as b','reseller.reseller_id = b.reseller_id','left')
            ->where('status','active')
            ->where('type','super agen')
            ->where('demo',0)
            ->having('rangking < 3')
            ->having('rentang_deposit < 0')
            ->having('rentang_wd > 60')
            ->get('mykindofbeauty_kemiri.reseller')->result();
        
        foreach ($hasil_reseller as $item){
            $cek = $this->db->where('reseller_id',$item->reseller_id)->where('status','active')->get('mykindofbeauty_kemiri.downgrade_reseller')->result();
            if(sizeof($cek)==0){
                $downgrade_reseller = array();
                $downgrade_reseller['reseller_id'] = $item->reseller_id;
                $downgrade_reseller['tanggal'] = $date;
                $downgrade_reseller['created_at'] = $datetime;
                $this->db->insert('mykindofbeauty_kemiri.downgrade_reseller',$downgrade_reseller);
                $this->change_reseller_status($item->reseller_id);
            }
        }
    }
    function change_reseller_status($reseller_id){
        $dataUpdate = array();
        $dataUpdate['type'] = 'agent';
        $this->db->where('reseller_id',$reseller_id)->update('mykindofbeauty_kemiri.reseller',$dataUpdate);

    }
    public function bintang_atas(){
        $sql1 = '(select reseller_id,max(reward_id) as rangking from mykindofbeauty_kemiri.claim_reward where status = "approve" GROUP BY reseller_id)';
        $date = date("Y-m-d");
        $data = $this->db->select('reseller.reseller_id,reseller.nama,next_deposit_date,DATEDIFF(next_deposit_date,"'.$date.'") as rentang_deposit,b.rangking')
                ->join($sql1.' as b','reseller.reseller_id = b.reseller_id','left')
                ->where('reseller.type','super agen')
                ->where('status','active')
                ->having('rangking >= 3')
                ->having('rentang_deposit < 0')
                ->get('mykindofbeauty_kemiri.reseller')->result();
        $datetime = date("Y-m-d H:i:s");
        foreach ($data as $item){
            $dataUpdate = array();
            $dataUpdate['is_frezze'] = 1;
            $this->db->where('reseller_id',$item->reseller_id)->update('mykindofbeauty_kemiri.reseller',$dataUpdate);
            if($item->rentang_deposit<-30){
                $cek = $this->db->where('reseller_id',$item->reseller_id)->where('status','active')->get('mykindofbeauty_kemiri.downgrade_reseller')->result();
                if(sizeof($cek)==0){
                    $downgrade_reseller = array();
                    $downgrade_reseller['reseller_id'] = $item->reseller_id;
                    $downgrade_reseller['tanggal'] = $date;
                    $downgrade_reseller['created_at'] = $datetime;
                    $this->db->insert('mykindofbeauty_kemiri.downgrade_reseller',$downgrade_reseller);
                    $this->change_reseller_status($item->reseller_id);
                }
            }
        }
    }
}

/* End of file ProdukByLocation.php */
/* Location: ./application/controllers/ProdukByLocation.php */