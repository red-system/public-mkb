<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManagementRewardResellerController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('reward_reseller','',true);

    }
    function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/management-reward-reseller.js");

        $link_result = $this->config->item('form-reward-result');
        $data['link_result'] = $link_result;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Management Reward < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = "pembayaran-withdraw-cash";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"terpakai_"));
        array_push($column, array("data"=>"jumlah_bintang_"));
        array_push($column, array("data"=>"jumlah_uang_reward_"));
        array_push($column, array("data"=>"status"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hadiah']['management-reward-reseller'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-reward-reseller/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->reward_reseller->_count_all();
        $result['iTotalDisplayRecords'] = $this->reward_reseller->_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reward_reseller->_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $key->row_id = $key->reward_reseller_id;
            $i ++;
            $key->total_penjualan_ = number_format($key->total_penjualan);
            $key->terpakai_ = number_format($key->terpakai);
            $key->jumlah_bintang_ = number_format($key->jumlah_bintang);
            $key->jumlah_uang_reward_ = number_format($key->jumlah_uang_reward);
            if($key->status!='request'){
                $key->deny_approve = true;
                $key->deny_refuse = true;
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function approve(){
        $reward_reseller_id = $this->input->post('reward_reseller_id');
        $data_reward_reseller = array();
        $data_reward_reseller['status'] = 'approve';
        $this->reward_reseller->update_by_id('reward_reseller_id',$reward_reseller_id,$data_reward_reseller);

        $reward_reseller = $this->reward_reseller->row_by_id($reward_reseller_id);

        $data_pengiriman_reward_reseller = array();
        $data_pengiriman_reward_reseller['reward_reseller_id'] = $reward_reseller_id;
        $data_pengiriman_reward_reseller['tanggal'] = date('Y-m-d');
        $data_pengiriman_reward_reseller['jumlah'] = $reward_reseller->jumlah_uang_reward;
        $data_pengiriman_reward_reseller['to_reseller_id'] = $reward_reseller->reseller_id;

        $this->load->model('pengiriman_reward_reseller','',true);
        $insert = $this->pengiriman_reward_reseller->insert($data_pengiriman_reward_reseller);
        $result['success'] = false;
        $result['message'] = 'Gagal memproses data';
        if($insert){
            $result['success'] = true;
            $result['message'] = 'Berhasil memproses data';
        }
        echo json_encode($result);

    }
    function refuse(){
        $reward_reseller_id = $this->input->post('reward_reseller_id');
        $data_reward_reseller = array();
        $data_reward_reseller['status'] = 'refuse';
        $proses =  $this->reward_reseller->update_by_id('reward_reseller_id',$reward_reseller_id,$data_reward_reseller);

        $result['success'] = false;
        $result['message'] = 'Gagal memproses data';
        if($proses){
            $result['success'] = true;
            $result['message'] = 'Berhasil memproses data';
        }
        echo json_encode($result);
    }

}