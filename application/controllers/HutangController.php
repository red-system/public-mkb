<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HutangController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hutang','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('po_bahan','',true);
        $this->load->model('po_produk','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/hutang.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Hutang < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = $this->uri->segment(1);
        $target = array(0,3,4,5);
        $sumColumn = array(3,4,5);

        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"suplier_nama"));
        array_push($column, array("data"=>"po_bahan_no"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"terbayar_lbl"));
        array_push($column, array("data"=>"sisa_lbl"));
        array_push($column, array("data"=>"status_pembayaran_hutang"));
        array_push($column, array("data"=>"tenggat_pelunasan"));
        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['hutang'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/hutang');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $tipe = 0;
        if(isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != ""){
            $tipe = $_GET["columns"][7]["search"]["value"];
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        if($tipe==0){
            $result['iTotalRecords'] = $this->hutang->hutang_count_all();
            $result['iTotalDisplayRecords'] = $this->hutang->hutang_count_filter($query);
            $data =  $this->hutang->hutang_resto_list($start,$length,$query);
        } else if ($tipe==1){
            $result['iTotalRecords'] = $this->hutang->hutang_akan_count_all();
            $result['iTotalDisplayRecords'] = $this->hutang->hutang_akan_count_filter($query);
            $data =  $this->hutang->hutang_akan_list($start,$length,$query);
        } else {
            $result['iTotalRecords'] = $this->hutang->hutang_jatuh_count_all();
            $result['iTotalDisplayRecords'] = $this->hutang->hutang_jatuh_count_filter($query);
            $data =  $this->hutang->hutang_jatuh_list($start,$length,$query);
        }
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->hutang_id;
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar_lbl = $this->idr_currency($key->terbayar);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa_lbl = $this->idr_currency($key->sisa);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'hutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
            $key->posting_url = base_url().'hutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function pay(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/hutang_piutang.js");
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang< Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = 'hutang';
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $hutang = $this->hutang->hutang_by_id($id);
        $data['id'] = $id;
        if ($hutang != null) {
            $data['hutang'] = $hutang;
            array_push($column, array("data"=>"no"));
            array_push($column, array("data"=>"pembayaran"));
            array_push($column, array("data"=>"jumlah_lbl"));
            array_push($column, array("data"=>"keterangan"));
            $data['sumColumn'] = json_encode(array(2));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,2)));
            $data["action"] = json_encode(array("stock"=>false,"view"=>false,"edit"=>true,"delete"=>true));

            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/pay_hutang');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function pay_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->hutang->pembayaran_hutang_count($this->uri->segment(4));
        $result['iTotalDisplayRecords'] = $this->hutang->pembayaran_hutang_filter($this->uri->segment(4),$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->hutang->pembayaran_hutang_list($start,$length,$query,$this->uri->segment(4));
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->delete_url = base_url().'hutang/pay/delete/'.$key->pembayaran_hutang_id;
            $key->row_id = $key->pembayaran_hutang_id;
            $key->jumlah_lbl = $this->idr_currency($key->jumlah);
            $key->jumlah = number_format($key->jumlah);
            $key->pembayaran = $key->tipe_pembayaran_nama." ".$key->no_akun;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function detail(){
        $hutang_id = $this->uri->segment(3);
        $data["result"] = $this->hutang->hutang_by_id($hutang_id);
        echo json_encode($data);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['hutang_id'] = $this->input->post('hutang_id');
        $hutang_id = $this->input->post('hutang_id');
        $data['tipe_pembayaran_id'] = $this->input->post('tipe_pembayaran_id');
        $data['jumlah'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal'] = date("Y-m-d",$temp);
        $sisa = $this->string_to_number($this->input->post('sisa'));
        if($sisa >= $data['jumlah']){
            $insert = $this->hutang->insert_pembayaran($data);
            if($insert){
                $cek = $this->hutang->hutang_by_id($data['hutang_id']);
                if($cek->grand_total <= $cek->terbayar){
                    if($cek->po_produk_id!=null){
                        $data = array();
                        $data['status_pembayaran'] = "Lunas";
                        $this->po_produk->update_by_id('po_produk_id',$cek->po_produk_id,$data);

                    } else {
                        $data = array();
                        $data['status_pembayaran'] = "Lunas";
                        $this->po_bahan->update_by_id('po_bahan_id',$cek->po_bahan_id,$data);
                    }
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->hutang->update_by_id('hutang_id',$hutang_id,$data);

                } else{
                    $data = array();
                    $data['status_pembayaran'] = "Hutang";
                    $this->po_produk->update_by_id('po_produk_id',$cek->po_produk_id,$data);
                    $data = array();
                    $data['status_pembayaran'] = "Belum Lunas";
                    $this->hutang->update_by_id('hutang_id',$hutang_id,$data);
                }
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }
        } else {
            $result['message'] = "Pembayaran melebihi sisa hutang";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['hutang_id'] = $this->input->post('hutang_id');
        $hutang_id = $this->input->post('hutang_id');
        $data['tipe_pembayaran_id'] = $this->input->post('tipe_pembayaran_id');
        $data['jumlah'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal'] = date("Y-m-d",$temp);
        $pembayaran_hutang_id = $this->input->post('pembayaran_hutang_id');
        $grand_total = $this->string_to_number($this->input->post('grand_total'));
        $terbayar = $this->string_to_number($this->input->post('terbayar'));
        $old_jumlah = $this->string_to_number($this->input->post('old_jumlah'));
        $new_terbayar = $terbayar - $old_jumlah + $data['jumlah'];
        if($grand_total >= $new_terbayar){
            $edit = $this->hutang->edit_pembayaran($data,$pembayaran_hutang_id);
            if($edit){
                $cek = $this->hutang->hutang_by_id($data['hutang_id']);
                if($cek->grand_total <= $cek->terbayar){
                    if($cek->po_produk_id!=null){
                        $data = array();
                        $data['status_pembayaran'] = "Lunas";
                        $this->po_produk->update_by_id('po_produk_id',$cek->po_produk_id,$data);

                    } else {
                        $data = array();
                        $data['status_pembayaran'] = "Lunas";
                        $this->po_bahan->update_by_id('po_bahan_id',$cek->po_bahan_id,$data);
                    }
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->hutang->update_by_id('hutang_id',$hutang_id,$data);
                } else{
                    $data = array();
                    $data['status_pembayaran'] = "Hutang";
                    $this->po_produk->update_by_id('po_produk_id',$cek->po_produk_id,$data);
                    $data = array();
                    $data['status_pembayaran'] = "Belum Lunas";
                    $this->hutang->update_by_id('hutang_id',$hutang_id,$data);
                }
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }
        } else {
            $result['message'] = "Pembayaran melebihi sisa hutang";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $pembayaran = $this->hutang->detail_pembayaran($id);
            $delete = $this->hutang->delete_pembayaran($id);
            if($delete){
                $cek = $this->hutang->hutang_by_id($pembayaran->hutang_id);
                if($cek->grand_total <= $cek->terbayar){
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->po_produk->update_by_id('po_produk_id',$cek->po_produk_id,$data);
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->hutang->update_by_id('hutang_id',$pembayaran->hutang_id,$data);
                } else{
                    $data = array();
                    $data['status_pembayaran'] = "Hutang";
                    $this->po_produk->update_by_id('po_produk_id',$cek->po_produk_id,$data);
                    $data = array();
                    $data['status_pembayaran'] = "Belum Lunas";
                    $this->hutang->update_by_id('hutang_id',$pembayaran->hutang_id,$data);
                }
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function posting(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $result['success'] = false;
        $result['message'] = "gagal merubah data";
        $data = array();
        $data['status_hutang'] = 'Tutup';
        $post = $this->hutang->update_by_id('hutang_id',$id,$data);
        if($post){
            $result['success'] = true;
            $result['message'] = "Data berhasil dirubah";
        }
        echo json_encode($result);
    }
}

/* End of file HutangController.php */
/* Location: ./application/controllers/HutangController.php */