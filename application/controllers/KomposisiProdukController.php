<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomposisiProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('bahan','',true);
		$this->load->model('satuan','',true);
		$this->load->model('komposisi_produk','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
	
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Komposisi Produk < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"jenis_produk_nama"));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$data["action"] = json_encode(array("composition"=>true));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/komposisi_produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->composition_url = base_url().'komposisi-produk/komposisi/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$key->row_id = $key->produk_id;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function komposisi_index(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Komposisi Produk < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$produk = $this->produk->row_by_id($id);
		$bahan = $this->bahan->all_list();
		$satuan = $this->satuan->all_list();
		$data["satuan"] = $satuan;
		$data['bahan'] = $bahan;
		$data['id'] = $id;
		if ($produk != null) {
			$data['produk'] = $produk;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"bahan_nama"));
			array_push($column, array("data"=>"satuan_nama"));
			array_push($column, array("data"=>"komposisi_qty"));
					$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['inventori']['koposisi-produk'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);	
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/komposisi_produk');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}		
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data["produk_id"] = $this->input->post('produk_id');
		$data["bahan_id"] = $this->input->post('bahan_id');
		$data["satuan_id"] = $this->input->post('satuan_id');
		$data["komposisi_qty"] = $this->input->post('komposisi_qty');
		$insert = $this->komposisi_produk->insert($data);
		if($insert){
			$result['success'] = true;
			$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data["produk_id"] = $this->input->post('produk_id');
		$data["bahan_id"] = $this->input->post('bahan_id');
		$data["satuan_id"] = $this->input->post('satuan_id');
		$data["komposisi_qty"] = $this->input->post('komposisi_qty');
		$komposisi_id = $this->input->post('komposisi_id');
		$update = $this->komposisi_produk->update_by_id('komposisi_id',$komposisi_id,$data);
		if($update){
			$result['success'] = true;
			$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->komposisi_produk->delete_by_id("komposisi_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function komposisi_list(){
		$id = $this->uri->segment(4);		
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->komposisi_produk->komposisi_count_all($id);
		$result['iTotalDisplayRecords'] = $this->komposisi_produk->komposisi_count_filter($query,$id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->komposisi_produk->komposisi_list($start,$length,$query,$id);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->composition_url = base_url().'komposisi-produk/komposisi/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$key->row_id = $key->produk_id;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
}

/* End of file KomposisiProdukController.php */
/* Location: ./application/config/KomposisiProdukController.php */