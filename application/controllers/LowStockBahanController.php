<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LowStockBahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bahan','',true);
		$this->load->model('jenis_bahan','',true);
		$this->load->model('satuan','',true);
		$this->load->model('suplier','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Bahan < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$target = array(0,5,6);
		$sumColumn = array(5,6);		
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"bahan_kode"));
		array_push($column, array("data"=>"bahan_nama"));
		array_push($column, array("data"=>"jenis_bahan_nama"));
		array_push($column, array("data"=>"satuan_nama"));
		array_push($column, array("data"=>"stock"));
		array_push($column, array("data"=>"jumlah_lokasi"));
		array_push($column, array("data"=>"lokasi_nama"));
		array_push($column, array("data"=>"suplier_nama"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$data["jenis_bahan"] = $this->jenis_bahan->all_list();
		$data["satuan"] = $this->satuan->all_list();
		$data["suplier"] = $this->suplier->all_list();
		$data['sumColumn'] = json_encode($sumColumn);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bahan->low_bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->bahan->low_bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bahan->low_bahan_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'bahan/delete/';
			$key->row_id = $key->bahan_id;
			$key->stok_url = base_url().'stock-bahan/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->bahan_id));
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
			$key->stock = number_format($key->stock);
			if(isset($_SESSION["redpos_login"]['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}

}

/* End of file LowStockBahanController.php */
/* Location: ./application/controllers/LowStockBahanController.php */