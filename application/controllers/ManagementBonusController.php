<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManagementBonusController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bonus','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');


    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        
        array_push($this->js, "script/admin/management-bonus.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama_penerima"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"persentase"));
        array_push($column, array("data"=>"jumlah_bonus"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"manage_status","template"=>"badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array(8));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-bonus/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->bonus->bonus_admin_all();
        $result['iTotalDisplayRecords'] = $this->bonus->bonus_admin_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->bonus->bonus_admin_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'" data-status="'.$key->manage_status.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function success_page(){

        $segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->success_list();			
				break;
			case 'pdf':
				$this->success_pdf();			
				break;
			case 'excel':
				$this->success_excel();			
				break;						
			default:
                array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
                array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
                array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
                array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
                array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
                array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
                array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
                array_push($this->js, "script/admin/management-bonus.js");
                $data["css"] = $this->css;
                $data["js"] = $this->js;
                $column = array();
                $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
                $data['parrent'] = "manage-bonus";
                $data['page'] = $this->uri->segment(1);
                $data['sumColumn'] = json_encode( array(8));
                array_push($column, array("data"=>"check"));
                array_push($column, array("data"=>"no"));
                array_push($column, array("data"=>"tanggal"));
                array_push($column, array("data"=>"nama_penerima"));
                array_push($column, array("data"=>"nama"));
                array_push($column, array("data"=>"type"));
                array_push($column, array("data"=>"jumlah_deposit"));
                array_push($column, array("data"=>"persentase"));
                array_push($column, array("data"=>"jumlah_bonus"));
                array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
                $data['column'] = json_encode($column);
                $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
                $akses_menu = json_decode($this->menu_akses,true);
                $action = array();
                $data['action'] = json_encode($action);
                $this->load->view('admin/static/header',$data);
                $this->load->view('admin/static/sidebar');
                $this->load->view('admin/static/topbar');
                $this->load->view('admin/management-bonus/index_success');
                $this->load->view('admin/static/footer');			
				break;
		}	

        
    }
    function success_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->bonus->bonus_admin_all_success();
        $result['iTotalDisplayRecords'] = $this->bonus->bonus_admin_filter_success($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->bonus->bonus_admin_list_success($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function success_pdf(){
		
		if(isset($_GET["tanggal_start"]) && $this->input->get('tanggal_start') != ""){
			$data['tanggal_start'] = $this->input->get('tanggal_start');
			$data['tanggal_end'] = $this->input->get('tanggal_end');
		} else {
			$data['tanggal_start'] = "-";
			$data['tanggal_end'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->bonus_admin_filter_success($query);
		$list =  $this->bonus->bonus_admin_list_success($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
            $key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_management_bonus_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Management Produk Success'.$date.".pdf","D");
	}

	function success_excel(){
		if(isset($_GET["tanggal_start"]) && $this->input->get('tanggal_start') != ""){
			$data['tanggal_start'] = $this->input->get('tanggal_start');
			$data['tanggal_end'] = $this->input->get('tanggal_end');
		} else {
			$data['tanggal_start'] = "-";
			$data['tanggal_end'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->bonus_admin_filter_success($query);
		$list =  $this->bonus->bonus_admin_list_success($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
            $key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Management Bonus Success')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Tanggal')
		->setCellValue('C7', 'Penerima')
		->setCellValue('D7', 'Agen/Super Agen')
		->setCellValue('E7', 'Type Bonus')
		->setCellValue('F7', 'Total')
		->setCellValue('G7', 'Persentase')
		->setCellValue('H7', 'Total Bonus')
		->setCellValue('I7', 'Status')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(18);		
		$spreadsheet->getActiveSheet()->getStyle("A7:I7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->tanggal)
		->setCellValue('C'.$i, $key->nama_penerima)
		->setCellValue('D'.$i, $key->nama)
		->setCellValue('E'.$i, $key->type)
		->setCellValue('F'.$i, $key->jumlah_deposit)
		->setCellValue('G'.$i, $key->persentase)
		->setCellValue('H'.$i, $key->jumlah_bonus)
		->setCellValue('I'.$i, $key->status);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:I".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:I7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('I8:I'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		$spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

    function hold(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);

        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama_penerima"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"persentase"));
        array_push($column, array("data"=>"jumlah_bonus"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-bonus/index_hold');
        $this->load->view('admin/static/footer');
    }
    function hold_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->bonus->bonus_admin_all_hold();
        $result['iTotalDisplayRecords'] = $this->bonus->bonus_admin_filter_hold($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->bonus->bonus_admin_list_hold($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function hold_pdf(){
		
		if(isset($_GET["tanggal_start"]) && $this->input->get('tanggal_start') != ""){
			$data['tanggal_start'] = $this->input->get('tanggal_start');
			$data['tanggal_end'] = $this->input->get('tanggal_end');
		} else {
			$data['tanggal_start'] = "-";
			$data['tanggal_end'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->bonus_admin_filter_hold($query);
		$list =  $this->bonus->bonus_admin_list_hold($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
            $key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_management_bonus_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Management Produk Hold '.$date.".pdf","D");
	}

	function hold_excel(){
		if(isset($_GET["tanggal_start"]) && $this->input->get('tanggal_start') != ""){
			$data['tanggal_start'] = $this->input->get('tanggal_start');
			$data['tanggal_end'] = $this->input->get('tanggal_end');
		} else {
			$data['tanggal_start'] = "-";
			$data['tanggal_end'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->bonus_admin_filter_hold($query);
		$list =  $this->bonus->bonus_admin_list_hold($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
            $key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Management Bonus Hold')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Tanggal')
		->setCellValue('C7', 'Penerima')
		->setCellValue('D7', 'Agen/Super Agen')
		->setCellValue('E7', 'Type Bonus')
		->setCellValue('F7', 'Total')
		->setCellValue('G7', 'Persentase')
		->setCellValue('H7', 'Total Bonus')
		->setCellValue('I7', 'Status')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(18);		
		$spreadsheet->getActiveSheet()->getStyle("A7:I7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->tanggal)
		->setCellValue('C'.$i, $key->nama_penerima)
		->setCellValue('D'.$i, $key->nama)
		->setCellValue('E'.$i, $key->type)
		->setCellValue('F'.$i, $key->jumlah_deposit)
		->setCellValue('G'.$i, $key->persentase)
		->setCellValue('H'.$i, $key->jumlah_bonus)
		->setCellValue('I'.$i, $key->status);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:I".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:I7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('I8:I'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		$spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

    function processing(){
        $this->load->model('dpp_reseller','',true);
        $this->load->model('send_bonus','',true);
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $bonus_id = explode(",",$bonus_id);

        $proses = $this->bonus->changeToProcessing($bonus_id);
        $listDpp = $this->bonus->listForDPP($bonus_id);

        $no_generate = $this->send_bonus->get_no_generate();
        foreach ($listDpp as $item){

            $this->dpp_reseller->delete_dpp_res($item->to_reseller_id,'harian');
            // reseller ID
            $reseller_id = $item->to_reseller_id;
            // bonus bruto
            $total_bonus = $item->total_bonus;
            // DPP
            $dpp = $item->total_bonus*0.5;
            // PTKP
            $ptkp = $item->ptkp;
            // sisa ptkp
            $sisa_ptkp = 0;
            if($item->case_res==3){
                $sisa_ptkp = $this->dpp_reseller->sisa_ptkp($reseller_id);
            }

            $sisa_ptkp = $sisa_ptkp - $dpp;
            // pkp = sisa ptkp - dpp
            if($sisa_ptkp < 0 ){
                $pkp = $sisa_ptkp * -1;
                $sisa_ptkp = 0;
            } else {
                $pkp = 0;
            }
            // jika case = 1 atau 2 ptkp = dpp
            if($item->case_res!=3){
                $pkp = $dpp;
            }

            $dpp_reseller_ids = array();
            $total_pajak = 0;
            $pkp_progresif = 0;
            $count = 0;
            if($item->case_res==3){
                $count = 1;
            }
            while ($pkp > 0 || $count > 0) {
                $pkp_kumulatif_awal = ($pkp_progresif + $this->dpp_reseller->pkp_kumulatif($item->to_reseller_id));
                $max_pkp = $this->dpp_reseller->max_dpp($pkp_kumulatif_awal);
                $sisa_pkp = $max_pkp - $pkp_kumulatif_awal;
                $temp = $pkp;
                $pkp = $pkp - $sisa_pkp;
                $pkp_input = $sisa_pkp;

                if($item->case_res==3){
                    $count = $count - 1;
                }

                if($pkp<0){
                    $pkp = 0;
                    $pkp_input = $temp;
                }
                $pkp_progresif = $pkp_progresif + $pkp_input;
                $dpp_reseller['reseller_id'] = $item->to_reseller_id;
                $dpp_reseller['jumlah_bonus'] = $total_bonus;
                $dpp_reseller['dasar_pemotongan'] = $dpp;
                if($item->case_res!=3){
                    $dpp_reseller['dasar_pemotongan'] = $pkp_input;
                }
                $dpp_reseller['pkp'] = $pkp_input;
                $dpp_reseller['ptkp'] = $ptkp;
                $dpp_reseller['sisa_ptkp_perbulan'] = $sisa_ptkp;

                $dpp_reseller['dpp_kumulatif'] = $pkp_progresif + $this->dpp_reseller->dpp_kumulatif($item->to_reseller_id);
                if($pkp_progresif==0){
                    $dpp_reseller['dpp_kumulatif'] = $dpp + $this->dpp_reseller->dpp_kumulatif($item->to_reseller_id);;
                }
                $dpp_reseller['pkp_kumulatif'] = $pkp_progresif + $this->dpp_reseller->pkp_kumulatif($item->to_reseller_id);
                $persentase_dpp = $this->dpp_reseller->persentase_dpp($dpp_reseller['pkp_kumulatif'] );
                $dpp_reseller['persentase'] =  $persentase_dpp * 100;
                $dpp_reseller['pph_npwp'] = $dpp_reseller['dasar_pemotongan']*$persentase_dpp;
                if($item->case_res==3){
                    $dpp_reseller['pph_npwp'] = $dpp_reseller['pkp']*$persentase_dpp;
                }
                if($dpp_reseller['pkp']==0){
                    $dpp_reseller['pph_npwp'] = 0;
                }
                $dpp_reseller['pph_non_npwp'] = 0;
                if($item->case_res==2){
                    $dpp_reseller['pph_non_npwp'] = $dpp_reseller['pph_npwp']*0.2;
                }
                $dpp_reseller['total_pph'] = $dpp_reseller['pph_npwp'] + $dpp_reseller['pph_non_npwp'];
                $this->dpp_reseller->insert($dpp_reseller);
                $dpp_reseller_id = $this->dpp_reseller->last_id();
                $total_pajak = $total_pajak + $dpp_reseller['total_pph'];
                array_push($dpp_reseller_ids,$dpp_reseller_id);
            }

            $send_bonus['reseller_id'] = $item->to_reseller_id;
            $send_bonus['type'] = 'bonus';
            $send_bonus['no_generate'] = $no_generate;
            $send_bonus['tanggal_proccess'] = date('Y-m-d H:i:s');
            $send_bonus['jumlah_bonus'] = $item->total_bonus;
            $send_bonus['admin'] = 10000;
            $send_bonus['pajak'] = $total_pajak;
            $send_bonus['bonus_net'] = $item->total_bonus - $send_bonus['admin'] - ($send_bonus['pajak']);

            $this->send_bonus->insert($send_bonus);
            $send_bonus_id = $this->send_bonus->last_id();
            $bonus_ids = explode(',',$item->bonus_ids);
            $this->bonus->update_bonus_send($send_bonus_id,$bonus_ids);
            $this->dpp_reseller->insert_send_bonus($send_bonus_id,$dpp_reseller_ids);

        }
        $url_get = base_url()."manage-bonus/print?no_generate=".$no_generate;
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
            $result["url_get"] = $url_get;
        }
        echo json_encode($result);
    }
    function fail(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $bonus_id = explode(",",$bonus_id);
        $proses = $this->bonus->changeToFail($bonus_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }
    function success_process(){
        $this->load->model('dpp_reseller');
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $bonus_id = explode(",",$bonus_id);
        $send_bonus_id = $bonus_id;
        $proses = $this->bonus->changeToSuccess($send_bonus_id);
        $this->dpp_reseller->changeToTerkirim($send_bonus_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }

    function success_process_date(){
        $this->load->model('dpp_reseller','',true);
        $this->load->model('send_bonus','',true);
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $tanggal_pengiriman = $this->input->post('tanggal_pengiriman');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $bonus_id = explode(",",$bonus_id);
        $reseller_id = $this->bonus->getResellerId($bonus_id);
        $reseller_id = explode(",",$reseller_id);
        $proses = $this->bonus->changeToSuccessWithDate($bonus_id, $tanggal_pengiriman);
        $this->dpp_reseller->changeToSuccessWithDate($bonus_id, $tanggal_pengiriman);
        $this->send_bonus->changeToSuccessWithDate($bonus_id, $tanggal_pengiriman);

        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);

    }

    function turnback(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $bonus_id = explode(",",$bonus_id);

        $proses = $this->bonus->changeToTurnback($bonus_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";

        }
        echo json_encode($result);
    }
    function print(){
        $this->load->model('send_bonus','',true);
        $no_generate = $this->input->get("no_generate");

        $potongan = 10000;
        $data['potongan'] = $potongan;
        $bonus = $this->send_bonus->bonusListPrint($no_generate);
        $total_bonus = $this->send_bonus->bonusTotal($no_generate);
        $data['bonus'] = $bonus;
        $data['total_bonus'] = $total_bonus;
        $data['no_generate'] = $no_generate;
        $this->load->view('admin/management-bonus/print',$data);
    }
    function porcess_data(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/management-bonus.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"no_generate"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"gross"));
        array_push($column, array("data"=>"admin"));
        array_push($column, array("data"=>"pajak"));
        array_push($column, array("data"=>"bonus_net"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array(5,6,7,8));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array("view"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-bonus/index_terproses');
        $this->load->view('admin/static/footer');
    }
    function porcess_list(){
        $this->load->model('send_bonus','',true);
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->send_bonus->send_prosessing_bonus_all();
        $result['iTotalDisplayRecords'] = $this->send_bonus->send_prosessing_bonus_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->send_bonus->send_prosessing_bonus_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->bonus_id = $key->send_bonus_id;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'" data-status="'.$key->manage_status.'"></label>';
            $key->gross = number_format($key->gross);
            $key->admin = number_format($key->admin);
            $key->pajak = number_format($key->pajak,2);
            $key->bonus_net = number_format($key->bonus_net,2);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function cancel(){
        $this->load->model('dpp_reseller','',true);
        $this->load->model('send_bonus','',true);
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $send_bonus_id = explode(",",$bonus_id);
        $proses = $this->bonus->changeTowaiting($send_bonus_id);
        $this->send_bonus->changeToCancel($send_bonus_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }
    function terkirim(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/management-bonus.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"no_generate"));
        array_push($column, array("data"=>"tanggal_terkirim"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"gross"));
        array_push($column, array("data"=>"admin"));
        array_push($column, array("data"=>"pajak"));
        array_push($column, array("data"=>"bonus_net"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array(6,7,8,9));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-bonus/index_terkirim');
        $this->load->view('admin/static/footer');
    }
    function terkirim_list(){
        $this->load->model('send_bonus','',true);
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->send_bonus->terkirim_bonus_all();
        $result['iTotalDisplayRecords'] = $this->send_bonus->terkirim_bonus_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->send_bonus->terkirim_bonus_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->bonus_id = $key->send_bonus_id;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'" data-status="'.$key->manage_status.'"></label>';
            $key->gross = number_format($key->gross);
            $key->admin = number_format($key->admin);
            $key->pajak = number_format($key->pajak,2);
            $key->bonus_net = number_format($key->bonus_net,2);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function proses_detail(){
        $send_bonus_id = $this->input->post('send_bonus_id');

        $result['data'] = $this->bonus->bonusBySendBonusId($send_bonus_id);
        echo json_encode($result);
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
