<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class TestController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pengiriman_hadiah_event','',true);
        $this->load->model('reseller','',true);

    }

    public function index()
    {
        $data = "1033,1056,346,1035,1037,1042,1041,1052,1050,1049,204,979,376,1034,512,1045,698,1057,776,218,1002,430,1038,628,719,1063,138,951,357,498,1043,676,751,216,987,409,595,711,1061,127,935,1032,461,650,739,206,986,380,542,1046,699,1058,838,219,1021,456,633,1051,732";
        $data = explode(",",$data);
        foreach ($data as $item){
            $data_pengiriman = array();
            $data_pengiriman['tanggal'] =  date("Y-m-d");
            $data_pengiriman['jumlah'] =  40000;
            $data_pengiriman['event_id'] =  8;
            $data_pengiriman['to_reseller_id'] =  $item;
            $data_pengiriman['staff_created'] =  1;
            $data_pengiriman['manage_status'] =  'waiting';
            $data_pengiriman['status'] =  'On Process';
            $data_pengiriman['keterangan'] =  'Lolos Kualifikasi';
            $this->pengiriman_hadiah_event->insert($data_pengiriman);

            $res['temp'] = 1;
            $this->reseller->update_by_id('reseller_id',$item,$res);
        }
    }
    function supres(){
        $data = "601,489,984,654,626,906,221,971,237,634,94,261,722";
        $data = explode(",",$data);
        foreach ($data as $item){
            $data_pengiriman = array();
            $data_pengiriman['tanggal'] =  date("Y-m-d");
            $data_pengiriman['jumlah'] =  80000;
            $data_pengiriman['event_id'] =  9;
            $data_pengiriman['to_reseller_id'] =  $item;
            $data_pengiriman['staff_created'] =  1;
            $data_pengiriman['manage_status'] =  'waiting';
            $data_pengiriman['status'] =  'On Process';
            $data_pengiriman['keterangan'] =  'Lolos Kualifikasi';
            $this->pengiriman_hadiah_event->insert($data_pengiriman);

            $res['temp'] = 1;
            $this->reseller->update_by_id('reseller_id',$item,$res);
        }
    }
    function date_test(){


    }
    function email(){
        $this->load->library('main');
        $this->main->mailer_auth('test','astra.danta@gmail.com','Danta','Halloo');
    }
    function test_function(){
        $this->load->model("fullpayment",'',true);
        $reseller_id = 1311;
        $result = $this->fullpayment->is_fullpayment($reseller_id);
        echo json_encode($result);
    }
    function test_tanggal_withdraw(){
        $sql = "SELECT mykindofbeauty_kemiri.reseller.reseller_id,b.tanggal from mykindofbeauty_kemiri.reseller 
LEFT JOIN (
SELECT DATE_FORMAT(max(mykindofbeauty_kemiri.pembayaran_hutang_produk.tanggal),'%Y-%m-%d') as tanggal,mykindofbeauty_kemiri.hutang_produk.reseller_id FROM mykindofbeauty_kemiri.hutang_produk
INNER JOIN mykindofbeauty_kemiri.pembayaran_hutang_produk on mykindofbeauty_kemiri.pembayaran_hutang_produk.pembayaran_hutang_produk_id = mykindofbeauty_kemiri.hutang_produk.pembayaran_hutang_produk_id
GROUP BY mykindofbeauty_kemiri.hutang_produk.reseller_id
) as b on b.reseller_id = mykindofbeauty_kemiri.reseller.reseller_id
WHERE status = 'active'
and demo = 0
and type = 'super agen'";
        $data = $this->db->query($sql)->result_array();
        foreach ($data as $item){
            $tanggal_withdraw = array();
            $tanggal_withdraw["reseller_id"] = $item['reseller_id'];
            if($item['tanggal']!=null){
                $tanggal_withdraw["tanggal"] = $item['tanggal'];
            }
            $cek = $this->db->where('reseller_id',$item['reseller_id'])->get('mykindofbeauty_kemiri.tanggal_withdraw')->result();
            if(sizeof($cek)>0){
                $this->db->where('mykindofbeauty_kemiri.tanggal_withdraw.reseller_id',$item['reseller_id'])->update('mykindofbeauty_kemiri.tanggal_withdraw',$tanggal_withdraw);
            }else{
                $this->db->insert('mykindofbeauty_kemiri.tanggal_withdraw',$tanggal_withdraw);
            }
        }

    }
}

/* End of file SuplierController.php */
/* Location: ./application/controllers/SuplierController.php */