<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class UserGuideController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('string');
	}
	public function index()
    {

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "User Guide < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/user-guide/index');
        $this->load->view('admin/static/footer');
    }
}
