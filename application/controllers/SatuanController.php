<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SatuanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('satuan','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Satuan < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"satuan_nama"));
		
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['satuan'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);	
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/satuan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->satuan->satuan_count_all();
		$result['iTotalDisplayRecords'] = $this->satuan->satuan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->satuan->satuan_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'satuan/delete/';
            $key->konversi_satuan_url = base_url().'konversi-satuan/'.$this->encrypt_id($key->satuan_id);
			$key->row_id = $key->satuan_id;

		}
		$result['aaData'] = $data;	
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
			$satuan_nama = $this->input->post('satuan_nama');
			$data = array("satuan_nama"=>$satuan_nama);
			$insert = $this->satuan->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["satuan_nama"] = $this->input->post('satuan_nama');
		$satuan_id = $this->input->post('satuan_id');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->satuan->update_by_id('satuan_id',$satuan_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->satuan->delete_by_id("satuan_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */