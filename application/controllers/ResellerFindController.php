<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ResellerFindController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reseller','',true);
        $this->load->model('province','',true);
        $this->load->library('main');
        $this->load->helper('string');

    }

    function getCity(){
        $province_id = $this->input->post('id');
        // $province_id = $this->input->post('province_id');
        // $search = $this->input->post('search');

	    $data = $this->db
            ->select('*')
            ->where('province_id', $province_id)
            ->get('city')
            ->result();

            if(!empty(count($data))){
                echo "<option value='' selected>- Semua Kabupaten -</option>";
                foreach($data as $city){
                    echo "<option value='".$city->city_id."'>".$city->city_name."</option>";
                }
            }else{
                echo "
                <option value='' selected>- Pilih Kabupaten -</option>
                <option disabled>Tidak ada Kabupaten</option>
                ";
            }
    }


    function getSubdistrict(){
        $city_id = $this->input->post('id');

	    $data = $this->db
            ->select('*')
            ->where('city_id', $city_id)
            ->get('subdistrict')
            ->result();

            if(!empty(count($data))){
                echo "<option value='' selected>- Semua Kecamatan -</option>";
                foreach($data as $subdistrict){
                    echo "<option value='".$subdistrict->subdistrict_id."'>".$subdistrict->subdistrict_name."</option>";
                }
            }else{
                echo "
                <option value='' selected>- Pilih Kecamatan -</option>
                <option disabled>Tidak ada Kecamatan</option>
                ";
            }
    }

    
}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */