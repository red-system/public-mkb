<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonfirmasiTransferBahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bahan','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('history_transfer_bahan');
	}

	public function index()
	{
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/konfirmasi_transfer.js");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = " Transfer Stock Bahan < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = 'transfer-bahan';
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"tanggal"));
		array_push($column, array("data"=>"bahan_kode"));
		array_push($column, array("data"=>"bahan_nama"));
		array_push($column, array("data"=>"dari"));
		array_push($column, array("data"=>"tujuan"));
		array_push($column, array("data"=>"history_transfer_qty"));
		array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
		$data['sumColumn'] = json_encode(array(6));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,6)));
		$data["action"] = json_encode(array("confirmation"=>true));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/konfirmasi_transfer_bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->history_transfer_bahan->need_confirm_count_all();
		$result['iTotalDisplayRecords'] = $this->history_transfer_bahan->need_confirm_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->history_transfer_bahan->need_confirm_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->row_id = $key->bahan_id;
			$key->history_transfer_qty = number_format($key->history_transfer_qty);
			$i++;
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function confirm(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data["keterangan"] = $this->input->post('keterangan');
		$qty_awal = $this->string_to_number($this->input->post('history_transfer_qty'));
		$qty_terima = $qty_awal;
		$status = $this->input->post('status_history');
		$data["status"] = $status;
		if($status == "Diterima Sebagian"){
			$qty_terima = $this->string_to_number($this->input->post('qty_terima'));
		} else if($data['status'] == "Ditolak"){
			$qty_terima =0;
		}
		$data['qty_terima'] = $qty_terima;
		$data["tanggal_konfirmasi"] = date("Y-m-d");
		$history_transfer_bahan_id = $this->input->post('history_transfer_bahan_id');
		//mengubah status dan jumlah terima
		$update = $this->history_transfer_bahan->update_by_id('history_transfer_bahan_id',$history_transfer_bahan_id,$data);
		$arus = array();
		if ($update){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
			$history = $this->history_transfer_bahan->row_by_id($history_transfer_bahan_id);
			if ($status == "Diterima Semua" || $status == "Diterima Sebagian"){
				$check = $this->stock_bahan->is_bahan_seri_ready_arr($history->bahan_seri,$history->bahan_id,$history->histori_lokasi_tujuan_id);
				if($check["status"]){
					$data = array();
					$data["bahan_id"] = $history->bahan_id;
					$data["stock_bahan_lokasi_id"] = $history->histori_lokasi_tujuan_id;
					$data["stock_bahan_seri"] = $history->bahan_seri;
					$data["stock_bahan_qty"] = $qty_terima;
					$this->stock_bahan->insert($data);
					$arus["keterangan"] = "Penerimaan transfer stok";
					$arus["method"] = "insert";
					$arus["stock_bahan_id"] = $this->stock_bahan->last_id();

				} else {
					$data = array();
					$data["stock_bahan_qty"] = $qty_terima + $check["data"]->stock_bahan_qty;
					$this->stock_bahan->update_by_id('stock_bahan_id',$check["data"]->stock_bahan_id,$data);
					$arus["keterangan"] = "Penerimaan transfer stok";
					$arus["method"] = "update";
					$arus["stock_bahan_id"] = $check["data"]->stock_bahan_id;
				}
				if($status == "Diterima Sebagian"){
					$sisa = $qty_awal - $qty_terima;
					$check = $this->stock_bahan->is_bahan_seri_ready_arr($history->bahan_seri,$history->bahan_id,$history->histori_lokasi_awal_id);
					$data = array();
					$data["stock_bahan_qty"] = $sisa + $check["data"]->stock_bahan_qty;
					$arus_sebagian["stock_in"] = $sisa;
					$arus_sebagian["stock_bahan_id"] = $check["data"]->stock_bahan_id;
					$this->stock_bahan->update_by_id('stock_bahan_id',$check["data"]->stock_bahan_id,$data);
					$arus_sebagian["keterangan"] = "Stok yang tidak diterima";
					$arus_sebagian["method"] = "update";
				}

			} else if ($status == "Ditolak") {
				$check = $this->stock_bahan->is_bahan_seri_ready_arr($history->bahan_seri,$history->bahan_id,$history->histori_lokasi_awal_id);
				$data = array();
				$data["stock_bahan_qty"] = $history->history_transfer_qty + $check["data"]->stock_bahan_qty;
				$arus["stock_bahan_id"] = $check["data"]->stock_bahan_id;
				$this->stock_bahan->update_by_id('stock_bahan_id',$check["data"]->stock_bahan_id,$data);
				$arus["keterangan"] = "Penolakan transfer stok";
				$arus["method"] = "update";

			} 
			
			$arus["tanggal"] = date("Y-m-d");
			$arus["table_name"] = "stock_bahan";
			$arus["bahan_id"] = $history->bahan_id;
			$arus["stock_out"] = 0;
			$arus["stock_in"] = $qty_terima;
			$arus["last_stock"] = $this->stock_bahan->last_stock($history->bahan_id)->result;
			$arus["last_stock_total"] = $this->stock_bahan->stock_total()->result;
			 $this->stock_bahan->arus_stock_bahan($arus);
			if($status == "Diterima Sebagian"){
				$arus["stock_bahan_id"] = $arus_sebagian["stock_bahan_id"];
				$arus["keterangan"] = $arus_sebagian["keterangan"];
				$arus["method"] = $arus_sebagian["method"];
				$arus["stock_in"] = $arus_sebagian["stock_in"];
				$this->stock_bahan->arus_stock_bahan($arus);
			}
		}
		echo json_encode($result);
	}

}

/* End of file KonfirmasiTransferBahanController.php */
/* Location: ./application/controllers/KonfirmasiTransferBahanController.php */