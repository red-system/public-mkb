<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotifController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
        $this->load->model('po_kutus_kutus','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('penerimaan_produk','',true);
        $this->load->model('penerimaan_produk_detail','',true);
        $this->load->model('notif','',true);
        $this->load->model('notif_detail','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/notif.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Notifikasi MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "notification";
        $data['page'] = "notification";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"notif_date"));
        array_push($column, array("data"=>"notif_title"));
        array_push($column, array("data"=>"notif_description_lbl"));
        array_push($column, array("data"=>"notif_status_lbl", "template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();

        foreach ($akses_menu['notification'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $action['view'] = true;
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/notif/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->notif->notif_count_all();
        $result['iTotalDisplayRecords'] = $this->notif->notif_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->notif->notif_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {

            if ($_SESSION["status_notif"] == true) {
                $reseller_id = $_SESSION["redpos_login"]["reseller_id"];
                $where = [
                    'reseller_id' => $reseller_id,
                    'notif_id' => $key->notif_id,
                ];
                $check_notif = $this->notif->notif_check($where)->num_rows();
                if ($check_notif == 0) {
                    $key->notif_status_lbl = "Baru";
                }else{
                    $key->notif_status_lbl = "Sudah dibaca";
                }
    
            }else{
                $key->notif_status_lbl = $key->notif_status;
            }
            
            if($key->notif_date != null){
                $time = strtotime($key->notif_date);
                $key->notif_date = date('d-m-Y',$time);
            }

            $key->notif_description_lbl = (strlen($key->notif_description) >= 50 ) ? $this->main->short_desc($key->notif_description) : $key->notif_description;
            
            $key->edit_url = base_url().'notification/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->notif_id));
            $key->view_url = base_url().'notification/detail-notification/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->notif_id));
            $key->no = $i;
            
            $i++;
            $key->delete_url = base_url().'notification/delete/';
            $key->row_id = $key->notif_id;
            $key->action = null;
            
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "vendors/general/tinymce/js/tinymce/tinymce.min.js");
        array_push($this->js, "script/admin/notif.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Notifikasi MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "notification";
        $data['page'] = $this->uri->segment(1);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/notif/add');
        $this->load->view('admin/static/footer');
    }
    
    function save_add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['notif_date'] = $this->input->post('notif_date');
        $data['notif_title'] = $this->input->post('notif_title');
        $data['notif_description'] = $this->input->post('notif_description');
        $data['notif_status'] = $this->input->post('notif_status');
        $insert = $this->notif->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $notif = $this->notif->row_by_id($id);
        if ($notif != null) {
            $data['notif'] = $notif;
            
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "vendors/general/tinymce/js/tinymce/tinymce.min.js");
            array_push($this->js, "script/admin/notif.js");

            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Notifikasi < ".$_SESSION["redpos_company"]['company_name'];
            $data['parrent'] = "notification";
            $data['page'] = $this->uri->segment(1);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/notif/edit');
            $this->load->view('admin/static/footer');
        }else {

            redirect('404_override','refresh');
        }
    }
    function save_edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['notif_date'] = $this->input->post('notif_date');
        $data['notif_title'] = $this->input->post('notif_title');
        $data['notif_description'] = $this->input->post('notif_description');
        $data['notif_status'] = $this->input->post('notif_status');

        $notif_id = $this->input->post('notif_id');

        $update = $this->notif->update_by_id('notif_id',$notif_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->notif->delete_by_id("notif_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function detail(){
        $id = $this->input->post('notif_id');
        $temp = $this->notif->row_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        
        if($temp->notif_date != null){
            $x = strtotime($temp->notif_date);
            $temp->notif_date = date("d-m-Y",$x);
        }
        
        echo json_encode($temp);
    }

    function detail_notif(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);

        if ($_SESSION["status_notif"] == true) {
            $reseller_id = $_SESSION["redpos_login"]["reseller_id"];
            $where = [
                'reseller_id' => $reseller_id,
                'notif_id' => $id
            ];
            $check_notif = $this->notif->notif_check($where)->num_rows();
            if ($check_notif == 0) {
                $data_notif['notif_id'] = $id;
                $data_notif['reseller_id'] = $reseller_id;
                $insert_notif = $this->notif_detail->insert($data_notif);
            }

        }

        $notif = $this->notif->row_by_id($id);
        if ($notif != null) {
            $data['notif'] = $notif;
            
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "vendors/general/tinymce/js/tinymce/tinymce.min.js");
            array_push($this->js, "script/admin/notif.js");

            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Notifikasi < ".$_SESSION["redpos_company"]['company_name'];
            $data['parrent'] = "notification";
            $data['page'] = $this->uri->segment(1);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/notif/detail');
            $this->load->view('admin/static/footer');
        }else {

            redirect('404_override','refresh');
        }
    }

}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */