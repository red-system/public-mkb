<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterAksesController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_type','',true);
        $this->load->library('master');

    }

    public function index()
    {
        array_push($this->master->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->master->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->master->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->master->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->master->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->master->js, "script/admin/akses_role.js");
        $menu = $this->master_type->all_menu();
        $submenu = array();
        foreach ($menu as $item){
            $submenu['_'.$item->menu_id] = $this->master_type->submenu_by_menu($item->menu_id);
        }
        $data['menu'] = $menu;
        $data['submenu'] = $submenu;
        $data["css"] = $this->master->css;
        $data["js"] = $this->master->js;
        $column = array();
        $data["meta_title"] = "Akses User Role < Type < Redpos";
        $data['parrent'] = "master_data";
        $data['page'] = $this->uri->segment(1);
        $data['menu'] = $menu;
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(4));
        $id = $this->encryption->decrypt($url);
        $role = $this->master_type->row_by_id($id);
        $data['id'] = $id;
        if ($role != null) {
            $data['akses_role'] = json_decode($this->master_type->get_akses_role($id),true);

            $this->load->view('master/static/header',$data);
            $this->load->view('master/static/sidebar');
            $this->load->view('master/static/topbar');
            $this->load->view('master/akses_role/index');
            $this->load->view('master/static/footer');
        } else {

            redirect('404_override','refresh');
        }

    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Gagal mengubah data";
        $role = $this->input->post('role');
        $menu = isset($role["menu"]) ? $role["menu"] : array();
        $submenu = isset($role["submenu"]) ? $role["submenu"] : array();
        $temp = $this->master_type->all_menu();
        $menuDatabase = array();
        foreach ($temp as $itemMenu){
            $menuDatabase['_'.$itemMenu->menu_id] = $itemMenu;
        }
        $temp = $this->master_type->all_submenu();
        $submenuDatabase = array();
        foreach ($temp as $itemMenu){
            $submenuDatabase['_'.$itemMenu->menu_id]['_'.$itemMenu->sub_menu_id] = $itemMenu;
        }

        //echo json_encode($submenuDatabase);
        $menuData = array();
        foreach ($menu as $key =>$item ){
            $menu_id = str_replace('_','',$key);
            $temp = $menuDatabase['_'.$menu_id];
            $menuData[$key] = $temp;
        }
        $submenuData = array();
        foreach ($submenu as $key =>$item){
            foreach ($item as $key2 =>$item2){

                $submenuData[$key][$key2] = $submenuDatabase[$key][$key2];
            }
        }
        $akses['menu'] = $menuData;
        $akses['submenu'] = $submenuData;
        $id = $this->uri->segment(5);
        $data['type_role'] = json_encode($akses);
        $update = $this->master_type->update_by_id('id',$id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }

}