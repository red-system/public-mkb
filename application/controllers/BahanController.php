<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bahan','',true);
		$this->load->model('jenis_bahan','',true);
		$this->load->model('satuan','',true);
		$this->load->model('suplier','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");

		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
	
		array_push($this->js, "script/app2.js");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Bahan < Inventori < ".$_SESSION["redpos_company"]['company_name'];
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$target = array(0,3);
		$sumColumn = array(3);
		array_push($column, array("data"=>"no"));
		// array_push($column, array("data"=>"bahan_kode"));
		array_push($column, array("data"=>"bahan_nama"));
		// array_push($column, array("data"=>"jenis_bahan_nama"));
		array_push($column, array("data"=>"satuan_nama"));
		array_push($column, array("data"=>"stock"));
		// if(isset($_SESSION["redpos_login"]['lokasi_id'])){
		// 	array_push($column, array("data"=>"jumlah_lokasi"));
		// 	array_push($target, 6);
		// 	array_push($sumColumn, 6);
		// }
		// array_push($column, array("data"=>"suplier_nama"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['inventori']['bahan'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		// $data["jenis_bahan"] = $this->jenis_bahan->all_list();
		$data["satuan"] = $this->satuan->all_list();
		// $data["suplier"] = $this->suplier->all_list();
		$data['sumColumn'] = json_encode($sumColumn);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bahan->bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->bahan->bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bahan->bahan_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'bahan/delete/';
			$key->row_id = $key->bahan_id;
			$key->stok_url = base_url().'stock-bahan/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->bahan_id));
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
			$key->stock = number_format($key->stock);
			$key->bahan_harga_lbl = "Rp. ". number_format($key->bahan_harga,11,".",",");
			$key->unit_url = base_url().'bahan-unit/'.$this->encrypt_id($key->bahan_id);
			// if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			// 	$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			// }
		}
		$result['aaData'] = $data;				
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		// $result['message'] = "Kode ini telah terdaftar";
		$result['message'] = "Gagal menyimpan data";
		// $this->form_validation->set_rules('bahan_kode', '', 'required|is_unique[bahan.bahan_kode]');
		// if ($this->form_validation->run() == TRUE) {
			// $data["bahan_kode"] = $this->input->post('bahan_kode');
			$data["bahan_nama"] = $this->input->post('bahan_nama');
			// $data["bahan_jenis_id"] = $this->input->post('bahan_jenis_id');
			$data["bahan_satuan_id"] = $this->input->post('bahan_satuan_id');
			// $data["bahan_suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));
			$data["bahan_minimal_stock"] = $this->string_to_number($this->input->post('bahan_minimal_stock'));
			$data["bahan_keterangan"] = $this->input->post('bahan_keterangan');
			$insert = $this->bahan->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} 
			// else {
			// 	$result['message'] = "Gagal menyimpan data";
			// }
		// }
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data = array();
		// $data["bahan_kode"] = $this->input->post('bahan_kode');
		$data["bahan_nama"] = $this->input->post('bahan_nama');
		// $data["bahan_jenis_id"] = $this->input->post('bahan_jenis_id');
		$data["bahan_satuan_id"] = $this->input->post('bahan_satuan_id');
		// $data["bahan_suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));
		$data["bahan_minimal_stock"] = $this->string_to_number($this->input->post('bahan_minimal_stock'));
		$data["bahan_keterangan"] = $this->input->post('bahan_keterangan');
		$updated_at = date('Y-m-d H:i:s');
		$data['updated_at'] = $updated_at;
		$bahan_id = $this->input->post('bahan_id');
		$update = $this->bahan->update_by_id('bahan_id',$bahan_id,$data);
		if($update){
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		} else {
			$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->bahan->delete_by_id("bahan_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file BahanController.php */
/* Location: ./application/controllers/BahanController.php */