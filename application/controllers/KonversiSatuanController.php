<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonversiSatuanController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('konversi_satuan','',true);
        $this->load->model('satuan','',true);

    }

    public function index()
    {

        $id = $this->dencrypt_id($this->uri->segment(2));
        $satuan = $this->satuan->row_by_id($id);
        if($satuan != null) {
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->css, "vendors/general/select2/dist/css/select2.css");
            array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
            array_push($this->js, "script/app2.js");

            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Konversi Satuan < Master Data < " . $_SESSION["redpos_company"]['company_name'];;
            $data['parrent'] = "master_data";
            $data['page'] = "satuan";
            $data['id'] = $id;
            $data['satuan_list'] = $this->satuan->all_list();
            array_push($column, array("data" => "no"));
            array_push($column, array("data" => "satuan_nama"));
            array_push($column, array("data" => "jumlah_konversi"));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0, 2)));
            $akses_menu = json_decode($this->menu_akses, true);
            $action = array("edit" => true, "delete" => true);
            $data['action'] = json_encode($action);
            $data['satuan'] = $satuan;
            $this->load->view('admin/static/header', $data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/konversi_satuan');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function list(){
        $id = $this->dencrypt_id($this->uri->segment(2));
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->konversi_satuan->konversi_satuan_count_all($id);
        $result['iTotalDisplayRecords'] = $this->konversi_satuan->konversi_satuan_count_filter($query,$id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->konversi_satuan->konversi_satuan_list($start,$length,$query,$id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'konversi-satuan-delete';
            $key->row_id = $key->konversi_satuan_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $data["id_satuan_awal"] = $this->input->post("id_satuan_awal");
        $data["id_satuan_akhir"] = $this->input->post("id_satuan_akhir");
        $data["jumlah_konversi"] = $this->string_to_decimal($this->input->post("jumlah_konversi"));
        $data["created_at"] = Date("Y-m-d H:i:s");
        $insert = $this->konversi_satuan->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Kode sudah terdaftar";
        $data = array();
        $data["id_satuan_awal"] = $this->input->post("id_satuan_awal");
        $data["id_satuan_akhir"] = $this->input->post("id_satuan_akhir");
        $data["jumlah_konversi"] = $this->string_to_decimal($this->input->post("jumlah_konversi"));
        $konversi_satuan_id = $this->input->post('konversi_satuan_id');
        $updated_at = date('Y-m-d H:i:s');
        $data['updated_at'] = $updated_at;
        $update = $this->konversi_satuan->update_by_id('konversi_satuan_id',$konversi_satuan_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->konversi_satuan->delete_by_id("konversi_satuan_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

}

/* End of file KonversiBahanController.php */
/* Location: ./application/controllers/KonversiBahanController.php */
