<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class HadiahEventController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('hadiah_event','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');


    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $sumColumn = array(3);
        $column = array();
        $data["meta_title"] = "Hadiah Event < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = $this->uri->segment(1);
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"jumlah"));
        array_push($column, array("data"=>"nama_event"));
        array_push($column, array("data"=>"keterangan"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array(3));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/hadiah-event/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['estimasi_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['estimasi_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        if(isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][7]["search"]["value"]);
            $_GET['pengiriman_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['pengiriman_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->hadiah_event->hadiah_event_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->hadiah_event->hadiah_event_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->hadiah_event->hadiah_event_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah = number_format($key->jumlah);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;
            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }


}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
