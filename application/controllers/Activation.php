<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activation extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reseller','',true);
        $this->load->model('user_role','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('staff','',true);
        $this->load->model('activasi','',true);
        $this->load->model('master_login','',true);
        $this->load->library('form_validation');
    }


    public function index()
    {
        $link = $this->uri->segment(2);
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
        $id = $this->encryption->decrypt($url);

        $check = $this->activasi->check($link,$id);

        $reseller = $this->reseller->row_by_id($id);

        $data['reseller'] = $reseller;
        $data['link'] = $link;
        $data["bank"] = $this->db->get("mykindofbeauty_kemiri.bank")->result();
        if ($reseller != null && $check) {
            $this->load->view('admin/activation2',$data);
        } else {
            redirect('404_override','refresh');

        }
    }
    function cek(){
        echo str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($this->uri->segment(2)));
    }
    function save(){
        $result['success'] = false;
        $result['message'] = "Parameter tidak sesuai";
        $this->form_validation->set_rules('reseller_id', 'ID Reseller', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric|is_unique_master[login.username]',array(
            'is_unique_master'     => 'This %s already exists.',
            'alpha_numeric'     => 'Just alphabetical and numeric characters allowed with lowercase format'
        ));
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('retype_password', 'Retype Password', 'required|matches[password]');
        $this->form_validation->set_rules('bank_id', 'Bank', 'required');
        $this->form_validation->set_rules('bank_rekening', 'Akun Bank', 'required');
        $this->form_validation->set_rules('bank_atas_nama', 'Atas Nama Akun Bank', 'required');
        $this->form_validation->set_rules('bank_cabang', 'Bank Cabang', 'required');
        $this->form_validation->set_error_delimiters('<span class="error-message" style="color:red">', '</span>');
        if ($this->form_validation->run() === TRUE) {

            $reseller_id = $this->input->post('reseller_id');
            $reseller = $this->reseller->row_by_id($reseller_id);

            $email = $reseller->email;
            $old_email = '#'.$email;

            $old_account = $this->reseller->row_by_field('email',$old_email);
            if (!empty($old_account)) {
                $old_reseller_id = $old_account->reseller_id;
                $downline_reseller = $this->reseller->reseller_downline($old_reseller_id);

                $leader['leader'] = $reseller_id;
                $update_leader = $this->reseller->update_by_id('reseller_id', $reseller_id, $leader);

                foreach ($downline_reseller as $downline) {
                    $downline_id = $downline->reseller_id;
                    $data_reseller['referal_id'] = $reseller_id;
                    $data_reseller['leader'] = $reseller_id;
                    $update_referal = $this->reseller->update_by_id('reseller_id', $downline_id, $data_reseller);
                }
            }

            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $bank_rekening = $this->input->post('bank_rekening');
            $bank_atas_nama = $this->input->post('bank_atas_nama');
            $bank_id = $this->input->post('bank_id');
            $bank_cabang = $this->input->post('bank_cabang');

            $resData['bank_rekening'] =$bank_rekening ;
            $resData['bank_atas_nama'] =$bank_atas_nama ;
            $resData['bank_id'] =$bank_id ;
            $resData['bank_cabang'] = $bank_cabang ;
            $this->reseller->update_by_id('reseller_id',$reseller_id,$resData);
            $link = $this->input->post('link',true);

            $activasiData = array();
            $activasiData['reseller_id'] = $reseller_id;
            $activasiData['token'] = $link;

            $this->activasi->insert($activasiData);
            $password = password_hash($password, PASSWORD_BCRYPT);
            $data = array("status"=>"pasive");
            if($reseller->type=="redgroup"){
                $lokasi_alamat = $reseller->alamat;
                $data = array("status"=>"active");
            }
            $this->reseller->update_by_id('reseller_id',$reseller_id,$data);
            $lokasi_nama = $reseller->nama;
            $lokasi_tipe = "Toko";
            $lokasi_alamat = $reseller->outlet_alamat;
            if($reseller->type=="redgroup"){
                $lokasi_alamat = $reseller->alamat;
            }
            $lokasi_telepon = $reseller->phone;
            $data = array("lokasi_nama"=>$lokasi_nama,"lokasi_tipe"=>$lokasi_tipe,"lokasi_alamat"=>$lokasi_alamat,"lokasi_telepon"=>$lokasi_telepon);
            $this->lokasi->insert($data);
            $lokasi_id = $this->lokasi->last_id();
            $data = array("nik"=>$reseller->no_ktp,"staff_nama"=>$reseller->nama,"staff_alamat"=>$reseller->alamat,"staff_status"=>"Agent","mulai_bekerja"=>date("Y-m-d"),"staff_email"=>$reseller->email,"staff_phone_number"=>$reseller->phone);
            $insert = $this->staff->insert($data);
            $staff_id = $this->staff->last_id();
            $user_role_id = $reseller->type == "agent" ? 6 : 7;
            $current_datetime = new DateTime(date("Y-m-d H:i:s"));
            $current_datetime->modify("+1 day");
            $exp_time = $current_datetime->format("Y-m-d H:i:s");
            $data = array(
                "username"=>$username,
                "password"=>$password,
                "reseller_id"=>$reseller_id,
                "user_staff_id"=>$staff_id,
                "company_id"=>3,
                "user_role_id"=>$user_role_id,
                "lokasi_id"=>$lokasi_id,
                "avatar"=>$reseller->foto_profile,
                "expired_time"=>$exp_time
            );
            if($reseller->type=="redgroup"){
                $data["user_role_id"] = 8;
            }
            $insert = $this->master_login->insert($data);
            $user_id = $this->master_login->last_id();
            $user_role = $this->user_role->row_by_id($user_role_id);
            $_SESSION["redpos_login"]["user_id"] = $user_id;
            $_SESSION["redpos_login"]["user_role_id"] = $user_role_id;
            $_SESSION["redpos_login"]["avatar"] = $reseller->foto_profile;
            $_SESSION["redpos_login"]["staff_id"] = $staff_id;
            $_SESSION["redpos_login"]["db_name"] = "mykindofbeauty_kemiri";
            $_SESSION["redpos_login"]["company_id"] = 3;
            $_SESSION["redpos_login"]["pay"] = 0;
            $_SESSION["redpos_login"]["pay-c"] = null;
            $_SESSION["redpos_login"]["pay-s"] = null;
            $_SESSION["redpos_login"]["pay-prod"] = false;
            $_SESSION["redpos_login"]["lokasi_id"] = $lokasi_id;
            $_SESSION["redpos_login"]["user_name"] = $reseller->nama;
            $_SESSION["redpos_login"]["user_role_name"] = $user_role->user_role_name;
            $_SESSION["redpos_login"]["email"] = $reseller->email;
            $_SESSION["redpos_company"]["company_name"] = "Agen MKB";
            $_SESSION["redpos_company"]["company_full_name"] = "Agen MKB";
            $_SESSION["redpos_company"]["company_email"] = $reseller->email;
            $_SESSION["redpos_company"]["company_address"] = $reseller->outlet_alamat;
            $_SESSION["redpos_company"]["company_phone"] = $reseller->phone;
            $_SESSION["redpos_company"]["max_user"] = 1;
            $_SESSION["redpos_company"]["max_outlet"] = 1;
            $_SESSION["redpos_company"]["database"] = "mykindofbeauty_kemiri";
            if($reseller->type=="redgroup"){
                $_SESSION["redpos_login"]["user_role_id"] = 8;
            }
            if($insert){
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }
            echo json_encode($result);

        }else{
            echo json_encode(
                array(
                    'status' => 'error',
                    'message' => 'Fill form completly',
                    'errors' => array(
                        'username' => form_error('username'),
                        'password' => form_error('password'),
                        'retype_password' => form_error('retype_password'),
                        'reseller_id' => form_error('reseller_id'),
                    )
                )
            );
        }
    }

}

/* End of file StockProdukController.php */
/* Location: ./application/controllers/StockProdukController.php */