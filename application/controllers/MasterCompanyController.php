<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterCompanyController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_company','',true);
        $this->load->model('master_type','',true);
        $this->load->library('master');
        if(!isset($_SESSION['master'])){
            redirect(base_url().'master/login');
        }

    }

    public function index()
    {
        array_push($this->master->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->master->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->master->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->master->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->master->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->master->js, "script/app2.js");
        array_push($this->master->js, "script/master/company.js");

        $data["css"] = $this->master->css;
        $data["js"] = $this->master->js;
        $column = array();
        $data["meta_title"] = "Company < Master < Redpos";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"company_name"));
        array_push($column, array("data"=>"company_full_name"));
        array_push($column, array("data"=>"company_email"));
        array_push($column, array("data"=>"company_address"));
        array_push($column, array("data"=>"company_phone"));
        array_push($column, array("data"=>"database_name"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $action = array("add"=>true,"edit"=>true,"delete"=>true,"enter"=>true);
        $data['action'] = json_encode($action);
        $data['type'] = $this->master_type->all_list();
        $this->load->view('master/static/header',$data);
        $this->load->view('master/static/sidebar');
        $this->load->view('master/static/topbar');
        $this->load->view('master/company/index');
        $this->load->view('master/static/footer');
    }

    function options(){
        $data = array();
        $size = $this->size->all_list();
        foreach ($size as $key) {
            array_push($data, strtolower($key->size_nama));
        }
        echo json_encode($data);
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->master_company->count_all();
        $result['iTotalDisplayRecords'] = $this->master_company->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->master_company->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'master/company/delete/';
            $key->enter_url = base_url().'master/company/enter/';
            $key->row_id = $key->id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $company_name = $this->input->post('company_name');
        $company_full_name = $this->input->post('company_full_name');
        $company_email = $this->input->post('company_email');
        $company_phone = $this->input->post('company_phone');
        $company_address = $this->input->post('company_address');
        $database_name = $this->input->post('database_name');
        $type_id = $this->input->post('type_id');
        $max_user = $this->input->post('max_user');
        $max_outlet = $this->input->post('max_outlet');
        $data = array(
            "company_name"=>$company_name,
            "company_full_name"=>$company_full_name,
            "company_email"=>$company_email,
            "company_phone"=>$company_phone,
            "company_address"=>$company_address,
            "database_name"=>$database_name,
            "max_user"=>$max_user,
            "max_outlet"=>$max_outlet,
            "type_id"=>$type_id,
        );
        $this->master_company->start_trans();
        $insert = $this->master_company->insert($data);
        $this->update_fitur($database_name,$type_id);
        $process = $this->master_company->result_trans();
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Kode sudah terdaftar";
        $id = $this->input->post('id');
        $company_name = $this->input->post('company_name');
        $company_full_name = $this->input->post('company_full_name');
        $company_email = $this->input->post('company_email');
        $company_phone = $this->input->post('company_phone');
        $company_address = $this->input->post('company_address');
        $database_name = $this->input->post('database_name');
        $max_user = $this->input->post('max_user');
        $max_outlet = $this->input->post('max_outlet');
        $type_id = $this->input->post('type_id');
        $data = array(
            "company_name"=>$company_name,
            "company_full_name"=>$company_full_name,
            "company_email"=>$company_email,
            "company_phone"=>$company_phone,
            "company_address"=>$company_address,
            "database_name"=>$database_name,
            "max_user"=>$max_user,
            "max_outlet"=>$max_outlet,
            "type_id"=>$type_id,
        );
        $this->master_company->start_trans();
        $update = $this->master_company->update_by_id('id',$id,$data);
        $this->update_fitur($database_name,$type_id);
        $process = $this->master_company->result_trans();
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function update_fitur($db_name,$type_id){
        $this->master_company->empty_menu($db_name);
        $this->master_company->empty_sub_menu($db_name);
        $role = $this->master_type->get_akses_role($type_id);
        $role = json_decode($role,true);
        $menu = $role["menu"];
        $submenu = $role["submenu"];
        $menuData = array();
        foreach ($menu as $item ){
            $menuItem = array();
            foreach ($item as $key=>$item2){
                if($key!='created_at'&&$key!='updated_at'){
                    $menuItem[$key] = $item2;
                }
            }
            array_push($menuData,$menuItem);
        }
        $this->master_company->insert_batch_menu($db_name,$menuData);
        $menuData = array();
        foreach ($submenu as $item ){


            foreach ($item as $key=>$item2){
                $menuItem = array();
               foreach ($item2 as $key2 =>$item3){
                   if($key!='created_at'&&$key!='updated_at'){
                       $menuItem[$key2] = $item3;
                   }
               }
                array_push($menuData,$menuItem);
            }

        }
        $this->master_company->insert_batch_sub_menu($db_name,$menuData);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->master_company->delete_by_id("id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function enter(){
        $id = $this->input->post('id');
        $company = $this->master_company->row_by_id($id);
        $_SESSION["redpos_login"]["user_id"] = $_SESSION['master']['id'];
        $_SESSION["redpos_login"]["user_role_id"] = 0;
        $_SESSION["redpos_login"]["avatar"] = "assets/media/users/user.jpg";
        $_SESSION["redpos_login"]["staff_id"] = 0;
        $_SESSION["redpos_login"]["db_name"] = $company->database_name;
        $_SESSION["redpos_login"]["user_name"] = "Super Admin";
        $_SESSION["redpos_login"]["email"] = "super-admin@redpos.id";
        $_SESSION["redpos_login"]["company_id"] = $id;
        $_SESSION["redpos_company"]["company_name"] = $company->company_name;
        $_SESSION["redpos_company"]["company_full_name"] = $company->company_full_name;
        $_SESSION["redpos_company"]["company_email"] = $company->company_email;
        $_SESSION["redpos_company"]["company_address"] = $company->company_address;
        $_SESSION["redpos_company"]["company_phone"] = $company->company_phone;
        $_SESSION["redpos_company"]["database"] = $company->database_name;
        $result['success'] = true;
        $result['message'] = "Berhasil masuk";
        echo json_encode($result);
    }

}

/* End of file SizeController.php */
/* Location: ./application/controllers/SizeController.php */
