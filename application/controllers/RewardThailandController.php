<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class RewardThailandController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reward_thailand');
    }

    public function index()
    {
        $query = $this->input->get('search')["value"];
        $start = 0;
        $length = $this->reward_thailand->_count_all();
        $result['iTotalRecords'] = $length;
        $result['iTotalDisplayRecords'] = $this->reward_thailand->_count_filter($query);
        $data =  $this->reward_thailand->_list($start,$length,$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    public function excel(){
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->reward_thailand->_count_all();
        $list =  $this->reward_thailand->_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {
            $key->no = $i;
            $i++;
        }
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
            ->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
            ->setTitle('Reward Thailand')
            ->setSubject('');
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            )
        );
        $border = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        );

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Nama')
            ->setCellValue('C1', 'Poin Reseller')
            ->setCellValue('D1', 'Poin Super')
            ->setCellValue('E1', 'Reseller Baru')
            ->setCellValue('F1', 'Super Reseller Baru')
            ->setCellValue('G1', 'Total Poin')
            ->setCellValue('H1', 'Total Join')

        ;
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(28);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(18);
        $spreadsheet->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $date = date("Y-m-d");
        $grand_total = 0;
        $laba = 0;
        $total_hpp = 0;
        $i=2; foreach($list as $key) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key->no)
                ->setCellValue('B'.$i, $key->nama)
                ->setCellValue('C'.$i, $key->reseller_poin)
                ->setCellValue('D'.$i, $key->super_poin)
                ->setCellValue('E'.$i, $key->reseller_bergabung)
                ->setCellValue('F'.$i, $key->super_bergabung)
                ->setCellValue('G'.$i, $key->total_poin)
                ->setCellValue('H'.$i, $key->total_join);
            $i++;
        }
        $spreadsheet->getActiveSheet()->getStyle("A1:H".($i))->applyFromArray($border);
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('BEBEBE');
        $spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->setTitle('Reward Thailand');
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reward Thailand '.$date.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
    public function special_ticket(){
        $query = $this->input->get('search')["value"];
        $start = 0;
        $length = $this->reward_thailand->spesial_count_all();
        $result['iTotalRecords'] = $length;
        $result['iTotalDisplayRecords'] = $this->reward_thailand->spesial_count_filter($query);
        $data =  $this->reward_thailand->spesial_list($start,$length,$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}


/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */