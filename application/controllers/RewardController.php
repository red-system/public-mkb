<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RewardController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reward','',true);
        $this->load->model('claim_reward','',true);
        $this->load->model('rekap_bulanan','',true);
        $this->load->model('omset_hangus','',true);
    }

    public function index()
    {
        $lokasi_id = isset($_SESSION["redpos_login"]["lokasi_id"])?$_SESSION["redpos_login"]["lokasi_id"]:null;
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/reward.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Reward Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reward";
        $data['page'] = $this->uri->segment(1);
        $sumColumn = array(4);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"name"));
        array_push($column, array("data"=>"rank_id"));
        array_push($column, array("data"=>"omset_lbl"));
        array_push($column, array("data"=>"keterangan"));
        array_push($column, array("data"=>"status"));

        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $this->load->model('claim_reward','',true);

        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $curMonth = number_format(date('m'));
        $waktu_end = date("Y-m-t");
        $first_date = date("Y-m-01");
        $datetime_start = new DateTime($first_date);
        $datetime_start->modify("-2 month");
        $waktu_start = $datetime_start->format("Y-m-d");
        $base = $this->claim_reward->cek_base($reseller_id);
        if($base==0){
            if($this->po_produk->is_deposit_in_april($lokasi_id)){
                $waktu_start = $this->config->item('reward-start-if-deposit-april');
            } else{
                $waktu_start = $this->config->item('start-count-reward');
            }

        }else{
            if($this->po_produk->is_deposit_in_april($lokasi_id)){
                $tanggal_claim = $this->claim_reward->get_base_date($reseller_id);
                $datetime_start = new DateTime(date("Y-m-01",strtotime($tanggal_claim)));
                if($tanggal_claim<=$this->config->item('old-reward-date')){
                    $waktu_start = '2021-07-01';
                }else{
                    $datetime_start->modify("-2 month");
                    $waktu_start = $datetime_start->format("Y-m-d");
                }
            } else{
                $waktu_start = $this->config->item('start-count-reward');
            }
        }
        $omset_group = $this->rekap_bulanan->omset_group($reseller_id,$waktu_start,$waktu_end);
        $omset_hangus = $this->omset_hangus->omset_hangus_reseller($reseller_id);
        $omset_group = $omset_group - $omset_hangus;
        $data['omset_group'] = $omset_group;
        $action = array();
        foreach ($akses_menu['reward'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/reward/index');
        $this->load->view('admin/static/footer');

//        $this->load->view('admin/static/header',$data);
//        $this->load->view('admin/static/sidebar');
//        $this->load->view('admin/static/topbar');
//        $this->load->view('admin/reward/preparing');
//        $this->load->view('admin/static/footer');
    }
    function list(){
        $lokasi_id = isset($_SESSION["redpos_login"]["lokasi_id"])?$_SESSION["redpos_login"]["lokasi_id"]:null;
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reward->data_all();
        $result['iTotalDisplayRecords'] = $this->reward->data_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reward->data_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        $curMonth = number_format(date('m'));
        $waktu_end = date("Y-m-t");
        $first_date = date("Y-m-01");
        $datetime_start = new DateTime($first_date);
        $datetime_start->modify("-2 month");
        $waktu_start = $datetime_start->format("Y-m-d");
        $base = $this->claim_reward->cek_base($reseller_id);
        if($base==0){
            if($this->po_produk->is_deposit_in_april($lokasi_id)){
                $waktu_start = $this->config->item('reward-start-if-deposit-april');

            } else{
                $waktu_start = $this->config->item('start-count-reward');
            }

        }else{
            if($this->po_produk->is_deposit_in_april($lokasi_id)){
                $tanggal_claim = $this->claim_reward->get_base_date($reseller_id);

                $datetime_start = new DateTime(date("Y-m-01",strtotime($tanggal_claim)));
                if($tanggal_claim<=$this->config->item('old-reward-date')){
                    $waktu_start = '2021-07-01';
                }else{
                    $datetime_start->modify("-2 month");
                    $waktu_start = $datetime_start->format("Y-m-d");
                }
            } else{
                $waktu_start = $this->config->item('start-count-reward');
            }
        }
        $omset_group = $this->rekap_bulanan->omset_group($reseller_id,$waktu_start,$waktu_end);
        $omset_hangus = $this->omset_hangus->omset_hangus_reseller($reseller_id);
        $omset_group = $omset_group - $omset_hangus;
        $max = 0;
        $currentTime = strtotime(date("Y-m-d"));
        $lastDepositTime = strtotime($reseller->next_deposit_date);
        $pass = true;
        if($currentTime>$lastDepositTime){
            $pass = false;
        }
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->max = 0;
            if($key->status != null){
                $key->max = $key->rank_id;
            }
            if($max<$key->max){
                $max = $key->max;
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reward_id;
            $key->name = 'Reward ke - '.$key->rank_id;
            $key->omset_lbl = number_format($key->omset);
            $key->budget_reward = number_format($key->budget_reward);
            $key->deny_claim = true;
            $key->status = ($key->status==null ? 'unclaimed':$key->status);
            if($key->status == 'unclaimed' && $key->omset<=$omset_group && $key->rank_id > $max && $pass){
                $key->deny_claim = false;
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function claim(){
        $result['success'] = false;
        $result['message'] = 'Gagal merubah data';
        $curMonth = number_format(date('m'));
        $startMonth = $curMonth - 2;
        $curYear = (date('Y'));
        $startYear = $curYear;
        if($startMonth<=$this->config->item('month-start')){
            $startMonth = $this->config->item('month-start');
        }
        if($startMonth < 0){
            $startMonth = 12 + $startMonth;
            $startYear = $curYear - 1;
        }
        $reward_id = $this->input->post('reward_id');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $this->load->model('claim_reward','',true);
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $curMonth = number_format(date('m'));
        $waktu_end = date("Y-m-t");
        $first_date = date("Y-m-01");
        $datetime_start = new DateTime($first_date);
        $datetime_start->modify("-2 month");
        $waktu_start = $datetime_start->format("Y-m-d");
        $base = $this->claim_reward->cek_base($reseller_id);
        if($base==0){
            if($this->config->item('reward-static-date')){
                $waktu_start = $this->config->item('start-count-reward');
            }
        }else{
            $tanggal_claim = $this->claim_reward->get_base_date($reseller_id);
            $datetime_start = new DateTime(date("Y-m-01",strtotime($tanggal_claim)));
            if($tanggal_claim<=$this->config->item('old-reward-date')){
                $waktu_start = '2021-07-01';
            }else{
                $datetime_start->modify("-2 month");
                $waktu_start = $datetime_start->format("Y-m-d");
            }

        }
        $omset_group = $this->rekap_bulanan->omset_group($reseller_id,$waktu_start,$waktu_end);
        $omset_hangus = $this->omset_hangus->omset_hangus_reseller($reseller_id);
        $omset_group = $omset_group - $omset_hangus;
        $start = 0;
        $length = 3;
        $query = "";
        $rekap_bulanan_list = $this->rekap_bulanan->rekap_bulanan_reseller_list($start,$length,$query,$reseller_id,$waktu_start,$waktu_end);
        $reward_rec = $this->reward->row_by_id($reward_id);
        $syarat = $reward_rec->omset;
        $i = 0;
        $dataClaim['reseller_id'] = $reseller_id;
        $dataClaim['reward_id'] = $reward_id;
        $dataClaim['tanggal_claim'] = date('Y-m-d');
        $this->rekap_bulanan->start_trans();
        $this->claim_reward->insert($dataClaim);
        $claim_reward_id = $this->claim_reward->last_id();
        foreach ($rekap_bulanan_list as $key){
            $i++;
            $claim =  $key->total_omset_reseller - $key->claimed;
            if($syarat<=$claim){
                $claim = $syarat;
            }
            $syarat = $syarat - $claim;
            $dataRekap['claimed'] =$key->claimed + $claim;
            $dataRekap['claim_reward_id'] =$claim_reward_id;
            $this->rekap_bulanan->update_by_id('rekap_bulanan_id',$key->rekap_bulanan_id,$dataRekap);
            if($syarat<=0){
                break;
            }
        }
        $this->load->model('rekap_omset','',true);
        $date_rekap = date("Y-m-d");
        $rekap_omset = $this->rekap_omset->get_rekap_omset($date_rekap,$reseller_id);

        if($rekap_omset==null){
            $rekap_omset_data['omset'] = 0;
            $rekap_omset_data['reseller_id'] = $reseller_id;
            $rekap_omset_data['claimed'] = 0;
            $this->rekap_omset->insert($rekap_omset);
            $rekap_omset_id = $this->rekap_omset->last_id();
            $rekap_omset_claim = 0;
        }else{
            $rekap_omset_id = $rekap_omset->rekap_omset_id;
            $rekap_omset_claim = $rekap_omset->claimed;
        }
        $rekap_omset_edit['claimed'] = $rekap_omset_claim + $syarat;
        $this->rekap_omset->update_by_id('rekap_omset_id',$rekap_omset_id,$rekap_omset_edit);
        $act = $this->rekap_bulanan->result_trans();
        if($act){
            $result['success'] = true;
            $result['message'] = 'Berhasil mengklaim reward';
        }
        echo json_encode($result);

    }

}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */