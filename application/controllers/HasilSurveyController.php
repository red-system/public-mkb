<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HasilSurveyController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('survey_response','',true);
        $this->load->model('survey_response_item','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/hasil_survey.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Hasil Survey  < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "survey";
        $data['page'] = $this->uri->segment(1);
        $jumlah_responden = $this->survey_response->jumlah_responden();
        $data['jumlah_responden'] = $jumlah_responden;
        $reseller_pindah = $this->survey_response->reseller_pindah();
        $reseller_tetap = $this->survey_response->reseller_tetap();
        $total_nilai = $this->survey_response->total_nilai();
        $data['tetap'] = $reseller_tetap;
        $data['pindah'] = $reseller_pindah;
        $data['rata_rata'] = $total_nilai/($jumlah_responden*10);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/hasil_survey/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = 10;
        $result['iTotalRecords'] = $this->survey_response->all();
        $result['iTotalDisplayRecords'] = $this->survey_response->filter($query);
        $data =  $this->survey_response->list($start,$length,$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}

/* End of file JenisBahanController.php */
/* Location: ./application/controllers/JenisBahanController.php */