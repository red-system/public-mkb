<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PaymentWithdrawCashController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('withdraw_cash','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');


    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/admin/payment-withdraw-cash.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Withdraw Cash < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang-piutang";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama_penerima"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"manage_status","template"=>"badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array(4));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/payment-withdraw-cash/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->withdraw_cash->withdraw_cash_admin_all();
        $result['iTotalDisplayRecords'] = $this->withdraw_cash->withdraw_cash_admin_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->withdraw_cash->withdraw_cash_admin_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->withdraw_cash_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->withdraw_cash_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function success_page(){

        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/payment-withdraw-cash.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Withdraw Cash < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang-piutang";
        $data['page'] = $this->uri->segment(1);
        $data['sumColumn'] = json_encode( array(4));
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama_penerima"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"manage_status","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/payment-withdraw-cash/index_success');
        $this->load->view('admin/static/footer');
    }
    function success_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->withdraw_cash->withdraw_cash_admin_all_success();
        $result['iTotalDisplayRecords'] = $this->withdraw_cash->withdraw_cash_admin_filter_success($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->withdraw_cash->withdraw_cash_admin_list_success($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->withdraw_cash_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->withdraw_cash_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function hold(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Withdraw Cash < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang-piutang";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama_penerima"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"manage_status","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/payment-withdraw-cash/index_hold');
        $this->load->view('admin/static/footer');
    }
    function hold_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->withdraw_cash->withdraw_cash_admin_all_hold();
        $result['iTotalDisplayRecords'] = $this->withdraw_cash->withdraw_cash_admin_filter_hold($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->withdraw_cash->withdraw_cash_admin_list_hold($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->withdraw_cash_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function processing(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $withdraw_cash_id = $this->input->post('withdraw_cash_id');
        $withdraw_cash_id = str_replace("##",",",$withdraw_cash_id);
        $withdraw_cash_id = str_replace("#","",$withdraw_cash_id);
        $url_get = base_url()."pengiriman-withdraw-cash/print?withdraw_cash_id=".$withdraw_cash_id;
        $withdraw_cash_id = explode(",",$withdraw_cash_id);
        $proses = $this->withdraw_cash->changeToProcessing($withdraw_cash_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
            $result["url_get"] = $url_get;
        }
        echo json_encode($result);
    }
    function fail(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $withdraw_cash_id = $this->input->post('withdraw_cash_id');
        $withdraw_cash_id = str_replace("##",",",$withdraw_cash_id);
        $withdraw_cash_id = str_replace("#","",$withdraw_cash_id);
        $withdraw_cash_id = explode(",",$withdraw_cash_id);
        $proses = $this->withdraw_cash->changeToFail($withdraw_cash_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }
    function success_process(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $withdraw_cash_id = $this->input->post('withdraw_cash_id');
        $withdraw_cash_id = str_replace("##",",",$withdraw_cash_id);
        $withdraw_cash_id = str_replace("#","",$withdraw_cash_id);
        $withdraw_cash_id = explode(",",$withdraw_cash_id);
        $proses = $this->withdraw_cash->changeToSuccess($withdraw_cash_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }
    function turnback(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $withdraw_cash_id = $this->input->post('withdraw_cash_id');
        $withdraw_cash_id = str_replace("##",",",$withdraw_cash_id);
        $withdraw_cash_id = str_replace("#","",$withdraw_cash_id);
        $withdraw_cash_id = explode(",",$withdraw_cash_id);

        $proses = $this->withdraw_cash->changeToTurnback($withdraw_cash_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";

        }
        echo json_encode($result);
    }
    function print(){
        $withdraw_cash_id = $this->input->get("withdraw_cash_id");
        $withdraw_cash_id = explode(",",$withdraw_cash_id);
        $potongan = 10000;
        $data['potongan'] = $potongan;
        $bonus = $this->withdraw_cash->bonusListPrint($withdraw_cash_id);
        $total_bonus = $this->withdraw_cash->bonusTotal($withdraw_cash_id);
        $data['bonus'] = $bonus;
        $data['total_bonus'] = $total_bonus;
        $this->load->view('admin/payment-withdraw-cash/print',$data);
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
