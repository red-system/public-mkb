<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class HutangReturController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_produk','',true);
        $this->load->model('voucher_retur','',true);
        $this->load->model('hutang_retur','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/voucher-retur.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Hutang Retur < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "hutang-retur";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_retur"));
        array_push($column, array("data"=>"voucher_code"));
        array_push($column, array("data"=>"produk_nama"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"tanggal_pelunasan"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['hutang-produk'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/hutang-retur/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->hutang_retur->count_all();
        $result['iTotalDisplayRecords'] = $this->hutang_retur->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->hutang_retur->_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pelunasan != null){
                $time = strtotime($key->tanggal_pelunasan);
                $key->tanggal_pelunasan = date('d-m-Y',$time);
            }

            $key->row_id = $key->hutang_retur_id;
            $key->jumlah = number_format($key->jumlah);
            $key->no = $i;
            $key->action = null;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */