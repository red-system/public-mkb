<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpgradeSuperResellerController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

        $this->load->model('master_login','',true);
        $this->load->model('reseller','',true);
		$this->load->helper('string');
        $this->load->library('main');
        if(isset($_SESSION['redpos_login'])){
            $this->load->model('user','',true);
            $this->load->model('staff','',true);
            $this->load->model('lokasi','',true);
        }
	}

	public function index()
	{
        if(!isset($_SESSION['log_pos']['log_kasir_id'])){
			unset($_SESSION["redpos_login"]);
            unset($_SESSION["redpos_company"]);
		}

        $this->load->view('admin/upgrade-super-reseller/index');
        
		// if(isset($_SESSION["redpos_login"])){
		// 	redirect(base_url(),'refresh');
		// } else {
		// 	$this->load->view('admin/upgrade-super-reseller/index');
		// }
	}

    function upgrade_user()
    {
        $result['success'] = false;
        $result['message'] = "missing parameter";
        $user_id = $_SESSION["redpos_login"]['user_id'];

        $data_user = $this->master_login->row_by_id($user_id);
        $old_username = '#'.$data_user->username;
        $reseller_id = $data_user->reseller_id;

        $old_reseller = array();
        $data_reseller = $this->reseller->row_by_id($reseller_id);
        $old_reseller['email'] = '#'.$data_reseller->email;

        $data = array(
            "email"=>$data_reseller->email,
            "no_ktp"=>$data_reseller->no_ktp,
            "phone"=>$data_reseller->phone,
            "nama"=>$data_reseller->nama,
            "type"=>'super agen',
            "province_id"=>$data_reseller->province_id,
            "city_id"=>$data_reseller->city_id,
            "subdistrict_id"=>$data_reseller->subdistrict_id,
            "alamat"=>$data_reseller->alamat,
            "outlet_province_id"=>$data_reseller->outlet_province_id,
            "outlet_city_id"=>$data_reseller->outlet_city_id,
            "outlet_subdistrict_id"=>$data_reseller->outlet_subdistrict_id,
            "outlet_alamat"=>$data_reseller->outlet_alamat,
            "foto_profile"=>$data_reseller->foto_profile,
            "foto_ktp"=>$data_reseller->foto_ktp,
            "selfi_ktp"=>$data_reseller->selfi_ktp,
        );

        $referal_code = $this->uri->segment(3);
        $reseler = $this->reseller->resseler_by_referal_code($referal_code);
        $data["referal_id"] = $reseler->reseller_id;

        $nonactive_email = $this->reseller->update_by_id('reseller_id', $reseller_id, $old_reseller);
        $nonactive_user = $this->master_login->nonactive_user($_SESSION["redpos_login"]['user_id'], $old_username);

        $insert = $this->reseller->insert($data);

        
        if ($insert) {
            $result["success"] = true;
            $result["message"] = "Akun dalam proses pengajuan Super Reseller";
            $new_reseller_id = $this->reseller->last_id();
            $prefix = random_string('alnum', 5);
            $referal_code_reseller = $prefix."-".$new_reseller_id;
            $data_new = array("referal_code"=>$referal_code_reseller);
            $this->reseller->update_by_id('reseller_id',$new_reseller_id,$data_new);
            unset($_SESSION["redpos_login"]);
            unset($_SESSION["redpos_company"]);
        } else {
            $result["message"] = "Gagal upgrade";
        }
        
        echo json_encode($result);
    }

	function check_user(){
		$result['success'] = false;
		$result['message'] = "missing parameter";
        $username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->load->model('reseller','',true);
        $this->load->model('user_role','',true);
		$auth = $this->master_login->auth_login($username);
		if($auth != null){
			if (password_verify($password, $auth->password)){
			    $reseller_id = $auth->reseller_id;
			    $reseller = $this->reseller->row_by_id($reseller_id);
			    $user_role = $this->user_role->row_by_id($auth->user_role_id);
			    if($reseller->status!="banned"){
                    if ($reseller->type == "agent") {
                        $result["success"] = true;
                        $result["message"] = "Login success";
                        $_SESSION["redpos_login"]["user_id"] = $auth->id;
                    }else{
                        $result["message"] = "Hanya Akun Reseller yang dapat diupgrade ke Super Reseller";
                    }
                    
                } else {
                    $result["message"] = "Akun anda telah di banned";
                }

			} else {
				$result["message"] = "Wrong password";
			}
		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}
	function logout(){
		$result['success'] = false;
		$result['message'] = 'Gagal logout, silakan lakukan closing pada POS terlebih dahulu';
		if(!isset($_SESSION['log_pos']['log_kasir_id'])){
			unset($_SESSION["redpos_login"]);
            unset($_SESSION["redpos_company"]);
			$result['success'] = true;
			$result['message'] = 'Berhasil logout';
		}
		echo json_encode($result);
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
