<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClaimedRewardController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/bonus.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $sumColumn = array(3,5);
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "master_data";
        $data['page'] = $this->uri->segment(1);
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"name"));
        array_push($column, array("data"=>"keterangan"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        $data["reseller_id"] = $reseller_id;
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $data['sumColumn'] = json_encode($sumColumn);
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/claimed-reward/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $this->load->model("reward","",true);
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->reward->claimed_reward_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->reward->claimed_reward_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reward->claimed_reward_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->name = 'Reward ke - '.$key->rank_id;
            $key->no = $i;
            $i++;
            $key->row_id = $key->claimed_reward_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }



}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
