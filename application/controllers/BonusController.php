<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class BonusController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bonus','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');


    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/bonus.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $sumColumn = array(3,5);
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "master_data";
        $data['page'] = $this->uri->segment(1);
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        array_push($column, array("data"=>"no"));
        if($reseller_id==null){
            array_push($column, array("data"=>"nama_penerima"));
        }
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"persentase"));
        array_push($column, array("data"=>"jumlah_bonus"));

        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"tanggal_pengiriman"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        $data["reseller_id"] = $reseller_id;
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $data['sumColumn'] = json_encode($sumColumn);
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/bonus/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['estimasi_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['estimasi_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        if(isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][7]["search"]["value"]);
            $_GET['pengiriman_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['pengiriman_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $reseller_id != null ? $this->bonus->bonus_all($reseller_id):$this->bonus->bonus_admin_all();
        $result['iTotalDisplayRecords'] = $reseller_id != null ? $this->bonus->bonus_filter($query,$reseller_id):$this->bonus->bonus_admin_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $reseller_id != null ?  $this->bonus->bonus_list($start,$length,$query,$reseller_id) : $this->bonus->bonus_admin_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;
            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function pembayaran()
    {
        $result['success'] = false;
        $result['message'] = "Parameter tidak sesuai";

        $this->form_validation->set_rules('tanggal_pengiriman', 'Tanggal Pengiriman', 'required');

        if (empty($_FILES['bukti_transfer']['name']) && $this->input->post("bukti_tranfer_old") == "") {
            $this->form_validation->set_rules('bukti_transfer', 'Bukti Transfer', 'required');
        }
        $this->form_validation->set_error_delimiters('<span class="error-message" style="color:red">', '</span>');
        if ($this->form_validation->run() === TRUE) {
            $bonus_id = $this->input->post('bonus_id');
            $tanggal_pengiriman = $this->input->post('tanggal_pengiriman');
            $bukti_transfer = $this->input->post("bukti_tranfer_old");

            if (!empty($_FILES['bukti_transfer']['name'])){

                if($bukti_transfer != ""){
                    $this->deleteImage($bukti_transfer);
                    $bukti_transfer = "";
                }
                $bukti_transfer = $this->uploadImage("bukti_transfer", $bukti_transfer);
            }
            $data = array(
                "tanggal_pengiriman"=>$tanggal_pengiriman,
                "status"=>"Terkirim",
                "bukti_transfer"=>$bukti_transfer,
            );
            $update = $this->bonus->update_by_id('bonus_id',$bonus_id,$data);
            if($update){
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
                $reseller_id = $this->bonus->row_by_id($bonus_id)->to_reseller_id;
                $reseller = $this->reseller->row_by_id($reseller_id);
                $bukti_transfer_url = $this->config->item('dev_storage_image').$bukti_transfer;
                $mailContentAdmin = "<p>Selamat bonus anda sudah dikirimkan dengan bukti transfer sebagi <a href='".$bukti_transfer_url."'>berikut</a></p>";
                $this->main->mailer_auth('Bonus Dikirim', $reseller->email, $reseller->nama, $mailContentAdmin);
                echo json_encode($result);
            }
        }else{
            echo json_encode(
                array(
                    'status' => 'error',
                    'message' => 'Fill form completly',
                    'errors' => array(
                        'tanggal_penerimaan' => form_error('tanggal_penerimaan'),
                        'bukti_transfer' => form_error('bukti_transfer'),
                    )
                )
            );
        }
    }
    function uploadImage($file,$url){
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $date = new DateTime();
        $file_name = $date->getTimestamp().random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/".$file_name.".".$ext, $_FILES[$file]['tmp_name'],NET_SFTP_LOCAL_FILE);
        if (!$output)
        {
            $url = "failed";
            return $url;
        }
        else
        {
            $url = $file_name.'.'.$ext;
        }
        return $url;
    }
    function deleteImage($filename){
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $output = $sftp->delete("../../../var/www/html/development/dev-storage.redsystem.id/redpos/".$filename,false);

    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
