<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class BackupController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if(is_file(FCPATH.'/upload/master_backup.zip')){
            unlink(FCPATH.'/upload/master_backup.zip');
        }
        $this->load->dbutil();

        $prefs = array(
            'format'      => 'zip',
            'filename'    => 'master_backup.sql'
        );
        $backup = $this->dbutil->backup($prefs);

        $db_name = 'master_backup'.'.zip';
        $save = FCPATH.'/upload/'.$db_name;

        $this->load->helper('file');
        write_file($save, $backup);
    }
    public function side()
    {
        if(is_file(FCPATH.'/upload/side_backup.zip')){
            unlink(FCPATH.'/upload/side_backup.zip');
        }
        $config =  array(
            'dsn'	=> '',
            'hostname' => $this->db->hostname,
            'username' => $this->db->username,
            'password' => $this->db->password,
            'database' => "mykindofbeauty_kemiri",
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => (ENVIRONMENT !== 'production'),
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
        $this->db = $this->load->database($config, TRUE);
        $this->load->dbutil();

        $prefs = array(
            'format'      => 'zip',
            'filename'    => 'side_backup.sql'
        );
        $backup = $this->dbutil->backup($prefs);

        $db_name = 'side_backup'.'.zip';
        $save = FCPATH.'/upload/'.$db_name;

        $this->load->helper('file');
        write_file($save, $backup);
    }


}

/* End of file ProdukByLocation.php */
/* Location: ./application/controllers/ProdukByLocation.php */