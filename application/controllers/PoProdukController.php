<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PoProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('po_produk','',true);
        $this->load->model('fullpayment','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('suplier','',true);
        $this->load->model('rekap_bulanan','',true);
        $this->load->model('bonus','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('reseller','',true);
		$this->load->model('tipe_pembayaran','',true);
        $this->load->model('response_payment','',true);
        $this->load->model('master_login','',true);
        $this->load->helper('string');
        $params = array('server_key' => $this->config->item('server-key'), 'production' => $this->config->item('mid-prod'));
        $this->load->library('midtrans');
        $this->load->library('main');
        $this->midtrans->config($params);
        $this->load->helper('url');
	}
	
	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/po/po_5.js");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Order Produk < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "order_produk";
		$data['page'] = "order_produk";
		$reseller = $this->getReseller();
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"po_produk_no"));
		array_push($column, array("data"=>"tanggal_pemesanan"));
		array_push($column, array("data"=>"grand_total_lbl"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
		if($reseller!= null && $reseller->type=="super agen") {
            array_push($column, array("data" => "estimasi_pengiriman"));
            array_push($column, array("data" => "tanggal_penerimaan"));
            array_push($column, array("data" => "status_penerimaan", "template" => "badgeTemplate"));
        }
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['order_produk'] as $key => $value) {
			if($key != "list" && $key != "akses_menu" && $key != "edit"){
				$action[$key] = $value;
			}
		}
		$reseller = $this->getReseller();
        $data["history"] = "";
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
        $data["pay"] = true;
        $data['reseller'] = $reseller;
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
        if($reseller->status!='temporary_ban'){
            $this->load->view('admin/po_produk');
        }else{
            $this->load->view('admin/home/frezze');
        }
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->po_produk_count_all_sa();
		$result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_sa($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->po_produk_list_sa($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
			$key->no = $i;
			if($key->jenis_pembayaran == "kas"){
				$key->tipe_pembayaran_nama = $key->kas_nama." ".$key->no_akun;
			}
			$i++;
			if($key->status_pembayaran != "Hutang"){
                $key->deny_edit = true;
                $key->deny_delete = true;
                $key->deny_pembayaran = true;
                $key->detail_pembayaran_btn = true;
            }
            if($key->status_pembayaran == "Gagal"||$key->status_pembayaran == "Batal"){
                $key->ulangi_pembayaran_btn = true;
            }

            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
			$key->delete_url = base_url().'po-produk/delete/';
			$key->row_id = $key->po_produk_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){

		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
		array_push($this->js, "script/admin/po/po_5.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Order Produk < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "order_produk";
		$data['page'] = $this->uri->segment(1);
		$data['lokasi'] = $this->lokasi->all_list();
		$data['suplier'] = $this->suplier->all_list();
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_po();
		$data['po_produk_no'] = $this->po_produk->get_kode_po_produk();

		$login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
		$reseller_id = $login->reseller_id;
		$reseller = $this->reseller->row_by_id($reseller_id);
        $data['minimal_po'] = $this->po_produk->minimal_po($reseller_id);
        $data['reseller'] = $reseller;
        $first_termin = $reseller->type == "agent" ? 1100000 : 11000000;
        $data['first_termin'] = $first_termin;
        $data['is_frezze'] = $reseller->is_frezze == null ? 0: $reseller->is_frezze;
        $data['termin'] = $reseller->type == 'super agen' ? $this->config->item('termin_super') : $this->config->item('termin_reseller');
        $history = $this->produk->history_beli_produk($reseller->reseller_id);
        $fullpayment = $this->po_produk->is_full_payment($history,$reseller_id);
        $termin_text = '';
        $redeposit_text = '';
        $row = 0;
        $historyList = array();
        $baris = 0;
        $grand_total = 0;
        //remove history list feature to check the backup look at commit message "change algoritma fulldeposit on notify"
        $data['historyList'] = $historyList;
        $data['grand_total'] = $grand_total;

		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/add_po_produk');
		$this->load->view('admin/static/footer');
		unset($_SESSION['po_produk']);
		$_SESSION['po_produk']['lokasi'] = 1;
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key == "get-po-no"){
			$code = $this->input->post('suplier_kode');
			echo $this->po_produk->get_kode_po_produk($code);
		}
		if($key=="list-produk"){
			$this->list_produk();
		}
		if($key=="sess-produk-add"){
			$no = $this->input->post('key');
			$produk_id = $this->input->post('produk_id');
			$_SESSION['po_produk']['produk']['con_'.$no]['produk_id'] = $produk_id;
			$_SESSION['po_produk']['produk']['con_'.$no]['value'] = 0;
		}
		if($key=="sess-produk-change"){		
			$jumlah = $this->input->post('jumlah');
			$no = $this->input->post('key');
			if(isset($_SESSION['po_produk']['produk']['con_'.$no]['produk_id'])){
				$_SESSION['po_produk']['produk']['con_'.$no]['value'] = $jumlah;
			}
		}
		if($key=="sess-produk-delete"){
			$no = $this->input->post('key');
			unset($_SESSION['po_produk']['produk']['con_'.$no]);
		}
		if($key=="sess-produk-reset"){
			unset($_SESSION['po_produk']['produk']);
		}
		if($key=="sess-lokasi"){
			$_SESSION['po_produk']['lokasi'] = $this->input->post('lokasi_id');
		}				
	}
	function list_produk(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$lokasi_id = $_SESSION["redpos_login"]["lokasi_id"];
		$login = $this->db->select('mykindofbeauty_master.login.*')->where('lokasi_id',$lokasi_id)->get('mykindofbeauty_master.login')->row();
		$reseller_id = $login->reseller_id;
		$reseller = $this->reseller->row_by_id($reseller_id);
		$this->load->model('produk');
		$history = $this->produk->history_beli_produk($reseller_id);
        $fullpayment = $this->fullpayment->is_fullpayment($reseller_id);
		$historyData = array();
		foreach ($history as $item){
		    $historyData["_".$item->produk_id] = $item->total;
        }
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produk/delete/';
			$key->action =null;
			$key->last_hpp = $key->hpp_global;
			$key->row_id = $key->produk_id;
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
			$validasi = 0;
//            if($reseller->type=="super agen"){
//                if($historyData["_".$key->produk_id]<$key->minimal_termin_super){
//                    $validasi = ($key->minimal_termin_super - $historyData["_".$key->produk_id])*$key->hpp_global;
//                }else{
//                    $validasi = $key->minimal_rede_super * $key->hpp_global;
//                }
//            }else{
//                if($historyData["_".$key->produk_id]<$key->minimal_termin_res){
//                    $validasi = ($key->minimal_termin_res - $historyData["_".$key->produk_id])*$key->hpp_global;
//                }else{
//                    $validasi = $key->minimal_rede_res * $key->hpp_global;
//                }
//            }
            $jumlah = 0;
            if($reseller->type=='agent'){
                if($historyData["_".$key->produk_id]<$key->old_fullpayment_res&&!$fullpayment){
                    if($historyData["_".$key->produk_id]==0){
                        $jumlah = $key->minimal_termin_res;
                        $key->min_mix = $key->minimal_termin_res_mix;
                    }else{
                        $jumlah = $key->old_fullpayment_res - $historyData["_".$key->produk_id];
                        $key->min_mix = $key->fullpayment_res- $historyData["_".$key->produk_id];
                    }
                }else{
                    $jumlah = $key->minimal_rede_res;
                    $key->min_mix = $key->minimal_rede_res_mix;
                }
            }else{
                if($historyData["_".$key->produk_id]==0&&!$fullpayment){
                    $jumlah = $key->minimal_termin_super;
                    $key->min_mix = $key->minimal_termin_super_mix;
                }else if($historyData["_".$key->produk_id]<$key->old_fullpayment_super&&!$fullpayment){
                    $jumlah = $key->old_fullpayment_super - $historyData["_".$key->produk_id];
                    $key->min_mix = $key->fullpayment_super - $historyData["_".$key->produk_id];
                }else{
                    $jumlah = $key->minimal_rede_super;
                    $key->min_mix = $key->minimal_rede_super_mix;
                }
            }
            $key->validasi = $jumlah *  $key->hpp_global;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
	function save_add(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));
        $bukti_transfer = "";
        $_POST['bukti_pembayaran'] = $bukti_transfer;
		$insert = $this->po_produk->insert_po_produk();
		if($insert['success']){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
            $result['po_produk_id'] = $insert['po_produk_id'];
        }
		echo json_encode($result);
	}
	function edit(){
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$po_produk = $this->po_produk->po_produk_by_id($id);

		if ($po_produk != null) {
			$data['po_produk'] = $po_produk;
			$data['po_produk_detail']=$this->po_produk->po_produk_detail_by_id($id);
			foreach ($data['po_produk_detail'] as $item){
			    $item->last_hpp = $item->last_hpp == 0 ?  $this->stock_produk->last_hpp($item->produk_id) : $item->last_hpp;
            }
			$tipe_pembayaran = $this->tipe_pembayaran->row_by_id($po_produk->tipe_pembayaran);
			$data['selc_tipe_pembayaran'] = $tipe_pembayaran;
			array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
			array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
			array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
			array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
			array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
			array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
			array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
			array_push($this->js, "script/admin/po/po_5.js");
            $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
            $reseller_id = $login->reseller_id;
            $reseller = $this->reseller->row_by_id($reseller_id);
            $data['minimal_po'] = $this->po_produk->minimal_po($reseller_id);
            $data['reseller'] = $reseller;
			$data['lokasi'] = $this->lokasi->all_list();
			$data['suplier'] = $this->suplier->all_list();
			$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_po();
			$data["css"] = $this->css;
			$data["js"] = $this->js;
			$column = array();
			$data["meta_title"] = "Produksi < ".$_SESSION["redpos_company"]['company_name'];;
			$data['parrent'] = "master_data";
			$data['page'] = $this->uri->segment(1);
            $data['is_frezze'] = $reseller->is_frezze == null ? 0: $reseller->is_frezze;
            $data['termin'] = $reseller->type == 'super agen' ? $this->config->item('termin_super') : $this->config->item('termin_reseller');
            $first_termin = $reseller->type == "agent" ? 1100000 : 11000000;
            $data['first_termin'] = $first_termin;
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/edit_po_produk');
			$this->load->view('admin/static/footer');
		}else {
			redirect('404_override','refresh');
		}
	}
	function save_edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));
		if(isset($_FILES['bukti_pembayaran'])){
            $bukti_transfer = "";
            $bukti_transfer = $this->uploadImage("bukti_pembayaran", $bukti_transfer);
            $_POST['bukti_pembayaran'] = $bukti_transfer;
        }
		$insert = $this->po_produk->edit_po_produk();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->po_produk->delete_by_id("po_produk_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		$id = $this->input->post('po_produk_id');
		$temp = $this->po_produk->po_produk_by_id($id);
		$no = 0;
		$no_produk = 0;
		$post = array();
		$temp->item = $this->po_produk->po_produk_detail_by_id($id);
		$tipe_pembayaran = $this->tipe_pembayaran->row_by_id($temp->tipe_pembayaran);
		$temp->metode_pembayaran = $tipe_pembayaran->tipe_pembayaran_nama." ".$tipe_pembayaran->no_akun;
        $temp->bukti_url = $this->config->item('dev_storage_image').$temp->bukti_pembayaran;
		if($temp->tanggal_pemesanan != null){
			$x = strtotime($temp->tanggal_pemesanan);
			$temp->tanggal_pemesanan = date("Y-m-d",$x);
		}
		if($temp->tanggal_penerimaan != null){
			$x = strtotime($temp->tanggal_penerimaan);
			$temp->tanggal_penerimaan = date("Y-m-d",$x);
		}
		if($temp->created_at != null){
			$time = strtotime($temp->created_at);
			$temp->created_at = date('d-m-Y H:i:s',$time);
		}
		if($temp->updated_at != null){
			$time = strtotime($temp->updated_at);
			$temp->updated_at = date('d-m-Y H:i:s',$time);
		}
		foreach ($temp->item as $key) {
			$key->harga = number_format($key->harga);
			$key->jumlah_qty = number_format($key->jumlah_qty);
			$key->sub_total = number_format($key->sub_total);
		}
			$temp->total = number_format($temp->total);
			$temp->tambahan = number_format($temp->tambahan);
			$temp->potongan = number_format($temp->potongan);
			$temp->grand_total = number_format($temp->grand_total);		
		echo json_encode($temp);
	}
	function penerimaan_po_produk(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$po_produk_id = $this->input->post('po_produk_id');
		$data["status_penerimaan"] = "Diterima";
		$data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
		$lokasi_id = $this->input->post('lokasi_penerimaan_id');
		$data['lokasi_penerimaan_id'] = $lokasi_id;
		$this->po_produk->start_trans();
		$update_po_produk = $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$data);
		if($update_po_produk){
			$po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
			foreach ($po_produk_item as $key) {
					$data = array();
					$data["stock_produk_qty"] = $key->jumlah_qty;
					$data["produk_id"] = $key->produk_id;
					$data["stock_produk_lokasi_id"] = $lokasi_id;
                    $data["year"] = date("y");
                    $data["month"] = date("m");
                    $data["po_id"] = $po_produk_id;
                    $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                    $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                    $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                    $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                    $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                    $data["hpp"] = $this->string_to_number($key->harga);
					$insert = $this->stock_produk->insert($data);					
					$arus["stock_produk_id"] = $this->stock_produk->last_id();
					$arus["method"] = "insert";
					$arus["tanggal"] = date("Y-m-d");
					$arus["table_name"] = "stock_produk";
					$arus["produk_id"] = $key->produk_id;
					$arus["stock_out"] = 0;
					$arus["stock_in"] = $key->jumlah_qty;
					$arus["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
					$arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
					$arus["keterangan"] = "Order Produk";
					$this->stock_produk->arus_stock_produk($arus);					
			}
		}
		if($this->po_produk->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);
	}
	function history(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app2.js");
		array_push($this->js, "script/admin/po/po_5.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "History Order Produk < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "order_produk";
		$data['page'] = "order_produk";
        $reseller = $this->getReseller();
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"po_produk_no"));
		array_push($column, array("data"=>"tanggal_pemesanan"));
		array_push($column, array("data"=>"grand_total_lbl"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        if($reseller!= null && $reseller->type=="super agen") {
            array_push($column, array("data" => "estimasi_pengiriman"));
            array_push($column, array("data" => "tanggal_penerimaan"));
            array_push($column, array("data" => "status_penerimaan", "template" => "badgeTemplate"));
        }
        array_push($column, array("data"=>"keterangan_tolak"));
        $data['reseller'] = $reseller;
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
		$data['lokasi'] = $this->lokasi->all_list();
		$action = array("view"=>true,"print"=>true);
		$data['action'] = json_encode($action);
        $data["history"] = "true";

		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/po_produk');
		$this->load->view('admin/static/footer');		
	}
	function history_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->po_produk_count_all_history();
		$result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_history($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->po_produk_list_history($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			
			$key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'po-produk/delete/';
            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
			$key->row_id = $key->po_produk_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function print(){
	    $id = $this->uri->segment(3);
        $temp = $this->po_produk->po_produk_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->po_produk->po_produk_detail_by_id($id);
        $temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
        if($temp->tanggal_pemesanan != null){
            $x = strtotime($temp->tanggal_pemesanan);
            $temp->tanggal_pemesanan = date("Y-m-d",$x);
        }
        if($temp->tanggal_penerimaan != null){
            $x = strtotime($temp->tanggal_penerimaan);
            $temp->tanggal_penerimaan = date("Y-m-d",$x);
        }
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga); 
            $key->jumlah_qty = number_format($key->jumlah_qty);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        $data['po_produk'] = $temp;
        $this->load->view('admin/po-produk/print',$data);
    }
    function pay(){
        $po_produk_id = $this->input->post('po_produk_id');
	    $check = $this->response_payment->check($po_produk_id);
	    if($check==null){
            $this->token($po_produk_id);
        } else {
	        $result["success"] = false;
            $result["message"] = "Gagal menyimpan data";
            $result['response_payment'] = $check;
            echo json_encode($result);
        }
    }
    function ulangi_pembayaran(){
        $po_produk_id = $this->input->post('po_produk_id');
        $repeat = $this->response_payment->count_repeat($po_produk_id);

        $this->token($po_produk_id,$repeat);
    }
    function detail_pembayaran(){
        $po_produk_id = $this->input->post('po_produk_id');
        $check = $this->response_payment->check($po_produk_id);
        $result["success"] = false;
        $result["message"] = "Gagal menyimpan data";
        $result['response_payment'] = $check;
        echo json_encode($result);
    }
    public function token($id,$repeat=null)
    {
        $po_produk = $this->po_produk->row_by_id($id);
        $order_id = $po_produk->po_produk_no;
        if($repeat!=null){
            $order_id .='-R'.$repeat;
        }
        $order_id .= '-'.random_string('alnum',5);
        $transaction_details = array(
            'order_id' => $order_id,
            'gross_amount' => $this->string_to_number($po_produk->grand_total), // no decimal allowed for creditcard
        );
        $po_produk_detail = $this->po_produk->po_produk_detail_by_id($id);
        $item_details = array ();
        foreach ($po_produk_detail as $key) {
            $detail = array(
                'id' => $key->produk_id,
                'price' => $this->string_to_number($key->harga),
                'quantity' => $this->string_to_number($key->jumlah_qty),
                'name' => $key->produk_nama
            );
            array_push($item_details,$detail);
        }
        $reseller = $this->getReseller();
        // Optional
        $billing_address = array(
            'first_name'    => $reseller->nama,
            'last_name'     => "",
            'address'       =>$reseller->alamat,
            'city'          => "",
            'postal_code'   => "",
            'phone'         => $reseller->phone,
            'country_code'  => ''
        );
//        // Optional
        $customer_details = array(
            'first_name'    => $reseller->nama,
            'last_name'     => "",

            'phone'         => $reseller->phone,
            'billing_address'  => $billing_address,
            //'shipping_address' => $shipping_address
        );
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;
        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'day',
            'duration'  => 1
        );
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $item_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
        error_log(json_encode($transaction_data));
        $snapToken = $this->midtrans->getSnapToken($transaction_data);
        error_log($snapToken);
        $result['success'] = true;
        $result['message'] = "Berhasil membuat token";
        $result['token'] = $snapToken;
        echo json_encode($result);
    }
    function response_payment_save(){
	    //here
	    $po_produk_id = $this->input->post('po_produk_id');
        $status = $this->input->post('status');
        $json = $this->input->post("json");
        $data["po_produk_id"] = $po_produk_id;
        $data["status"] = $status;
        $data["json"] = json_encode($json);
        $po_data = $this->po_produk->row_by_id($po_produk_id);
        $lokasi_id = $po_data->lokasi_reseller;
        $login = $this->db->where("lokasi_id",$po_data->lokasi_reseller)->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        
        $data_user = $this->master_login->row_by_field('reseller_id', $reseller_id);
        $username = $data_user->username;
        
        $referal_id = $reseller->referal_id;
        $paramPengiriman = "13:00:00";
        $paramBonus = "16:00:00";
        $insert = $this->response_payment->insert($data);
        $estimasiBonus = $this->main->tanggal_kerja($paramBonus);
        $deposit_anak = $this->po_produk->total_deposit_reseller($po_data->lokasi_reseller);
        $deposit_anak = $deposit_anak == "" ? 0 : $deposit_anak;
        $minimal_po_anak = $this->po_produk->minimal_po_cek($reseller_id);
        $indikator_anak = 0;
        if($deposit_anak<$minimal_po_anak){
            $indikator_anak = 1;
        }
        $login_induk = $this->db->where("reseller_id",$referal_id)->get("mykindofbeauty_master.login")->row();
        $lokasi_induk = $login_induk->lokasi_id;
        $deposit_induk = $this->po_produk->total_deposit_reseller($lokasi_induk);
        $deposit_induk = $deposit_induk == "" ? 0 : $deposit_induk;
        $minimal_po_induk = $this->po_produk->minimal_po_cek($referal_id);
        $indikator_induk = 0;
        if($deposit_induk<$minimal_po_induk){
            $indikator_induk = 1;
        }

        if($status=="success"){
                $voucher_code = time().$reseller_id.random_string('numeric', 5);
                $voucher_text = '';
                if ( $reseller->type=="agent"){
                    $voucher_text = '<tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Kode Voucher </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$voucher_code.'</td>
                                    </tr>';
                }
                $referal_link = $this->config->item('url-landing').$reseller->referal_code;
                $message_tanda_terima = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 500px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h3 style="text-align: center;font-weight: 500">Tanda Terima Pembayaran</h3>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table width="100%">
                                <tbody>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">No PO </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$po_data->po_produk_no.'</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Tanggal </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.date("Y-m-d").'</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Kepada </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.$reseller->nama.' ('.$username.')</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td width="20%" style="text-align: left">Total </td>
                                        <td>:</td>
                                        <td style="text-align: right">'.number_format($po_data->grand_total).'</td>
                                    </tr>
                                    '.$voucher_text.'
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Mohon tanda terima pembayaran ini disimpan dengan baik sebagai bukti pembayaran yang valid.
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                       <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                       <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>
                </td>
            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>
</div>
</body></html>';
            $this->main->mailer_auth('Bukti Pembayaran', $reseller->email, $reseller->nama, $message_tanda_terima);
                $po_produk = array();
                $po_produk["status_pembayaran"] = "Lunas";
                $estimasi = $this->main->tanggal_kerja($paramPengiriman);
                $po_produk['estimasi_pengiriman'] =$estimasi;
                if($reseller->leader!=null){
                    $po_produk['om_leader'] = $reseller->leader;
                }
                $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$po_produk);
                $tanggal_rekap = date("Y-m-d");
                $this->po_produk->rekap_bulanan($reseller_id,$tanggal_rekap,$lokasi_id,'"total_po"');
                $reseller_penerima = $this->reseller->row_by_id($referal_id);

                $data_user_penerima = $this->master_login->row_by_field('reseller_id', $referal_id);
                $username_penerima = $data_user_penerima->username;

                $resellerData["first_deposit"] = 1;
                if($reseller->first_deposit==0){
                    $first_deposit_date = new DateTime(date("Y-m-d"));
                    $money_date = new DateTime(date("Y-m-d"));
                    $resellerData["first_deposit_date"] = $first_deposit_date->format("Y-m-d");
                    if($reseller->type = 'agent'){
                        $first_deposit_date->modify("+1 month");
                    } else{
                        $first_deposit_date->modify("+2 month");
                    }
                    $resellerData["first_limit_date"] = $first_deposit_date->format("Y-m-d");
                    $resellerData["next_limit_date"] = $first_deposit_date->format("Y-m-d");
                    $money_date->modify("+2 year");
                    $resellerData["money_back_guarantee"] = $money_date->format("Y-m-d");

                    $dataLogin["active"] = 1;
                    $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id);
                    $this->db->update('mykindofbeauty_master.login',$dataLogin);
                }
                $resellerData["status"] = "active";
                $this->reseller->update_by_id("reseller_id",$reseller_id,$resellerData);
                $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
                $this->db->select("mykindofbeauty_kemiri.staff.*");
                $this->db->join("mykindofbeauty_kemiri.staff","mykindofbeauty_master.login.user_staff_id = mykindofbeauty_kemiri.staff.staff_id");
                $this->db->where("mykindofbeauty_master.login.company_id",3);
                $this->db->where("mykindofbeauty_master.login.user_role_id",2);
                $login = $this->db->get("mykindofbeauty_master.login")->result();
                $reseller_bank = $this->db->where("bank_id",$reseller->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
                if ( $reseller->type=="super agen"){


            } else{
                $po["status_penerimaan"] = "Diterima";
                $po["tanggal_penerimaan"] = date("Y-m-d");
                $po['lokasi_penerimaan_id'] = $po_data->lokasi_reseller;
                $this->po_produk->update_by_id("po_produk_id",$po_produk_id,$po);
                $voucher = array();
                $voucher["reseller_pemilik"] = $reseller_id;

                $voucher["voucher_code"] = $voucher_code;
                $voucher["po_produk_id"] = $po_produk_id;
                $this->voucher_produk->insert($voucher);
            }
                $withdraw_cash = $reseller->withdraw;
                $sisa_deposit_withdraw = ($reseller->sisa_deposit_withdraw-$po_data->grand_total) <= 0 ? 0 : abs($reseller->sisa_deposit_withdraw-$po_data->grand_total);
                $loyalti_withdraw = ($reseller->loyalti_withdraw-$po_data->grand_total) <= 0 ? 0 : abs($reseller->loyalti_withdraw-$po_data->grand_total);
                $sisa = $withdraw_cash - $po_data->grand_total;
                $sisa_deposit = $sisa <= 0 ? abs($sisa) : 0;

                $withdraw_reseller = $sisa <= 0 ? 0 : abs($sisa);
                $resellerUpdate = array();
                $resellerUpdate['withdraw'] = $withdraw_reseller;
                $resellerUpdate['sisa_deposit_withdraw'] = $sisa_deposit_withdraw;
                $resellerUpdate['loyalti_withdraw'] = $loyalti_withdraw;
                $this->reseller->update_by_id('reseller_id',$reseller_id,$resellerUpdate);
                if($referal_id!=null){

                    //
                    $withdraw_penerima = $reseller_penerima->sisa_deposit_withdraw;
                    $sisa_penerima = $withdraw_penerima - $po_data->grand_total;
                    $withdraw_penerima_ganti = $sisa_penerima <= 0 ? 0 : abs($sisa_penerima);
                    $resellerUpdate = array();
                    $resellerUpdate['sisa_deposit_withdraw'] = $withdraw_penerima_ganti;
                    $this->reseller->update_by_id('reseller_id',$referal_id,$resellerUpdate);

                    //
                    $this->po_produk->rekap_bulanan($reseller_id,$tanggal_rekap,$lokasi_id,'"omzet"');
                    if($withdraw_reseller>0){
                        $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                        $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Pemberitahuan</h3>
                        <span>Super Reseller referensi anda atas nama '.$reseller->nama.' melakukan redeposit dengan detail sebagai berikut : 
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Super Reseller </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah_qty) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->po_total) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Total Withdraw Cash </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($withdraw_cash) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Sisa Withdraw Cash </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($withdraw_reseller) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span style="color: red">
                          Note : anda belum bisa mendapakat bonus referensi dari '.$reseller_penerima->nama.' ('.$username_penerima.') sebelum super reseller tersebut melakukan redeposit sejumlah total sisa withdraw cashnya.
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                        $this->main->mailer_auth('Pemberitahuan Deposit Reseller Referensi', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                    }
                    if($sisa_deposit>0){
                        if($reseller_penerima->type=="super agen" && $reseller->type=="super agen" ){
                            if($this->bonus->cekBonus($po_produk_id,$reseller_id,"loyalty bonus")) {
                                $status_bonus_reseller_loyal = (($deposit_induk < $minimal_po_induk)||($withdraw_penerima>0)) ? "Hold" : "On Process";
                                $bonus = array();
                                $bonus["tanggal"] = $estimasiBonus;
                                $bonus["from_reseller_id"] = $reseller->reseller_id;
                                $bonus["type"] = "loyalty bonus";
                                $bonus["jumlah_deposit"] = $sisa_deposit;
                                $bonus["persentase"] = 5;
                                $bonus["jumlah_bonus"] = $sisa_deposit * 0.05;
                                $bonus["to_reseller_id"] = $referal_id;
                                $bonus["po_produk_id"] = $po_produk_id;
                                $bonus["status"] = $status_bonus_reseller_loyal;
                                $this->bonus->insert($bonus);
                                $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                                if ($deposit_induk < $minimal_po_induk){
                                    $status_message = "Bonus anda akan di hold karena anda belum memenuhi minimal deposit. Silakan lakukan deposit untuk mencairkan bonus.";
                                }
                                $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                                $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS LOYALTI yang akan langsung di transfer ke rekening MKB Anda.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table style="width: 100%">
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Pemilik Voucher </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah_qty) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah/sisa Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">5%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit * 0.05) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span>
                           '.$status_message.'
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                                $this->main->mailer_auth('Bonus Loyalti', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                            }
                        }
                        if($this->bonus->cekBonus($po_produk_id,$referal_id)) {


                            $bonus = array();
                            $status_bonus_reseller = (($indikator_induk == 1 && $reseller_penerima->type == "agent")||($withdraw_penerima>0)) ? "Hold" : "On Process";

                            $bank = $this->db->where("bank_id", $reseller_penerima->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
                            $bonus["tanggal"] = $estimasiBonus;
                            $bonus["from_reseller_id"] = $reseller_id;
                            $bonus["type"] = "referal bonus";
                            $bonus["jumlah_deposit"] = $sisa_deposit;
                            $bonus["persentase"] = 10;
                            $bonus["jumlah_bonus"] = $sisa_deposit * 0.1;
                            $bonus["to_reseller_id"] = $referal_id;
                            $bonus["po_produk_id"] = $po_produk_id;
                            $bonus["status"] = $status_bonus_reseller;
                            $this->bonus->insert($bonus);
                            $status_bonus = "On Process";
                            $referal_link  = $this->config->item('url-landing').$reseller_penerima->referal_code;
                            $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                            if ($reseller_penerima->is_frezze == 1) {
                                $status_bonus = "Hold";
                                $status_message = "Bonus anda akan di hold karena akun anda termasuk kedalam akun frezze. Silakan lakukan deposit untuk mencairkan bonus.";
                            }
                            $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 550px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller_penerima->nama . ' ('.$username_penerima.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS REFERENSI dari pendaftaran Reseller/Super Reseller MKB a/n ' . $reseller->nama . '
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah/sisa Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">10%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($sisa_deposit * 0.1) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Status </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $status_bonus . '</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span>
                            ' . $status_message . '
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                       <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                       <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>
                </td>
            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>
</div>
</body></html>';
                            $this->main->mailer_auth('Bonus Referensi', $reseller_penerima->email, $reseller_penerima->nama, $mailContentAdmin);
                        }
                    }


            }
            if(($deposit_anak>=$minimal_po_anak)&&($withdraw_reseller==0)){
                $this->bonus->change_hold_bonus($reseller_id,$estimasiBonus);
            }
            if($reseller_id->is_pass_first_limit==1){
                $next_date = new DateTime(date("Y-m-d"));
                if($reseller->type = 'agent'){
                    $next_date->modify("+1 month");
                } else{
                    $next_date->modify("+2 month");
                }
                $next_date_label = $next_date->format("Y-m-d");
                $next_data = array();
                $next_data['next_limit_date'] = $next_date_label;
                $next_data['skip_deposit_count'] = 0;
                $next_data["is_frezze"] = 0;
                if($deposit_anak<$minimal_po_anak){
                    $next_data["is_frezze"] = 1;
                }
                $this->reseller->update_by_id('reseller_id',$reseller_id,$next_data);
            }
            //here
            if($reseller->leader!=null){
                if($reseller->reseller_id == $reseller->leader){
                    $this->rekap_bulanan->total_po($reseller->leader,$tanggal_rekap);
                } else {
                    $this->rekap_bulanan->total_omzet_reseller($reseller->leader,$tanggal_rekap);
                }
            }


        }
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function uploadImage($file,$url){
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $date = new DateTime();
        $file_name = $date->getTimestamp().random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/".$file_name.".".$ext, $_FILES[$file]['tmp_name'],NET_SFTP_LOCAL_FILE);
        if (!$output)
        {
            $url = "failed";
            return $url;
        }
        else
        {
            $url = $file_name.'.'.$ext;
        }
        return $url;
    }
}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */