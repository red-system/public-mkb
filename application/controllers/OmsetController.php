<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OmsetController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('po_produk','',true);
		$this->load->model('reseller','',true);
		$this->load->model('master_login','',true);
        $this->load->helper('string');
        $this->load->library('main');
        $this->load->helper('url');
    }

    function updateStatusOmset(){
        $reseller = $this->reseller->super_reseller_active();
        foreach ($reseller as $key) {
            $id_reseller = $key->reseller_id;
            $next_deposit_date = $key->next_deposit_date;
            $before_deposit_date = date('Y-m-d', strtotime($next_deposit_date. ' -2 months'));
            $today = date('Y-m-d');
            if ($today > $next_deposit_date) {
                $get_last_deposit_po = $this->po_produk->get_last_deposit_po($id_reseller);
                if ($get_last_deposit_po->tanggal_penerimaan > $next_deposit_date) {
                    $data['tidak_diakui'] = 1;
                    $update = $this->po_produk->update_omset_hangus($id_reseller, $before_deposit_date, $next_deposit_date, $data);
                }
            }
        }
    }

}