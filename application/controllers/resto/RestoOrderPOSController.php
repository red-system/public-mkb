<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RestoOrderPOSController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('pos','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('harga_produk','',true);
        $this->load->model('tipe_pembayaran','',true);

        $this->load->model('log_kasir','',true);
        $this->load->model('unit','',true);
    }
    public function index(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/order.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Rekapitulasi POS < POS < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "pos";
        $data['page'] = "pos";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"order_name"));
        array_push($column, array("data"=>"lokasi_nama"));
        array_push($column, array("data"=>"nama_pelanggan"));
        array_push($column, array("data"=>"staff_nama"));
        array_push($column, array("data"=>"order_note"));
        array_push($column, array("data"=>"tanggal"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        //$data['sumColumn'] = json_encode(array(5,6,7,8,9));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['pos']['resto-pos'] as $key => $value) {
            if($key != "list" && $key != "akses_menu" && $key != "view" && $key != "delete"){
                $action[$key] = $value;
            }
        }
        $data['tipe_pemabayaran'] = $this->tipe_pembayaran->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/resto-pos/order');
        $this->load->view('admin/static/footer');

    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pos->order_count_all();
        $result['iTotalDisplayRecords'] = $this->pos->order_count_all($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pos->order_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->tanggal = date('d-m-Y',$time);
            }
            $key->order_detail_url = base_url().'order-resto-pos-detail/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->penjualan_id));
            $key->order_add_url = base_url().'resto-pos/add-item/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->penjualan_id));
            $key->no = $i;
            $i++;

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function detail(){
        $id = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
        $id = $this->encryption->decrypt($id);
        $penjualan = $this->pos->row_by_id($id);
        if($penjualan!=null){
            $detailPenjualan = $this->pos->detailTransaksi($id);
            $data['penjualan'] = $penjualan;
            $data['detailPenjualan'] = $detailPenjualan;
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "script/admin/order.js");
            $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
            $data['css'] = $this->css;
            $data['js'] = $this->js;
            $data["meta_title"] = "Rekapitulasi POS < POS < ".$_SESSION["redpos_company"]['company_name'];;
            $data['parrent'] = "pos";
            $data['page'] = "resto-pos";
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/resto-pos/order-detail');
            $this->load->view('admin/static/footer');

        }else{
            redirect('404_override','refresh');
        }

    }
    function print(){
        $id = $this->uri->segment(3);
        $id = explode('-',$id);
        $penjualan_produk = $this->pos->penjualan_detail_by_array_id($id);
        $data['penjualan_produk'] = $penjualan_produk;
        $data['penjualan'] = $this->pos->row_by_id($penjualan_produk[0]->penjualan_id);
        $this->load->view("admin/resto-pos/print_order",$data);

    }
    function get_item(){
        $penjualan_id = $this->input->post('id');
        $detailTransaksi = $this->pos->detailTransaksi($penjualan_id);
        $penjualan = $this->pos->row_by_id($penjualan_id);
        $result['tambahan'] = $penjualan->biaya_tambahan;
        $result['potongan'] = $penjualan->potongan_akhir;
        $result['total'] = $penjualan->total;
        $result['grand_total'] = $penjualan->grand_total;
        $result['terbayar'] = $penjualan->terbayar;
        $item = array();
        foreach ($detailTransaksi as $key=>$value){
            $item['trans'.$value->penjualan_id] = $value;
        }
        $result['item'] = $item;
        echo json_encode($result);
    }
    function process(){

        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $save = $this->pos->process_order();
        if($save['result']){
            $transaksi = json_decode($this->input->post('transaksi'));
            $grand_total = $transaksi->grand_total;
            $terbayar = $transaksi->terbayar;
            $kembalian = $terbayar - $grand_total;
            $result['success'] = true;
            $result['message'] = "Transaksi tersimpan";
            $result['kembalian'] = number_format($kembalian);
            $id = $save["id"];
            $result["url"] = base_url()."pos/print/".$id;
        }
        echo json_encode($result);

    }

    function cancel(){
        $result['success'] = false;
        $result['message'] = "Gagal membatalkan item";
        $id = $this->input->post('id');
        $id = explode('-',$id);
        $cancel = $this->pos->cancel_penjualan_item($id);
        if($cancel){
            $result['success'] = true;
            $result['message'] = "Berhasil membatalkan item";
        }
        echo json_encode($result);
    }

}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
