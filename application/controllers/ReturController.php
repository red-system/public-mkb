<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReturController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('produk','',true);
        $this->load->model('user','',true);
        $this->load->model('staff','',true);
        $this->load->model('arus_stock_produk','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('hutang','',true);
        $this->load->model('retur_produk','',true);
        $this->load->model('retur_produk_detail','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/retur.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Retur Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "retur-produk";
        $data['page'] = $this->uri->segment(1);
        $sumColumn = array(4);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"suplier_nama"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"total_retur"));
        array_push($column, array("data"=>"staff_nama"));

        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['retur-produk'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/retur-produk/list');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->retur_produk->index_all();
        $result['iTotalDisplayRecords'] = $this->retur_produk->index_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->retur_produk->index_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->total_retur = $this->idr_currency($key->total_retur);
            $key->delete_url = base_url().'retur-produk/delete/';
            $key->edit_url = base_url().'retur-produk/retur-form/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->id));
            $key->row_id = $key->id;
            $key->posting_url = base_url().'retur-produk/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function retur_form(){
        $id = $this->uri->segment(3);
        $id = $id == '' ? '' : str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $id == '' ? '' : $this->encryption->decrypt($id);
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");

        array_push($this->js, "script/admin/retur.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;

        $column = array();
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"suplier_nama"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $data['action'] = json_encode(array("chose"=>true));
        $data["meta_title"] = "From Retur < Retur Produk< ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "inventori";
        $data['page'] = $this->uri->segment(1);
        $data['lokasi'] = $this->lokasi->all_list();
        $data['id'] = $id;
        unset($_SESSION['retur']);
        $_SESSION['retur']['location_id'] = $data['lokasi'][0]->lokasi_id;
        $retur_produk = null;
        $retur_produk_detail = null;
        if($id!=""){
            $retur_produk = $this->retur_produk->row_by_id($id);
            $_SESSION['retur']['location_id'] = $retur_produk->lokasi_id;
            $retur_produk_detail = $this->po_produk->po_detail_by_po_location($retur_produk->po_produk_id,$retur_produk->lokasi_id);
            foreach ($retur_produk_detail as $item){
                $item->item = $this->retur_produk_detail->jumlah_subtotal_retur_detail($id,$item->produk_id);
                $item->jumlah_stock = $item->jumlah_stock + $item->item->jumlah;
            }
        }
        $data['retur_produk'] = $retur_produk;
        $data['retur_produk_detail'] = $retur_produk_detail;
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/retur-produk/form');
        $this->load->view('admin/static/footer');
    }
    function save(){
        $result['success'] = false;
        $result['message'] = 'Gagal menyimpan data';
        $retur_produk_id = $this->input->post('retur_produk_id');
        $po_id = $this->input->post('input_po_id');
        $tanggal = $this->input->post('tanggal');
        $item = $this->input->post('item');
        $total_retur = $this->input->post('total_retur');
        $staff_id = $_SESSION['redpos_login']['staff_id'];
        $lokasi_id = $_SESSION['retur']['location_id'];
        $this->retur_produk->start_trans();
        if($retur_produk_id == ""){
            $retur_produk = array("lokasi_id"=>$lokasi_id,"po_produk_id"=>$po_id,"tanggal"=>$tanggal,"total_retur"=>$total_retur,"staff_id"=>$staff_id);
            $this->retur_produk->insert($retur_produk);
            $retur_produk_id = $this->retur_produk->last_id();

        } else {
            $retur_produk = array("lokasi_id"=>$lokasi_id,"po_produk_id"=>$po_id,"tanggal"=>$tanggal,"total_retur"=>$total_retur,"staff_id"=>$staff_id);
            $this->retur_produk->update_by_id('id',$retur_produk_id,$retur_produk);
            $this->delete_retur_po($retur_produk_id,"Edit");
        }
        foreach ($item as $val){

            $retur_produk_detail = array();
            $retur_produk_detail['arus_stock_produk'] = "";
            $stock_po_location = $this->stock_produk->stock_produk_by_po_lokasi($po_id,$lokasi_id);
            $row_stock = $this->stock_produk->stock_produk_by_po_lokasi_data($po_id,$lokasi_id);
            $sisa = $val['jumlah'];
            if($stock_po_location>0){

                $stock = array();
                $jumlah_stock = $row_stock->stock_produk_qty < $val['jumlah'] ? 0 : ($stock_po_location-$val['jumlah']);
                $stock['stock_produk_qty'] = $jumlah_stock;
                $sisa = $row_stock->stock_produk_qty <$val['jumlah'] ? ($val['jumlah']-$stock_po_location):0;
                $this->stock_produk->update_by_id('stock_produk_id',$row_stock->stock_produk_id,$stock);

                $arus = array();
                $arus["tanggal"] = date("Y-m-d");
                $arus["table_name"] = "stock_produk";
                $arus["stock_produk_id"] = $row_stock->stock_produk_id;
                $arus["produk_id"] = $val['produk_id'];
                $arus["stock_out"] = $val['jumlah'];
                $arus["stock_in"] = 0;
                $arus["last_stock"] = $this->stock_produk->last_stock($val['produk_id'])->result;
                $arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
                $arus["keterangan"] = "Retur stok produk";
                $arus["method"] = "update";
                $this->arus_stock_produk->insert($arus);
                $retur_produk_detail['arus_stock_produk'] .=$this->arus_stock_produk->last_id()."|";
            }
            $list_stock = $this->stock_produk->stock_by_location_produk_id($lokasi_id,$val['produk_id']);
            foreach ($list_stock as $row){
                if($sisa>0){
                    $data = array();
                    $total = $row->stock_produk_qty - $sisa;
                    $stock_out = $sisa;
                    if($row->stock_produk_qty < $sisa){
                        $total = 0;
                        $stock_out = $row->stock_produk_qty;
                    }
                    $sisa =  ($sisa-$row->stock_produk_qty);
                    $data["stock_produk_qty"] = $total;
                    $stock_produk_id = $row->stock_produk_id;
                    $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
                    if($updateStok){
                        $data = array();
                        $data["tanggal"] = date("Y-m-d");
                        $data["table_name"] = "stock_produk";
                        $data["stock_produk_id"] = $row->stock_produk_id;
                        $data["produk_id"] = $val['produk_id'];
                        $data["stock_out"] = $stock_out;
                        $data["stock_in"] = 0;
                        $data["last_stock"] = $this->stock_produk->last_stock($val['produk_id'])->result;
                        $data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                        $data["keterangan"] = "Retur Produk";
                        $data["method"] = "update";
                        $this->arus_stock_produk->insert($data);
                        $retur_produk_detail['arus_stock_produk'] .= "|".$this->arus_stock_produk->last_id();
                    }
                }else {
                    break;
                }

            }

            $retur_produk_detail['po_produk_id'] = $po_id;
            $retur_produk_detail['retur_produk_id'] = $retur_produk_id;
            $retur_produk_detail['produk_id'] = $val['produk_id'];
            $retur_produk_detail['jumlah'] = $val['jumlah'];
            $retur_produk_detail['harga'] = $val['harga'];
            $retur_produk_detail['sub_total_retur'] = $val['subtotal'];
            $retur_produk_detail['keterangan'] = $val['keterangan'];
            $this->retur_produk_detail->insert($retur_produk_detail);
        }
        $po_produk = $this->po_produk->row_by_id($po_id);
        $hutang = $this->hutang->hutang_by_po($po_id);

        $hutang_data = array();
        $hutang_data["grand_total"] = $po_produk->grand_total - $total_retur;
        $this->hutang->update_by_id('hutang_id',$hutang->hutang_id,$hutang_data);
        $hutang = $this->hutang->hutang_by_po($po_id);
        $process = $this->retur_produk->result_trans();
        if($process){
            $result['success'] = true;
            $result['message'] = 'Berhasil menyimpan data';
        }
        echo json_encode($result);
    }
    function list_po(){
        $lokasi_id = $_SESSION['retur']['location_id'];
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->po_retur_all();
        $result['iTotalDisplayRecords'] = $this->po_produk->po_retur_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->po_produk->po_retur_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->po_produk_id;
            $key->item = $this->po_produk->po_detail_by_po_location($key->po_produk_id,$lokasi_id);
            $key->aksi = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function posting(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $result['success'] = false;
        $result['message'] = "gagal merubah data";
        $data = array();
        $data['post'] = 'ya';
        $post = $this->retur_produk->update_by_id('id',$id,$data);
        if($post){
            $result['success'] = true;
            $result['message'] = "Data berhasil dirubah";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $this->retur_produk->start_trans();
            $retur_produk = $this->retur_produk->row_by_id($id);
            $po_id = $retur_produk->po_produk_id;
            $delete = $this->retur_produk->delete_by_id("id",$id);
            $this->delete_retur_po($id);
            $po_produk = $this->po_produk->row_by_id($po_id);
            $hutang = $this->hutang->hutang_by_po($po_id);
            $hutang_data = array();
            $hutang_data["grand_total"] = $po_produk->grand_total;
            $this->hutang->update_by_id('hutang_id',$hutang->hutang_id,$hutang_data);
            $hutang = $this->hutang->hutang_by_po($po_id);
            $proces = $this->retur_produk->result_trans();
            if($proces){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function change_location(){
        $_SESSION['retur']['location_id'] = $this->input->post('lokasi_id');
    }
    function delete_retur_po($retur_produk_id,$type="Delete"){
        $retur_produk_detail = $this->retur_produk_detail->data_by_retur_id($retur_produk_id);
        foreach ($retur_produk_detail as $key1=>$val1){
            if($val1->arus_stock_produk!=''){
                $arus_stock = explode('|',$val1->arus_stock_produk);
                foreach ($arus_stock as $key2=>$val2){
                    if($key2>0){
                        $arus = $this->arus_stock_produk->row_by_id($val2);
                        $stock_produk_data = $this->stock_produk->row_by_id($arus->stock_produk_id);
                        $stock = array();
                        $stock['stock_produk_qty'] = $stock_produk_data->stock_produk_qty + $arus->stock_out;
                        $this->stock_produk->update_by_id('stock_produk_id',$arus->stock_produk_id,$stock);

                        $arus_stock_data = array();
                        $arus_stock_data["tanggal"] = date("Y-m-d");
                        $arus_stock_data["table_name"] = "stock_produk";
                        $arus_stock_data["stock_produk_id"] = $arus->stock_produk_id;
                        $arus_stock_data["produk_id"] = $arus->produk_id;
                        $arus_stock_data["stock_out"] = 0;
                        $arus_stock_data["stock_in"] = $arus->stock_out;
                        $arus_stock_data["last_stock"] = $this->stock_produk->last_stock($arus->produk_id)->result;
                        $arus_stock_data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                        $arus_stock_data["keterangan"] = $type." Retur stok produk";
                        $arus_stock_data["method"] = "update";

                        $this->arus_stock_produk->insert($arus_stock_data);
                    }
                }
            }
        }
        $this->retur_produk_detail->delete_by_id('retur_produk_id',$retur_produk_id);
    }
    function detail(){
        $id = $this->uri->segment(3);
        $retur_produk = $this->retur_produk->row_by_id($id);
        $_SESSION['retur']['location_id'] = $retur_produk->lokasi_id;
        $retur_produk_detail = $this->po_produk->po_detail_by_po_location($retur_produk->po_produk_id,$retur_produk->lokasi_id);
        foreach ($retur_produk_detail as $item){
            $item->item = $this->retur_produk_detail->jumlah_subtotal_retur_detail($id,$item->produk_id);
            $item->jumlah_stock = $item->jumlah_stock + $item->item->jumlah;
        }
        $result['retur_produk'] = $retur_produk;
        $result['retur_produk_detail'] = $retur_produk_detail;
        echo json_encode($result);
    }
}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */