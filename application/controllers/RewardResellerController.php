<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RewardResellerController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pos');
        $this->load->model('po_produk');
        $this->load->model('reward_reseller');

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");

        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/reward_reseller.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Reward Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reward";
        $data['page'] = $this->uri->segment(1);
        $sumColumn = array(4);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"periode"));
        array_push($column, array("data"=>"terpakai_"));
        array_push($column, array("data"=>"jumlah_bintang_"));
        array_push($column, array("data"=>"jumlah_uang_reward_"));
        array_push($column, array("data"=>"status"));

        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $omset_group = 0;
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
        $penjualan_perbulan = $this->po_produk->penjualan_perbulan($lokasi_id,$reseller_id);
        $this->load->model('rule_reward_reseller','',true);
        $min_penjualan = $this->rule_reward_reseller->min_penjualan();
        $jumlahBintang = $this->reward_reseller->jumlahBintang($reseller_id);
        $passBintang = $jumlahBintang>=16?true:false;
        $pass = $penjualan_perbulan<$min_penjualan?false:true;
        $data['pass'] = $pass;
        $data['passBintang'] = $passBintang;
        $data['penjualan_perbulan'] = $penjualan_perbulan;
        $action = array();

        $data['action'] = json_encode($action);
        $data['reseller_id'] = $reseller_id;
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/reward-reseller/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->reward_reseller->_count_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->reward_reseller->_count_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reward_reseller->_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $key->row_id = $key->reward_reseller_id;
            $i ++;
            $key->total_penjualan_ = number_format($key->total_penjualan);
            $key->terpakai_ = number_format($key->terpakai);
            $key->jumlah_bintang_ = number_format($key->jumlah_bintang);
            $key->jumlah_uang_reward_ = number_format($key->jumlah_uang_reward);
            $month = number_format(date('m'));
            $year = date('Y');
            $month_lbl = $this->monthIdn($month);
            $key->periode = $month_lbl.' '.$year;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function claim_data(){
        $penjualan = $this->input->post('penjualan');
        $data = $this->generate_data($penjualan);
        echo json_encode($data);
    }

    function generate_data($penjualan){
        $data = new stdClass();
        $this->load->model('rule_reward_reseller','',true);
        $min_penjualan = $this->rule_reward_reseller->min_penjualan();
        $rule = $this->rule_reward_reseller->all_list();
        $data->penjualan = $penjualan;
        foreach ($rule as $item){
            if($item->penjualan<=$penjualan){
                $data->diakui = $item->penjualan;
                $data->jumlah_reward = $item->reward;
                $data->bintang = $item->bintang;
                $data->terpakai = $data->diakui;
            }
        }
        return $data;
    }

    function claim(){
        $reseller_id = $this->input->post('reseller_id');
        $penjualan = $this->input->post('penjualan');
        $penjualan_data = $this->generate_data($penjualan);
        $data = array();
        $data['reseller_id'] = $reseller_id;
        $data['total_penjualan'] = $penjualan_data->penjualan;
        $data['jumlah_bintang'] = $penjualan_data->bintang;
        $data['terpakai'] = $penjualan_data->terpakai;
        $data['jumlah_uang_reward'] = $penjualan_data->jumlah_reward;
        $data['tanggal'] = date("Y-m-d");
        $insert = $this->reward_reseller->insert($data);
        $result['success'] = false;
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function list_tambahan(){
        $result['iTotalRecords'] = 0;
        $result['iTotalDisplayRecords'] = 0;
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $jumlahBintang = $this->reward_reseller->jumlahBintang($reseller_id);
        $data = array();
        $object1 = new stdClass();
        $object1->nama_reward = "Emas Senilai 4,5 Juta";
        $object1->jumlah_bintang = 16;
        $object1->row_id = 1;
        $object1->reseller_id = $reseller_id;

        $object2 = new stdClass();
        $object2->nama_reward = "Handphone Senilai 14 Juta";
        $object2->jumlah_bintang = 48;
        $object2->row_id = 2;
        $object2->reseller_id = $reseller_id;
        if($jumlahBintang>=16) {
            array_push($data, $object1);
        }
        if($jumlahBintang>=48){
            array_push($data,$object2);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function claim_tambahan(){

        $reseller_id = $this->input->post('reseller_id');
        $row_id = $this->input->post('row_id');
        $data['reseller_id'] = $reseller_id;
        $data['jumlah_bintang'] = $row_id==1?-16:-48;
        $data['jumlah_uang_reward'] = $row_id==1?4500000:14000000;
        $insert = $this->reward_reseller->insert($data);
        $result['success'] = false;
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }

}


/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */