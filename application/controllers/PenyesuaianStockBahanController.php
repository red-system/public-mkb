<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenyesuaianStockBahanController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('bahan','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('history_penyesuaian_bahan','',true);
		$this->load->model('lokasi','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
	
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = " Penyesuaian Stock Bahan < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-bahan';
		$target = array(0,4);
		$sumColumn = array(4);
		array_push($column, array("data"=>"no"));
		// array_push($column, array("data"=>"bahan_kode"));
		array_push($column, array("data"=>"bahan_nama"));
		// array_push($column, array("data"=>"jenis_bahan_nama"));
		array_push($column, array("data"=>"satuan_nama"));
		array_push($column, array("data"=>"bahan_harga_lbl"));
		array_push($column, array("data"=>"stock"));
		if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			array_push($column, array("data"=>"jumlah_lokasi"));
			array_push($target, 6);
			array_push($sumColumn, 6);
		}
		// array_push($column, array("data"=>"suplier_nama"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$data['sumColumn'] = json_encode($sumColumn);
		$data["list_url"] = base_url()."penyesuaian-bahan/list";
		$data["action"] = json_encode(array("stock"=>true,"view"=>false,"edit"=>false,"delete"=>false));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/penyesuaian_bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bahan->bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->bahan->bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bahan->bahan_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->delete_url = base_url().'bahan/delete/';
			$key->row_id = $key->bahan_id;
			$key->bahan_harga_lbl = ($key->bahan_harga);
			$key->stok_url = base_url().'penyesuaian-bahan/stock/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->bahan_id));
			$i++;
			$key->stock = number_format($key->stock);
			if(isset($_SESSION["redpos_login"]['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}
		}
		$result['aaData'] = $data;				
		echo json_encode($result);
	}
	function stock_index(){
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penyesuaian Stok < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-bahan';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$bahan = $this->bahan->bahan_by_id($id);
		$data['id'] = $id;

		if ($bahan != null) {
			$data['bahan'] = $bahan;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"lokasi_nama"));
			// array_push($column, array("data"=>"stock_bahan_seri"));
			array_push($column, array("data"=>"total_bahan"));
			$data['sumColumn'] = json_encode(array(2));	
			$data['column'] = json_encode($column);
			$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
			$data["action"] = json_encode(array("adjust"=>true));
			$data["list_url"] = base_url()."stock-bahan/list-stock/".$id;
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/penyesuaian_bahan');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
	}

	function adjust(){

		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$bahan_id = $this->input->post('bahan_id');
		$gudang = $this->lokasi->all_gudang();
		$gudang = $gudang[0];

		$total_qty = str_replace(",", "", $this->input->post('total_bahan'));
		$old_total = str_replace(",", "", $this->input->post('old_total'));

		$selisih = $old_total - $total_qty;

		if ($selisih != 0) {
			if ($total_qty > $old_total) {

				$row_stock = $this->stock_bahan->stock_by_location_bahan_id($gudang->lokasi_id,$bahan_id);

				foreach ($row_stock as $row) {

					$stock = array();

					$jumlah = $row->stock_bahan_qty + abs($selisih);
					$stock_out = 0;
					$stock_in = abs($selisih);

					$stock["stock_bahan_qty"] = $jumlah;
					$stock_bahan_id = $row->stock_bahan_id;

					$updateStok = $this->stock_bahan->update_by_id('stock_bahan_id',$stock_bahan_id,$stock);

					if($updateStok){

						$arus["tanggal"] = date("Y-m-d");
						$arus["table_name"] = "stock_bahan";
						$arus["stock_bahan_id"] = $stock_bahan_id;
						$arus["bahan_id"] = $this->input->post('bahan_id');
						$arus["stock_in"] = $stock_in;
						$arus["stock_out"] = $stock_out;
						// $arus_last_stock = $this->stock_bahan->last_stock_gudang($bahan_id);
						$arus["last_stock"] = $this->stock_bahan->last_stock_gudang($bahan_id)->result;
						// $arus_last_stock_total = $this->stock_bahan->stock_total_gudang();
                        $arus["last_stock_total"] = $this->stock_bahan->stock_total_gudang()->result;
						$arus["keterangan"] = "Penyesuaian stok bahan";
						$arus["method"] = "update";
						$this->stock_bahan->arus_stock_bahan($arus);

						$result['success'] = true;
						$result['message'] = "Data berhasil disimpan";
						$data = array();
						$data = array();
						$data["tanggal"] = date("Y-m-d");
						$data["bahan_id"] = $this->input->post('bahan_id');
						// $data["jenis_bahan_id"] = $this->input->post('jenis_bahan_id');
						$data["lokasi_id"] = $this->input->post('stock_bahan_lokasi_id');
						$data["stock_bahan_id"] = $stock_bahan_id;
						// $data["bahan_kode"] = $this->input->post('bahan_kode');
						$data["bahan_nama"] = $this->input->post('bahan_nama');
						// $data["jenis_bahan_nama"] = $this->input->post('jenis_bahan_nama');
						$data["lokasi_nama"] = $this->input->post('lokasi_nama');
						$data["qty_awal"] = $row->stock_bahan_qty;
						$data["qty_akhir"] = $jumlah;
						$data["keterangan"] = $this->input->post('keterangan');
						$this->history_penyesuaian_bahan->insert($data);

					}

					break;
				}

			}else{
				$row_stock = $this->stock_bahan->stock_by_location_bahan_id($gudang->lokasi_id,$bahan_id);
				foreach ($row_stock as $row) {

					$stock = array();

                    $jumlah = $row->stock_bahan_qty - abs($selisih);
                    $stock_out = abs($selisih);
                    if($row->stock_bahan_qty < abs($selisih)){
                        $jumlah = 0;
                        $stock_out = $row->stock_bahan_qty;
                    }
                    $selisih =  (abs($selisih) - $row->stock_bahan_qty);
                    $stock["stock_bahan_qty"] = $jumlah;
                    $stock_bahan_id = $row->stock_bahan_id;
                    $updateStok = $this->stock_bahan->update_by_id('stock_bahan_id',$stock_bahan_id,$stock);
                    if($updateStok){
                        $arus = array();
                        $arus["tanggal"] = date("Y-m-d");
                        $arus["table_name"] = "stock_bahan";
                        $arus["stock_bahan_id"] = $stock_bahan_id;
                        $arus["bahan_id"] = $this->input->post('bahan_id');
                        $arus["stock_out"] = $stock_out;
                        $arus["stock_in"] = 0;
                        $arus["last_stock"] = $this->stock_bahan->last_stock_gudang($bahan_id)->result;
                        $arus["last_stock_total"] = $this->stock_bahan->stock_total_gudang()->result;
                        $arus["keterangan"] = "Penyesuaian stok bahan";
                        $arus["method"] = "update";
                        $this->stock_bahan->arus_stock_bahan($arus);

						$result['success'] = true;
						$result['message'] = "Data berhasil disimpan";
						$data = array();
						$data = array();
						$data["tanggal"] = date("Y-m-d");
						$data["bahan_id"] = $this->input->post('bahan_id');
						// $data["jenis_bahan_id"] = $this->input->post('jenis_bahan_id');
						$data["lokasi_id"] = $this->input->post('stock_bahan_lokasi_id');
						$data["stock_bahan_id"] = $stock_bahan_id;
						// $data["bahan_kode"] = $this->input->post('bahan_kode');
						$data["bahan_nama"] = $this->input->post('bahan_nama');
						// $data["jenis_bahan_nama"] = $this->input->post('jenis_bahan_nama');
						$data["lokasi_nama"] = $this->input->post('lokasi_nama');
						$data["qty_awal"] = $row->stock_bahan_qty;
						$data["qty_akhir"] = $jumlah;
						$data["keterangan"] = $this->input->post('keterangan');
						$this->history_penyesuaian_bahan->insert($data);
                    }
                    if($selisih <= 0){
                        break;
                    }

                    if ($jumlah > 0) {
						break;
					}
				}
			}
		}


		echo json_encode($result);
	}

	function adjust_bahan(){
		// var_dump($this->input->post());
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data["stock_bahan_qty"] = $this->string_to_number($this->input->post('stock_bahan_qty'));
		$stock_bahan_id = $this->input->post('stock_bahan_id');
		$old_data = $this->stock_bahan->row_by_id($stock_bahan_id);
		$update = $this->stock_bahan->update_by_id('stock_bahan_id',$stock_bahan_id,$data);
		if($update){
				$data = array();
				$diff = $old_data->stock_bahan_qty - $this->string_to_number($this->input->post('stock_bahan_qty'));
				if ($diff != 0) {
					$data["tanggal"] = date("Y-m-d");
					$data["table_name"] = "stock_bahan";
					$data["stock_bahan_id"] = $stock_bahan_id;
					$data["bahan_id"] = $this->input->post('bahan_id');
					if ($diff > 0) {
						$data["stock_out"] = abs($diff);
						$data["stock_in"] = 0;
					} else {
						$data["stock_in"] = abs($old_data->stock_bahan_qty - $this->string_to_number($this->input->post('stock_bahan_qty')));
						$data["stock_out"] = 0;
					}
					$data["last_stock"] = $this->stock_bahan->last_stock($this->input->post('bahan_id'))->result;
					$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
					$data["keterangan"] = "Penyesuaian stok bahan";
					$data["method"] = "update";
					$this->stock_bahan->arus_stock_bahan($data);
				}
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["bahan_id"] = $this->input->post('bahan_id');
				// $data["jenis_bahan_id"] = $this->input->post('jenis_bahan_id');
				$data["lokasi_id"] = $this->input->post('stock_bahan_lokasi_id');
				$data["stock_bahan_id"] = $stock_bahan_id;
				// $data["bahan_kode"] = $this->input->post('bahan_kode');
				$data["bahan_nama"] = $this->input->post('bahan_nama');
				// $data["jenis_bahan_nama"] = $this->input->post('jenis_bahan_nama');
				$data["lokasi_nama"] = $this->input->post('lokasi_nama');
				$data["qty_awal"] = $old_data->stock_bahan_qty;
				$data["qty_akhir"] = $this->string_to_number($this->input->post('stock_bahan_qty'));
				$data["keterangan"] = $this->input->post('keterangan');
				$this->history_penyesuaian_bahan->insert($data);
		}
		echo json_encode($result);
	}
}

/* End of file PenyesuaianStockBahanController.php */
/* Location: ./application/controllers/PenyesuaianStockBahanController.php */