<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest','',true);
        $this->load->model('pos','',true);
        $this->load->model('hutang','',true);
        $this->load->model('piutang','',true);
        $this->load->model('reseller','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('bonus','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('rekap_bulanan','',true);
        $this->load->model('withdraw_cash','',true);
        $this->load->model('user_role','',true);
        $this->load->model('pembayaran_hutang_produk','',true);
    }

    public function index()
    {

        $this->load->model("omset_hangus",'',true);
        if($_SESSION["redpos_login"]['user_role_id']==2&&$this->input->get('start_date')==''){
            $start_date_awal = date("Y-m").'-01';
            $end_date_awal = date('Y-m-d');
            redirect(base_url().'?start_date='.$start_date_awal.'&end_date='.$end_date_awal);
        }
        $lokasi_id = isset($_SESSION["redpos_login"]["lokasi_id"])?$_SESSION["redpos_login"]["lokasi_id"]:null;
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->js, "vendors/general/raphael/raphael.js");
        array_push($this->js, "vendors/general/morris.js/morris.js");
        array_push($this->css, "vendors/general/morris.js/morris.css");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");

        array_push($this->js, "script/admin/home4.js");
        $data["meta_title"] = "Dashboard < ".$_SESSION["redpos_company"]['company_name'];;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data['parrent'] = "dasboard";
        $data['page'] = "dashboard";
        $data["chartData"] = array();
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;

        $lokasi_id = null;
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION["redpos_login"]['lokasi_id'];
            $lokasi_id = $_SESSION["redpos_login"]['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);

        $reselller = null;
        $npwp = 0;
        $jenis_kelamin = "";

        if($lokasi_id!=null){
            $reselller = $this->getReseller();
            $reseller_id = $reselller->reseller_id;
            $this->load->model('npwp','',true);
            $cek = $this->npwp->find_npwp($reseller_id);
            if ($cek&&$reselller->status=="active"){
                $npwp = 1;
            }
            if($reselller->jenis_kelamin!=null&&$reselller->jenis_kelamin!=""){
                $jenis_kelamin = $reselller->jenis_kelamin;
            }
        }
        $data['npwp'] = $npwp;
        $reselller_id = $reselller == null ? null : $reselller->reseller_id;

        $data['jenis_kelamin'] = $jenis_kelamin;
        $data["salesMonth"] = $this->po_produk->penjualanByMonth($search,$reselller_id);
        $data['bonus_lunas'] = $this->bonus->bonus_lunas($reselller_id);
        $data['withdraw_cash'] = $this->withdraw_cash->withdraw_cash_lunas();


        foreach ($data["salesMonth"] as $key) {
            $temp = explode(" ", $key->label);
            $temp[0] = $this->monthIdn($temp[0]);
            $key->label = $temp[0]." ".$temp[1];
            array_push($data["chartData"], array("y"=>$key->label,"a"=>$key->sales));
        }
        $user_role_id = $_SESSION["redpos_login"]['user_role_id'];
        if($reselller==null){
            $status = "active";
            $aktif_agen = $this->reseller->agen_status_all($status);
            $aktif_super_agen = $this->reseller->super_agen_status_all($status);
            $total_po = $this->po_produk->total_po();
            $mkb_terjual = $this->po_produk->total_mkb();;
            $ls_terjual = $this->po_produk->total_ls();;
            $data['mkb_terjual'] = $mkb_terjual;
            $data['ls_terjual'] = $ls_terjual;
            $data['aktif_agen'] = $aktif_agen;
            $data['aktif_super_agen'] = $aktif_super_agen;
            $data['total_po'] = $total_po;
            $data['total_omset_reward'] = $this->rekap_bulanan->total_omset_reward();
            $data['budget_yang_keluar'] = $data['total_omset_reward'] * 0.04;
            $jumlah_leader = $this->rekap_bulanan->rekap_omset_group_all();

            $rekap_omset_leader = $this->rekap_bulanan->rekap_omset_group_list(0,$jumlah_leader,'');
            $total_omset_leader = $this->rekap_bulanan->total_omset_leader();


            $newObject = new stdClass;
            $newObject->nama = 'Lain-lain';
            $newObject->total = $total_po->grand_total - $total_omset_leader;

                array_push($rekap_omset_leader,$newObject);
            $data['rekap_omset_leader'] = $rekap_omset_leader;
            $curMonth = number_format(date('m'));
            $waktu_end = date("Y-m-t");
            $first_date = date("Y-m-01");
            $datetime_start = new DateTime($first_date);
            $datetime_start->modify("-2 month");
            $waktu_start = $datetime_start->format("Y-m-d");
            $startMonth = $curMonth - 2;
            $curYear = (date('Y'));
            $startYear = $curYear;
            if($startMonth <= 0){
                $startMonth = 12 + $startMonth;
                $startYear = $curYear - 1;

            }
            $rangeMonth = range($startMonth,$curMonth);
            $curMonthLbl = $this->monthIdn($curMonth);
            $startMonthLbl = $this->monthIdn($startMonth);
            $data['curMonthLbl'] = $curMonthLbl;
            $data['startMonthLbl'] = $startMonthLbl;
            $data['curYear'] = $curYear;
            $data['startYear'] = $startYear;
        }
        if($user_role_id==7){
            $warning_redeposit = true;
            $date1 = date_create(date("Y-m-d"));
            $date2 = date_create($reselller->next_deposit_date);
            $diff=date_diff($date1,$date2);
            $sisa_tanggal = $diff->format("%R")=="-"?$diff->format("%R%a"):$diff->format("%a");
            $warning_message = "Halooo Kak, ".$sisa_tanggal." hari lagi loh waktu untuk redeposit. Ayo segera sebelum
                    omsetnya hangus";
            if(($sisa_tanggal > 0) && ($sisa_tanggal <=7)){
                $warning_message = "Halooo Kak, ".$sisa_tanggal." hari lagi loh waktu untuk redeposit. Ayo segera sebelum
                    omsetnya hangus";
            } else if($sisa_tanggal == 0){
                $warning_message = "Halooo Kak,  hari ini terakhir waktu untuk redeposit. Ayo segera sebelum
                    omsetnya hangus";
            } else if ($sisa_tanggal < 0){
                $warning_message = "Halooo Kak, karena kakak belum melakukan deposit di tanggal ".$reselller->next_deposit_date." atau sebelumnya, maka omset kakak yang sebelumnya dihanguskan ";
            }else {
                $warning_redeposit = false;
            }

            $data['sisa_tanggal'] = $sisa_tanggal;
            $data['warning_message'] = $warning_message;
            $data['warning_redeposit'] = $warning_redeposit;

            $this->load->model('claim_reward','',true);

            $annual_omset = $this->po_produk->anual_omset($reseller_id);
            if($annual_omset!=null){
                $annual_omset = $annual_omset->omset == null ? 0 :$annual_omset->omset;
            }else{
                $annual_omset = 0;
            }
            $annual_rank = sizeof($this->po_produk->annual_rank($annual_omset));
            $data['annual_rank'] = $annual_rank;
            $curMonth = number_format(date('m'));
            $waktu_end = date("Y-m-t");
            $first_date = date("Y-m-01");
            $thailand_poin = 0;
            $thailand_join = 0;
            $datetime_start = new DateTime($first_date);
            $datetime_start->modify("-2 month");
            $waktu_start = $datetime_start->format("Y-m-d");
            $base = $this->claim_reward->cek_base($reseller_id);
            if($base==0){
                if($this->po_produk->is_deposit_in_april($lokasi_id)){
                    $waktu_start = $this->config->item('reward-start-if-deposit-april');

                } else{
                    $waktu_start = $this->config->item('start-count-reward');
                }

            }else{
                if($this->po_produk->is_deposit_in_april($lokasi_id)){
                    $tanggal_claim = $this->claim_reward->get_base_date($reseller_id);

                    $datetime_start = new DateTime(date("Y-m-01",strtotime($tanggal_claim)));
                    if($tanggal_claim<=$this->config->item('old-reward-date')){
                        $waktu_start = '2021-07-01';
                    }else{
                        $datetime_start->modify("-2 month");
                        $waktu_start = $datetime_start->format("Y-m-d");
                    }
                } else{
                    $waktu_start = $this->config->item('start-count-reward');
                }



            }
//            $waktu_start = $this->config->item('start-count-reward');
            $startMonth = number_format(date('m',strtotime($waktu_start)));
            $curYear = (date('Y'));
            $startYear = date('Y',strtotime($waktu_start));;

            $curMonthLbl = $this->monthIdn($curMonth);
            $startMonthLbl = $this->monthIdn($startMonth);
            $data['curMonthLbl'] = $curMonthLbl;
            $data['startMonthLbl'] = $startMonthLbl;
            $data['curYear'] = $curYear;
            $data['startYear'] = $startYear;
            $total_po = $this->po_produk->omset_referensi($reseller_id);
            $data['total_po'] = $total_po;
            $omset_group = $this->rekap_bulanan->omset_group($reseller_id,$waktu_start,$waktu_end);
            $omset_hangus = $this->omset_hangus->omset_hangus_reseller($reseller_id);
            $omset_group = $omset_group - $omset_hangus;
            $data['omset_group'] = $omset_group;
            $reseller_referensi = $this->reseller->reseller_refernsi($reseller_id);
            $ref_res = 0;
            $ref_sup = 0;
            if($reseller_referensi!=null){
                foreach ($reseller_referensi as $key){
                    if($key->type == 'agent'){
                        $ref_res = $key->jumlah;
                    }else {
                        $ref_sup = $key->jumlah;
                    }
                }
            }
            $data['ref_res'] = $ref_res;
            $data['ref_sup'] = $ref_sup;
            $this->load->model('claim_reward','',true);
            $data['rangking'] = $this->claim_reward->get_bintang($reseller_id);
            $sisa_deposit = 27500000;
            $deposit_before = $this->po_produk->deposit_before_this_year($reseller_id);
            $deposit_this_year = $this->po_produk->deposit_this_year($reseller_id);
            if($sisa_deposit > $deposit_before){
                $sisa_deposit = $sisa_deposit - $deposit_before;
            }else{
                $sisa_deposit = 0;
            }
            $qty_deposit = $deposit_this_year - $sisa_deposit;
            $this->load->model('reward_thailand','',true);
            $reward_thailand = $this->reward_thailand->row_by_field('reseller_id',$reseller_id);
            if($reward_thailand!=null){
                $thailand_poin = $reward_thailand->total_poin;
                $thailand_join = $reward_thailand->total_join;

            }
            $data['qty_deposit'] = $qty_deposit;
            $data['thailand_poin'] = $thailand_poin;
            $data['thailand_join'] = $thailand_join;
        }
        if($user_role_id==6){
            $this->load->model('reward_reseller','',true);
            $bintang = $this->reward_reseller->bintang_reseller($reseller_id);
            $data['jumlah_bintang_reseller'] = $bintang;
            $omset_reseller = $this->po_produk->omset_bulanan_reseller($lokasi_id);
            $data['omset_reseller'] = $omset_reseller;
            $reseller_annual_omset = $this->po_produk->reseller_anual_omset($reseller_id);
            if($reseller_annual_omset!=null){
                $reseller_annual_omset = $reseller_annual_omset->omset;
                $reseller_annual_rank = sizeof($this->po_produk->reseller_annual_rank($reseller_annual_omset));
            }else{
                $reseller_annual_rank = "-";
            }
            $this->load->model('kuisioner','',true);
            $this->load->model('survey_response','',true);
            $pernah_isi_survey = $this->survey_response->pernah_isi_survey($reseller_id);
            $data["pernah_isi_survey"] = $pernah_isi_survey;
            $pertanyaan = $this->kuisioner->all_list();
            $leader = $this->reseller->row_by_id($reselller->leader);
            $leader_name = $leader->nama;
            $leader_lain = $this->reseller->leaderLain($reselller->leader);
            $data['leader_lain'] = $leader_lain;
            $data['leader_name'] = $leader_name;
            $data['pertanyaan'] = $pertanyaan;
            $data['reseller_annual_rank'] = $reseller_annual_rank;
        }

        $reselller = $this->reseller->row_by_id($reselller_id);
        if (@$reselller->status == 'active') {
            if (@$reselller->latitude == null && @$reselller->longitude == null) {
                $data['status_latlon'] = true;
            }else{
                $data['status_latlon'] = false;
            }
        }else{
            $data['status_latlon'] = false;
        }


        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');



        $check = 0;
        if($reselller!=null){
            $check = $reselller->first_deposit + $reselller->is_red;
        }
        if($reselller_id!=null && $check == 0){
            $this->load->view('admin/home/pasive');
        } else if($reselller_id!=null && $reselller->is_frezze == 1){
            $this->load->view('admin/home/frezze');
        } else if($reselller==null&& $user_role_id!=11){
            if($user_role_id==13){
                $this->load->view('admin/home/pajak');
            }else {
                $this->load->view('admin/home_admin');
            }

        }else if($reselller==null&& $user_role_id==11){
            $this->load->view('admin/home_admin_ter');
        } else {
            if($user_role_id==7){
                $this->load->view('admin/home/super');
            } else {

                $this->load->view('admin/home');
            }
//            $this->load->view('admin/home');
        }
        if($_SESSION['redpos_login']['user_role_id']==9){
            redirect(base_url()."money-back");
        }
        $this->load->view('admin/static/footer');
    }
    function country(){
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION["redpos_login"]['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->guest->country_count_all($search);
        $result['iTotalDisplayRecords'] = $this->guest->country_count_filter($query,$search);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->guest->country_list($start,$length,$query,$search);
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function most(){
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["min_tanggal"] = $data["min_tanggal"] == null ? date('Y-m-d') : $data["min_tanggal"];
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION["redpos_login"]['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $result['iTotalRecords'] = $this->pos->mostPenjualanDashboardCount($search);
        $result['iTotalDisplayRecords'] = $this->pos->mostPenjualanDashboardCount($search);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->pos->mostPenjualanDashboardList($search);
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function least(){
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["min_tanggal"] = $data["min_tanggal"] == null ? date('Y-m-d') : $data["min_tanggal"];
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION["redpos_login"]['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $result['iTotalRecords'] = $this->pos->leastPenjualanDashboardCount($search);
        $result['iTotalDisplayRecords'] = $this->pos->leastPenjualanDashboardCount($search);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->pos->leastPenjualanDashboardList($search);
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function piutang_akan(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->piutang->piutang_akan_count_all();
        $result['iTotalDisplayRecords'] = $this->piutang->piutang_akan_count_filter($query);
        $data =  $this->piutang->piutang_akan_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->piutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'piutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
            $key->posting_url = base_url().'piutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function piutang_jatuh(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->piutang->piutang_jatuh_count_all();
        $result['iTotalDisplayRecords'] = $this->piutang->piutang_jatuh_count_filter($query);
        $data =  $this->piutang->piutang_jatuh_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->piutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'piutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
            $key->posting_url = base_url().'piutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function hutang_akan(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->hutang->hutang_akan_count_all();
        $result['iTotalDisplayRecords'] = $this->hutang->hutang_akan_count_filter($query);
        $data =  $this->hutang->hutang_akan_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->hutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'hutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
            $key->posting_url = base_url().'hutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function hutang_jatuh(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->hutang->hutang_jatuh_count_all();
        $result['iTotalDisplayRecords'] = $this->hutang->hutang_jatuh_count_filter($query);
        $data =  $this->hutang->hutang_jatuh_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->hutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'hutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
            $key->posting_url = base_url().'hutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function bonus(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $all = $this->bonus->bonus_all($reseller_id);
        $filter = $this->bonus->bonus_filter($query,$reseller_id);
        $result['iTotalRecords'] =  $all > 5 ? 5 : $all ;
        $result['iTotalDisplayRecords'] = $filter > 5 ? 5 :$filter;
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->bonus->bonus_list(0,5,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;
            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function group(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->reseller->group_count_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->reseller->group_count_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->group_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'guest/delete/';
            $key->row_id = $key->reseller_id;
            $key->action = null;
            $key->type_group = "Downline";
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function request_agen(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reseller->prospective_all();
        $result['iTotalDisplayRecords'] = $this->reseller->prospective_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->prospective_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function rekap_omset_leader(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->rekap_bulanan->rekap_omset_group_all();
        $result['iTotalDisplayRecords'] = $this->rekap_bulanan->rekap_omset_group_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->rekap_bulanan->rekap_omset_group_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
            $key->total = number_format($key->total);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function request_super(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reseller->super_all();
        $result['iTotalDisplayRecords'] = $this->reseller->super_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->link_foto_profile = $this->config->item('dev_storage_image').$key->foto_profile;
            $key->link_foto_ktp = $this->config->item('dev_storage_image').$key->foto_ktp;
            $key->link_selfi_ktp = $this->config->item('dev_storage_image').$key->selfi_ktp;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function bonus_admin(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $all = $this->bonus->bonus_admin_all();
        $filter = $this->bonus->bonus_admin_filter($query);
        $result['iTotalRecords'] =  $all > 5 ? 5 : $all ;
        $result['iTotalDisplayRecords'] = $filter > 5 ? 5 :$filter;
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->bonus->bonus_admin_list(0,5,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;
            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function po(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $all = $this->po_produk->po_produk_count_all_admin();
        $filter = $this->po_produk->po_produk_count_filter_admin($query);
        $result['iTotalRecords'] = $all > 5 ? 5 : $all ;
        $result['iTotalDisplayRecords'] = $filter > 5 ? 5 :$filter;
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_produk->po_produk_list_admin(0,5,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);
            $key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
            $key->no = $i;
            if($key->jenis_pembayaran == "kas"){
                $key->tipe_pembayaran_nama = $key->kas_nama." ".$key->no_akun;
            }
            $i++;
            if($key->status_pembayaran != "Hutang"){
                $key->deny_edit = true;
                $key->deny_delete = true;
                $key->deny_pembayaran = true;
                $key->detail_pembayaran_btn = true;
            }
            if($key->status_pembayaran == "Gagal"||$key->status_pembayaran == "Batal"){
                $key->ulangi_pembayaran_btn = true;
            }

            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
            $key->delete_url = base_url().'po-produk/delete/';
            $key->row_id = $key->po_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function order_reseller(){
        $total_po = 0;
        $db_po = $this->po_produk->cek_jumlah_po_lunas();
        $super_agen = $this->reseller->count_super_agen();
        $agen = $this->reseller->count_agen();
        $pembayaran_hutang_produk = $this->pembayaran_hutang_produk->_all();
        $reseller = $super_agen +$agen;
        $db_po = $db_po > 0 ? $db_po : $total_po;
        $data['db_po'] = $db_po;
        $data['super_agen'] = $super_agen;
        $data['agen'] = $agen;
        $data['reseller'] = $reseller;
        $hutang_piutang = $pembayaran_hutang_produk;
        $data['hutang_piutang'] = $hutang_piutang;
        $data['pembayaran_hutang_produk'] = $pembayaran_hutang_produk;
        echo json_encode($data);
    }
    function total_bonus_lunas(){

    }
    function rekap_bulanan(){
        $lokasi_id = isset($_SESSION["redpos_login"]["lokasi_id"])?$_SESSION["redpos_login"]["lokasi_id"]:null;
        $this->load->model('claim_reward','',true);
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $curMonth = number_format(date('m'));
        $waktu_end = date("Y-m-t");
        $first_date = date("Y-m-01");
        $datetime_start = new DateTime($first_date);
        $datetime_start->modify("-2 month");
        $waktu_start = $datetime_start->format("Y-m-d");
        $base = $this->claim_reward->cek_base($reseller_id);
        if($base==0){
            if($this->po_produk->is_deposit_in_april($lokasi_id)){
                $waktu_start = $this->config->item('reward-start-if-deposit-april');

            } else{
                $waktu_start = $this->config->item('start-count-reward');
            }

        }else{
            if($this->po_produk->is_deposit_in_april($lokasi_id)){
                $tanggal_claim = $this->claim_reward->get_base_date($reseller_id);

                $datetime_start = new DateTime(date("Y-m-01",strtotime($tanggal_claim)));
                if($tanggal_claim<=$this->config->item('old-reward-date')){
                    $waktu_start = '2021-07-01';
                }else{
                    $datetime_start->modify("-2 month");
                    $waktu_start = $datetime_start->format("Y-m-d");
                }
            } else{
                $waktu_start = $this->config->item('start-count-reward');
            }
        }
        $startMonth = number_format(date('m',strtotime($waktu_start)));
        $curYear = (date('Y'));
        $startYear = date('Y',strtotime($waktu_start));;


        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->rekap_bulanan->rekap_bulanan_reseller_all($reseller_id,$waktu_start,$waktu_end);
        $result['iTotalDisplayRecords'] = $this->rekap_bulanan->rekap_bulanan_reseller_filter($query,$reseller_id,$waktu_start,$waktu_end);
        $data =  $this->rekap_bulanan->rekap_bulanan_reseller_list($start,$length,$query,$reseller_id,$waktu_start,$waktu_end);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->monthLbl = $this->monthIdn($key->month);
            $key->total_po = number_format($key->total_po);
            $key->total_omset_reseller = number_format($key->total_omset_reseller);
            $key->withdraw_cash = number_format($key->withdraw_cash);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function reward(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $this->load->model('reward');
        $result['iTotalRecords'] = $this->reward->all();
        $result['iTotalDisplayRecords'] = $this->reward->filter($query);
        $data =  $this->rekap_bulanan->reward->_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->omset = number_format($key->omset);
            $key->budget_reward = number_format($key->budget_reward);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function performa_group(){
            if($_GET['order'][0]["column"]!="0"){
                switch ($this->input->get('order')[0]["column"]){
                    case "1":
                        $_GET['ordered'] = 'reseller.nama';
                        break;
                    case "2":
                        $_GET['ordered'] = 'total_downline';
                        break;
                    case "3":
                        $_GET['ordered'] = 'jumlah_bonus';
                        break;
                    case "4":
                        $_GET['ordered'] = 'total_po';
                        break;
                }
            }
            $_GET['direct'] = $_GET['order'][0]["dir"];


        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->rekap_bulanan->rekap_aktivitas_group_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->rekap_bulanan->rekap_aktivitas_group_filter($query,$reseller_id);
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->rekap_bulanan->rekap_aktivitas_group($start,$length,$query,$reseller_id);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->total_downline = number_format($key->total_downline);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->total_po = number_format($key->total_po);
        }
        $result['aaData'] = $data;
        echo json_encode($result);

        ;
    }
    function performa_super(){
        if($_GET['order'][0]["column"]!="0"){
            switch ($this->input->get('order')[0]["column"]){
                case "1":
                    $_GET['ordered'] = 'reseller.nama';
                    break;
                case "2":
                    $_GET['ordered'] = 'total_po';
                    break;
                case "3":
                    $_GET['ordered'] = 'jumlah_penukaran';
                    break;
                case "4":
                    $_GET['ordered'] = 'jumlah_withdraw';
                    break;
            }
        }
        $_GET['direct'] = $_GET['order'][0]["dir"];
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->rekap_bulanan->rekap_performa_supres_level_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->rekap_bulanan->rekap_performa_supres_level_filter($query,$reseller_id);
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->rekap_bulanan->rekap_performa_supres_level($start,$length,$query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->total_po = number_format($key->total_po);
            $key->jumlah_penukaran = number_format($key->jumlah_penukaran);
            $key->jumlah_withdraw = number_format($key->jumlah_withdraw);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
        ;
    }
    function po_perprovinsi(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->uang_masuk_per_propinsi_all();
        $result['iTotalDisplayRecords'] = $this->po_produk->uang_masuk_per_propinsi_filter($query);
        $data =  $this->rekap_bulanan->po_produk->uang_masuk_per_propinsi_list($start,$length,$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->total = number_format($key->total);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function leader_board(){
        $query = $this->input->get('search')["value"];
        $start = 0;
        $length = 20;
        $result['iTotalRecords'] = 20;
        $result['iTotalDisplayRecords'] = 20;
        $data =  $this->rekap_bulanan->po_produk->leader_board_data($start,$length,$query);
        if(sizeof($data)<20){
            $result['iTotalDisplayRecords'] = sizeof($data);
            $result['iTotalRecords'] = sizeof($data);
        }
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->omset = number_format($key->omset);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function reseller_leader_board(){
        $query = $this->input->get('search')["value"];
        $start = 0;
        $length = 20;
        $result['iTotalRecords'] = 20;
        $result['iTotalDisplayRecords'] = 20;
        $data =  $this->rekap_bulanan->po_produk->reseller_leader_board_data($start,$length,$query);
        if(sizeof($data)<20){
            $result['iTotalDisplayRecords'] = sizeof($data);
            $result['iTotalRecords'] = sizeof($data);
        }
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->omset = number_format($key->omset);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}