<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class ProdukRecipeController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('produk_recipe','',true);
        $this->load->model('produk_recipe_detail','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Recipe < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "produk-recipe";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"produk_kode"));
        array_push($column, array("data"=>"produk_nama"));
        array_push($column, array("data"=>"jenis_produk_nama"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['produk-recipe'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/produk-recipe/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->produk_recipe->count_all();
        $result['iTotalDisplayRecords'] = $this->produk_recipe->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk_recipe->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'produk-recipe/delete';
            $key->edit_url = base_url().'produk-recipe/form/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_recipe_id));;
            $key->row_id = $key->produk_recipe_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function form(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/recipe.js");
        $id = $this->uri->segment(3);
        unset($_SESSION["redpos_recipe"]);
        $data['last_no'] = 1;
        $data['produk_recipe_id'] = '';
        $data['produk_recipe_detail']= array();
        $data['produk_recipe'] = null;
        if($id!=''){
            $id = str_replace(array("-","_"), array("+","/"), $id);
            $id = $this->encryption->decrypt($id);
            $_SESSION["redpos_recipe"]["produk_recipe_id"] = $id;
            $produk_recipe_detail = $this->produk_recipe_detail->recipe_by_produk($id);
            $data['produk_recipe_detail'] = $produk_recipe_detail;
            $data['last_no'] = sizeof($produk_recipe_detail) > 0 ? ($produk_recipe_detail[sizeof($produk_recipe_detail)-1]->produk_recipe_detail_id + 1):1;
            $data['produk_recipe_detail'] = $produk_recipe_detail ==null ? array() : $produk_recipe_detail;
            $data['produk_recipe_id'] = $id;
            $data['produk_recipe'] = $this->produk_recipe->row_by_id($id);
            $data['produk_recipe']->total_pokok_lbl = $this->idr_currency($data['produk_recipe']->total_pokok);
        }
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Recipe < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "produk-recipe";
        $data['page'] = $this->uri->segment(1);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/produk-recipe/form');
        $this->load->view('admin/static/footer');
    }
    function list_produk(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->produk_recipe->produk_all();
        $result['iTotalDisplayRecords'] = $this->produk_recipe->produk_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk_recipe->produk_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->produk_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function list_bahan(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->produk_recipe_detail->bahan_all();
        $result['iTotalDisplayRecords'] = $this->produk_recipe_detail->bahan_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk_recipe_detail->bahan_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->bahan_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function save(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $produk_recipe_id = $this->input->post('produk_recipe_id');
        $produk_id = $this->input->post('produk_id');
        $recipe = $this->input->post('recipe');
        $total_pokok = $this->input->post('total_pokok');
        $this->produk_recipe->start_trans();
        if($produk_recipe_id==''){
            $insert = array(
                "produk_id" => $produk_id,
                "total_pokok" => $total_pokok,
            );
            $this->produk_recipe->insert($insert);
            $produk_recipe_id = $this->produk_recipe->last_id();
        } else{
            $update = array(
                "produk_id" => $produk_id,
                "total_pokok" => $total_pokok,
            );
            $this->produk_recipe->update_by_id('produk_recipe_id',$produk_recipe_id,$update);
        }
        $this->produk_recipe_detail->delete_by_id('produk_recipe_id',$produk_recipe_id,'Save Produk Recipe');
        $recipe_detail = array();
        foreach ($recipe as $item){
            $data = array(
                "produk_recipe_id"=>$produk_recipe_id,
                "bahan_id"=>$item['bahan_id'],
                "satuan_id"=>$item['satuan_id'],
                "takaran"=>$item['takaran'],
                "konversi_takaran"=>$item['konversi_takaran']*$item['takaran'],
                "harga_pokok"=>$item['harga_pokok'],
                "staff_created"=>$_SESSION['redpos_login']['staff_id'],
                "created_at"=>date('Y-m-d H:i:s'),
            );
            array_push($recipe_detail,$data);

        }
        $this->produk_recipe_detail->insert_batch($recipe_detail);
        $process = $this->produk_recipe->result_trans();
        if($process){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $this->produk_recipe->start_trans();
            $this->produk_recipe->delete_by_id("produk_recipe_id",$id);
            $this->produk_recipe_detail->delete_by_id("produk_recipe_id",$id);
            $delete = $this->produk_recipe->result_trans();
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

}

/* End of file SuplierController.php */
/* Location: ./application/controllers/SuplierController.php */