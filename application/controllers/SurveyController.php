<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class SurveyController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kuisioner','',true);

    }

    public function index()
    {

    }
    public function simpan_survey(){
        $reseller = $this->getReseller();
        $inputSurveyResponse = array();
        $pindah = $this->input->post('question')[11];
        if($pindah==0){
            $pindah = "ya";
        }else{
            $pindah = "tidak";
        }
        $saran = $this->input->post('saran');
        $inputSurveyResponse['reseller_id'] = $reseller->reseller_id;
        $inputSurveyResponse['tanggal'] = date("Y-m-d");
        $inputSurveyResponse['pindah'] = $pindah;
        $inputSurveyResponse['saran'] = $saran;
        $inputSurveyResponse['created_at'] = date("Y-m-d H:i:s");
        $alasan = "";
        if($this->input->post("alasan")!=null){
            $alasan = $this->input->post("alasan");
        }
        $inputSurveyResponse['alasan_pindah'] = $alasan;
        if($this->input->post("leader")!=null&&$this->input->post("leader")!="Pilih Leader"){
            $inputSurveyResponse['leader_id'] = $this->input->post("leader");
        }
        $this->load->model('survey_response','',true);
        $this->survey_response->insert($inputSurveyResponse);
        $survey_response_id = $this->survey_response->last_id();
        $question = $this->input->post('question');
        $this->load->model('survey_response_item','',true);
       for ($i = 1;$i<=10;$i++){
           $inputItem = array();
           $inputItem['survey_response_id'] =$survey_response_id;
           $inputItem['kusioner_id'] =$i;
           $inputItem['nilai'] =$question[$i];
           $this->survey_response_item->insert($inputItem);
       }
       echo json_encode(array("success"=>true,"message"=>"berhasil menyimpan data"));

    }
}
