<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TableController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('table','',true);

    }

    public function index()
    {

        array_push($this->js,'script/admin/table.js');
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;

        $data["meta_title"] = "Table < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "pos";
        $data['page'] = $this->uri->segment(1);

        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['pos']['table'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);

        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/table');
        $this->load->view('admin/static/footer');
    }

    function list(){
        $result['status'] = true;
        $list  =  $this->table->list();
        foreach ($list as $item){
            $item->delete_url = base_url().'table/delete/';
            $item->row_id = $item->table_id;
        }
        $result['list'] = $list;
        echo json_encode($result);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $table_name = $this->input->post('table_name');
        $data = array("table_name"=>$table_name);
        $insert = $this->table->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Kode sudah terdaftar";

        $table_name = $this->input->post('table_name');
        $data = array("table_name"=>$table_name);
        $table_id = $this->input->post('table_id');
        $updated_at = date('Y-m-d H:i:s');
        $data['updated_at'] = $updated_at;
        $update = $this->table->update_by_id('table_id',$table_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->table->delete_by_id("table_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
