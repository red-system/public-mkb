<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManageReturController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');
        $this->load->model('money_back','',true);
        $this->load->model('reseller','',true);
        $this->load->model('retur_agen','',true);
        $this->load->model('retur_agen_detail','',true);
        $this->load->model('produk','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('voucher_retur','',true);
        $this->load->model('stock_produk','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/manage-retur.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Manage Money Back Guarantee < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-money-back";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"no_retur"));
        array_push($column, array("data"=>"status"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['manage-money-back'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }

        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-retur/index');
        $this->load->view('admin/static/footer');
    }
    function approve(){
        $retur_agen_id = $this->input->post("id");
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data = array();
        $data["status"] = "approve";
        $retur_detail = $this->retur_agen_detail->data_by_id($retur_agen_id);
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        $retur_agen = $this->retur_agen->row_by_id($retur_agen_id);
        $reseller_id = $retur_agen->reseller_id;
        $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
        $lokasi_id = $login->lokasi_id;
        $edit = $this->retur_agen->update_by_id("retur_agen_id",$retur_agen_id,$data);

        if($edit){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
            foreach ($retur_detail as $item){
                $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
                $this->db->where("produk_id",$item->produk_id);
                $this->db->order_by("stock_produk_id");
                $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
                $jumlah = $stock_gudang->stock_produk_qty + $item->jumlah;

                $stock_update = array();
                $stock_update["stock_produk_qty"] = $jumlah;
                $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
                $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

                $this->db->where("stock_produk_lokasi_id",$lokasi_id);
                $this->db->where("produk_id",$item->produk_id);
                $this->db->order_by("stock_produk_id");
                $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
                $jumlah = $stock_produk->stock_produk_qty - $item->jumlah;

                $stock_update = array();
                $stock_update["stock_produk_qty"] = $jumlah;
                $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
                $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
            }
            $voucher = array();

            $voucher["reseller_pemilik"] = $reseller_id;
            $voucher_code = time().$reseller_id.random_string('numeric', 5);
            $voucher["voucher_code"] = $voucher_code;
            $voucher["retur_agen_id"] = $retur_agen_id;
            $this->voucher_retur->insert($voucher);

        }
        echo json_encode($result);
    }
    function refuse(){
        $retur_agen_id = $this->input->post("id");
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data = array();
        $data["status"] = "refuse";
        $edit = $this->retur_agen->update_by_id("retur_agen_id",$retur_agen_id,$data);
        if($edit){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->retur_agen->retur_request_all();
        $result['iTotalDisplayRecords'] = $this->retur_agen->retur_request_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->retur_agen->retur_request_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'retur/delete/';
            $key->edit_url = base_url().'retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->retur_agen_id));
            $key->row_id = $key->retur_agen_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function approve_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Manage Money Back Guarantee < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-money-back";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"no_retur"));
        array_push($column, array("data"=>"status"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-retur/index_approve');
        $this->load->view('admin/static/footer');
    }
    function refuse_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Manage Money Back Guarantee < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-money-back";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"no_retur"));
        array_push($column, array("data"=>"status"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-retur/index_refuse');
        $this->load->view('admin/static/footer');
    }
    function approve_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->retur_agen->retur_approve_all();
        $result['iTotalDisplayRecords'] = $this->retur_agen->retur_approve_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->retur_agen->retur_approve_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'retur/delete/';
            $key->edit_url = base_url().'retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->retur_agen_id));
            $key->row_id = $key->retur_agen_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function refuse_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->retur_agen->retur_refuse_all();
        $result['iTotalDisplayRecords'] = $this->retur_agen->retur_refuse_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->retur_agen->retur_refuse_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'retur/delete/';
            $key->edit_url = base_url().'retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->retur_agen_id));
            $key->row_id = $key->retur_agen_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

