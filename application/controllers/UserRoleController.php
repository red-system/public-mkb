<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserRoleController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_role','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "User Role < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"user_role_name"));
		array_push($column, array("data"=>"keterangan"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$data["action"] = json_encode(array("menu"=>true,"edit"=>true,"delete"=>true));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/user_role');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->user_role->user_role_count_all();
		$result['iTotalDisplayRecords'] = $this->user_role->user_role_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->user_role->user_role_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->menu_url = base_url().'user-role/access/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->user_role_id));
			$key->delete_url = base_url().'user-role/delete';
			$key->row_id = $key->user_role_id;
		}
		$result['aaData'] = $data;	
		echo json_encode($result);		
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
		$data['user_role_name'] = $this->input->post('user_role_name');
		$data['keterangan'] = $this->input->post('keterangan');
		$menu = $this->user_role->all_menu();
		$akses_role = array();
								foreach ($menu as $key) {
									$akses_role["".$key->menu_kode]["akses_menu"] = false;
									if($key->action != null){
											$action = explode("|", $key->action);
											foreach ($action as $key2) {
												$akses_role["".$key->menu_kode]["".$key2] = false;
											}				
									}
									if($key->sub_menu_kode != null){
										$akses_role["".$key->menu_kode]["".$key->sub_menu_kode]["akses_menu"] = false;
										
										if($key->data != null){
											$action = explode("|", $key->data);
											foreach ($action as $key2) {
												$akses_role["".$key->menu_kode]["".$key->sub_menu_kode]["data"]["".$key2] = false;
											}
										}
										if($key->sub_action != null){
											$action = explode("|", $key->sub_action);
											foreach ($action as $key2) {
												$akses_role["".$key->menu_kode]["".$key->sub_menu_kode]["".$key2] = false;
											}
										}
									}
								}
		$data['user_role_akses'] = json_encode($akses_role);		
		$insert = $this->user_role->insert($data);
		if($insert){
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["user_role_name"] = $this->input->post('user_role_name');
		$data["keterangan"] = $this->input->post('keterangan');
		$user_role_id = $this->input->post('user_role_id');
		$update = $this->user_role->update_by_id('user_role_id',$user_role_id,$data);
		if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->user_role->delete_by_id("user_role_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}