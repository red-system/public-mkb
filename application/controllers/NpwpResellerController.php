<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/phpseclib/Net/SFTP.php';

class NpwpResellerController extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('npwp', '', true);
        $this->load->model('reseller', '', true);
        $this->load->library('main');
        $this->load->helper('string');

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/npwp_reseller.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "NPWP Reseller < Reseller < " . $_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data" => "no"));
        array_push($column, array("data" => "nama"));
        array_push($column, array("data" => "phone"));
        array_push($column, array("data" => "alamat"));
        array_push($column, array("data" => "no_npwp"));
        array_push($column, array("data" => "lain"));
        array_push($column, array("data" => "keterangan_lain"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
        $akses_menu = json_decode($this->menu_akses, true);
        $action = array();
        foreach ($akses_menu['reseller']['npwp-reseller'] as $key => $value) {
            if ($key != "list" && $key != "akses_menu" && $key != "edit" && $key != "delete") {
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header', $data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/npwp-reseller/index');
        $this->load->view('admin/static/footer');
    }

    function list()
    {
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->npwp->request_count();
        $result['iTotalDisplayRecords'] = $this->npwp->request_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->npwp->request_list($start, $length, $query);
        $i = $start + 1;
        foreach ($data as $key) {
            if ($key->created_at != null) {
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s', $time);
            }
            if ($key->updated_at != null) {
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s', $time);
            }

            $key->no = $i;
            $key->link_foto_profile = $this->config->item('dev_storage_image') . $key->foto_profile;
            $key->link_foto_ktp = $this->config->item('dev_storage_image') . $key->foto_ktp;
            $key->link_selfi_ktp = $this->config->item('dev_storage_image') . $key->selfi_ktp;
            $key->punya_npwp_lbl = $key->punya_npwp == 1 ? 'Ya' : 'Tidak';
            $key->atasnama_sendiri_lbl = $key->atasnama_sendiri == 1 ? 'Ya' : 'Tidak';
            $key->foto_npwp = $this->config->item('dev_storage_image').$key->foto_npwp;
            $key->foto_selfi = $this->config->item('dev_storage_image').$key->foto_selfi;
            $key->penghasilan_lain_lbl = $key->penghasilan_lain == 1 ? 'Ya' : 'Tidak';
            $key->punya_tanggungan_lbl = $key->punya_tanggungan == 1 ? 'Ya' : 'Tidak';
            $key->ptkp = number_format($key->ptkp);
            $key->ptkp_lbl = $key->ptkp;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function aktif()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/select2/dist/css/select2.min.css");
        array_push($this->js, "vendors/general/select2/dist/js/select2.min.js");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");
        $jumlah = $this->reseller->prospective_maps();
        $data['empty_maps'] = $jumlah;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Aktif Agen < Reseller < " . $_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data" => "no"));
        array_push($column, array("data" => "nama"));
        array_push($column, array("data" => "username"));
        array_push($column, array("data" => "phone"));
        array_push($column, array("data" => "email"));
        array_push($column, array("data" => "no_ktp"));
        array_push($column, array("data" => "alamat"));
        array_push($column, array("data" => "outlet_subdistrict_name"));
        array_push($column, array("data" => "outlet_alamat"));
        array_push($column, array("data" => "referal_nama"));
        array_push($column, array("data" => "status_maps", "template" => "badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
        $akses_menu = json_decode($this->menu_akses, true);
        $action = array();
        foreach ($akses_menu['reseller']['prospective-reseller'] as $key => $value) {
            if ($key == "banned") {
                $action[$key] = $value;
            }

        }
        $action['complate'] = true;
        $data['action'] = json_encode($action);

        $data['province'] = $this->province->getProvinceAll();

        $this->load->view('admin/static/header', $data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/prospective-reseller/active_index');
        $this->load->view('admin/static/footer');
    }

    function aktif_list()
    {

        if (isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != "") {
            $_GET['province_id'] = $_GET["columns"][1]["search"]["value"];
        }

        if (isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != "") {
            $_GET['city_id'] = $_GET["columns"][2]["search"]["value"];
        }

        if (isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != "") {
            $_GET['subdistrict_id'] = $_GET["columns"][3]["search"]["value"];
        }

        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "active";
        $result['iTotalRecords'] = $this->reseller->agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->agen_status_filter($query, $status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->reseller->agen_status_list($start, $length, $query, $status);
        $i = $start + 1;
        foreach ($data as $key) {
            if ($key->created_at != null) {
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s', $time);
            }
            if ($key->updated_at != null) {
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s', $time);
            }
            if ($key->longitude == null && $key->latitude == null) {
                $key->status_maps = 'empty';
            } else {
                $key->status_maps = 'filled';
            }

            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function approved()
    {
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $npwp_id = $this->input->post('npwp_id');
        $case = $this->input->post('case');
        $ptkp = $this->input->post('ptkp');
        $data['case'] = $case;
        $data['approved'] = 1;
        $data['ptkp'] = $ptkp;
        $edit = $this->npwp->update_by_id('npwp_id', $npwp_id, $data);
        if ($edit) {
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }

    function refuse()
    {
        $result['success'] = false;
        $result['message'] = "missing parameter";
        $npwp_id = $this->input->post('npwp_id');
        $alasan = $this->input->post('alasan_banned');;
        $reseller = $this->npwp->detailData($npwp_id);
        $delete = $this->npwp->delete_by_id('npwp_id', $npwp_id);
        if ($delete) {
            $result['success'] = true;
            $result['message'] = "Berhasil merubah status reseller";
            $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #bebebe">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 250px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller->nama . ',</h4>
                        <h3 style="text-align: left;font-weight: 600">Mohon Maaf,</h3>
                        <span>Pendaftaran NPWP anda kami tolak karena ' . $alasan . '
                        </span>
                        <br>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                        <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
            $this->main->mailer_auth('NPWP Ditolak', $reseller->email, $reseller->nama, $mailContentAdmin);

        }
        echo json_encode($result);
    }

    function approved_page()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/npwp_reseller.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "NPWP Reseller < Reseller < " . $_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data" => "no"));
        array_push($column, array("data" => "nama"));
        array_push($column, array("data" => "phone"));
        array_push($column, array("data" => "alamat"));
        array_push($column, array("data" => "no_npwp"));
        array_push($column, array("data" => "lain"));
        array_push($column, array("data" => "keterangan_lain"));
        array_push($column, array("data" => "case"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
        $akses_menu = json_decode($this->menu_akses, true);
        $action = array();
        foreach ($akses_menu['reseller']['npwp-reseller'] as $key => $value) {
            if ($key != "list" && $key != "akses_menu" && $key != "accept" && $key != "refuse") {
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header', $data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/npwp-reseller/approve');
        $this->load->view('admin/static/footer');
    }

    function approved_list()
    {
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->npwp->approve_count();
        $result['iTotalDisplayRecords'] = $this->npwp->approve_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->npwp->approve_list($start, $length, $query);
        $i = $start + 1;
        foreach ($data as $key) {
            if ($key->created_at != null) {
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s', $time);
            }
            if ($key->updated_at != null) {
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s', $time);
            }

            $key->no = $i;
            $key->link_foto_profile = $this->config->item('dev_storage_image') . $key->foto_profile;
            $key->link_foto_ktp = $this->config->item('dev_storage_image') . $key->foto_ktp;
            $key->link_selfi_ktp = $this->config->item('dev_storage_image') . $key->selfi_ktp;
            $i++;
            $key->row_id = $key->npwp_id;
            $key->delete_url = base_url().'npwp-reseller-approve/delete';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function approved_add()
    {
        $this->form_validation->set_rules('no_npwp', '', 'required');
        $this->form_validation->set_rules('nama', '', 'required');
        $this->form_validation->set_rules('input_foto_npwp', '', 'required');
        $this->form_validation->set_rules('case', '', 'required');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if ($this->form_validation->run() == TRUE) {
            $result['success'] = true;
            $result['message'] = "Success updated npwp info";

            $no_npwp = $this->input->post('no_npwp');
            $case = $this->input->post('case');
            $reseller_id = $this->input->post('reseller_id');
            $foto_npwp = $this->input->post('input_foto_npwp');
            $penghasilan_lain = $this->input->post('input_penghasilan_lain');
            $keterangan_lain = $this->input->post('keterangan_lain');

            $data = array(
                "reseller_id" => $reseller_id,
                "no_npwp" => $no_npwp,
                "foto_npwp" => $foto_npwp,
                "penghasilan_lain" => $penghasilan_lain,
                "keterangan_lain" => $keterangan_lain,
                "case" => $case,
                "approved" => 1
            );

            $this->npwp->insert($data);
        } else {
            $result['message'] = "Failed to update";
        }


        echo json_encode($result);
    }

    function approved_edit()
    {
        $this->form_validation->set_rules('no_npwp', '', 'required');
        $this->form_validation->set_rules('nama', '', 'required');
        $this->form_validation->set_rules('case', '', 'required');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if ($this->form_validation->run() == TRUE) {
            $result['success'] = true;
            $result['message'] = "Success updated npwp info";

            $npwp_id = $this->input->post('npwp_id');
            $no_npwp = $this->input->post('no_npwp');
            $case = $this->input->post('case');
            $reseller_id = $this->input->post('reseller_id');
            $data_foto = $this->input->post('data_foto');
            $foto_npwp = $this->input->post('input_foto_npwp');
            $penghasilan_lain = $this->input->post('input_penghasilan_lain');
            $keterangan_lain = $this->input->post('keterangan_lain');

            $data = array(
                "reseller_id" => $reseller_id,
                "no_npwp" => $no_npwp,
                "foto_npwp" => $foto_npwp,
                "penghasilan_lain" => $penghasilan_lain,
                "keterangan_lain" => $keterangan_lain,
                "case" => $case,
                "approved" => 1
            );

            $this->npwp->update_by_id("npwp_id", $npwp_id, $data);

            if ($data_foto != $foto_npwp){
                $this->deleteImage($data_foto);
            }
        } else {
            $result['message'] = "Failed to update";
        }


        echo json_encode($result);
    }

    function approved_delete()
    {
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $npwp = $this->npwp->pilih_by_id($id);
            $delete = $this->npwp->delete_by_id("npwp_id",$id);
            if($delete){
                if ($npwp->foto_npwp){
                    $this->deleteImage($npwp->foto_npwp);
                }
                if ($npwp->foto_selfi){
                    $this->deleteImage($npwp->foto_selfi);
                }
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

    function deleteImage($filename)
    {
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        if ($sftp->file_exists("../../../var/www/html/development/dev-storage.redsystem.id/redpos/" . $filename)) {
            $output = $sftp->delete("../../../var/www/html/development/dev-storage.redsystem.id/redpos/" . $filename, false);
        }


    }

    function list_reseller()
    {
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reseller->res_active_count_all();
        $result['iTotalDisplayRecords'] = $this->reseller->res_active_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->reseller->res_active_list($start, $length, $query);
        $i = $start + 1;
        foreach ($data as $key) {
            if ($key->created_at != null) {
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s', $time);
            }
            if ($key->updated_at != null) {
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s', $time);
            }
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->type = $key->type == "agent" ? 'Reseller' : "Super Reseller";
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function save_home(){
        $post_data = $this->input->post();
        $data = array();
        foreach ($post_data as $key=>$val){
            if($key=='input_foto_npwp'){
                $data['foto_npwp'] = $val;
            } else if($key=='input_foto_selfi'){
                $data['foto_selfi'] = $val;
            } else if($key=='jumlah_tanggungan'){
                $data[$key] = $this->string_to_number($val);
            } else {
                $data[$key] = $val;
            }
        }
        $data['ptkp'] = 0;
        $jenis_kelamin = $this->input->post('kelamin');
        $case = 2;
        if($data['punya_npwp']==0){
            $case = 2;
            $data['case'] = 2;
        }else{
            $ptkp = 0;
            $punya_npwp = $this->input->post('punya_npwp');
            $atasnama_sendiri = $this->input->post('atasnama_sendiri');
            $case = 1;
            if($atasnama_sendiri==0){
                $case = 1;

                if($jenis_kelamin=='laki-laki'){
                    $case = 2;
                }
            }else{
                $penghasilan_lain = $this->input->post('penghasilan_lain');
                if($penghasilan_lain==1){
                    $case = 1;
                }else{
                    $case = 3;
                    $pengkali_menikah = 0;
                    $jumlah_tanggungan = 0;
                    $input_tanggungan = $this->string_to_number($this->input->post('jumlah_tanggungan'));
                    if($input_tanggungan==""){
                        $jumlah_tanggungan = 0;
                    }else if($input_tanggungan>3){
                        $jumlah_tanggungan = 3;
                    } else {
                        $jumlah_tanggungan = $input_tanggungan;
                    }
                    $status_pernikahan = $this->input->post('status_pernikahan');
                    if($status_pernikahan=='menikah'&&$jenis_kelamin=='laki-laki'){
                        $pengkali_menikah = 1;
                    }
                    if($jenis_kelamin=='perempuan'&&$status_pernikahan=='menikah'){
                        $jumlah_tanggungan = 0;
                    }
                    $menikah = ($pengkali_menikah*4500000);
                    $tanggungan = ($jumlah_tanggungan*4500000);
                    $ptkp = ((54000000)+$menikah+$tanggungan)/12;
                }
                $data['ptkp'] = $ptkp;
            }
        }
        $data['case'] = $case;
        if ($data['case'] ==2){
            $data['approved'] = 1;
        }
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $data['reseller_id'] = $reseller_id;
        $this->npwp->insert($data);
        $result['success'] = true;
        $result['message'] = 'Berhasil menyimpan data';
        echo json_encode($result);
    }


}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */