<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class VoucherReturController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_produk','',true);
        $this->load->model('voucher_retur','',true);
        $this->load->model('retur_agen_detail','',true);
        $this->load->model('tipe_pembayaran','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/voucher.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Voucher Retur < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "voucher-retur";
        $data['page'] = "voucher-retur";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"voucher_code"));
        array_push($column, array("data"=>"status_penukaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"tanggal_penukaran"));
        array_push($column, array("data"=>"ditukarkan"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['voucher-retur'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/voucher-retur/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->voucher_retur->voucher_all();
        $result['iTotalDisplayRecords'] = $this->voucher_retur->voucher_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->voucher_retur->voucher_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_penukaran != null){
                $time = strtotime($key->tanggal_penukaran);
                $key->tanggal_penukaran = date('d-m-Y',$time);
            }
            $key->print_url = base_url()."voucher/print/".$key->voucher_retur_id;
            $key->row_id = $key->voucher_retur_id;
            $key->no = $i;
            $key->action = null;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function detail(){
        $id = $this->input->post('retur_agen_id');
        $temp = $this->voucher_retur->retur_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->retur_agen_detail->data_by_id($id);
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {

            $key->jumlah = number_format($key->jumlah);

        }
        echo json_encode($temp);
    }
    function print(){
        $voucher_produk_id = $this->uri->segment(3);
        $id = $voucher_produk_id;
        $temp = $this->voucher_produk->po_produk_by_id($id);

        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->po_produk->po_produk_detail_by_id($id);
        $tipe_pembayaran = $this->tipe_pembayaran->row_by_id($temp->tipe_pembayaran);
        $temp->metode_pembayaran = $tipe_pembayaran->tipe_pembayaran_nama." ".$tipe_pembayaran->no_akun;
        $temp->bukti_url = $this->config->item('dev_storage_image').$temp->bukti_pembayaran;
        if($temp->tanggal_pemesanan != null){
            $x = strtotime($temp->tanggal_pemesanan);
            $temp->tanggal_pemesanan = date("Y-m-d",$x);
        }
        if($temp->tanggal_penerimaan != null){
            $x = strtotime($temp->tanggal_penerimaan);
            $temp->tanggal_penerimaan = date("Y-m-d",$x);
        }
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga);
            $key->jumlah = number_format($key->jumlah);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        $data["detail"] = $temp;
        $this->load->view("admin/voucher/print",$data);
    }
}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */