<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class DepositSuperResellerController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('deposit_super_reseller');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Deposit Super Reseller < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "master_data";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"last_deposit"));
        array_push($column, array("data"=>"deadline_deposit"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['reseller']['deposit-super-reseller'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/deposit-super-reseller/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->deposit_super_reseller->count_all();
        $result['iTotalDisplayRecords'] = $this->deposit_super_reseller->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->deposit_super_reseller->_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $key->row_id = $key->reseller_id;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}


/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */