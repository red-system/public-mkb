<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class POSAgenController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('pos','',true);
        $this->load->model('bonus','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('reseller','',true);
        $this->load->model('harga_produk','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('log_kasir','',true);
        $this->load->model('unit','',true);
        $this->load->model('hutang_produk','',true);
        $this->load->model('piutang_produk','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('po_produk_detail','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('master_login','',true);
        $this->load->library('main');
        $this->load->model('assembly','',true);
        if($_SESSION['redpos_login']['pay']==1){
            $params = array('server_key' => $_SESSION['redpos_login']['pay-s'], 'production' => $_SESSION['redpos_login']['pay-prod']);
            $this->load->library('midtrans');
            $this->midtrans->config($params);
            $this->load->helper('url');
        }
    }
    public function index(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/pos_.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Penjualan < POS < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "pos";
        $data['page'] = 'penjualan';
        $data['lokasi'] = $this->lokasi->all_list();
        $data['pay'] = $_SESSION['redpos_login']['pay'] == 0 ? false : true;
        unset($_SESSION['pos']);
        if($data['lokasi']!=null){
            $_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
            if(isset($_SESSION["redpos_login"]['location_id'])){
                $_SESSION['pos']['location_id'] = $_SESSION["redpos_login"]['location_id'];
            }
            $data['faktur'] = $this->pos->getFakturCode();
            $data['urutan'] = $this->pos->getUrutan();
            $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            if (isset($_SESSION['log_pos']['log_kasir_id'])) {
                $this->load->view('admin/penjualan_agen');
            } else {
                $this->load->view('admin/form_pos');
            }
            $this->load->view('admin/static/footer');
        }else{
            $_SESSION['error'] = 'Anda tidak bisa menjalankan pos tanpa data lokasi/outlet';
            redirect(base_url());
        }
    }
    function guest_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reseller->active_agen_all();
        $result['iTotalDisplayRecords'] = $this->reseller->active_agen_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->active_agen_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->aksi = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function save_preaty_cash(){
        $result['success'] = false;
        $result['message'] = 'Gagal menyimpan data';
        $data['user_id'] = $_SESSION["redpos_login"]['user_id'];
        $data['staff_id'] = $_SESSION["redpos_login"]['staff_id'];
        $data['waktu_buka'] = Date('Y-m-d H:i:s');
        $data['kas_awal'] = $this->string_to_number($this->input->post('kas_awal'));
        $data['created_at'] = Date('Y-m-d H:i:s');
        $insert = $this->log_kasir->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = 'Berhasil menyimpan data';
            $_SESSION['log_pos']['log_kasir_id'] = $this->log_kasir->last_id();
        }
        echo json_encode($result);
    }
    function info_closing(){
        $log_pos_id = $_SESSION['log_pos']['log_kasir_id'];
        $info = $this->pos->log_kasir_info($log_pos_id);
        $info->jumlah_transaksi = $this->pos->jumlah_transaksi_by_log($log_pos_id);
        $info->total_transaksi = $this->idr_currency($this->pos->total_transaksi_by_log($log_pos_id)->total);
        $info->kas_akhir = $this->idr_currency((int)$this->pos->transaksi_by_log($log_pos_id)->total+(int)$info->kas_awal);
        $info->kas_awal = $this->idr_currency($info->kas_awal);
        echo json_encode($info);
    }
    function closing(){
        $result['success'] = true;
        $result['message'] = 'Gagal Merubah data';
        $log_pos_id = $_SESSION['log_pos']['log_kasir_id'];
        $result['log_kasir_id'] = $log_pos_id;
        $log_pos = $this->log_kasir->row_by_id($log_pos_id);
        $total_penjualan = $this->pos->transaksi_by_log($log_pos_id)->total;
        $total = (int)$total_penjualan + (int)$log_pos->kas_awal;
        $data['kas_akhir'] = $total;
        $data['waktu_tutup'] = Date('Y-m-d H:i:s');
        $data['updated_at'] = Date('Y-m-d H:i:s');
        $update = $this->log_kasir->update_by_id('log_kasir_id',$log_pos_id,$data);
        if($update){
            unset($_SESSION['log_pos']);
            $result['total_kas'] = number_format($total);
            $result['success'] = true;
            $result['message'] = 'Berhasil Merubah data';
        }
        echo json_encode($result);
    }
    function utility(){
        $key = $this->uri->segment(3);
        if($key=="change-location"){
            $_SESSION['pos']['location_id'] = $this->input->post('lokasi_id');
        }
    }
    function produk_list(){
        $lokasi_id = $_SESSION['pos']['location_id'];
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $lokasi_id = $_SESSION["redpos_login"]['lokasi_id'];
        }
        if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
            $_GET['stock_produk_seri'] = $_GET["columns"][3]["search"]["value"];
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->stock_produk->pos_produk_count($lokasi_id);
        $result['iTotalDisplayRecords'] = $this->stock_produk->pos_produk_filter($query,$lokasi_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $count = $result['iTotalDisplayRecords'];
        $_GET["is_pos"] = 1;
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->stock_produk->pos_produk_list($start,$length,$query,$lokasi_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->aksi = null;
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->ar_minimal_pembelian = "";
            $key->ar_harga ="";
            $harga = $this->harga_produk->harga_produk_satuan($key->produk_id,$key->produk_satuan_id);
            foreach ($harga as $each) {
                $key->ar_minimal_pembelian .= $each->minimal_pembelian."|";
                $key->ar_harga .= $each->harga."|";
            }
            $key->ar_minimal_pembelian .= "0";
            $key->ar_harga .= $key->harga_eceran;
            $key->no = $i;
            $key->delete_url = base_url().'stock-bahan/delete/'.$key->produk_id;
            $key->row_id = $key->produk_id;
            $key->stock_produk_qty =  $this->number_to_string($key->stock_produk_qty);
            $key->harga_eceran = number_format($key->harga_eceran);
            $key->harga_grosir = number_format($key->harga_grosir);
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function save(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $transaksi = json_decode($this->input->post('transaksi'));
        $save = $this->pos->save();
        if($save['result']){
            $transaksi = json_decode($this->input->post('transaksi'));
            $grand_total = $transaksi->grand_total;
            $terbayar = $transaksi->terbayar;
            $kembalian = $terbayar - $grand_total;
            $result['success'] = true;
            $result['message'] = "Transaksi tersimpan";
            $result['kembalian'] = number_format($kembalian);
            $id = $save["id"];
            $result['id'] = $id;
            $result["url"] = base_url()."pos/print/".$id;

        }
        $tipe_pembayaran = $this->input->post('tipe_pembayaran_id');
        if($tipe_pembayaran==-13&&$save['result']){
            $this->token($result,$save["id"]);
        } else {
            echo json_encode($result);
        }

    }
    public function token($result,$id)
    {
        $transaksi = json_decode($this->input->post('transaksi'));
        $transaction_details = array(
            'order_id' => $_SESSION['redpos_login']['company_id']."-".$id,
            'gross_amount' => $this->string_to_number($transaksi->grand_total), // no decimal allowed for creditcard
        );
        $item_details = array ();
        foreach ($transaksi->item as $key) {

            $detail = array(
                'id' => $key->produk_id,
                'price' => $this->string_to_number($key->harga_eceran),
                'quantity' => $this->string_to_number($key->jumlah_qty),
                'name' => $key->produk_nama
            );
            array_push($item_details,$detail);
        }
        // Optional
        $billing_address = array(
            'first_name'    => $this->input->post('guest_nama'),
            'last_name'     => "",
            'address'       => $this->input->post('guest_alamat'),
            'city'          => "",
            'postal_code'   => "",
            'phone'         => $this->input->post('guest_telepon'),
            'country_code'  => ''
        );


        // Optional
        $customer_details = array(
            'first_name'    => $this->input->post('guest_nama'),
            'last_name'     => "",

            'phone'         => $this->input->post('guest_telepon'),
            'billing_address'  => $billing_address,
            //'shipping_address' => $shipping_address
        );
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'minute',
            'duration'  => 2
        );

        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $item_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
        error_log(json_encode($transaction_data));
        $snapToken = $this->midtrans->getSnapToken($transaction_data);
        error_log($snapToken);
        $result['token'] = $snapToken;
        echo json_encode($result);
    }
    function print(){
        $id = $this->uri->segment(3);
        $trans = $this->pos->penjualan_by_id($id);
        $penjualan_detail = $this->pos->detailTransaksi($id);
        foreach ($penjualan_detail as $key){
            if($key->produk_nama == null){
                $key->produk_nama = $key->deskripsi;
            }
            if($key->produk_kode == null){
                $key->produk_kode = "custom";
            }
        }
        $trans->detail_transaksi = $penjualan_detail;
        $data["trans"] = $trans;

        $this->load->view("admin/print_struct",$data);
    }
    function print_closing(){
        $log_pos_id = $this->uri->segment(3);
        $info = $this->pos->log_kasir_info($log_pos_id);
        $info->jumlah_transaksi = $this->pos->jumlah_transaksi_by_log($log_pos_id);
        $info->total_transaksi = $this->idr_currency($this->pos->total_transaksi_by_log($log_pos_id)->total);
        $info->kas_awal = $this->idr_currency($info->kas_awal);
        $info->kas_akhir = $this->idr_currency($info->kas_akhir);
        $data["info"] = $info;
        $this->load->view("admin/pos/print_closing",$data);
    }
    function get_unit_satuan(){
        $idproduk = $this->input->post('id');
        $data = $this->assembly->get_unit_satuan($idproduk);
        foreach ($data as $key){
            $key->ar_minimal_pembelian = "";
            $key->ar_harga ="";
            $harga = $this->harga_produk->harga_produk_satuan($idproduk,$key->satuan_id);

            foreach ($harga as $each) {
                $key->ar_minimal_pembelian .= $each->minimal_pembelian."|";
                $key->ar_harga .= $each->harga."|";
            }
            $key->ar_minimal_pembelian .= "0";
            $key->ar_harga .= $key->harga_jual;
        }
        echo json_encode($data);
    }
    function normalisasi(){
        $data = $this->pos->normalisasi();
        foreach ($data as $item){
            $temp = array("hpp"=>$item->hpp_global);
            $this->pos->update_penjualan_produk($item->produk_id,$temp);
        }
    }
    function penukaran(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/pos_.js");
        array_push($this->js, "script/admin/penukaran.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Penjualan < POS < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "pos";
        $data['page'] = 'penukaran-po';
        $data['lokasi'] = $this->lokasi->all_list();
        $data['pay'] = $_SESSION['redpos_login']['pay'] == 0 ? false : true;
        unset($_SESSION['pos']);
        $data['faktur'] = $this->pos->getFakturCode();
        $data['urutan'] = $this->pos->getUrutan();
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/penukaran_po');
        $this->load->view('admin/static/footer');
    }
    function check(){
        $kode_po = $this->input->post("po_kode");
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $this->db->where('voucher_code',$kode_po);
        $this->db->where('status_penukaran',"Tersedia");
        $check = $this->db->get('mykindofbeauty_kemiri.voucher_produk')->row();

        if($check!=null){
            $id = $check->po_produk_id;
            $temp = $this->voucher_produk->po_by_voucher_code($kode_po);
            $no = 0;
            $no_produk = 0;
            $post = array();
            $temp->item = $this->po_produk->po_produk_detail_by_code($kode_po);
            $tipe_pembayaran = $this->tipe_pembayaran->row_by_id($temp->tipe_pembayaran);
            $temp->metode_pembayaran = $tipe_pembayaran->tipe_pembayaran_nama." ".$tipe_pembayaran->no_akun;
            $temp->bukti_url = $this->config->item('dev_storage_image').$temp->bukti_pembayaran;
            if($temp->tanggal_pemesanan != null){
                $x = strtotime($temp->tanggal_pemesanan);
                $temp->tanggal_pemesanan = date("Y-m-d",$x);
            }
            if($temp->tanggal_penerimaan != null){
                $x = strtotime($temp->tanggal_penerimaan);
                $temp->tanggal_penerimaan = date("Y-m-d",$x);
            }
            if($temp->created_at != null){
                $time = strtotime($temp->created_at);
                $temp->created_at = date('d-m-Y H:i:s',$time);
            }
            if($temp->updated_at != null){
                $time = strtotime($temp->updated_at);
                $temp->updated_at = date('d-m-Y H:i:s',$time);
            }
            foreach ($temp->item as $key) {
                $key->harga = number_format($key->harga);
                $key->jumlah_qty = number_format($key->jumlah_qty);
                $key->sub_total = number_format($key->sub_total);
                $this->db->select('sum(stock_produk_qty) as stock');
                $this->db->where('stock_produk_lokasi_id',$_SESSION['redpos_login']['lokasi_id']);
                $this->db->where('produk_id',$key->produk_id);
                $key->stock = $this->db->get('mykindofbeauty_kemiri.stock_produk')->row()->stock;
                $key->indicator = $key->jumlah_qty <= $key->stock ? 0 : 1;
            }
            $temp->total = number_format($temp->total);
            $temp->tambahan = number_format($temp->tambahan);
            $temp->potongan = number_format($temp->potongan);
            $temp->grand_total = number_format($temp->grand_total);
            $result['success'] = true;
            $result['detail'] = $temp;
            $result['message'] = 'Berhasil mendapatkan data';
            echo json_encode($result);
        }else{
            $result['success'] = false;
            $result['message'] = 'Voucher Tidak Tersedia';
            echo json_encode($result);
        }

    }
    function penukaran_save(){
        $paramBonus = "16:00:00";
        $estimasiBonus = $this->main->tanggal_kerja($paramBonus);
        $lokasi_reseller_id = $_SESSION['redpos_login']['lokasi_id'];
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);

        $data_user = $this->master_login->row_by_field('reseller_id', $reseller_id);
        $username = $data_user->username;

        $minimal_po = $this->po_produk->minimal_po_cek($reseller_id);
        $kode_po = $this->input->post("po_kode");
        $this->db->where('voucher_code',$kode_po);
        $this->db->where('status_penukaran',"Tersedia");
        $check = $this->db->get('mykindofbeauty_kemiri.voucher_produk')->row();
        $result['success'] = True;
        $result['message'] = 'Berhasil menukarkan voucher';
//        rubah voucher menjadi terpakai
        $voucher = array();
        $voucher['tanggal_penukaran'] = date("Y-m-d");
        $voucher['status_penukaran'] = "Terpakai";
        $voucher['reseller_penukar'] = $reseller_id;
        $this->db->where("voucher_produk_id",$check->voucher_produk_id);
        $this->db->update("mykindofbeauty_kemiri.voucher_produk",$voucher);
        $this->load->model("po_produk","",true);
//        voucher base po_produk atau po_detail (po produk versi lama)
        $detail_param = $check->po_produk_detail_id == null ? $check->po_produk_id: $check->po_produk_detail_id;
        if($check->po_produk_detail_id==null){
            $po_produk_item = $this->po_produk->po_produk_detail_by_id($check->po_produk_id);
        }else{
            $po_produk_item = $this->po_produk->po_produk_detail_by_detail_id($check->po_produk_detail_id);
        }
        $po_produk_id = $check->po_produk_id;
        $po_data = $this->po_produk->row_by_id($po_produk_id);
//        cek full payment
        $history = $this->produk->history_beli_produk($reseller_id);
        $is_full_payment = $this->po_produk->is_full_payment($history,$reseller_id);
        $deposit = $this->po_produk->total_deposit_reseller($lokasi_reseller_id);
        $deposit = $deposit == "" ? 0 : $deposit;

        $withdraw_penerima = $reseller->loyalti_withdraw;
        $sisa_penerima = $withdraw_penerima - $po_data->grand_total;
        $withdraw_penerima_ganti = $sisa_penerima <= 0 ? 0 : abs($sisa_penerima);
        $resellerUpdate = array();
        $resellerUpdate['loyalti_withdraw'] = $withdraw_penerima_ganti;
        $this->reseller->update_by_id('reseller_id',$reseller_id,$resellerUpdate);
        foreach ($po_produk_item as $key) {

            $data = array();
            $data["stock_produk_qty"] = $key->jumlah_qty;
            $data["produk_id"] = $key->produk_id;
            $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
            $data["year"] = date("y");
            $data["month"] = date("m");
            $data["po_id"] = $po_produk_id;
            $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;

            $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
            $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
            $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
            $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
            $data["hpp"] = $this->string_to_number($key->harga);
            $insert = $this->stock_produk->insert($data);
            $stock_produk_reseller_id = $this->stock_produk->last_id();
            $arus_reseller = array();
            $arus_reseller["tanggal"] = date("Y-m-d");
            $arus_reseller["table_name"] = "stock_produk";
            $arus_reseller["stock_produk_id"] = $stock_produk_reseller_id;
            $arus_reseller["produk_id"] = $key->produk_id;
            $arus_reseller["stock_out"] = 0;
            $arus_reseller["stock_in"] = $key->jumlah_qty;
            $arus_reseller["last_stock"] = $this->stock_produk->last_stock_reseller($key->produk_id, $po_data->lokasi_reseller)->result;
            // $arus_reseller["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
            $arus_reseller["last_stock_total"] = $this->stock_produk->last_stock_reseller($key->produk_id, $po_data->lokasi_reseller)->result;
            $arus_reseller["keterangan"] = "Penukaran Voucher No : ". $kode_po;
            $arus_reseller["method"] = "update";
            $this->stock_produk->arus_stock_produk($arus_reseller);
            $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
            $row_stock = $this->stock_produk->stock_by_location_produk_id($lokasi_id,$key->produk_id);
            $jumlah_bahan = $key->jumlah_qty;

            foreach ($row_stock as $row) {
                $data = array();
                $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                $stock_out = $jumlah_bahan;
                if($row->stock_produk_qty < $jumlah_bahan){
                    $jumlah = 0;
                    $stock_out = $row->stock_produk_qty;
                }
                $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                $data["stock_produk_qty"] = $jumlah;
                $stock_produk_id = $row->stock_produk_id;
                $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
                if ($updateStok) {
                    $arus_super = array();
                    $arus_super["tanggal"] = date("Y-m-d");
                    $arus_super["table_name"] = "stock_produk";
                    $arus_super["stock_produk_id"] = $stock_produk_id;
                    $arus_super["produk_id"] = $key->produk_id;
                    $arus_super["stock_out"] = $stock_out;
                    $arus_super["stock_in"] = 0;
                    $arus_super["last_stock"] = $this->stock_produk->last_stock_reseller($key->produk_id, $lokasi_id)->result;
                    // $arus_super["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                    $arus_super["last_stock_total"] = $this->stock_produk->last_stock_reseller($key->produk_id, $lokasi_id)->result;
                    $arus_super["keterangan"] = "Penukaran Voucher No : ". $kode_po;
                    $arus_super["method"] = "update";
                    $this->stock_produk->arus_stock_produk($arus_super);
                }
                if($jumlah_bahan <= 0){
                    break;
                }

            }
            $hutang_produks = array();
            $hutang_produks['reseller_id'] = $reseller_id;
            $hutang_produks['po_produk_id'] = $po_produk_id;
            $hutang_produks['voucher_produk_id'] = $check->voucher_produk_id;
            $hutang_produks['produk_id'] = $key->produk_id;
            $hutang_produks['jumlah'] = $key->jumlah_qty;
            $this->hutang_produk->insert($hutang_produks);
            $piutang_produks = array();
            $hutang_produks['reseller_id'] = $reseller_id;
            $hutang_produks['po_produk_id'] = $po_produk_id;
            $hutang_produks['voucher_produk_id'] = $check->voucher_produk_id;
            $hutang_produks['produk_id'] = $key->produk_id;
            $hutang_produks['jumlah'] = $key->jumlah_qty;
            $this->piutang_produk->insert($hutang_produks);
        }
        $po_data = $this->po_produk->row_by_id($po_produk_id);
        $login = $this->db->where("lokasi_id",$po_data->lokasi_reseller)->get("mykindofbeauty_master.login")->row();
        $pemberi_id = $login->reseller_id;
        $pemberi = $this->reseller->row_by_id($pemberi_id);
        $bank = $this->db->where("bank_id",$reseller->bank_id)->get('mykindofbeauty_kemiri.bank')->row();

        if($this->bonus->cekBonus($po_produk_id,$reseller_id,"loyalty bonus",$check->po_produk_detail_id)) {
            $status_bonus_reseller_loyal = (!$is_full_payment||($withdraw_penerima>0)) ? "Hold" : "On Process";
            $bonus = array();
            $bonus["tanggal"] = $estimasiBonus;
            $bonus["from_reseller_id"] = $pemberi->reseller_id;
            $bonus["type"] = "loyalty bonus";
            $bonus["jumlah_deposit"] = $po_produk_item[0]->sub_total;
            $bonus["persentase"] = 5;
            $bonus["jumlah_bonus"] = $po_produk_item[0]->sub_total * 0.05;
            $bonus["to_reseller_id"] = $reseller_id;
            $bonus["po_produk_id"] = $po_produk_id;
            if($check->po_produk_detail_id!=null){
                $bonus["po_produk_detail_id"] = $check->po_produk_detail_id;
            }
            $bonus["status"] = $status_bonus_reseller_loyal;
            $kelebihan = $reseller->kelebihan;
            $potongan = 0;
            $text_kelebihan = '';
            if($kelebihan>0){
                if($kelebihan<$bonus["jumlah_bonus"]){
                    $bonus["potongan"] = $kelebihan;
                    $potongan = $kelebihan;
                    $bonus["jumlah_bonus"] = $bonus["jumlah_bonus"] - $kelebihan;
                    $kelebihan = 0;
                }else{
                    $bonus["potongan"] = $bonus["jumlah_bonus"];
                    $potongan = $bonus["jumlah_bonus"];
                    $kelebihan = $kelebihan - $bonus["jumlah_bonus"];
                    $bonus["jumlah_bonus"] = 0;
                }
                $total_bonus = $bonus["jumlah_bonus"];
                $text_kelebihan = '<tr style="height: 25px">
                                                            <td style="text-align: left">Potongan </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($potongan). ' </td>
                                                        </tr>
                                                        <tr style="height: 25px">
                                                            <td style="text-align: left">Total Bonus Didapat </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($total_bonus). ' </td>
                                                        </tr>';
            }
            $this->bonus->insert($bonus);
            $dataKelebihan['kelebihan'] = $kelebihan;
            $this->reseller->update_by_id('reseller_id',$reseller->reseller_id,$dataKelebihan);
            $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
            if (!$is_full_payment){
                $status_message = "Bonus anda akan di hold karena anda belum memenuhi minimal deposit. Silakan lakukan deposit untuk mencairkan bonus.";
            }
            $referal_link  = $this->config->item('url-landing').$reseller->referal_code;
            $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }
        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller->nama . ' ('.$username.'),</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS LOYALTY yang akan langsung di transfer ke rekening MKB Anda.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Pemilik Voucher </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $pemberi->nama . ' </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Produk </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->jumlah_qty) . ' botol</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Deposit </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->sub_total) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">5%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($po_produk_item[0]->sub_total * 0.05) . '</td>
                                    </tr>
                                    '.$text_kelebihan.'
                                </tbody>
                            </table>
                        </div>
                        <span>
                           '.$status_message.'
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
            $this->main->mailer_auth('Bonus Loyalty', $reseller->email, $reseller->nama, $mailContentAdmin);
        }

        echo json_encode($result);
    }
    function history_penukaran(){

        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "History Penukaran < POS < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "pos";
        $data['page'] = 'penukaran-po';
        $data['lokasi'] = $this->lokasi->all_list();
        $data['sumColumn'] = json_encode( array(4,5,6));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal_penukaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"voucher_code"));
        array_push($column, array("data"=>"total_produk"));
        array_push($column, array("data"=>"grand_total"));
        array_push($column, array("data"=>"bonus"));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $data['column'] = json_encode($column);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/penukaran-po/history');
        $this->load->view('admin/static/footer');
    }
    function history_penukaran_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['estimasi_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['estimasi_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        if(isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][7]["search"]["value"]);
            $_GET['pengiriman_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['pengiriman_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }

        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;

        $result['iTotalRecords'] = $this->voucher_produk->history_penukaran_voucher_count($reseller_id);
        $result['iTotalDisplayRecords'] = $this->voucher_produk->history_penukaran_voucher_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->voucher_produk->history_penukaran_voucher_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $key->bonus = number_format($key->grand_total * 0.05);
            $key->grand_total = number_format($key->grand_total);
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
