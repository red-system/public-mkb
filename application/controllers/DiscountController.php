<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DiscountController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('discount','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/discount.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Discount < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "master_data";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"discount_nama"));
        array_push($column, array("data"=>"mulai_tanggal"));
        array_push($column, array("data"=>"akhir_tanggal"));
        array_push($column, array("data"=>"jangka_waktu"));
        array_push($column, array("data"=>"tipe_discount"));
        array_push($column, array("data"=>"nilai"));
        array_push($column, array("data"=>"keterangan"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        unset($_SESSION['redpos_discount']);
        $action = array();
        foreach ($akses_menu['master_data']['jenis-bahan'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/discount/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->discount->count_all();
        $result['iTotalDisplayRecords'] = $this->discount->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->discount->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'discount/delete/';
            $key->nilai = $key->tipe_discount == "Nominal"?$this->idr_currency($key->value_nominal):$key->value_percent."%";
            $key->row_id = $key->discount_id;
            $key->rangkuman = json_encode($key);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $discount_nama = $this->input->post('discount_nama');
        $jangka_waktu = $this->input->post('jangka_waktu');
        $produk_id = $this->input->post('produk_id_list');
        $mulai_tanggal = $jangka_waktu == "Ya" ? $this->input->post('mulai_tanggal') : null;
        $selesai_tanggal = $jangka_waktu == "Ya" ? $this->input->post('selesai_tanggal') : null;
        $tipe_discount = $this->input->post('tipe_discount');
        $minimal_qty_item = $this->input->post('minimal_qty_item');
        $minimal_total_belanja = $this->input->post('minimal_total_belanja');
        $value_nominal = $tipe_discount == "Nominal" ? $this->input->post('value_nominal') : null;
        $value_percent = $tipe_discount == "Percent" ? $this->input->post('value_percent') : null;
        $keterangan = $this->input->post('keterangan');
        $data = array(
            "discount_nama" => $discount_nama,
            "jangka_waktu" => $jangka_waktu,
            "produk_id" => $produk_id,
            "mulai_tanggal" => $mulai_tanggal,
            "akhir_tanggal" => $selesai_tanggal,
            "value_nominal" => $value_nominal,
            "value_percent" => $value_percent,
            "keterangan" => $keterangan,
            "minimal_qty_item" => $minimal_qty_item,
            "minimal_total_belanja" => $minimal_total_belanja,
        );
        $insert = $this->discount->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $discount_nama = $this->input->post('discount_nama');
        $jangka_waktu = $this->input->post('jangka_waktu');
        $produk_id = $this->input->post('produk_id_list');
        $mulai_tanggal = $jangka_waktu == "Ya" ? $this->input->post('mulai_tanggal') : null;
        $selesai_tanggal = $jangka_waktu == "Ya" ? $this->input->post('selesai_tanggal') : null;
        $tipe_discount = $this->input->post('tipe_discount');
        $value_nominal = $tipe_discount == "Nominal" ? $this->string_to_decimal($this->input->post('value_nominal')) : null;
        $value_percent = $tipe_discount == "Percent" ? $this->string_to_decimal($this->input->post('value_percent')) : null;
        $keterangan = $this->input->post('keterangan');
        $minimal_qty_item = $this->input->post('minimal_qty_item');
        $minimal_total_belanja = $this->input->post('minimal_total_belanja');
        $data = array(
            "discount_nama" => $discount_nama,
            "jangka_waktu" => $jangka_waktu,
            "produk_id" => $produk_id,
            "mulai_tanggal" => $mulai_tanggal,
            "akhir_tanggal" => $selesai_tanggal,
            "value_nominal" => $value_nominal,
            "value_percent" => $value_percent,
            "keterangan" => $keterangan,
            "minimal_qty_item" => $minimal_qty_item,
            "minimal_total_belanja" => $minimal_total_belanja,
        );
        $id = $this->input->post('discount_id');
        $update = $this->discount->update_by_id('discount_id',$id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->discount->delete_by_id("discount_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function produk_add(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->discount->produk_add_count_all();
        $result['iTotalDisplayRecords'] = $this->discount->produk_add_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->discount->produk_add_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->row_id = $key->produk_id;
            $key->aksi = null;
            $key->check = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function produk_all(){
        $query = $this->input->post('query');
        $response = $this->discount->get_all_produk($query);
        echo json_encode($response);
    }
    function set_id(){
        $produk_id = $this->input->post('produk_id');
        $_SESSION['redpos_discount']["produk_id"] = $produk_id;
        $result['success'] = true;
        $result['message'] = "Data berhasil simpan";
        echo json_encode($result);
    }
    function produk_detail_set_id(){
        $discount_id = $this->input->post('discount_id');
        $_SESSION['redpos_discount']["discount_id"] = $discount_id;
        $result['success'] = true;
        $result['message'] = "Data berhasil simpan";
        echo json_encode($result);
    }
    function unset_id(){
        unset($_SESSION['redpos_discount']);
        $result['success'] = true;
        $result['message'] = "Data berhasil simpan";
        echo json_encode($result);
    }
    function produk_detail(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->discount->produk_detail_count_all();
        $result['iTotalDisplayRecords'] = $this->discount->produk_detail_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->discount->produk_detail_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->row_id = $key->produk_id;
            $key->aksi = null;
            $key->check = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file PaternController.php */
/* Location: ./application/controllers/PaternController.php */
