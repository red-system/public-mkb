<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PoSaController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_produk','',true);
        $this->load->model('po_produk_detail','',true);
        $this->load->model('pengiriman_produk','',true);
        $this->load->model('pengiriman_produk_detail','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('bonus','',true);
        $this->load->model('reseller','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('master_login','',true);
        $this->load->helper('string');
        $this->load->library('main');
        $this->load->helper('url');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/po_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Order Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_produk";
        $data['page'] = "order_produk";
        $data["history"] = "";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"lokasi_nama"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"tipe_pembayaran_nama"));
        array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"estimasi_pengiriman"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['order-super'] as $key => $value) {
            if($key != "list" && $key != "akses_menu" ){
                $action[$key] = $value;
            }
        }
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po-sa/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->po_produk_count_all_admin();
        $result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_admin($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_produk->po_produk_list_admin($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);
            $key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
            $key->no = $i;
            if($key->jenis_pembayaran == "kas"){
                $key->tipe_pembayaran_nama = $key->kas_nama." ".$key->no_akun;
            }
            $i++;
            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
            $key->delete_url = base_url().'po-produk/delete/';
            $key->enc_row_id = str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
            $key->row_id = $key->po_produk_id;
            if($key->status_pembayaran!="Lunas"){
                $key->deny_penerimaan = true;
            }
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function penerimaan(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pengiriman_produk.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Order Produk < Penerimaan < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_produk";
        $data['page'] = 'penerimaan';
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $pengiriman = $this->pengiriman_produk->pengiriman_by_id($id);
        $data['id'] = $id;
        if ($id != null) {
            $data['pengiriman'] = $pengiriman;
            $data['po_detail'] = $this->pengiriman_produk->item_po($id);
//            echo json_encode($data['po_detail']);
//            return;
            array_push($column, array("data"=>"no"));
            array_push($column, array("data"=>"produk_nama"));
            array_push($column, array("data"=>"tanggal_pengiriman"));
            array_push($column, array("data"=>"total_qty_pengiriman"));
            array_push($column, array("data"=>"status_pengiriman_lbl"));
            array_push($column, array("data"=>"keterangan_pengiriman"));
            $data['sumColumn'] = json_encode(array(3));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className"=>"text__center","targets"=>array(0,2)));
            $data["action"] = json_encode(array("stock"=>false,"view"=>false, "edit"=>false,"delete"=>true, "submit" => true), true);

            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/po-sa/penerimaan');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function penerimaan_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pengiriman_produk->pengiriman_produk_count($this->uri->segment(4));
        $result['iTotalDisplayRecords'] = $this->pengiriman_produk->pengiriman_produk_filter($this->uri->segment(4),$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pengiriman_produk->pengiriman_produk_list($start,$length,$query,$this->uri->segment(4));
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pengiriman != null){
                $time = strtotime($key->tanggal_pengiriman);
                $key->tanggal_pengiriman = date('d-m-Y',$time);
            }
            $key->no = $i;
            $key->delete_url = base_url().'order-super-agen/penerimaan/delete/'.$key->pengiriman_produk_id;
            // $key->submit_url = base_url().'order-super-agen/penerimaan/accept-stock/'.$key->pengiriman_id;
            $key->row_id = $key->pengiriman_produk_id;
            $key->total_qty_pengiriman = number_format($key->total_qty_pengiriman);
            $key->status_pengiriman_lbl = 'Belum submit';
            if ($key->status_pengiriman == 1) {
                $key->deny_edit = 'true';
                $key->deny_delete = 'true';
                $key->deny_submit = 'true';
                $key->status_pengiriman_lbl = 'Submit';
            }
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function detail(){
        $pengiriman_id = $this->uri->segment(4);
        $data = $this->pengiriman_produk->pengiriman_by_pengiriman_id($pengiriman_id);
        echo json_encode($data);
    }
    
    function add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['po_produk_id'] = $this->string_to_number($this->input->post('po_produk_id'));
        $data['keterangan_pengiriman'] = $this->input->post('keterangan');
        $po_produk_id = $this->string_to_number($this->input->post('po_produk_id'));
        $produk_id = $this->string_to_number($this->input->post('produk_id'));
        $data['total_qty_pengiriman'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal_pengiriman'] = date("Y-m-d",$temp);
        $data['status_pengiriman'] = 0;
        $data['produk_id'] = $produk_id;
        $sisa = $this->string_to_number($this->input->post('sisa'));

        $last_stock = $this->stock_produk->last_stock_gudang($produk_id)->result;

        if($sisa >= $data['total_qty_pengiriman']){
            if ($last_stock >= $data['total_qty_pengiriman']) {
                $insert = $this->pengiriman_produk->insert($data);
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }else{
                $result['message'] = "Pengiriman melebihi sisa stock gudang";
            }
        } else {
            $result['message'] = "Pengiriman melebihi sisa order";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['po_produk_id'] = $this->string_to_number($this->input->post('po_produk_id'));
        $data['keterangan_pengiriman'] = $this->input->post('keterangan_pengiriman');
        $po_produk_id = $this->string_to_number($this->input->post('po_produk_id'));
        $produk_id = $this->string_to_number($this->input->post('produk_id'));
        $data['total_qty_pengiriman'] = $this->string_to_number($this->input->post('total_qty_pengiriman'));
        $temp = strtotime($this->input->post('tanggal_pengiriman'));
        $data['tanggal_pengiriman'] = date("Y-m-d",$temp);
        $pengiriman_produk_id = $this->string_to_number($this->input->post('pengiriman_produk_id'));
        
        $jumlah_order = $this->string_to_number($this->input->post('jumlah_order'));
        $sisa = $this->string_to_number($this->input->post('sisa'));
        // $terkirim = $this->string_to_number($this->input->post('terkirim'));
        // $old_jumlah = $this->string_to_number($this->input->post('old_jumlah'));
        // $new_terkirim = $terkirim - $old_jumlah + $data['total_qty_penerimaan'];

        $last_stock = $this->stock_produk->last_stock_gudang($produk_id)->result;

        if($data['total_qty_pengiriman'] <= $sisa){
            if ($last_stock >= $data['total_qty_pengiriman']) {
                $edit = $this->pengiriman_produk->update_by_id('pengiriman_produk_id',$pengiriman_produk_id,$data);
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }else{
                $result['message'] = "Pengiriman melebihi sisa stock gudang";
            }

        } else {
            $result['message'] = "Pembayaran melebihi sisa pengiriman";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->pengiriman_produk->delete_pengiriman($id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

    function accept_stock(){
        $pengiriman_produk_id = $this->input->post("id");

        // get data pengiriman
        $data_pengiriman = $this->pengiriman_produk->detail_pengiriman($pengiriman_produk_id);
        $po_produk_id = $data_pengiriman->po_produk_id;
        $produk_id = $data_pengiriman->produk_id;
        $stock_qty = $data_pengiriman->total_qty_pengiriman;

        $this->po_produk->start_trans();

        // update pengiriman status
        $pengiriman['status_pengiriman'] = 1;
        $this->pengiriman_produk->update_by_id('pengiriman_produk_id',$pengiriman_produk_id,$pengiriman);

        $data_detail = $this->pengiriman_produk->get_produk_detail_by_pengiriman($po_produk_id,$produk_id);
        foreach ($data_detail as $value) {
            $po_produk_detail_id = $value->po_produk_detail_id;
            $lokasi_nama = $value->lokasi_nama;
            $po_detail['sisa_qty'] = $value->sisa_qty - $stock_qty;
            $this->po_produk_detail->update_by_id('po_produk_detail_id',$po_produk_detail_id,$po_detail);

            $kirim['pengiriman_produk_id'] = $pengiriman_produk_id;
            $kirim['po_produk_detail_id'] = $po_produk_detail_id;
            $kirim['qty_pengiriman'] = $stock_qty; 
            $insert_kirim = $this->pengiriman_produk_detail->insert($kirim);

            // cek detail untuk update status
            $cek_sisa = $this->po_produk_detail->cek_sisa($po_produk_id);
            if ($cek_sisa->sisa_qty >0) {
                $po["status_penerimaan"] = "Sebagian";
            }else{
                $po["status_penerimaan"] = "Diterima";
            }

            $po_data = $this->po_produk->row_by_id($po_produk_id);
            $po["tanggal_penerimaan"] = date("Y-m-d");
            $po['lokasi_penerimaan_id'] = $po_data->lokasi_reseller;
            $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
            $gudang = $this->lokasi->all_gudang();
            $gudang = $gudang[0];
            foreach ($po_produk_item as $key) {
                
                $row_stock = $this->stock_produk->stock_by_location_produk_id($gudang->lokasi_id,$key->produk_id);
                $jumlah_bahan = $stock_qty;
                foreach ($row_stock as $row) {
                    $stock = array();
                    $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                    $stock_out = $jumlah_bahan;
                    if($row->stock_produk_qty < $jumlah_bahan){
                        $jumlah = 0;
                        $stock_out = $row->stock_produk_qty;
                    }
                    $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                    $stock["stock_produk_qty"] = $jumlah;
                    $stock_produk_id = $row->stock_produk_id;
                    $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$stock);
                    if($updateStok){
                        $arus = array();
                        $arus["tanggal"] = date("Y-m-d");
                        $arus["table_name"] = "stock_produk";
                        $arus["stock_produk_id"] = $row->stock_produk_id;
                        $arus["produk_id"] = $key->produk_id;
                        $arus["stock_out"] = $stock_out;
                        $arus["stock_in"] = 0;
                        $arus["last_stock"] = $this->stock_produk->last_stock_gudang($row->produk_id)->result;
                        $arus["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                        $arus["keterangan"] = "Order Super Agent a/n ".$lokasi_nama;
                        $arus["method"] = "update";
                        $this->stock_produk->arus_stock_produk($arus);

                        $data = array();
                        $data["stock_produk_qty"] = $stock_out;
                        $data["produk_id"] = $key->produk_id;
                        $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
                        $data["year"] = date("y");
                        $data["month"] = date("m");
                        $data["po_id"] = $po_produk_id;
                        $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                        $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                        $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                        $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                        $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                        $data["hpp"] = $this->string_to_number($key->harga);
                        $insert = $this->stock_produk->insert($data);
                    }
                    if($jumlah_bahan <= 0){
                        break;
                    }

                    if ($jumlah > 0) {
						break;
					}
    
                }
            }

            $update_po = $this->po_produk->update_by_id("po_produk_id",$po_produk_id,$po);

            if ($update_po) {

                $data_user = $this->master_login->row_by_field('lokasi_id', $po_data->lokasi_reseller);
                $reseller_id = $data_user->reseller_id;
                $username = $data_user->username;

                $reseller = $this->reseller->row_by_id($reseller_id);
                $referal_link  = $this->config->item('url-landing').$reseller->referal_code;

                $mailContentAdmin = '<html><head>
                    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
                    <link rel="preconnect" href="https://fonts.gstatic.com">
                    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
                    <style>
                        body{
                            font-family: \'Open Sans\', sans-serif;
                        }

                        .content {
                            max-width: 550px;
                            margin: auto;
                        }
                        .title{
                            width: 60%;
                        }
                        .data,.data th,.data td {
                            border: 1px solid black;
                        }
                        a{
                            color: #990000;
                        }
                        a:hover{
                            color: #990000;
                        }
                        .btn {
                            display: inline-block;
                            font-weight: normal;
                            color: #212529;
                            text-align: center;
                            vertical-align: middle;
                            -webkit-user-select: none;
                            -moz-user-select: none;
                            -ms-user-select: none;
                            user-select: none;
                            background-color: transparent;
                            border: 1px solid transparent;
                            padding: 0.65rem 1rem;
                            font-size: 1rem;
                            cursor: pointer;
                            line-height: 1.5;
                            border-radius: 0.25rem;
                            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                        }
                        .btn {
                            background: transparent;
                            outline: none !important;
                            vertical-align: middle;
                        }
                        .btn-success {
                            color: #fff;
                            background-color: #0abb87;
                            border-color: #0abb87;
                        }
                        .btn.btn-pill {
                            border-radius: 2rem;
                            padding-right: 40px;
                            padding-left: 40px;
                            font-size: 12px;
                        }
                    </style>
                </head>
                <body style="background-color: #fff">
                <div class="content" style="background-color: #fff">
                    <div>
                        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

                            <tbody>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <div style="width: 450px; height: 550px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller->nama . ' ('.$username.') ,</h4>
                                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                                        <span>Produk yang anda pesan sudah dikirimkan
                                        </span>
                                        <br>
                                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                                            <table width="100%">
                                                <tbody>
                                                    <tr style="height: 25px">
                                                        <td style="text-align: left;width:25%">No Order </td>
                                                        <td>:</td>
                                                        <td style="text-align: right">' . $po_data->po_produk_no . ' </td>
                                                    </tr>
                                                    <tr style="height: 25px">
                                                        <td style="text-align: left;width:25%">Jumlah Produk Dipesan </td>
                                                        <td>:</td>
                                                        <td style="text-align: right">' . number_format($cek_detail->jumlah_qty) . ' botol</td>
                                                    </tr>
                                                    <tr style="height: 25px">
                                                        <td style="text-align: left;width:25%">Jumlah Dikirim </td>
                                                        <td>:</td>
                                                        <td style="text-align: right">' . number_format($stock_qty) . ' botol</td>
                                                    </tr>
                                                    <tr style="height: 25px">
                                                        <td style="text-align: left;width:25%">Sisa </td>
                                                        <td>:</td>
                                                        <td style="text-align: right">' . number_format($cek_detail->sisa_qty) . ' botol</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        <br>
                                        <span style="margin-top: 20px">
                                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                                        </span>
                                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                                        <span style="margin-top: 20px">
                                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                                        </span>
                                        <div style="margin-top: 40px"></div>
                                        <span >
                                            Terima Kasih,
                                        </span>
                                        <div style="margin-top: 25px"></div>
                                        <span >
                                            Tim My Kind Of Beauty
                                        </span>
                                    </div>
                                    <div style="margin-top: 20px;text-align: center">
                                        <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                                    </div>
                                    <div style="margin-top: 10px;text-align: center">
                                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                                    </div>

                                </td>

                            </tr>
                            </tbody></table>
                        <p>&nbsp;<br></p>
                    </div>

                </div>

                </body></html>';
                $this->main->mailer_auth('Pengiriman Produk', $reseller->email, $reseller->nama, $mailContentAdmin);

            }
            
            
        }

        if($this->po_produk->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}

        echo json_encode($result);
    }


    function accept(){
        $po_produk_id = $this->input->post("id");
        $po_data = $this->po_produk->row_by_id($po_produk_id);
        $po["status_penerimaan"] = "Diterima";
        $po["tanggal_penerimaan"] = date("Y-m-d");
        $po['lokasi_penerimaan_id'] = $po_data->lokasi_reseller;
        $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        foreach ($po_produk_item as $key) {
            $data = array();
            $data["stock_produk_qty"] = $key->jumlah;
            $data["produk_id"] = $key->produk_id;
            $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
            $data["year"] = date("y");
            $data["month"] = date("m");
            $data["po_id"] = $po_produk_id;
            $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
            $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
            $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
            $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
            $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
            $data["hpp"] = $this->string_to_number($key->harga);
            $insert = $this->stock_produk->insert($data);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$key->produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get($_SESSION["redpos_company"]["database"] .".stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty - $key->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update($_SESSION["redpos_company"]["database"] .".stock_produk",$stock_update);
        }

        $this->po_produk->update_by_id("po_produk_id",$po_produk_id,$po);
        $result["success"] = true;
        $result["message"] = "Sukses mengubah status data";
        echo json_encode($result);
    }
    function refuse(){
        $po_produk_id = $this->input->post("po_produk_id");
        $keterangan = $this->input->post("keterangan_tolak");
        $po["status_penerimaan"] = "Ditolak";
        $po["keterangan_tolak"] = $keterangan;
        $this->po_produk->update_by_id("po_produk_id",$po_produk_id,$po);
        $result["success"] = true;
        $result["message"] = "Sukses mengubah status data";
        echo json_encode($result);

    }
    function history(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/po_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["history"] = "true";
        $data["meta_title"] = "History Order Super Agent < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order-super";
        $data['page'] = "order-super";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"lokasi_nama"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"tipe_pembayaran_nama"));
        array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"estimasi_pengiriman"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"keterangan_tolak"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
        $data['lokasi'] = $this->lokasi->all_list();
        $action = array("view"=>true,"print"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po-sa/index');
        $this->load->view('admin/static/footer');
    }
    function history_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->po_produk_count_all_history_admin();
        $result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_history_admin($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_produk->po_produk_list_history_admin($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);

            $key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'po-produk/delete/';
            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
            $key->tipe_pembayaran_nama = $key->tipe_pembayaran_nama2." ".$key->no_akun;
            $key->row_id = $key->po_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */