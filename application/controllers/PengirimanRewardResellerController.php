<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PengirimanRewardResellerController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pengiriman_reward_reseller','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/admin/pengiriman-reward-reseller.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"jumlah"));
        array_push($column, array("data"=>"keterangan"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"manage_status","template"=>"badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array(4));
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pengiriman-reward-reseller/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_all();
        $result['iTotalDisplayRecords'] = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->pengiriman_reward_reseller_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->pengiriman_reward_reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function success_page(){

        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/pengiriman-reward-reseller.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = $this->uri->segment(1);
        $data['sumColumn'] = json_encode( array(4));
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"jumlah"));
        array_push($column, array("data"=>"nama_event"));
        array_push($column, array("data"=>"keterangan"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"manage_status","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pengiriman-reward-reseller/index_success');
        $this->load->view('admin/static/footer');
    }
    function success_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_all_success();
        $result['iTotalDisplayRecords'] = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_filter_success($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_list_success($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->pengiriman_reward_reseller_id.'"></label>';
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->pengiriman_reward_reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function hold(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hadiah";
        $data['page'] = $this->uri->segment(1);

        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama_penerima"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"jumlah_deposit"));
        array_push($column, array("data"=>"persentase"));
        array_push($column, array("data"=>"jumlah_bonus"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pengiriman-reward-reseller/index_hold');
        $this->load->view('admin/static/footer');
    }
    function hold_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_all_hold();
        $result['iTotalDisplayRecords'] = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_filter_hold($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->pengiriman_reward_reseller->pengiriman_reward_reseller_list_hold($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_deposit = number_format($key->jumlah_deposit);
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->pengiriman_reward_reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function processing(){

//        $this->load->model('dpp_reseller','',true);
        $this->load->model('send_bonus','',true);
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";

        $pengiriman_reward_reseller_id = $this->input->post('pengiriman_reward_reseller_id');
        $pengiriman_reward_reseller_id = str_replace("##",",",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = str_replace("#","",$pengiriman_reward_reseller_id);
        $no_generate = $this->send_bonus->get_no_generate();
        $url_get = base_url()."pengiriman-reward-reseller/print?no_generate=".$no_generate;
        $pengiriman_reward_reseller_id = explode(",",$pengiriman_reward_reseller_id);
        $proses = $this->pengiriman_reward_reseller->changeToProcessing($pengiriman_reward_reseller_id);
        $listDpp = $this->pengiriman_reward_reseller->listForDPPKontes($pengiriman_reward_reseller_id);

        $no_generate = $this->send_bonus->get_no_generate();
        foreach ($listDpp as $item){



            $send_bonus['reseller_id'] = $item->to_reseller_id;
            $send_bonus['type'] = 'bonus';
            $send_bonus['no_generate'] = $no_generate;
            $send_bonus['tanggal_proccess'] = date('Y-m-d H:i:s');
            $send_bonus['jumlah_bonus'] = $item->total_bonus;
            $send_bonus['admin'] = 10000;
            $send_bonus['pajak'] = 0;
            $send_bonus['type'] = 'reward';
            $send_bonus['bonus_net'] = $item->total_bonus - $send_bonus['admin'] - ($send_bonus['pajak']);
            $this->send_bonus->insert($send_bonus);
            $send_bonus_id = $this->send_bonus->last_id();
            $bonus_ids = explode(',',$item->pengiriman_reward_reseller_ids);
            $this->pengiriman_reward_reseller->update_bonus_send($send_bonus_id,$bonus_ids);

        }

        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
            $result["url_get"] = $url_get;
        }
        echo json_encode($result);
    }
    function fail(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $pengiriman_reward_reseller_id = $this->input->post('pengiriman_reward_reseller_id');
        $pengiriman_reward_reseller_id = str_replace("##",",",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = str_replace("#","",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = explode(",",$pengiriman_reward_reseller_id);
        $proses = $this->pengiriman_reward_reseller->changeToFail($pengiriman_reward_reseller_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }
    function success_process(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $pengiriman_reward_reseller_id = $this->input->post('pengiriman_reward_reseller_id');
        $pengiriman_reward_reseller_id = str_replace("##",",",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = str_replace("#","",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = explode(",",$pengiriman_reward_reseller_id);

        $proses = $this->pengiriman_reward_reseller->changeToSuccess($pengiriman_reward_reseller_id);
//        if($proses){
//            $result["success"] = true;
//            $result["message"] = "Berhasil mengubah data";
//        }
//        echo json_encode($result);
    }
    function success_process_date(){
        $this->load->model('send_bonus','',true);
        $this->load->model('reward_reseller','',true);
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $tanggal_pengiriman = $this->input->post('tanggal_pengiriman');
        $pengiriman_reward_reseller_id = $this->input->post('pengiriman_reward_reseller_id');
        $pengiriman_reward_reseller_id = str_replace("##",",",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = str_replace("#","",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = explode(",",$pengiriman_reward_reseller_id);
        $reseller_id = $this->pengiriman_reward_reseller->getResellerId($pengiriman_reward_reseller_id);
        $reseller_id = explode(",",$reseller_id);
        $proses = $this->pengiriman_reward_reseller->changeToSuccessWithDate($pengiriman_reward_reseller_id, $tanggal_pengiriman);
        $this->send_bonus->changeToSuccessWithDate($pengiriman_reward_reseller_id, $tanggal_pengiriman);
        $this->reward_reseller->changeToSuccessWithDate($reseller_id, $tanggal_pengiriman);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);

    }
    function turnback(){
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $pengiriman_reward_reseller_id = $this->input->post('pengiriman_reward_reseller_id');
        $pengiriman_reward_reseller_id = str_replace("##",",",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = str_replace("#","",$pengiriman_reward_reseller_id);
        $pengiriman_reward_reseller_id = explode(",",$pengiriman_reward_reseller_id);

        $proses = $this->pengiriman_reward_reseller->changeToTurnback($pengiriman_reward_reseller_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";

        }
        echo json_encode($result);
    }
    function print(){
        $this->load->model('send_bonus','',true);
        $no_generate = $this->input->get("no_generate");

        $potongan = 10000;
        $data['potongan'] = $potongan;
        $bonus = $this->send_bonus->bonusListPrint($no_generate);
        $total_bonus = $this->send_bonus->bonusTotal($no_generate);
        $data['bonus'] = $bonus;
        $data['total_bonus'] = $total_bonus;
        $data['no_generate'] = $no_generate;
        $this->load->view('admin/management-bonus/print',$data);
    }
    function porcess_data(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/pengiriman-reward-reseller.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"gross"));
        array_push($column, array("data"=>"admin"));
        array_push($column, array("data"=>"bonus_net"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode(
            array(4,5,6)
        );
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array("view"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pengiriman-reward-reseller/index_terproses');
        $this->load->view('admin/static/footer');
    }
    function porcess_list(){
        $this->load->model('send_bonus','',true);
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->send_bonus->send_prosessing_bonus_all('reward');
        $result['iTotalDisplayRecords'] = $this->send_bonus->send_prosessing_bonus_filter($query,'reward');
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->send_bonus->send_prosessing_bonus_list($start,$length,$query,'reward');
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->bonus_id = $key->send_bonus_id;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'" data-status="'.$key->manage_status.'"></label>';
            $key->gross = number_format($key->gross);
            $key->admin = number_format($key->admin);
            $key->pajak = number_format($key->pajak,2);
            $key->bonus_net = number_format($key->bonus_net,2);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function cancel(){
        $this->load->model('send_bonus','',true);
        $result["success"] = false;
        $result["message"] = "Gagal mengubah data";
        $bonus_id = $this->input->post('bonus_id');
        $bonus_id = str_replace("##",",",$bonus_id);
        $bonus_id = str_replace("#","",$bonus_id);
        $send_bonus_id = explode(",",$bonus_id);
        $proses = $this->pengiriman_reward_reseller->changeTowaiting($send_bonus_id);
        $this->send_bonus->changeToCancel($send_bonus_id);
        if($proses){
            $result["success"] = true;
            $result["message"] = "Berhasil mengubah data";
        }
        echo json_encode($result);
    }
    function terkirim(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/pengiriman-reward-reseller.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"check"));
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"gross"));
        array_push($column, array("data"=>"admin"));
        array_push($column, array("data"=>"bonus_net"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode(
            array(4,5,6)
        );
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array("view"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pengiriman-reward-reseller/index_terkirim');
        $this->load->view('admin/static/footer');
    }
    function terkirim_list(){
        $this->load->model('send_bonus','',true);
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->send_bonus->terkirim_bonus_all('reward');
        $result['iTotalDisplayRecords'] = $this->send_bonus->terkirim_bonus_filter($query,'reward');
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->send_bonus->terkirim_bonus_list($start,$length,$query,'reward');
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->bonus_id = $key->send_bonus_id;
            $key->check = '<label><input class="check-bonus" type="checkbox" value="'.$key->bonus_id.'" data-status="'.$key->manage_status.'"></label>';
            $key->gross = number_format($key->gross);
            $key->admin = number_format($key->admin);
            $key->pajak = number_format($key->pajak,2);
            $key->bonus_net = number_format($key->bonus_net,2);
            $key->persentase = $key->persentase."%";
            $key->bukti_transfer_url = $this->config->item("dev_storage_image").$key->bukti_transfer;

            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function proses_detail(){}

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
