<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterLoginController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_login','',true);
    }

    public function index()
    {
        if(isset($_SESSION["master"])){
            redirect(base_url().'master','refresh');
        } else {
            $this->load->view('master/login/index');
        }
    }
    function auth(){
        $result['success'] = false;
        $result['message'] = "missing parameter";
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $auth = $this->master_login->auth_login_master($username);
        if($auth != null){
            if (password_verify($password, $auth->password)){

                $result["success"] = true;
                $result["message"] = "Login success";
                $_SESSION["master"]["id"] = $auth->id;
                $_SESSION["master"]["company_id"] = $auth->company_id;
            } else {
                $result["message"] = "Wrong password";
            }
        } else {
            $result["message"] = "Username not registered";
        }
        echo json_encode($result);
    }
    function logout(){
        $result['success'] = false;
        $result['message'] = 'Gagal logout, silakan lakukan closing pada POS terlebih dahulu';
            unset($_SESSION["master"]);
            $result['success'] = true;
            $result['message'] = 'Berhasil logout';
        echo json_encode($result);
    }
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
