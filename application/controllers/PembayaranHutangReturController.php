<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PembayaranHutangReturController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_produk','',true);
        $this->load->model('pembayaran_hutang_produk','',true);
        $this->load->model('pembayaran_hutang_retur','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('reseller','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('hutang_produk','',true);
        $this->load->model('hutang_retur','',true);
        $this->load->model('piutang_produk','',true);
        $this->load->model('produk','',true);
        $this->load->model('bonus','',true);
        $this->load->model('refuse_pembayaran_piutang','',true);
        $this->load->model('refuse_pembayaran_retur','',true);
        $this->load->helper('string');
        $this->load->library('main');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pem_hutang_retur.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang-retur";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['pembayaran-hutang-retur'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-retur/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_retur->_all();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_retur->_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_retur->_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            $key->edit_url = base_url().'pembayaran-hutang-retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->no = $i;
            $i++;
            $key->print_url = base_url()."pembayaran-hutang-retur/print/".$key->pembayaran_hutang_produk_id;
            $key->delete_url = base_url().'pembayaran-hutang-retur/delete/';
            $key->row_id = $key->pembayaran_hutang_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['suplier'] = $this->suplier->all_list();
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_po();
        $data['po_produk_no'] = $this->po_produk->get_kode_po_produk();

        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-retur/add_pembayaran');
        $this->load->view('admin/static/footer');
        unset($_SESSION['po_produk']);
        $_SESSION['po_produk']['lokasi'] = 1;
    }
    function utility(){
        $key = $this->uri->segment(3);
        if($key == "get-po-no"){
            $code = $this->input->post('suplier_kode');
            echo $this->po_produk->get_kode_po_produk($code);
        }
        if($key=="list-reseller"){
            $this->list_reseller();
        }
        if($key=="list-produk"){
            $this->list_produk();
        }
        if($key=="sess-reseller"){
            $_SESSION['po_produk']["reseller_id"] = $this->input->post('reseller_id');
            echo json_encode($_SESSION['po_produk']);
        }
        if($key=="sess-produk-add"){
            $no = $this->input->post('key');
            $produk_id = $this->input->post('produk_id');
            $_SESSION['po_produk']['produk']['con_'.$no]['produk_id'] = $produk_id;
            $_SESSION['po_produk']['produk']['con_'.$no]['value'] = 0;
        }
        if($key=="sess-produk-change"){
            $jumlah = $this->input->post('jumlah');
            $no = $this->input->post('key');
            if(isset($_SESSION['po_produk']['produk']['con_'.$no]['produk_id'])){
                $_SESSION['po_produk']['produk']['con_'.$no]['value'] = $jumlah;
            }
        }
        if($key=="sess-produk-delete"){
            $no = $this->input->post('key');
            unset($_SESSION['po_produk']['produk']['con_'.$no]);
        }
        if($key=="sess-produk-reset"){
            unset($_SESSION['po_produk']['produk']);
        }
        if($key=="sess-lokasi"){
            $_SESSION['po_produk']['lokasi'] = $this->input->post('lokasi_id');
        }
        if($key=="list-hutang"){
            $this->list_hutang();
        }
    }
    function list_reseller(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "active";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function list_hutang(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_hutang_all();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_hutang_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_hutang_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pelunasan != null){
                $time = strtotime($key->tanggal_pelunasan);
                $key->tanggal_pelunasan = date('d-m-Y',$time);
            }

            $key->row_id = $key->hutang_produk_id;
            $key->jumlah = number_format($key->jumlah);
            $key->no = $i;
            $key->action = null;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function list_produk(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->produk->produk_count_all();
        $result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk->produk_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'produk/delete/';
            $key->action =null;
            $key->last_hpp = $this->stock_produk->last_hpp($key->produk_id);
            $key->last_hpp = $key->last_hpp == 0 ? $key->hpp_global : $key->last_hpp;
            $key->row_id = $key->produk_id;
            $key->produk_minimal_stock = number_format($key->produk_minimal_stock);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function save_add(){
        $input = $this->input->post();
        $tanggal = $this->input->post('tanggal');
        $prefix = "PP-".date("Ymd")."/";
        $hutang_produk_id = $this->input->post('hutang_produk_id');
        $pembayaran = $this->pembayaran_hutang_produk->getPembayaranNo($prefix);
        if($pembayaran==null){
            $no_pembayaran = $prefix."1";
        }else{
            $arUrutan = explode($prefix,$pembayaran->no_pembayaran);
            $no_pembayaran = $prefix.($arUrutan[1]+1);
        }
        $pembayaran_hutang = array();
        $pembayaran_hutang["tanggal"] = date("Y-m-d");
        $pembayaran_hutang["no_pembayaran"] = $no_pembayaran;
        $insert = $this->pembayaran_hutang_produk->insert($pembayaran_hutang);
        $pembayaran_hutang_produk_id = $this->pembayaran_hutang_produk->last_id();
        $pembayaran = $this->hutang_produk->pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id);
        $reseller_id = null;
        $lokasi_id = null;
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        foreach ($hutang_produk_id as $item){
            $hutang = $this->hutang_produk->row_by_id($item);
            if($reseller_id==null){
                $reseller_id = $hutang->reseller_id;
                $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
                $lokasi_id = $login->lokasi_id;
            }
            $produk_id = $hutang->produk_id;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty + $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty - $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

        }
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        echo json_encode($result);

    }
    function edit(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $pembayaran_hutang_produk = $this->pembayaran_hutang_produk->row_by_id($id);
        unset($_SESSION['po_produk']);
        if ($pembayaran_hutang_produk != null) {
            $pembayaran_hutang_produk_detail  = $this->pembayaran_hutang_produk->pembayaran_hutang_produk_detail($id);
            $data["pembayaran_hutang_produk_detail"] = $pembayaran_hutang_produk_detail;
            $data["size"] = sizeof($pembayaran_hutang_produk_detail);
            $_SESSION['po_produk']["reseller_id"] = $pembayaran_hutang_produk_detail[0]->reseller_id;
            $_SESSION['po_produk']["pembayaran_hutang_produk"] = $id;
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "script/admin/pem_hutang_produk.js");
            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
            $data['parrent'] = "hutang_piutang";
            $data['page'] = "pembayaran-hutang";
            $data['lokasi'] = $this->lokasi->all_list();
            $data['suplier'] = $this->suplier->all_list();
            $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_po();
            $data['po_produk_no'] = $this->po_produk->get_kode_po_produk();

            $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
            $reseller_id = $login->reseller_id;
            $reseller = $this->reseller->row_by_id($reseller_id);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/pembayaran-hutang-retur/edit_pembayaran');
            $this->load->view('admin/static/footer');
        }else {
            redirect('404_override','refresh');
        }
    }
    function save_edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $input = $this->input->post();
        $tanggal = $this->input->post('tanggal');
        $prefix = "PP-".date("Ymd")."/";
        $hutang_produk_id = $this->input->post('hutang_produk_id');
        $pembayaran = $this->pembayaran_hutang_produk->getPembayaranNo($prefix);

        if($pembayaran==null){
            $no_pembayaran = $prefix."1";
        }else{
            $arUrutan = explode($prefix,$pembayaran->no_pembayaran);
            $no_pembayaran = $prefix.($arUrutan[1]+1);
        }
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $pembayaran_hutang = array();
        $pembayaran_hutang["tanggal"] = date("Y-m-d");
        $pembayaran_hutang["no_pembayaran"] = $no_pembayaran;
        $insert = $this->pembayaran_hutang_produk->update_by_id("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id,$pembayaran_hutang);
        $this->pembayaran_hutang_produk->penarikan_stock($pembayaran_hutang_produk_id);
        $pembayaran = $this->hutang_produk->pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id);
        $pembayaran = $this->piutang_produk->pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id);
        $reseller_id = null;
        $lokasi_id = null;
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        foreach ($hutang_produk_id as $item){
            $hutang = $this->hutang_produk->row_by_id($item);
            if($reseller_id==null){
                $reseller_id = $hutang->reseller_id;
                $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
                $lokasi_id = $login->lokasi_id;
            }
            $produk_id = $hutang->produk_id;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty + $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty - $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

        }
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $this->pembayaran_hutang_produk->penarikan_stock($id);
            $delete = $this->pembayaran_hutang_produk->delete_by_id("pembayaran_hutang_produk_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function detail(){

        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $pembayaran_hutang_produk = $this->pembayaran_hutang_retur->row_by_id($pembayaran_hutang_produk_id);

        $item = $this->pembayaran_hutang_retur->pembayaran_hutang_produk_detail($pembayaran_hutang_produk_id);
        if($item==null){
            $refuse = $this->refuse_pembayaran_retur->row_by_field('pembayaran_hutang_retur_id',$pembayaran_hutang_produk_id);
            $json = $refuse->json;
            $item = json_decode($json);
        }
        foreach ($item as $val){
            $val->jumlah = number_format($val->jumlah);
        }
        $pembayaran_hutang_produk->item = $item;
        echo json_encode($pembayaran_hutang_produk);
    }
    function penerimaan_po_produk(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $po_produk_id = $this->input->post('po_produk_id');
        $data["status_penerimaan"] = "Diterima";
        $data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
        $lokasi_id = $this->input->post('lokasi_penerimaan_id');
        $data['lokasi_penerimaan_id'] = $lokasi_id;
        $this->po_produk->start_trans();
        $update_po_produk = $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$data);
        if($update_po_produk){
            $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
            foreach ($po_produk_item as $key) {
                $data = array();
                $data["stock_produk_qty"] = $key->jumlah;
                $data["produk_id"] = $key->produk_id;
                $data["stock_produk_lokasi_id"] = $lokasi_id;
                $data["year"] = date("y");
                $data["month"] = date("m");
                $data["po_id"] = $po_produk_id;
                $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                $data["hpp"] = $this->string_to_number($key->harga);
                $insert = $this->stock_produk->insert($data);
                $arus["stock_produk_id"] = $this->stock_produk->last_id();
                $arus["method"] = "insert";
                $arus["tanggal"] = date("Y-m-d");
                $arus["table_name"] = "stock_produk";
                $arus["produk_id"] = $key->produk_id;
                $arus["stock_out"] = 0;
                $arus["stock_in"] = $key->jumlah;
                $arus["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
                $arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
                $arus["keterangan"] = "Order Produk";
                $this->stock_produk->arus_stock_produk($arus);
            }
        }
        if($this->po_produk->result_trans()){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function history(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/po_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "History Order Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_produk";
        $data['page'] = "order_produk";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"tipe_pembayaran_nama"));
        array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"keterangan_tolak"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
        $data['lokasi'] = $this->lokasi->all_list();
        $action = array("view"=>true,"print"=>true);
        $data['action'] = json_encode($action);
        $data["history"] = "true";
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po_produk');
        $this->load->view('admin/static/footer');
    }
    function history_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->po_produk_count_all_history();
        $result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_history($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_produk->po_produk_list_history($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);

            $key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'po-produk/delete/';
            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
            $key->row_id = $key->po_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function print(){
        $id = $this->uri->segment(3);
        $temp = $this->po_produk->po_produk_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->po_produk->po_produk_detail_by_id($id);
        $temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
        if($temp->tanggal_pemesanan != null){
            $x = strtotime($temp->tanggal_pemesanan);
            $temp->tanggal_pemesanan = date("Y-m-d",$x);
        }
        if($temp->tanggal_penerimaan != null){
            $x = strtotime($temp->tanggal_penerimaan);
            $temp->tanggal_penerimaan = date("Y-m-d",$x);
        }
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga);
            $key->jumlah = number_format($key->jumlah);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        $data['po_produk'] = $temp;
        $this->load->view('admin/po-produk/print',$data);
    }
    function uploadImage($file,$url){
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $date = new DateTime();
        $file_name = $date->getTimestamp().random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/".$file_name.".".$ext, $_FILES[$file]['tmp_name'],NET_SFTP_LOCAL_FILE);
        if (!$output)
        {
            $url = "failed";
            return $url;
        }
        else
        {
            $url = $file_name.'.'.$ext;
        }
        return $url;
    }
    function approve(){
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $approve = $this->pembayaran_hutang_retur->approve($pembayaran_hutang_produk_id);
        $hutang_produk_id = $this->hutang_retur->hutang_retur_ids($pembayaran_hutang_produk_id);
        $this->hutang_retur->pembayaran_hutang_retur($pembayaran_hutang_produk_id);
//        $reseller_id = null;
//        $lokasi_id = null;
//        $gudang = $this->lokasi->all_gudang();
//        $gudang = $gudang[0];
//
//
//        foreach ($hutang_produk_id as $item){
//            $hutang_id = $item->hutang_produk_id;
//            $hutang = $this->hutang_retur->row_by_id($hutang_id);
//            $reseller_id = $hutang->reseller_id;
//            if($reseller_id==null){
//                $reseller_id = $hutang->reseller_id;
//                $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
//                $lokasi_id = $login->lokasi_id;
//            }
//            $produk_id = $hutang->produk_id;
//            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
//            $this->db->where("produk_id",$produk_id);
//            $this->db->order_by("stock_produk_id");
//            $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
//            $jumlah = $stock_produk->stock_produk_qty + $hutang->jumlah;
//            $stock_update = array();
//            $stock_update["stock_produk_qty"] = $jumlah;
//            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
//            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
//            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
//            $this->db->where("produk_id",$produk_id);
//            $this->db->order_by("stock_produk_id");
//            $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
//            $jumlah = $stock_gudang->stock_produk_qty - $hutang->jumlah;
//            $stock_update = array();
//            $stock_update["stock_produk_qty"] = $jumlah;
//            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
//            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
//        }
//        $reseller = $this->reseller->row_by_id($reseller_id);

        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        echo json_encode($result);
    }
    function refuse(){
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_id");
        $hutang_produks = $this->hutang_retur->hutang_retur_ids($pembayaran_hutang_produk_id);
        $reseller_id = $hutang_produks[0]->reseller_id;
        $json = json_encode($hutang_produks);
        $this->pembayaran_hutang_retur->setPembayaranNull($pembayaran_hutang_produk_id);
        $data = array();
        $data["status"] = "refuse";
        $this->pembayaran_hutang_retur->update_by_id('pembayaran_hutang_retur_id',$pembayaran_hutang_produk_id,$data);
        $refuse = array();
        $message = $this->input->post("alasan_refuse");
        $refuse["pembayaran_hutang_retur_id"] = $pembayaran_hutang_produk_id;
        $refuse["message"] = $message;
        $refuse["json"] = $json;
        $refuse["reseller_id"] = $reseller_id;
        $this->refuse_pembayaran_retur->insert($refuse);
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        echo json_encode($result);
    }
    function approve_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['pembayaran-hutang'] as $key => $value) {
            if($key == "view"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-retur/index_approve');
        $this->load->view('admin/static/footer');
    }
    function approve_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_all_approve();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_filter_approve($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_list_approve($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            $key->edit_url = base_url().'pembayaran-hutang-retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->no = $i;
            $i++;
            $key->print_url = base_url()."pembayaran-hutang-retur/print/".$key->pembayaran_hutang_produk_id;
            $key->delete_url = base_url().'pembayaran-hutang-retur/delete/';
            $key->row_id = $key->pembayaran_hutang_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function refuse_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['pembayaran-hutang'] as $key => $value) {
            if($key == "view"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-retur/index_refuse');
        $this->load->view('admin/static/footer');
    }
    function refuse_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_all_refuse();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_filter_refuse($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_list_refuse($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            $key->edit_url = base_url().'pembayaran-hutang-retur/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->no = $i;
            $i++;
            $key->print_url = base_url()."pembayaran-hutang-retur/print/".$key->pembayaran_hutang_produk_id;
            $key->delete_url = base_url().'pembayaran-hutang-retur/delete/';
            $key->row_id = $key->pembayaran_hutang_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */