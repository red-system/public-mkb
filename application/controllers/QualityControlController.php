<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QualityControlController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('produk','',true);
        $this->load->model('user','',true);
        $this->load->model('staff','',true);
        $this->load->model('rugi','',true);
        $this->load->model('arus_stock_produk','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('retur','',true);
        $this->load->model('hutang','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Stock Control < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "inventori";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"produk_kode"));
        array_push($column, array("data"=>"produk_nama"));
        array_push($column, array("data"=>"jenis_produk_nama"));
        array_push($column, array("data"=>"satuan_nama"));
        array_push($column, array("data"=>"lokasi_nama"));
        array_push($column, array("data"=>"jumlah"));


        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['inventori']['quality-control'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/qulity-control/list');
        $this->load->view('admin/static/footer');
    }
    function list(){
        if(isset($_GET["columns"][5]["search"]["value"]) && $_GET["columns"][5]["search"]["value"] != ""){

            $_GET['lokasi_id'] = $_GET["columns"][5]["search"]["value"];
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->stock_produk->quality_control_all();
        $result['iTotalDisplayRecords'] = $this->stock_produk->quality_control_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->stock_produk->quality_control_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->produk_id;
            $rugi_url = $key->produk_id.'_'.$key->stock_produk_lokasi_id;
            $key->rugi_url = base_url().'quality-control/rugi-form/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($rugi_url));
            $key->retur_url = base_url().'quality-control/retur-form/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($rugi_url));

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function rugi_form(){

        $id_page = $this->uri->segment(3);
        $id_page = str_replace(array("-","_"), array("+","/"), $id_page);
        $id_page = $this->encryption->decrypt($id_page);
        $id_page = explode('_',$id_page);
        $produk_id = $id_page[0];
        $lokasi_id = $id_page[1];
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/quality_control.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;

        $column = array();
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"stock_produk_seri"));
        array_push($column, array("data"=>"hpp"));
        array_push($column, array("data"=>"stock_produk_qty"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $data['action'] = json_encode(array("chose"=>true));
        $data["meta_title"] = "Form Rugi < Inventori< Stock Control < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "inventori";
        $data['page'] = $this->uri->segment(1);
        $data['produk'] = $this->produk->row_by_id($produk_id);
        $data['lokasi'] = $this->lokasi->row_by_id($lokasi_id);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/qulity-control/rugi');
        $this->load->view('admin/static/footer');

    }
    function stock_list(){
        $produk_id = $this->uri->segment(3);
        $lokasi_id = $this->uri->segment(4);
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->stock_produk->quality_stock_all($produk_id,$lokasi_id);
        $result['iTotalDisplayRecords'] = $this->stock_produk->quality_stock_filter($query,$produk_id,$lokasi_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->stock_produk->quality_stock_list($start,$length,$query,$produk_id,$lokasi_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->hpp = $this->idr_currency($key->hpp);
            $key->stock_produk_qty = number_format($key->stock_produk_qty);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function rugi(){
        $result['success'] = false;
        $result['message'] = 'Gagal menyimpan data';
        $produk_id = $this->input->post('produk_id');
        $lokasi_id = $this->input->post('lokasi_id');
        $stock_produk_id = $this->input->post('stock_produk_lokasi_id');
        $harga = $this->input->post('hpp');
        $jumlah = $this->string_to_number($this->input->post('jumlah_kerugian'));
        $total_kerugian = $this->input->post('total_kerugian');
        $keterangan = $this->input->post('keterangan');
        $produk = $this->produk->row_by_id($produk_id);
        $lokasi = $this->lokasi->row_by_id($lokasi_id);
        $user_id = $_SESSION["redpos_login"]['user_id'];
        $user = $this->user->row_by_id($user_id);
        $staff = $this->staff->row_by_id($user->user_staff_id);
        $stock_data = $this->stock_produk->row_by_id($stock_produk_id);
        if($stock_produk_id!=""){
            $data = array();
            $data['stock_produk_id'] = $stock_produk_id;
            $data['produk_id'] = $produk_id;
            $data['lokasi_id'] = $lokasi_id;
            $data['harga'] = $harga;
            $data['jumlah'] = $jumlah;
            $data['total_kerugian'] = $total_kerugian;
            $data['keterangan'] = $keterangan;
            $data['produk_nama'] = $produk->produk_nama;
            $data['lokasi_nama'] = $lokasi->lokasi_nama;
            $data['staff_id'] = $_SESSION['redpos_login']['staff_id'];
            $data['staff_nama'] = $staff->staff_nama;

            $stock = array();
            $stock['stock_produk_qty'] = $stock_data->stock_produk_qty-$jumlah;

            $arus = array();
            $arus["tanggal"] = date("Y-m-d");
            $arus["table_name"] = "stock_produk";
            $arus["stock_produk_id"] = $stock_produk_id;
            $arus["produk_id"] = $produk_id;
            $arus["stock_out"] = $jumlah;
            $arus["stock_in"] = 0;
            $arus["keterangan"] = "Kerugian stok produk";
            $arus["method"] = "update";

            $this->stock_produk->start_trans();
            $this->rugi->insert($data);
            $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$stock);
            $arus["last_stock"] = $this->stock_produk->last_stock($produk_id)->result;
            $arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
            $this->arus_stock_produk->insert($arus);
            $process = $this->stock_produk->result_trans();
            if($process){
                $result['success'] = true;
                $result['message'] = 'Berhasil menyimpan data';
            }
        }
        echo json_encode($result);

    }
    function retur_form(){
        $id_page = $this->uri->segment(3);
        $id_page = str_replace(array("-","_"), array("+","/"), $id_page);
        $id_page = $this->encryption->decrypt($id_page);
        $id_page = explode('_',$id_page);
        $produk_id = $id_page[0];
        $lokasi_id = $id_page[1];
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/quality_control.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data["meta_title"] = "Form Retur < Inventori< Stock Control < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "inventori";
        $data['page'] = $this->uri->segment(1);
        $column = array();
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"suplier_nama"));
        array_push($column, array("data"=>"jumlah"));
        array_push($column, array("data"=>"harga"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $data['action'] = json_encode(array("chose"=>true));
        $data['produk'] = $this->produk->row_by_id($produk_id);
        $data['lokasi'] = $this->lokasi->row_by_id($lokasi_id);
        $data['stock_produk_lokasi'] = $this->stock_produk->stock_produk_lokasi_by_id($produk_id,$lokasi_id);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/qulity-control/retur');
        $this->load->view('admin/static/footer');
    }
    function po_list(){
        $produk_id = $this->uri->segment(3);
        $lokasi_id = $this->uri->segment(4);
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->po_quality_all($produk_id);
        $result['iTotalDisplayRecords'] = $this->po_produk->po_quality_filter($query,$produk_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->po_produk->po_quality_list($start,$length,$query,$produk_id);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->po_produk_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function retur(){
        $result['success'] = false;
        $result['message'] = 'Gagal menyimpan data';
        $produk_id = $this->input->post('produk_id');
        $lokasi_id = $this->input->post('lokasi_id');
        $po_produk_id = $this->input->post('input_po_id');
        $harga = $this->input->post('hpp');
        $jumlah = $this->input->post('jumlah_retur');
        $total_retur = $this->input->post('total_retur');
        $tanggal = $this->input->post('tanggal');
        $user_id = $_SESSION["redpos_login"]['user_id'];
        $user = $this->user->row_by_id($user_id);
        $data = array();
        $data["produk_id"] = $produk_id;
        $data["lokasi_id"] = $lokasi_id;
        $data["po_produk_id"] = $po_produk_id;
        $data["harga"] = $harga;
        $data["jumlah"] = $jumlah;
        $data["total_retur"] = $total_retur;
        $data["tanggal"] = $tanggal;
        $data["staff_id"] = $_SESSION['redpos_login']['staff_id'];;

        $this->stock_produk->start_trans();

        $this->retur->insert($data);
        $stock_po_location = $this->stock_produk->stock_produk_by_po_lokasi($po_produk_id,$lokasi_id);
        $row_stock = $this->stock_produk->stock_produk_by_po_lokasi_data($po_produk_id,$lokasi_id);
        $sisa = $jumlah;
        if($stock_po_location>0){

            $stock = array();
            $jumlah_stock = $row_stock->stock_produk_qty < $jumlah ? 0 : ($stock_po_location-$jumlah);

            $stock['stock_produk_qty'] = $jumlah_stock;
            $sisa = $row_stock->stock_produk_qty <$jumlah ? ($jumlah-$stock_po_location):0;
            $this->stock_produk->update_by_id('stock_produk_id',$row_stock->stock_produk_id,$stock);

            $arus = array();
            $arus["tanggal"] = date("Y-m-d");
            $arus["table_name"] = "stock_produk";
            $arus["stock_produk_id"] = $row_stock->stock_produk_id;
            $arus["produk_id"] = $produk_id;
            $arus["stock_out"] = $jumlah;
            $arus["stock_in"] = 0;
            $arus["last_stock"] = $this->stock_produk->last_stock($produk_id)->result;
            $arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
            $arus["keterangan"] = "Retur stok produk";
            $arus["method"] = "update";

            $this->arus_stock_produk->insert($arus);
        }
        $list_stock = $this->stock_produk->stock_by_location_produk_id($lokasi_id,$produk_id);
        foreach ($list_stock as $row){
            if($sisa>0){
                $data = array();
                $total = $row->stock_produk_qty - $sisa;
                $stock_out = $sisa;
                if($row->stock_produk_qty < $sisa){
                    $total = 0;
                    $stock_out = $row->stock_produk_qty;
                }
                $sisa =  ($sisa-$row->stock_produk_qty);
                $data["stock_produk_qty"] = $total;
                $stock_produk_id = $row->stock_produk_id;
                $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
                if($updateStok){
                    $data = array();
                    $data["tanggal"] = date("Y-m-d");
                    $data["table_name"] = "stock_produk";
                    $data["stock_produk_id"] = $row->stock_produk_id;
                    $data["produk_id"] = $produk_id;
                    $data["stock_out"] = $stock_out;
                    $data["stock_in"] = 0;
                    $data["last_stock"] = $this->stock_produk->last_stock($produk_id)->result;
                    $data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                    $data["keterangan"] = "Retur Produk";
                    $data["method"] = "update";
                    $this->stock_produk->arus_stock_produk($data);
                }
            }else {
                break;
            }
        }
        $po_produk = $this->po_produk->row_by_id($po_produk_id);
        $hutang = $this->hutang->hutang_by_po($po_produk_id);

        $hutang_data = array();
        $hutang_data["grand_total"] = $po_produk->grand_total - $total_retur;
        $this->hutang->update_by_id('hutang_id',$hutang->hutang_id,$hutang_data);
        $hutang = $this->hutang->hutang_by_po($po_produk_id);
        $process = $this->stock_produk->result_trans();
        if($process){
            $result['success'] = true;
            $result['message'] = 'Berhasil menyimpan data';
        }
        echo json_encode($result);


    }

}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */