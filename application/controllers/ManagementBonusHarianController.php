<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManagementBonusHarianController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bonus','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/admin/management-bonus.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Bonus < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-bonus";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"tanggal_proccess"));
        array_push($column, array("data"=>"no_generate"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array());
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array("download"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/management-bonus-harian/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->bonus->bonus_harian_all();
        $result['iTotalDisplayRecords'] = $this->bonus->bonus_harian_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->bonus->bonus_harian_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->row_id = $key->no_generate;
            $key->download_url = base_url()."manage-bonus/print?no_generate=".$key->no_generate;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
