<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManageMoneyBackController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');
        $this->load->model('money_back','',true);
        $this->load->model('reseller','',true);
        $this->load->model('produk','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/manage-money-back.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Manage Money Back Guarantee < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-money-back";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"tanggal_request"));
        array_push($column, array("data"=>"uang_dikembalikan"));
        array_push($column, array("data"=>"potongan"));
        array_push($column, array("data"=>"total_pengembalian"));
        array_push($column, array("data"=>"tanggal_approve"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"status_pengiriman","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['manage-money-back'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }

        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-money-back/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->money_back->request_all();
        $result['iTotalDisplayRecords'] = $this->money_back->request_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->money_back->request_list($start,$length,$query);

        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            if($key->status=="approve"){
                $key->pengiriman_btn = true;
                $key->deny_approve = true;
                $key->deny_refuse = true;
            }
            $key->uang_dikembalikan = number_format($key->uang_dikembalikan);
            $key->total_pengembalian = number_format($key->total_pengembalian);
            $key->potongan = $key->potongan."%";
            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);

    }
    function approve(){
        $money_back_guarantee_id = $this->input->post('id');
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data = array();
        $data["status"] = "approve";
        $data["tanggal_approve"] = date("Y-m-d");
        $status = $this->money_back->update_by_id('money_back_guarantee_id',$money_back_guarantee_id,$data);
        if($status){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
            $money_back_guarantee = $this->money_back->row_by_id($money_back_guarantee_id);
            $reseler_id = $money_back_guarantee->reseller_id;
            $data = array();
            $data['status'] = "banned";
            $this->reseller->update_by_id("reseller_id",$reseler_id,$data);
        }

        echo json_encode($result);
    }
    function penerimaan(){
        $money_back_guarantee_id = $this->input->post('money_back_guarantee_id');
        $money = $this->money_back->row_by_id($money_back_guarantee_id);
        $reseller_id = $money->reseller_id;
        $login = $this->db->where('reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
        $lokasi_id = $login->lokasi_id;
        $daftar_produk = $this->produk->produk_list_money_back("",$lokasi_id);
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        foreach ($daftar_produk as $item){
            $this->stock_produk->stock_money_back($gudang->lokasi_id,$item->produk_id,$item->jumlah_lokasi);
        }
        $this->stock_produk->delete_by_id('stock_produk_lokasi_id',$lokasi_id);
        $tanggal = $this->input->post('tanggal_penerimaan');
        $data = array();
        $data['tanggal_penerimaan'] = $tanggal;
        $data['status_pengiriman'] = "Terkirim";
        $status = $this->money_back->update_by_id('money_back_guarantee_id',$money_back_guarantee_id,$data);
        if($status){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function refuse(){
        $money_back_guarantee_id = $this->input->post('id');
        $money_back_guarantee = $this->money_back->row_by_id($money_back_guarantee_id);
        $reseller_id = $money_back_guarantee->reseller_id;
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $delete = $this->money_back->delete_by_id('money_back_guarantee_id',$money_back_guarantee_id);
        if($delete){
            $result['success'] = true;
            $result['message'] = "Berhasil Menyimpan data";
            $data['user_role_id'] = $money_back_guarantee->user_role_id;
            $this->db->where('reseller_id',$reseller_id);
            $this->db->update('mykindofbeauty_master.login',$data);
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function approve_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/manage-money-back.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Manage Money Back Guarantee < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-money-back";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"tanggal_request"));
        array_push($column, array("data"=>"uang_dikembalikan"));
        array_push($column, array("data"=>"potongan"));
        array_push($column, array("data"=>"total_pengembalian"));
        array_push($column, array("data"=>"tanggal_approve"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"status_pengiriman","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-money-back/index_approve');
        $this->load->view('admin/static/footer');
    }
    function approve_list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->money_back->approve_all();
        $result['iTotalDisplayRecords'] = $this->money_back->approve_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->money_back->approve_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            if($key->status=="approve"){
                $key->pengiriman_btn = true;
                $key->deny_approve = true;
                $key->deny_refuse = true;
            }
            $key->uang_dikembalikan = number_format($key->uang_dikembalikan);
            $key->total_pengembalian = number_format($key->total_pengembalian);
            $key->potongan = $key->potongan."%";
            $key->row_id = $key->bonus_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

