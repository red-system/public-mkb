<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PembayaranHutangProdukController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_produk','',true);
        $this->load->model('po_produk_detail','',true);
        $this->load->model('pengiriman_produk','',true);
        $this->load->model('pengiriman_produk_detail','',true);
        $this->load->model('pembayaran_hutang_produk','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('reseller','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('hutang_produk','',true);
        $this->load->model('piutang_produk','',true);
        $this->load->model('produk','',true);
        $this->load->model('bonus','',true);
        $this->load->model('refuse_pembayaran_piutang','',true);
        $this->load->model('master_login','',true);
        $this->load->helper('string');
        $this->load->library('main');
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['pembayaran-hutang'] as $key => $value) {
            if($key != "list" && $key != "akses_menu" && $key != "penerimaan"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-produk/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_all();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            $key->edit_url = base_url().'pembayaran-hutang-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->no = $i;
            $i++;
            $key->print_url = base_url()."pembayaran-hutang-produk/print/".$key->pembayaran_hutang_produk_id;
            $key->delete_url = base_url().'pembayaran-hutang-produk/delete/';
            $key->row_id = $key->pembayaran_hutang_produk_id;
            $key->action = null;
            
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['suplier'] = $this->suplier->all_list();
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_po();
        $data['po_produk_no'] = $this->po_produk->get_kode_po_produk();

        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-produk/add_pembayaran');
        $this->load->view('admin/static/footer');
        unset($_SESSION['po_produk']);
        $_SESSION['po_produk']['lokasi'] = 1;
    }
    function utility(){
        $key = $this->uri->segment(3);
        if($key == "get-po-no"){
            $code = $this->input->post('suplier_kode');
            echo $this->po_produk->get_kode_po_produk($code);
        }
        if($key=="list-reseller"){
            $this->list_reseller();
        }
        if($key=="list-produk"){
            $this->list_produk();
        }
        if($key=="sess-reseller"){
            $_SESSION['po_produk']["reseller_id"] = $this->input->post('reseller_id');
            echo json_encode($_SESSION['po_produk']);
        }
        if($key=="sess-produk-add"){
            $no = $this->input->post('key');
            $produk_id = $this->input->post('produk_id');
            $_SESSION['po_produk']['produk']['con_'.$no]['produk_id'] = $produk_id;
            $_SESSION['po_produk']['produk']['con_'.$no]['value'] = 0;
        }
        if($key=="sess-produk-change"){
            $jumlah = $this->input->post('jumlah');
            $no = $this->input->post('key');
            if(isset($_SESSION['po_produk']['produk']['con_'.$no]['produk_id'])){
                $_SESSION['po_produk']['produk']['con_'.$no]['value'] = $jumlah;
            }
        }
        if($key=="sess-produk-delete"){
            $no = $this->input->post('key');
            unset($_SESSION['po_produk']['produk']['con_'.$no]);
        }
        if($key=="sess-produk-reset"){
            unset($_SESSION['po_produk']['produk']);
        }
        if($key=="sess-lokasi"){
            $_SESSION['po_produk']['lokasi'] = $this->input->post('lokasi_id');
        }
        if($key=="list-hutang"){
            $this->list_hutang();
        }
    }
    function list_reseller(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "active";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function list_hutang(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_hutang_all();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_hutang_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_hutang_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pelunasan != null){
                $time = strtotime($key->tanggal_pelunasan);
                $key->tanggal_pelunasan = date('d-m-Y',$time);
            }

            $key->row_id = $key->hutang_produk_id;
            $key->jumlah = number_format($key->jumlah);
            $key->no = $i;
            $key->action = null;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function list_produk(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->produk->produk_count_all();
        $result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk->produk_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'produk/delete/';
            $key->action =null;
            $key->last_hpp = $this->stock_produk->last_hpp($key->produk_id);
            $key->last_hpp = $key->last_hpp == 0 ? $key->hpp_global : $key->last_hpp;
            $key->row_id = $key->produk_id;
            $key->produk_minimal_stock = number_format($key->produk_minimal_stock);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function save_add(){
        $input = $this->input->post();
        $tanggal = $this->input->post('tanggal');
        $prefix = "PP-".date("Ymd")."/";
        $hutang_produk_id = $this->input->post('hutang_produk_id');
        $pembayaran = $this->pembayaran_hutang_produk->getPembayaranNo($prefix);
        if($pembayaran==null){
            $no_pembayaran = $prefix."1";
        }else{
            $arUrutan = explode($prefix,$pembayaran->no_pembayaran);
            $no_pembayaran = $prefix.($arUrutan[1]+1);
        }
        $pembayaran_hutang = array();
        $pembayaran_hutang["tanggal"] = date("Y-m-d");
        $pembayaran_hutang["no_pembayaran"] = $no_pembayaran;
        $insert = $this->pembayaran_hutang_produk->insert($pembayaran_hutang);
        $pembayaran_hutang_produk_id = $this->pembayaran_hutang_produk->last_id();
        $pembayaran = $this->hutang_produk->pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id);
        $reseller_id = null;
        $lokasi_id = null;
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        foreach ($hutang_produk_id as $item){
            $hutang = $this->hutang_produk->row_by_id($item);
            if($reseller_id==null){
                $reseller_id = $hutang->reseller_id;
                $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
                $lokasi_id = $login->lokasi_id;
            }
            $produk_id = $hutang->produk_id;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty + $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty - $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

        }
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        echo json_encode($result);

    }
    function edit(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $pembayaran_hutang_produk = $this->pembayaran_hutang_produk->row_by_id($id);
        unset($_SESSION['po_produk']);
        if ($pembayaran_hutang_produk != null) {
            $pembayaran_hutang_produk_detail  = $this->pembayaran_hutang_produk->pembayaran_hutang_produk_detail($id);
            $data["pembayaran_hutang_produk_detail"] = $pembayaran_hutang_produk_detail;
            $data["size"] = sizeof($pembayaran_hutang_produk_detail);
            $_SESSION['po_produk']["reseller_id"] = $pembayaran_hutang_produk_detail[0]->reseller_id;
            $_SESSION['po_produk']["pembayaran_hutang_produk"] = $id;
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "script/admin/pem_hutang_produk.js");
            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
            $data['parrent'] = "hutang_piutang";
            $data['page'] = "pembayaran-hutang";
            $data['lokasi'] = $this->lokasi->all_list();
            $data['suplier'] = $this->suplier->all_list();
            $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_po();
            $data['po_produk_no'] = $this->po_produk->get_kode_po_produk();

            $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
            $reseller_id = $login->reseller_id;
            $reseller = $this->reseller->row_by_id($reseller_id);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/pembayaran-hutang-produk/edit_pembayaran');
            $this->load->view('admin/static/footer');
        }else {
            redirect('404_override','refresh');
        }
    }
    function save_edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $input = $this->input->post();
        $tanggal = $this->input->post('tanggal');
        $prefix = "PP-".date("Ymd")."/";
        $hutang_produk_id = $this->input->post('hutang_produk_id');
        $pembayaran = $this->pembayaran_hutang_produk->getPembayaranNo($prefix);

        if($pembayaran==null){
            $no_pembayaran = $prefix."1";
        }else{
            $arUrutan = explode($prefix,$pembayaran->no_pembayaran);
            $no_pembayaran = $prefix.($arUrutan[1]+1);
        }
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $pembayaran_hutang = array();
        $pembayaran_hutang["tanggal"] = date("Y-m-d");
        $pembayaran_hutang["no_pembayaran"] = $no_pembayaran;
        $insert = $this->pembayaran_hutang_produk->update_by_id("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id,$pembayaran_hutang);
        $this->pembayaran_hutang_produk->penarikan_stock($pembayaran_hutang_produk_id);
        $pembayaran = $this->hutang_produk->pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id);
        $pembayaran = $this->piutang_produk->pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id);
        $reseller_id = null;
        $lokasi_id = null;
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        foreach ($hutang_produk_id as $item){
            $hutang = $this->hutang_produk->row_by_id($item);
            if($reseller_id==null){
                $reseller_id = $hutang->reseller_id;
                $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
                $lokasi_id = $login->lokasi_id;
            }
            $produk_id = $hutang->produk_id;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty + $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty - $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);

        }
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $this->pembayaran_hutang_produk->penarikan_stock($id);
            $delete = $this->pembayaran_hutang_produk->delete_by_id("pembayaran_hutang_produk_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function detail(){

        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $pembayaran_hutang_produk = $this->pembayaran_hutang_produk->row_by_id($pembayaran_hutang_produk_id);

        $item = $this->pembayaran_hutang_produk->pembayaran_hutang_produk_detail($pembayaran_hutang_produk_id);
        if($item==null){
            $refuse = $this->refuse_pembayaran_piutang->row_by_field('pembayaran_hutang_produk_id',$pembayaran_hutang_produk_id);
            $json = $refuse->json;
            $item = json_decode($json);
        }
        foreach ($item as $val){
            $val->jumlah = number_format($val->jumlah);
        }
        $pembayaran_hutang_produk->item = $item;
        echo json_encode($pembayaran_hutang_produk);
    }
    function penerimaan_po_produk(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $po_produk_id = $this->input->post('po_produk_id');
        $data["status_penerimaan"] = "Diterima";
        $data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
        $lokasi_id = $this->input->post('lokasi_penerimaan_id');
        $data['lokasi_penerimaan_id'] = $lokasi_id;
        $this->po_produk->start_trans();
        $update_po_produk = $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$data);
        if($update_po_produk){
            $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
            foreach ($po_produk_item as $key) {
                $data = array();
                $data["stock_produk_qty"] = $key->jumlah;
                $data["produk_id"] = $key->produk_id;
                $data["stock_produk_lokasi_id"] = $lokasi_id;
                $data["year"] = date("y");
                $data["month"] = date("m");
                $data["po_id"] = $po_produk_id;
                $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                $data["hpp"] = $this->string_to_number($key->harga);
                $insert = $this->stock_produk->insert($data);
                $arus["stock_produk_id"] = $this->stock_produk->last_id();
                $arus["method"] = "insert";
                $arus["tanggal"] = date("Y-m-d");
                $arus["table_name"] = "stock_produk";
                $arus["produk_id"] = $key->produk_id;
                $arus["stock_out"] = 0;
                $arus["stock_in"] = $key->jumlah;
                $arus["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
                $arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
                $arus["keterangan"] = "Order Produk";
                $this->stock_produk->arus_stock_produk($arus);
            }
        }
        if($this->po_produk->result_trans()){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function history(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/po_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "History Order Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_produk";
        $data['page'] = "order_produk";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"tipe_pembayaran_nama"));
        array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"keterangan_tolak"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
        $data['lokasi'] = $this->lokasi->all_list();
        $action = array("view"=>true,"print"=>true);
        $data['action'] = json_encode($action);
        $data["history"] = "true";
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po_produk');
        $this->load->view('admin/static/footer');
    }
    function history_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_produk->po_produk_count_all_history();
        $result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_history($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_produk->po_produk_list_history($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);

            $key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'po-produk/delete/';
            $key->print_url = base_url()."po-produk/print/".$key->po_produk_id;
            $key->row_id = $key->po_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function print(){
        $id = $this->uri->segment(3);
        $temp = $this->po_produk->po_produk_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->po_produk->po_produk_detail_by_id($id);
        $temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
        if($temp->tanggal_pemesanan != null){
            $x = strtotime($temp->tanggal_pemesanan);
            $temp->tanggal_pemesanan = date("Y-m-d",$x);
        }
        if($temp->tanggal_penerimaan != null){
            $x = strtotime($temp->tanggal_penerimaan);
            $temp->tanggal_penerimaan = date("Y-m-d",$x);
        }
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga);
            $key->jumlah = number_format($key->jumlah);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        $data['po_produk'] = $temp;
        $this->load->view('admin/po-produk/print',$data);
    }
    function uploadImage($file,$url){
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $date = new DateTime();
        $file_name = $date->getTimestamp().random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/".$file_name.".".$ext, $_FILES[$file]['tmp_name'],NET_SFTP_LOCAL_FILE);
        if (!$output)
        {
            $url = "failed";
            return $url;
        }
        else
        {
            $url = $file_name.'.'.$ext;
        }
        return $url;
    }
    function approve(){
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $approve = $this->pembayaran_hutang_produk->approve($pembayaran_hutang_produk_id);
        $hutang_produk_id = $this->hutang_produk->hutang_produk_ids($pembayaran_hutang_produk_id);
        $this->hutang_produk->pembayaran_hutang_produk($pembayaran_hutang_produk_id);
        $reseller_id = null;
        $lokasi_id = null;
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];

        $paramBonus = "16:00:00";
        $estimasiBonus = $this->main->tanggal_kerja($paramBonus);
        $grand_total = 0;
        foreach ($hutang_produk_id as $item){
            $hutang_id = $item->hutang_produk_id;
            $hutang = $this->hutang_produk->row_by_id($hutang_id);
            $reseller_id = $hutang->reseller_id;
            $login = $this->db->where('mykindofbeauty_master.login.reseller_id',$reseller_id)->get('mykindofbeauty_master.login')->row();
            $lokasi_id = $login->lokasi_id;
            $produk_id = $hutang->produk_id;
            $produk = $this->produk->row_by_id($produk_id);
            $total = $produk->hpp_global * $hutang->jumlah;
            $grand_total +=$total;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty + $hutang->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("mykindofbeauty_kemiri.stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty - $hutang->jumlah;
            // $stock_update = array();
            // $stock_update["stock_produk_qty"] = $jumlah;
            // $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            // $this->db->update("mykindofbeauty_kemiri.stock_produk",$stock_update);
        }
        $reseller = $this->reseller->row_by_id($reseller_id);
        $referal_id = $reseller->referal_id;
        if($referal_id!=null){
            $referal = $this->reseller->row_by_id($referal_id);
            $data_referal = $this->master_login->row_by_field('reseller_id', $referal_id);
            $username = $data_referal->username;
            $referal_type = $referal->type;
            $withdraw_penerima = $referal->sisa_deposit_withdraw;
            if($referal_type=="super agen"){
                if($this->bonus->cekBonusWithdraw($pembayaran_hutang_produk_id,$referal_id,"loyalty bonus")) {
                    $login_induk = $this->db->where("reseller_id",$referal_id)->get("mykindofbeauty_master.login")->row();
                    $lokasi_induk = $login_induk->lokasi_id;
                    $deposit_induk = $this->po_produk->total_deposit_reseller($lokasi_induk);
                    $deposit_induk = $deposit_induk == "" ? 0 : $deposit_induk;
                    $minimal_po_induk = $this->po_produk->minimal_po_cek($referal_id);
                    $status_bonus_reseller_loyal = (($deposit_induk < $minimal_po_induk)||($withdraw_penerima>0)) ? "Hold" : "On Process";
                    $bonus = array();
                    $bonus["tanggal"] = $estimasiBonus;
                    $bonus["from_reseller_id"] = $reseller->reseller_id;
                    $bonus["type"] = "loyalty bonus";
                    $bonus["jumlah_deposit"] = $grand_total;
                    $bonus["persentase"] = 5;
                    $bonus["jumlah_bonus"] = $grand_total * 0.05;
                    $bonus["to_reseller_id"] = $referal_id;
                    $bonus["pembayaran_hutang_produk_id"] = $pembayaran_hutang_produk_id;
                    $bonus["status"] = $status_bonus_reseller_loyal;

                    $kelebihan = $referal->kelebihan;
                    $potongan = 0;
                    $text_kelebihan = '';
                    if($kelebihan>0){

                        if($kelebihan<$bonus["jumlah_bonus"]){
                            $bonus["potongan"] = $kelebihan;
                            $potongan = $kelebihan;
                            $bonus["jumlah_bonus"] = $bonus["jumlah_bonus"] - $kelebihan;
                            $kelebihan = 0;
                        }else{
                            $bonus["potongan"] = $bonus["jumlah_bonus"];
                            $potongan = $bonus["jumlah_bonus"];
                            $kelebihan = $kelebihan - $bonus["jumlah_bonus"];
                            $bonus["jumlah_bonus"] = 0;

                        }
                        $total_bonus = $bonus["jumlah_bonus"];
                        $text_kelebihan = '<tr style="height: 25px">
                                                            <td style="text-align: left">Potongan </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($potongan). ' </td>
                                                        </tr>
                                                        <tr style="height: 25px">
                                                            <td style="text-align: left">Total Bonus Didapat </td>
                                                            <td>:</td>
                                                            <td style="text-align: right">' . number_format($total_bonus). ' </td>
                                                        </tr>';
                    }
                    $this->bonus->insert($bonus);
                    $dataKelebihan['kelebihan'] = $kelebihan;
                    $this->reseller->update_by_id('reseller_id',$referal->reseller_id,$dataKelebihan);

                    $status_message = "Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Jam Kerja Kantor)";
                    if ($deposit_induk < $minimal_po_induk){
                        $status_message = "Bonus anda akan di hold karena anda belum memenuhi minimal deposit. Silakan lakukan deposit untuk mencairkan bonus.";
                    }
                    $referal_link  = $this->config->item('url-landing').$referal->referal_code;
                    $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' . $referal->nama . ' ('.$username.') ,</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS LOYALTY yang akan langsung di transfer ke rekening MKB Anda.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table width="100%">
                                <tbody>
                                    <tr style="height: 25px">
                                        <td style="text-align: left;width:25%">Pemilik Voucher </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . $reseller->nama . ' </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left;width:25%">Total Nilai Withdraw </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($grand_total) . '</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left;width:25%">Persentase </td>
                                        <td>:</td>
                                        <td style="text-align: right">5%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="text-align: left;width:25%">Jumlah Bonus </td>
                                        <td>:</td>
                                        <td style="text-align: right">' . number_format($grand_total * 0.05) . '</td>
                                    </tr>
                                    '.$text_kelebihan.'
                                </tbody>
                            </table>
                        </div>
                        <span>
                           '.$status_message.'
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                        </span>
                        <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                        <span style="margin-top: 20px">
                            Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                        </span>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                         <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
                    $this->main->mailer_auth('Bonus Loyalty', $referal->email, $referal->nama, $mailContentAdmin);
                }
            }
        }
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";

        echo json_encode($result);
    }
    function refuse(){
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_id");
        $hutang_produks = $this->hutang_produk->hutang_produk_ids($pembayaran_hutang_produk_id);
        $reseller_id = $hutang_produks[0]->reseller_id;
        $json = json_encode($hutang_produks);
        $this->pembayaran_hutang_produk->setPembayaranNull($pembayaran_hutang_produk_id);
        $data = array();
        $data["status"] = "refuse";
        $this->pembayaran_hutang_produk->update_by_id('pembayaran_hutang_produk_id',$pembayaran_hutang_produk_id,$data);
        $refuse = array();
        $message = $this->input->post("alasan_refuse");
        $refuse["pembayaran_hutang_produk_id"] = $pembayaran_hutang_produk_id;
        $refuse["message"] = $message;
        $refuse["json"] = $json;
        $refuse["reseller_id"] = $reseller_id;
        $this->refuse_pembayaran_piutang->insert($refuse);
        $result['success'] = true;
        $result['message'] = "Data berhasil disimpan";
        echo json_encode($result);
    }
    function approve_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));
        array_push($column, array("data"=>"status_penerimaan_lbl"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['pembayaran-hutang'] as $key => $value) {
            if($key == "view" || $key == "penerimaan"){
                $action[$key] = $value;
            }
        }
        
        $data["history"] = "";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-produk/index_approve');
        $this->load->view('admin/static/footer');
    }
    function approve_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_all_approve();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_filter_approve($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_list_approve($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            if ($key->status_penerimaan == "Diterima") {
                $key->status_penerimaan_lbl = '<span class="badge badge-success">'.$key->status_penerimaan.'</span>';
            }elseif ($key->status_penerimaan == "Sebagian") {
                $key->status_penerimaan_lbl = '<span class="badge badge-warning">'.$key->status_penerimaan.'</span>';
            }else{
                $key->status_penerimaan_lbl = '<span class="badge badge-danger">'.$key->status_penerimaan.'</span>';
            }
            
            $key->edit_url = base_url().'pembayaran-hutang-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->no = $i;
            $i++;
            $key->print_url = base_url()."pembayaran-hutang-produk/print/".$key->pembayaran_hutang_produk_id;
            $key->delete_url = base_url().'pembayaran-hutang-produk/delete/';
            $key->row_id = $key->pembayaran_hutang_produk_id;
            $key->enc_row_id = str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->action = null;

            if($key->status_penerimaan == "Diterima"){
                $key->deny_penerimaan = true;
            }

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function refuse_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pem_hutang_produk.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "pembayaran-hutang";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"tanggal"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['pembayaran-hutang'] as $key => $value) {
            if($key == "view"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['lokasi'] = $this->lokasi->all_list();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/pembayaran-hutang-produk/index_refuse');
        $this->load->view('admin/static/footer');
    }
    function refuse_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pembayaran_hutang_produk->_all_refuse();
        $result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->_filter_refuse($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pembayaran_hutang_produk->_list_refuse($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal != null){
                $time = strtotime($key->tanggal);
                $key->tanggal = date('d-m-Y',$time);
            }

            $key->edit_url = base_url().'pembayaran-hutang-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->pembayaran_hutang_produk_id));
            $key->no = $i;
            $i++;
            $key->print_url = base_url()."pembayaran-hutang-produk/print/".$key->pembayaran_hutang_produk_id;
            $key->delete_url = base_url().'pembayaran-hutang-produk/delete/';
            $key->row_id = $key->pembayaran_hutang_produk_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pengiriman_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/pengiriman_produk_hutang.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Hutang Produk < Pengiriman < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = 'pengiriman';
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $pengiriman = $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_id($id);
        $data['id'] = $id;
        if ($id != null) {
            $data['pengiriman'] = $pengiriman;
            array_push($column, array("data"=>"no"));
            array_push($column, array("data"=>"tanggal_pengiriman"));
            array_push($column, array("data"=>"total_qty_pengiriman"));
            array_push($column, array("data"=>"status_kirim_lbl"));
            array_push($column, array("data"=>"keterangan_pengiriman"));
            $data['sumColumn'] = json_encode(array(2));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className"=>"text__center","targets"=>array(0,2)));
            $data["action"] = json_encode(array("view"=>false, "edit"=>true,"delete"=>true, "submit" => true), true);

            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/pembayaran-hutang-produk/index_pengiriman');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function pengiriman_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->pengiriman_produk->pengiriman_by_hutang_produk_count($this->uri->segment(4));
        $result['iTotalDisplayRecords'] = $this->pengiriman_produk->pengiriman_by_hutang_produk_filter($this->uri->segment(4),$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->pengiriman_produk->pengiriman_by_hutang_produk_list($start,$length,$query,$this->uri->segment(4));
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pengiriman != null){
                $time = strtotime($key->tanggal_pengiriman);
                $key->tanggal_pengiriman = date('d-m-Y',$time);
            }
            $key->no = $i;
            $key->delete_url = base_url().'pembayaran-hutang-produk/pengiriman/delete/'.$key->pengiriman_produk_id;
            // $key->submit_url = base_url().'order-super-agen/penerimaan/accept-stock/'.$key->pengiriman_id;
            $key->row_id = $key->pengiriman_produk_id;
            $key->total_qty_pengiriman = number_format($key->total_qty_pengiriman);
            $key->status_kirim_lbl = 'Belum submit';
            if ($key->status_pengiriman == 1) {
                $key->deny_edit = 'true';
                $key->deny_delete = 'true';
                $key->deny_submit = 'true';
                $key->status_kirim_lbl = 'Submit';
            }
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail_pengiriman(){
        $pembayaran_hutang_id = $this->uri->segment(4);
        $data["result"] = $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_id($pembayaran_hutang_id);
        echo json_encode($data);
    }

    function add_pengiriman(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['pembayaran_hutang_produk_id'] = $this->string_to_number($this->input->post('pembayaran_hutang_produk_id'));
        $data['keterangan_pengiriman'] = $this->input->post('keterangan');
        $data['total_qty_pengiriman'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal_pengiriman'] = date("Y-m-d",$temp);
        $data['status_pengiriman'] = 0;
        $sisa = $this->string_to_number($this->input->post('sisa'));
        $produk_id = $this->string_to_number($this->input->post('produk_id'));

        $last_stock = $this->stock_produk->last_stock_gudang($produk_id)->result;

        if($sisa >= $data['total_qty_pengiriman']){
            if ($last_stock >= $data['total_qty_pengiriman']) {
                $insert = $this->pengiriman_produk->insert($data);
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }else{
                $result['message'] = "Pengiriman melebihi sisa stock gudang";
            }
        } else {
            $result['message'] = "Pengiriman melebihi sisa order";
        }
        echo json_encode($result);
    }
    function edit_pengiriman(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['keterangan_pengiriman'] = $this->input->post('keterangan_pengiriman');
        $pembayaran_hutang_produk_id = $this->string_to_number($this->input->post('pembayaran_hutang_produk_id'));
        $data['total_qty_pengiriman'] = $this->string_to_number($this->input->post('total_qty_pengiriman'));
        $temp = strtotime($this->input->post('tanggal_pengiriman'));
        $data['tanggal_pengiriman'] = date("Y-m-d",$temp);
        $pengiriman_produk_id = $this->input->post('pengiriman_produk_id');
        $jumlah_order = $this->string_to_number($this->input->post('jumlah_order'));
        $sisa = $this->string_to_number($this->input->post('sisa'));
        // $terkirim = $this->string_to_number($this->input->post('terkirim'));
        // $old_jumlah = $this->string_to_number($this->input->post('old_jumlah'));
        // $new_terkirim = $terkirim - $old_jumlah + $data['total_qty_penerimaan'];

        $produk_id = $this->string_to_number($this->input->post('produk_id'));

        $last_stock = $this->stock_produk->last_stock_gudang($produk_id)->result;

        if($data['total_qty_pengiriman'] <= $sisa){
            if ($last_stock >= $data['total_qty_pengiriman']) {
                $edit = $this->pengiriman_produk->update_by_id('pengiriman_produk_id',$pengiriman_produk_id,$data);
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }else{
                $result['message'] = "Pengiriman melebihi sisa stock gudang";
            }
        } else {
            $result['message'] = "Pembayaran melebihi sisa pengiriman";
        }
        echo json_encode($result);
    }
    function delete_pengiriman(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->pengiriman_produk->delete_pengiriman($id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

    function accept_stock(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";

        $pengiriman_produk_id = $this->input->post("id");

        $this->pembayaran_hutang_produk->start_trans();

        // get data pengiriman
        $data_pengiriman = $this->pengiriman_produk->detail_pengiriman($pengiriman_produk_id);
        $pembayaran_hutang_produk_id = $data_pengiriman->pembayaran_hutang_produk_id;
        $total_qty_pengiriman = $data_pengiriman->total_qty_pengiriman;

        // update pengiriman status
        $pengiriman['status_pengiriman'] = 1;
        $this->pengiriman_produk->update_by_id('pengiriman_produk_id',$pengiriman_produk_id,$pengiriman);

        // get data detail PO dari pembayaran hutang produk
        $detail_po = $this->pengiriman_produk->get_produk_detail_by_hutang($pembayaran_hutang_produk_id);

        // 100
        $jml_kirim = $total_qty_pengiriman;

        foreach ($detail_po as $key) {

            $kirim['pengiriman_produk_id'] = $pengiriman_produk_id;
            $kirim['po_produk_detail_id'] = $key->po_produk_detail_id;
            $po_produk_id = $key->po_produk_id;
            $produk_id = $key->produk_id;
            $sisa_qty = $key->sisa_qty;
            $no_pembayaran = $key->no_pembayaran;
            $reseller_nama = $key->reseller_nama;

            $gudang = $this->lokasi->all_gudang();
            $gudang = $gudang[0];

            $po_data = $this->po_produk->row_by_id($po_produk_id);
            
            // 50<=100
            if ($sisa_qty <= $jml_kirim) {
                // 100 = 100 - 50
                $jml_kirim = $jml_kirim - $sisa_qty;
                $kirim['qty_pengiriman'] = $sisa_qty;
                
                $insert_kirim = $this->pengiriman_produk_detail->insert($kirim);

                $po_detail['sisa_qty'] = 0;
                $this->po_produk_detail->update_by_id('po_produk_detail_id',$key->po_produk_detail_id,$po_detail);

                // $stock_qty = $kirim['qty_pengiriman'];

                //cari total qty gudang
                $last_stock_produk = $this->stock_produk->last_stock_gudang($produk_id)->result; //4600

                // update semua row stock qty = 0
                $update_empty_stock = $this->db->query("UPDATE mykindofbeauty_kemiri.stock_produk SET stock_produk_qty = 0  WHERE stock_produk_lokasi_id = $gudang->lokasi_id AND  produk_id = $produk_id");

                // cari stock id gudang terakhir 
                $stock_produk_gudang_id = $this->stock_produk->get_last_stock_id_gudang($produk_id)->stock_produk_id;

                $stock_new['stock_produk_qty'] = $last_stock_produk - $sisa_qty; //4600-50

                // update stock id gudang terakhir ke total stock
                $update_last_stock_produk = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_gudang_id,$stock_new);

                if($update_last_stock_produk){
                    $arus = array();
                    $arus["tanggal"] = date("Y-m-d");
                    $arus["table_name"] = "stock_produk";
                    $arus["stock_produk_id"] = $stock_produk_gudang_id;
                    $arus["produk_id"] = $produk_id;
                    $arus["stock_out"] = $sisa_qty;
                    $arus["stock_in"] = 0;
                    $arus["last_stock"] = $this->stock_produk->last_stock_gudang($produk_id)->result;
                    // $arus["last_stock"] =  $stock_new['stock_produk_qty'];
                    $arus["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                    // $arus["last_stock_total"] =  $stock_new['stock_produk_qty'];
                    $arus["keterangan"] = "Pembayaran Hutang Produk No. : ".$no_pembayaran. " a/n ". $reseller_nama;
                    $arus["method"] = "update";
                    $this->stock_produk->arus_stock_produk($arus);

                    $data = array();
                    $data["stock_produk_qty"] = $sisa_qty;
                    $data["produk_id"] = $produk_id;
                    $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
                    $data["year"] = date("y");
                    $data["month"] = date("m");
                    $data["po_id"] = $po_produk_id;
                    $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                    $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                    $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                    $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                    $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                    $data["hpp"] = $this->string_to_number($key->harga);
                    $insert = $this->stock_produk->insert($data);
                }

            }else{

                if ($jml_kirim > 0) {
                    
                    $qty_pengiriman = $sisa_qty - $jml_kirim;
                    $kirim['qty_pengiriman'] = $jml_kirim; 
                    
                    $insert_kirim = $this->pengiriman_produk_detail->insert($kirim);

                    $po_detail['sisa_qty'] = $qty_pengiriman;
                    $this->po_produk_detail->update_by_id('po_produk_detail_id',$key->po_produk_detail_id,$po_detail);

                    // $stock_qty = $kirim['qty_pengiriman'];

                    //cari total qty gudang
                    $last_stock_produk = $this->stock_produk->last_stock_gudang($produk_id)->result;

                    // update semua row stock qty = 0
                    $update_empty_stock = $this->db->query("UPDATE mykindofbeauty_kemiri.stock_produk SET stock_produk_qty = 0  WHERE stock_produk_lokasi_id = $gudang->lokasi_id AND  produk_id = $produk_id");

                    // cari stock id gudang terakhir 
                    $stock_produk_gudang_id = $this->stock_produk->get_last_stock_id_gudang($produk_id)->stock_produk_id;

                    $stock_new['stock_produk_qty'] = $last_stock_produk - $jml_kirim;

                    // update stock id gudang terakhir ke total stock
                    $update_last_stock_produk = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_gudang_id,$stock_new);

                    if($update_last_stock_produk){
                        $arus = array();
                        $arus["tanggal"] = date("Y-m-d");
                        $arus["table_name"] = "stock_produk";
                        $arus["stock_produk_id"] = $stock_produk_gudang_id;
                        $arus["produk_id"] = $produk_id;
                        $arus["stock_out"] = $jml_kirim;
                        $arus["stock_in"] = 0;
                        $arus["last_stock"] = $this->stock_produk->last_stock_gudang($produk_id)->result;
                        // $arus["last_stock"] =  $stock_new['stock_produk_qty'];
                        $arus["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                        // $arus["last_stock_total"] =  $stock_new['stock_produk_qty'];
                        $arus["keterangan"] = "Pembayaran Hutang Produk No. : ".$no_pembayaran. " a/n ". $reseller_nama;
                        $arus["method"] = "update";
                        $this->stock_produk->arus_stock_produk($arus);

                        $data = array();
                        $data["stock_produk_qty"] = $sisa_qty;
                        $data["produk_id"] = $produk_id;
                        $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
                        $data["year"] = date("y");
                        $data["month"] = date("m");
                        $data["po_id"] = $po_produk_id;
                        $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                        $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                        $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                        $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                        $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                        $data["hpp"] = $this->string_to_number($key->harga);
                        $insert = $this->stock_produk->insert($data);

                    }

                    $jml_kirim = 0;
                }
                
            }

        }

        $php_by_hutang = $this->pengiriman_produk->mail_by_hutang_id($pembayaran_hutang_produk_id);

        $data_user = $this->master_login->row_by_field('reseller_id', $php_by_hutang->reseller_id);
        $username = $data_user->username;

        $reseller = $this->reseller->row_by_id($php_by_hutang->reseller_id);
        $referal_link  = $this->config->item('url-landing').$reseller->referal_code;

        $mailContentAdmin = '<html><head>
            <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
            <style>
                body{
                    font-family: \'Open Sans\', sans-serif;
                }

                .content {
                    max-width: 550px;
                    margin: auto;
                }
                .title{
                    width: 60%;
                }
                .data,.data th,.data td {
                    border: 1px solid black;
                }
                a{
                    color: #990000;
                }
                a:hover{
                    color: #990000;
                }
                .btn {
                    display: inline-block;
                    font-weight: normal;
                    color: #212529;
                    text-align: center;
                    vertical-align: middle;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                    background-color: transparent;
                    border: 1px solid transparent;
                    padding: 0.65rem 1rem;
                    font-size: 1rem;
                    cursor: pointer;
                    line-height: 1.5;
                    border-radius: 0.25rem;
                    -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                }
                .btn {
                    background: transparent;
                    outline: none !important;
                    vertical-align: middle;
                }
                .btn-success {
                    color: #fff;
                    background-color: #0abb87;
                    border-color: #0abb87;
                }
                .btn.btn-pill {
                    border-radius: 2rem;
                    padding-right: 40px;
                    padding-left: 40px;
                    font-size: 12px;
                }
            </style>
        </head>
        <body style="background-color: #fff">
        <div class="content" style="background-color: #fff">
            <div>
                <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

                    <tbody>
                    <tr>
                        <td style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <div style="width: 450px; height: 550px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                                <h4 style="text-align: left;font-weight: 300;">Halo ' . $reseller->nama . ' ('.$username.') ,</h4>
                                <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                                <span>Produk yang anda pesan sudah dikirimkan
                                </span>
                                <br>
                                <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                                    <table width="100%">
                                        <tbody>
                                            <tr style="height: 25px">
                                                <td style="text-align: left;width:25%">No Pembayaran Hutang Produk </td>
                                                <td>:</td>
                                                <td style="text-align: right">' . $php_by_hutang->no_pembayaran . ' </td>
                                            </tr>
                                            <tr style="height: 25px">
                                                <td style="text-align: left;width:25%">Jumlah Produk Dipesan </td>
                                                <td>:</td>
                                                <td style="text-align: right">' . number_format($php_by_hutang->jumlah_order) . ' botol</td>
                                            </tr>
                                            <tr style="height: 25px">
                                                <td style="text-align: left;width:25%">Jumlah Dikirim </td>
                                                <td>:</td>
                                                <td style="text-align: right">' . number_format($total_qty_pengiriman) . ' botol</td>
                                            </tr>
                                            <tr style="height: 25px">
                                                <td style="text-align: left;width:25%">Sisa </td>
                                                <td>:</td>
                                                <td style="text-align: right">' . number_format($php_by_hutang->sisa_order) . ' botol</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <br>
                                <span style="margin-top: 20px">
                                    Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                                    mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id</span>
                                </span>
                                <div style="height: 10px;border-bottom: 1px solid #000;margin-bottom: 10px"></div>
                                <span style="margin-top: 20px">
                                    Ajak temanmu bergabung dengan menggunakan link referral ini untuk mendapatkan bonus : <a href="'.$referal_link.'">'.$referal_link.'</a>
                                </span>
                                <div style="margin-top: 40px"></div>
                                <span >
                                    Terima Kasih,
                                </span>
                                <div style="margin-top: 25px"></div>
                                <span >
                                    Tim My Kind Of Beauty
                                </span>
                            </div>
                            <div style="margin-top: 20px;text-align: center">
                                <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                            </div>
                            <div style="margin-top: 10px;text-align: center">
                                <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                            </div>

                        </td>

                    </tr>
                    </tbody></table>
                <p>&nbsp;<br></p>
            </div>

        </div>

        </body></html>';
        $this->main->mailer_auth('Pengiriman Pembayaran Hutang Produk', $reseller->email, $reseller->nama, $mailContentAdmin);

        // cek detail untuk update status
        $cek_sisa = $this->pengiriman_produk->count_total_sisa($pembayaran_hutang_produk_id);
        if ($cek_sisa->sisa >0) {
            $php["status_penerimaan"] = "Sebagian";
        }else{
            $php["status_penerimaan"] = "Diterima";
        }

        $this->pembayaran_hutang_produk->update_by_id("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id,$php);

        if($this->pembayaran_hutang_produk->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}

        
        echo json_encode($result);
    }


    function accept_stockss(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";

        $pengiriman_produk_id = $this->input->post("id");

        $this->pembayaran_hutang_produk->start_trans();

        // get data pengiriman
        $data_pengiriman = $this->pengiriman_produk->detail_pengiriman($pengiriman_produk_id);
        $pembayaran_hutang_produk_id = $data_pengiriman->pembayaran_hutang_produk_id;
        $total_qty_pengiriman = $data_pengiriman->total_qty_pengiriman;

        // update pengiriman status
        $pengiriman['status_pengiriman'] = 1;
        $this->pengiriman_produk->update_by_id('pengiriman_produk_id',$pengiriman_produk_id,$pengiriman);

        // get data detail PO dari pembayaran hutang produk
        $detail_po = $this->pengiriman_produk->get_produk_detail_by_hutang($pembayaran_hutang_produk_id);

        $jml_kirim = $total_qty_pengiriman;

        foreach ($detail_po as $key) {
            $kirim['pengiriman_produk_id'] = $pengiriman_produk_id;
            $kirim['po_produk_detail_id'] = $key->po_produk_detail_id;
            $po_produk_id = $key->po_produk_id;

            $sisa_qty = $key->sisa_qty;

            $gudang = $this->lokasi->all_gudang();
            $gudang = $gudang[0];

            if ($sisa_qty <= $jml_kirim) {
                $jml_kirim = $jml_kirim - $sisa_qty;
                $kirim['qty_pengiriman'] = $sisa_qty; 
                
                $insert_kirim = $this->pengiriman_produk_detail->insert($kirim);

                $po_detail['sisa_qty'] = 0;
                $this->po_produk_detail->update_by_id('po_produk_detail_id',$key->po_produk_detail_id,$po_detail);

                $stock_qty = $kirim['qty_pengiriman'];

                $po_data = $this->po_produk->row_by_id($po_produk_id);
                $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);

                foreach ($po_produk_item as $key) {

                    // $data = array();
                    // $data["stock_produk_qty"] = $stock_qty;
                    // $data["produk_id"] = $key->produk_id;
                    // $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
                    // $data["year"] = date("y");
                    // $data["month"] = date("m");
                    // $data["po_id"] = $po_produk_id;
                    // $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                    // $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                    // $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                    // $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                    // $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                    // $data["hpp"] = $this->string_to_number($key->harga);
                    // $insert = $this->stock_produk->insert($data);

                    // $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
                    // $this->db->where("produk_id",$key->produk_id);
                    // $this->db->order_by("stock_produk_id");
                    // $stock_gudang = $this->db->get($_SESSION["redpos_company"]["database"] .".stock_produk")->row();
                    // $jumlah = $stock_gudang->stock_produk_qty - $stock_qty;
                    // $stock_update = array();
                    // $stock_update["stock_produk_qty"] = $jumlah;
                    // $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
                    // $this->db->update($_SESSION["redpos_company"]["database"] .".stock_produk",$stock_update);

                    $row_stock = $this->stock_produk->stock_by_location_produk_id($gudang->lokasi_id,$key->produk_id);
                    $jumlah_bahan = $stock_qty;
                    foreach ($row_stock as $row) {
                        $stock = array();
                        $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                        $stock_out = $jumlah_bahan;
                        if($row->stock_produk_qty < $jumlah_bahan){
                            $jumlah = 0;
                            $stock_out = $row->stock_produk_qty;
                        }
                        $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                        $stock["stock_produk_qty"] = $jumlah;
                        $stock_produk_id = $row->stock_produk_id;
                        $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$stock);
                        if($updateStok){
                            $arus = array();
                            $arus["tanggal"] = date("Y-m-d");
                            $arus["table_name"] = "stock_produk";
                            $arus["stock_produk_id"] = $row->stock_produk_id;
                            $arus["produk_id"] = $key->produk_id;
                            $arus["stock_out"] = $stock_out;
                            $arus["stock_in"] = 0;
                            $arus["last_stock"] = $this->stock_produk->last_stock_gudang($row->produk_id)->result;
                            $arus["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                            $arus["keterangan"] = "Pembayaran Hutang Produk";
                            $arus["method"] = "update";
                            $this->stock_produk->arus_stock_produk($arus);
                        }
                        if($jumlah_bahan <= 0){
                            break;
                        }

                        if ($jumlah > 0) {
                            break;
                        }
        
                    }
                    
                }

            }else{
                if ($jml_kirim > 0) {
                    $qty_pengiriman = $sisa_qty - $jml_kirim;
                    $kirim['qty_pengiriman'] = $jml_kirim; 
                    
                    $insert_kirim = $this->pengiriman_produk_detail->insert($kirim);

                    $po_detail['sisa_qty'] = $qty_pengiriman;
                    $this->po_produk_detail->update_by_id('po_produk_detail_id',$key->po_produk_detail_id,$po_detail);

                    $stock_qty = $kirim['qty_pengiriman'];

                    $po_data = $this->po_produk->row_by_id($po_produk_id);
                    $po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
                    

                    foreach ($po_produk_item as $key) {

                        $row_stock = $this->stock_produk->stock_by_location_produk_id($gudang->lokasi_id,$key->produk_id);
                        $jumlah_bahan = $stock_qty;
                        foreach ($row_stock as $row) {
                            $stock = array();
                            $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                            $stock_out = $jumlah_bahan;
                            if($row->stock_produk_qty < $jumlah_bahan){
                                $jumlah = 0;
                                $stock_out = $row->stock_produk_qty;
                            }
                            $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                            $stock["stock_produk_qty"] = $jumlah;
                            $stock_produk_id = $row->stock_produk_id;
                            $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$stock);
                            if($updateStok){
                                $arus = array();
                                $arus["tanggal"] = date("Y-m-d");
                                $arus["table_name"] = "stock_produk";
                                $arus["stock_produk_id"] = $row->stock_produk_id;
                                $arus["produk_id"] = $key->produk_id;
                                $arus["stock_out"] = $stock_out;
                                $arus["stock_in"] = 0;
                                $arus["last_stock"] = $this->stock_produk->last_stock_gudang($row->produk_id)->result;
                                $arus["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                                $arus["keterangan"] = "Pembayaran Hutang Produk";
                                $arus["method"] = "update";
                                $this->stock_produk->arus_stock_produk($arus);
                            }
                            if($jumlah_bahan <= 0){
                                break;
                            }

                            if ($jumlah > 0) {
                                break;
                            }
            
                        }

                    }

                    $jml_kirim = 0;
                }
                
            }

        }

        // cek detail untuk update status
        $cek_sisa = $this->pengiriman_produk->count_total_sisa($pembayaran_hutang_produk_id);
        if ($cek_sisa->sisa >0) {
            $php["status_penerimaan"] = "Sebagian";
        }else{
            $php["status_penerimaan"] = "Diterima";
        }

        $this->pembayaran_hutang_produk->update_by_id("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id,$php);

        if($this->pembayaran_hutang_produk->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}

        
        echo json_encode($result);
    }


}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */