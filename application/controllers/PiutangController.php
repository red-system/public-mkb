<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PiutangController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('piutang','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('pos','',true);
    }

    public function index()
    {

        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/piutang.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Piutang < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = $this->uri->segment(1);
        $target = array(0,3,4,5);
        $sumColumn = array(3,4,5);

        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama_pelanggan"));
        array_push($column, array("data"=>"no_faktur"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"terbayar_lbl"));
        array_push($column, array("data"=>"sisa_lbl"));
        array_push($column, array("data"=>"status_piutang"));
        array_push($column, array("data"=>"tenggat_pelunasan"));
        $data['sumColumn'] = json_encode($sumColumn);
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['piutang'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/piutang');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $tipe = 0;
        if(isset($_GET["columns"][7]["search"]["value"]) && $_GET["columns"][7]["search"]["value"] != ""){
            $tipe = $_GET["columns"][7]["search"]["value"];
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        if($tipe==0){
            $result['iTotalRecords'] = $this->piutang->piutang_count_all();
            $result['iTotalDisplayRecords'] = $this->piutang->piutang_count_filter($query);
            $data =  $this->piutang->piutang_list($start,$length,$query);
        } else if ($tipe==1){
            $result['iTotalRecords'] = $this->piutang->piutang_akan_count_all();
            $result['iTotalDisplayRecords'] = $this->piutang->piutang_akan_count_filter($query);
            $data =  $this->piutang->piutang_akan_list($start,$length,$query);
        } else {
            $result['iTotalRecords'] = $this->piutang->piutang_jatuh_count_all();
            $result['iTotalDisplayRecords'] = $this->piutang->piutang_jatuh_count_filter($query);
            $data =  $this->piutang->piutang_jatuh_list($start,$length,$query);
        }

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->piutang_id;
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar_lbl = $this->idr_currency($key->terbayar);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa_lbl = $this->idr_currency($key->sisa);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'piutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
            $key->posting_url = base_url().'piutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function pay(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/hutang_piutang.js");
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Pembayaran Piutang< Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = 'piutang';
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $piutang = $this->piutang->piutang_by_id($id);
        $data['id'] = $id;
        if ($piutang != null) {
            $data['piutang'] = $piutang;
            array_push($column, array("data"=>"no"));
            array_push($column, array("data"=>"pembayaran"));
            array_push($column, array("data"=>"jumlah_lbl"));
            array_push($column, array("data"=>"keterangan"));
            $data['sumColumn'] = json_encode(array(2));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,2)));
            $data["action"] = json_encode(array("stock"=>false,"view"=>false,"edit"=>true,"delete"=>true));

            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/pay_piutang');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function pay_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->piutang->pembayaran_piutang_count($this->uri->segment(4));
        $result['iTotalDisplayRecords'] = $this->piutang->pembayaran_piutang_filter($this->uri->segment(4),$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->piutang->pembayaran_piutang_list($start,$length,$query,$this->uri->segment(4));
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->delete_url = base_url().'piutang/pay/delete/'.$key->pembayaran_piutang_id;
            $key->row_id = $key->pembayaran_piutang_id;
            $key->jumlah_lbl = $this->idr_currency($key->jumlah);
            $key->jumlah = number_format($key->jumlah);
            $key->pembayaran = $key->tipe_pembayaran_nama." ".$key->no_akun;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function detail(){
        $piutang_id = $this->uri->segment(3);
        $data["result"] = $this->piutang->piutang_by_id($piutang_id);
        echo json_encode($data);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $piutang_id = $this->input->post('piutang_id');
        $data['piutang_id'] = $piutang_id;
        $data['tipe_pembayaran_id'] = $this->input->post('tipe_pembayaran_id');
        $data['jumlah'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal'] = date("Y-m-d",$temp);
        $sisa = $this->string_to_number($this->input->post('sisa'));
        if($sisa >= $data['jumlah']){
            $insert = $this->piutang->insert_pembayaran($data);
            if($insert){
                $cek = $this->piutang->piutang_by_id($data['piutang_id']);
                if($cek->grand_total <= $cek->terbayar){
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->pos->update_by_id('penjualan_id',$cek->penjualan_id,$data);
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->piutang->update_by_id('piutang_id',$piutang_id,$data);
                } else{
                    $data = array();
                    $data['status_pembayaran'] = "Hutang";
                    $this->pos->update_by_id('penjualan_id',$cek->penjualan_id,$data);
                    $data = array();
                    $data['status_pembayaran'] = "Belum Lunas";
                    $this->piutang->update_by_id('piutang_id',$piutang_id,$data);
                }
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }
        } else {
            $result['message'] = "Pembayaran melebihi sisa piutang";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['piutang_id'] = $this->input->post('piutang_id');
        $data['tipe_pembayaran_id'] = $this->input->post('tipe_pembayaran_id');
        $data['jumlah'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal'] = date("Y-m-d",$temp);
        $pembayaran_piutang_id = $this->input->post('pembayaran_piutang_id');
        $grand_total = $this->string_to_number($this->input->post('grand_total'));
        $terbayar = $this->string_to_number($this->input->post('terbayar'));
        $old_jumlah = $this->string_to_number($this->input->post('old_jumlah'));
        $new_terbayar = $terbayar - $old_jumlah + $data['jumlah'];
        if($grand_total >= $new_terbayar){
            $edit = $this->piutang->edit_pembayaran($data,$pembayaran_piutang_id);
            if($edit){
                $cek = $this->piutang->piutang_by_id($data['piutang_id']);
                if($cek->grand_total <= $cek->terbayar){
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->pos->update_by_id('penjualan_id',$cek->penjualan_id,$data);
                } else{
                    $data = array();
                    $data['status_pembayaran'] = "Hutang";
                    $this->pos->update_by_id('penjualan_id',$cek->penjualan_id,$data);
                }
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
            }
        } else {
            $result['message'] = "Pembayaran melebihi sisa piutang";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $pembayaran = $this->piutang->detail_pembayaran($id);
            $delete = $this->piutang->delete_pembayaran($id);
            if($delete){
                $cek = $this->piutang->piutang_by_id($pembayaran->piutang_id);
                if($cek->grand_total <= $cek->terbayar){
                    $data = array();
                    $data['status_pembayaran'] = "Lunas";
                    $this->pos->update_by_id('penjualan_id',$cek->penjualan_id,$data);
                } else{
                    $data = array();
                    $data['status_pembayaran'] = "Hutang";
                    $this->pos->update_by_id('penjualan_id',$cek->penjualan_id,$data);
                }
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function posting(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $result['success'] = false;
        $result['message'] = "gagal merubah data";
        $data = array();
        $data['status_piutang'] = 'Tutup';
        $post = $this->piutang->update_by_id('piutang_id',$id,$data);
        if($post){
            $result['success'] = true;
            $result['message'] = "Data berhasil dirubah";
        }
        echo json_encode($result);
    }
}

/* End of file HutangController.php */
/* Location: ./application/controllers/HutangController.php */