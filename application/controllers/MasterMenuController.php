<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterMenuController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_menu','',true);
        $this->load->library('master');
        if(!isset($_SESSION['master'])){
            redirect(base_url().'master/login');
        }

    }

    public function index()
    {
        array_push($this->master->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->master->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->master->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->master->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->master->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->master->js, "script/app2.js");
        array_push($this->master->js, "script/master/company.js");

        $data["css"] = $this->master->css;
        $data["js"] = $this->master->js;
        $column = array();
        $data["meta_title"] = "Type < Master < Redpos";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"menu_kode"));
        array_push($column, array("data"=>"menu_nama"));
        array_push($column, array("data"=>"action"));
        array_push($column, array("data"=>"url"));
        array_push($column, array("data"=>"keterangan"));
        array_push($column, array("data"=>"icon"));
        array_push($column, array("data"=>"urutan"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $action = array("add"=>true,"edit"=>true,"delete"=>true,"submenu"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('master/static/header',$data);
        $this->load->view('master/static/sidebar');
        $this->load->view('master/static/topbar');
        $this->load->view('master/menu/index');
        $this->load->view('master/static/footer');
    }

    function options(){
        $data = array();
        $size = $this->size->all_list();
        foreach ($size as $key) {
            array_push($data, strtolower($key->size_nama));
        }
        echo json_encode($data);
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->master_menu->count_all();
        $result['iTotalDisplayRecords'] = $this->master_menu->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->master_menu->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'master/type/delete/';
            $key->menu_url = base_url().'master/type/access/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->menu_id));
            $key->submenu_url = base_url().'master/submenu/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->menu_id));
            $key->row_id = $key->menu_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $menu_kode = $this->input->post('menu_kode');
        $menu_nama = $this->input->post('menu_nama');
        $action = $this->input->post('action');
        $url = $this->input->post('url');
        $keterangan = $this->input->post('keterangan');
        $icon = $this->input->post('icon');
        $urutan = $this->input->post('urutan');

        $data = array(
            "menu_kode"=>$menu_kode,
            "menu_nama"=>$menu_nama,
            "action"=>$action,
            "url"=>$url,
            "keterangan"=>$keterangan,
            "icon"=>$icon,
            "urutan"=>$urutan,
        );
        $insert = $this->master_menu->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Kode sudah terdaftar";
        $id = $this->input->post('menu_id');
        $menu_kode = $this->input->post('menu_kode');
        $menu_nama = $this->input->post('menu_nama');
        $action = $this->input->post('action');
        $url = $this->input->post('url');
        $keterangan = $this->input->post('keterangan');
        $icon = $this->input->post('icon');
        $urutan = $this->input->post('urutan');

        $data = array(
            "menu_kode"=>$menu_kode,
            "menu_nama"=>$menu_nama,
            "action"=>$action,
            "url"=>$url,
            "keterangan"=>$keterangan,
            "icon"=>$icon,
            "urutan"=>$urutan,
        );
        $update = $this->master_menu->update_by_id('menu_id',$id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->master_menu->delete_by_id("menu_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

}

/* End of file SizeController.php */
/* Location: ./application/controllers/SizeController.php */
