<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class ManageVoucherController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('voucher_produk','',true);
        $this->load->model('reseller','',true);
        $this->load->library('form_validation');
        $this->load->library('main');
        $this->load->helper('string');


    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Management Voucher < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-voucher";
        $data['page'] = "manage-voucher";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"created_at"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"leader"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array());
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-voucher/index');
        $this->load->view('admin/static/footer');
    }

    function list(){
        if(isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][1]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->voucher_produk->manage_voucher_all();
        $result['iTotalDisplayRecords'] = $this->voucher_produk->manage_voucher_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->voucher_produk->manage_voucher_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y',$time);
            }
            $key->no = $i;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function terpakai_page(){

        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");


        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Management Voucher < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "manage-voucher";
        $data['page'] = "manage-voucher";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"created_at"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"leader"));
        array_push($column, array("data"=>"penukar"));
        array_push($column, array("data"=>"tanggal_penukaran"));

        $data['column'] = json_encode($column);
        $data['sumColumn'] = json_encode( array());
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/manage-voucher/index_terpakai');
        $this->load->view('admin/static/footer');

        
    }
    function terpakai_list(){
        if(isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][1]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');


        $result['iTotalRecords'] = $this->voucher_produk->manage_voucher_all_terpakai();
        $result['iTotalDisplayRecords'] = $this->voucher_produk->manage_voucher_filter_terpakai($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data = $this->voucher_produk->manage_voucher_list_terpakai($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y',$time);
            }
            if($key->tanggal_penukaran != null){
                $time = strtotime($key->tanggal_penukaran);
                $key->tanggal_penukaran = date('d-m-Y',$time);
            }
            $key->no = $i;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
