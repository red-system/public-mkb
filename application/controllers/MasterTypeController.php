<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterTypeController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_type','',true);
        $this->load->library('master');
        if(!isset($_SESSION['master'])){
            redirect(base_url().'master/login');
        }

    }

    public function index()
    {
        array_push($this->master->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->master->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->master->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->master->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->master->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->master->js, "script/app2.js");
        array_push($this->master->js, "script/master/company.js");

        $data["css"] = $this->master->css;
        $data["js"] = $this->master->js;
        $column = array();
        $data["meta_title"] = "Type < Master < Redpos";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"type_name"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $action = array("add"=>true,"edit"=>true,"delete"=>true,"menu"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('master/static/header',$data);
        $this->load->view('master/static/sidebar');
        $this->load->view('master/static/topbar');
        $this->load->view('master/type/index');
        $this->load->view('master/static/footer');
    }

    function options(){
        $data = array();
        $size = $this->size->all_list();
        foreach ($size as $key) {
            array_push($data, strtolower($key->size_nama));
        }
        echo json_encode($data);
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->master_type->count_all();
        $result['iTotalDisplayRecords'] = $this->master_type->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->master_type->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'master/type/delete/';
            $key->menu_url = base_url().'master/type/access/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->id));
            $key->row_id = $key->id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $type_name = $this->input->post('type_name');


        $data = array(
            "type_name"=>$type_name,
        );
        $insert = $this->master_type->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Kode sudah terdaftar";
        $id = $this->input->post('id');
       $type_name = $this->input->post('type_name');

        $data = array(
            "type_name"=>$type_name,
        );
        $update = $this->master_type->update_by_id('id',$id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->master_type->delete_by_id("id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function enter(){
        $id = $this->input->post('id');
        $company = $this->master_type->row_by_id($id);
        $_SESSION["redpos_login"]["user_id"] = $_SESSION['master']['id'];
        $_SESSION["redpos_login"]["user_role_id"] = 0;
        $_SESSION["redpos_login"]["avatar"] = "assets/media/users/user.jpg";
        $_SESSION["redpos_login"]["staff_id"] = 0;
        $_SESSION["redpos_login"]["db_name"] = $company->database_name;
        $_SESSION["redpos_login"]["user_name"] = "Super Admin";
        $_SESSION["redpos_login"]["email"] = "super-admin@redpos.id";
        $_SESSION["redpos_login"]["company_id"] = $id;
        $_SESSION["redpos_company"]["company_name"] = $company->company_name;
        $_SESSION["redpos_company"]["company_full_name"] = $company->company_full_name;
        $_SESSION["redpos_company"]["company_email"] = $company->company_email;
        $_SESSION["redpos_company"]["company_address"] = $company->company_address;
        $_SESSION["redpos_company"]["company_phone"] = $company->company_phone;
        $_SESSION["redpos_company"]["database"] = $company->database_name;
        $result['success'] = true;
        $result['message'] = "Berhasil masuk";
        echo json_encode($result);
    }

}

/* End of file SizeController.php */
/* Location: ./application/controllers/SizeController.php */
