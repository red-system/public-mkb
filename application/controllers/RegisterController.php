<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class RegisterController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->model('master_login','',true);
        $this->load->helper('string');

        $this->load->model('province');
        $this->load->model('city');
        $this->load->model('subdistrict');
        $this->load->model('reseller');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('image_lib');
        $this->load->helper('string');
        $this->load->library('image_lib');
    }

    public function index()
    {
        $uri = $this->uri->segment(2);
        $data['referal_code'] = "";
       if($uri!=""){
           $data['referal_code'] = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
           $data['referal_code'] = $this->encryption->decrypt($data['referal_code']);
       }
        $province = $this->province->all_list();
        $data['province'] = $province;
        $data["uri"] = $this->uri->segment(2);
        $this->load->view('admin/register/register-awal2',$data);
    }
    function v_satu(){
        $uri = $this->uri->segment(2);
        $data['referal_code'] = $this->uri->segment(2);
        $data['reseller'] = $this->reseller->resseler_by_referal_code($uri);
        $province = $this->province->all_list();
        $data['province'] = $province;
        $data["uri"] = str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($uri));
        $this->load->view('admin/register/register-awal2',$data);
    }
    function superagen(){
        $uri = $this->uri->segment(2);
        $data['referal_code'] = "";
        if($uri!=""){
            $data['referal_code'] = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
            $data['referal_code'] = $this->encryption->decrypt($data['referal_code']);
        }
        $province = $this->province->all_list();
        $data["bank"] = $this->db->get("mykindofbeauty_kemiri.bank")->result();
        $data['province'] = $province;
        $data["type"] = "super agen";
        $this->load->view('admin/register/register2',$data);
    }
    function agen(){
        $uri = $this->uri->segment(2);
        $data['referal_code'] = "";
        if($uri!=""){
            $data['referal_code'] = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
            $data['referal_code'] = $this->encryption->decrypt($data['referal_code']);
        }
        $province = $this->province->all_list();
        $data["bank"] = $this->db->get("mykindofbeauty_kemiri.bank")->result();
        $data['province'] = $province;
        $data["type"] = "agent";
        $this->load->view('admin/register/register2',$data);
    }
    function city(){
        $province_id = $this->input->post('province_id');
        $city = $this->city->geProvinceCity($province_id);
        echo json_encode(array("data"=>$city));
    }
    function subdistrict(){
        $city_id = $this->input->post('city_id');
        $subdistrict = $this->subdistrict->getCitySubdistrict($city_id);
        echo json_encode(array("data"=>$subdistrict));
    }
    public function foto_profile($str)
    {
        $allowed = array('png', 'jpg', 'jpeg');
        $filename = $_FILES['foto_profile']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if ($_FILES['foto_profile']['size'] > 5242880) { //10 MB (size is also in bytes)

            $this->form_validation->set_message('foto_profile', 'The {field} size is to big');
            return false;
        } else {
            if ((!in_array($ext, $allowed)) || $_FILES['foto_profile']['size'] == 0) {
                $this->form_validation->set_message('foto_profile', 'The {field} type is not allowed');
                return false;
            } else {
                return true;
            }
        }
    }

    public function foto_ktp($str)
    {
        $allowed = array('png', 'jpg', 'jpeg');
        $filename = $_FILES['foto_ktp']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);


        if ($_FILES['foto_ktp']['size'] > 5242880) { //10 MB (size is also in bytes)
            $this->form_validation->set_message('foto_ktp', 'The {field} size is to big');

            return false;
        } else {
            if ((!in_array($ext, $allowed)) || $_FILES['foto_ktp']['size'] == 0) {
                $this->form_validation->set_message('foto_ktp', 'The {field} type is not allowed');
                return false;
            } else {
                return true;
            }
        }
    }

    public function selfi_ktp($str)
    {

        $allowed = array('png', 'jpg', 'jpeg');
        $filename = $_FILES['selfi_ktp']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if ($_FILES['selfi_ktp']['size'] > 5242880) { //10 MB (size is also in bytes)
            $this->form_validation->set_message('selfi_ktp', 'The {field} size is to big');
            return false;
        } else {
            if ((!in_array($ext, $allowed)) || $_FILES['selfi_ktp']['size'] == 0) {
                $this->form_validation->set_message('selfi_ktp', 'The {field} type is not allowed');
                return false;
            } else {
                return true;
            }
        }
    }
    function save(){
        $result['success'] = false;
        $result['message'] = "Parameter tidak sesuai";
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[reseller.email]',array(
            'is_unique'     => 'This %s already exists.'
        ));
        $this->form_validation->set_rules('phone', 'No Hp', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('province_id', 'Propinsi Tinggal', 'required');
        $this->form_validation->set_rules('city_id', 'Kabupaten/Kota Tinggal', 'required');
        $this->form_validation->set_rules('subdistrict_id', 'Kecamatan', 'required');
        $this->form_validation->set_rules('no_ktp', 'No KTP', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat Tinggal', 'required');
        $this->form_validation->set_rules('outlet_province_id', 'Propinsi Outlet', 'required');
        $this->form_validation->set_rules('outlet_city_id', 'Kabupaten Kota Outlet', 'required');
        $this->form_validation->set_rules('outlet_subdistrict_id', 'Kecamatan Outlet', 'required');
        $this->form_validation->set_rules('outlet_alamat', 'Alamat Outlet', 'required');
//        $this->form_validation->set_rules('input_foto_profile', 'Foto Profil', 'required');
//        $this->form_validation->set_rules('input_foto_ktp', 'Foto KTP', 'required');
//        $this->form_validation->set_rules('input_selfi_ktp', 'Foto Selfie KTP', 'required');
//        if (empty($_FILES['foto_profile']['name']))
//        {
//            $this->form_validation->set_rules('foto_profile', 'Foto Profil', 'required');
//        }else{
//            $this->form_validation->set_rules('foto_profile', 'Foto Profil', 'callback_foto_profile');
//        }
//        if (empty($_FILES['foto_ktp']['name']))
//        {
//            $this->form_validation->set_rules('foto_ktp', 'Foto KTP', 'required');
//        } else {
//            $this->form_validation->set_rules('foto_ktp', 'Foto KTP', 'callback_foto_ktp');
//        }
//        if (empty($_FILES['selfi_ktp']['name']))
//        {
//            $this->form_validation->set_rules('selfi_ktp', 'Selfi dengan KTP', 'required');
//        }else {
//            $this->form_validation->set_rules('selfi_ktp', 'Selfi dengan KTP', 'callback_selfi_ktp');
//        }
        $this->form_validation->set_error_delimiters('<span class="error-message" style="color:red">', '</span>');
        if ($this->form_validation->run() === TRUE) {
            $email = $this->input->post('email',true);
            $phone = $this->input->post('phone',true);
            $nama = $this->input->post('nama',true);
            $type = $this->input->post('type',true);
            $no_ktp = $this->input->post('no_ktp',true);
            $province_id = $this->input->post('province_id',true);
            $city_id = $this->input->post('city_id',true);
            $subdistrict_id = $this->input->post('subdistrict_id',true);
            $alamat = $this->input->post('alamat',true);
            $outlet_province_id = $this->input->post('outlet_province_id',true);
            $outlet_city_id = $this->input->post('outlet_city_id',true);
            $outlet_subdistrict_id = $this->input->post('outlet_subdistrict_id',true);
            $outlet_alamat = $this->input->post('outlet_alamat',true);
            $jenis_kelamin = $this->input->post('jenis_kelamin',true);
            $foto_profile = $this->input->post('input_foto_profile');;
//            $foto_profile = $this->uploadImage("foto_profile", $foto_profile);
            $foto_ktp = $this->input->post('input_foto_ktp');
//            $foto_ktp = $this->uploadImage("foto_ktp", $foto_ktp);
            $selfi_ktp = $this->input->post('input_selfi_ktp');
//            $selfi_ktp = $this->uploadImage("selfi_ktp", $selfi_ktp);

            $data = array(
                "email"=>$email,
                "no_ktp"=>$no_ktp,
                "phone"=>$phone,
                "nama"=>$nama,
                "type"=>$type,
                "province_id"=>$province_id,
                "city_id"=>$city_id,
                "subdistrict_id"=>$subdistrict_id,
                "alamat"=>$alamat,
                "outlet_province_id"=>$outlet_province_id,
                "outlet_city_id"=>$outlet_city_id,
                "outlet_subdistrict_id"=>$outlet_subdistrict_id,
                "outlet_alamat"=>$outlet_alamat,
                "foto_profile"=>$foto_profile,
                "foto_ktp"=>$foto_ktp,
                "selfi_ktp"=>$selfi_ktp,
                "jenis_kelamin"=>$jenis_kelamin
            );
            $referal_code = $this->input->post('referal_code');
            if($referal_code!=""){
                $reseler = $this->reseller->resseler_by_referal_code($referal_code);
                $data["referal_id"] = $reseler->reseller_id;
            }
            $insert = $this->reseller->insert($data);
            if($insert){
                $result['success'] = true;
                $result['message'] = "Berhasil menyimpan data";
                $reseller_id = $this->reseller->last_id();
                $this->reseller->cek_leader($reseller_id);
                $prefix = random_string('alnum', 5);
                $referal_code = $prefix."-".$reseller_id;
                $data = array("referal_code"=>$referal_code);
                $this->reseller->update_by_id('reseller_id',$reseller_id,$data);
            }
            echo json_encode($result);
        }else{
            echo json_encode(
                array(
                    'status' => 'error',
                    'message' => 'Fill form completely',
                    'errors' => array(
                        'email' => form_error('email'),
                        'no_ktp' => form_error('no_ktp'),
                        'phone' => form_error('phone'),
                        'nama' => form_error('nama'),
                        'province_id' => form_error('province_id'),
                        'city_id' => form_error('city_id'),
                        'subdistrict_id' => form_error('subdistrict_id'),
                        'alamat' => form_error('alamat'),
                        'outlet_province_id' => form_error('outlet_province_id'),
                        'outlet_city_id' => form_error('outlet_city_id'),
                        'outlet_subdistrict_id' => form_error('outlet_subdistrict_id'),
                        'outlet_alamat' => form_error('outlet_alamat'),
                        'input_foto_profile' => form_error('foto_profile'),
                        'input_foto_ktp' => form_error('foto_ktp'),
//                        'input_selfi_ktp' => form_error('selfi_ktp'),
                    )
                )
            );
        }

    }
    function uploadImage($file, $url)
    {
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        $image = $_FILES[$file]["tmp_name"];
//        $image = imagecreatefromjpeg($image);
//        $thumbImage = imagecreatetruecolor(450, 500);
////        $new_image = imagecopyresized($thumbImage, $image, 0, 0, 0, 0, 50, 50, 400, 400);
//
//        $new_image = $thumbImage;
//        $data = array(
//            'image' => $image,
//            'temp' => $local_path
//        );
//
//        return $thumbImage;

        $path = $_FILES[$file.'_base']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        if (!$ext){
            $ext = 'jpg';
        }
        $date = new DateTime();
        $file_name = $date->getTimestamp() . random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/" . $file_name . "." . $ext, $image, NET_SFTP_LOCAL_FILE);
        if (!$output) {
            $url = "failed";
            return $url;
        } else {
            $url = $file_name . '.' . $ext;
        }
        return $url;


    }
    function success(){
        $this->load->view('admin/register/register-success');
    }
    function upload()
    {
        $foto_profile = "";
        if (!empty($_FILES['foto_profile']['name'])) {
            $foto_profile = $this->uploadImage("foto_profile", $foto_profile);
        }
        if (!empty($_FILES['foto_ktp']['name'])) {
            $foto_profile = $this->uploadImage("foto_ktp", $foto_profile);
        }
        if (!empty($_FILES['selfi_ktp']['name'])) {
            $foto_profile = $this->uploadImage("selfi_ktp", $foto_profile);
        }
        $data['url'] = $foto_profile;
        echo json_encode($data);
    }
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
