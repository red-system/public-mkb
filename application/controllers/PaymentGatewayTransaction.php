<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentGatewayTransaction extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('midtrans_response','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Payment Gateway Transaction < POS < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "pos";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"no_faktur"));
        array_push($column, array("data"=>"transaction_time"));
        array_push($column, array("data"=>"transaction_status"));
        array_push($column, array("data"=>"status_message"));
        array_push($column, array("data"=>"payment_type"));
        array_push($column, array("data"=>"gross_amount"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/payment-gateway-transaction/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->midtrans_response->count_all();
        $result['iTotalDisplayRecords'] = $this->midtrans_response->count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->midtrans_response->list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->no = $i;
            $i++;
            $key->payment_type = str_replace('_',' ',$key->payment_type);
            $key->gross_amount = $this->number_to_string($key->gross_amount);

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file PaternController.php */
/* Location: ./application/controllers/PaternController.php */
