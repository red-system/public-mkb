<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PoKutusKutusController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_kutus_kutus','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('suplier','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('penerimaan_produk','',true);
        $this->load->model('penerimaan_produk_detail','',true);
        $this->load->model('history_hpp_produk','',true);
		$this->load->model('piutang_produk_diterima','',true);
		$this->load->model('produk','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/po_kutus_kutus.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Order MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_red";
        $data['page'] = "order_red";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_kutus_kutus_no"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"suplier_nama"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"jenis_pembayaran"));
        array_push($column, array("data"=>"tipe_pembayaran_nama"));
        array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['order_red'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['lokasi'] = $this->lokasi->all_gudang();
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po-kutus-kutus/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_kutus_kutus->po_kutus_kutus_count_all();
        $result['iTotalDisplayRecords'] = $this->po_kutus_kutus->po_kutus_kutus_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_kutus_kutus->po_kutus_kutus_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);
            $key->penerimaan_status_btn = true;
            $key->edit_url = base_url().'order-produsen/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_kutus_kutus_id));
            $key->no = $i;
            if($key->jenis_pembayaran == "kas"){
                $key->tipe_pembayaran_nama = $key->kas_nama." ".$key->no_akun;
            }
            $i++;
            $key->print_url = base_url()."order-produsen/print/".$key->po_kutus_kutus_id;
            $key->delete_url = base_url().'order-produsen/delete/';
            $key->row_id = $key->po_kutus_kutus_id;
            $key->enc_row_id = str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_kutus_kutus_id));
            $key->action = null;
            
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "script/admin/po_kutus_kutus.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Order MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_red";
        $data['page'] = $this->uri->segment(1);
        $data['lokasi'] = $this->lokasi->all_list();
        $data['suplier'] = $this->suplier->all_list();
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
        $data['po_kutus_kutus_no'] = 	$this->po_kutus_kutus->get_kode_po_kutus_kutus();
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po-kutus-kutus/add_po_kutus_kutus');
        $this->load->view('admin/static/footer');
        unset($_SESSION['po_kutus_kutus']);
        $_SESSION['po_kutus_kutus']['lokasi'] = 1;
    }
    function utility(){
        $key = $this->uri->segment(3);
        if($key == "get-po-no"){
            $code = $this->input->post('suplier_kode');
            echo $this->po_kutus_kutus->get_kode_po_kutus_kutus($code);
        }
        if($key=="list-produk"){
            $this->list_produk();
        }
        if($key=="sess-produk-add"){
            $no = $this->input->post('key');
            $produk_id = $this->input->post('produk_id');
            $_SESSION['po_kutus_kutus']['produk']['con_'.$no]['produk_id'] = $produk_id;
            $_SESSION['po_kutus_kutus']['produk']['con_'.$no]['value'] = 0;
        }
        if($key=="sess-produk-change"){
            $jumlah = $this->input->post('jumlah');
            $no = $this->input->post('key');
            if(isset($_SESSION['po_kutus_kutus']['produk']['con_'.$no]['produk_id'])){
                $_SESSION['po_kutus_kutus']['produk']['con_'.$no]['value'] = $jumlah;
            }
        }
        if($key=="sess-produk-delete"){
            $no = $this->input->post('key');
            unset($_SESSION['po_kutus_kutus']['produk']['con_'.$no]);
        }
        if($key=="sess-produk-reset"){
            unset($_SESSION['po_kutus_kutus']['produk']);
        }
        if($key=="sess-lokasi"){
            $_SESSION['po_kutus_kutus']['lokasi'] = $this->input->post('lokasi_id');
        }
    }
    function list_produk(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->produk->produk_count_all();
        $result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->produk->produk_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'produk/delete/';
            $key->action =null;
            $key->last_hpp = $this->stock_produk->last_hpp($key->produk_id);
            $key->last_hpp = $key->last_hpp == 0 ? $key->hpp_global : $key->last_hpp;
            $key->row_id = $key->produk_id;
            $key->produk_minimal_stock = number_format($key->produk_minimal_stock);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function save_add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));
        $insert = $this->po_kutus_kutus->insert_po_kutus_kutus();
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function edit(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $po_kutus_kutus = $this->po_kutus_kutus->po_kutus_kutus_by_id($id);
        if ($po_kutus_kutus != null) {
            $data['po_kutus_kutus'] = $po_kutus_kutus;
            $data['po_kutus_kutus_detail']=$this->po_kutus_kutus->po_kutus_kutus_detail_by_id($id);
            foreach ($data['po_kutus_kutus_detail'] as $item){
                $item->last_hpp = $item->last_hpp == 0 ?  $this->stock_produk->last_hpp($item->produk_id) : $item->last_hpp;
            }
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "script/admin/po_kutus_kutus.js");
            $data['lokasi'] = $this->lokasi->all_list();
            $data['suplier'] = $this->suplier->all_list();
            $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Produksi < ".$_SESSION["redpos_company"]['company_name'];;
            $data['parrent'] = "master_data";
            $data['page'] = $this->uri->segment(1);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/po-kutus-kutus/edit_po_kutus_kutus');
            $this->load->view('admin/static/footer');
        }else {

            redirect('404_override','refresh');
        }
    }
    function save_edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));
        $insert = $this->po_kutus_kutus->edit_po_kutus_kutus();
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->po_kutus_kutus->delete_by_id("po_kutus_kutus_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function detail(){
        $id = $this->input->post('po_kutus_kutus_id');
        $temp = $this->po_kutus_kutus->po_kutus_kutus_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->po_kutus_kutus->po_kutus_kutus_detail_by_id($id);
        $temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
        if($temp->tanggal_pemesanan != null){
            $x = strtotime($temp->tanggal_pemesanan);
            $temp->tanggal_pemesanan = date("Y-m-d",$x);
        }
        if($temp->tanggal_penerimaan != null){
            $x = strtotime($temp->tanggal_penerimaan);
            $temp->tanggal_penerimaan = date("Y-m-d",$x);
        }
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga);
            $key->jumlah = number_format($key->jumlah);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        echo json_encode($temp);
    }
    function penerimaan_po_kutus_kutus(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $po_kutus_kutus_id = $this->input->post('po_kutus_kutus_id');
        $data["status_penerimaan"] = "Diterima";
        $data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
        $lokasi_id = $this->input->post('lokasi_penerimaan_id');
        $data['lokasi_penerimaan_id'] = $lokasi_id;
        $this->po_kutus_kutus->start_trans();
        $update_po_kutus_kutus = $this->po_kutus_kutus->update_by_id('po_kutus_kutus_id',$po_kutus_kutus_id,$data);
        if($update_po_kutus_kutus){
            $po_kutus_kutus_item = $this->po_kutus_kutus->po_kutus_kutus_detail_by_id($po_kutus_kutus_id);
            foreach ($po_kutus_kutus_item as $key) {
                $data = array();
                $data["stock_produk_qty"] = $key->jumlah;
                $data["produk_id"] = $key->produk_id;
                $data["stock_produk_lokasi_id"] = $lokasi_id;
                $data["year"] = date("y");
                $data["month"] = date("m");
                $data["po_id"] = $po_kutus_kutus_id;
                $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                $data["hpp"] = $this->string_to_number($key->harga);
                $insert = $this->stock_produk->insert($data);
                $arus["stock_produk_id"] = $this->stock_produk->last_id();
                $arus["method"] = "insert";
                $arus["tanggal"] = date("Y-m-d");
                $arus["table_name"] = "stock_produk";
                $arus["produk_id"] = $key->produk_id;
                $arus["stock_out"] = 0;
                $arus["stock_in"] = $key->jumlah;
                $arus["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
                $arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
                $arus["keterangan"] = "Order MKB";
                $this->stock_produk->arus_stock_produk($arus);
            }
        }
        if($this->po_kutus_kutus->result_trans()){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function history(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/po_kutus_kutus.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "History Order MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_red";
        $data['page'] = "order_red";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_kutus_kutus_no"));
        array_push($column, array("data"=>"tanggal_pemesanan"));
        array_push($column, array("data"=>"suplier_nama"));
        array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"jenis_pembayaran"));
        array_push($column, array("data"=>"tipe_pembayaran_nama"));
        array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"tanggal_penerimaan"));
        array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
        $data['lokasi'] = $this->lokasi->all_list();
        $action = array("view"=>true,"print"=>true);
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/po-kutus-kutus/index');
        $this->load->view('admin/static/footer');
    }
    function history_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->po_kutus_kutus->po_kutus_kutus_count_all_history();
        $result['iTotalDisplayRecords'] = $this->po_kutus_kutus->po_kutus_kutus_count_filter_history($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->po_kutus_kutus->po_kutus_kutus_list_history($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pemesanan != null){
                $time = strtotime($key->tanggal_pemesanan);
                $key->tanggal_pemesanan = date('d-m-Y',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan = date('d-m-Y',$time);
            }
            $key->total = number_format($key->total);
            $key->tambahan = number_format($key->tambahan);
            $key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
            $key->grand_total = number_format($key->grand_total);

            $key->edit_url = base_url().'order-kutus-kutus/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_kutus_kutus_id));
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'order-kutus-kutus/delete/';
            $key->print_url = base_url()."order-kutus-kutus/print/".$key->po_kutus_kutus_id;
            $key->row_id = $key->po_kutus_kutus_id;
            $key->action = null;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function print(){
        $id = $this->uri->segment(3);
        $temp = $this->po_kutus_kutus->po_kutus_kutus_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        $temp->item = $this->po_kutus_kutus->po_kutus_kutus_detail_by_id($id);
        $temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
        if($temp->tanggal_pemesanan != null){
            $x = strtotime($temp->tanggal_pemesanan);
            $temp->tanggal_pemesanan = date("Y-m-d",$x);
        }
        if($temp->tanggal_penerimaan != null){
            $x = strtotime($temp->tanggal_penerimaan);
            $temp->tanggal_penerimaan = date("Y-m-d",$x);
        }
        if($temp->created_at != null){
            $time = strtotime($temp->created_at);
            $temp->created_at = date('d-m-Y H:i:s',$time);
        }
        if($temp->updated_at != null){
            $time = strtotime($temp->updated_at);
            $temp->updated_at = date('d-m-Y H:i:s',$time);
        }
        foreach ($temp->item as $key) {
            $key->harga = number_format($key->harga);
            $key->jumlah = number_format($key->jumlah);
            $key->sub_total = number_format($key->sub_total);
        }
        $temp->total = number_format($temp->total);
        $temp->tambahan = number_format($temp->tambahan);
        $temp->potongan = number_format($temp->potongan);
        $temp->grand_total = number_format($temp->grand_total);
        $data['po_kutus_kutus'] = $temp;
        $this->load->view('admin/po-kutus-kutus/print',$data);
    }

    function penerimaan_page(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/penerimaan_produk.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Order ke Produsen < Penerimaan < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "order_red";
        $data['page'] = 'penerimaan';
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $penerimaan = $this->penerimaan_produk->penerimaan_by_id($id);
        $data['id'] = $id;
        if ($id != null) {
            $data['penerimaan'] = $penerimaan;
            array_push($column, array("data"=>"no"));
            array_push($column, array("data"=>"tanggal_penerimaan"));
            array_push($column, array("data"=>"total_qty_penerimaan"));
            array_push($column, array("data"=>"status_kirim_lbl"));
            array_push($column, array("data"=>"keterangan_penerimaan"));
            $data['sumColumn'] = json_encode(array(2));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className"=>"text__center","targets"=>array(0,2)));
            $data["action"] = json_encode(array("view"=>false, "edit"=>true,"delete"=>true, "submit" => true), true);
            
            $data['lokasi'] = $this->lokasi->all_gudang();
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/po-kutus-kutus/penerimaan');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function penerimaan_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->penerimaan_produk->penerimaan_produk_count($this->uri->segment(4));
        $result['iTotalDisplayRecords'] = $this->penerimaan_produk->penerimaan_produk_filter($this->uri->segment(4),$query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->penerimaan_produk->penerimaan_produk_list($start,$length,$query,$this->uri->segment(4));
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_penerimaan != null){
                $time = strtotime($key->tanggal_penerimaan);
                $key->tanggal_penerimaan_label = date('d-m-Y',$time);
            }
            $key->no = $i;
            $key->delete_url = base_url().'order-produsen/penerimaan/delete/'.$key->penerimaan_produk_id;
            // $key->submit_url = base_url().'order-super-agen/penerimaan/accept-stock/'.$key->penerimaan_id;
            $key->row_id = $key->penerimaan_produk_id;
            $key->total_qty_penerimaan = number_format($key->total_qty_penerimaan);
            $key->status_kirim_lbl = 'Belum submit';
            if ($key->status_penerimaan == 1) {
                $key->deny_edit = 'true';
                $key->deny_delete = 'true';
                $key->deny_submit = 'true';
                $key->status_kirim_lbl = 'Submit';
            }
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail_penerimaan(){
        $po_kutus_kutus_id = $this->uri->segment(4);
        $data["result"] = $this->penerimaan_produk->penerimaan_by_id($po_kutus_kutus_id);
        echo json_encode($data);
    }

    function add_penerimaan(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['po_kutus_kutus_id'] = $this->input->post('po_kutus_kutus_id');
        $data['lokasi_penerimaan_id'] = $this->input->post('lokasi_penerimaan_id');
        $data['keterangan_penerimaan'] = $this->input->post('keterangan');
        $data['total_qty_penerimaan'] = $this->string_to_number($this->input->post('jumlah'));
        $temp = strtotime($this->input->post('tanggal'));
        $data['tanggal_penerimaan'] = date("Y-m-d",$temp);
        $data['status_penerimaan'] = 0;
        $sisa = $this->string_to_number($this->input->post('sisa'));

        if($sisa >= $data['total_qty_penerimaan']){
            $insert = $this->penerimaan_produk->insert($data);

            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        } else {
            $result['message'] = "Penerimaan Produk melebihi sisa order";
        }
        echo json_encode($result);
    }


    function edit_penerimaan(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['keterangan_penerimaan'] = $this->input->post('keterangan_penerimaan');
        $po_kutus_kutus_id = $this->input->post('po_kutus_kutus_id');
        $data['lokasi_penerimaan_id'] = $this->input->post('lokasi_penerimaan_id');
        $data['total_qty_penerimaan'] = $this->string_to_number($this->input->post('total_qty_penerimaan'));
        $temp = strtotime($this->input->post('tanggal_penerimaan_label'));
        $data['tanggal_penerimaan'] = date("Y-m-d",$temp);
        $penerimaan_produk_id = $this->input->post('penerimaan_produk_id');
        $sisa = $this->string_to_number($this->input->post('sisa'));
        // $terkirim = $this->string_to_number($this->input->post('terkirim'));
        // $old_jumlah = $this->string_to_number($this->input->post('old_jumlah'));
        // $new_terkirim = $terkirim - $old_jumlah + $data['total_qty_penerimaan'];

        if($data['total_qty_penerimaan'] <= $sisa){
            $edit = $this->penerimaan_produk->update_by_id('penerimaan_produk_id',$penerimaan_produk_id,$data);

            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        } else {
            $result['message'] = "Penerimaan Produk melebihi sisa order";
        }

        echo json_encode($result);
    }
    function delete_penerimaan(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->penerimaan_produk->delete_penerimaan($id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }

    function accept_stock(){

        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";

        $penerimaan_produk_id = $this->input->post("id");

        // get data penerimaan
        $data_penerimaan = $this->penerimaan_produk->detail_penerimaan($penerimaan_produk_id);
        $po_kutus_kutus_id = $data_penerimaan->po_kutus_kutus_id;
        $lokasi_id = $data_penerimaan->lokasi_penerimaan_id;
        $total_qty_penerimaan = $data_penerimaan->total_qty_penerimaan;

        $this->po_kutus_kutus->start_trans();

        // update penerimaan status
        $penerimaan['status_penerimaan'] = 1;
        $this->penerimaan_produk->update_by_id('penerimaan_produk_id',$penerimaan_produk_id,$penerimaan);

        $data_detail = $this->penerimaan_produk->get_produk_detail_by_po($po_kutus_kutus_id);
        foreach ($data_detail as $value) {
            $po_kutus_kutus_detail_id = $value->po_kutus_kutus_detail_id;
            $kirim['penerimaan_produk_id'] = $penerimaan_produk_id;
            $kirim['po_kutus_kutus_detail_id'] = $po_kutus_kutus_detail_id;
            $kirim['qty_penerimaan'] = $total_qty_penerimaan; 
            $insert_kirim = $this->penerimaan_produk_detail->insert($kirim);

            $sisa = $value->sisa - $total_qty_penerimaan;
            $this->db->query("UPDATE mykindofbeauty_kemiri.po_kutus_kutus_detail SET sisa = $sisa  WHERE po_kutus_kutus_detail_id=$po_kutus_kutus_detail_id ");

            // insert ke piutang produk diterima
			$piutang_produk = array();
			$piutang_produk["penerimaan_produk_id"] = $penerimaan_produk_id;
			$piutang_produk["produk_id"] = $value->produk_id;
			$piutang_produk["tanggal"] = date("Y-m-d");
			$piutang_produk["stock_in"] = $total_qty_penerimaan;
			$piutang_produk['sisa_piutang'] = $sisa;
			$piutang_produk_diterima = $this->piutang_produk_diterima->insert($piutang_produk);
        }

        $cek_sisa = $this->penerimaan_produk->count_total_sisa($po_kutus_kutus_id);

        if ($cek_sisa->sisa >0) {
            $data["status_penerimaan"] = "Sebagian";
        }else{
            $data["status_penerimaan"] = "Diterima";
        }

        $data["tanggal_penerimaan"] = date("Y-m-d");
        $data['lokasi_penerimaan_id'] = $lokasi_id;

        $update_po_kutus_kutus = $this->po_kutus_kutus->update_by_id('po_kutus_kutus_id',$po_kutus_kutus_id,$data);
        if($update_po_kutus_kutus){
            $po_kutus_kutus_item = $this->po_kutus_kutus->po_kutus_kutus_detail_by_id($po_kutus_kutus_id);
            foreach ($po_kutus_kutus_item as $key) {

                // perhitungan hpp
				// $hpp_bahan = $this->history_hpp_produk->get_total_hpp_bahan($key->produk_id);
				// $total_hpp_bahan = $hpp_bahan->total_hpp_bahan;

				$stock_gudang = $this->history_hpp_produk->get_total_stock_produk_last($key->produk_id);
				$total_stok_gudang = $stock_gudang->total_stock_gudang;

				$data_produk = $this->produk->row_by_id($key->produk_id);
                $harga_hpp_global = $data_produk->hpp_global;
                $harga_hpp_asli = $data_produk->hpp_asli;
                $harga_hpp_produsen = $data_produk->hpp_kutus;

                $hpp_stock_old = $total_stok_gudang * $harga_hpp_asli;
                $hpp_stock_new = $total_qty_penerimaan * $harga_hpp_produsen;
                $total_hpp = $hpp_stock_old + $hpp_stock_new;
                $total_stock_produk = $total_stok_gudang + $total_qty_penerimaan;

                $new_hpp_produk = $total_hpp / $total_stock_produk;

				// $new_hpp_produk = (($total_stok_gudang * $harga_hpp_asli) + ($total_qty_penerimaan * $harga_hpp_produsen)) / ($total_stok_gudang + $total_qty_penerimaan);

                $data = array();
                $data["stock_produk_qty"] = $total_qty_penerimaan;
                $data["produk_id"] = $key->produk_id;
                $data["stock_produk_lokasi_id"] = $lokasi_id;
                $data["year"] = date("y");
                $data["month"] = date("m");
                $data["po_id"] = $po_kutus_kutus_id;
                $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                // $data["hpp"] = $this->string_to_number($key->harga);
                $data["hpp"] = $this->string_to_number($harga_hpp_global);
                $data["hpp_asli"] = $this->string_to_number($harga_hpp_produsen);
                $insert = $this->stock_produk->insert($data);

                $arus["stock_produk_id"] = $this->stock_produk->last_id();
                $arus["method"] = "insert";
                $arus["tanggal"] = date("Y-m-d");
                $arus["table_name"] = "stock_produk";
                $arus["produk_id"] = $key->produk_id;
                $arus["stock_out"] = 0;
                $arus["stock_in"] = $total_qty_penerimaan;
                $arus["last_stock"] = $this->stock_produk->last_stock_gudang($key->produk_id)->result;
                $arus["last_stock_total"] = $this->stock_produk->stock_total_gudang()->result;
                $arus["keterangan"] = "Order MKB";
                $this->stock_produk->arus_stock_produk($arus);

                $histori_produk = array();
                $histori_produk["produk_id"] = $key->produk_id;
                $histori_produk["tanggal"] = date("Y-m-d");
                $histori_produk["produk_qty"] = $total_qty_penerimaan;
                $histori_produk['hpp'] = $new_hpp_produk;
                $history_hpp_produk = $this->history_hpp_produk->insert($histori_produk);

                $produk["hpp_asli"] = $new_hpp_produk;
				$update_produk = $this->produk->update_by_id('produk_id',$key->produk_id,$produk);
            }
        }
        if($this->po_kutus_kutus->result_trans()){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
            // $result['total_qty_penerimaan'] = $total_qty_penerimaan;
			// $result['total_stok_gudang'] = $total_stok_gudang;
			// $result['harga_hpp_asli'] = $harga_hpp_asli;
			// $result['harga_beli'] = $harga_hpp_produsen;
			// $result['hpp_stock_old'] = $hpp_stock_old;
			// $result['hpp_stock_new'] = $hpp_stock_new;
			// $result['total_hpp'] = $total_hpp;
			// $result['total_stock_produk'] = $total_stock_produk;
			// $result['new_hpp_produk'] = $new_hpp_produk;
        }

        echo json_encode($result);
    }

}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */