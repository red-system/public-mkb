<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class POSController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('guest','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('pos','',true);
		$this->load->model('lokasi','',true);
        $this->load->model('province','',true);
		$this->load->model('harga_produk','',true);
		$this->load->model('tipe_pembayaran','',true);
		$this->load->model('log_kasir','',true);
		$this->load->model('unit','',true);
        $this->load->model('assembly','',true);
        if($_SESSION['redpos_login']['pay']==1){
            $params = array('server_key' => $_SESSION['redpos_login']['pay-s'], 'production' => $_SESSION['redpos_login']['pay-prod']);
            $this->load->library('midtrans');
            $this->midtrans->config($params);
            $this->load->helper('url');
        }
	}
	public function index(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");		
		array_push($this->js, "script/admin/pos_.js");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penjualan < POS < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "pos";
		$data['page'] = 'penjualan';
		$data['lokasi'] = $this->lokasi->all_list();
		$data['pay'] = $_SESSION['redpos_login']['pay'] == 0 ? false : true;
		unset($_SESSION['pos']);
		if($data['lokasi']!=null){
            $_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
            if(isset($_SESSION["redpos_login"]['location_id'])){
                $_SESSION['pos']['location_id'] = $_SESSION["redpos_login"]['location_id'];
            }
            $province = $this->province->all_list();
            $data['province'] = $province;
            $data['faktur'] = $this->pos->getFakturCode();
            $data['urutan'] = $this->pos->getUrutan();
            $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            if (isset($_SESSION['log_pos']['log_kasir_id'])) {
                $this->load->view('admin/penjualan');
            } else {
                $this->load->view('admin/form_pos');
            }
            $this->load->view('admin/static/footer');
		}else{
            $_SESSION['error'] = 'Anda tidak bisa menjalankan pos tanpa data lokasi/outlet';
			redirect(base_url());
        }
	}
	function guest_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->guest->guest_count_all();
		$result['iTotalDisplayRecords'] = $this->guest->guest_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->guest->guest_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			$key->no = $i;
			$i++;
			$key->aksi = null;
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save_preaty_cash(){
		$result['success'] = false;
		$result['message'] = 'Gagal menyimpan data';
		$data['user_id'] = $_SESSION["redpos_login"]['user_id'];
        $data['staff_id'] = $_SESSION["redpos_login"]['staff_id'];
		$data['waktu_buka'] = Date('Y-m-d H:i:s');
		$data['kas_awal'] = $this->string_to_number($this->input->post('kas_awal'));
		$data['created_at'] = Date('Y-m-d H:i:s');
		$insert = $this->log_kasir->insert($data);
		if($insert){
			$result['success'] = true;
			$result['message'] = 'Berhasil menyimpan data';
			$_SESSION['log_pos']['log_kasir_id'] = $this->log_kasir->last_id();
		}
		echo json_encode($result);
	}
	function info_closing(){

        $log_pos_id = $_SESSION['log_pos']['log_kasir_id'];
	    $info = $this->pos->log_kasir_info($log_pos_id);
        $info->jumlah_transaksi = $this->pos->jumlah_transaksi_by_log($log_pos_id);
        $info->total_transaksi = $this->idr_currency($this->pos->total_transaksi_by_log($log_pos_id)->total);
        $info->kas_akhir = $this->idr_currency((int)$this->pos->transaksi_by_log($log_pos_id)->total+(int)$info->kas_awal);
        $info->kas_awal = $this->idr_currency($info->kas_awal);
	    echo json_encode($info);
    }
	function closing(){
		$result['success'] = true;
		$result['message'] = 'Gagal Merubah data';

		$log_pos_id = $_SESSION['log_pos']['log_kasir_id'];
        $result['log_kasir_id'] = $log_pos_id;
		$log_pos = $this->log_kasir->row_by_id($log_pos_id);
		$total_penjualan = $this->pos->transaksi_by_log($log_pos_id)->total;
		$total = (int)$total_penjualan + (int)$log_pos->kas_awal;
		$data['kas_akhir'] = $total;
		$data['waktu_tutup'] = Date('Y-m-d H:i:s');
		$data['updated_at'] = Date('Y-m-d H:i:s');
		$update = $this->log_kasir->update_by_id('log_kasir_id',$log_pos_id,$data);
		if($update){
			unset($_SESSION['log_pos']);
			$result['total_kas'] = number_format($total);
			$result['success'] = true;
			$result['message'] = 'Berhasil Merubah data';
		}
		echo json_encode($result);
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key=="change-location"){
			$_SESSION['pos']['location_id'] = $this->input->post('lokasi_id');
		}
	}
	function produk_list(){
		$lokasi_id = $_SESSION['pos']['location_id'];
		if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			$lokasi_id = $_SESSION["redpos_login"]['lokasi_id'];
		}
		if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
			$_GET['stock_produk_seri'] = $_GET["columns"][3]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_produk->pos_produk_count($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->stock_produk->pos_produk_filter($query,$lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_produk->pos_produk_list($start,$length,$query,$lokasi_id);
		$i = $start+1;
		foreach ($data as $key) {
			$key->aksi = null;
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->ar_minimal_pembelian = "";
			$key->ar_harga ="";
			$harga = $this->harga_produk->harga_produk_satuan($key->produk_id,$key->produk_satuan_id);
			foreach ($harga as $each) {
				$key->ar_minimal_pembelian .= $each->minimal_pembelian."|";
				$key->ar_harga .= $each->harga."|";
			}
			$key->ar_minimal_pembelian .= "0";
			$key->ar_harga .= $key->harga_eceran;			
			$key->no = $i; 
			$key->delete_url = base_url().'stock-bahan/delete/'.$key->produk_id;
			$key->row_id = $key->produk_id;
			$key->stock_produk_qty =  $this->number_to_string($key->stock_produk_qty);
			$key->harga_eceran = number_format($key->harga_eceran);
			$key->harga_grosir = number_format($key->harga_grosir);
			$i++;
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$transaksi = json_decode($this->input->post('transaksi'));
		$save = $this->pos->save();
		if($save['result']){
			$transaksi = json_decode($this->input->post('transaksi'));
			$grand_total = $transaksi->grand_total;
			$terbayar = $transaksi->terbayar;
			$kembalian = $terbayar - $grand_total;
			$result['success'] = true;
			$result['message'] = "Transaksi tersimpan";
			$result['kembalian'] = number_format($kembalian);
			$id = $save["id"];
            $result['id'] = $id;
			$result["url"] = base_url()."pos/print/".$id;
			$result["download"] = base_url()."pos/pdf/".$id;
		}
		$tipe_pembayaran = $this->input->post('tipe_pembayaran_id');
		if($tipe_pembayaran==-13&&$save['result']){
		    $this->token($result,$save["id"]);
        } else {
            echo json_encode($result);
        }

	}
    public function token($result,$id)
    {
        $transaksi = json_decode($this->input->post('transaksi'));
        $transaction_details = array(
            'order_id' => $_SESSION['redpos_login']['company_id']."-".$id,
            'gross_amount' => $this->string_to_number($transaksi->grand_total), // no decimal allowed for creditcard
        );
        $item_details = array ();
        foreach ($transaksi->item as $key) {

            $detail = array(
                'id' => $key->produk_id,
                'price' => $this->string_to_number($key->harga_eceran),
                'quantity' => $this->string_to_number($key->jumlah),
                'name' => $key->produk_nama
            );
            array_push($item_details,$detail);
        }
        // Optional
        $billing_address = array(
            'first_name'    => $this->input->post('guest_nama'),
            'last_name'     => "",
            'address'       => $this->input->post('guest_alamat'),
            'city'          => "",
            'postal_code'   => "",
            'phone'         => $this->input->post('guest_telepon'),
            'country_code'  => ''
        );


        // Optional
        $customer_details = array(
            'first_name'    => $this->input->post('guest_nama'),
            'last_name'     => "",

            'phone'         => $this->input->post('guest_telepon'),
            'billing_address'  => $billing_address,
            //'shipping_address' => $shipping_address
        );
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'minute',
            'duration'  => 2
        );

        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $item_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
        error_log(json_encode($transaction_data));
        $snapToken = $this->midtrans->getSnapToken($transaction_data);
        error_log($snapToken);
        $result['token'] = $snapToken;
        echo json_encode($result);
    }
	function print(){
		$id = $this->uri->segment(3);
		$trans = $this->pos->penjualan_by_id($id);
		$penjualan_detail = $this->pos->detailTransaksi($id);
		foreach ($penjualan_detail as $key){
			if($key->produk_nama == null){
				$key->produk_nama = $key->deskripsi;
			}
			if($key->produk_kode == null){
				$key->produk_kode = "custom";
			}
		}
		$trans->detail_transaksi = $penjualan_detail;
		$data["trans"] = $trans;
        $reseller = $this->getReseller();
        $data["reseller"] = $reseller;
		$this->load->view("admin/print_struct",$data);
	}

	function pos_pdf(){

		$id = $this->uri->segment(3);
		$trans = $this->pos->penjualan_by_id($id);
		$penjualan_detail = $this->pos->detailTransaksi($id);
		foreach ($penjualan_detail as $key){
			if($key->produk_nama == null){
				$key->produk_nama = $key->deskripsi;
			}
			if($key->produk_kode == null){
				$key->produk_kode = "custom";
			}
		}

		
		$trans->detail_transaksi = $penjualan_detail;
		$data["trans"] = $trans;
        $reseller = $this->getReseller();
        $data["reseller"] = $reseller;
		$no_faktur = $trans->no_faktur;
		
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'P']);
        $html = $this->load->view("admin/print_struct",$data, true);
        $mpdf->WriteHTML($html);
        $this->load->view('admin/print_struct',$data);

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'P']);
        $html = $this->load->view('admin/print_struct',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Struk Faktur '.$no_faktur .' - '. $date.".pdf","D");
	}

    function print_closing(){
        $log_pos_id = $this->uri->segment(3);
        $info = $this->pos->log_kasir_info($log_pos_id);
        $info->jumlah_transaksi = $this->pos->jumlah_transaksi_by_log($log_pos_id);
        $info->total_transaksi = $this->idr_currency($this->pos->total_transaksi_by_log($log_pos_id)->total);
        $info->kas_awal = $this->idr_currency($info->kas_awal);
        $info->kas_akhir = $this->idr_currency($info->kas_akhir);
        $data["info"] = $info;
        $this->load->view("admin/pos/print_closing",$data);
    }
    function get_unit_satuan(){
        $idproduk = $this->input->post('id');
        $data = $this->assembly->get_unit_satuan($idproduk);
        foreach ($data as $key){
            $key->ar_minimal_pembelian = "";
            $key->ar_harga ="";
            $harga = $this->harga_produk->harga_produk_satuan($idproduk,$key->satuan_id);

            foreach ($harga as $each) {
                $key->ar_minimal_pembelian .= $each->minimal_pembelian."|";
                $key->ar_harga .= $each->harga."|";
            }
            $key->ar_minimal_pembelian .= "0";
            $key->ar_harga .= $key->harga_jual;
        }
        echo json_encode($data);
    }
    function normalisasi(){
        $data = $this->pos->normalisasi();
        foreach ($data as $item){
            $temp = array("hpp"=>$item->hpp_global);
            $this->pos->update_penjualan_produk($item->produk_id,$temp);
        }
    }

}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
