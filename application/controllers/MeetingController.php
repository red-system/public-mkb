<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeetingController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
        $this->load->model('meeting','',true);
        $this->load->model('kehadiran_meeting','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/meeting.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Meeting MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "meeting";
        $data['page'] = "meeting";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"meeting_date"));
        array_push($column, array("data"=>"meeting_name"));
        array_push($column, array("data"=>"meeting_description"));
        array_push($column, array("data"=>"biaya_lbl"));
        array_push($column, array("data"=>"meeting_status_lbl"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array()));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();

        foreach ($akses_menu['meeting'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        // $action['view'] = true;
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/meeting/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->meeting->meeting_count_all();
        $result['iTotalDisplayRecords'] = $this->meeting->meeting_count_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->meeting->meeting_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {

            $reseller_id = $_SESSION["redpos_login"]["reseller_id"];
            $where = [
                'reseller_id' => $reseller_id,
                'meeting_id' => $key->meeting_id,
            ];
            $check_meeting = $this->meeting->meeting_check($where)->num_rows();
            if ($check_meeting == 0) {
                $key->meeting_status_lbl = "Belum ditentukan";
                // $key->biaya_lbl = number_format(0);
                $key->biaya_lbl = 'Free';
                
                if ($key->meeting_date >= date('Y-m-d')) {
                    $key->kehadiran = true;
                }else{
                    $key->deny_kehadiran = true;
                }
                
            }else{
                $kehadiran = $this->meeting->meeting_check($where)->row();
                $key->meeting_status_lbl = $kehadiran->status_kehadiran;
                $key->deny_kehadiran = true;
                if ($kehadiran->status_kehadiran == 'Online') {
                    // $key->biaya_lbl = number_format(100000);
                    $key->biaya_lbl = 'Free';
                }else{
                    // $key->biaya_lbl = number_format(50000);
                    $key->biaya_lbl = 'Free';
                }
                
            }
            
            if($key->meeting_date != null){
                $time = strtotime($key->meeting_date);
                $key->meeting_date = date('d-m-Y',$time);
            }

            

            // $key->meeting_description_lbl = (strlen($key->meeting_description) >= 50 ) ? $this->main->short_desc($key->meeting_description) : $key->meeting_description;
            
            // $key->edit_url = base_url().'meeting/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->meeting_id));
            // $key->view_url = base_url().'meeting/detail-meeting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->meeting_id));
            $key->no = $i;
            
            $i++;
            // $key->delete_url = base_url().'meeting/delete/';
            $key->row_id = $key->meeting_id;
            $key->action = null;
            
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
        array_push($this->js, "vendors/general/tinymce/js/tinymce/tinymce.min.js");
        array_push($this->js, "script/admin/meeting.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Meeting MKB < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "meeting";
        $data['page'] = $this->uri->segment(1);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/meeting/add');
        $this->load->view('admin/static/footer');
    }
    
    function save_add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['meeting_date'] = $this->input->post('meeting_date');
        $data['meeting_title'] = $this->input->post('meeting_title');
        $data['meeting_description'] = $this->input->post('meeting_description');
        $data['meeting_status'] = $this->input->post('meeting_status');
        $insert = $this->meeting->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }

    function save_kehadiran_add(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['meeting_id'] = $this->input->post('meeting_id');
        $data['reseller_id'] = $this->input->post('reseller_id');
        $data['status_kehadiran'] = $this->input->post('status_kehadiran');
        $insert = $this->kehadiran_meeting->insert($data);
        $_SESSION["meeting_notif"] = false;
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }

    function edit(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $meeting = $this->meeting->row_by_id($id);
        if ($meeting != null) {
            $data['meeting'] = $meeting;
            
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "vendors/general/tinymce/js/tinymce/tinymce.min.js");
            array_push($this->js, "script/admin/meeting.js");

            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "meetingikasi < ".$_SESSION["redpos_company"]['company_name'];
            $data['parrent'] = "meeting";
            $data['page'] = $this->uri->segment(1);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/meeting/edit');
            $this->load->view('admin/static/footer');
        }else {

            redirect('404_override','refresh');
        }
    }
    function save_edit(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $data['meeting_date'] = $this->input->post('meeting_date');
        $data['meeting_title'] = $this->input->post('meeting_title');
        $data['meeting_description'] = $this->input->post('meeting_description');
        $data['meeting_status'] = $this->input->post('meeting_status');

        $meeting_id = $this->input->post('meeting_id');

        $update = $this->meeting->update_by_id('meeting_id',$meeting_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Berhasil menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->meeting->delete_by_id("meeting_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function detail(){
        $id = $this->input->post('meeting_id');
        $temp = $this->meeting->row_by_id($id);
        $no = 0;
        $no_produk = 0;
        $post = array();
        
        if($temp->meeting_date != null){
            $x = strtotime($temp->meeting_date);
            $temp->meeting_date = date("d-m-Y",$x);
        }
        
        echo json_encode($temp);
    }

    function detail_meeting(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);

        if ($_SESSION["status_meeting"] == true) {
            $reseller_id = $_SESSION["redpos_login"]["reseller_id"];
            $where = [
                'reseller_id' => $reseller_id,
                'meeting_id' => $id
            ];
            $check_meeting = $this->meeting->meeting_check($where)->num_rows();
            if ($check_meeting == 0) {
                $data_meeting['meeting_id'] = $id;
                $data_meeting['reseller_id'] = $reseller_id;
                $insert_meeting = $this->meeting_detail->insert($data_meeting);
            }

        }

        $meeting = $this->meeting->row_by_id($id);
        if ($meeting != null) {
            $data['meeting'] = $meeting;
            
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->js, "vendors/general/tinymce/js/tinymce/tinymce.min.js");
            array_push($this->js, "script/admin/meeting.js");

            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "meetingikasi < ".$_SESSION["redpos_company"]['company_name'];
            $data['parrent'] = "meeting";
            $data['page'] = $this->uri->segment(1);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/meeting/detail');
            $this->load->view('admin/static/footer');
        }else {

            redirect('404_override','refresh');
        }
    }

}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */