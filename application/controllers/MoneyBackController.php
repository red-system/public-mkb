<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MoneyBackController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('reseller','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('bonus','',true);
        $this->load->model('produk','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('money_back','',true);
        $this->load->model('money_back_detail','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/money_back.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Money Back Guarantee < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "money-back";
        $data['page'] = $this->uri->segment(1);
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $data['reseller'] = $reseller;
        $lokasi_reseller = $_SESSION['redpos_login']['lokasi_id'];
        $deposit = $this->po_produk->total_deposit_reseller($lokasi_reseller);
        $bonus = $this->bonus->getTotalBonus($reseller_id);
        $money = $this->money_back->row_by_field('reseller_id',$reseller_id);
        $data['total_deposit'] = $deposit;
        $data['total_bonus'] = $bonus;
        $syarat = "Tidak";
        $curent = date("Y-m-d");
        $tanggal_param = $reseller->money_back_guarantee;
        if($curent>=$tanggal_param&&$bonus<$deposit){
            $syarat = "Iya";
        }
        $data["syarat"] = $syarat;
        if($money!=null){
            $data['potongan_lbl'] = $money->potongan;
            $data['total'] = $money->uang_dikembalikan;
            $data['grand_total'] = $money->total_pengembalian;
            $produk = $this->money_back_detail->data_by_id($money->money_back_guarantee_id);
            $data['produk'] = $produk;
        } else {
            $total_data_produk = $this->produk->produk_count_all();
            $produk = $this->produk->produk_list(0,$total_data_produk,"");
            $total = 0;
            foreach ($produk as $item){
                $item->subtotal = $item->hpp_global*$item->jumlah_lokasi;
                $total += $item->subtotal;
            }
            $potongan  = 0;
            $potongan_lbl = 0;
            if ($reseller->type == "super agen"){
                $potongan  = 0.05;
                $potongan_lbl = 5;
            }
            $data['potongan'] = $potongan;
            $data['potongan_lbl'] = $potongan_lbl;
            $gran_total = $total -($total * $potongan);
            $data['total'] = $total;
            $data['grand_total'] = $gran_total;
            $data['produk'] = $produk;
        }


        $data['money'] = $money;
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/money-back/index');
        $this->load->view('admin/static/footer');
    }
    function submit(){
        $result['success'] = false;
        $result['message'] = "Gagal Menyimpan data";
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $tanggal_request = date("Y-m-d");
        $lokasi_reseller = $_SESSION['redpos_login']['lokasi_id'];
        $deposit = $this->po_produk->total_deposit_reseller($lokasi_reseller);
        $bonus = $this->bonus->getTotalBonus($reseller_id);
        $total_data_produk = $this->produk->produk_count_all();
        $produk = $this->produk->produk_list(0,$total_data_produk,"");
        $total = 0;
        $detail = array();
        foreach ($produk as $item){
            $item->subtotal = $item->hpp_global*$item->jumlah_lokasi;
            $total += $item->subtotal;
            $detail_item = array();
            $detail_item['produk_id'] = $item->produk_id;
            $detail_item['hpp_global'] = $item->hpp_global;
            $detail_item['jumlah_lokasi'] = $item->jumlah_lokasi;
            $detail_item['subtotal'] = $item->subtotal;
            $detail_item['created_at'] = date("Y-m-d H:i:s");
            array_push($detail,$detail_item);
        }
        $potongan  = 0;
        $potongan_lbl = 0;
        if ($reseller->type == "super agen"){
            $potongan  = 0.05;
            $potongan_lbl = 5;
        }
        $gran_total = $total -($total * $potongan);
        $money = array();
        $money['reseller_id'] = $reseller_id;
        $money['tanggal_request'] = $tanggal_request;
        $money['user_role_id'] = $_SESSION['redpos_login']['user_role_id'];
        $money['uang_dikembalikan'] = $total;
        $money['potongan'] = $potongan_lbl;
        $money['total_pengembalian'] = $gran_total;
        $insert = $this->money_back->insert($money);
        $last_id = $this->money_back->last_id();
        $i = 0;
        foreach ($detail as $item){
            $detail[$i]['money_back_guarantee_id'] = $last_id;
            $i++;
        }
        $this->money_back_detail->insert_batch($detail);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Berhasil Menyimpan data";
            $_SESSION['redpos_login']['user_role_id'] = 9;
            $data['user_role_id'] = 9;
            $this->db->where('reseller_id',$reseller_id);
            $this->db->update('mykindofbeauty_master.login',$data);
        }
        echo json_encode($result);

    }
    function batal(){
        $result['success'] = false;
        $result['message'] = "Gagal Menyimpan data";
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $money = $this->money_back->row_by_field('reseller_id',$reseller_id);
        if($money->status_pengiriman!="Terkirim"){
            $delete = $this->money_back->delete_by_id('reseller_id',$reseller_id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Berhasil Menyimpan data";
                $_SESSION['redpos_login']['user_role_id'] = $money->user_role_id;
                $data['user_role_id'] = $money->user_role_id;
                $this->db->where('reseller_id',$reseller_id);
                $this->db->update('mykindofbeauty_master.login',$data);
            }
        }
        echo json_encode($result);
    }
    function print(){
        $reseller = $this->getReseller();
        $reseller_id = $reseller->reseller_id;
        $data['reseller'] = $reseller;
        $lokasi_reseller = $_SESSION['redpos_login']['lokasi_id'];
        $deposit = $this->po_produk->total_deposit_reseller($lokasi_reseller);
        $bonus = $this->bonus->getTotalBonus($reseller_id);
        $money = $this->money_back->row_by_field('reseller_id',$reseller_id);
        $data['total_deposit'] = $deposit;
        $data['total_bonus'] = $bonus;
        $syarat = "Tidak";
        $curent = date("Y-m-d");
        $tanggal_param = $reseller->money_back_guarantee;
        if($curent>=$tanggal_param&&$bonus<$deposit){
            $syarat = "Iya";
        }
        $data["syarat"] = $syarat;
        if($money!=null){
            $data['potongan_lbl'] = $money->potongan;
            $data['total'] = $money->uang_dikembalikan;
            $data['grand_total'] = $money->total_pengembalian;
            $produk = $this->money_back_detail->data_by_id($money->money_back_guarantee_id);
            $data['produk'] = $produk;
        } else {
            $total_data_produk = $this->produk->produk_count_all();
            $produk = $this->produk->produk_list(0,$total_data_produk,"");
            $total = 0;
            foreach ($produk as $item){
                $item->subtotal = $item->hpp_global*$item->jumlah_lokasi;
                $total += $item->subtotal;
            }
            $potongan  = 0;
            $potongan_lbl = 0;
            if ($reseller->type == "super agen"){
                $potongan  = 0.05;
                $potongan_lbl = 5;
            }
            $data['potongan'] = $potongan;
            $data['potongan_lbl'] = $potongan_lbl;
            $gran_total = $total -($total * $potongan);
            $data['total'] = $total;
            $data['grand_total'] = $gran_total;
            $data['produk'] = $produk;
        }


        $data['money'] = $money;
        $this->load->view('admin/money-back/print',$data);
    }

}

/* End of file Lokasi.php */
/* Location: ./application/controllers/Lokasi.php */