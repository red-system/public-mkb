<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterHomeController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('master');
        if(!isset($_SESSION['master'])){
            redirect(base_url().'master/login');
        }
    }

    public function index()
    {

        $data['css'] = $this->master->css;
        $data['js'] = $this->master->js;
        $this->load->view('master/static/header',$data);
        $this->load->view('master/static/sidebar');
    }
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
