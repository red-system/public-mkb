<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest','',true);
        $this->load->model('lokasi','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "script/app2.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Group < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "guest";
        $data['page'] = $this->uri->segment(1);
        $sumColumn = array(6,7,8);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"username"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"type_group"));
        array_push($column, array("data"=>"status"));
        array_push($column, array("data"=>"subdistrict_name"));
        array_push($column, array("data"=>"total_bonus"));
        array_push($column, array("data"=>"total_penjualan"));
        array_push($column, array("data"=>"total_po"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['member'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $action["detail_member"] = true;
        $data['sumColumn'] = json_encode($sumColumn);
        $data['action'] = json_encode($action);
        $data['lokasi'] = $this->lokasi->all_list();
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/group/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        if(isset($_GET["columns"][6]["search"]["value"]) && $_GET["columns"][6]["search"]["value"] != ""){
            $temp = explode("|", $_GET["columns"][6]["search"]["value"]);
            $_GET['tanggal_start'] = (isset($temp[1])?$temp[0]:date("Y-m-d",0));
            $_GET['tanggal_end'] = (isset($temp[1])?$temp[1]:date("Y-m-d"));
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $login = $this->db->where("lokasi_id",$_SESSION["redpos_login"]["lokasi_id"])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $result['iTotalRecords'] = $this->reseller->group_count_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->reseller->group_count_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->group_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        if($data!=null){
            foreach ($data as $key) {
                $key->no = $i;
                $i++;
                $key->delete_url = base_url().'guest/delete/';
                $key->row_id = $key->reseller_id;
                $key->detail_member_url = base_url().'detail-member/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->reseller_id));
                $key->action = null;
                $key->type_group = "Downline";
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file JenisBahanController.php */
/* Location: ./application/controllers/JenisBahanController.php */