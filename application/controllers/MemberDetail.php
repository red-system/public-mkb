<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MemberDetail extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('color','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Member Detail < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "group";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"type"));
        array_push($column, array("data"=>"waktu"));
        array_push($column, array("data"=>"total"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['master_data']['jenis-bahan'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
        $id = $this->encryption->decrypt($url);
        $data['id'] = $id;
        $this->load->model('reseller','',true);
        $data['reseller'] = $this->reseller->row_by_id($id);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/member_detail/index');
        $this->load->view('admin/static/footer');
    }
    function table_list(){
        $id = $this->uri->segment(3);
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $this->load->model('aktivitas_group','',true);

        $result['iTotalRecords'] = $this->aktivitas_group->all($id);
        $result['iTotalDisplayRecords'] = $this->aktivitas_group->all($id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->aktivitas_group->list($start,$length,$id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->waktu = date("Y-m-d",strtotime($key->waktu));
            $key->total = number_format($key->total);
            $key->no = $i;
            $i++;
            $key->delete_url = base_url().'jenis-bahan/delete/';
            $key->row_id = $key->color_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file ColorController.php */
/* Location: ./application/controllers/ColorController.php */
