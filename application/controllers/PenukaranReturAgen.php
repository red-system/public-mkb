<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenukaranReturAgen extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('pos','',true);
        $this->load->model('bonus','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('reseller','',true);
        $this->load->model('harga_produk','',true);
        $this->load->model('tipe_pembayaran','',true);
        $this->load->model('log_kasir','',true);
        $this->load->model('unit','',true);
        $this->load->model('hutang_retur','',true);
        $this->load->model('piutang_retur','',true);
        $this->load->model('po_produk','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('voucher_retur','',true);
        $this->load->model('retur_agen','',true);
        $this->load->model('retur_agen_detail','',true);
        $this->load->library('main');
        $this->load->model('assembly','',true);
        if($_SESSION['redpos_login']['pay']==1){
            $params = array('server_key' => $_SESSION['redpos_login']['pay-s'], 'production' => $_SESSION['redpos_login']['pay-prod']);
            $this->load->library('midtrans');
            $this->midtrans->config($params);
            $this->load->helper('url');
        }
    }
    public function index(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "script/admin/pos_.js");
        array_push($this->js, "script/admin/penukaran.js");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Penukaran Retur < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "penukaran-retur-agen";
        $data['page'] = 'penukaran-retur-agen';
        $data['lokasi'] = $this->lokasi->all_list();
        $data['pay'] = $_SESSION['redpos_login']['pay'] == 0 ? false : true;
        unset($_SESSION['pos']);
        $data['faktur'] = $this->pos->getFakturCode();
        $data['urutan'] = $this->pos->getUrutan();
        $data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/penukaran-retur/index');
        $this->load->view('admin/static/footer');
    }
    function check(){
        $kode_po = $this->input->post("po_kode");
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $check = $this->voucher_retur->checkByCode($kode_po);

        if($check!=null){
            $id = $check->retur_agen_id;
            $temp = $this->voucher_retur->retur_by_id($id);
            $no = 0;
            $no_produk = 0;
            $post = array();
            $temp->item = $this->retur_agen->retur_agen_detail_by_id($id);
            $tipe_pembayaran = $this->tipe_pembayaran->row_by_id($temp->tipe_pembayaran);
            $temp->metode_pembayaran = $tipe_pembayaran->tipe_pembayaran_nama." ".$tipe_pembayaran->no_akun;
            $temp->bukti_url = $this->config->item('dev_storage_image').$temp->bukti_pembayaran;
            if($temp->tanggal_pemesanan != null){
                $x = strtotime($temp->tanggal_pemesanan);
                $temp->tanggal_pemesanan = date("Y-m-d",$x);
            }
            if($temp->tanggal_penerimaan != null){
                $x = strtotime($temp->tanggal_penerimaan);
                $temp->tanggal_penerimaan = date("Y-m-d",$x);
            }
            if($temp->created_at != null){
                $time = strtotime($temp->created_at);
                $temp->created_at = date('d-m-Y H:i:s',$time);
            }
            if($temp->updated_at != null){
                $time = strtotime($temp->updated_at);
                $temp->updated_at = date('d-m-Y H:i:s',$time);
            }
            foreach ($temp->item as $key) {
                $key->harga = 0;
                $key->jumlah = number_format($key->jumlah);
                $key->sub_total = 0;
                $this->db->select('sum(stock_produk_qty) as stock');
                $this->db->where('stock_produk_lokasi_id',$_SESSION['redpos_login']['lokasi_id']);
                $this->db->where('produk_id',$key->produk_id);
                $key->stock = $this->db->get('mykindofbeauty_kemiri.stock_produk')->row()->stock;
                $key->indicator = $key->jumlah <= $key->stock ? 0 : 1;

            }

            $result['success'] = true;
            $result['detail'] = $temp;
            $result['message'] = 'Berhasil mendapatkan data';
            echo json_encode($result);
        }else{
            $result['success'] = false;
            $result['message'] = 'Voucher Tidak Tersedia';
            echo json_encode($result);
        }


    }
    function penukaran_save(){
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $kode_po = $this->input->post("po_kode");
        $check = $this->voucher_retur->checkByCode($kode_po);
        $result['success'] = True;
        $result['message'] = 'Berhasil menukarkan voucher';
        $voucher = array();
        $voucher['tanggal_penukaran'] = date("Y-m-d");
        $voucher['status_penukaran'] = "Terpakai";
        $voucher['reseller_penukar'] = $reseller_id;
        $this->voucher_retur->update_by_id('voucher_retur_id',$check->voucher_retur_id,$voucher);
        $this->load->model("po_produk","",true);
        $po_produk_item = $this->retur_agen_detail->data_by_id($check->retur_agen_id);
        $po_produk_id = $check->retur_agen_id;
        $po_data = $this->retur_agen->row_by_id($po_produk_id);
        $reseller_punya_id = $po_data->reseller_id;
        $login2 = $this->db->where("reseller_id",$reseller_punya_id)->get("mykindofbeauty_master.login")->row();
        $lokasi_reseller = $login2->lokasi_id;
        foreach ($po_produk_item as $key) {
            $data = array();
            $data["stock_produk_qty"] = $key->jumlah;
            $data["produk_id"] = $key->produk_id;
            $data["stock_produk_lokasi_id"] =$lokasi_reseller;
            $data["year"] = date("y");
            $data["month"] = date("m");
            $data["po_id"] = $po_produk_id;
            $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
            $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
            $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
            $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
            $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
            $data["hpp"] = 0;
            $insert = $this->stock_produk->insert($data);
            $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
            $row_stock = $this->stock_produk->stock_by_location_produk_id($lokasi_id,$key->produk_id);
            $jumlah_bahan = $key->jumlah;
            foreach ($row_stock as $row) {
                $data = array();
                $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                $stock_out = $jumlah_bahan;
                if($row->stock_produk_qty < $jumlah_bahan){
                    $jumlah = 0;
                    $stock_out = $row->stock_produk_qty;
                }
                $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                $data["stock_produk_qty"] = $jumlah;
                $stock_produk_id = $row->stock_produk_id;
                $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
                if($jumlah_bahan <= 0){
                    break;
                }
            }
            $hutang_produks = array();
            $hutang_produks['reseller_id'] = $reseller_id;
            $hutang_produks['retur_agen_id'] = $po_produk_id;
            $hutang_produks['voucher_retur_id'] = $check->voucher_retur_id;
            $hutang_produks['produk_id'] = $key->produk_id;
            $hutang_produks['jumlah'] = $key->jumlah;
            $this->hutang_retur->insert($hutang_produks);
            $piutang_produks = array();
            $hutang_produks['reseller_id'] = $reseller_id;
            $hutang_produks['retur_agen_id'] = $po_produk_id;
            $hutang_produks['voucher_retur_id'] = $check->voucher_retur_id;
            $hutang_produks['produk_id'] = $key->produk_id;
            $hutang_produks['jumlah'] = $key->jumlah;
            $this->piutang_retur->insert($hutang_produks);
            //here
        }


        echo json_encode($result);
    }


}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
