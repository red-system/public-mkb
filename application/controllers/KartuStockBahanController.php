<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	require 'vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;

class KartuStockBahanController extends MY_Controller {
	

	public function __construct()
	{
		parent::__construct();
		$this->load->model('stock_bahan','',true);
		$this->load->model('bahan','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Kartu stok bahan < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$data['bahan'] = $this->bahan->all_list();
		array_push($column, array("data"=>"bahan_nama_in"));
		array_push($column, array("data"=>"satuan_nama_in"));
		array_push($column, array("data"=>"lokasi_nama_in"));
		array_push($column, array("data"=>"stock_in"));
		array_push($column, array("data"=>"invoice"));
		array_push($column, array("data"=>"keterangan_in"));
		array_push($column, array("data"=>"tanggal"));
		array_push($column, array("data"=>"bahan_nama_out"));
		array_push($column, array("data"=>"satuan_nama_out"));
		array_push($column, array("data"=>"lokasi_nama_out"));
		array_push($column, array("data"=>"stock_out"));
		array_push($column, array("data"=>"keterangan_out"));
		array_push($column, array("data"=>"total_stock"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(3,10,12)));
		$data['first_date'] = $this->stock_bahan->first_date();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/kartu_stock');
		$this->load->view('admin/static/footer');
	}
	function list(){

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_bahan->arus_stock_bahan_count();
		$result['iTotalDisplayRecords'] = $this->stock_bahan->arus_stock_bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_bahan->arus_stock_bahan_list($start,$length,$query);
		$displayData = array();
		$empty = array("bahan_nama_in"=>"","satuan_nama_in"=>"","lokasi_nama_in"=>"","stock_in"=>"","invoice"=>"","keterangan_in"=>"","bahan_nama_out"=>"","satuan_nama_out"=>"","lokasi_nama_out"=>"","stock_out"=>"","keterangan_out"=>"","total_stock"=>"");
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$temp = $empty;
			if ($key->stock_in>0){
				$temp["bahan_nama_in"] = $key->bahan_nama;
				$temp["satuan_nama_in"] = $key->satuan_nama;
				$temp["lokasi_nama_in"] = $key->lokasi_nama;
				$temp["stock_in"] = number_format($key->stock_in,2);
				$temp["invoice"] = "";
				$temp["keterangan_in"] = $key->keterangan;
			} else {
				$temp["bahan_nama_out"] = $key->bahan_nama;
				$temp["satuan_nama_out"] = $key->satuan_nama;
				$temp["lokasi_nama_out"] = $key->lokasi_nama;
				$temp["stock_out"] = number_format($key->stock_out,2);
				$temp["keterangan_out"] = $key->keterangan;				
			}
			$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
			$temp["total_stock"] = number_format($key->last_stock,2);
			array_push($displayData, $temp);
		}
		$result['aaData'] = $displayData;
		echo json_encode($result);
	}
	function pdf(){
		$data["start_date"] = $this->input->get('start_date');
		$data["end_date"] = $this->input->get('end_date');
		if($this->input->get('bahan_id')!="" && $this->input->get('bahan_id')!="all"){
			$data["bahan_nama"] = $this->bahan->row_by_id($this->input->get('bahan_id'))->bahan_nama;
		}
		$start = 0;
		$data['first_date'] = $this->stock_bahan->first_date();
		$length = $this->stock_bahan->arus_stock_bahan_count_filter('');
		$list =  $this->stock_bahan->arus_stock_bahan_list($start,$length,'');
		$displayData = array();
		$empty = array("bahan_nama_in"=>"","satuan_nama_in"=>"","lokasi_nama_in"=>"","stock_in"=>"","invoice"=>"","keterangan_in"=>"","bahan_nama_out"=>"","satuan_nama_out"=>"","lokasi_nama_out"=>"","stock_out"=>"","keterangan_out"=>"","total_stock"=>"");
		foreach ($list as $key) {
			$temp = $empty;
			if ($key->stock_in>0){
				$temp["bahan_nama_in"] = $key->bahan_nama;
				$temp["satuan_nama_in"] = $key->satuan_nama;
				$temp["lokasi_nama_in"] = $key->lokasi_nama;
				$temp["stock_in"] = number_format($key->stock_in);
				$temp["invoice"] = "";
				$temp["keterangan_in"] = $key->keterangan;
			} else {
				$temp["bahan_nama_out"] = $key->bahan_nama;
				$temp["satuan_nama_out"] = $key->satuan_nama;
				$temp["lokasi_nama_out"] = $key->lokasi_nama;
				$temp["stock_out"] = number_format($key->stock_out);
				$temp["keterangan_out"] = $key->keterangan;				
			}
						$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
			$temp["total_stock"] = number_format($key->last_stock);
			array_push($displayData, $temp);
		}
		$css = array();
		array_push($css, base_url()."assets/vendors/custom/datatables/datatables.bundle.css");
		array_push($css, base_url()."assets/demo/default/base/style.bundle.css");
		$data["css"] = $css;
		$data['list'] = $displayData;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/pdf_kartu_stock_bahan',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = " ".$this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Kartu Stok Bahan '.$date.".pdf","D");
        
	}
	function excel(){
		$start = 0;
		$length = $this->stock_bahan->arus_stock_bahan_count_filter('');
		$list =  $this->stock_bahan->arus_stock_bahan_list($start,$length,'');
		$displayData = array();
		$empty = array("bahan_nama_in"=>"","satuan_nama_in"=>"","lokasi_nama_in"=>"","stock_in"=>"","invoice"=>"","keterangan_in"=>"","bahan_nama_out"=>"","satuan_nama_out"=>"","lokasi_nama_out"=>"","stock_out"=>"","keterangan_out"=>"","total_stock"=>"");
		foreach ($list as $key) {
			$temp = $empty;
			if ($key->stock_in>0){
				$temp["bahan_nama_in"] = $key->bahan_nama;
				$temp["satuan_nama_in"] = $key->satuan_nama;
				$temp["lokasi_nama_in"] = $key->lokasi_nama;
				$temp["stock_in"] = number_format($key->stock_in);
				$temp["invoice"] = "";
				$temp["keterangan_in"] = $key->keterangan;
			} else {
				$temp["bahan_nama_out"] = $key->bahan_nama;
				$temp["satuan_nama_out"] = $key->satuan_nama;
				$temp["lokasi_nama_out"] = $key->lokasi_nama;
				$temp["stock_out"] = number_format($key->stock_out);
				$temp["keterangan_out"] = $key->keterangan;				
			}
						$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
			$temp["total_stock"] = number_format($key->last_stock);
			array_push($displayData, $temp);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Kartu Stock Bahan')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    );
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );
		// Add some data
		$data['first_date'] = $this->stock_bahan->first_date();
		$tanggal =$data['first_date']. " s/d ".date("d-m-Y");
		$bahan = "Semua Bahan";
		if($this->input->get('bahan_id')!="" && $this->input->get('bahan_id')!="all"){
			$bahan = $this->bahan->row_by_id($this->input->get('bahan_id'))->bahan_nama;
		}
		if($this->input->get('start_date')!="") $tanggal = $this->input->get('start_date')." s/d ".$this->input->get('end_date');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A6','Stok Masuk');
		$spreadsheet->getActiveSheet()->mergeCells('A6:F6');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H6','Stok Keluar');
		$spreadsheet->getActiveSheet()->mergeCells('H6:L6');
		$spreadsheet->getActiveSheet()->mergeCells('G6:G7');
		$spreadsheet->getActiveSheet()->mergeCells('M6:M7');
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'Bahan')
		->setCellValue('B7', 'Satuan')
		->setCellValue('C7', 'Lokasi')
		->setCellValue('D7', 'Masuk')
		->setCellValue('E7', 'Invoice')
		->setCellValue('F7', 'Keterangan')
		->setCellValue('G6', 'Tanggal')
		->setCellValue('H7', 'Bahan')
		->setCellValue('I7', 'Satuan')
		->setCellValue('J7', 'Lokasi')
		->setCellValue('K7', 'Keluar')
		->setCellValue('L7', 'Keterangan')
		->setCellValue('M6', 'Stok Akhir')
		;
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(12);
		$spreadsheet->getActiveSheet()->getStyle("A6:M7")->applyFromArray($style);
		// Miscellaneous glyphs, UTF-8
		$i=8; foreach($displayData as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key["bahan_nama_in"])
		->setCellValue('B'.$i, $key["satuan_nama_in"])
		->setCellValue('C'.$i, $key["lokasi_nama_in"])
		->setCellValue('D'.$i, $key["stock_in"])
		->setCellValue('E'.$i, $key["invoice"])
		->setCellValue('F'.$i, $key["keterangan_in"])
		->setCellValue('G'.$i, $key["tanggal"])
		->setCellValue('H'.$i, $key["bahan_nama_out"])
		->setCellValue('I'.$i, $key["satuan_nama_out"])
		->setCellValue('J'.$i, $key["lokasi_nama_out"])
		->setCellValue('K'.$i, $key["stock_out"])
		->setCellValue('L'.$i, $key["keterangan_out"])
		->setCellValue('M'.$i, $key["total_stock"]);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A6:M".$i)->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A6:M7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('F8:F'.$i)->getAlignment()->setWrapText(true);
	    $spreadsheet->getActiveSheet()->getStyle('L8:L'.$i)->getAlignment()->setWrapText(true);	    
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Kartu Stok Bahan '.date('d-m-Y'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet

		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('B1',$_SESSION["redpos_company"]['company_name']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('B2',$_SESSION["redpos_company"]['company_address']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('B3',$_SESSION["redpos_company"]['company_phone']);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('M1','Kartu Stok Produk');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('M2',$tanggal);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('M3',$bahan);
		$spreadsheet->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("M1:M4")->applyFromArray($right);
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Kartu Stock Bahan '.date("d-m-Y").'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}

/* End of file KartuStockBahanController.php */
/* Location: ./application/controllers/KartuStockBahanController.php */