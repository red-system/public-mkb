<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	require 'vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
class AccountingController extends MY_Controller {
	var $g_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('po_produk','',true);
		$this->load->model('bonus','',true);
		$this->load->model('hutang_produk','',true);
		$this->load->model('reseller','',true);
		$this->load->model('pembayaran_hutang_produk','',true);
		$this->load->model('pengiriman_produk','',true);
		$this->load->model('penerimaan_produk','',true);
		$this->load->model('penerimaan_bahan','',true);
	}

	public function index()
	{
		$segment1 = $this->uri->segment(2);
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "script/app2.js");

		$this->g_data["css"] = $this->css;
		$this->g_data["js"] = $this->js;
		$this->g_data["meta_title"] = "Accounting < ".$_SESSION["redpos_company"]['company_name'];;
		$this->g_data['parrent'] = "accounting";
		$this->g_data['page'] = ucwords(str_replace("-", " ", $this->uri->segment(1)));		
		switch ($segment1) {
			case 'pendapatan-deposit':
				$this->pendapatan_deposit();			
				break;
			case 'pendapatan-penukaran-produk':
				$this->pendapatan_penukaran_produk();			
				break;
			case 'accounting-bonus':
				$this->accounting_bonus();			
				break;
			case 'accounting-ferby':
				$this->accounting_ferby();			
				break;
			case 'bonus-withdraw':
				$this->bonus_withdraw();			
				break;
			case 'daftar-reseller-aktif':
				$this->daftar_reseller_aktif();			
				break;
			case 'hutang-produk-keseluruhan':
				$this->hutang_produk_keseluruhan();			
				break;
			case 'produk-keluar-by-deposit':
				$this->produk_keluar_by_deposit();			
				break;
			case 'produk-keluar-by-hutang-produk':
				$this->produk_keluar_by_hutang_produk();			
				break;
			case 'sisa-hutang-produk':
				$this->sisa_hutang_produk();			
				break;
			case 'withdraw-cash':
				$this->withdraw_cash();			
				break;
			case 'withdraw-produk':
				$this->withdraw_produk();			
				break;
			case 'pengiriman-by-po':
				$this->pengiriman_by_po();			
				break;
			case 'pengiriman-by-pembayaran-hutang':
				$this->pengiriman_by_pembayaran_hutang();			
				break;
			case 'penerimaan-produk-produsen':
				$this->penerimaan_produk_produsen();			
				break;
			case 'history-hpp-produk':
				$this->history_hpp_produk();			
				break;
			case 'history-hpp-bahan':
				$this->history_hpp_bahan();			
				break;
            case 'omset-leader':
                $this->omset_leader();
                break;
            case 'omset-leader-real':
                $this->omset_leader_real();
                break;
            case 'peringkat-leader':
                $this->peringkat_leader();
                break;
            case 'pph-21':
                $this->pph21();
                break;
			default:
				redirect('404_override','refresh');
				break;
		}
	}
	
	function pendapatan_deposit(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->pendapatan_deposit_list();			
				break;
			case 'pdf':
				$this->pendapatan_deposit_pdf();			
				break;
			case 'excel':
				$this->pendapatan_deposit_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,6);
				$sumColumn = array(6);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_produk_no"));
				array_push($column, array("data"=>"tanggal_order"));
				array_push($column, array("data"=>"tanggal_pengiriman"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"pcs"));
				array_push($column, array("data"=>"grand_total_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'pendapatan-deposit';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/pendapatan_deposit');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function pendapatan_deposit_list(){
		
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->pendapatan_deposit_all();
		$result['iTotalDisplayRecords'] = $this->po_produk->pendapatan_deposit_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->pendapatan_deposit_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_order != null){
				$time = strtotime($key->tanggal_order);
				$key->tanggal_order = date('d-m-Y',$time);
			}
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->no = $i;
            $i++;

            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->pcs = ($key->pcs);
			
			$key->row_id = $key->po_produk_no;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function pendapatan_deposit_pdf(){
		
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_produk->pendapatan_deposit_filter($query);
		$list =  $this->po_produk->pendapatan_deposit_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
            if($key->tanggal_order != null){
				$time = strtotime($key->tanggal_order);
				$key->tanggal_order = date('d-m-Y',$time);
			}
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->no = $i;
            $i++;
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->pcs = ($key->pcs);
			$key->row_id = $key->po_produk_no;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_pendapatan_deposit_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Pendapatan Deposit Super Reseller '.$date.".pdf","D");
	}
	function pendapatan_deposit_excel(){
	
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_produk->pendapatan_deposit_filter($query);
		$list =  $this->po_produk->pendapatan_deposit_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->tanggal_order != null){
				$time = strtotime($key->tanggal_order);
				$key->tanggal_order = date('d-m-Y',$time);
			}
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->no = $i;
            $i++;
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->pcs = ($key->pcs);
			$key->row_id = $key->po_produk_no;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Pendapatan Deposit Super Reseller')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Order Produk No')
		->setCellValue('C7', 'Tanggal Order')
		->setCellValue('D7', 'Tanggal Pengiriman')
		->setCellValue('E7', 'Reseller')
		->setCellValue('F7', 'Pcs')
		->setCellValue('G7', 'Grand Total')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(28);	
		$spreadsheet->getActiveSheet()->getStyle("A7:G7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; 
		foreach($list as $key) {
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_produk_no)
		->setCellValue('C'.$i, $key->tanggal_order)
		->setCellValue('D'.$i, $key->tanggal_pengiriman)
		->setCellValue('E'.$i, $key->nama)
		->setCellValue('F'.$i, $key->pcs)
		->setCellValue('G'.$i, $key->grand_total_lbl);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:G".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:G7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('G8:G'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function pendapatan_penukaran_produk(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->pendapatan_penukaran_produk_list();			
				break;
			case 'pdf':
				$this->pendapatan_penukaran_produk_pdf();			
				break;
			case 'excel':
				$this->pendapatan_penukaran_produk_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,5);
				$sumColumn = array(5);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_pemesanan"));
				array_push($column, array("data"=>"tanggal_pelunasan"));
				array_push($column, array("data"=>"pcs"));
				array_push($column, array("data"=>"grand_total_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'pendapatan-penukaran-produk';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/pendapatan_penukaran_produk');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function pendapatan_penukaran_produk_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang_produk->pendapatan_produk_all();
		$result['iTotalDisplayRecords'] = $this->hutang_produk->pendapatan_produk_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->hutang_produk->pendapatan_produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);
			$key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function pendapatan_penukaran_produk_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->pendapatan_produk_filter($query);
		$list =  $this->hutang_produk->pendapatan_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);
			$key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_penukaran_produk_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Pendapatan Penukaran Produk '.$date.".pdf","D");
	}
	function pendapatan_penukaran_produk_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->pendapatan_produk_filter($query);
		$list =  $this->hutang_produk->pendapatan_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);
			$key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Pendapatan Penukaran Produk')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Reseller')
		->setCellValue('C7', 'Tanggal Pemesanan')
		->setCellValue('D7', 'Tanggal Pelunasan')
		->setCellValue('E7', 'Pcs')
		->setCellValue('F7', 'Grand Total')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
		$spreadsheet->getActiveSheet()->getStyle("A7:F7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->nama)
		->setCellValue('C'.$i, $key->tanggal_pemesanan)
		->setCellValue('D'.$i, $key->tanggal_pelunasan)
		->setCellValue('E'.$i, $key->pcs)
		->setCellValue('F'.$i, $key->grand_total_lbl);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:F".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:F7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('F8:F'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function accounting_bonus(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->accounting_bonus_list();			
				break;
			case 'pdf':
				$this->accounting_bonus_pdf();			
				break;
			case 'excel':
				$this->accounting_bonus_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,5);
				$sumColumn = array(8);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"penerima"));
				array_push($column, array("data"=>"pemberi"));
				array_push($column, array("data"=>"created_at"));
				array_push($column, array("data"=>"tanggal_pengiriman"));
				array_push($column, array("data"=>"jumlah_deposit_lbl"));
				array_push($column, array("data"=>"type"));
				array_push($column, array("data"=>"persentase"));
				array_push($column, array("data"=>"jumlah_bonus_lbl"));
				array_push($column, array("data"=>"status"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'accounting-bonus';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/accounting_bonus');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function accounting_bonus_list(){

		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bonus->accounting_bonus_all();
		$result['iTotalDisplayRecords'] = $this->bonus->accounting_bonus_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bonus->accounting_bonus_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
			}

			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}
			
			$key->persentase = number_format($key->persentase). "%";
			$key->jumlah_bonus_lbl = $this->idr_currency($key->jumlah_bonus);
            $key->jumlah_deposit_lbl = $this->idr_currency($key->jumlah_deposit);
			$key->jumlah_deposit = number_format($key->jumlah_deposit);
			$key->jumlah_bonus = number_format($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function accounting_bonus_pdf(){
		
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->accounting_bonus_filter($query);
		$list =  $this->bonus->accounting_bonus_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}

			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}
			
			$key->persentase = number_format($key->persentase). "%";
			$key->jumlah_bonus_lbl = $this->idr_currency($key->jumlah_bonus);
            $key->jumlah_deposit_lbl = $this->idr_currency($key->jumlah_deposit);
			$key->jumlah_deposit = number_format($key->jumlah_deposit);
			$key->jumlah_bonus = number_format($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_accounting_bonus_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Bonus '.$date.".pdf","D");
	}
	function accounting_bonus_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->accounting_bonus_filter($query);
		$list =  $this->bonus->accounting_bonus_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}

			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}
			
			$key->persentase = ($key->persentase). "%";
			$key->jumlah_bonus_lbl = ($key->jumlah_bonus);
            $key->jumlah_deposit_lbl = ($key->jumlah_deposit);
			$key->jumlah_deposit = ($key->jumlah_deposit);
			$key->jumlah_bonus = ($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Bonus')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Penerima')
		->setCellValue('C7', 'Pemberi')
		->setCellValue('D7', 'Tanggal')
		->setCellValue('E7', 'Tgl. Pengiriman')
		->setCellValue('F7', 'Jumlah Deposit')
		->setCellValue('G7', 'Type')
		->setCellValue('H7', 'Persentase')
		->setCellValue('I7', 'Jumlah Bonus')
		->setCellValue('J7', 'Status')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(18);		
		$spreadsheet->getActiveSheet()->getStyle("A7:J7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; 
		foreach($list as $key) {
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->penerima)
		->setCellValue('C'.$i, $key->pemberi)
		->setCellValue('D'.$i, $key->created_at)
		->setCellValue('E'.$i, $key->tanggal_pengiriman)
		->setCellValue('F'.$i, $key->jumlah_deposit_lbl)
		->setCellValue('G'.$i, $key->type)
		->setCellValue('H'.$i, $key->persentase)
		->setCellValue('I'.$i, $key->jumlah_bonus_lbl)
		->setCellValue('J'.$i, $key->status);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:J".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:J7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('J8:J'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function accounting_ferby(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->accounting_ferby_list();			
				break;
			case 'pdf':
				$this->accounting_ferby_pdf();			
				break;
			case 'excel':
				$this->accounting_ferby_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,4);
				$sumColumn = array(4);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_pemesanan"));
				array_push($column, array("data"=>"tanggal_penerimaan"));
				array_push($column, array("data"=>"grand_total_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'accounting-ferby';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/accounting_ferby');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function accounting_ferby_list(){
		
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->accounting_ferby_all();
		$result['iTotalDisplayRecords'] = $this->po_produk->accounting_ferby_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->accounting_ferby_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function accounting_ferby_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_produk->accounting_ferby_filter($query);
		$list =  $this->po_produk->accounting_ferby_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_accounting_ferby_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Ferby '.$date.".pdf","D");
	}
	function accounting_ferby_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_produk->accounting_ferby_filter($query);
		$list =  $this->po_produk->accounting_ferby_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->grand_total_lbl = ($key->grand_total);
			$key->grand_total = ($key->grand_total);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Ferby')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Reseller')
		->setCellValue('C7', 'Tanggal Pemesanan')
		->setCellValue('D7', 'Tanggal Penerimaan')
		->setCellValue('E7', 'Grand Total')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);		
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->nama)
		->setCellValue('C'.$i, $key->tanggal_pemesanan)
		->setCellValue('D'.$i, $key->tanggal_penerimaan)
		->setCellValue('E'.$i, $key->grand_total_lbl);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}


	function bonus_withdraw(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->bonus_withdraw_list();			
				break;
			case 'pdf':
				$this->bonus_withdraw_pdf();			
				break;
			case 'excel':
				$this->bonus_withdraw_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,4);
				$sumColumn = array(4);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"penerima"));
				array_push($column, array("data"=>"pemberi"));
				array_push($column, array("data"=>"created_at"));
				array_push($column, array("data"=>"jumlah_deposit_lbl"));
				array_push($column, array("data"=>"type"));
				array_push($column, array("data"=>"persentase"));
				array_push($column, array("data"=>"jumlah_bonus_lbl"));
				array_push($column, array("data"=>"status"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'bonus-withdraw';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/bonus_withdraw');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function bonus_withdraw_list(){

		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}
		
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bonus->bonus_withdraw_all();
		$result['iTotalDisplayRecords'] = $this->bonus->bonus_withdraw_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bonus->bonus_withdraw_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			
			$key->persentase = number_format($key->persentase). "%";
			$key->jumlah_bonus_lbl = $this->idr_currency($key->jumlah_bonus);
            $key->jumlah_deposit_lbl = $this->idr_currency($key->jumlah_deposit);
			$key->jumlah_deposit = number_format($key->jumlah_deposit);
			$key->jumlah_bonus = number_format($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function bonus_withdraw_pdf(){
		
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->bonus_withdraw_filter($query);
		$list =  $this->bonus->bonus_withdraw_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			
			$key->persentase = number_format($key->persentase). "%";
			$key->jumlah_bonus_lbl = $this->idr_currency($key->jumlah_bonus);
            $key->jumlah_deposit_lbl = $this->idr_currency($key->jumlah_deposit);
			$key->jumlah_deposit = number_format($key->jumlah_deposit);
			$key->jumlah_bonus = number_format($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_bonus_withdraw_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Bonus Withdraw '.$date.".pdf","D");
	}
	function bonus_withdraw_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->bonus->bonus_withdraw_filter($query);
		$list =  $this->bonus->bonus_withdraw_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			
			$key->persentase = ($key->persentase). "%";
			$key->jumlah_bonus_lbl = ($key->jumlah_bonus);
            $key->jumlah_deposit_lbl = ($key->jumlah_deposit);
			$key->jumlah_deposit = ($key->jumlah_deposit);
			$key->jumlah_bonus = ($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Bonus Withdraw')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Penerima')
		->setCellValue('C7', 'Pemberi')
		->setCellValue('D7', 'Tanggal')
		->setCellValue('E7', 'Jumlah Deposit')
		->setCellValue('F7', 'Type')
		->setCellValue('G7', 'Persentase')
		->setCellValue('H7', 'Jumlah Bonus')
		->setCellValue('I7', 'Status')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(18);		
		$spreadsheet->getActiveSheet()->getStyle("A7:I7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; 
		foreach($list as $key) {
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->penerima)
		->setCellValue('C'.$i, $key->pemberi)
		->setCellValue('D'.$i, $key->created_at)
		->setCellValue('E'.$i, $key->jumlah_deposit_lbl)
		->setCellValue('F'.$i, $key->type)
		->setCellValue('G'.$i, $key->persentase)
		->setCellValue('H'.$i, $key->jumlah_bonus_lbl)
		->setCellValue('I'.$i, $key->status);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:I".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:I7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('I8:I'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function daftar_reseller_aktif(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->daftar_reseller_aktif_list();			
				break;
			case 'pdf':
				$this->daftar_reseller_aktif_pdf();			
				break;
			case 'excel':
				$this->daftar_reseller_aktif_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array();
				$sumColumn = array();
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tipe"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'daftar-reseller-aktif';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/daftar_reseller_aktif');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function daftar_reseller_aktif_list(){

		// if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
		// 	$temp = $_GET["columns"][2]["search"]["value"];
		// 	$temp = explode("|", $temp);
		// 	$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
		// 	$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		// }
		
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->reseller->daftar_reseller_aktif_all();
		$result['iTotalDisplayRecords'] = $this->reseller->daftar_reseller_aktif_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->reseller->daftar_reseller_aktif_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			$key->no = $i;	
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function daftar_reseller_aktif_pdf(){
		
		// if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
		// 	$data['start_tanggal'] = $this->input->get('start_tanggal');
		// 	$data['end_tanggal'] = $this->input->get('end_tanggal');
		// } else {
		// 	$data['start_tanggal'] = "-";
		// 	$data['end_tanggal'] = "-";
		// }
		
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->reseller->daftar_reseller_aktif_filter($query);
		$list =  $this->reseller->daftar_reseller_aktif_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            // if($key->created_at != null){
			// 	$time = strtotime($key->created_at);
			// 	$key->created_at = date('d-m-Y H:i:s',$time);
			// }
			
			// $key->persentase = number_format($key->persentase). "%";
			// $key->jumlah_bonus_lbl = $this->idr_currency($key->jumlah_bonus);
            // $key->jumlah_deposit_lbl = $this->idr_currency($key->jumlah_deposit);
			// $key->jumlah_deposit = number_format($key->jumlah_deposit);
			// $key->jumlah_bonus = number_format($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_daftar_reseller_aktif_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Daftar Reseller Aktif '.$date.".pdf","D");
	}
	function daftar_reseller_aktif_excel(){
		// if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
		// 	$data['start_tanggal'] = $this->input->get('start_tanggal');
		// 	$data['end_tanggal'] = $this->input->get('end_tanggal');
		// } else {
		// 	$data['start_tanggal'] = "-";
		// 	$data['end_tanggal'] = "-";
		// }
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->reseller->daftar_reseller_aktif_filter($query);
		$list =  $this->reseller->daftar_reseller_aktif_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            // if($key->created_at != null){
			// 	$time = strtotime($key->created_at);
			// 	$key->created_at = date('d-m-Y H:i:s',$time);
			// }
			
			// $key->persentase = number_format($key->persentase). "%";
			// $key->jumlah_bonus_lbl = $this->idr_currency($key->jumlah_bonus);
            // $key->jumlah_deposit_lbl = $this->idr_currency($key->jumlah_deposit);
			// $key->jumlah_deposit = number_format($key->jumlah_deposit);
			// $key->jumlah_bonus = number_format($key->jumlah_bonus);
			$key->no = $i;	
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Daftar Reseller Aktif')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Nama Reseller')
		->setCellValue('C7', 'Type')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);		
		$spreadsheet->getActiveSheet()->getStyle("A7:C7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; 
		foreach($list as $key) {
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->nama)
		->setCellValue('C'.$i, $key->tipe);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:C".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:C7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('C8:C'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Order Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function hutang_produk_keseluruhan(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->hutang_produk_keseluruhan_list();			
				break;
			case 'pdf':
				$this->hutang_produk_keseluruhan_pdf();			
				break;
			case 'excel':
				$this->hutang_produk_keseluruhan_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,5);
				$sumColumn = array(5);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_produk_no"));
				array_push($column, array("data"=>"no_pembayaran"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_pelunasan"));
				// array_push($column, array("data"=>"created_at"));
				array_push($column, array("data"=>"pcs"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'hutang-produk-keseluruhan';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/hutang_produk_keseluruhan');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function hutang_produk_keseluruhan_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang_produk->hutang_produk_keseluruhan_all();
		$result['iTotalDisplayRecords'] = $this->hutang_produk->hutang_produk_keseluruhan_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->hutang_produk->hutang_produk_keseluruhan_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function hutang_produk_keseluruhan_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->hutang_produk_keseluruhan_filter($query);
		$list =  $this->hutang_produk->hutang_produk_keseluruhan_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_hutang_produk_keseluruhan_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Hutang Produk Keseluruhan '.$date.".pdf","D");
	}
	function hutang_produk_keseluruhan_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->hutang_produk_keseluruhan_filter($query);
		$list =  $this->hutang_produk->hutang_produk_keseluruhan_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Hutang Produk Keseluruhan')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. PO Produk')
		->setCellValue('C7', 'No. Pembayaran')
		->setCellValue('D7', 'Reseller')
		->setCellValue('E7', 'Tanggal Pelunasan')
		->setCellValue('F7', 'Pcs')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$spreadsheet->getActiveSheet()->getStyle("A7:F7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_produk_no)
		->setCellValue('C'.$i, $key->no_pembayaran)
		->setCellValue('D'.$i, $key->nama)
		->setCellValue('E'.$i, $key->tanggal_pelunasan)
		->setCellValue('F'.$i, $key->pcs);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:F".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:F7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('F8:F'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Hutang Produk Keseluruhan'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function produk_keluar_by_deposit(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->produk_keluar_by_deposit_list();			
				break;
			case 'pdf':
				$this->produk_keluar_by_deposit_pdf();			
				break;
			case 'excel':
				$this->produk_keluar_by_deposit_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,4);
				$sumColumn = array(4);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_penerimaan"));
                array_push($column, array("data"=>"produk_nama"));
				array_push($column, array("data"=>"pcs"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'produk-keluar-by-deposit';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/produk_keluar_by_deposit');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function produk_keluar_by_deposit_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->produk_keluar_by_deposit_all();
		$result['iTotalDisplayRecords'] = $this->po_produk->produk_keluar_by_deposit_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->produk_keluar_by_deposit_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function produk_keluar_by_deposit_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_produk->produk_keluar_by_deposit_filter($query);
		$list =  $this->po_produk->produk_keluar_by_deposit_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_produk_keluar_by_deposit_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Produk Keluar by Deposit '.$date.".pdf","D");
	}
	function produk_keluar_by_deposit_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->po_produk->produk_keluar_by_deposit_filter($query);
		$list =  $this->po_produk->produk_keluar_by_deposit_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Produk Keluar by Deposit')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Reseller')
		->setCellValue('C7', 'Tanggal Penerimaan')
            ->setCellValue('D7', 'Produk')
		->setCellValue('E7', 'Pcs')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->nama)
		->setCellValue('C'.$i, $key->tanggal_penerimaan)
		->setCellValue('D'.$i, $key->pcs);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Produk Keluar by Deposit'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function produk_keluar_by_hutang_produk(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->produk_keluar_by_hutang_produk_list();			
				break;
			case 'pdf':
				$this->produk_keluar_by_hutang_produk_pdf();			
				break;
			case 'excel':
				$this->produk_keluar_by_hutang_produk_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,4);
				$sumColumn = array(4);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_pelunasan"));
                array_push($column, array("data"=>"produk_nama"));
				array_push($column, array("data"=>"jumlah_produk"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'produk-keluar-by-hutang-produk';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/produk_keluar_by_hutang_produk');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function produk_keluar_by_hutang_produk_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang_produk->produk_keluar_by_hutang_produk_all();
		$result['iTotalDisplayRecords'] = $this->hutang_produk->produk_keluar_by_hutang_produk_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->hutang_produk->produk_keluar_by_hutang_produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->jumlah_produk = number_format($key->jumlah_produk);
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function produk_keluar_by_hutang_produk_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->produk_keluar_by_hutang_produk_filter($query);
		$list =  $this->hutang_produk->produk_keluar_by_hutang_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->jumlah_produk = number_format($key->jumlah_produk);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_produk_keluar_by_hutang_produk_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Produk Keluar by Hutang Produk '.$date.".pdf","D");
	}
	function produk_keluar_by_hutang_produk_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->produk_keluar_by_hutang_produk_filter($query);
		$list =  $this->hutang_produk->produk_keluar_by_hutang_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->jumlah_produk = ($key->jumlah_produk);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Produk Keluar by Hutang Produk')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Reseller')
		->setCellValue('C7', 'Tanggal Pelunasan')
        ->setCellValue('D7', 'Produk')
		->setCellValue('E7', 'Jumlah Order')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->nama)
		->setCellValue('C'.$i, $key->tanggal_pelunasan)
        ->setCellValue('D'.$i, $key->produk_nama)
		->setCellValue('E'.$i, $key->jumlah_produk);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Produk Keluar by Hutang Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function sisa_hutang_produk(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->sisa_hutang_produk_list();			
				break;
			case 'pdf':
				$this->sisa_hutang_produk_pdf();			
				break;
			case 'excel':
				$this->sisa_hutang_produk_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,4);
				$sumColumn = array(4);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_produk_no"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"created_at"));
				array_push($column, array("data"=>"pcs"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'sisa-hutang-produk';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/sisa_hutang_produk');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function sisa_hutang_produk_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang_produk->sisa_hutang_produk_all();
		$result['iTotalDisplayRecords'] = $this->hutang_produk->sisa_hutang_produk_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->hutang_produk->sisa_hutang_produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function sisa_hutang_produk_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->sisa_hutang_produk_filter($query);
		$list =  $this->hutang_produk->sisa_hutang_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_sisa_hutang_produk_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Sisa Hutang Produk '.$date.".pdf","D");
	}
	function sisa_hutang_produk_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->sisa_hutang_produk_filter($query);
		$list =  $this->hutang_produk->sisa_hutang_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);
			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Sisa Hutang Produk')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. PO Produk')
		->setCellValue('C7', 'Reseller')
		->setCellValue('D7', 'Tanggal Pelunasan')
		->setCellValue('E7', 'Jumlah Order')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_produk_no)
		->setCellValue('C'.$i, $key->nama)
		->setCellValue('D'.$i, $key->created_at)
		->setCellValue('E'.$i, $key->pcs);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Sisa Hutang Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function withdraw_cash(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->withdraw_cash_list();			
				break;
			case 'pdf':
				$this->withdraw_cash_pdf();			
				break;
			case 'excel':
				$this->withdraw_cash_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,5);
				$sumColumn = array(5);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_produk_no"));
				array_push($column, array("data"=>"no_pembayaran"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_pelunasan"));
				array_push($column, array("data"=>"total_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'withdraw-cash';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/withdraw_cash');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function withdraw_cash_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pembayaran_hutang_produk->withdraw_cash_all();
		$result['iTotalDisplayRecords'] = $this->pembayaran_hutang_produk->withdraw_cash_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->pembayaran_hutang_produk->withdraw_cash_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->total_lbl = $this->idr_currency($key->total);
			$key->total = number_format($key->total);

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function withdraw_cash_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pembayaran_hutang_produk->withdraw_cash_filter($query);
		$list =  $this->pembayaran_hutang_produk->withdraw_cash_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->total_lbl = $this->idr_currency($key->total);
			$key->total = number_format($key->total);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_withdraw_cash_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Withdraw Cash '.$date.".pdf","D");
	}
	function withdraw_cash_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pembayaran_hutang_produk->withdraw_cash_filter($query);
		$list =  $this->pembayaran_hutang_produk->withdraw_cash_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->total_lbl = ($key->total);
			$key->total = ($key->total);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Withdraw Cash')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. PO Produk')
		->setCellValue('C7', 'No. Pembayaran')
		->setCellValue('D7', 'Reseller')
		->setCellValue('E7', 'Tanggal Pelunasan')
		->setCellValue('F7', 'Total')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:F7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_produk_no)
		->setCellValue('C'.$i, $key->no_pembayaran)
		->setCellValue('D'.$i, $key->nama)
		->setCellValue('E'.$i, $key->tanggal_pelunasan)
		->setCellValue('F'.$i, $key->total_lbl);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:F".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:F7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('F8:F'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Withdraw Cash'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function withdraw_produk(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->withdraw_produk_list();			
				break;
			case 'pdf':
				$this->withdraw_produk_pdf();			
				break;
			case 'excel':
				$this->withdraw_produk_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,5);
				$sumColumn = array(5);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_produk_no"));
				array_push($column, array("data"=>"no_pembayaran"));
				array_push($column, array("data"=>"nama"));
				array_push($column, array("data"=>"tanggal_pelunasan"));
				array_push($column, array("data"=>"pcs"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'withdraw-produk';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/withdraw_produk');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function withdraw_produk_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->hutang_produk->withdraw_produk_all();
		$result['iTotalDisplayRecords'] = $this->hutang_produk->withdraw_produk_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->hutang_produk->withdraw_produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function withdraw_produk_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->withdraw_produk_filter($query);
		$list =  $this->hutang_produk->withdraw_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = number_format($key->pcs);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_withdraw_produk_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Withdraw Produk '.$date.".pdf","D");
	}
	function withdraw_produk_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->hutang_produk->withdraw_produk_filter($query);
		$list =  $this->hutang_produk->withdraw_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_pelunasan != null){
				$time = strtotime($key->tanggal_pelunasan);
				$key->tanggal_pelunasan = date('d-m-Y',$time);
			}

			$key->pcs = ($key->pcs);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Withdraw Produk')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. PO Produk')
		->setCellValue('C7', 'No. Pembayaran')
		->setCellValue('D7', 'Reseller')
		->setCellValue('E7', 'Tanggal Pelunasan')
		->setCellValue('F7', 'Jumlah Produk')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:F7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_produk_no)
		->setCellValue('C'.$i, $key->no_pembayaran)
		->setCellValue('D'.$i, $key->nama)
		->setCellValue('E'.$i, $key->tanggal_pelunasan)
		->setCellValue('F'.$i, $key->pcs);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:F".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:F7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('F8:F'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Withdraw Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function pengiriman_by_po(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->pengiriman_by_po_list();			
				break;
			case 'pdf':
				$this->pengiriman_by_po_pdf();			
				break;
			case 'excel':
				$this->pengiriman_by_po_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,3);
				$sumColumn = array(3);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_produk_no"));
				array_push($column, array("data"=>"tanggal_pengiriman"));
				array_push($column, array("data"=>"total_qty_pengiriman"));
				array_push($column, array("data"=>"keterangan_pengiriman"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'pengiriman-by-po';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/pengiriman_by_po');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function pengiriman_by_po_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pengiriman_produk->pengiriman_by_po_all();
		$result['iTotalDisplayRecords'] = $this->pengiriman_produk->pengiriman_by_po_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->pengiriman_produk->pengiriman_by_po_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->total_qty_pengiriman = number_format($key->total_qty_pengiriman);

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function pengiriman_by_po_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pengiriman_produk->pengiriman_by_po_filter($query);
		$list =  $this->pengiriman_produk->pengiriman_by_po_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->total_qty_pengiriman = number_format($key->total_qty_pengiriman);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_pengiriman_by_po_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Pengiriman Produk by PO '.$date.".pdf","D");
	}
	function pengiriman_by_po_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pengiriman_produk->pengiriman_by_po_filter($query);
		$list =  $this->pengiriman_produk->pengiriman_by_po_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->total_qty_pengiriman = ($key->total_qty_pengiriman);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Pengiriman Produk by PO')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. PO Produk')
		->setCellValue('C7', 'Tanggal Pengiriman')
		->setCellValue('D7', 'Qty')
		->setCellValue('E7', 'Keterangan Pengiriman')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_produk_no)
		->setCellValue('C'.$i, $key->tanggal_pengiriman)
		->setCellValue('D'.$i, $key->total_qty_pengiriman)
		->setCellValue('E'.$i, $key->keterangan_pengiriman);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Pengiriman Produk by PO'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function pengiriman_by_pembayaran_hutang(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->pengiriman_by_pembayaran_hutang_list();			
				break;
			case 'pdf':
				$this->pengiriman_by_pembayaran_hutang_pdf();			
				break;
			case 'excel':
				$this->pengiriman_by_pembayaran_hutang_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,3);
				$sumColumn = array(3);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"no_pembayaran"));
				array_push($column, array("data"=>"tanggal_pengiriman"));
				array_push($column, array("data"=>"total_qty_pengiriman"));
				array_push($column, array("data"=>"keterangan_pengiriman"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'pengiriman-by-pembayaran-hutang';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/pengiriman_by_pembayaran_hutang');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function pengiriman_by_pembayaran_hutang_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_all();
		$result['iTotalDisplayRecords'] = $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->total_qty_pengiriman = number_format($key->total_qty_pengiriman);

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function pengiriman_by_pembayaran_hutang_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_filter($query);
		$list =  $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->total_qty_pengiriman = number_format($key->total_qty_pengiriman);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_pengiriman_by_pembayaran_hutang_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Pengiriman Produk by Pembayaran Hutang '.$date.".pdf","D");
	}
	function pengiriman_by_pembayaran_hutang_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_filter($query);
		$list =  $this->pengiriman_produk->pengiriman_by_pembayaran_hutang_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_pengiriman != null){
				$time = strtotime($key->tanggal_pengiriman);
				$key->tanggal_pengiriman = date('d-m-Y',$time);
			}

			$key->total_qty_pengiriman = ($key->total_qty_pengiriman);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Pengiriman Produk by Pembayaran Hutang')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. Pembayaran')
		->setCellValue('C7', 'Tanggal Pengiriman')
		->setCellValue('D7', 'Qty')
		->setCellValue('E7', 'Keterangan Pengiriman')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->no_pembayaran)
		->setCellValue('C'.$i, $key->tanggal_pengiriman)
		->setCellValue('D'.$i, $key->total_qty_pengiriman)
		->setCellValue('E'.$i, $key->keterangan_pengiriman);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Pengiriman Produk by Pembayaran Hutang'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function penerimaan_produk_produsen(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->penerimaan_produk_produsen_list();			
				break;
			case 'pdf':
				$this->penerimaan_produk_produsen_pdf();			
				break;
			case 'excel':
				$this->penerimaan_produk_produsen_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array(0,3);
				$sumColumn = array(3);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"po_kutus_kutus_no"));
				array_push($column, array("data"=>"tanggal_penerimaan"));
				array_push($column, array("data"=>"total_qty_penerimaan"));
				array_push($column, array("data"=>"keterangan_penerimaan"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'penerimaan-produk-produsen';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/penerimaan_produk_produsen');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function penerimaan_produk_produsen_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penerimaan_produk->penerimaan_produk_produsen_all();
		$result['iTotalDisplayRecords'] = $this->penerimaan_produk->penerimaan_produk_produsen_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->penerimaan_produk->penerimaan_produk_produsen_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->total_qty_penerimaan = number_format($key->total_qty_penerimaan);

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function penerimaan_produk_produsen_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->penerimaan_produk->penerimaan_produk_produsen_filter($query);
		$list =  $this->penerimaan_produk->penerimaan_produk_produsen_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->total_qty_penerimaan = number_format($key->total_qty_penerimaan);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_penerimaan_produk_produsen_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Penerimaan Produk dari Produsen '.$date.".pdf","D");
	}
	function penerimaan_produk_produsen_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->penerimaan_produk->penerimaan_produk_produsen_filter($query);
		$list =  $this->penerimaan_produk->penerimaan_produk_produsen_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}

			$key->total_qty_penerimaan = ($key->total_qty_penerimaan);

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan Penerimaan Produk dari Produsen')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'No. Order Produk')
		->setCellValue('C7', 'Tanggal Penerimaan')
		->setCellValue('D7', 'Qty')
		->setCellValue('E7', 'Keterangan Penerimaan')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->po_kutus_kutus_no)
		->setCellValue('C'.$i, $key->tanggal_penerimaan)
		->setCellValue('D'.$i, $key->total_qty_penerimaan)
		->setCellValue('E'.$i, $key->keterangan_penerimaan);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan Penerimaan Produk dari Produsen'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function history_hpp_produk(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->history_hpp_produk_list();			
				break;
			case 'pdf':
				$this->history_hpp_produk_pdf();			
				break;
			case 'excel':
				$this->history_hpp_produk_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array();
				// $target = array(0,3);
				$sumColumn = array();
				// $sumColumn = array(3);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"produk_nama"));
				array_push($column, array("data"=>"tanggal"));
				array_push($column, array("data"=>"produk_qty"));
				array_push($column, array("data"=>"hpp_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'history-hpp-produk';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/history_hpp_produk');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function history_hpp_produk_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penerimaan_produk->history_hpp_produk_all();
		$result['iTotalDisplayRecords'] = $this->penerimaan_produk->history_hpp_produk_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->penerimaan_produk->history_hpp_produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal != null){
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y',$time);
			}

			$key->produk_qty = number_format($key->produk_qty);
			$key->hpp_lbl = 'Rp '. number_format($key->hpp,11,".",",");

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function history_hpp_produk_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->penerimaan_produk->history_hpp_produk_filter($query);
		$list =  $this->penerimaan_produk->history_hpp_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal != null){
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y',$time);
			}

			$key->produk_qty = number_format($key->produk_qty);
			$key->hpp_lbl = 'Rp '. number_format($key->hpp,11,".",",");

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_history_hpp_produk_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan History HPP Produk '.$date.".pdf","D");
	}
	function history_hpp_produk_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->penerimaan_produk->history_hpp_produk_filter($query);
		$list =  $this->penerimaan_produk->history_hpp_produk_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal != null){
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y',$time);
			}

			$key->produk_qty = number_format($key->produk_qty);
			$key->hpp_lbl = 'Rp '. number_format($key->hpp,11,".",",");

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan History HPP Produk')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Produk')
		->setCellValue('C7', 'Tanggal')
		->setCellValue('D7', 'Qty')
		->setCellValue('E7', 'HPP')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->produk_nama)
		->setCellValue('C'.$i, $key->tanggal)
		->setCellValue('D'.$i, $key->produk_qty)
		->setCellValue('E'.$i, $key->hpp_lbl);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok Produk');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order Produk');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan History HPP Produk'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

	function history_hpp_bahan(){
		$segment2 = $this->uri->segment(3);
		switch ($segment2) {
			case 'list':
				$this->history_hpp_bahan_list();			
				break;
			case 'pdf':
				$this->history_hpp_bahan_pdf();			
				break;
			case 'excel':
				$this->history_hpp_bahan_excel();			
				break;						
			default:
				$data = $this->g_data;
				$target = array();
				// $target = array(0,3);
				$sumColumn = array();
				// $sumColumn = array(3);
				$column = array();
				array_push($column, array("data"=>"no"));
				array_push($column, array("data"=>"bahan_nama"));
				array_push($column, array("data"=>"tanggal"));
				array_push($column, array("data"=>"bahan_qty"));
				array_push($column, array("data"=>"hpp_lbl"));
				$data['column'] = json_encode($column);
				$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
				$data['sumColumn'] = json_encode($sumColumn);
				$data['page'] = 'history-hpp-bahan';
				$this->load->view('admin/static/header',$data);
				$this->load->view('admin/static/sidebar');
				$this->load->view('admin/static/topbar');
				$this->load->view('admin/accounting/history_hpp_bahan');
				$this->load->view('admin/static/footer');				
				break;
		}		
	}
	function history_hpp_bahan_list(){
		if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$temp = $_GET["columns"][2]["search"]["value"];
			$temp = explode("|", $temp);
			$_GET['start_tanggal'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
			$_GET['end_tanggal'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
		}

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->penerimaan_bahan->history_hpp_bahan_all();
		$result['iTotalDisplayRecords'] = $this->penerimaan_bahan->history_hpp_bahan_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->penerimaan_bahan->history_hpp_bahan_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			
			if($key->tanggal != null){
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y',$time);
			}

			$key->bahan_qty = number_format($key->bahan_qty);
			$key->hpp_lbl = 'Rp '. number_format($key->hpp,11,".",",");

			$key->no = $i;
			$i++;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function history_hpp_bahan_pdf(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->penerimaan_bahan->history_hpp_bahan_filter($query);
		$list =  $this->penerimaan_bahan->history_hpp_bahan_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
		
			if($key->tanggal != null){
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y',$time);
			}

			$key->bahan_qty = number_format($key->bahan_qty);
			$key->hpp_lbl = 'Rp '. number_format($key->hpp,11,".",",");

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		$data['list'] = $list;

		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_history_hpp_bahan_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan History HPP bahan '.$date.".pdf","D");
	}
	function history_hpp_bahan_excel(){
		if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
			$data['start_tanggal'] = $this->input->get('start_tanggal');
			$data['end_tanggal'] = $this->input->get('end_tanggal');
		} else {
			$data['start_tanggal'] = "-";
			$data['end_tanggal'] = "-";
		}
		if(isset($_GET['key']) && $this->input->get('key')!=""){
			$data['cari'] = $this->input->get('key');
		} else {
			$data['cari'] = " - ";
		}
		$query = $this->input->get('key');
		$start = 0;
		$length = $this->penerimaan_bahan->history_hpp_bahan_filter($query);
		$list =  $this->penerimaan_bahan->history_hpp_bahan_list($start,$length,$query);
		$i = $start+1;
        foreach ($list as $key) {
            
			if($key->tanggal != null){
				$time = strtotime($key->tanggal);
				$key->tanggal = date('d-m-Y',$time);
			}

			$key->bahan_qty = number_format($key->bahan_qty);
			$key->hpp_lbl = 'Rp '. number_format($key->hpp,11,".",",");

			$key->no = $i;
			$i++;
			$key->action = null;
        }
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
		->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
		->setTitle('Laporan History HPP Bahan')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Bahan')
		->setCellValue('C7', 'Tanggal')
		->setCellValue('D7', 'Qty')
		->setCellValue('E7', 'HPP')
		;				
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
		$sum = 0;
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->bahan_nama)
		->setCellValue('C'.$i, $key->tanggal)
		->setCellValue('D'.$i, $key->bahan_qty)
		->setCellValue('E'.$i, $key->hpp_lbl);
		$i++;
		}
		
		$spreadsheet->getActiveSheet()->getStyle("A7:E".($i-1))->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:E7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
		// $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok bahan');
		// $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
		// $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
		// $spreadsheet->getActiveSheet()->setTitle('Laporan Order bahan');		
		$spreadsheet->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Laporan History HPP Bahan'.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;			
	}

    function omset_leader(){
        $segment2 = $this->uri->segment(3);
        switch ($segment2) {
            case 'list':
                $this->omset_leader_list();
                break;
            case 'pdf':
                $this->omset_leader_pdf();
                break;
            case 'excel':
                $this->omset_leader_excel();
                break;
            default:
                $data = $this->g_data;
                $target = array();
                // $target = array(0,3);
                $sumColumn = array();
                // $sumColumn = array(3);
                $column = array();
                array_push($column, array("data"=>"no"));
                array_push($column, array("data"=>"nama"));
                array_push($column, array("data"=>"bulan"));
                array_push($column, array("data"=>"omset"));
                $data['column'] = json_encode($column);
                $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
                $data['sumColumn'] = json_encode($sumColumn);
                $data['page'] = 'history-hpp-bahan';
                $this->load->view('admin/static/header',$data);
                $this->load->view('admin/static/sidebar');
                $this->load->view('admin/static/topbar');
                $this->load->view('admin/accounting/omset_leader');
                $this->load->view('admin/static/footer');
                break;
        }
    }
    function omset_leader_list(){
	    $this->load->model('rekap_bulanan','',true);
        if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
            $temp = $_GET["columns"][2]["search"]["value"];
            $temp = explode("|", $temp);
            $_GET['start_date'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
            $_GET['end_date'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
        }

        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->rekap_bulanan->report_rekap_omset_group_all();
        $result['iTotalDisplayRecords'] = $this->rekap_bulanan->report_rekap_omset_group_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->rekap_bulanan->report_rekap_omset_group_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->bulan = $this->monthIdn($key->month);
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->omset = $this->idr_currency($key->total);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function omset_leader_pdf(){
        $this->load->model('rekap_bulanan','',true);
        if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
            $data['start_tanggal'] = $this->input->get('start_tanggal');
            $data['end_tanggal'] = $this->input->get('end_tanggal');
            $_GET['start_date'] = $this->input->get('start_tanggal');
            $_GET['end_date'] = $this->input->get('end_tanggal');
        } else {
            $data['start_tanggal'] = "-";
            $data['end_tanggal'] = "-";
        }
        if(isset($_GET['key']) && $this->input->get('key')!=""){
            $data['cari'] = $this->input->get('key');
        } else {
            $data['cari'] = " - ";
        }
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->rekap_bulanan->report_rekap_omset_group_filter($query);
        $list =  $this->rekap_bulanan->report_rekap_omset_group_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {

            $key->bulan = $this->monthIdn($key->month);
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->omset = $this->idr_currency($key->total);
        }
        $data['list'] = $list;

        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_omset_leader_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
            $date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Omset Leader '.$date.".pdf","D");
    }
    function omset_leader_excel(){
	    $this->load->model('rekap_bulanan');
        if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
            $data['start_tanggal'] = $this->input->get('start_tanggal');
            $data['end_tanggal'] = $this->input->get('end_tanggal');
            $_GET['start_date'] = $this->input->get('start_tanggal');
            $_GET['end_date'] = $this->input->get('end_tanggal');
        } else {
            $data['start_tanggal'] = "-";
            $data['end_tanggal'] = "-";
        }
        if(isset($_GET['key']) && $this->input->get('key')!=""){
            $data['cari'] = $this->input->get('key');
        } else {
            $data['cari'] = " - ";
        }
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->rekap_bulanan->report_rekap_omset_group_filter($query);
        $list =  $this->rekap_bulanan->report_rekap_omset_group_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {

            $key->bulan = $this->monthIdn($key->month);
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->omset = $this->idr_currency($key->total);
        }
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
            ->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
            ->setTitle('Laporan History HPP Bahan')
            ->setSubject('');
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            )
        );
        $border = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        );

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A7', 'No')
            ->setCellValue('B7', 'Nama')
            ->setCellValue('C7', 'Bulan')
            ->setCellValue('D7', 'Omset')
        ;
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $spreadsheet->getActiveSheet()->getStyle("A7:D7")->applyFromArray($style);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $date = date("Y-m-d");
        $sum = 0;
        $i=8; foreach($list as $key) {

            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key->no)
                ->setCellValue('B'.$i, $key->nama)
                ->setCellValue('C'.$i, $key->bulan)
                ->setCellValue('D'.$i, $key->total);
            $i++;
        }

        $spreadsheet->getActiveSheet()->getStyle("A7:D".($i-1))->applyFromArray($border);
        $spreadsheet->getActiveSheet()->getStyle('A7:D7')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('BEBEBE');
        $spreadsheet->getActiveSheet()->getStyle('D8:D'.$i)->getAlignment()->setWrapText(true);
        // Rename worksheet
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(1);
        $drawing->setWidth(80);
        $drawing->setHeight(80);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok bahan');
        // $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
        // $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
        // $spreadsheet->getActiveSheet()->setTitle('Laporan Order bahan');
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Omset Leader'.$date.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    function omset_leader_real(){
        $segment2 = $this->uri->segment(3);
        switch ($segment2) {
            case 'list':
                $this->omset_leader_real_list();
                break;
            case 'pdf':
                $this->omset_leader_real_pdf();
                break;
            case 'excel':
                $this->omset_leader_real_excel();
                break;
            default:
                $data = $this->g_data;
                $target = array();
                // $target = array(0,3);
                $sumColumn = array();
                // $sumColumn = array(3);
                $column = array();
                array_push($column, array("data"=>"no"));
                array_push($column, array("data"=>"nama"));
                array_push($column, array("data"=>"bulan"));
                array_push($column, array("data"=>"omset"));
                $data['column'] = json_encode($column);
                $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
                $data['sumColumn'] = json_encode($sumColumn);
                $data['page'] = 'history-hpp-bahan';
                $this->load->view('admin/static/header',$data);
                $this->load->view('admin/static/sidebar');
                $this->load->view('admin/static/topbar');
                $this->load->view('admin/accounting/omset_leader_real');
                $this->load->view('admin/static/footer');
                break;
        }
    }
    function omset_leader_real_list(){
        $this->load->model('rekap_bulanan','',true);
        if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
            $temp = $_GET["columns"][2]["search"]["value"];
            $temp = explode("|", $temp);
            $_GET['start_date'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
            $_GET['end_date'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
        }

        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->rekap_bulanan->report_rekap_omset_group_real_all();
        $result['iTotalDisplayRecords'] = $this->rekap_bulanan->report_rekap_omset_group_real_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->rekap_bulanan->report_rekap_omset_group_real_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->bulan = $this->monthIdn($key->month);
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->omset = $this->idr_currency($key->total);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function omset_leader_real_pdf(){
        $this->load->model('rekap_bulanan','',true);
        if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
            $data['start_tanggal'] = $this->input->get('start_tanggal');
            $data['end_tanggal'] = $this->input->get('end_tanggal');
            $_GET['start_date'] = $this->input->get('start_tanggal');
            $_GET['end_date'] = $this->input->get('end_tanggal');
        } else {
            $data['start_tanggal'] = "-";
            $data['end_tanggal'] = "-";
        }
        if(isset($_GET['key']) && $this->input->get('key')!=""){
            $data['cari'] = $this->input->get('key');
        } else {
            $data['cari'] = " - ";
        }
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->rekap_bulanan->report_rekap_omset_group_real_filter($query);
        $list =  $this->rekap_bulanan->report_rekap_omset_group_real_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {

            $key->bulan = $this->monthIdn($key->month);
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->omset = $this->idr_currency($key->total);
        }
        $data['list'] = $list;

        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/laporan_omset_leader_real_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
            $date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Omset Leader '.$date.".pdf","D");
    }
    function omset_leader_real_excel(){
        $this->load->model('rekap_bulanan');
        if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
            $data['start_tanggal'] = $this->input->get('start_tanggal');
            $data['end_tanggal'] = $this->input->get('end_tanggal');
            $_GET['start_date'] = $this->input->get('start_tanggal');
            $_GET['end_date'] = $this->input->get('end_tanggal');
        } else {
            $data['start_tanggal'] = "-";
            $data['end_tanggal'] = "-";
        }
        if(isset($_GET['key']) && $this->input->get('key')!=""){
            $data['cari'] = $this->input->get('key');
        } else {
            $data['cari'] = " - ";
        }
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->rekap_bulanan->report_rekap_omset_group_real_filter($query);
        $list =  $this->rekap_bulanan->report_rekap_omset_group_real_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {

            $key->bulan = $this->monthIdn($key->month);
            $key->no = $i;
            $i++;
            $key->action = null;
            $key->omset = $this->idr_currency($key->total);
        }
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
            ->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
            ->setTitle('Laporan History HPP Bahan')
            ->setSubject('');
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            )
        );
        $border = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        );

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A7', 'No')
            ->setCellValue('B7', 'Nama')
            ->setCellValue('C7', 'Bulan')
            ->setCellValue('D7', 'Omset')
        ;
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $spreadsheet->getActiveSheet()->getStyle("A7:D7")->applyFromArray($style);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $date = date("Y-m-d");
        $sum = 0;
        $i=8; foreach($list as $key) {

            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key->no)
                ->setCellValue('B'.$i, $key->nama)
                ->setCellValue('C'.$i, $key->bulan)
                ->setCellValue('D'.$i, $key->total);
            $i++;
        }

        $spreadsheet->getActiveSheet()->getStyle("A7:D".($i-1))->applyFromArray($border);
        $spreadsheet->getActiveSheet()->getStyle('A7:D7')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('BEBEBE');
        $spreadsheet->getActiveSheet()->getStyle('D8:D'.$i)->getAlignment()->setWrapText(true);
        // Rename worksheet
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(1);
        $drawing->setWidth(80);
        $drawing->setHeight(80);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok bahan');
        // $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
        // $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
        // $spreadsheet->getActiveSheet()->setTitle('Laporan Order bahan');
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Omset Leader Real'.$date.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    function peringkat_leader(){
        $segment2 = $this->uri->segment(3);
        switch ($segment2) {
            case 'list':
                $this->peringkat_leader_list();
                break;
            case 'pdf':
                $this->peringkat_leader_pdf();
                break;
            case 'excel':
                $this->peringkat_leader_excel();
                break;
            default:
                $data = $this->g_data;
                $target = array();
                // $target = array(0,3);
                $sumColumn = array();
                // $sumColumn = array(3);
                $column = array();
                array_push($column, array("data"=>"no"));
                array_push($column, array("data"=>"nama"));
                array_push($column, array("data"=>"bintang"));
                $data['column'] = json_encode($column);
                $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
                $data['sumColumn'] = json_encode($sumColumn);
                $data['page'] = 'history-hpp-bahan';
                $this->load->view('admin/static/header',$data);
                $this->load->view('admin/static/sidebar');
                $this->load->view('admin/static/topbar');
                $this->load->view('admin/accounting/omset_leader');
                $this->load->view('admin/static/footer');
                break;
        }
    }

    function pph21(){
        $segment2 = $this->uri->segment(3);
        switch ($segment2) {
            case 'list':
                $this->pph21_list();
                break;
            case 'pdf':
                $this->pph21_pdf();
                break;
            case 'excel':
                $this->pph21_excel();
                break;
            default:
                $data = $this->g_data;
                $target = array();
                // $target = array(0,3);
                $sumColumn = array(3,4,5,6,7,8,9,11,12,13);
                // $sumColumn = array(3);
                $column = array();
                array_push($column, array("data"=>"no"));
                array_push($column, array("data"=>"nama"));
                array_push($column, array("data"=>"tanggal"));
                array_push($column, array("data"=>"jumlah_bonus"));
                array_push($column, array("data"=>"dasar_pemotongan"));
                array_push($column, array("data"=>"dpp_kumulatif"));
                array_push($column, array("data"=>"ptkp"));
                array_push($column, array("data"=>"sisa_ptkp_perbulan"));
                array_push($column, array("data"=>"pkp"));
                array_push($column, array("data"=>"pkp_kumulatif"));
                array_push($column, array("data"=>"persentase"));
                array_push($column, array("data"=>"pph_npwp"));
                array_push($column, array("data"=>"pph_non_npwp"));
                array_push($column, array("data"=>"total_pph"));
                $data['column'] = json_encode($column);
                $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
                $data['sumColumn'] = json_encode($sumColumn);
                $data['page'] = 'history-hpp-bahan';
                $this->load->view('admin/static/header',$data);
                $this->load->view('admin/static/sidebar');
                $this->load->view('admin/static/topbar');
                $this->load->view('admin/accounting/pph21');
                $this->load->view('admin/static/footer');
                break;
        }
    }
    function pph21_list(){
        $this->load->model('dpp_reseller','',true);
        if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
            $temp = $_GET["columns"][2]["search"]["value"];
            $temp = explode("|", $temp);
            $_GET['start_date'] = (isset($temp[1]) ? $temp[0] : date("Y-m-d",0));
            $_GET['end_date'] = (isset($temp[1]) ? $temp[1] : date("Y-m-d"));
        }

        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->dpp_reseller->dpp_reseller_data_all();
        $result['iTotalDisplayRecords'] = $this->dpp_reseller->dpp_reseller_data_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->dpp_reseller->dpp_reseller_data_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {

            $key->no = $i;
            $i++;
            $key->action = null;
            $key->jumlah_bonus = number_format($key->jumlah_bonus);
            $key->dasar_pemotongan = number_format($key->dasar_pemotongan);
            $key->dpp_kumulatif = number_format($key->dpp_kumulatif);
            $key->ptkp = number_format($key->ptkp);
            $key->sisa_ptkp_perbulan = number_format($key->sisa_ptkp_perbulan);
            $key->pkp = number_format($key->pkp);
            $key->pkp_kumulatif = number_format($key->pkp_kumulatif);
            $key->pph_npwp = number_format($key->pph_npwp,2);
            $key->pph_non_npwp = number_format($key->pph_non_npwp,2);
            $key->total_pph = number_format($key->total_pph,2);

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
	function pph21_pdf(){
        $this->load->model('dpp_reseller','',true);
        if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
            $data['start_tanggal'] = $this->input->get('start_tanggal');
            $data['end_tanggal'] = $this->input->get('end_tanggal');
            $_GET['start_date'] = $this->input->get('start_tanggal');
            $_GET['end_date'] = $this->input->get('end_tanggal');
        } else {
            $data['start_tanggal'] = "-";
            $data['end_tanggal'] = "-";
        }
        if(isset($_GET['key']) && $this->input->get('key')!=""){
            $data['cari'] = $this->input->get('key');
        } else {
            $data['cari'] = " - ";
        }
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->dpp_reseller->dpp_reseller_data_filter($query);
        $list =  $this->dpp_reseller->dpp_reseller_data_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {

            $key->no = $i;
            $i++;
            $key->action = null;
            $key->jumlah_bonus = ($key->jumlah_bonus);
            $key->dasar_pemotongan = ($key->dasar_pemotongan);
            $key->dpp_kumulatif = ($key->dpp_kumulatif);
            $key->ptkp = ($key->ptkp);
            $key->sisa_ptkp_perbulan = ($key->sisa_ptkp_perbulan);
            $key->pkp = ($key->pkp);
            $key->pkp_kumulatif = ($key->pkp_kumulatif);
            $key->pph_npwp = ($key->pph_npwp);
            $key->pph_non_npwp = ($key->pph_non_npwp);
            $key->total_pph = ($key->total_pph);
        }
        $data['list'] = $list;
//        $this->load->view('admin/pdf/laporan_pph_21',$data);
        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

        $html = $this->load->view('admin/pdf/laporan_pph_21',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
            $date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan PPH 21.pdf',"D");
    }
    function pph21_excel(){
        $this->load->model('dpp_reseller','',true);
        if(isset($_GET["start_tanggal"]) && $this->input->get('start_tanggal') != ""){
            $data['start_tanggal'] = $this->input->get('start_tanggal');
            $data['end_tanggal'] = $this->input->get('end_tanggal');
            $_GET['start_date'] = $this->input->get('start_tanggal');
            $_GET['end_date'] = $this->input->get('end_tanggal');
        } else {
            $data['start_tanggal'] = "-";
            $data['end_tanggal'] = "-";
        }
        if(isset($_GET['key']) && $this->input->get('key')!=""){
            $data['cari'] = $this->input->get('key');
        } else {
            $data['cari'] = " - ";
        }
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->dpp_reseller->dpp_reseller_data_filter($query);
        $list =  $this->dpp_reseller->dpp_reseller_data_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {

            $key->no = $i;
            $i++;
            $key->action = null;
            $key->jumlah_bonus = ($key->jumlah_bonus);
            $key->dasar_pemotongan = ($key->dasar_pemotongan);
            $key->dpp_kumulatif = ($key->dpp_kumulatif);
            $key->ptkp = ($key->ptkp);
            $key->sisa_ptkp_perbulan = ($key->sisa_ptkp_perbulan);
            $key->pkp = ($key->pkp);
            $key->pkp_kumulatif = ($key->pkp_kumulatif);
            $key->pph_npwp = ($key->pph_npwp);
            $key->pph_non_npwp = ($key->pph_non_npwp);
            $key->total_pph = ($key->total_pph);
        }
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
            ->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
            ->setTitle('Laporan PPH 21')
            ->setSubject('');
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            )
        );
        $border = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        );

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A7', 'No')
            ->setCellValue('B7', 'Nama')
            ->setCellValue('C7', 'Tanggal')
            ->setCellValue('D7', 'Jumlah Bonus')
            ->setCellValue('E7', 'DPP')
            ->setCellValue('F7', 'DPP Kumulatif')
            ->setCellValue('G7', 'PTKP')
            ->setCellValue('H7', 'Sisa PTKP')
            ->setCellValue('I7', 'PKP')
            ->setCellValue('J7', 'PKP Kumultaif')
            ->setCellValue('K7', 'Persentase')
            ->setCellValue('L7', 'PPH NPWP')
            ->setCellValue('M7', 'PPH Non NPWP')
            ->setCellValue('N7', 'Total PPH')

        ;
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(18);
        $spreadsheet->getActiveSheet()->getStyle("A7:N7")->applyFromArray($style);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $date = date("Y-m-d");
        $sum = 0;
        $i=8; foreach($list as $key) {

            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key->no)
                ->setCellValue('B'.$i, $key->nama)
                ->setCellValue('C'.$i, $key->tanggal)
                ->setCellValue('D'.$i, $key->jumlah_bonus)
                ->setCellValue('E'.$i, $key->dasar_pemotongan)
                ->setCellValue('F'.$i, $key->dpp_kumulatif)
                ->setCellValue('G'.$i, $key->ptkp)
                ->setCellValue('H'.$i, $key->sisa_ptkp_perbulan)
                ->setCellValue('I'.$i, $key->pkp)
                ->setCellValue('J'.$i, $key->pkp_kumulatif)
                ->setCellValue('K'.$i, $key->persentase)
                ->setCellValue('L'.$i, $key->pph_npwp)
                ->setCellValue('M'.$i, $key->pph_non_npwp)
                ->setCellValue('N'.$i, $key->total_pph)
            ;
            $i++;
        }

        $spreadsheet->getActiveSheet()->getStyle("A7:N".($i-1))->applyFromArray($border);
        $spreadsheet->getActiveSheet()->getStyle('A7:N7')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('BEBEBE');
        $spreadsheet->getActiveSheet()->getStyle('N8:N'.$i)->getAlignment()->setWrapText(true);
        // Rename worksheet
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(1);
        $drawing->setWidth(80);
        $drawing->setHeight(80);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
        // $spreadsheet->setActiveSheetIndex(0)->setCellValue('J1','Laporan Stok bahan');
        // $spreadsheet->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
        // $spreadsheet->getActiveSheet()->getStyle("J1:J4")->applyFromArray($right);
        // $spreadsheet->getActiveSheet()->setTitle('Laporan Order bahan');
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Omset Leader Real'.$date.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}

/* End of file AccountingController.php */
/* Location: ./application/controllers/AccountingController.php */