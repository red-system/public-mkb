<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/phpseclib/Net/SFTP.php';
class PiutangProdukController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_produk','',true);
        $this->load->model('voucher_produk','',true);
        $this->load->model('piutang_produk','',true);
        $this->load->model('hutang_produk','',true);
    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/voucher.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Piutang Produk < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "hutang_piutang";
        $data['page'] = "piutang-produk";
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"po_produk_no"));
        array_push($column, array("data"=>"voucher_code"));
        array_push($column, array("data"=>"produk_nama"));
        array_push($column, array("data"=>"jumlah"));
        array_push($column, array("data"=>"status","template"=>"badgeTemplate"));
        array_push($column, array("data"=>"no_pembayaran"));
        array_push($column, array("data"=>"tanggal_pelunasan"));
        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['hutang_piutang']['piutang-produk'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"){
                $action[$key] = $value;
            }
        }
        $data["history"] = "";
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/piutang-produk/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $reseller = $this->getReseller();

        $reseller_id = $reseller->reseller_id;
        $result['iTotalRecords'] = $this->hutang_produk->count_piutang_all($reseller_id);
        $result['iTotalDisplayRecords'] = $this->hutang_produk->count_piutang_filter($query,$reseller_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->hutang_produk->_piutang_list($start,$length,$query,$reseller_id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->tanggal_pelunasan != null){
                $time = strtotime($key->tanggal_pelunasan);
                $key->tanggal_pelunasan = date('d-m-Y',$time);
            }

            $key->row_id = $key->hutang_produk_id;
            $key->jumlah = number_format($key->jumlah);
            $key->no = $i;
            if($key->status=="Hutang"){
                $key->status = "Piutang";
            }
            $key->action = null;
            $i++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */