<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class SuplierController extends MY_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->model('suplier','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app2.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Satuan < Master Data < ".$_SESSION["redpos_company"]['company_name'];;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"suplier_nama"));
		array_push($column, array("data"=>"suplier_alamat"));
		array_push($column, array("data"=>"suplier_telepon"));
		array_push($column, array("data"=>"suplier_email"));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$data["action"] = json_encode(array("view"=>true,"edit"=>true,"delete"=>true));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['suplier'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/suplier');
		$this->load->view('admin/static/footer');
	}

	function options(){
		$data = array();
		$suplier = $this->suplier->all_list();
		foreach ($suplier as $key) {
			array_push($data, strtoupper($key->suplier_nama));
		}
		echo json_encode($data);
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->suplier->suplier_count_all();
		$result['iTotalDisplayRecords'] = $this->suplier->suplier_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->suplier->suplier_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'suplier/delete/';
			$key->row_id = $key->suplier_id;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
			$suplier_nama = $this->input->post('suplier_nama');
			$suplier_alamat = $this->input->post('suplier_alamat');
			$suplier_telepon = $this->input->post('suplier_telepon');
			$suplier_email = $this->input->post('suplier_email');
			$data = array("suplier_nama"=>$suplier_nama,"suplier_alamat"=>$suplier_alamat,"suplier_telepon"=>$suplier_telepon,"suplier_email"=>$suplier_email);
			$insert = $this->suplier->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}

		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["suplier_nama"] = $this->input->post('suplier_nama');
		$data["suplier_alamat"] = $this->input->post('suplier_alamat');
		$data["suplier_telepon"] = $this->input->post('suplier_telepon');
		$data["suplier_email"] = $this->input->post('suplier_email');
		$suplier_id = $this->input->post('suplier_id');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->suplier->update_by_id('suplier_id',$suplier_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}

		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->suplier->delete_by_id("suplier_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
    function import_form(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->css, "vendors/general/select2/dist/css/select2.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/general/dropzone/dist/dropzone.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/dropzone/dist/dropzone.css");
        array_push($this->js, "app/custom/general/crud/forms/widgets/dropzone.js");
        array_push($this->js, "script/app2.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Suplier < Inventori < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "master_data";
        $data['page'] = $this->uri->segment(1);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/suplier/import');
        $this->load->view('admin/static/footer');
    }
    function import_save(){
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = 'import/';
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('data')) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');


        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('import/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
            $this->suplier->delete_all();
            $data = array();
            $numrow = 1;
            $this->produk->start_trans();
            foreach($sheet as $row){
                if($row["A"]==''&&$row["B"]==''&&$row["C"]==''&&$row["D"]==''&&$row["E"]==''&&$row["F"]==''&&$row["G"]==''&&$row["H"]==''&&$row["I"]==''&&$row["J"]=='')
                    break;
                if($numrow > 1){

                    $data = array("suplier_kode"=>$row["A"],"suplier_nama"=>$row["B"],"suplier_alamat"=>$row["C"],"suplier_telepon"=>$row["D"],"suplier_email"=>$row["E"]);
                    $this->suplier->insert($data);
                }
                $numrow++;
            }
            $import =  $this->produk->result_trans();
            echo json_encode($import);

        }
    }
    function pdf(){
        $data['cari'] = (($this->input->get('key')!="")?$this->input->get('key'):"");
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->suplier->suplier_count_filter($query);
        $list =  $this->suplier->suplier_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {
            $key->no = $i;
            $i++;
        }
        $data['list'] = $list;
        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/suplier_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
            $date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }

        $mpdf->Output('Suplier'.$date.".pdf","D");
    }
    function excel(){
        $data['cari'] = (($this->input->get('key')!="")?$this->input->get('key'):"");
        $query = $this->input->get('key');
        $start = 0;
        $length = $this->suplier->suplier_count_filter($query);
        $list =  $this->suplier->suplier_list($start,$length,$query);
        $i = $start+1;
        foreach ($list as $key) {
            $key->no = $i;
            $i++;
        }
        $spreadsheet = new Spreadsheet();
        // Set document properties
        $spreadsheet->getProperties()->setCreator($_SESSION["redpos_company"]['company_name'])
            ->setLastModifiedBy($_SESSION["redpos_login"]['user_name'])
            ->setTitle('Suplier')
            ->setSubject('');
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            )
        );
        $border = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        );

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A6', 'No')
            ->setCellValue('B6', 'Kode')
            ->setCellValue('C6', 'Nama')
            ->setCellValue('D6', 'Alamat')
            ->setCellValue('E6', 'Telepon')
            ->setCellValue('F6', 'Email')
        ;

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(22);
        $spreadsheet->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style);


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $date = date("Y-m-d");
        $i=7; foreach($list as $key) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key->no)
                ->setCellValue('B'.$i, $key->suplier_kode)
                ->setCellValue('C'.$i, $key->suplier_nama)
                ->setCellValue('D'.$i, $key->suplier_alamat)
                ->setCellValue('E'.$i, $key->suplier_telepon)
                ->setCellValue('F'.$i, $key->suplier_email);
            $i++;
        }
        $range = 'E7'.':E'.$i;
        $spreadsheet->getActiveSheet()
            ->getStyle($range)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
        $spreadsheet->getActiveSheet()->getStyle("A6:F".($i))->applyFromArray($border);
        $spreadsheet->getActiveSheet()->getStyle('A6:F6')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('BEBEBE');
        $spreadsheet->getActiveSheet()->getStyle('A7:F'.$i)->getAlignment()->setWrapText(true);
        // Rename worksheet
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(1);
        $drawing->setWidth(80);
        $drawing->setHeight(80);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$_SESSION["redpos_company"]['company_name']);
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$_SESSION["redpos_company"]['company_address']);
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$_SESSION["redpos_company"]['company_phone']);
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('F1','Suplier');
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('F2','Pencarian : '.$data['cari']);
        $spreadsheet->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle("F1:F5")->applyFromArray($right);
        $spreadsheet->getActiveSheet()->setTitle('Suplier');
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Suplier'.$date.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

}

/* End of file SuplierController.php */
/* Location: ./application/controllers/SuplierController.php */