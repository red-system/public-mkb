<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/phpseclib/Net/SFTP.php';

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user', '', true);
        $this->load->model('master_login', '', true);
        $this->load->model('staff', '', true);
        $this->load->model('reseller', '', true);
        $this->load->model('npwp', '', true);
        $this->load->helper('string');
        $this->load->library('upload');
        $this->load->library('image_lib');
    }

    public function index()
    {
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->js, "vendors/general/jquery-form/dist/jquery.form.min.js");
        array_push($this->js, "vendors/general/sweetalert2/dist/sweetalert2.min.js");
        array_push($this->js, "vendors/custom/components/vendors/sweetalert2/init.js");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
        array_push($this->js, "vendors/general/clipboard2/clipboard.min.js");
        array_push($this->js, "script/admin/profile.js");
        array_push($this->css, "vendors/general/sweetalert2/dist/sweetalert2.css");
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $user = $this->master_login->user_by_id($_SESSION["redpos_login"]['user_id']);

//        $user->referal_link = base_url()."registrasi/".str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($user->referal_code));
        $user->referal_link = $this->config->item('url-landing') . $user->referal_code;
        $user->referal_link_upgrade = $this->config->item('url-upgrade') . $user->referal_code;
        $data['user'] = $user;
        $login = $this->db->where("lokasi_id", @$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();

        $reseller_id = $login->reseller_id;

        $reseller = $this->reseller->row_by_id($reseller_id);
        $data["province"] = $this->db->get("mykindofbeauty_kemiri.province")->result();
        if ($reseller != null) {
            $data["city"] = $this->db->where("mykindofbeauty_kemiri.city.province_id", $reseller->province_id)->get("mykindofbeauty_kemiri.city")->result();
            $data["sub_district"] = $this->db->where("mykindofbeauty_kemiri.subdistrict.subdistrict_id", $reseller->subdistrict_id)->get("mykindofbeauty_kemiri.subdistrict")->result();

        }
        $is_save = false;
        $find_npwp = $this->npwp->find_npwp($reseller_id);
        if($find_npwp == true){
            $is_save = true;
        }
        $text_npwp = 'Pelaporan NPWP Anda sudah diproses, menunggu pengecekan dari admin kami';
        $cek_approved = $this->npwp->cekapprove($reseller_id);
        if($cek_approved == true){
            $text_npwp = 'Pelaporan NPWP anda sudah di setujui admin';
        }
        $data['text_npwp'] = $text_npwp;
        $data['is_save'] = $is_save;
        $data["bank"] = $this->db->get("mykindofbeauty_kemiri.bank")->result();
        $data['reseller'] = $reseller;
        $data["meta_title"] = "User Profile < " . $_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "";
        $data['page'] = $this->uri->segment(1);
        $this->load->view('admin/static/header', $data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/profile');
        $this->load->view('admin/static/footer');

    }

    function personal_info()
    {
        $this->form_validation->set_rules('user_name', '', 'required');
        $this->form_validation->set_rules('email', '', 'required');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if ($this->form_validation->run() == TRUE) {
            $user_name = $this->input->post('user_name');
            $email = $this->input->post('email');
            $staff_phone_number = $this->input->post('phone_number');
            $tempat_lahir = $this->input->post('tempat_lahir');
            $tanggal_lahir = date("Y-m-d", strtotime($this->input->post('tanggal_lahir')));
            $staff_alamat = $this->input->post('staff_alamat');
            $data = array("staff_nama" => $user_name, "staff_email" => $email, "staff_phone_number" => $staff_phone_number, "tempat_lahir" => $tempat_lahir, "tanggal_lahir" => $tanggal_lahir, "staff_alamat" => $staff_alamat);
            $update = $this->staff->update_by_id('staff_id', $_SESSION["redpos_login"]['staff_id'], $data);
            if ($update) {
                $result['success'] = true;
                $result['message'] = "Success updated personal info";
                $_SESSION["redpos_login"]["user_name"] = $user_name;
                $_SESSION["redpos_login"]["email"] = $email;
                $reseller = $this->getReseller();
                $reseller_id = $reseller == null ? null : $reseller->reseller_id;
                if ($reseller_id != null) {
                    $email = $this->input->post('email');
                    $phone = $this->input->post('phone_number');
                    $nama = $this->input->post('user_name');
                    $no_ktp = $this->input->post('no_ktp');
                    $province_id = $this->input->post('province_id');
                    $city_id = $this->input->post('city_id');
                    $subdistrict_id = $this->input->post('subdistrict_id');
                    $alamat = $this->input->post('staff_alamat');
                    $bank_id = $this->input->post('bank_id');
                    $bank_rekening = $this->input->post('bank_rekening');
                    $bank_atas_nama = $this->input->post('bank_atas_nama');
                    $tokopedia = $this->input->post('tokopedia');
                    $tiktok = $this->input->post('tiktok');
                    $shopee = $this->input->post('shopee');
                    $maps = $this->input->post('maps_url');
                    $data = array(
                        "email" => $email,
                        "no_ktp" => $no_ktp,
                        "phone" => $phone,
                        "nama" => $nama,
                        "bank_id" => $bank_id,
                        "province_id" => $province_id,
                        "city_id" => $city_id,
                        "subdistrict_id" => $subdistrict_id,
                        "bank_id" => $bank_id,
                        "bank_rekening" => $bank_rekening,
                        "bank_atas_nama" => $bank_atas_nama,
                        "tokopedia" => $tokopedia,
                        "tiktok" => $tiktok,
                        "shopee" => $shopee,
                        "maps" => $maps,
                    );
                    $this->reseller->update_by_id("reseller_id", $reseller_id, $data);
                }
            } else {
                $result['message'] = "Failed to update";
            }
        }
        echo json_encode($result);
    }
    function upgrade_user()
    {
        $result['success'] = false;
        $result['message'] = "missing parameter";
        $user_id = $_SESSION["redpos_login"]['user_id'];

        $data_user = $this->master_login->row_by_id($user_id);
        $old_username = '#'.$data_user->username;
        $reseller_id = $data_user->reseller_id;

        $old_reseller = array();
        $data_reseller = $this->reseller->row_by_id($reseller_id);
        $old_reseller['email'] = '#'.$data_reseller->email;

        $data = array(
            "email"=>$data_reseller->email,
            "no_ktp"=>$data_reseller->no_ktp,
            "phone"=>$data_reseller->phone,
            "nama"=>$data_reseller->nama,
            "type"=>'super agen',
            "province_id"=>$data_reseller->province_id,
            "city_id"=>$data_reseller->city_id,
            "subdistrict_id"=>$data_reseller->subdistrict_id,
            "alamat"=>$data_reseller->alamat,
            "outlet_province_id"=>$data_reseller->outlet_province_id,
            "outlet_city_id"=>$data_reseller->outlet_city_id,
            "outlet_subdistrict_id"=>$data_reseller->outlet_subdistrict_id,
            "outlet_alamat"=>$data_reseller->outlet_alamat,
            "foto_profile"=>$data_reseller->foto_profile,
            "foto_ktp"=>$data_reseller->foto_ktp,
            "selfi_ktp"=>$data_reseller->selfi_ktp,
        );

        $nonactive_email = $this->reseller->update_by_id('reseller_id', $reseller_id, $old_reseller);
        $nonactive_user = $this->master_login->nonactive_user($_SESSION["redpos_login"]['user_id'], $old_username);

        $referal_code = $this->input->post('referal_code');
        if($referal_code!=""){
            $reseler = $this->reseller->resseler_by_referal_code($referal_code);
            $data["referal_id"] = $reseler->reseller_id;
        }

        $insert = $this->reseller->insert($data);

        
        if ($insert) {
            $result["success"] = true;
            $result["message"] = "Akun dalam proses pengajuan Super Reseller";
        } else {
            $result["message"] = "Gagal upgrade";
        }
        
        echo json_encode($result);
    }

    function change_password()
    {
        $this->form_validation->set_rules('old_pass', '', 'required');
        $this->form_validation->set_rules('new_pass', '', 'required');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if ($this->form_validation->run() == TRUE) {
            $old_pass = $this->input->post('old_pass');
            $new_pass = $this->input->post('new_pass');
            $email = $_SESSION["redpos_login"]['email'];
            $user = $this->master_login->user_by_id($_SESSION["redpos_login"]['user_id']);
            if (password_verify($old_pass, $user->password)) {
                $password = password_hash($new_pass, PASSWORD_BCRYPT);
                $change = $this->master_login->change_password($_SESSION["redpos_login"]['user_id'], $password);
                if ($change) {
                    $result["success"] = true;
                    $result["message"] = "Your password successfully changed";
                } else {
                    $result["message"] = "Failed to change password";
                }
            } else {
                $result["message"] = "Wrong password";
            }
        }
        echo json_encode($result);
    }

    function change_avatar()
    {
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != null) {
            $url = $_SESSION["redpos_login"]['avatar'];
            $url = $this->uploadImage("avatar", $url);
            $user_id = $_SESSION["redpos_login"]['user_id'];
            if ($url != "failed") {
                $change = $this->master_login->change_avatar($user_id, $url);
                if ($change) {
                    $temp = $_SESSION["redpos_login"]['avatar'];
                    $this->deleteImage($temp);
                    $result['success'] = true;
                    $result['message'] = "Success updating your avatar";
                    $result['url'] = $url;
                    $_SESSION["redpos_login"]['avatar'] = $url;
                } else {
                    $result['message'] = "Updating failed";
                }
            } else {
                $result['message'] = "Uploading file failed";
            }
        }
        echo json_encode($result);

    }

    function uploadImage($file, $url)
    {

        $sftp = new Net_SFTP($this->config->item('image_host'));

        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }

        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $date = new DateTime();
        $file_name = $date->getTimestamp() . random_string('alnum', 5);
        $output = $sftp->put("../../../var/www/html/development/dev-storage.redsystem.id/redpos/" . $file_name . "." . $ext, $_FILES[$file]['tmp_name'], NET_SFTP_LOCAL_FILE);
        if (!$output) {
            $url = "failed";
            return $url;
        } else {
            $url = $file_name . '.' . $ext;
        }
        return $url;
    }

    function deleteImage($filename)
    {
        $sftp = new Net_SFTP($this->config->item('image_host'));
        if (!$sftp->login($this->config->item('image_username'), $this->config->item('image_password'))) {
            exit('Login Failed');
        }
        if ($sftp->file_exists("../../../var/www/html/development/dev-storage.redsystem.id/redpos/" . $filename)) {
            $output = $sftp->delete("../../../var/www/html/development/dev-storage.redsystem.id/redpos/" . $filename, false);
        }


    }

    function resizeImage($url, $name, $size)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = FCPATH . $url;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $size;
        $config['height'] = $size;
        $temp = explode(".", $name);
        $folder = $temp[0];
        $config['new_image'] = FCPATH . $url;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    function upload_npwp()
    {
        $images = "";
        if (!empty($_FILES['foto_npwp']['name'])) {
            $images = $this->upload("foto_npwp");
        }
        if (!empty($_FILES['foto_selfi']['name'])) {
            $images = $this->uploadImage("foto_selfi", $images);
        }
        $data['url'] = $images;
        echo json_encode($data);
    }

    function update_npwp()
    {
        $this->form_validation->set_rules('no_npwp', '', 'required');
        $this->form_validation->set_rules('input_foto_npwp', '', 'required');
        $this->form_validation->set_rules('input_foto_selfi', '', 'required');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if ($this->form_validation->run() == TRUE) {
            $result['success'] = true;
            $result['message'] = "Success updated npwp info";
            $reseller = $this->getReseller();
            $reseller_id = $reseller == null ? null : $reseller->reseller_id;
            $count = $this->npwp->count($reseller_id);
            if ($count > 0){
                $result['success'] = false;
                $result['message'] = "Reseller sudah pernah melakukan upload npwp";
            }else{
                if ($reseller_id != null) {
                    $no_npwp = $this->input->post('no_npwp');
                    $foto_npwp = $this->input->post('input_foto_npwp');
                    $foto_selfi = $this->input->post('input_foto_selfi');
                    $penghasilan_lain = $this->input->post('input_penghasilan_lain');
                    $keterangan_lain = $this->input->post('keterangan_lain');


                    $data = array(
                        "reseller_id" => $reseller_id,
                        "no_npwp" => $no_npwp,
                        "foto_npwp" => $foto_npwp,
                        "foto_selfi" => $foto_selfi,
                        "penghasilan_lain" => $penghasilan_lain,
                        "keterangan_lain" => $keterangan_lain,
                    );
                    if($penghasilan_lain==0){
                        $kelamin = $this->input->post('kelamin');
                        $status_pernikahan = $this->input->post('status_pernikahan');
                        $jumlah_tanggungan = $this->input->post('jumlah_tanggungan');

                        $data['kelamin'] = $kelamin;
                        $data['status_pernikahan'] = $status_pernikahan;
                        $data['jumlah_tanggungan'] = $jumlah_tanggungan;

                        $ptkp = 54000000;
                        if($status_pernikahan=='menikah'){
                            if($kelamin=='laki-laki'){
                                $ptkp = 58500000+($jumlah_tanggungan*4500000);
                            }else{
                                $ptkp = 54000000;
                            }
                        } else {
                            $ptkp = 54000000+($jumlah_tanggungan*4500000);
                        }
                        $data['ptkp'] = $ptkp;
                    }
                    $this->npwp->insert($data);
                }
            }
        } else {
            $result['message'] = "Failed to update";
        }
        echo json_encode($result);
    }
    function upload($file){
        $path = $_FILES[$file]['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $date = new DateTime();
        $file_name = $date->getTimestamp() . random_string('alnum', 5).".".$ext;
        $config['upload_path']          = 'npwp/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name'] = $file_name;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($file))
        {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
            $url = "failed";
            return $url;
        }
        else
        {
            $url = $file_name;
            return $url;
        }
    }
}