<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperAgentController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reseller','',true);
        $this->load->model('province','',true);
        $this->load->model('master_login','',true);
        $this->load->library('main');

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $jumlah = $this->reseller->super_maps();
        $data['empty_maps'] = $jumlah;
        $column = array();
        $data["meta_title"] = "Prospective Reseller < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"phone"));
        array_push($column, array("data"=>"email"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"alamat"));
        array_push($column, array("data"=>"outlet_subdistrict_name"));
        array_push($column, array("data"=>"outlet_alamat"));
        array_push($column, array("data"=>"referal_nama"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['reseller']['super-agen'] as $key => $value) {
            if($key != "list" && $key != "akses_menu"&& $key != "activate"&& $key != "banned" && $key != "update" ){
                $action[$key] = $value;
            }
        }
        $data['action'] = json_encode($action);

        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/super-agen/index');
        $this->load->view('admin/static/footer');
    }
    function list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->reseller->super_all();
        $result['iTotalDisplayRecords'] = $this->reseller->super_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->link_foto_profile = $this->config->item('dev_storage_image').$key->foto_profile;
            $key->link_foto_ktp = $this->config->item('dev_storage_image').$key->foto_ktp;
            $key->link_selfi_ktp = $this->config->item('dev_storage_image').$key->selfi_ktp;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function accept(){
        $result['success'] = false;
        $result['message'] = "missing parameter";
        $reseller_id = $this->input->post('reseller_id');
        $data['status'] = 'accept';
        $reseller = $this->reseller->detail('reseller_id',$reseller_id)->row();
        $update = $this->reseller->update_by_id('reseller_id',$reseller_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Berhasil merubah status reseller";

            $url = base_url().'activation/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($reseller_id));
            $result['url'] = $url;
            $mailContentAdmin = ', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn-primary {
            color: #fff;
            background-color: #209ebb;
            border-color: #209ebb;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 450px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo ' .$reseller->nama.',</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Registrasi Anda sudah diterima, klik link di bawah untuk
                                aktivasi akun Anda.
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <a href="'.$url.'" class="btn btn-success btn-pill" style="color: #fff">Aktivasi Akun</a>
                        </div>
                        <span>
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@mykindofbeauty.co.id/</span>
                        </span>

                        <div style="margin-top: 40px"></div>
                         <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <a href="http://app.mykindofbeauty.co.id/" class="btn btn-primary btn-pill" style="color: #fff">Login Area</a>
                        </div>
                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                        <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>
                </td>
            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>
</div>
</body></html>';
            $this->main->mailer_auth('Pendaftaran Diterima', $reseller->email, $reseller->nama, $mailContentAdmin);
        }
        echo json_encode($result);
    }
    function refuse(){
        $result['success'] = false;
        $result['message'] = "missing parameter";
        $reseller_id = $this->input->post('reseller_id');
        $alasan = $this->input->post('alasan_banned');;
        $reseller = $this->reseller->detail('reseller_id',$reseller_id)->row();
        $email = $reseller->email;
        $old_email = '#'.$email;

        $old_account = $this->reseller->row_by_field('email',$old_email);
        if (!empty($old_account)) {
            $old_reseller_id = $old_account->reseller_id;
            $data_reseller['email'] = $email;
            $rollback_reseller = $this->reseller->update_by_id('reseller_id', $old_reseller_id, $data_reseller);

            $old_user = $this->master_login->row_by_field('reseller_id', $old_reseller_id);
            $old_username = $old_user->username;
            $old_user_id = $old_user->id;
            $data_user['username'] = str_replace("#","", $old_username);
            $rollback_user = $this->master_login->update_by_id('id', $old_user_id, $data_user);
        }

        $delete = $this->reseller->delete_by_id('reseller_id',$reseller_id);

        if($delete){
            $this->deleteImage($reseller->foto_profile);
            $this->deleteImage($reseller->foto_ktp);
            $this->deleteImage($reseller->selfi_ktp);
            $result['success'] = true;
            $result['message'] = "Berhasil merubah status reseller";
            $mailContentAdmin = '<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #bebebe">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://dev.redsystem.id/redpos-kemiri/assets/media/logo_mkb_health_and_beauty1.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 250px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo '.$reseller->nama.',</h4>
                        <h3 style="text-align: left;font-weight: 600">Mohon Maaf,</h3>
                        <span>Pendaftaran anda kami tolak karena '.$alasan.'
                        </span>
                        <br>


                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim My Kind Of Beauty
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                        <span style="font-size: 10px">My Kind Of Beauty. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/mkb.mykindofbeauty/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/mkbhealthandbeauty">Facebook</a><a style="color: #900000" href="http://www.mykindofbeauty.co.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>';
            $this->main->mailer_auth('Pendaftaran Ditolak', $reseller->email, $reseller->nama, $mailContentAdmin);

        }
        echo json_encode($result);
    }
    function aktif(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $jumlah = $this->reseller->super_maps();
        $data['empty_maps'] = $jumlah;
        $column = array();
        $data["meta_title"] = "Aktif Agen < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = 'super-agen';
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"username"));
        array_push($column, array("data"=>"phone"));
        array_push($column, array("data"=>"email"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"alamat"));
        array_push($column, array("data"=>"outlet_subdistrict_name"));
        array_push($column, array("data"=>"outlet_alamat"));
        array_push($column, array("data"=>"referal_nama"));
        array_push($column, array("data"=>"status_maps","template"=>"badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['reseller']['prospective-reseller'] as $key => $value) {
            if($key == "banned"){
                $action[$key] = $value;
            }

        }
        $action['complate'] = true;
        $action['update'] = true;
        $data['action'] = json_encode($action);

        $data['province'] = $this->province->getProvinceAll();

        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/super-agen/active_index');
        $this->load->view('admin/static/footer');
    }
    function aktif_list(){

        if(isset($_GET["columns"][1]["search"]["value"]) && $_GET["columns"][1]["search"]["value"] != ""){
			$_GET['province_id'] = $_GET["columns"][1]["search"]["value"];
		}

        if(isset($_GET["columns"][2]["search"]["value"]) && $_GET["columns"][2]["search"]["value"] != ""){
			$_GET['city_id'] = $_GET["columns"][2]["search"]["value"];
		}

        if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
			$_GET['subdistrict_id'] = $_GET["columns"][3]["search"]["value"];
		}
        
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "active";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->longitude==null&&$key->latitude==null){
                $key->status_maps = 'empty';
            }else{
                $key->status_maps = 'filled';
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function pasif(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $jumlah = $this->reseller->super_maps();
        $data['empty_maps'] = $jumlah;
        $column = array();
        $data["meta_title"] = "Aktif Agen < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"username"));
        array_push($column, array("data"=>"phone"));
        array_push($column, array("data"=>"email"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"alamat"));
        array_push($column, array("data"=>"outlet_subdistrict_name"));
        array_push($column, array("data"=>"outlet_alamat"));
        array_push($column, array("data"=>"referal_nama"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        $action['update'] = true;
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/super-agen/pasif_index');
        $this->load->view('admin/static/footer');
    }
    function pasif_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "pasive";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function banned(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Banned Agen < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        $jumlah = $this->reseller->super_maps();
        $data['empty_maps'] = $jumlah;
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"phone"));
        array_push($column, array("data"=>"email"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"alamat"));
        array_push($column, array("data"=>"outlet_subdistrict_name"));
        array_push($column, array("data"=>"outlet_alamat"));
        array_push($column, array("data"=>"alasan_banned"));
        array_push($column, array("data"=>"referal_nama"));


        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['reseller']['prospective-reseller'] as $key => $value) {
            if($key == "activate"){
                $action[$key] = $value;
            }

        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/super-agen/banned_index');
        $this->load->view('admin/static/footer');
    }
    function banned_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "banned";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }


    function accepted(){
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "script/app2.js");
        array_push($this->js, "script/admin/prospective.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $jumlah = $this->reseller->super_maps();
        $data['empty_maps'] = $jumlah;
        $column = array();
        $data["meta_title"] = "Accepted Agen < Reseller < ".$_SESSION["redpos_company"]['company_name'];;
        $data['parrent'] = "reseller";
        $data['page'] = $this->uri->segment(1);
        array_push($column, array("data"=>"no"));
        array_push($column, array("data"=>"nama"));
        array_push($column, array("data"=>"phone"));
        array_push($column, array("data"=>"email"));
        array_push($column, array("data"=>"no_ktp"));
        array_push($column, array("data"=>"alamat"));
        array_push($column, array("data"=>"outlet_subdistrict_name"));
        array_push($column, array("data"=>"outlet_alamat"));
        array_push($column, array("data"=>"referal_nama"));
        array_push($column, array("data"=>"status_maps","template"=>"badgeTemplate"));

        $data['column'] = json_encode($column);
        $data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
        $akses_menu = json_decode($this->menu_akses,true);
        $action = array();
        foreach ($akses_menu['reseller']['prospective-reseller'] as $key => $value) {
            if($key == "resend"){
                $action[$key] = $value;
            }

        }
        $data['action'] = json_encode($action);
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/super-agen/accepted_index');
        $this->load->view('admin/static/footer');
    }
    function accepted_list(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $status = "accept";
        $result['iTotalRecords'] = $this->reseller->super_agen_status_all($status);
        $result['iTotalDisplayRecords'] = $this->reseller->super_agen_status_filter($query,$status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->reseller->super_agen_status_list($start,$length,$query,$status);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            if($key->longitude==null&&$key->latitude==null){
                $key->status_maps = 'empty';
            }else{
                $key->status_maps = 'filled';
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->reseller_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

}

/* End of file SatuanController.php */
/* Location: ./application/controllers/SatuanController.php */