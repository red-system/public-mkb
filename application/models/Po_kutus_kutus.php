<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_kutus_kutus extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "po_kutus_kutus";
    }
    function get_kode_po_kutus_kutus(){
        $year = date("y");
        $month = date("m");
        $prefix = "POK/".$month.$year."/";
        $this->db->like('po_kutus_kutus_no', $prefix, 'BOTH');
        $this->db->select('(max(urutan)+1) as kode');
        $this->db->from('po_kutus_kutus');
        $result = $this->db->get()->row()->kode;
        if ($result != null){
            return $prefix.($result);
        } else {
            return $prefix."1";
        }
    }
    function po_kutus_kutus_count_all(){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->group_by('po_kutus_kutus.po_kutus_kutus_id');
        if($this->uri->segment(1)=="order-produsen"){
            $this->db->where('status_penerimaan !=', "Diterima");
        }

        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_kutus_kutus_count_filter($query){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_kutus_kutus.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_kutus_kutus.grand_total', $total, 'BOTH');
        $this->db->group_end();
        if($this->uri->segment(1)=="order-produsen"){
            $this->db->where('status_penerimaan !=', "Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_kutus_kutus.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_kutus_kutus.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_kutus_kutus.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_kutus_kutus.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_kutus_kutus.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_kutus_kutus.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_kutus_kutus.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_by('po_kutus_kutus.po_kutus_kutus_id');
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_kutus_kutus_list($start,$length,$query){
        $this->db->select('po_kutus_kutus.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_kutus_kutus.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_kutus_kutus.grand_total', $total, 'BOTH');
        $this->db->group_end();
        if($this->uri->segment(1)=="order-produsen"){

            $this->db->where('status_penerimaan', "Belum Diterima");
            $this->db->or_where('status_penerimaan', "Sebagian");

        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_kutus_kutus.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_kutus_kutus.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_kutus_kutus.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_kutus_kutus.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_kutus_kutus.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_kutus_kutus.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_kutus_kutus.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_by('po_kutus_kutus.po_kutus_kutus_id');
        $this->db->order_by('po_kutus_kutus.po_kutus_kutus_id', 'desc');
        return $this->db->get('po_kutus_kutus', $length, $start)->result();
    }
    function po_kutus_kutus_count_all_history(){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->group_by('po_kutus_kutus.po_kutus_kutus_id');
        $this->db->where('status_penerimaan', "Diterima");
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_kutus_kutus_count_filter_history($query){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->group_start();
        $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_penerimaan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_kutus_kutus.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where('status_penerimaan', "Diterima");
        $this->db->group_by('po_kutus_kutus.po_kutus_kutus_id');
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_kutus_kutus_list_history($start,$length,$query){
        $this->db->select('po_kutus_kutus.*,suplier.suplier_nama');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->group_start();
        $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_kutus_kutus.status_penerimaan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_kutus_kutus.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where('status_penerimaan', "Diterima");
        $this->db->group_by('po_kutus_kutus.po_kutus_kutus_id');
        $this->db->order_by('po_kutus_kutus.po_kutus_kutus_id', 'desc');
        return $this->db->get('po_kutus_kutus', $length, $start)->result();
    }
    function insert_po_kutus_kutus(){
        $this->db->trans_begin();
        $po_no = $this->input->post('po_kutus_kutus_no');
        $temp = explode("/", $po_no);
        $urutan = $temp[sizeof($temp)-1];
        $data['po_kutus_kutus_no'] = $po_no;
        $data['urutan'] = $urutan;
        $data['suplier_id'] = $this->input->post('suplier_id');
        $temp = strtotime($this->input->post('tanggal_pemesanan'));
        $tanggal_pemesanan = date("Y-m-d",$temp);
        $data['tanggal_pemesanan'] = $tanggal_pemesanan;
        $data['keterangan'] = $this->input->post('keterangan');
        $jenis_pembayaran = $this->input->post('jenis_pembayaran');
        $data['jenis_pembayaran'] = $jenis_pembayaran;
        if($jenis_pembayaran == "kas"){
            $data['tipe_pembayaran'] = $this->input->post('tipe_pembayaran_id');
            $data['tipe_pembayaran_no'] = $this->input->post('tipe_pembayaran_no');
            $data['tipe_pembayaran_keterangan'] = $this->input->post('tipe_pembayaran_keterangan');
            $data['status_pembayaran'] = "Lunas";
        } else {
            $data['tipe_pembayaran'] = null;
            $data['tipe_pembayaran_nama'] = "Kredit";
            $data['status_pembayaran'] = "Hutang";
        }
        $data["total"] = $this->input->post('total_item');
        $data["grand_total"] = $this->input->post('grand_total');
        $data["potongan"] = $this->string_to_number($this->input->post('potongan'));
        $data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
        $data["status_penerimaan"] = "Belum Diterima";
        $this->db->insert('po_kutus_kutus', $data);
        $po_kutus_kutus_id = $this->db->insert_id();
        $item = $this->input->post('item_produk');
        foreach ($item as $key) {
            $data = array();
            $data["po_kutus_kutus_id"] = $po_kutus_kutus_id;
            $data["produk_id"] = $key["produk_id"];
            $data["last_hpp"] = $this->string_to_number($key["last_hpp"]);
            $data["harga"] = $this->string_to_number($key["harga"]);
            $data["jumlah"] = $this->string_to_number($key["jumlah"]);
            $data["sisa"] = $this->string_to_number($key["jumlah"]);
            $data["sub_total"] = $this->string_to_number($key["subtotal"]);
            $this->db->insert('po_kutus_kutus_detail', $data);
        }
        if($jenis_pembayaran != "kas"){
            $data = array();
            $data['po_kutus_kutus_id'] = $po_kutus_kutus_id;
            $temp = strtotime($this->input->post('tenggat_pelunasan')) ;
            $tenggat_pelunasan = date("Y-m-d",$temp);
            $data['tenggat_pelunasan'] = $tenggat_pelunasan;
            $this->db->insert('hutang', $data);
        }
        if ($this->db->trans_status() === FALSE)
            return FALSE;
        $this->db->trans_commit();
        return TRUE;
    }
    function edit_po_kutus_kutus(){
        $this->db->trans_begin();
        $po_kutus_kutus_id = $this->input->post('po_kutus_kutus_id');
        $this->db->where('po_kutus_kutus_id', $po_kutus_kutus_id);
        $this->db->delete('po_kutus_kutus_detail');
        $po_no = $this->input->post('po_kutus_kutus_no');
        $temp = explode("/", $po_no);
        $urutan = $temp[sizeof($temp)-1];
        $data['po_kutus_kutus_no'] = $po_no;
        $data['urutan'] = $urutan;
        $data['suplier_id'] = $this->input->post('suplier_id');
        $temp = strtotime($this->input->post('tanggal_pemesanan'));
        $tanggal_pemesanan = date("Y-m-d",$temp);
        $data['tanggal_pemesanan'] = $tanggal_pemesanan;
        $data['keterangan'] = $this->input->post('keterangan');
        $jenis_pembayaran = $this->input->post('jenis_pembayaran');
        $data['jenis_pembayaran'] = $jenis_pembayaran;
        if($jenis_pembayaran == "kas"){
            $data['tipe_pembayaran'] = $this->input->post('tipe_pembayaran_id');
            $data['tipe_pembayaran_no'] = $this->input->post('tipe_pembayaran_no');
            $data['tipe_pembayaran_keterangan'] = $this->input->post('tipe_pembayaran_keterangan');
            $data['status_pembayaran'] = "Lunas";
        } else {
            $data['tipe_pembayaran'] = null;
            $data['tipe_pembayaran_nama'] = "Kredit";
            $data['status_pembayaran'] = "Hutang";
        }
        $data["total"] = $this->input->post('total_item');
        $data["grand_total"] = $this->input->post('grand_total');
        $data["potongan"] = $this->string_to_number($this->input->post('potongan'));
        $data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
        $data["status_penerimaan"] = "Belum Diterima";
        $this->db->where('po_kutus_kutus_id', $po_kutus_kutus_id);
        $this->db->update('po_kutus_kutus', $data);
        $item = $this->input->post('item_produk');
        foreach ($item as $key) {
            $data = array();
            $data["po_kutus_kutus_id"] = $po_kutus_kutus_id;
            $data["produk_id"] = $key["produk_id"];
            $data["last_hpp"] = $this->string_to_number($key["last_hpp"]);
            $data["harga"] = $this->string_to_number($key["harga"]);
            $data["jumlah"] = $this->string_to_number($key["jumlah"]);
            $data["sub_total"] = $this->string_to_number($key["subtotal"]);
            $this->db->insert('po_kutus_kutus_detail', $data);
        }
        $data = array();
        $this->db->where('po_kutus_kutus_id', $po_kutus_kutus_id);
        $hutang = $this->db->get('hutang')->row();
        if($jenis_pembayaran != "kas"){
            if($hutang != null){
                $temp = strtotime($this->input->post('tenggat_pelunasan')) ;
                $tenggat_pelunasan = date("Y-m-d",$temp);
                $data['tenggat_pelunasan'] = $tenggat_pelunasan;
                $this->db->where('hutang_id', $hutang->hutang_id);
                $this->db->update('hutang', $data);
            } else {
                $temp = strtotime($this->input->post('tenggat_pelunasan')) ;
                $tenggat_pelunasan = date("Y-m-d",$temp);
                $data['po_kutus_kutus_id'] = $po_kutus_kutus_id;
                $data['tenggat_pelunasan'] = $tenggat_pelunasan;
                $this->db->insert('hutang', $data);
            }
        } else {
            $this->db->where('po_kutus_kutus_id', $po_kutus_kutus_id);
            $this->db->delete('hutang');
        }

        if ($this->db->trans_status() === FALSE)
            return FALSE;

        $this->db->trans_commit();
        return TRUE;
    }
    function po_kutus_kutus_detail_by_id($po_kutus_kutus_id){
        $this->db->where('po_kutus_kutus_detail.po_kutus_kutus_id', $po_kutus_kutus_id);
        $this->db->select('po_kutus_kutus_detail.*,produk.produk_nama,suplier.suplier_nama');
        $this->db->join('produk', 'produk.produk_id = po_kutus_kutus_detail.produk_id');
        $this->db->join('po_kutus_kutus', 'po_kutus_kutus.po_kutus_kutus_id = po_kutus_kutus_detail.po_kutus_kutus_id');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        return $this->db->get('po_kutus_kutus_detail')->result();
    }
    function po_kutus_kutus_by_id($po_kutus_kutus_id){
        $this->db->where('po_kutus_kutus.po_kutus_kutus_id', $po_kutus_kutus_id);
        $this->db->select('po_kutus_kutus.*,suplier.suplier_nama,hutang.tenggat_pelunasan');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('hutang', 'hutang.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id', 'left');
        return $this->db->get('po_kutus_kutus')->row();
    }
    function laporan_po_kutus_kutus_count_all(){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->join('produk', 'produk.produk_id = po_kutus_kutus_detail.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        return $this->db->count_all_results('po_kutus_kutus');

    }
    function laporan_po_kutus_kutus_count_filter($query){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->join('produk', 'produk.produk_id = po_kutus_kutus_detail.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->group_start();
        $this->db->like('po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $temp = str_replace(",", "", $query);
        $temp = str_replace(".", "", $temp);
        $this->db->or_like('po_kutus_kutus_detail.jumlah', $temp, 'BOTH');
        $this->db->or_like('po_kutus_kutus_detail.harga', $temp, 'BOTH');
        $this->db->or_like('po_kutus_kutus_detail.sub_total', $temp, 'BOTH');
        $this->db->group_end();
        if(isset($_GET['po_kutus_kutus_no']) && $this->input->get('po_kutus_kutus_no')!=""){
            $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $this->input->get('po_kutus_kutus_no'),'BOTH');
        }
        if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
            $this->db->where('po_kutus_kutus.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
            $this->db->where('po_kutus_kutus.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_kutus_kutus.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_kutus_kutus.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
            $this->db->where('po_kutus_kutus.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
            $this->db->where('produk.produk_id', $this->input->get('produk_id'));
        }
        if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
            if($this->input->get('tipe_pembayaran_id')!="kredit"){
                $this->db->where('po_kutus_kutus.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
            } else {
                $this->db->where('po_kutus_kutus.tipe_pembayaran is null', null, false);
            }

        }
        return $this->db->count_all_results('po_kutus_kutus');

    }
    function laporan_po_kutus_kutus_list($start,$length,$query){
        $this->db->select('po_kutus_kutus.jenis_pembayaran,po_kutus_kutus.po_kutus_kutus_id,po_kutus_kutus.po_kutus_kutus_no,jenis_produk.jenis_produk_nama,satuan.satuan_nama,po_kutus_kutus.tanggal_pemesanan,po_kutus_kutus.tanggal_penerimaan,suplier.suplier_nama,produk.produk_kode,produk.produk_nama,po_kutus_kutus_detail.jumlah,po_kutus_kutus_detail.harga,po_kutus_kutus_detail.sub_total,tipe_pembayaran.tipe_pembayaran_nama as "kas_nama" ,tipe_pembayaran.no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_kutus_kutus.suplier_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->join('produk', 'produk.produk_id = po_kutus_kutus_detail.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_kutus_kutus.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $temp = str_replace(",", "", $query);
        $temp = str_replace(".", "", $temp);
        $this->db->or_like('po_kutus_kutus_detail.jumlah', $temp, 'BOTH');
        $this->db->or_like('po_kutus_kutus_detail.harga', $temp, 'BOTH');
        $this->db->or_like('po_kutus_kutus_detail.sub_total', $temp, 'BOTH');
        $this->db->group_end();
        if(isset($_GET['po_kutus_kutus_no']) && $this->input->get('po_kutus_kutus_no')!=""){
            $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $this->input->get('po_kutus_kutus_no'),'BOTH');
        }
        if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
            $this->db->where('po_kutus_kutus.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
            $this->db->where('po_kutus_kutus.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_kutus_kutus.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_kutus_kutus.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
            $this->db->where('po_kutus_kutus.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
            $this->db->where('produk.produk_id', $this->input->get('produk_id'));
        }
        if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
            if($this->input->get('tipe_pembayaran_id')!="kredit"){
                $this->db->where('po_kutus_kutus.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
            } else {
                $this->db->where('po_kutus_kutus.tipe_pembayaran is null', null, false);
            }

        }
        $this->db->order_by('po_kutus_kutus.po_kutus_kutus_id', 'desc');
        return $this->db->get('po_kutus_kutus', $length, $start)->result();
    }
    function po_quality_list($start,$length,$query,$produk_id){
        $this->db->select('po_kutus_kutus.tanggal_pemesanan,po_kutus_kutus_detail.harga,po_kutus_kutus_detail.jumlah,po_kutus_kutus.tanggal_penerimaan,po_kutus_kutus.po_kutus_kutus_id,po_kutus_kutus.po_kutus_kutus_no,suplier.suplier_nama,po_kutus_kutus_detail.po_kutus_kutus_detail_id');
        $this->db->distinct();
        $this->db->join('suplier','po_kutus_kutus.suplier_id=suplier.suplier_id');
        $this->db->join('po_kutus_kutus_detail','po_kutus_kutus.po_kutus_kutus_id=po_kutus_kutus_detail.po_kutus_kutus_id');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_kutus_kutus_detail.po_kutus_kutus_id',$query);
        $this->db->or_like('po_kutus_kutus_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->get('po_kutus_kutus', $length, $start)->result();
    }
    function po_quality_filter($query,$produk_id){
        $this->db->select('po_kutus_kutus.*');
        $this->db->distinct();
        $this->db->join('suplier','po_kutus_kutus.suplier_id=suplier.suplier_id');
        $this->db->join('po_kutus_kutus_detail','po_kutus_kutus.po_kutus_kutus_id=po_kutus_kutus_detail.po_kutus_kutus_id');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_kutus_kutus_detail.po_kutus_kutus_id',$query);
        $this->db->or_like('po_kutus_kutus_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_quality_all($produk_id){
        $this->db->select('po_kutus_kutus.*');
        $this->db->distinct();
        $this->db->join('suplier','po_kutus_kutus.suplier_id=suplier.suplier_id');
        $this->db->join('po_kutus_kutus_detail','po_kutus_kutus.po_kutus_kutus_id=po_kutus_kutus_detail.po_kutus_kutus_id');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_retur_list($start,$length,$query){
        $this->db->select('po_kutus_kutus.tanggal_pemesanan,po_kutus_kutus.tanggal_penerimaan,po_kutus_kutus.po_kutus_kutus_id,po_kutus_kutus.po_kutus_kutus_no,suplier.suplier_nama');
        $this->db->join('suplier','po_kutus_kutus.suplier_id=suplier.suplier_id');
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_kutus_kutus_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->get('po_kutus_kutus', $length, $start)->result();
    }
    function po_retur_filter($query){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier','po_kutus_kutus.suplier_id=suplier.suplier_id');
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_kutus_kutus_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_retur_all(){
        $this->db->select('po_kutus_kutus.*');
        $this->db->join('suplier','po_kutus_kutus.suplier_id=suplier.suplier_id');
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        return $this->db->count_all_results('po_kutus_kutus');
    }
    function po_detail_by_po_location($po_kutus_kutus_id,$lokasi_id){
        $this->db->select('po_kutus_kutus_detail.*,produk.produk_nama,display_stock_produk_lokasi.jumlah as jumlah_stock');
        $this->db->join('display_stock_produk_lokasi','po_kutus_kutus_detail.produk_id=display_stock_produk_lokasi.produk_id');
        $this->db->join('produk','produk.produk_id=po_kutus_kutus_detail.produk_id');
        $this->db->where('po_kutus_kutus_detail.po_kutus_kutus_id',$po_kutus_kutus_id);
        $this->db->where('display_stock_produk_lokasi.stock_produk_lokasi_id',$lokasi_id);
        return $this->db->get('po_kutus_kutus_detail')->result();
    }
    function po_by_suplier($suplier_id){
        $this->db->where('suplier_id',$suplier_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        return $this->db->get('po_kutus_kutus')->result();
    }
}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */