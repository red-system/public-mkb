<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "testCrown";
    }
    function list(){
        return $this->db->get('table')->result();
    }
}

/* End of file Color.php */
/* Location: ./application/models/Color.php */