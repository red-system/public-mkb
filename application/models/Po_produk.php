<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "po_produk";
	}
	function insert($data)
    {
        $temp = parent::insert($data);
        return $temp;
    }

    function get_kode_po_produk(){
		$year = date("y");
		$month = date("m");
        $lokasi_kode = isset($_SESSION['redpos_login']['lokasi_id']) ? $_SESSION['redpos_login']['lokasi_id'] : "00";
		$prefix = "POP/".$month.$year."/".$lokasi_kode."/";
		$this->db->like('po_produk_no', $prefix, 'BOTH');
		$this->db->select('(max(urutan)+1) as kode');
		$this->db->from('po_produk');
		$result = $this->db->get()->row()->kode;
		if ($result != null){
			return $prefix.($result);
		} else {
			return $prefix."1";
		}		
	}
	function po_produk_count_all(){
        $this->db->select('po_produk.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_by('po_produk.po_produk_id');
		if($this->uri->segment(1)=="po-produk"){
			$this->db->where('status_penerimaan', "Belum Diterima");
		}

		return $this->db->count_all_results('po_produk');
	}
	function po_produk_count_filter($query){
		$this->db->select('po_produk.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="po-produk"){
			$this->db->where('status_penerimaan', "Belum Diterima");
		}
		if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
			$this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
			$this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
		}
		if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
			$this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
			$this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};		
		$this->db->group_by('po_produk.po_produk_id');
		return $this->db->count_all_results('po_produk');
	}
	function po_produk_list($start,$length,$query){
		$this->db->select('po_produk.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="po-produk"){
			$this->db->where('status_penerimaan', "Belum Diterima");
		}
		if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
			$this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
			$this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
		}
		if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
			$this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
			$this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};		
		$this->db->group_by('po_produk.po_produk_id');
		$this->db->order_by('po_produk.po_produk_id', 'desc');
		return $this->db->get('po_produk', $length, $start)->result();		
	}

    function po_produk_count_all_lunas(){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->where('status_pembayaran','Lunas');
        $this->db->group_by('po_produk.po_produk_id');
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }

        return $this->db->count_all_results('po_produk');
    }
    function po_produk_count_filter_lunas($query){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where('status_pembayaran','Lunas');
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_by('po_produk.po_produk_id');
        return $this->db->count_all_results('po_produk');
    }
    function po_produk_list_lunas($start,$length,$query){
        $this->db->select('po_produk.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where('status_pembayaran','Lunas');
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_by('po_produk.po_produk_id');
        $this->db->order_by('po_produk.po_produk_id', 'desc');
        return $this->db->get('po_produk', $length, $start)->result();
    }

    function acc_po_produk_count_all_lunas(){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('mykindofbeauty_master.login ', 'mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller', 'reseller.reseller_id = mykindofbeauty_master.login.reseller_id');
        
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where('reseller.type','super agen');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('po_produk.status_penerimaan','Diterima');
        $this->db->group_end();

        $this->db->or_group_start();
        $this->db->where('reseller.type','agent');
        $this->db->where('voucher_produk.status_penukaran','Terpakai');
        $this->db->group_end();
        $this->db->group_end();

        $this->db->group_by('po_produk.po_produk_id');
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }

        return $this->db->count_all_results('po_produk');
    }
    function acc_po_produk_count_filter_lunas($query){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('mykindofbeauty_master.login ', 'mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller', 'reseller.reseller_id = mykindofbeauty_master.login.reseller_id');
        
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where('reseller.type','super agen');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('po_produk.status_penerimaan','Diterima');
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_end();

        $this->db->or_group_start();
        $this->db->where('reseller.type','agent');
        $this->db->where('voucher_produk.status_penukaran','Terpakai');
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('voucher_produk.tanggal_penukaran >=', $this->input->get('start_penerimaan'));
            $this->db->where('voucher_produk.tanggal_penukaran <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_end();
        $this->db->group_end();

        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        
        $this->db->group_end();
        // $this->db->where('status_pembayaran','Lunas');
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        // if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
        //     $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
        //     $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        // };
        $this->db->group_by('po_produk.po_produk_id');
        return $this->db->count_all_results('po_produk');
    }
    function acc_po_produk_list_lunas($start,$length,$query){
        $this->db->select('po_produk.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('mykindofbeauty_master.login ', 'mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller', 'reseller.reseller_id = mykindofbeauty_master.login.reseller_id');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where('reseller.type','super agen');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('po_produk.status_penerimaan','Diterima');
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_end();

        $this->db->or_group_start();
        $this->db->where('reseller.type','agent');
        $this->db->where('voucher_produk.status_penukaran','Terpakai');
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('voucher_produk.tanggal_penukaran >=', $this->input->get('start_penerimaan'));
            $this->db->where('voucher_produk.tanggal_penukaran <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_end();
        $this->db->group_end();
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        // $this->db->where('status_pembayaran','Lunas');
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        
        $this->db->group_by('po_produk.po_produk_id');
        $this->db->order_by('po_produk.po_produk_id', 'desc');
        return $this->db->get('po_produk', $length, $start)->result();
    }

    function po_produk_count_all_admin(){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');

        $this->db->group_by('po_produk.po_produk_id');
        $this->db->where('status_penerimaan !=', "Diterima");
        $this->db->where('status_pembayaran !=', "Gagal");
        $this->db->where('status_pembayaran !=', "Hutang");
        return $this->db->count_all_results('po_produk');
    }

    function po_produk_count_filter_admin($query){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where('status_penerimaan !=', "Diterima");
        $this->db->where('status_pembayaran !=', "Gagal");
        $this->db->where('status_pembayaran !=', "Hutang");
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('estimasi_pengiriman >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('estimasi_pengiriman <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_by('po_produk.po_produk_id');
        return $this->db->count_all_results('po_produk');
    }
    function po_produk_list_admin($start,$length,$query){
        $this->db->select('po_produk.*,suplier.suplier_nama,lokasi_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('lokasi', 'po_produk.lokasi_reseller = lokasi.lokasi_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where('status_penerimaan !=', "Diterima");
        $this->db->where('status_pembayaran !=', "Gagal");
        $this->db->where('status_pembayaran !=', "Hutang");
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('estimasi_pengiriman >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('estimasi_pengiriman <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_by('po_produk.po_produk_id');
        $this->db->order_by('po_produk.po_produk_id', 'desc');
        return $this->db->get('po_produk', $length, $start)->result();
    }

	function po_produk_count_all_history(){
        $this->db->select('po_produk.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_by('po_produk.po_produk_id');
		$this->db->where_in('status_penerimaan', array("Diterima","Ditolak"));
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $this->db->where("lokasi_reseller",$_SESSION['redpos_login']['lokasi_id']);
        }
		return $this->db->count_all_results('po_produk');
	}
	function po_produk_count_filter_history($query){
		$this->db->select('po_produk.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
        $this->db->where_in('status_penerimaan', array("Diterima","Ditolak"));
		$this->db->group_by('po_produk.po_produk_id');
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $this->db->where("lokasi_reseller",$_SESSION['redpos_login']['lokasi_id']);
        }
		return $this->db->count_all_results('po_produk');
	}
	function po_produk_list_history($start,$length,$query){
		$this->db->select('po_produk.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as tipe_pembayaran_nama2,no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
		$this->db->group_start();
		$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_produk.grand_total', $total, 'BOTH');
		$this->db->group_end();
        $this->db->where_in('status_penerimaan', array("Diterima","Ditolak"));
		$this->db->group_by('po_produk.po_produk_id');
		$this->db->order_by('po_produk.po_produk_id', 'desc');
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $this->db->where("lokasi_reseller",$_SESSION['redpos_login']['lokasi_id']);
        }
		return $this->db->get('po_produk', $length, $start)->result();		
	}

    function po_produk_count_all_history_admin(){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->group_by('po_produk.po_produk_id');
        $this->db->where_in('status_penerimaan', array("Diterima","Ditolak"));

        return $this->db->count_all_results('po_produk');
    }
    function po_produk_count_filter_history_admin($query){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where_in('status_penerimaan', array("Diterima","Ditolak"));
        $this->db->group_by('po_produk.po_produk_id');

        return $this->db->count_all_results('po_produk');
    }
    function po_produk_list_history_admin($start,$length,$query){
        $this->db->select('po_produk.*,lokasi.lokasi_nama,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as tipe_pembayaran_nama2,no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('lokasi', 'po_produk.lokasi_penerimaan_id = lokasi.lokasi_id','left');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->where_in('status_penerimaan', array("Diterima","Ditolak"));
        $this->db->group_by('po_produk.po_produk_id');
        $this->db->order_by('po_produk.po_produk_id', 'desc');

        return $this->db->get('po_produk', $length, $start)->result();
    }

	function insert_po_produk(){
        $this->load->helper('security');
		$this->db->trans_begin();
		$po_no = $this->input->post('po_produk_no');
		$temp = explode("/", $po_no);
		$urutan = $temp[sizeof($temp)-1];
		$data['po_produk_no'] = $po_no;
		$data['urutan'] = $urutan;
		$temp = strtotime($this->input->post('tanggal_pemesanan'));
		$tanggal_pemesanan = date("Y-m-d",$temp);
		$data['tanggal_pemesanan'] = date("Y-m-d");

		$data['keterangan'] = $this->input->post('keterangan');
        $data['alamat_pengiriman'] = ($this->input->post('alamat_pengiriman',true));
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		$data['jenis_pembayaran'] = $jenis_pembayaran;
        $data['tipe_pembayaran_nama'] = "Payment Gateway";
        $data['tipe_pembayaran'] = null;
        $data['status_pembayaran'] = "Hutang";
		$data["total"] = $this->input->post('total_item');
		$data["grand_total"] = $this->input->post('grand_total');
		$data["potongan"] = $this->string_to_number($this->input->post('potongan'));
		$data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
		$data["bukti_pembayaran"] = $this->input->post('bukti_pembayaran');
        $voucher_code = time() . $_SESSION["redpos_login"]["reseller_id"] . random_string('numeric', 5);
        $data["voucher_code"] = $voucher_code;
		if($this->input->post('termin')!=""){
            $data["termin"] = $this->input->post('termin');
        }
		$data["status_penerimaan"] = "Belum Diterima";
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $data["lokasi_reseller"] = $_SESSION['redpos_login']['lokasi_id'];
        }
		$this->db->insert('po_produk', $data);
		$po_produk_id = $this->db->insert_id();
		$item = $this->input->post('item_produk');
		foreach ($item as $key) {
			$data = array();
            $voucher_detail = time() . $_SESSION["redpos_login"]["reseller_id"] . random_string('numeric', 5);
			$data["po_produk_id"] = $po_produk_id;
			$data["produk_id"] = $key["produk_id"];
            $data["last_hpp"] = $this->string_to_number($key["last_hpp"]);
			$data["harga"] = $this->string_to_number($key["harga"]);
            // $data["jumlah"] = $this->string_to_number($key["jumlah"]);
			$data["jumlah_qty"] = $this->string_to_number($key["jumlah"]);
			$data["sisa_qty"] = $this->string_to_number($key["jumlah"]);
			$data["sub_total"] = $this->string_to_number($key["subtotal"]);
			$data['voucher_detail'] = $voucher_detail;
			$this->db->insert('po_produk_detail', $data);
		}
		if($jenis_pembayaran != "kas"){
			$data = array();
			$data['po_produk_id'] = $po_produk_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
			$tenggat_pelunasan = date("Y-m-d",$temp);
			$data['tenggat_pelunasan'] = $tenggat_pelunasan;
			$this->db->insert('hutang', $data);
		}
		$status['success'] = FALSE;
		$status['po_produk_id'] = $po_produk_id;
		if ($this->db->trans_status() === FALSE)
            return $status;

		$this->db->trans_commit();
        $status['success'] = TRUE;
		return $status;
	}
	function edit_po_produk(){
		$this->db->trans_begin();
		$po_produk_id = $this->input->post('po_produk_id');
		$this->db->where('po_produk_id', $po_produk_id);
		$this->db->delete('po_produk_detail');
        $data['alamat_pengiriman'] = $this->input->post('alamat_pengiriman');
		$data['keterangan'] = $this->input->post('keterangan');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if($this->input->post('bukti_pembayaran')!=""){
            $data["bukti_pembayaran"] = $this->input->post('bukti_pembayaran');
        }
		$data['jenis_pembayaran'] = $jenis_pembayaran;
        $data['tipe_pembayaran_nama'] = "Payment Gateway";
        $data['tipe_pembayaran'] = null;
        $data['status_pembayaran'] = "Hutang";
		$data["total"] = $this->input->post('total_item');
		$data["grand_total"] = $this->input->post('grand_total');
		$data["potongan"] = $this->string_to_number($this->input->post('potongan'));
		$data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
		$data["status_penerimaan"] = "Belum Diterima";
		$this->db->where('po_produk_id', $po_produk_id);
		$this->db->update('po_produk', $data);
		$item = $this->input->post('item_produk');
		foreach ($item as $key) {

			$data = array();
			$data["po_produk_id"] = $po_produk_id;
			$data["produk_id"] = $key["produk_id"];
            $data["last_hpp"] = $this->string_to_number($key["last_hpp"]);
			$data["harga"] = $this->string_to_number($key["harga"]);
            $data["jumlah"] = $this->string_to_number($key["jumlah"]);
			$data["jumlah_qty"] = $this->string_to_number($key["jumlah"]);
			$data["sisa_qty"] = $this->string_to_number($key["jumlah"]);
			$data["sub_total"] = $this->string_to_number($key["subtotal"]);
			$this->db->insert('po_produk_detail', $data);
		}
		$data = array();
		$this->db->where('po_produk_id', $po_produk_id);
		$hutang = $this->db->get('hutang')->row();
		if($jenis_pembayaran != "kas"){
			if($hutang != null){
				$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
				$tenggat_pelunasan = date("Y-m-d",$temp);
				$data['tenggat_pelunasan'] = $tenggat_pelunasan;
				$this->db->where('hutang_id', $hutang->hutang_id);
				$this->db->update('hutang', $data);
			} else {
				$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
				$tenggat_pelunasan = date("Y-m-d",$temp);
				$data['po_produk_id'] = $po_produk_id;
				$data['tenggat_pelunasan'] = $tenggat_pelunasan;
				$this->db->insert('hutang', $data);			
			}			
		} else {
			$this->db->where('po_produk_id', $po_produk_id);
			$this->db->delete('hutang');
		}

		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;			
	}
	function po_produk_detail_by_id($po_produk_id){
		$this->db->where('po_produk_detail.po_produk_id', $po_produk_id);
		$this->db->select('po_produk_detail.*,produk.produk_nama,suplier.suplier_nama,po_produk.grand_total as po_total,');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
		$this->db->join('po_produk', 'po_produk.po_produk_id = po_produk_detail.po_produk_id');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		return $this->db->get('po_produk_detail')->result();
	}
    function po_produk_detail_by_detail_id($po_produk_detail_id){
        $this->db->where('po_produk_detail.po_produk_detail_id', $po_produk_detail_id);
        $this->db->select('po_produk_detail.*,produk.produk_nama,suplier.suplier_nama,po_produk.grand_total as po_total,');
        $this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
        $this->db->join('po_produk', 'po_produk.po_produk_id = po_produk_detail.po_produk_id');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        return $this->db->get('po_produk_detail')->result();
    }
    function po_produk_detail_by_code($code){
        $this->db->where('voucher_produk.voucher_code', $code);
        $this->db->select('po_produk_detail.*,produk.produk_nama,suplier.suplier_nama,po_produk.grand_total as po_total,');
        $this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
        $this->db->join('po_produk', 'po_produk.po_produk_id = po_produk_detail.po_produk_id');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_detail_id = po_produk_detail.po_produk_detail_id');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        return $this->db->get('po_produk_detail')->result();
    }
	function po_produk_by_id($po_produk_id){
		$this->db->where('po_produk.po_produk_id', $po_produk_id);
		$this->db->select('po_produk.*,suplier.suplier_nama,hutang.tenggat_pelunasan');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
		$this->db->join('hutang', 'hutang.po_produk_id = po_produk.po_produk_id', 'left');
		return $this->db->get('po_produk')->row();
	}
	function laporan_po_produk_count_all(){
        $this->db->select('po_produk.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		return $this->db->count_all_results('po_produk');

	}
	function laporan_po_produk_count_filter($query){
        $this->db->select('po_produk.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->group_start();
			$this->db->like('po_produk_no', $query, 'BOTH');
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $temp);
			$this->db->or_like('po_produk_detail.jumlah', $temp, 'BOTH');
			$this->db->or_like('po_produk_detail.harga', $temp, 'BOTH');
			$this->db->or_like('po_produk_detail.sub_total', $temp, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['po_produk_no']) && $this->input->get('po_produk_no')!=""){
			$this->db->like('po_produk.po_produk_no', $this->input->get('po_produk_no'),'BOTH');
		}
		if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};	
		if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
			$this->db->where('po_produk.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
			if($this->input->get('tipe_pembayaran_id')!="kredit"){
				$this->db->where('po_produk.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
			} else {
				$this->db->where('po_produk.tipe_pembayaran is null', null, false);
			}
			
		}	
		return $this->db->count_all_results('po_produk');

	}
	function laporan_po_produk_list($start,$length,$query){
		$this->db->select('po_produk.jenis_pembayaran,po_produk.po_produk_id,po_produk.po_produk_no,jenis_produk.jenis_produk_nama,satuan.satuan_nama,po_produk.tanggal_pemesanan,po_produk.tanggal_penerimaan,suplier.suplier_nama,produk.produk_kode,produk.produk_nama,po_produk_detail.jumlah,po_produk_detail.harga,po_produk_detail.sub_total,tipe_pembayaran.tipe_pembayaran_nama as "kas_nama" ,tipe_pembayaran.no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id');
		$this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
		$this->db->join('produk', 'produk.produk_id = po_produk_detail.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
		$this->db->group_start();
			$this->db->like('po_produk_no', $query, 'BOTH');
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $temp);
			$this->db->or_like('po_produk_detail.jumlah', $temp, 'BOTH');
			$this->db->or_like('po_produk_detail.harga', $temp, 'BOTH');
			$this->db->or_like('po_produk_detail.sub_total', $temp, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['po_produk_no']) && $this->input->get('po_produk_no')!=""){
			$this->db->like('po_produk.po_produk_no', $this->input->get('po_produk_no'),'BOTH');
		}
		if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
			$this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
			$this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};	
		if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
			$this->db->where('po_produk.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['produk_id'])&& $this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
			if($this->input->get('tipe_pembayaran_id')!="kredit"){
				$this->db->where('po_produk.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
			} else {
				$this->db->where('po_produk.tipe_pembayaran is null', null, false);
			}
			
		}	
		$this->db->order_by('po_produk.po_produk_id', 'desc');
		return $this->db->get('po_produk', $length, $start)->result();
	}
	function po_quality_list($start,$length,$query,$produk_id){
	    $this->db->select('po_produk.tanggal_pemesanan,po_produk_detail.harga,po_produk_detail.jumlah,po_produk.tanggal_penerimaan,po_produk.po_produk_id,po_produk.po_produk_no,suplier.suplier_nama,po_produk_detail.po_produk_detail_id');
        $this->db->distinct();
	    $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->join('po_produk_detail','po_produk.po_produk_id=po_produk_detail.po_produk_id');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
            $this->db->or_like('tanggal_pemesanan',$query);
            $this->db->or_like('tanggal_penerimaan',$query);
            $this->db->or_like('po_produk_detail.po_produk_id',$query);
            $this->db->or_like('po_produk_no',$query);
            $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
	    return $this->db->get('po_produk', $length, $start)->result();
    }
    function po_quality_filter($query,$produk_id){
        $this->db->select('po_produk.*');
        $this->db->distinct();
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->join('po_produk_detail','po_produk.po_produk_id=po_produk_detail.po_produk_id');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_produk_detail.po_produk_id',$query);
        $this->db->or_like('po_produk_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('po_produk');
    }
    function po_quality_all($produk_id){
        $this->db->select('po_produk.*');
        $this->db->distinct();
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->join('po_produk_detail','po_produk.po_produk_id=po_produk_detail.po_produk_id');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        return $this->db->count_all_results('po_produk');
    }
    function po_retur_list($start,$length,$query){
        $this->db->select('po_produk.tanggal_pemesanan,po_produk.tanggal_penerimaan,po_produk.po_produk_id,po_produk.po_produk_no,suplier.suplier_nama');
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_produk_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->get('po_produk', $length, $start)->result();
    }
    function po_retur_filter($query){
        $this->db->select('po_produk.*');
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        $this->db->group_start();
        $this->db->or_like('tanggal_pemesanan',$query);
        $this->db->or_like('tanggal_penerimaan',$query);
        $this->db->or_like('po_produk_no',$query);
        $this->db->or_like('suplier_nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('po_produk');
    }
    function po_retur_all(){
        $this->db->select('po_produk.*');
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        return $this->db->count_all_results('po_produk');
    }
    function po_detail_by_po_location($po_produk_id,$lokasi_id){
	    $this->db->select('po_produk_detail.*,produk.produk_nama,display_stock_produk_lokasi.jumlah as jumlah_stock');
	    $this->db->join('display_stock_produk_lokasi','po_produk_detail.produk_id=display_stock_produk_lokasi.produk_id');
        $this->db->join('produk','produk.produk_id=po_produk_detail.produk_id');
        $this->db->where('po_produk_detail.po_produk_id',$po_produk_id);
        $this->db->where('display_stock_produk_lokasi.stock_produk_lokasi_id',$lokasi_id);
        return $this->db->get('po_produk_detail')->result();
    }
    function po_by_suplier($suplier_id){
        $this->db->where('suplier_id',$suplier_id);
        $this->db->where('status_pembayaran','Hutang');
        $this->db->where('status_penerimaan','Diterima');
        return $this->db->get('po_produk')->result();
    }

    function po_produk_count_all_sa(){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->group_by('po_produk.po_produk_id');
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $this->db->where("lokasi_reseller",$_SESSION['redpos_login']['lokasi_id']);
        }
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }

        return $this->db->count_all_results('po_produk');
    }
    function po_produk_count_filter_sa($query){
        $this->db->select('po_produk.*');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $this->db->where("lokasi_reseller",$_SESSION['redpos_login']['lokasi_id']);
        }
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_by('po_produk.po_produk_id');
        return $this->db->count_all_results('po_produk');
    }
    function po_produk_list_sa($start,$length,$query){
        $this->db->select('po_produk.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('po_produk_detail', 'po_produk_detail.po_produk_id = po_produk.po_produk_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_produk.tipe_pembayaran','left');
        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_produk.jenis_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.tipe_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_pembayaran', $query, 'BOTH');
        $this->db->or_like('po_produk.status_penerimaan', $query, 'BOTH');
        $this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();
        if(isset($_SESSION['redpos_login']['lokasi_id'])){
            $this->db->where("lokasi_reseller",$_SESSION['redpos_login']['lokasi_id']);
        }
        if($this->uri->segment(1)=="po-produk"){
            $this->db->where('status_penerimaan', "Belum Diterima");
        }
        if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
            $this->db->where('po_produk.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
        }
        if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
            $this->db->where('po_produk.status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
            $this->db->where('po_produk.status_penerimaan', $this->input->get('status_penerimaan'));
        }
        if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
            $this->db->where('po_produk.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
            $this->db->where('po_produk.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
        }
        if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
        };
        $this->db->group_by('po_produk.po_produk_id');
        $this->db->order_by('po_produk.po_produk_id', 'desc');
        return $this->db->get('po_produk', $length, $start)->result();
    }
    function minimal_po($reseller_id){
        $agen_first = 2750000;
        $agen_next = 550000;
        $super_first =  27500000;
        $super_next =  5500000;

//        $agen_first = 10000;
//        $agen_next = 10000;
//        $super_first =  10000;
//        $super_next =  10000;
        $reseller = $this->db->where('reseller_id',$reseller_id)->get('reseller')->row();
        $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
        $this->db->select('sum(grand_total) as total');
        $this->db->where('lokasi_reseller',$lokasi_id);
        $this->db->where('status_pembayaran','Lunas');
        $total = $this->db->get('po_produk')->row();
        $total = $total == null ? 0 : $total->total;
        if($total<$super_first){
            $super_next =  $super_first-$total;
        }
        if($total<$agen_first){
            $agen_next =  $agen_first-$total;
        }
        if($reseller->type == 'agent'){
            return ($reseller->first_deposit == 0 ? $agen_first : $agen_next);
        }else{
            return ($reseller->first_deposit == 0 ? $super_first : $super_next);
        }
    }
    function minimal_po_cek($reseller_id){
        $agen_first = 2750000;
        $super_first =  27500000;
//
        $reseller = $this->db->where('reseller_id',$reseller_id)->get('reseller')->row();
        if($reseller->type == 'agent'){
            return $agen_first;
        }else{
            return $super_first;
        }
    }
    function accept(){
        $po_produk_id = $this->input->post("id");
        $this->load->model("reseller","",true);
        $this->load->model("lokasi","",true);
        $this->load->model("produk","",true);
        $this->load->model("stock_produk","",true);
        $this->load->model("bonus","",true);
        $this->load->model("voucher_produk","",true);
        $this->load->library("main");
        $po_data = $this->row_by_id($po_produk_id);
        $login = $this->db->where("lokasi_id",$po_data->lokasi_reseller)->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        $referal_id = $reseller->referal_id;
        $reseller_penerima = $this->reseller->row_by_id($referal_id);
        $resellerData["first_deposit"] = 1;
        $resellerData["status"] = "active";
        $this->reseller->update_by_id("reseller_id",$reseller_id,$resellerData);
        if($reseller->type=="agent"){
            $po["status_penerimaan"] = "Diterima";
            $po["tanggal_penerimaan"] = date("Y-m-d");
            $po['lokasi_penerimaan_id'] = $po_data->lokasi_reseller;
            $this->update_by_id("po_produk_id",$po_produk_id,$po);
        }
        $po_produk_item = $this->po_produk_detail_by_id($po_produk_id);
        $this->db->select("mykindofbeauty_kemiri.staff.*");
        $this->db->join("mykindofbeauty_kemiri.staff","mykindofbeauty_master.login.user_staff_id = mykindofbeauty_kemiri.staff.staff_id");
        $this->db->where("mykindofbeauty_master.login.company_id",3);
        $this->db->where("mykindofbeauty_master.login.user_role_id",2);
        $login = $this->db->get("mykindofbeauty_master.login")->result();
        $bank = $this->db->where("bank_id",$reseller_penerima->bank_id)->get('mykindofbeauty_kemiri.bank')->row();
        $reseller_bank = $this->db->where("bank_id",$reseller->bank_id)->get('mykindofbeauty_kemiri.bank')->row();

        if ( $reseller->type=="super agen"){
            foreach ($po_produk_item as $key) {
                $data = array();
                $data["stock_produk_qty"] = $key->jumlah;
                $data["produk_id"] = $key->produk_id;
                $data["stock_produk_lokasi_id"] =$po_data->lokasi_reseller;
                $data["year"] = date("y");
                $data["month"] = date("m");
                $data["po_id"] = $po_produk_id;
                $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                $data["hpp"] = $this->string_to_number($key->harga);
                $insert = $this->stock_produk->insert($data);
            }
            $bonus = array();
            $bonus["tanggal"] = date("Y-m-d");
            $bonus["from_reseller_id"] = $reseller_id;
            $bonus["type"] = "loyalty bonus";
            $bonus["jumlah_deposit"] = $po_produk_item[0]->po_total;
            $bonus["persentase"] = 5;
            $bonus["jumlah_bonus"] =  $po_produk_item[0]->po_total*0.05;
            $bonus["to_reseller_id"] = $referal_id;
            $this->bonus->insert($bonus);

            foreach ($login as $item){
                $mailContentAdmin ="<H3>Pemberitahuan Bonus Reseller</H3> <br>";
                $mailContentAdmin .="<table><thead><tr></tr><tr></tr></thead>";
                $mailContentAdmin .="<tbody>";
                $mailContentAdmin .="<tr><td>Penerima Bonus</td><td>:</td><td>".$reseller->nama."</td></tr>";
                $mailContentAdmin .="<tr><td>Tipe Bonus</td><td>:</td><td>Loyalty</td></tr>";
                $mailContentAdmin .="<tr><td>Jumlah Bonus</td><td>:</td><td>".number_format($po_produk_item[0]->po_total*0.05)."</td></tr>";
                $mailContentAdmin .="<tr><td>Nama Bank</td><td>:</td><td>".$reseller_bank->nama_bank."</td></tr>";
                $mailContentAdmin .="<tr><td>No Akun</td><td>:</td><td>".$reseller->bank_rekening."</td></tr>";
                $mailContentAdmin .="<tr><td>Atas Nama Akun</td><td>:</td><td>".$reseller->bank_atas_nama."</td></tr>";
                $mailContentAdmin .="<br>";
                $mailContentAdmin .="<br>";
                $mailContentAdmin .="Bonus harus di transfer ke rekening reseller dalam estimasi 1 x 24 jam";

                $this->main->mailer_auth('Bonus MKB', $item->staff_email,$item->staff_nama, $mailContentAdmin);
            }
            $mailContentAdmin ="<H3>Selamat Anda Mendapatkan bonus Loyalty</H3> <br>";
            $mailContentAdmin .="<table><thead><tr></tr><tr></tr></thead>";
            $mailContentAdmin .="<tbody>";
            $mailContentAdmin .="<tr><td>Jumlah Deposit</td><td>:</td><td>".number_format($po_produk_item[0]->po_total)."</td></tr>";
            $mailContentAdmin .="<tr><td>Persentase</td><td>:</td><td>5%</td></tr>";
            $mailContentAdmin .="<tr><td>Jumlah Bonus</td><td>:</td><td>".number_format($po_produk_item[0]->po_total*0.05)."</td></tr>";
            $mailContentAdmin .="<br>";
            $mailContentAdmin .="<br>";
            $mailContentAdmin .="Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam";
            $this->main->mailer_auth('Bonus Loyalty', $reseller->email,$reseller->nama, $mailContentAdmin);

        } else{
            $voucher = array();

            $voucher["reseller_pemilik"] = $reseller_id;
            $voucher_code = time().$reseller_id.random_string('numeric', 5);
            $voucher["voucher_code"] = $voucher_code;
            $voucher["po_produk_id"] = $po_produk_id;
            $this->voucher_produk->insert($voucher);
        }
        if($referal_id!=null){
            $bonus = array();
            $bonus["tanggal"] = date("Y-m-d");
            $bonus["from_reseller_id"] = $reseller_id;
            $bonus["type"] = "referal bonus";
            $bonus["jumlah_deposit"] = $po_produk_item[0]->po_total;
            $bonus["persentase"] = 10;
            $bonus["jumlah_bonus"] =  $po_produk_item[0]->po_total*0.1;
            $bonus["to_reseller_id"] = $referal_id;
            $this->bonus->insert($bonus);

            foreach ($login as $item){
                $mailContentAdmin ="<H3>Pemberitahuan Bonus Reseller</H3> <br>";
                $mailContentAdmin .="<table><thead><tr></tr><tr></tr></thead>";
                $mailContentAdmin .="<tbody>";
                $mailContentAdmin .="<tr><td>Penerima Bonus</td><td>:</td><td>".$reseller_penerima->nama."</td></tr>";
                $mailContentAdmin .="<tr><td>Tipe Bonus</td><td>:</td><td>Referal</td></tr>";
                $mailContentAdmin .="<tr><td>Dari</td><td>:</td><td>".$reseller->nama."</td></tr>";
                $mailContentAdmin .="<tr><td>Jumlah Bonus</td><td>:</td><td>".number_format($po_produk_item[0]->po_total*0.1)."</td></tr>";
                $mailContentAdmin .="<tr><td>Nama Bank</td><td>:</td><td>".$bank->nama_bank."</td></tr>";
                $mailContentAdmin .="<tr><td>No Akun</td><td>:</td><td>".$reseller_penerima->bank_rekening."</td></tr>";
                $mailContentAdmin .="<tr><td>Atas Nama Akun</td><td>:</td><td>".$reseller_penerima->bank_atas_nama."</td></tr>";
                $mailContentAdmin .="<br>";
                $mailContentAdmin .="<br>";
                $mailContentAdmin .="Bonus harus di transfer ke rekening reseller dalam estimasi 1 x 24 jam";
                $this->main->mailer_auth('Bonus MKB', $item->staff_email,$item->staff_nama, $mailContentAdmin);
            }
            $mailContentAdmin ="<H3>Selamat Anda Mendapatkan bonus referal</H3> <br>";
            $mailContentAdmin .="<table><thead><tr></tr><tr></tr></thead>";
            $mailContentAdmin .="<tbody>";
            $mailContentAdmin .="<tr><td>Reseller yang di referensikan</td><td>:</td><td>".$reseller->nama."</td></tr>";
            $mailContentAdmin .="<tr><td>Jumlah Deposit</td><td>:</td><td>".number_format($po_produk_item[0]->po_total)."</td></tr>";
            $mailContentAdmin .="<tr><td>Persentase</td><td>:</td><td>10%</td></tr>";
            $mailContentAdmin .="<tr><td>Jumlah Bonus</td><td>:</td><td>".number_format($po_produk_item[0]->po_total*0.1)."</td></tr>";
            $mailContentAdmin .="<br>";
            $mailContentAdmin .="<br>";
            $mailContentAdmin .="Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam";
            $this->main->mailer_auth('Bonus Referal', $reseller_penerima->email,$reseller_penerima->nama, $mailContentAdmin);
        }
    }
    function total_deposit_reseller($lokasi_reseller){
        $this->db->select('sum(po_produk.grand_total) as total');
        $this->db->where('status_pembayaran',"Lunas");
        $this->db->where('lokasi_reseller',$lokasi_reseller);
        $row = $this->db->get('po_produk')->row();
        if($row==null){
            return 0;
        }else{
            return $row->total;
        }
    }

    function total_po(){
	    $this->db->select('sum(grand_total) as grand_total');
	    $this->db->where('status_pembayaran','Lunas');
	    if($this->input->get("start_date")!=""){
	        $start_date = $this->input->get("start_date")." 00:00:00";
            $this->db->where('tanggal_uang_diterima >=',$start_date);
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date")." 23:59:59";
            $this->db->where('tanggal_uang_diterima <=',$end_date);
        }
	    return $this->db->get('po_produk')->row();
    }
    function total_mkb(){
        $this->db->select('sum(po_produk_detail.jumlah_qty) as total');
        $this->db->join('po_produk_detail','po_produk.po_produk_id = po_produk_detail.po_produk_id');
        $this->db->where('status_pembayaran','Lunas');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date")." 00:00:00";
            $this->db->where('tanggal_uang_diterima >=',$start_date);
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date")." 23:59:59";
            $this->db->where('tanggal_uang_diterima <=',$end_date);
        }
        $this->db->where('po_produk_detail.produk_id',23117);
        return $this->db->get('po_produk')->row();
    }
    function total_ls(){
        $this->db->select('sum(po_produk_detail.jumlah_qty) as total');
        $this->db->join('po_produk_detail','po_produk.po_produk_id = po_produk_detail.po_produk_id');
        $this->db->where('status_pembayaran','Lunas');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date")." 00:00:00";
            $this->db->where('tanggal_uang_diterima >=',$start_date);
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date")." 23:59:59";
            $this->db->where('tanggal_uang_diterima <=',$end_date);
        }
        $this->db->where('po_produk_detail.produk_id',23118);
        return $this->db->get('po_produk')->row();
    }
    function cek_jumlah_po_lunas(){
	    $this->db->select('count(po_produk_id) as jumlah');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('po_produk.status_penerimaan','Belum Diterima');
	    return $this->db->get('po_produk')->row()->jumlah;
    }


    function rekap_bulanan($reseller_id,$tanggal,$lokasi_id,$act){
	    $sql = 'call update_rekap('.$reseller_id.',"'.$tanggal.'",'.$lokasi_id.','.$act.')';
	    $this->db->query($sql);
    }

    function omset_referensi($reseller_id){
        $this->db->select('sum(grand_total) as grand_total');
	    $this->db->join('mykindofbeauty_master.login','mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','mykindofbeauty_master.login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('reseller.referal_id',$reseller_id);
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
            $this->db->where('tanggal_pemesanan >=',$start_date);
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $this->db->where('tanggal_pemesanan <=',$end_date);
        }
        $record = $this->db->get('po_produk')->row();
        if($record==null){
            return 0;
        }else {
            return $record->grand_total;
        }
    }



    // accounting
    function pendapatan_deposit_all(){
        $this->db->select('pendapatan_deposit.*');
        $this->db->order_by('po_produk_id', 'desc');
        return $this->db->count_all_results('pendapatan_deposit');
    }
    function pendapatan_deposit_filter($query){
        $this->db->select('pendapatan_deposit.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_pengiriman >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_pengiriman <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('po_produk_no', $query, 'BOTH');
        $this->db->or_like('tanggal_order ', $query, 'BOTH');
        $this->db->or_like('tanggal_pengiriman', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('po_produk_id', 'desc');
        return $this->db->count_all_results('pendapatan_deposit');
    }
    function pendapatan_deposit_list($start,$length,$query){
        $this->db->select('pendapatan_deposit.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_pengiriman >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_pengiriman <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('po_produk_no', $query, 'BOTH');
        $this->db->or_like('tanggal_order ', $query, 'BOTH');
        $this->db->or_like('tanggal_pengiriman', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('po_produk_id', 'desc');
        return $this->db->get('pendapatan_deposit', $length, $start)->result();
    }

    function accounting_ferby_all(){
        $this->db->select('po_produk.tanggal_pemesanan, reseller.nama, po_produk.grand_total, po_produk.tanggal_penerimaan');
        $this->db->join('mykindofbeauty_master.login', 'mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller', 'mykindofbeauty_master.login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('reseller.referal_id','95');
        $this->db->order_by('po_produk.tanggal_pemesanan', 'desc');
        return $this->db->count_all_results('po_produk');
    }
    function accounting_ferby_filter($query){

        $this->db->select('po_produk.tanggal_pemesanan, reseller.nama, po_produk.grand_total, po_produk.tanggal_penerimaan');
        $this->db->join('mykindofbeauty_master.login', 'mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller', 'mykindofbeauty_master.login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('reseller.referal_id','95');
        

        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_tanggal'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.tanggal_pemesanan ', $query, 'BOTH');
        $this->db->or_like('po_produk.tanggal_penerimaan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();

        $this->db->order_by('po_produk.tanggal_pemesanan', 'desc');

        return $this->db->count_all_results('po_produk');
    }
    function accounting_ferby_list($start,$length,$query){

        $this->db->select('po_produk.tanggal_pemesanan, reseller.nama, po_produk.grand_total, po_produk.tanggal_penerimaan');
        $this->db->join('mykindofbeauty_master.login', 'mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller', 'mykindofbeauty_master.login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('reseller.referal_id','95');
        

        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('po_produk.tanggal_penerimaan >=', $this->input->get('start_tanggal'));
            $this->db->where('po_produk.tanggal_penerimaan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.tanggal_pemesanan ', $query, 'BOTH');
        $this->db->or_like('po_produk.tanggal_penerimaan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('po_produk.grand_total', $total, 'BOTH');
        $this->db->group_end();

        $this->db->order_by('po_produk.tanggal_pemesanan', 'desc');
        
        return $this->db->get('po_produk', $length, $start)->result();
    }


    function produk_keluar_by_deposit_all(){
        $this->db->select('produk_keluar_by_deposit.*');
        return $this->db->count_all_results('produk_keluar_by_deposit');
    }
    function produk_keluar_by_deposit_filter($query){

        $this->db->select('produk_keluar_by_deposit.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_penerimaan >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_penerimaan <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('tanggal_penerimaan', $query, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('tanggal_penerimaan', 'desc');

        return $this->db->count_all_results('produk_keluar_by_deposit');
    }
    function produk_keluar_by_deposit_list($start,$length,$query){

        $this->db->select('produk_keluar_by_deposit.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_penerimaan >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_penerimaan <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('tanggal_penerimaan', $query, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('tanggal_penerimaan', 'desc');
        return $this->db->get('produk_keluar_by_deposit', $length, $start)->result();
    }

    function penjualanByMonth($search,$reseller_id){
        $this->db->select('sum(po_produk.grand_total) as sales, concat_ws(" ",MONTH(po_produk.tanggal_uang_diterima),YEAR(po_produk.tanggal_uang_diterima)) as "label"');
        $this->db->from('po_produk');
        $this->db->group_by('MONTH(po_produk.tanggal_uang_diterima)');
        $this->db->group_by('YEAR(po_produk.tanggal_uang_diterima)');
        $this->db->order_by('po_produk.tanggal_uang_diterima');
        $this->db->where('po_produk.tanggal_uang_diterima is not null','',false);
        return $this->db->get('')->result();
    }

    function uang_masuk_per_propinsi_list($start,$length,$query){
	    $this->db->select('province.province,sum(po_produk.grand_total) as total');
	    $this->db->join('mykindofbeauty_master.login','mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
	    $this->db->join('reseller','reseller.reseller_id = mykindofbeauty_master.login.reseller_id');
	    $this->db->join('province','reseller.outlet_province_id = province.province_id');
	    $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->group_by('reseller.outlet_province_id');
        $this->db->order_by('total','desc');
        $start_date = date("Y-m-01 00:00:00");
        $end_date = date("Y-m-t 23:59:59");
        if($this->input->get('start_date')!=''){
            $start_date = $this->input->get('start_date').' 00:00:00';
        }
        if($this->input->get('end_date')!=''){
            $end_date = $this->input->get('end_date').' 23:59:59';
        }
        $this->db->where('po_produk.tanggal_uang_diterima >=',$start_date);
        $this->db->where('po_produk.tanggal_uang_diterima <=',$end_date);
        return $this->db->get('po_produk',$length,$start)->result();
    }
    function uang_masuk_per_propinsi_filter($query){
        $this->db->select('province.province,sum(po_produk.grand_total) as total');
        $this->db->join('mykindofbeauty_master.login','mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','reseller.reseller_id = mykindofbeauty_master.login.reseller_id');
        $this->db->join('province','reseller.outlet_province_id = province.province_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->group_by('reseller.outlet_province_id');
        $this->db->order_by('total','desc');
        $start_date = date("Y-m-01 00:00:00");
        $end_date = date("Y-m-t 23:59:59");
        if($this->input->get('start_date')!=''){
            $start_date = $this->input->get('start_date').' 00:00:00';
        }
        if($this->input->get('end_date')!=''){
            $end_date = $this->input->get('end_date').' 23:59:59';
        }
        $this->db->where('po_produk.tanggal_uang_diterima >=',$start_date);
        $this->db->where('po_produk.tanggal_uang_diterima <=',$end_date);
        return $this->db->count_all_results('po_produk');
    }
    function uang_masuk_per_propinsi_all(){
        $this->db->select('province.province_id,sum(po_produk.grand_total) as total');
        $this->db->join('mykindofbeauty_master.login','mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','reseller.reseller_id = mykindofbeauty_master.login.reseller_id');
        $this->db->join('province','reseller.outlet_province_id = province.province_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->group_by('reseller.outlet_province_id');
        $this->db->order_by('total','desc');
        $start_date = date("Y-m-01 00:00:00");
        $end_date = date("Y-m-t 23:59:59");
        if($this->input->get('start_date')!=''){
            $start_date = $this->input->get('start_date').' 00:00:00';
        }
        if($this->input->get('end_date')!=''){
            $end_date = $this->input->get('end_date').' 23:59:59';
        }
        $this->db->where('po_produk.tanggal_uang_diterima >=',$start_date);
        $this->db->where('po_produk.tanggal_uang_diterima <=',$end_date);
        return $this->db->count_all_results('po_produk');
    }

    function penjualan_perbulan($lokasi_id,$reseller_id){
        $start_tanggal = date('Y-m-01');
        $end_tanggal = date('Y-m-t');
        $this->db->select('if(sum(po_produk.grand_total) is null,0,sum(po_produk.grand_total)) as qty');
        $this->db->where('po_produk.lokasi_reseller',$lokasi_id);
        $this->db->where('po_produk.tanggal_penerimaan >=',$start_tanggal);
        $this->db->where('po_produk.tanggal_penerimaan <=',$end_tanggal);
        $penjualan = $this->db->get('po_produk')->row()->qty;

        $this->db->select('if(sum(terpakai) is null,0,sum(terpakai)) as qty');
        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('status !=','refuse');
        $this->db->where('tanggal >=',$start_tanggal);
        $this->db->where('tanggal <=',$end_tanggal);
        $terpakai = $this->db->get('reward_reseller')->row()->qty;

        $result = $penjualan - $terpakai;
        return $result;
    }
    function omset_bulanan_reseller($lokasi_id){
	    $this->db->select('if(sum(po_produk.grand_total) is null,0,sum(po_produk.grand_total)) as omset');
	    $this->db->where('lokasi_reseller',$lokasi_id);
        $this->db->where('tanggal_penerimaan >=',date("Y-m-01"));
        $this->db->where('tanggal_penerimaan <=',date("Y-m-t"));
        return $this->db->get('po_produk')->row()->omset;
    }

    function get_last_deposit_po($id_reseller){
        // $this->db->join('mykindofbeauty_master.login','po_produk.lokasi_reseller = mykindofbeauty_master.login.lokasi_id');
        // $this->db->join('reseller','mykindofbeauty_master.login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.om_leader', $id_reseller);
        $this->db->where('po_produk.tanggal_penerimaan is NOT NULL', NULL, FALSE);
        $this->db->order_by('po_produk.tanggal_penerimaan','desc');
        $this->db->limit(1);
        return $this->db->get('po_produk')->row();
    }

    function update_omset_hangus($id_reseller, $before_date, $next_date, $data){
        $this->db->where('tanggal_penerimaan >=', $before_date);
        $this->db->where('tanggal_penerimaan <=', $next_date);
        $this->db->where('om_leader', $id_reseller);
        return $this->db->update('po_produk', $data);
    }
    function get_last_deposit_reseller($reseller_id){
	    $this->db->select('max(tanggal_uang_diterima) as tanggal');
	    $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('reseller.reseller_id',$reseller_id);
	    $result =  $this->db->get('po_produk')->row()->tanggal;

	    if($result == null){
	        return '2021-06-01';
        }else{
	        return $result;
        }
    }
    function omset_dihanguskan($reseller_id){

    }
    function is_deposit_in_april($lokasi_id){
	    $this->db->where('lokasi_reseller',$lokasi_id);
	    $this->db->where('tanggal_uang_diterima >','2022-03-30');
        $this->db->where('tanggal_uang_diterima <','2022-05-01');
        $result = $this->db->get('po_produk')->row();
        if($result==null){
            return false;
        } else {
            return true;
        }
    }

    function get_reseller_last_po($reseller_id){
	    $this->db->select("po_produk.*,max(po_produk.tanggal_uang_diterima) as tanggal");
	    $this->db->join("mykindofbeauty_master.login as login","login.lokasi_id = po_produk.lokasi_reseller");
	    $this->db->join("reseller","login.reseller_id = reseller.reseller_id");
	    $this->db->where("reseller.reseller_id","$reseller_id");
	    $this->db->where("po_produk.tanggal_uang_diterima is not null",null,false);
	    return $this->db->get("po_produk")->row();
    }
    function total_omset_reseller_date_range($reseller_id,$date_start,$date_limit){
	    $this->db->select("sum(po_produk.grand_total) as total");
        $this->db->join("mykindofbeauty_master.login as login","login.lokasi_id = po_produk.lokasi_reseller");
        $this->db->join("reseller","login.reseller_id = reseller.reseller_id");
        $this->db->where("reseller.reseller_id !=",$reseller_id);
        $this->db->where("po_produk.tanggal_uang_diterima >=",$date_start);
        $this->db->where("po_produk.tanggal_uang_diterima <=",$date_limit);
        $this->db->where("po_produk.om_leader",$reseller_id);
        return $this->db->get("po_produk")->row();
    }

    function leader_board_data($start,$length,$query){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
	    $sql = "(SELECT reseller.reseller_id,reseller.nama,count(reseller.reseller_id) as jumlah from po_produk
INNER JOIN mykindofbeauty_master.login as login on login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on login.reseller_id = reseller.reseller_id
WHERE tanggal_uang_diterima > '.$date_start.'
and tanggal_uang_diterima < '.$date_end.'
and status_pembayaran = 'Lunas'
and reseller.reseller_id != 372
and reseller.type = 'super agen'
GROUP BY reseller.reseller_id)";
	    $this->db->select('b.nama,sum(po_produk.grand_total) as omset, if (a.jumlah is null,0,a.jumlah) as deposit');
	    $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','reseller on login.reseller_id = reseller.reseller_id');
        $this->db->join('reseller as b','b.reseller_id = reseller.leader');
        $this->db->join($sql.' as a','a.reseller_id = b.reseller_id','left');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('status_pembayaran','Lunas');
        $this->db->where('b.reseller_id !=',372);
        $this->db->group_by('reseller.leader');
        $this->db->order_by('omset','desc');
        return $this->db->get('po_produk', $length, $start)->result();
    }
    function anual_omset($reseller_id){
	    $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
        $sql = "(SELECT reseller.reseller_id,reseller.nama,count(reseller.reseller_id) as jumlah from po_produk
INNER JOIN mykindofbeauty_master.login as login on login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on login.reseller_id = reseller.reseller_id
WHERE tanggal_uang_diterima > '.$date_start.'
and tanggal_uang_diterima < '.$date_end.'
and status_pembayaran = 'Lunas'
and reseller.reseller_id != 372
and reseller.type = 'super agen'
GROUP BY reseller.reseller_id)";
        $this->db->select('b.nama,sum(po_produk.grand_total) as omset, if (a.jumlah is null,0,a.jumlah) as deposit');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','reseller on login.reseller_id = reseller.reseller_id');
        $this->db->join('reseller as b','b.reseller_id = reseller.leader');
        $this->db->join($sql.' as a','a.reseller_id = b.reseller_id','left');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('status_pembayaran','Lunas');
        $this->db->where('b.reseller_id !=',372);
        $this->db->where('b.reseller_id',$reseller_id);
        $this->db->group_by('reseller.leader');
        $this->db->order_by('omset','desc');
        return $this->db->get('po_produk')->row();
    }
    function annual_rank($omset){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
        $sql = "(SELECT reseller.reseller_id,reseller.nama,count(reseller.reseller_id) as jumlah from po_produk
INNER JOIN mykindofbeauty_master.login as login on login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on login.reseller_id = reseller.reseller_id
WHERE tanggal_uang_diterima > '.$date_start.'
and tanggal_uang_diterima < '$date_end'
and status_pembayaran = 'Lunas'
and reseller.reseller_id != 372
and reseller.type = 'super agen'
GROUP BY reseller.reseller_id)";
        $this->db->select('b.nama,sum(po_produk.grand_total) as omset, if (a.jumlah is null,0,a.jumlah) as deposit');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','reseller on login.reseller_id = reseller.reseller_id');
        $this->db->join('reseller as b','b.reseller_id = reseller.leader');
        $this->db->join($sql.' as a','a.reseller_id = b.reseller_id','left');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('status_pembayaran','Lunas');
        $this->db->where('b.reseller_id !=',372);
        $this->db->having('omset >=',$omset);
        $this->db->group_by('reseller.leader');
        $this->db->order_by('omset','desc');
        return $this->db->get('po_produk')->result();
    }
    function reseller_leader_board_data($start,$length){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
	    $this->db->select('reseller.nama,sum(po_produk.grand_total) as omset');
	    $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->not_like('reseller.email','#');
        $this->db->where('reseller.type','agent');
        $this->db->group_by('lokasi_reseller');
        $this->db->order_by('omset','desc');
        return $this->db->get('po_produk', $length, $start)->result();
    }
    function reseller_anual_omset($reseller_id){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
        $this->db->select('reseller.nama,sum(po_produk.grand_total) as omset');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('reseller.type','agent');
        $this->db->where('reseller.reseller_id',$reseller_id);
        $this->db->group_by('lokasi_reseller');
        $this->db->order_by('omset','desc');
        return $this->db->get('po_produk')->row();
    }
    function reseller_annual_rank($omset){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
        $omset = $omset == null ? 0 : $omset;
        $this->db->select('reseller.nama,sum(po_produk.grand_total) as omset');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('reseller.type','agent');
        $this->db->having('omset >=',$omset);
        $this->db->group_by('lokasi_reseller');
        $this->db->order_by('omset','desc');
        return $this->db->get('po_produk')->result();
    }
    function qty_deposit($reseller_id){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
	    $this->db->select('reseller_id');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('tanggal_uang_diterima >',$date_start);
        $this->db->where('tanggal_uang_diterima <',$date_end);
        $this->db->where('reseller.reseller_id',$reseller_id);
        return $this->db->count_all_results('po_produk');
    }

    function deposit_before_this_year($reseller_id){
	    $date = date("Y-01-01");
	    $this->db->select('sum(po_produk.grand_total) as omset');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('tanggal_uang_diterima <',$date);
        $this->db->where('reseller.reseller_id',$reseller_id);
        $result = $this->db->get('po_produk')->row();
        if($result==null){
            return 0;
        }else{
            return $result->omset;
        }
    }
    function deposit_this_year($reseller_id){
        $date_start = date("Y-01-01 00:00:00");
        $date_end = date("Y-12-31 23:59:59");
        $this->db->select('sum(po_produk.grand_total) as omset');
        $this->db->join('mykindofbeauty_master.login as login','login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','login.reseller_id = reseller.reseller_id');
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $this->db->where('tanggal_uang_diterima >=',$date_start);
        $this->db->where('tanggal_uang_diterima <=',$date_end);
        $this->db->where('reseller.reseller_id',$reseller_id);
        $result = $this->db->get('po_produk')->row();
        if($result==null){
            return 0;
        }else{
            return $result->omset;
        }
    }

    function is_full_payment($history,$reseller_id){
	    $batas = 0;
	    $reseller = $this->db->where('reseller_id',$reseller_id)->get('reseller')->row();
        foreach ($history as $item){
            $batas = $batas+$item->fullpayment;
        }
        if($reseller->first_deposit_date==null||($batas==0)||($batas==1&&$reseller->first_deposit_date>'2023-02-06')){
            return false;
        }else{
            return true;
        }
    }
}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */