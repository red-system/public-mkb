<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deposit_super_reseller extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "deposit_super_reseller";
    }
    function _list($start,$length,$query){
        $this->db->like('reseller_id', $query, 'BOTH');
        $this->db->or_like('nama', $query, 'BOTH');
        return $this->db->get('deposit_super_reseller', $length, $start)->result();
    }
    function count_all(){

        return $this->db->count_all_results('deposit_super_reseller');
    }
    function count_filter($query){
        $this->db->like('reseller_id', $query, 'BOTH');
        $this->db->or_like('nama', $query, 'BOTH');
        return $this->db->count_all_results('deposit_super_reseller');
    }


}

/* End of file Color.php */
/* Location: ./application/models/Color.php */