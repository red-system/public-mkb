<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_role extends MY_Model {

public function __construct()
	{
		parent::__construct();
		$this->table_name = "user_role";
	}	
	function user_role_list($start,$length,$query){
		$this->db->like('user_role_id', $query, 'BOTH'); 
		$this->db->or_like('user_role_name', $query, 'BOTH'); 
		$this->db->order_by('user_role_id', 'desc');
		return $this->db->get('user_role', $length, $start)->result();
	}
	function user_role_count_filter($query){
		$this->db->like('user_role_id', $query, 'BOTH'); 
		$this->db->or_like('user_role_name', $query, 'BOTH'); 
		return $this->db->count_all_results('user_role');
	}
	function user_role_count_all(){
		return $this->db->count_all_results('user_role');
	}
	function all_menu(){
		$this->db->select('menu.*,sub_menu.sub_menu_kode,sub_menu.sub_menu_nama,sub_menu.action as "sub_action",sub_menu.url as "sub_url", sub_menu.data,sub_menu.urutan as sub_urutan');
		$this->db->join('sub_menu', 'sub_menu.menu_id = menu.menu_id', 'left');
		$this->db->order_by('menu.urutan', 'asc');
        $this->db->order_by('sub_menu.urutan', 'asc');
		return $this->db->get('menu')->result();
	}
	function get_akses_role($id){
		$this->db->select('user_role_akses');
		$this->db->where('user_role_id', $id);
		return $this->db->get('user_role')->row()->user_role_akses;
	}
}

/* End of file User_role.php */
/* Location: ./application/models/User_role.php */