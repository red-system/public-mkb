<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Midtrans_response extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "midtrans_response";
    }
    function count_all(){
        $this->db->join('penjualan','penjualan.payment_id = midtrans_response.order_id');
        return $this->db->count_all_results($this->table_name);
    }
    function count_filter($query){
        $this->db->join('penjualan','penjualan.payment_id = midtrans_response.order_id');
        $this->db->group_start();
            $this->db->or_like('transaction_time',$query,'BOTH');
            $this->db->or_like('transaction_status',$query,'BOTH');
            $this->db->or_like('status_message',$query,'BOTH');
            $this->db->or_like('payment_type',$query,'BOTH');
            $this->db->or_like('gross_amount',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results($this->table_name);
    }
    function list($start,$length,$query){
        $this->db->select('midtrans_response.*,penjualan.no_faktur');
        $this->db->join('penjualan','penjualan.payment_id = midtrans_response.order_id');
        $this->db->group_start();
        $this->db->or_like('transaction_time',$query,'BOTH');
        $this->db->or_like('transaction_status',$query,'BOTH');
        $this->db->or_like('status_message',$query,'BOTH');
        $this->db->or_like('payment_type',$query,'BOTH');
        $this->db->or_like('gross_amount',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get($this->table_name,$length,$start)->result();
    }
}

/* End of file Piutang.php */
/* Location: ./application/models/Piutang.php */