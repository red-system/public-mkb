<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response_payment extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "response_payment";
    }
    function check($po_produk_id){
        $this->db->where("status <>","error");
        $this->db->where("po_produk_id",$po_produk_id);
        return $this->db->get('response_payment')->row();
    }
    function count_repeat($po_produk_id){
        $this->db->where("po_produk_id",$po_produk_id);
        return $this->db->count_all_results('response_payment')+1;
    }


}

/* End of file Color.php */
/* Location: ./application/models/Color.php */