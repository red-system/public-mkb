<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "penjualan";
	}
	//untuk pos retail
	function save(){
		$this->load->model('stock_produk','',true);
		$this->db->trans_begin();
		$lokasi_id = $this->input->post('lokasi_id');
		$data["lokasi_id"] = $this->input->post('lokasi_id');
        $tipe_pembayaran_id = $this->input->post('tipe_pembayaran_id');
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$data["lokasi_id"] = $_SESSION["redpos_login"]["lokasi_id"];
            $lokasi_id = $_SESSION["redpos_login"]["lokasi_id"];
		}
		$transaksi = json_decode($this->input->post('transaksi'));
		$potongan = $transaksi->potongan;
		$tambahan = $transaksi->tambahan;
		$total_item = $transaksi->total_item;
		$grand_total = $transaksi->grand_total;
		$terbayar = $transaksi->terbayar;
		$kembalian = $terbayar - $grand_total;
        $no_faktur = $this->input->post('no_faktur');
		$data['urutan'] = $this->input->post('urutan');
		$data['no_faktur'] = $this->input->post('no_faktur');
		$data["nama_pelanggan"] = $this->input->post('guest_nama');
		$data["alamat_pelanggan"] = $this->input->post('guest_alamat');
		$data["telepon_pelanggan"] = $this->input->post('guest_telepon');
        $data["province_id"] = $this->input->post('outlet_province_id');
		$data["subdistrict_id"] = $this->input->post('outlet_subdistrict_id');
        $data["city_id"] = $this->input->post('outlet_city_id');
		$data["urutan"] = $this->input->post('urutan');
		$data["no_faktur"] = $this->input->post('no_faktur');
		$data["tanggal"] = date("Y-m-d");
		$data["pengiriman"] = $this->input->post('pengiriman');
		$data["total"] = $total_item;
		$data["biaya_tambahan"] = $tambahan;
		$data["potongan_akhir"] = $potongan;
		$data["grand_total"] = $grand_total;
		$data["additional_no"] = $this->input->post('additional_no');
		$data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d",strtotime($this->input->post('additional_tanggal'))) : "";
		$data["additional_keterangan"] = $this->input->post('additional_keterangan');
		$data["terbayar"] = $terbayar;
		$data["sisa_pembayaran"] = 0;
		$data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
		$data["kembalian"] = $kembalian;
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if($jenis_pembayaran == "kredit"){
			$data['status_pembayaran'] = "Hutang";
		} else {
			$data['status_pembayaran'] = "Lunas";
		}
		$this->db->insert('penjualan', $data);
		$penjualan_id = $this->db->insert_id();
		if($tipe_pembayaran_id==-13){
		$mid = array("payment_id"=>$_SESSION['redpos_login']['company_id'].'-'.$penjualan_id);
		$this->db->where('penjualan_id',$penjualan_id);
		$this->db->update('penjualan',$mid);
		}
		foreach ($transaksi->item as $key) {
			$total_stock_belanja = $key->jumlah*$key->nilai_konversi;
            $row_stock = $this->stock_produk->stock_by_location_produk_id($lokasi_id,$key->produk_id);
            $stock_produk_id_arr = array();
            $qty_stock = array();
            $jumlah_bahan = $total_stock_belanja;
            foreach ($row_stock as $row) {
                $data = array();
                $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                $stock_out = $jumlah_bahan;
                if($row->stock_produk_qty < $jumlah_bahan){
                    $jumlah = 0;
                    $stock_out = $row->stock_produk_qty;
                }
                $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                $data["stock_produk_qty"] = $jumlah;
                $stock_produk_id = $row->stock_produk_id;
                $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
                array_push($stock_produk_id_arr,$stock_produk_id);
                array_push($qty_stock,$stock_out);
                if($updateStok){
                    $data = array();
                    $data["penjualan_id"] = $penjualan_id;
                    $data["tanggal"] = date("Y-m-d");
                    $data["table_name"] = "stock_produk";
                    $data["stock_produk_id"] = $row->stock_produk_id;
                    $data["produk_id"] = $key->produk_id;
                    $data["stock_out"] = $stock_out;
                    $data["stock_in"] = 0;
                    $data["last_stock"] = $this->stock_produk->last_stock($row->produk_id)->result;
                    $data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                    $data["keterangan"] = "Penjualan Produk no penjualan ".$no_faktur;
                    $data["method"] = "update";
                    $this->stock_produk->arus_stock_produk($data);
                }
                if($jumlah_bahan <= 0){
                    break;
                }

            }
            $harga_net = $key->harga_eceran;
            if($key->tipe_penjualan == "grosir"){
                $harga_net = $key->harga_grosir;
            }
            $data = array();
            $data["penjualan_id"] = $penjualan_id;
            $data["produk_id"] = $key->produk_id;
            $data["satuan_nama"] = $key->satuan_nama;
            $data["qty"] = $this->string_to_number($key->jumlah);
            $data["harga"] = $this->string_to_number($harga_net);
            $data["stock_produk_id"] = implode("|",$stock_produk_id_arr);
            $data["qty_stock"] = implode("|",$qty_stock);
            $data["ppn_persen"] = "0.1";
            $data["hpp"] = $key->hpp;
            $data["sub_total"] = $this->string_to_number($key->subtotal);
            $this->db->insert('penjualan_produk', $data);
		}
		if($jenis_pembayaran == "kredit"){
			$data = array();
			$data['penjualan_id'] = $penjualan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan'));
			$data['tenggat_pelunasan'] = date("Y-m-d",$temp);
			$this->db->insert('piutang', $data);
			$piutang_id = $this->db->insert_id();
			if($terbayar > 0){

			}
		}
		if ($this->db->trans_status() === FALSE){
			$return["result"] = false;
			return $return;
		}

		$this->db->trans_commit();
		$return["result"] = true;
		$return["id"] = $penjualan_id;
		return $return;
	}
	// process langsung resto
    function direct_pos_resto(){
        $this->load->model('stock_produk','',true);
        $this->load->model('stock_bahan','',true);
        $this->db->trans_begin();
        $lokasi_id = $this->input->post('lokasi_id');
        $penjualan_id = $this->input->post('penjualan_id');
        $data["lokasi_id"] = $this->input->post('lokasi_id');
        $tipe_pembayaran_id = $this->input->post('tipe_pembayaran_id');
        $data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
        if(isset($_SESSION["redpos_login"]["lokasi_id"])){
            $data["lokasi_id"] = $_SESSION["redpos_login"]["lokasi_id"];
            $lokasi_id = $_SESSION["redpos_login"]["lokasi_id"];
        }
        $transaksi = json_decode($this->input->post('transaksi'));
        $potongan = $this->input->post('potongan_akhir');
        $tambahan = $this->input->post('biaya_tambahan');
        //here
        $total_item = $transaksi->total_item;
        $grand_total = $transaksi->grand_total;
        $terbayar = $transaksi->terbayar;
        $kembalian = $terbayar - $grand_total;
        $no_faktur = $this->input->post('no_faktur');
        $data['urutan'] = $this->input->post('urutan');
        $data['no_faktur'] = $this->input->post('no_faktur');
        $data["nama_pelanggan"] = $this->input->post('guest_nama');
        $data["alamat_pelanggan"] = $this->input->post('guest_alamat');
        $data["telepon_pelanggan"] = $this->input->post('guest_telepon');
        $data["urutan"] = $this->input->post('urutan');
        $data["no_faktur"] = $this->input->post('no_faktur');
        $data["tanggal"] = date("Y-m-d");
        $data["pengiriman"] = $this->input->post('pengiriman');
        $data["total"] = $total_item;
        $data["biaya_tambahan"] = $tambahan;
        $data["potongan_akhir"] = $potongan;
        $data["grand_total"] = $grand_total;
        $data["additional_no"] = $this->input->post('additional_no');
        $data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d",strtotime($this->input->post('additional_tanggal'))) : "";
        $data["additional_keterangan"] = $this->input->post('additional_keterangan');
        $data["terbayar"] = $terbayar;
        $data["sisa_pembayaran"] = 0;
        $data["status"] = 'process';
        $data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
        $data["kembalian"] = $kembalian;
        $jenis_pembayaran = $this->input->post('jenis_pembayaran');
        $lokasi_kode = $this->lokasi->row_by_id($lokasi_id)->lokasi_kode;
        if($jenis_pembayaran == "kredit"){
            $data['status_pembayaran'] = "Hutang";
        } else {
            $data['status_pembayaran'] = "Lunas";
        }
        if($penjualan_id==''){
            $this->insert($data);
            $penjualan_id = $this->last_id();
        } else {
            $this->update_by_id('penjualan_id',$penjualan_id,$data);
        }
        if($tipe_pembayaran_id==-13){
            $mid = array("payment_id"=>$_SESSION['redpos_login']['company_id'].'-'.$penjualan_id);
            $this->db->where('penjualan_id',$penjualan_id);
            $this->db->update('penjualan',$mid);
        }
        $detailTransaksi = $this->detailTransaksi($penjualan_id);
        foreach ($transaksi->item as $key) {

            $this->db->where('produk_id',$key->produk_id);
            $produk_recipe = $this->db->get('produk_recipe')->row();
            $produk_recipe_id = $produk_recipe == null ? null : $produk_recipe->produk_recipe_id;
            $stock_produk_id_ar = array();
            $stock_bahan_id_ar = array();
            $qty_stock = array();
            $qty_stock_bahan = array();
            if($produk_recipe_id == null){

                $total_stock_belanja = $key->jumlah*$key->nilai_konversi;
                $row_stock = $this->resto_stock_by_location_produk_id($lokasi_id,$key->produk_id);
                if($row_stock==null){
                    $jenis_produk_kode = $this->produk->produk_by_id($key->produk_id)->jenis_produk_kode;
                    $stock_produk_seri = date("my").$jenis_produk_kode.$lokasi_kode;
                    $urutan = $this->stock_produk->urutan_seri($stock_produk_seri);
                    $new_stock = array(
                        "produk_id"=>$key->produk_id,
                        "stock_produk_qty"=>0,
                        "stock_produk_lokasi_id"=>$lokasi_id,
                        "stock_produk_seri"=>$stock_produk_seri,
                        "urutan"=>$urutan
                    );
                    $insert = $this->db->insert('stock_produk',$new_stock);
                    if($insert){
                        $last_id = $this->last_id();
                        $obj_stock = new \stdClass;
                        $obj_stock->stock_produk_id = $last_id;
                        $obj_stock->produk_id = $key->produk_id;
                        $obj_stock->stock_produk_qty = 0;
                        $obj_stock->stock_produk_lokasi_id = $lokasi_id;
                        $obj_stock->stock_produk_seri = $stock_produk_seri;
                        $obj_stock->urutan = $urutan;
                        $row_stock = array();
                        $row_stock[]=$obj_stock;
                    }
                }

                $jumlah_bahan = $total_stock_belanja;
                foreach ($row_stock as $row) {
                    $data = array();
                    $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                    $stock_out = $jumlah_bahan;
                    if($row->stock_produk_qty < $jumlah_bahan){
                        $jumlah = 0;
                        $stock_out = $row->stock_produk_qty;
                    }
                    $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                    $data["stock_produk_qty"] = $jumlah;
                    $stock_produk_id = $row->stock_produk_id;
                    $this->db->where('stock_produk_id',$stock_produk_id);
                    $updateStok = $this->db->update('stock_produk',$data);
                    if($updateStok){
                        array_push($stock_produk_id_ar,$row->stock_produk_id);
                        array_push($qty_stock,$stock_out);
                        $data = array();
                        $data["penjualan_id"] = $penjualan_id;
                        $data["tanggal"] = date("Y-m-d");
                        $data["table_name"] = "stock_produk";
                        $data["stock_produk_id"] = $row->stock_produk_id;
                        $data["produk_id"] = $key->produk_id;
                        $data["stock_out"] = $stock_out;
                        $data["stock_in"] = 0;
                        $data["last_stock"] = $this->produk_last_stock($row->produk_id)->result;
                        $data["last_stock_total"] = $this->produk_stock_total()->result;
                        $data["keterangan"] = "Penjualan Produk no penjualan ".$no_faktur;
                        $data["method"] = "update";
                        $this->stock_produk->arus_stock_produk($data);
                    }
                    if($jumlah_bahan <= 0){
                        break;
                    }

                }
            } else{
                $this->db->where('produk_recipe_id',$produk_recipe_id);
                $produk_recipe_detail = $this->db->get('produk_recipe_detail')->result();
                foreach ($produk_recipe_detail as $recipe_detail_item){
                    $total_stock_belanja = $key->jumlah*$key->nilai_konversi*$recipe_detail_item->konversi_takaran;
                    $row_stock = $this->resto_stock_by_location_bahan_id($lokasi_id, $recipe_detail_item->bahan_id);
                    $jumlah_bahan = $total_stock_belanja;
                    if($row_stock==null){
                        $new_stock = array(
                            "bahan_id"=>$recipe_detail_item->bahan_id,
                            "stock_bahan_qty"=>0,
                            "stock_bahan_lokasi_id"=>$lokasi_id,
                        );
                        $insert = $this->db->insert('stock_bahan',$new_stock);
                        if($insert){
                            $last_id = $this->last_id();
                            $obj_stock = new \stdClass;
                            $obj_stock->bahan_id=$recipe_detail_item->bahan_id;
                            $obj_stock->stock_bahan_id = $last_id;
                            $obj_stock->stock_bahan_qty = 0;
                            $obj_stock->stock_bahan_lokasi_id = $lokasi_id;
                            $row_stock = array();
                            $row_stock[]=$obj_stock;
                        }
                    }
                    foreach ($row_stock as $row) {

                        $data = array();
                        $jumlah = $row->stock_bahan_qty - $jumlah_bahan;
                        $stock_out = $jumlah_bahan;
                        if ($row->stock_bahan_qty < $jumlah_bahan) {
                            $jumlah = 0;
                            $stock_out = $row->stock_bahan_qty;
                        }
                        $jumlah_bahan = ($jumlah_bahan - $row->stock_bahan_qty);
                        $data["stock_bahan_qty"] = $jumlah;
                        $data["updated_at"] = date("Y-m-d H:i:s");
                        $data["staff_updated"] = $_SESSION["redpos_login"]["staff_id"];
                        $stock_bahan_id = $row->stock_bahan_id;
                        $this->db->where('stock_bahan_id', $stock_bahan_id);
                        $updateStok = $this->db->update('stock_bahan', $data);
                        if ($updateStok) {
                            $data = array();
                            array_push($stock_bahan_id_ar,$row->stock_bahan_id);
                            array_push($qty_stock_bahan,$stock_out);
                            $data["penjualan_id"] = $penjualan_id;
                            $data["tanggal"] = date("Y-m-d");
                            $data["table_name"] = "stock_bahan";
                            $data["stock_bahan_id"] = $row->stock_bahan_id;
                            $data["bahan_id"] = $recipe_detail_item->bahan_id;
                            $data["stock_out"] = $stock_out;
                            $data["stock_in"] = 0;
                            $data["last_stock"] = $this->bahan_last_stock($row->bahan_id)->result;
                            $data["last_stock_total"] = $this->bahan_stock_total()->result;
                            $data["keterangan"] = "Recipe Penjualan Produk no penjualan " . $no_faktur;
                            $data["method"] = "update";
                            $data["created_at"] = date("Y-m-d H:i:s");
                            $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
                            $this->db->insert('arus_stock_bahan', $data);
                        }
                        if ($jumlah_bahan <= 0) {
                            break;
                        }

                    }
                }
            }
            $harga_net = $key->harga_eceran;
            if($key->tipe_penjualan == "grosir"){
                $harga_net = $key->harga_grosir;
            }
            $data = array();
            $data["stock_produk_id"] = implode("|",$stock_produk_id_ar);
            $data["stock_bahan_id"] = implode("|",$stock_bahan_id_ar);
            $data["qty_stock"] = implode("|",$qty_stock);
            $data["qty_stock_bahan"] = implode("|",$qty_stock_bahan);
            $data["penjualan_id"] = $penjualan_id;
            $data["produk_id"] = $key->produk_id;
            $data["satuan_nama"] = $key->satuan_nama;
            $data["qty"] = $this->string_to_number($key->jumlah);
            $data["harga"] = $this->string_to_number($harga_net);
            $data["ppn_persen"] = "0.1";
            $data["hpp"] = $key->hpp;
            $data["sub_total"] = $this->string_to_number($key->subtotal);
            $data["keterangan"] = $this->string_to_number($key->keterangan);
            $this->db->insert('penjualan_produk', $data);
        }
        foreach ($detailTransaksi as $value) {
            $stock_produk_id_ar = array();
            $stock_bahan_id_ar = array();
            $qty_stock = array();
            $qty_stock_bahan = array();
            if($value->produk_recipe_id == null){
                $total_stock_belanja = $value->qty*$value->nilai_konversi;
                $row_stock = $this->resto_stock_by_location_produk_id($lokasi_id,$value->produk_id);
                if($row_stock==null){
                    $jenis_produk_kode = $this->produk->produk_by_id($key->produk_id)->jenis_produk_kode;
                    $stock_produk_seri = date("my").$jenis_produk_kode.$lokasi_kode;
                    $urutan = $this->stock_produk->urutan_seri($stock_produk_seri);

                    $new_stock = array(
                        "produk_id"=>$key->produk_id,
                        "stock_produk_qty"=>0,
                        "stock_produk_lokasi_id"=>$lokasi_id,
                        "stock_produk_seri"=>$stock_produk_seri,
                        "urutan"=>$urutan
                    );
                    $insert = $this->db->insert('stock_produk',$new_stock);
                    if($insert){
                        $last_id = $this->last_id();
                        $obj_stock = new \stdClass;
                        $obj_stock->stock_produk_id = $last_id;
                        $obj_stock->stock_produk_qty = 0;
                        $obj_stock->stock_produk_lokasi_id = $lokasi_id;
                        $obj_stock->stock_produk_seri = $stock_produk_seri;
                        $obj_stock->urutan = $urutan;
                        $row_stock = array();
                        $row_stock[]=$obj_stock;
                    }
                }
                $jumlah_bahan = $total_stock_belanja;
                foreach ($row_stock as $row) {
                    $data = array();
                    $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                    $stock_out = $jumlah_bahan;
                    if($row->stock_produk_qty < $jumlah_bahan){
                        $jumlah = 0;
                        $stock_out = $row->stock_produk_qty;
                    }
                    $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                    $data["stock_produk_qty"] = $jumlah;
                    $stock_produk_id = $row->stock_produk_id;
                    $this->db->where('stock_produk_id',$stock_produk_id);
                    $updateStok = $this->db->update('stock_produk',$data);
                    if($updateStok){
                        array_push($stock_produk_id_ar,$row->stock_produk_id);
                        array_push($qty_stock,$stock_out);
                        $data = array();
                        $data["penjualan_id"] = $penjualan_id;
                        $data["tanggal"] = date("Y-m-d");
                        $data["table_name"] = "stock_produk";
                        $data["stock_produk_id"] = $row->stock_produk_id;
                        $data["produk_id"] = $value->produk_id;
                        $data["stock_out"] = $stock_out;
                        $data["stock_in"] = 0;
                        $data["last_stock"] = $this->produk_last_stock($row->produk_id)->result;
                        $data["last_stock_total"] = $this->produk_stock_total()->result;
                        $data["keterangan"] = "Penjualan Produk no penjualan ".$no_faktur;
                        $data["method"] = "update";
                        $this->stock_produk->arus_stock_produk($data);
                    }
                    if($jumlah_bahan <= 0){
                        break;
                    }

                }
            } else{
                $this->db->where('produk_recipe_id',$value->produk_recipe_id);
                $produk_recipe_detail = $this->db->get('produk_recipe_detail')->result();
                foreach ($produk_recipe_detail as $recipe_detail_item){
                    $total_stock_belanja = $value->qty*$value->nilai_konversi*$recipe_detail_item->konversi_takaran;
                    $row_stock = $this->resto_stock_by_location_bahan_id($lokasi_id, $recipe_detail_item->bahan_id);
                    if($row_stock==null){
                        $new_stock = array(
                            "bahan_id"=>$recipe_detail_item->bahan_id,
                            "stock_bahan_qty"=>0,
                            "stock_bahan_lokasi_id"=>$lokasi_id,
                        );
                        $insert = $this->db->insert('stock_bahan',$new_stock);
                        if($insert){
                            $last_id = $this->last_id();
                            $obj_stock = new \stdClass;
                            $obj_stock->stock_bahan_id = $last_id;
                            $obj_stock->stock_bahan_qty = 0;
                            $obj_stock->stock_bahan_lokasi_id = $lokasi_id;
                            $row_stock = array();
                            $row_stock[]=$obj_stock;
                        }
                    }
                    $jumlah_bahan = $total_stock_belanja;
                    foreach ($row_stock as $row) {
                        $data = array();
                        $jumlah = $row->stock_bahan_qty - $jumlah_bahan;
                        $stock_out = $jumlah_bahan;
                        if ($row->stock_bahan_qty < $jumlah_bahan) {
                            $jumlah = 0;
                            $stock_out = $row->stock_bahan_qty;
                        }
                        $jumlah_bahan = ($jumlah_bahan - $row->stock_bahan_qty);
                        $data["stock_bahan_qty"] = $jumlah;
                        $data["updated_at"] = date("Y-m-d H:i:s");
                        $data["staff_updated"] = $_SESSION["redpos_login"]["staff_id"];
                        $stock_bahan_id = $row->stock_bahan_id;
                        $this->db->where('stock_bahan_id', $stock_bahan_id);
                        $updateStok = $this->db->update('stock_bahan', $data);
                        if ($updateStok) {
                            array_push($stock_bahan_id_ar,$row->stock_bahan_id);
                            array_push($qty_stock_bahan,$stock_out);
                            $data = array();
                            $data["penjualan_id"] = $penjualan_id;
                            $data["tanggal"] = date("Y-m-d");
                            $data["table_name"] = "stock_bahan";
                            $data["stock_bahan_id"] = $row->stock_bahan_id;
                            $data["bahan_id"] = $recipe_detail_item->bahan_id;
                            $data["stock_out"] = $stock_out;
                            $data["stock_in"] = 0;
                            $data["last_stock"] = $this->bahan_last_stock($row->bahan_id)->result;
                            $data["last_stock_total"] = $this->bahan_stock_total()->result;
                            $data["keterangan"] = "Recipe Penjualan Produk no penjualan " . $no_faktur;
                            $data["method"] = "update";
                            $data["created_at"] = date("Y-m-d H:i:s");
                            $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
                            $this->db->insert('arus_stock_bahan', $data);
                        }
                        if ($jumlah_bahan <= 0) {
                            break;
                        }

                    }
                }
            }
            $change = array();
            $change["stock_produk_id"] = implode("|",$stock_produk_id_ar);
            $change["stock_bahan_id"] = implode("|",$stock_bahan_id_ar);
            $change["qty_stock"] = implode("|",$qty_stock);
            $change["qty_stock_bahan"] = implode("|",$qty_stock_bahan);
            $this->db->where('penjualan_produk_id',$value->penjualan_produk_id);
            $this->db->update('penjualan_produk',$change);
        }
        if($jenis_pembayaran == "kredit"){
            $data = array();
            $data['penjualan_id'] = $penjualan_id;
            $temp = strtotime($this->input->post('tenggat_pelunasan'));
            $data['tenggat_pelunasan'] = date("Y-m-d",$temp);
            $this->db->insert('piutang', $data);
            $piutang_id = $this->db->insert_id();
            if($terbayar > 0){

            }
        }
        if ($this->db->trans_status() === FALSE){
            $return["result"] = false;
            return $return;
        }

        $this->db->trans_commit();
        $return["result"] = true;
        $return["id"] = $penjualan_id;
        return $return;
    }
	//simpan order resto
	function save_order(){
	    $this->start_trans();
	    $penjualan_id = $this->input->post('penjualan_id');
        $guest_nama = $this->input->post('guest_nama');
        $urutan = $this->input->post('urutan');
        $guest_alamat = $this->input->post('guest_alamat');
        $guest_telepon = $this->input->post('guest_telepon');
        $no_faktur = $this->input->post('no_faktur');
        $tenggat_pelunasan = $this->input->post('tenggat_pelunasan');
        $jenis_pembayaran = $this->input->post('jenis_pembayaran');
        $additional_no = $this->input->post('additional_no');
        $additional_tanggal = $this->input->post('additional_tanggal');
        $additional_keterangan = $this->input->post('additional_keterangan');
        $order_name = $this->input->post('order_name');
        $order_note = $this->input->post('order_note');
        $pengiriman = $this->input->post('pengiriman');
        $lokasi_id = $this->input->post('lokasi_id');
        $transaksi = $this->input->post('transaksi');
        $transaksi = json_decode($transaksi);
        $potongan = $transaksi->potongan;
        $tambahan = $transaksi->tambahan;
        $total_item = $transaksi->total_item;
        $grand_total = $transaksi->grand_total;
        $terbayar = 0;
        $kembalian = null;
        $data['lokasi_id'] = $lokasi_id;
        $data['urutan'] = $urutan;
        $data['no_faktur'] = $no_faktur;
        $data["nama_pelanggan"] = $guest_nama;
        $data["alamat_pelanggan"] = $guest_alamat;
        $data["telepon_pelanggan"] = $guest_telepon;
        $data["no_faktur"] = $no_faktur;
        $data["tanggal"] = date("Y-m-d");
        $data["pengiriman"] = $pengiriman;
        $data["total"] = $total_item;
        $data["biaya_tambahan"] = $tambahan;
        $data["order_name"] = $order_name;
        $data["order_note"] = $order_note;
        $data["potongan_akhir"] = $potongan;
        $data["grand_total"] = $grand_total;
        $data["additional_no"] = $additional_no;
        $data["additional_tanggal"] = $additional_tanggal != "" ? Date("Y-m-d",strtotime($additional_tanggal)) : "";
        $data["additional_keterangan"] = $additional_keterangan;
        $data["terbayar"] = $terbayar;
        $data["sisa_pembayaran"] = 0;
        $data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
        $data["kembalian"] = $kembalian;
        $data["status"] = 'order';
        if($penjualan_id==''){
            $this->insert($data);
            $penjualan_id = $this->last_id();
        } else {
            $this->update_by_id('penjualan_id',$penjualan_id,$data);
        }
        foreach ($transaksi->item as $key) {
            $harga_net = $key->harga_eceran;
            if($key->tipe_penjualan == "grosir"){
                $harga_net = $key->harga_grosir;
            }
            $data = array();
            $data["penjualan_id"] = $penjualan_id;
            $data["produk_id"] = $key->produk_id;
            $data["satuan_nama"] = $key->satuan_nama;
            $data["qty"] = $this->string_to_number($key->jumlah);
            $data["harga"] = $this->string_to_number($harga_net);
            $data["ppn_persen"] = "0.1";
            $data["hpp"] = $key->hpp;
            $data["nilai_konversi"] = $key->nilai_konversi;
            $data["sub_total"] = $this->string_to_number($key->subtotal);
            $data["keterangan"] = $this->string_to_number($key->keterangan);
            $this->db->insert('penjualan_produk', $data);
        }
        $process = $this->result_trans();
        if($process){
            $return["result"] = true;
            $return["id"] = $penjualan_id;
        } else {
            $return["result"] = false;
        }
        return $return;

    }
    // prosess order resto
    function process_order(){
        $this->load->model('stock_produk','',true);
        $this->load->model('stock_bahan','',true);
	    $this->start_trans();
	    $tipe_pembayaran_id = $this->input->post('tipe_pembayaran_id');
        $penjualan_id = $this->input->post('penjualan_id');
        $additional_no = $this->input->post('additional_no');
        $additional_tanggal = $this->input->post('additional_tanggal');
        $additional_keterangan = $this->input->post('additional_keterangan');
        $transaksi = $this->input->post('transaksi');

        $transaksi = json_decode($transaksi);
        $potongan = $transaksi->potongan;
        $tambahan = $transaksi->tambahan;
        $total_item = $transaksi->total_item;
        $grand_total = $transaksi->grand_total;
        $terbayar = $transaksi->terbayar;
        $kembalian =$transaksi->terbayar - $transaksi->grand_total ;
        $data["tanggal"] = date("Y-m-d");
        $detailTransaksi = $this->detailTransaksi($penjualan_id);
        $data["total"] = $total_item;
        $data["biaya_tambahan"] = $tambahan;
        $data["potongan_akhir"] = $potongan;
        $data["grand_total"] = $grand_total;
        $data["additional_no"] = $additional_no;
        $data["additional_tanggal"] = $additional_tanggal != "" ? Date("Y-m-d",strtotime($additional_tanggal)) : "";
        $data["additional_keterangan"] = $additional_keterangan;
        $data["terbayar"] = $terbayar;
        $data["sisa_pembayaran"] = 0;
        $data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
        $data["kembalian"] = $kembalian;
        $data["status"] = 'process';
        $this->update_by_id('penjualan_id',$penjualan_id,$data);
        $penjualan = $this->row_by_id($penjualan_id);
        $lokasi_kode = $this->lokasi->row_by_id($penjualan->lokasi_id)->lokasi_kode;
        if($tipe_pembayaran_id==-13){
            $mid = array("payment_id"=>$_SESSION['redpos_login']['company_id'].'-'.$penjualan_id);
            $this->db->where('penjualan_id',$penjualan_id);
            $this->db->update('penjualan',$mid);
        }
        foreach ($detailTransaksi as $value) {
            $stock_produk_id_ar = array();
            $stock_bahan_id_ar = array();
            $qty_stock = array();
            $qty_stock_bahan = array();
            if($value->produk_recipe_id == null) {
                $total_stock_belanja = $value->qty * $value->nilai_konversi;
                $row_stock = $this->resto_stock_by_location_produk_id($penjualan->lokasi_id, $value->produk_id);
                if($row_stock==null){
                    $jenis_produk_kode = $this->produk->produk_by_id($value->produk_id)->jenis_produk_kode;
                    $stock_produk_seri = date("my").$jenis_produk_kode.$lokasi_kode;
                    $urutan = $this->stock_produk->urutan_seri($stock_produk_seri);
                    $new_stock = array(
                        "produk_id"=>$value->produk_id,
                        "stock_produk_qty"=>0,
                        "stock_produk_lokasi_id"=>$penjualan->lokasi_id,
                        "stock_produk_seri"=>$stock_produk_seri,
                        "urutan"=>$urutan
                    );
                    $insert = $this->db->insert('stock_produk',$new_stock);
                    if($insert){
                        $last_id = $this->last_id();
                        $obj_stock = new \stdClass;
                        $obj_stock->stock_produk_id = $last_id;
                        $obj_stock->stock_produk_qty = 0;
                        $obj_stock->stock_produk_lokasi_id = $penjualan->lokasi_id;
                        $obj_stock->stock_produk_seri = $stock_produk_seri;
                        $obj_stock->urutan = $urutan;
                        $obj_stock->produk_id = $value->produk_id;
                        $row_stock = array();
                        $row_stock[]=$obj_stock;
                    }
                }
                $jumlah_bahan = $total_stock_belanja;
                foreach ($row_stock as $row) {
                    $data = array();

                    $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                    $stock_out = $jumlah_bahan;
                    if ($row->stock_produk_qty < $jumlah_bahan) {
                        $jumlah = 0;
                        $stock_out = $row->stock_produk_qty;
                    }
                    $jumlah_bahan = ($jumlah_bahan - $row->stock_produk_qty);
                    $data["stock_produk_qty"] = $jumlah;
                    $data["updated_at"] = date("Y-m-d H:i:s");
                    $data["staff_updated"] = $_SESSION["redpos_login"]["staff_id"];
                    $stock_produk_id = $row->stock_produk_id;
                    $this->db->where('stock_produk_id', $stock_produk_id);
                    $updateStok = $this->db->update('stock_produk', $data);
                    if ($updateStok) {
                        $data = array();
                        array_push($stock_produk_id_ar,$row->stock_produk_id);
                            array_push($qty_stock,$stock_out);
                        $data["tanggal"] = date("Y-m-d");
                        $data['penjualan_id'] = $penjualan_id;
                        $data["table_name"] = "stock_produk";
                        $data["stock_produk_id"] = $row->stock_produk_id;
                        $data["produk_id"] = $value->produk_id;
                        $data["stock_out"] = $stock_out;
                        $data["stock_in"] = 0;
                        $data["last_stock"] = $this->produk_last_stock($row->produk_id)->result;
                        $data["last_stock_total"] = $this->produk_stock_total()->result;
                        $data["keterangan"] = "Penjualan Produk no penjualan " . $penjualan->no_faktur;
                        $data["method"] = "update";
                        $data["created_at"] = date("Y-m-d H:i:s");
                        $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
                        $this->db->insert('arus_stock_produk', $data);
                    }
                    if ($jumlah_bahan <= 0) {
                        break;
                    }
                }
            }else{
                $this->db->where('produk_recipe_id',$value->produk_recipe_id);
                $produk_recipe_detail = $this->db->get('produk_recipe_detail')->result();
                foreach ($produk_recipe_detail as $recipe_detail_item){

                    $total_stock_belanja = $value->qty*$value->nilai_konversi * $recipe_detail_item->konversi_takaran;
                    $row_stock = $this->resto_stock_by_location_bahan_id($penjualan->lokasi_id, $recipe_detail_item->bahan_id);
                    if($row_stock==null){
                        $new_stock = array(
                            "bahan_id"=>$recipe_detail_item->bahan_id,
                            "stock_bahan_qty"=>0,
                            "stock_bahan_lokasi_id"=>$penjualan->lokasi_id,
                        );
                        $insert = $this->db->insert('stock_bahan',$new_stock);
                        if($insert){
                            $last_id = $this->last_id();
                            $obj_stock = new \stdClass;
                            $obj_stock->stock_bahan_id = $last_id;
                            $obj_stock->stock_bahan_qty = 0;
                            $obj_stock->stock_bahan_lokasi_id = $penjualan->lokasi_id;
                            $row_stock = array();
                            $row_stock[]=$obj_stock;
                        }
                    }
                    $jumlah_bahan = $total_stock_belanja;

                    foreach ($row_stock as $row) {

                        $data = array();
                        $jumlah = $row->stock_bahan_qty - $jumlah_bahan;
                        $stock_out = $jumlah_bahan;
                        if ($row->stock_bahan_qty < $jumlah_bahan) {
                            $jumlah = 0;
                            $stock_out = $row->stock_bahan_qty;
                        }
                        $jumlah_bahan = ($jumlah_bahan - $row->stock_bahan_qty);
                        $data["stock_bahan_qty"] = $jumlah;
                        $data["updated_at"] = date("Y-m-d H:i:s");
                        $data["staff_updated"] = $_SESSION["redpos_login"]["staff_id"];
                        $stock_bahan_id = $row->stock_bahan_id;
                        $this->db->where('stock_bahan_id', $stock_bahan_id);
                        $updateStok = $this->db->update('stock_bahan', $data);
                        if ($updateStok) {
                            $data = array();
                            array_push($stock_bahan_id_ar,$row->stock_bahan_id);
                            array_push($qty_stock_bahan,$stock_out);
                            $data["penjualan_id"] = $penjualan_id;
                            $data["tanggal"] = date("Y-m-d");
                            $data["table_name"] = "stock_bahan";
                            $data["stock_bahan_id"] = $row->stock_bahan_id;
                            $data["bahan_id"] = $recipe_detail_item->bahan_id;
                            $data["stock_out"] = $stock_out;
                            $data["stock_in"] = 0;
                            $data["last_stock"] = $this->bahan_last_stock($row->bahan_id)->result;
                            $data["last_stock_total"] = $this->bahan_stock_total()->result;
                            $data["keterangan"] = "Recipe Penjualan Produk no penjualan " . $penjualan->no_faktur;
                            $data["method"] = "update";
                            $data["created_at"] = date("Y-m-d H:i:s");
                            $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
                            $this->db->insert('arus_stock_bahan', $data);
                        }
                        if ($jumlah_bahan <= 0) {
                            break;
                        }

                    }
                }
            }
            $change = array();
            $change["stock_produk_id"] = implode("|",$stock_produk_id_ar);
            $change["stock_bahan_id"] = implode("|",$stock_bahan_id_ar);
            $change["qty_stock"] = implode("|",$qty_stock);
            $change["qty_stock_bahan"] = implode("|",$qty_stock_bahan);
            $this->db->where('penjualan_produk_id',$value->penjualan_produk_id);
            $this->db->update('penjualan_produk',$change);
        }
        $process = $this->result_trans();
        if($process){
            $return["result"] = true;
            $return["id"] = $penjualan_id;
        } else {
            $return["result"] = false;
        }
        return $return;

    }
    function resto_stock_by_location_produk_id($lokasi_id,$produk_id){
        $this->db->where('stock_produk_lokasi_id', $lokasi_id);
        $this->db->where('produk_id', $produk_id);
        $this->db->where('delete_flag', "0");
        return $this->db->get('stock_produk')->result();
    }
    function resto_stock_by_location_bahan_id($lokasi_id,$bahan_id){
        $this->db->where('stock_bahan_lokasi_id', $lokasi_id);
        $this->db->where('bahan_id', $bahan_id);
        $this->db->where('delete_flag', "0");
        return $this->db->get('stock_bahan')->result();
    }
    function produk_last_stock($produk_id){
        $this->db->select('sum(stock_produk.stock_produk_qty) as result');
        $this->db->from('stock_produk');
        $this->db->where('produk_id', $produk_id);
        $this->db->where('delete_flag', "0");
        return $this->db->get()->row();
    }
    function produk_stock_total(){
        $this->db->select('sum(stock_produk.stock_produk_qty) as result');
        $this->db->from('stock_produk');
        return $this->db->get()->row();
    }
    function bahan_last_stock($bahan_id){
        $this->db->select('sum(stock_bahan.stock_bahan_qty) as result');
        $this->db->from('stock_bahan');
        $this->db->where('bahan_id', $bahan_id);
        $this->db->where('delete_flag', "0");
        return $this->db->get()->row();
    }
    function bahan_stock_total(){
        $this->db->select('sum(stock_bahan.stock_bahan_qty) as result');
        $this->db->from('stock_bahan');
        return $this->db->get()->row();
    }
    // fungsi balioz untuk custom produk
	function custom_save(){
		$this->load->model('stock_produk','',true);
		$this->db->trans_begin();
		$data["lokasi_id"] = $this->input->post('lokasi_id');
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$data["lokasi_id"] = $_SESSION["redpos_login"]["lokasi_id"];
		}
		$transaksi = json_decode($this->input->post('transaksi'));
		$potongan = $transaksi->potongan;
		$tambahan = $transaksi->tambahan;
		$total_item = $transaksi->total_item;
		$grand_total = $transaksi->grand_total;
		$terbayar = $transaksi->terbayar;
		$kembalian = $terbayar - $grand_total;

		$data['urutan'] = $this->input->post('urutan');
		$data['no_faktur'] = $this->input->post('no_faktur');
		$data["nama_pelanggan"] = $this->input->post('guest_nama');
		$data["alamat_pelanggan"] = $this->input->post('guest_alamat');
		$data["telepon_pelanggan"] = $this->input->post('guest_telepon');
		$data["urutan"] = $this->input->post('urutan');
		$data["no_faktur"] = $this->input->post('no_faktur');
		$data["tanggal"] = date("Y-m-d");
		$data["pengiriman"] = $this->input->post('pengiriman');
		$data["total"] = $total_item;
		$data["biaya_tambahan"] = $tambahan;
		$data["potongan_akhir"] = $potongan;
		$data["grand_total"] = $grand_total;
		$data["terbayar"] = $terbayar;
		$data["sisa_pembayaran"] = 0;
		$data["additional_no"] = $this->input->post('additional_no');
		$data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d",strtotime($this->input->post('additional_tanggal'))) : "";
		$data["additional_keterangan"] = $this->input->post('additional_keterangan');
		$data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
		$data["kembalian"] = $kembalian;
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if($jenis_pembayaran == "kredit"){
			$data['status_pembayaran'] = "Hutang";
		} else {
			$data['status_pembayaran'] = "Lunas";
		}
		$this->db->insert('penjualan', $data);
		$penjualan_id = $this->db->insert_id();
		if($jenis_pembayaran == "kredit"){
			$data = array();
			$data['penjualan_id'] = $penjualan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan'));
			$data['tenggat_pelunasan'] = date("Y-m-d",$temp);
			$this->db->insert('piutang', $data);
			$piutang_id = $this->db->insert_id();
			if($terbayar > 0){

			}
		}
		if ($this->db->trans_status() === FALSE){
			$return["result"] = false;
			return $return;
		}

		$this->db->trans_commit();
		$return["result"] = true;
		$return["id"] = $penjualan_id;
		return $return;
	}
	// mendapatkan urutan faktur
	function getUrutan(){
		$date = date("Y-m-d");
		$this->db->select('max(urutan) as urutan');
		$this->db->where('tanggal', $date);
		$urutan = $this->db->get('penjualan')->row()->urutan;
		if($urutan == null){
			$urutan = 1;
		} else {
			$urutan++;
		}
		return $urutan;
	}
	//mendapatkan no faktur terbaru
	function getFakturCode(){
		$date = date("ymd");
		$urutan = $this->getUrutan();
		$lok = "00";
		if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			$lok = $_SESSION["redpos_login"]['lokasi_id'];
		}
		$tipe = "PL";
		$faktur = $tipe.'-'.$date.'/'.$lok.'/'.$urutan;
		return $faktur;
	}
	// jumlah data penjualan
	function transaksi_count_all(){
        $this->db->select('penjualan.*');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id','left');
		$this->db->where('status','process');
        if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
            $this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
        }
		return $this->db->count_all_results('penjualan');
	}
	// jumlah data penjualan terfilter
	function transaksi_count_filter($query){
        $this->db->select('penjualan.*');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id','left');
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&&$this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('penjualan.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
        if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
            $this->db->where('staff.staff_id', $this->input->get('staff_id'));
        }
        $this->db->where('status','process');

		$this->db->order_by('penjualan.penjualan_id', 'desc');
		//echo $this->db->_compile_select();		
		return $this->db->count_all_results('penjualan');
	}
	// list data penjualan
	function transaksi_list($start,$length,$query){

		$this->db->select('penjualan.*,lokasi.lokasi_nama,tipe_pembayaran.tipe_pembayaran_nama,(penjualan.grand_total - sum(penjualan_produk.qty*penjualan_produk.hpp)) as laba, (sum(penjualan_produk.qty*penjualan_produk.hpp)) as total_hpp,staff_nama');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id','left');
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&&$this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('penjualan.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
        if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
            $this->db->where('staff.staff_id', $this->input->get('staff_id'));
        }
        $this->db->where('status','process');
        $this->db->group_by('penjualan_id');
		$this->db->order_by('penjualan_id', 'desc');
		return $this->db->get('penjualan',$length,$start)->result();
	}
	// jumlah data order
    function order_count_all(){
        $this->db->select('penjualan.*');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
        $this->db->where('status','order');
        return $this->db->count_all_results('penjualan');
    }
    // jumlah data order terfilter
    function order_count_filter($query){
        $this->db->select('penjualan.*');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
        $this->db->group_start();
        $this->db->like('nama_pelanggan', $query, 'BOTH');
        $this->db->or_like('no_faktur', $query, 'BOTH');
        $this->db->or_like('tanggal', $query, 'BOTH');
        $this->db->or_like('lokasi_nama', $query, 'BOTH');

        $this->db->group_end();
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
            $this->db->like('no_faktur', $this->input->get('no_faktur'));
        }
        if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
            $this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
        }
        if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
            $this->db->where('staff.staff_id', $this->input->get('staff_id'));
        }
        $this->db->order_by('penjualan.penjualan_id', 'desc');
        //echo $this->db->_compile_select();
        $this->db->where('status','order');
        return $this->db->count_all_results('penjualan');
    }
    //list data order
    function order_list($start,$length,$query){
        $this->db->select('penjualan.*,lokasi.lokasi_nama,(penjualan.grand_total - sum(penjualan_produk.qty*penjualan_produk.hpp)) as laba, (sum(penjualan_produk.qty*penjualan_produk.hpp)) as total_hpp,staff_nama');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
        $this->db->group_start();
        $this->db->like('nama_pelanggan', $query, 'BOTH');
        $this->db->or_like('no_faktur', $query, 'BOTH');
        $this->db->or_like('tanggal', $query, 'BOTH');
        $this->db->or_like('lokasi_nama', $query, 'BOTH');

        $this->db->group_end();
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
            $this->db->like('no_faktur', $this->input->get('no_faktur'));
        }
        if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
            $this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
        }
        if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
            $this->db->where('staff.staff_id', $this->input->get('staff_id'));
        }
        $this->db->where('status','order');
        $this->db->group_by('penjualan.penjualan_id');
        $this->db->order_by('penjualan.penjualan_id', 'desc');
        return $this->db->get('penjualan',$length,$start)->result();
    }
    // mendapatkan staff mana saja yang telah melakukan penjualan
    function staff_penjualan(){
        $this->db->select('staff.staff_id,staff.staff_nama');
        $this->db->distinct();
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
        return $this->db->get('penjualan')->result();
    }
    // list transaksi by log kasir
	function transaksi_by_log($log_kasir_id){
		$this->db->select("if(sum(penjualan.grand_total) is null, 0, sum(penjualan.grand_total)) as total");
		$this->db->join('tipe_pembayaran','tipe_pembayaran on penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
		$this->db->from('penjualan');
		$this->db->where('tipe_pembayaran.kembalian', '1');
		$this->db->where('penjualan.log_kasir_id', $log_kasir_id);
		return $this->db->get()->row();
	}
	// total transaksi by log kasir
    function total_transaksi_by_log($log_kasir_id){
        $this->db->select("if(sum(penjualan.grand_total) is null, 0, sum(penjualan.grand_total)) as total");
        $this->db->join('tipe_pembayaran','tipe_pembayaran on penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
        $this->db->from('penjualan');
        $this->db->where('penjualan.log_kasir_id', $log_kasir_id);
        return $this->db->get()->row();
    }
    // jumlah transaksi by log kasir
    function jumlah_transaksi_by_log($log_kasir_id){
        $this->db->where('penjualan.log_kasir_id', $log_kasir_id);
        return $this->db->count_all_results('penjualan');
    }
    // info log kasir
	function log_kasir_info($log_kasir_id){
	    $text = 'log_kasir.*,"Master Admin" as staff_nama';
	    if($_SESSION['redpos_login']['staff_id']!=0){
            $this->db->join('staff','log_kasir.staff_id=staff.staff_id');
            $text = 'log_kasir.*,staff.staff_nama';
	    }
        $this->db->select($text);
	    $this->db->where('log_kasir_id',$log_kasir_id);
	    return $this->db->get('log_kasir')->row();
    }
	function detailTransaksi($id){
		$this->db->select("penjualan_produk.*,produk.*,produk_recipe.produk_recipe_id ");
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id','left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id','left');
        $this->db->join('produk_recipe', 'produk.produk_id = produk_recipe.produk_id','left');
		$this->db->where('penjualan_produk.penjualan_id', $id);
		return $this->db->get('penjualan_produk')->result();
	}
	function laporan_detail_transaksi_count_all(){
        $this->db->select('penjualan.*');
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');	
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
		return $this->db->count_all_results('penjualan');
	}
	function laporan_detail_transaksi_count_filter($query){
        $this->db->select('penjualan.*');
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
		$this->db->group_start();
			$this->db->like('nama_pelanggan', $query, 'BOTH');
			$this->db->or_like('no_faktur', $query, 'BOTH');
			$this->db->or_like('tanggal', $query, 'BOTH');
			$this->db->or_like('lokasi_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
			$this->db->or_like('qty', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['produk_id'])&&$this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
        if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
            $this->db->where('staff.staff_id', $this->input->get('staff_id'));
        }
		$this->db->order_by('penjualan.penjualan_id', 'desc');
		return $this->db->count_all_results('penjualan');
	}
	function laporan_detail_transaksi_list($start,$length,$query){
	    $text = 'penjualan.*,lokasi.lokasi_nama,produk.produk_kode,produk.produk_nama,penjualan_produk.qty,penjualan_produk.harga,penjualan_produk.sub_total,(penjualan_produk.sub_total - (penjualan_produk.qty*penjualan_produk.hpp)) as laba, (penjualan_produk.qty*penjualan_produk.hpp) as total_hpp,staff.staff_nama';
        $text = 'penjualan.*,satuan.satuan_nama,jenis_produk.jenis_produk_nama,lokasi.lokasi_nama,lokasi.lokasi_id,produk.produk_kode,produk.produk_nama,produk.produk_id,penjualan_produk.qty,penjualan_produk.harga,penjualan_produk.sub_total,(penjualan_produk.sub_total - (penjualan_produk.qty*penjualan_produk.hpp)) as laba, (penjualan_produk.qty*penjualan_produk.hpp) as total_hpp,staff.staff_nama,staff.staff_id';
		$this->db->select($text);
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');	
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('staff', 'log_kasir.staff_id = staff.staff_id');
		$this->db->group_start();
			$this->db->like('nama_pelanggan', $query, 'BOTH');
			$this->db->or_like('no_faktur', $query, 'BOTH');
			$this->db->or_like('tanggal', $query, 'BOTH');
			$this->db->or_like('lokasi_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
			$this->db->or_like('qty', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['produk_id'])&&$this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
        if(isset($_GET['staff_id'])&&$this->input->get('staff_id')!=""){
            $this->db->where('staff.staff_id', $this->input->get('staff_id'));
        }
        $this->db->where('staff.staff_id <>', 7);
		$this->db->order_by('penjualan.penjualan_id', 'desc');
		return $this->db->get('penjualan',$length,$start)->result();

	}
    function mostPenjualanDashboardList($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama,produk.produk_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'desc');
        return $this->db->get('penjualan_produk', 5, 0)->result();
    }
    function mostPenjualanDashboardCount($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'desc');
        return $this->db->count_all_results('penjualan_produk');
    }
    function leastPenjualanDashboardList($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama,produk.produk_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'asc');
        return $this->db->get('penjualan_produk', 5, 0)->result();
    }
    function leastPenjualanDashboardCount($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'asc');
        return $this->db->count_all_results('penjualan_produk');
    }
    function minTanggalPenjualan(){
        $this->db->select('min(tanggal) as tanggal');
        return $this->db->get('penjualan')->row()->tanggal;
    }
	function totalPenjualan(){
		$this->db->select('sum(penjualan.grand_total) as sales');
		$this->db->from('penjualan');
		return $this->db->get()->row()->sales;
	}
	function penjualanByMonth($search,$reseller_id){
		$this->db->select('sum(penjualan.grand_total) as sales, concat_ws(" ",MONTH(penjualan.tanggal),YEAR(penjualan.tanggal)) as "label"');
		$this->db->from('penjualan');
		$this->db->group_by('MONTH(penjualan.tanggal)');
		$this->db->group_by('YEAR(penjualan.tanggal)');
        if($reseller_id!=null){
            $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
            $this->db->where('lokasi_id',$lokasi_id);
         }
        $this->db->order_by('penjualan.tanggal');
		return $this->db->get('')->result();
	}
	function penjualan_by_id($id){
        $this->db->select('penjualan.*, bank.nama_bank, reseller.bank_rekening');
        $this->db->join('log_kasir', 'penjualan.log_kasir_id = log_kasir.log_kasir_id');
        $this->db->join('mykindofbeauty_master.login', 'log_kasir.user_id = mykindofbeauty_master.login.id');
        $this->db->join('reseller', 'mykindofbeauty_master.login.reseller_id = reseller.reseller_id');
        $this->db->join('bank', 'reseller.bank_id = bank.bank_id');
		$this->db->from('penjualan');
		$this->db->where("penjualan.penjualan_id",$id);
		return $this->db->get()->row();
	}
	function normalisasi(){
	    $this->db->select('penjualan_produk_id,hpp,produk.hpp_global,produk.produk_id');
	    $this->db->distinct();
	    $this->db->join('produk','penjualan_produk.produk_id = produk.produk_id');
	    $this->db->where('penjualan_produk.hpp != produk.hpp_global ');
	    return $this->db->get('penjualan_produk')->result();
    }
    function update_penjualan_produk($produk_id,$data){
	    $this->db->where('produk_id',$produk_id);
	    return $this->db->update('penjualan_produk',$data);
    }
    function penjualan_harian_list($start,$length,$query){
	    $this->db->select('tanggal,count(penjualan_id)as jumlah_transaksi,sum(grand_total) as total_transaksi, sum(if(tipe_pembayaran.jenis_pembayaran="kas",penjualan.grand_total,0)) as total_tunai,sum(if(tipe_pembayaran.jenis_pembayaran="kredit",penjualan.grand_total,0)) as total_kredit,sum(if(tipe_pembayaran.jenis_pembayaran="k_debet",penjualan.grand_total,0)) as total_k_debet,sum(if(tipe_pembayaran.jenis_pembayaran="k_kredit",penjualan.grand_total,0)) as total_k_kredit');
        $this->db->join('tipe_pembayaran','penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
            $this->db->or_having('tanggal ',$query);
            $num = $this->string_to_decimal($query);
            $this->db->or_having('total_transaksi LIKE "%'.$num.'%"');
            $this->db->or_having('total_tunai LIKE "%'.$num.'%"');
            $this->db->or_having('total_kredit LIKE "%'.$num.'%"');
            $this->db->or_having('total_k_debet LIKE "%'.$num.'%"');
            $this->db->or_having('total_k_kredit LIKE "%'.$num.'%"');
	    $this->db->group_by('tanggal');
	    return $this->db->get('penjualan',$length,$start)->result();
    }
    function penjualan_harian_filter($query){
        $this->db->select('tanggal,count(penjualan_id)as jumlah_transaksi,sum(grand_total) as total_transaksi, sum(if(tipe_pembayaran.jenis_pembayaran="kas",penjualan.grand_total,0)) as total_tunai,sum(if(tipe_pembayaran.jenis_pembayaran="kredit",penjualan.grand_total,0)) as total_kredit,sum(if(tipe_pembayaran.jenis_pembayaran="k_debet",penjualan.grand_total,0)) as total_k_debet,sum(if(tipe_pembayaran.jenis_pembayaran="k_kredit",penjualan.grand_total,0)) as total_k_kredit');
        $this->db->join('tipe_pembayaran','penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->or_having('tanggal ',$query);
        $num = $this->string_to_decimal($query);
        $this->db->or_having('total_transaksi LIKE "%'.$num.'%"');
        $this->db->or_having('total_tunai LIKE "%'.$num.'%"');
        $this->db->or_having('total_kredit LIKE "%'.$num.'%"');
        $this->db->or_having('total_k_debet LIKE "%'.$num.'%"');
        $this->db->or_having('total_k_kredit LIKE "%'.$num.'%"');
        $this->db->group_by('tanggal');
        return $this->db->count_all_results('penjualan');
    }
    function penjualan_harian_all(){
        $this->db->select('tanggal,count(penjualan_id)as jumlah_transaksi,sum(grand_total) as total_transaksi, sum(if(tipe_pembayaran.jenis_pembayaran="kas",penjualan.grand_total,0)) as total_tunai,sum(if(tipe_pembayaran.jenis_pembayaran="kredit",penjualan.grand_total,0)) as total_kredit,sum(if(tipe_pembayaran.jenis_pembayaran="k_debet",penjualan.grand_total,0)) as total_k_debet,sum(if(tipe_pembayaran.jenis_pembayaran="k_kredit",penjualan.grand_total,0)) as total_k_kredit');
        $this->db->join('tipe_pembayaran','penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
        $this->db->group_by('tanggal');
        return $this->db->count_all_results('penjualan');
    }
    function penjualan_detail_by_array_id($id){
	    $this->db->select('penjualan_produk.*,produk.produk_nama');
	    $this->db->join('produk','penjualan_produk.produk_id=produk.produk_id');
	    $this->db->where_in('penjualan_produk_id',$id);
	    return $this->db->get('penjualan_produk')->result();
    }
    function cancel_penjualan_item($id){
	    $this->db->where_in('penjualan_produk_id',$id);
	    return $this->db->delete('penjualan_produk');
    }
    function delete_arus_stock_penjualan($penjualan_id){
        $this->db->where('penjualan_id',$penjualan_id);
        return $this->db->delete('arus_stock_produk');
    }
    function delete_arus_stock_penjualan_bahan($penjualan_id){
        $this->db->where('penjualan_id',$penjualan_id);
        return $this->db->delete('arus_stock_bahan');
    }
    function penjualan_perbulan($lokasi_id,$reseller_id){
	    $start_tanggal = date('Y-m-01');
        $end_tanggal = date('Y-m-t');
	    $this->db->select('if(sum(penjualan.grand_total) is null,0,sum(penjualan.grand_total)) as qty');
        $this->db->where('penjualan.lokasi_id',$lokasi_id);
	    $this->db->where('penjualan.tanggal >=',$start_tanggal);
        $this->db->where('penjualan.tanggal <=',$end_tanggal);
	    $penjualan = $this->db->get('penjualan')->row()->qty;

        $this->db->select('if(sum(terpakai) is null,0,sum(terpakai)) as qty');
        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('tanggal >=',$start_tanggal);
        $this->db->where('tanggal <=',$end_tanggal);
        $terpakai = $this->db->get('reward_reseller')->row()->qty;

        $result = $penjualan - $terpakai;
	    return $result;
    }

}

/* End of file Pos.php */
/* Location: ./application/models/Pos.php */
