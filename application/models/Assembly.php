<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assembly extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "assembly";
    }

    function check_produk_kode($produk_kode){
        $this->db->select('produk_kode');
        $this->db->where('produk_kode',$produk_kode);      
        $query =$this->db->get('produk');
        $row = $query->row();
        if ($row!=null){
            return $row->produk_kode; 
        }else{
             return "";
        }
    }
    function get_unit_satuan($idproduk) {
    	$this->db->select('unit.*,satuan.satuan_nama');
        $this->db->join('satuan','satuan.satuan_id = unit.satuan_id');
       	$this->db->where('produk_id',$idproduk);
        return $this->db->get('unit')->result();
    }
    function insert_assembly(){
        $this->db->trans_begin();

        $data["produk_kode"] = $this->input->post('produk_kode');
        $data["barcode"] = $this->input->post('barcode');
        $data["produk_nama"] = $this->input->post('produk_nama');
        $data["produk_satuan_id"] = $this->input->post('produk_satuan_id');
        $data["produk_jenis_id"] = $this->input->post('produk_jenis_id');
        $data["produk_minimal_stock"] = $this->string_to_number($this->input->post('produk_minimal_stock'));
        $data["harga_eceran"] = $this->string_to_number($this->input->post('harga_eceran'));
        $data["hpp_global"] = $this->string_to_number($this->input->post('hpp_global'));
        $this->db->insert('produk', $data);

        $produk_id = $this->db->insert_id();
        $data_assembly['produk_id'] = $produk_id;
        $this->db->insert('assembly', $data_assembly);

        $assembly_id = $this->db->insert_id();
        $item = $this->input->post('item');
        $transaksi = json_decode($this->input->post('transaksi'));
        foreach ($transaksi->item as $key) {
            $data = array();
            $data["assembly_id"] = $assembly_id;
            $data["produk_id"] =  $key->produk_id;
            $data["jumlah"] = $key->jumlah;
            $data["unit_id"] = $key->unit_id;
            $data["satuan_id_unit"] = $key->satuan_id_unit;
            $this->db->insert('assembly_item', $data);
        }

        if ($this->db->trans_status() === FALSE)
            return FALSE;

        $this->db->trans_commit();
        return TRUE;            
    }
    function edit_assembly(){
        $this->db->trans_begin();

        $assembly_id = $this->input->post('assembly_id');
        $this->db->where('assembly_id', $assembly_id);
        $this->db->delete('assembly_item');

        $produk_id = $this->input->post('produk_id');
        $data["produk_kode"] = $this->input->post('produk_kode');
        $data["barcode"] = $this->input->post('barcode');
        $data["produk_nama"] = $this->input->post('produk_nama');
        $data["produk_satuan_id"] = $this->input->post('produk_satuan_id');
        $data["produk_jenis_id"] = $this->input->post('produk_jenis_id');
        $data["produk_minimal_stock"] = $this->string_to_number($this->input->post('produk_minimal_stock'));
        $data["harga_eceran"] = $this->string_to_number($this->input->post('harga_eceran'));
        $data["hpp_global"] = $this->string_to_number($this->input->post('hpp_global'));
        $this->db->where('produk_id', $produk_id);
        $this->db->update('produk', $data);
        $item = $this->input->post('item');
        $item_assembly = $this->input->post('item_assembly');
        if (count($item_assembly) > 0) {
            foreach ($item_assembly as $key) {
                $data = array();
                $data["assembly_id"] = $assembly_id;
                $data["produk_id"] =  $key["produk_id"];
                $data["jumlah"] = $this->string_to_number($key["jumlah"]);
                $data["unit_id"] = $key["unit_id"];
                $data["satuan_id_unit"] = $key["satuan_id_unit"];
                $this->db->insert('assembly_item', $data);
            }
        }
        
        $transaksi = json_decode($this->input->post('transaksi'));
        if ($transaksi != null) {
            foreach ($transaksi->item as $key) {
                $data = array();
                $data["assembly_id"] = $assembly_id;
                $data["produk_id"] =  $key->produk_id;
                $data["jumlah"] = $key->jumlah;
                $data["unit_id"] = $key->unit_id;
                $data["satuan_id_unit"] = $key->satuan_id_unit;
                $this->db->insert('assembly_item', $data);
            }
        }
        if ($this->db->trans_status() === FALSE)
            return FALSE;

        $this->db->trans_commit();
        return TRUE;            
    }
    function produk_by_id($produk_id){
        $this->db->where('produk.produk_id', $produk_id);
        $this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
        $this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
        $this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
        $sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
            $this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
            
        }
        $this->db->select($sql);        
        return $this->db->get('produk')->row();
    }
    function get_id_assembly($produk_id){
        $this->db->select('*');
        $this->db->where('produk_id', $produk_id);
        return $this->db->get('assembly')->row_array();
    }
    function assembly_detail_by_id($assembly_id){
        $this->db->where('assembly_item.assembly_id', $assembly_id);
        $this->db->select('assembly_item.*,produk.*,satuan.*');
        $this->db->join('produk', 'produk.produk_id=assembly_item.produk_id');
        $this->db->join('satuan', 'satuan.satuan_id=produk.produk_satuan_id');
        return $this->db->get('assembly_item')->result();
    }
    function stock_perlokasi($produk_id){
        $this->db->select('stock_produk.*,lokasi_nama');
        $this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
        $this->db->where('stock_produk.produk_id', $produk_id);
        $this->db->where('delete_flag', "0"); 
        $this->db->where('stock_produk_qty >', "0"); 
        return $this->db->get('stock_produk')->result();
    }
    function assembly_item_produk_count($assembly_id){
        $this->db->where('assembly_item.assembly_id', $assembly_id);
        $this->db->select('count(assembly_item_id) as qty');
        $this->db->join('produk', 'produk.produk_id=assembly_item.produk_id');
        $this->db->join('satuan', 'satuan.satuan_id=produk.produk_satuan_id');
        return $this->db->get('assembly_item')->num_rows();
    }
    function item_list($start,$length,$query,$assembly_id){
        $this->db->where('assembly_item.assembly_id', $assembly_id);
        $this->db->select('assembly_item.*,produk.*,satuan.*');
        $this->db->join('produk', 'produk.produk_id=assembly_item.produk_id');
        $this->db->join('satuan', 'satuan.satuan_id=produk.produk_satuan_id');
        $this->db->group_start();
            $this->db->like('assembly_item.jumlah', $query, 'BOTH');
            $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $this->db->group_end();
       return $this->db->get('assembly_item', $length, $start)->result();
    }
}

/* End of file Assembly.php */
/* Location: ./application/models/Assembly.php */