<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_bulanan extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "rekap_bulanan_new";
    }
    //act -> total_po -> untuk menghitung jumlah po selama sebulan
    //
    function omset_group($reseller_id,$waktu_start,$waktu_end){

        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('waktu >=',$waktu_start);
        $this->db->where('waktu <=',$this->config->item('reward-static-start'));

        $sql = 'if(sum(total_po) is null,0,sum(total_po)) as total_po,'.
                'if(sum(total_omzet_reseller) is null,0,sum(total_omzet_reseller)) as total_omzet_reseller,'.
                'if(sum(withdraw_cash) is null,0,sum(withdraw_cash)) as withdraw_cash,'.
                'if(sum(claimed) is null,0,sum(claimed)) as claimed'
                ;
        $this->db->select($sql);
        $record = $this->db->get('rekap_bulanan_new')->row();
        $omset1 = 0;
        if($record!=null) {
            $omset1 = $record->total_po + $record->total_omzet_reseller - $record->withdraw_cash - $record->claimed;
        }

        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('waktu >',$this->config->item('reward-static-start'));

        $sql = 'if(sum(total_po) is null,0,sum(total_po)) as total_po,'.
            'if(sum(total_omzet_reseller) is null,0,sum(total_omzet_reseller)) as total_omzet_reseller,'.
            'if(sum(withdraw_cash) is null,0,sum(withdraw_cash)) as withdraw_cash,'.
            'if(sum(claimed) is null,0,sum(claimed)) as claimed'
        ;
        $this->db->select($sql);
        $record = $this->db->get('rekap_bulanan_new')->row();
        $omset2 = 0;
        if($record!=null) {
            $omset2 =  $record->total_omzet_reseller - $record->withdraw_cash - $record->claimed;
        }
        return $omset1+$omset2;

    }
    function rekap_bulanan_reseller_all($reseller_id,$waktu_start,$waktu_end){
        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('waktu >=',$waktu_start);
        $this->db->where('waktu <=',$waktu_end);
        $this->db->group_by('reseller_id');
        $this->db->group_by('year');
        $this->db->group_by('month');
        return $this->db->count_all_results('rekap_bulanan_new');
    }
    function rekap_bulanan_reseller_filter($query,$reseller_id,$waktu_start,$waktu_end){
        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('waktu >=',$waktu_start);
        $this->db->where('waktu <=',$waktu_end);
        $this->db->group_by('reseller_id');
        $this->db->group_by('year');
        $this->db->group_by('month');
        return $this->db->count_all_results('rekap_bulanan_new');
    }
    function rekap_bulanan_reseller_list($start,$length,$query,$reseller_id,$waktu_start,$waktu_end){

        $this->db->select('rekap_bulanan_id,rekap_bulanan_new.year,rekap_bulanan_new.month,if(sum(total_po) is null,0,sum(total_po)) as total_po,if(sum(total_omzet_reseller) is null,0,sum(total_omzet_reseller)) as total_omset_reseller, if(sum(claimed) is null,0,sum(claimed)) as claimed');
        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('waktu >=',$waktu_start);
        $this->db->where('waktu <=',$waktu_end);
        $this->db->where('claim_reward_id is null',null,false);
        $this->db->group_by('reseller_id');
        $this->db->group_by('year');
        $this->db->group_by('month');
        $this->db->order_by('reseller_id');
        $this->db->order_by('year');
        $this->db->order_by('month');
        $this->db->order_by('rekap_bulanan_id','desc');
        return ( $this->db->get('rekap_bulanan_new')->result());
    }
    function claimed($reseller_id,$month,$year){

    }
    function total_po($reseller_id,$tanggal){
        $sql = 'call total_po_new('.$reseller_id.',"'.$tanggal.'")';
        $this->db->query($sql);
    }
    function total_po_new($reseller_id,$tanggal){
        $bulan = number_format(date("m",strtotime($tanggal)));
        $tahun = date("Y",strtotime($tanggal));
        $this->db->select('sum(po_produk.grand_total) as total');
        $this->db->join('mykindofbeauty_master.login','mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller');
        $this->db->join('reseller','mykindofbeauty_master.login.reseller_id = reseller._reseller_id');
        $this->db->where('reseller.type',$reseller_id);
    }
    function total_omzet_reseller($reseller_id,$tanggal){
        $sql = 'call total_omzet_reseller_new('.$reseller_id.',"'.$tanggal.'")';
        $this->db->query($sql);
    }

    function rekap_omset_group_all(){
        $this->db->select('reseller.nama');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->group_by('rekap_bulanan_new.reseller_id');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
            $start_date = date("Y-m-01",strtotime($start_date));

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('rekap_bulanan_new.waktu >=',($start_date));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $end_date = date("Y-m-t",strtotime($end_date));
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('rekap_bulanan_new.waktu <=',($end_date));

        }
        $this->db->group_start();
        $this->db->or_where('reseller.type','super agen');
        $this->db->or_where('reseller.type','shareholder');
        $this->db->group_end();
        $this->db->where('reseller.type','super agen');
        return $this->db->count_all_results('rekap_bulanan_new');
    }
    function rekap_omset_group_filter($filter){
        $this->db->select('reseller.nama');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->like('reseller.nama',$filter);
        $this->db->group_by('rekap_bulanan_new.reseller_id');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
            $start_date = date("Y-m-01",strtotime($start_date));

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('rekap_bulanan_new.waktu >=',($start_date));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $end_date = date("Y-m-t",strtotime($end_date));
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('rekap_bulanan_new.waktu <=',($end_date));

        }
        $this->db->group_start();
        $this->db->or_where('reseller.type','super agen');
        $this->db->or_where('reseller.type','shareholder');
        $this->db->group_end();
        $this->db->where('reseller.type','super agen');
        return $this->db->count_all_results('rekap_bulanan_new');
    }
    function rekap_omset_group_list($start,$length,$filter){
        $this->db->select('reseller.reseller_id,reseller.nama,if(sum(rekap_bulanan_new.total_po)is null,0,sum(rekap_bulanan_new.total_po))+if(sum(rekap_bulanan_new.total_omzet_reseller) is null,0,sum(rekap_bulanan_new.total_omzet_reseller)) as total');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->like('reseller.nama',$filter);
        $this->db->group_by('rekap_bulanan_new.reseller_id');

        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
            $start_date = date("Y-m-01",strtotime($start_date));

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('rekap_bulanan_new.waktu >=',($start_date));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $end_date = date("Y-m-t",strtotime($end_date));
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('rekap_bulanan_new.waktu <=',($end_date));

        }
        $this->db->group_start();
        $this->db->or_where('reseller.type','super agen');
        $this->db->or_where('reseller.type','shareholder');
        $this->db->group_end();
        $this->db->order_by('total','desc');
        return $this->db->get('rekap_bulanan_new',$length,$start)->result();
    }
    function total_omset_leader(){
        $this->db->select('sum(po_produk.grand_total) as total');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
            $this->db->where('po_produk.tanggal_pemesanan >=',$start_date);
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $this->db->where('po_produk.tanggal_pemesanan <=',$end_date);
        }
        $this->db->where('om_leader IS NOT NULL', null, false);
        $this->db->where('po_produk.status_pembayaran','Lunas');
        $data = $this->db->get('po_produk')->row();
        if($data==null){
            return 0;
        }else{
            return $data->total;
        }
    }
    function total_omset_reward(){
        $date = date("Y-m-01");
        $date_time = new DateTime($date);
        $date_time = $date_time->modify("-2 month");
        $start = $date_time->format("Y-m-d");
        $this->db->select('sum(total_po+total_omzet_reseller-claimed) as grand_total');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->where('waktu >=',$start);
        $this->db->where('reseller.demo',0);
        $data = $this->db->get("rekap_bulanan_new")->row();
        if($data == null){
            return 0;
        }else {
            return $data->grand_total;
        }

    }

    function report_rekap_omset_group_all(){
        $this->db->select('reseller.nama');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->group_by('rekap_bulanan_new.reseller_id');
        $this->db->group_by('rekap_bulanan_new.month');
        $this->db->group_start();
        $this->db->or_where('reseller.type','super agen');
        $this->db->or_where('reseller.type','shareholder');
        $this->db->group_end();
        return $this->db->count_all_results('rekap_bulanan_new');
    }
    function report_rekap_omset_group_filter($filter){
        $this->db->select('reseller.nama');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->like('reseller.nama',$filter);
        $this->db->group_by('rekap_bulanan_new.reseller_id');
        $this->db->group_by('rekap_bulanan_new.month');

        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('rekap_bulanan_new.month >=',($month_start));
            $this->db->where('rekap_bulanan_new.year >=',($year_start));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('rekap_bulanan_new.month <=',($month_end));
            $this->db->where('rekap_bulanan_new.year <=',($year_end));
        }
        $this->db->group_start();
        $this->db->or_where('reseller.type','super agen');
        $this->db->or_where('reseller.type','shareholder');
        $this->db->group_end();
        return $this->db->count_all_results('rekap_bulanan_new');
    }
    function report_rekap_omset_group_list($start,$length,$filter){
        $this->db->select('reseller.reseller_id,reseller.nama,rekap_bulanan_new.month,if(sum(rekap_bulanan_new.total_po)is null,0,sum(rekap_bulanan_new.total_po))+if(sum(rekap_bulanan_new.total_omzet_reseller) is null,0,sum(rekap_bulanan_new.total_omzet_reseller)) as total');
        $this->db->join('reseller','reseller.reseller_id = rekap_bulanan_new.reseller_id');
        $this->db->like('reseller.nama',$filter);
        $this->db->group_by('rekap_bulanan_new.reseller_id');
        $this->db->group_by('rekap_bulanan_new.month');

        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('rekap_bulanan_new.month >=',($month_start));
            $this->db->where('rekap_bulanan_new.year >=',($year_start));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('rekap_bulanan_new.month <=',($month_end));
            $this->db->where('rekap_bulanan_new.year <=',($year_end));
        }
        $this->db->group_start();
        $this->db->or_where('reseller.type','super agen');
        $this->db->or_where('reseller.type','shareholder');
        $this->db->group_end();
        $this->db->order_by('reseller.reseller_id','asc');
        $this->db->order_by('rekap_bulanan_new.month','asc');
        return $this->db->get('rekap_bulanan_new',$length,$start)->result();
    }

    function report_rekap_omset_group_real_all(){
        $this->db->select('reseller.nama');
        $this->db->join('reseller','reseller.reseller_id = po_produk.om_leader');
        $this->db->group_by('reseller.reseller_id');
        $this->db->group_by('month(po_produk.updated_at)');
        $this->db->group_by('year(po_produk.updated_at)');
        $this->db->order_by('reseller.reseller_id','asc');
        $this->db->order_by('month(po_produk.updated_at)','asc');
        $this->db->order_by('year(po_produk.updated_at)','asc');
        return $this->db->count_all_results('po_produk');
    }
    function report_rekap_omset_group_real_filter($filter){
        $this->db->select('reseller.nama');
        $this->db->join('reseller','reseller.reseller_id = po_produk.om_leader');
        $this->db->like('reseller.nama',$filter);
        $this->db->group_by('reseller.reseller_id');
        $this->db->group_by('month(po_produk.updated_at)');
        $this->db->group_by('year(po_produk.updated_at)');

        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('month(po_produk.updated_at) >=',($month_start));
            $this->db->where('year(po_produk.updated_at) >=',($year_start));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('month(po_produk.updated_at) <=',($month_end));
            $this->db->where('year(po_produk.updated_at) <=',($year_end));
        }
        $this->db->order_by('reseller.reseller_id','asc');
        $this->db->order_by('month(po_produk.updated_at)','asc');
        $this->db->order_by('year(po_produk.updated_at)','asc');
        return $this->db->count_all_results('po_produk');
    }
    function report_rekap_omset_group_real_list($start,$length,$filter){
        $this->db->select('reseller.nama,month(po_produk.updated_at) as month,YEAR(po_produk.updated_at) as tahun,sum(po_produk.grand_total) as total ');
        $this->db->join('reseller','reseller.reseller_id = po_produk.om_leader');
        $this->db->like('reseller.nama',$filter);
        $this->db->group_by('reseller.reseller_id');
        $this->db->group_by('month(po_produk.updated_at)');
        $this->db->group_by('year(po_produk.updated_at)');

        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");

            $month_start = number_format(date('m',strtotime($start_date)));
            $year_start = (date('Y',strtotime($start_date)));
            $this->db->where('month(po_produk.updated_at) >=',($month_start));
            $this->db->where('year(po_produk.updated_at) >=',($year_start));
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
            $month_end = number_format(date('m',strtotime($end_date)));
            $year_end = (date('Y',strtotime($end_date)));
            $this->db->where('month(po_produk.updated_at) <=',($month_end));
            $this->db->where('year(po_produk.updated_at) <=',($year_end));
        }
        $this->db->order_by('reseller.reseller_id','asc');
        $this->db->order_by('month(po_produk.updated_at)','asc');
        $this->db->order_by('year(po_produk.updated_at)','asc');
        return $this->db->get('po_produk',$length,$start)->result();
    }

    function rekap_aktivitas_group($start,$length,$query,$reseller_id){
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
        }
        $b = '(SELECT reseller.referal_id,count(reseller.reseller_id) as total_downline
from reseller
where STATUS = "active"
and referal_id is not null
and first_deposit_date >= "'.$start_date.'"
and first_deposit_date <= "'.$end_date.'"
GROUP BY referal_id)';
        $c = '(SELECT reseller.reseller_id,sum(bonus.jumlah_bonus) as jumlah_bonus
FROM bonus
INNER JOIN reseller on bonus.to_reseller_id = reseller.reseller_id
WHERE bonus.STATUS = "Terkirim"
and tanggal_pengiriman >= "'.$start_date.'"
and tanggal_pengiriman <= "'.$end_date.'"
GROUP BY bonus.to_reseller_id)';
        $d = '(SELECT reseller.reseller_id,sum(po_produk.grand_total) as total_po from po_produk
INNER join mykindofbeauty_master.login on mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on mykindofbeauty_master.login.reseller_id = reseller.reseller_id
where po_produk.status_pembayaran = "Lunas"
and po_produk.tanggal_uang_diterima >= "'.$start_date.' 00:00:00"
and po_produk.tanggal_uang_diterima <= "'.$end_date.' 23:59:59"
GROUP BY reseller.reseller_id)';
        $this->db->select('reseller.reseller_id,reseller.nama,if(b.total_downline is null,0,b.total_downline) as total_downline,
if(c.jumlah_bonus is null, 0, c.jumlah_bonus) as jumlah_bonus,
if(d.total_po is null,0,d.total_po) as total_po');
        $this->db->join($b.' as b','b.referal_id = reseller.reseller_id','left');
        $this->db->join($c.' as c','c.reseller_id = reseller.reseller_id','left');
        $this->db->join($d.' as d','d.reseller_id = reseller.reseller_id','left');
        $this->db->where('leader',$reseller_id);
        $this->db->where('leader != reseller.reseller_id','',false);
        $this->db->where('reseller.status','active');
        $this->db->group_start();
            $this->db->or_like('reseller.nama',$query);
        $this->db->group_end();
        if($this->input->get('ordered')!=""){
            $dir = $this->input->get('direct');
            $this->db->order_by($this->input->get('ordered'),$dir);
        }
        return $this->db->get('reseller',$length,$start)->result();

    }
    function rekap_aktivitas_group_filter($query,$reseller_id){
        $this->db->where('leader',$reseller_id);
        $this->db->where('leader != reseller.reseller_id','',false);
        $this->db->where('reseller.status','active');
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function rekap_aktivitas_group_all($reseller_id){
        $this->db->where('leader',$reseller_id);
        $this->db->where('leader != reseller.reseller_id','',false);
        $this->db->where('reseller.status','active');
        return $this->db->count_all_results('reseller');
    }

    function rekap_performa_supres_level($start,$length,$query,$reseller_id){
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');
        if($this->input->get("start_date")!=""){
            $start_date = $this->input->get("start_date");
        }
        if($this->input->get("end_date")!=""){
            $end_date = $this->input->get("end_date");
        }
        $a = '(SELECT reseller.reseller_id,sum(po_produk.grand_total) as total_po from po_produk
INNER join mykindofbeauty_master.login on mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on mykindofbeauty_master.login.reseller_id = reseller.reseller_id
where po_produk.status_pembayaran = "Lunas"
and po_produk.tanggal_pemesanan >= "'.$start_date.'"
and po_produk.tanggal_pemesanan <= "'.$end_date.'"
GROUP BY reseller.reseller_id)';
        $b = '(SELECT reseller.reseller_id,count(voucher_produk.voucher_produk_id) as jumlah_penukaran from voucher_produk
INNER JOIN reseller on reseller.reseller_id = voucher_produk.reseller_penukar
where voucher_produk.tanggal_penukaran >= "'.$start_date.'"
and voucher_produk.tanggal_penukaran <= "'.$end_date.'"
GROUP BY reseller.reseller_id)';
        $c = '(SELECT reseller.reseller_id, count(voucher.reseller_id) as jumlah_withdraw from reseller
INNER JOIN
(SELECT hutang_produk.reseller_id,pembayaran_hutang_produk_id,tanggal_pelunasan from hutang_produk
WHERE pembayaran_hutang_produk_id is not null
and tanggal_pelunasan is not null
GROUP BY pembayaran_hutang_produk_id) as voucher on reseller.reseller_id = voucher.reseller_id
where voucher.tanggal_pelunasan >= "'.$start_date.'"
and voucher.tanggal_pelunasan <= "'.$end_date.'"
GROUP BY reseller.reseller_id)';
        $this->db->select('reseller.reseller_id,reseller.nama,
if(a.total_po is NULL,0,a.total_po) as total_po,
if(b.jumlah_penukaran is null,0,b.jumlah_penukaran) as jumlah_penukaran,
if(c.jumlah_withdraw is null,0,c.jumlah_withdraw) as jumlah_withdraw');
        $this->db->join($a.' as a','a.reseller_id = reseller.reseller_id','left');
        $this->db->join($b.' as b','b.reseller_id = reseller.reseller_id','left');
        $this->db->join($c.' as c','c.reseller_id = reseller.reseller_id','left');
        $this->db->where('referal_id',$reseller_id);
        $this->db->where('type','super agen');
        $this->db->where('status','active');
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->group_end();
        if($this->input->get('ordered')!=""){
            $dir = $this->input->get('direct');
            $this->db->order_by($this->input->get('ordered'),$dir);
        }
        return $this->db->get('reseller',$length,$start)->result();
    }
    function rekap_performa_supres_level_filter($query,$reseller_id){
        $this->db->where('referal_id',$reseller_id);
        $this->db->where('type','super agen');
        $this->db->where('status','active');
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function rekap_performa_supres_level_all($reseller_id){
        $this->db->where('referal_id',$reseller_id);
        $this->db->where('type','super agen');
        $this->db->where('status','active');
        return $this->db->count_all_results('reseller');
    }
    function get_redeposit($reseller_id,$grand_total,$po_produk_id){
        $sql = 'call change_total_deposit('.$reseller_id.','.$grand_total.','.$po_produk_id.')';
        $this->db->query($sql);
    }
}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
