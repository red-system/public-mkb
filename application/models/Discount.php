<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "discount";
    }
    function list($start,$length,$query){
        return $this->db->get('discount', $length, $start)->result();
    }
    function count_all(){
        return $this->db->count_all_results('discount');
    }
    function count_filter($query){

        return $this->db->count_all_results('discount');
    }
    function produk_add_count_all(){
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        return $this->db->count_all_results('produk');
    }
    function produk_add_count_filter($query){
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        $this->db->group_start();
        $this->db->or_like('produk_nama',$query,'BOTH');
        $this->db->or_like('jenis_produk_nama',$query,'BOTH');
        $this->db->or_like('satuan_nama',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('produk');
    }
    function produk_add_list($start,$length,$query){
        $sql = 'produk_id,produk_nama,jenis_produk_nama,satuan_nama,produk_kode';
        if(isset($_SESSION['redpos_discount']['produk_id'])){
            $sql .= ',if("'.$_SESSION['redpos_discount']['produk_id'].'" like CONCAT("%",CONCAT("|",produk.produk_id,"|"),"%"),0,1)as urutan';
        }
        $this->db->select($sql);
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        $this->db->group_start();
        $this->db->or_like('produk_nama',$query,'BOTH');
        $this->db->or_like('jenis_produk_nama',$query,'BOTH');
        $this->db->or_like('satuan_nama',$query,'BOTH');
        $this->db->group_end();
        if(isset($_SESSION['redpos_discount']['produk_id'])){
            $this->db->order_by('urutan');
        }else{
            $this->db->order_by('produk.produk_id');
        }
        return $this->db->get('produk',$length,$start)->result();
    }
    function produk_detail_count_all(){
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('discount','discount.produk_id like concat("%",concat("|",produk.produk_id,"|"),"%")');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        if(isset($_SESSION['redpos_discount']['discount_id'])){
            $this->db->where('discount_id',$_SESSION['redpos_discount']['discount_id']);
        }
        return $this->db->count_all_results('produk');
    }
    function produk_detail_count_filter($query){
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('discount','discount.produk_id like concat("%",concat("|",produk.produk_id,"|"),"%")');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        $this->db->group_start();
        $this->db->or_like('produk_nama',$query,'BOTH');
        $this->db->or_like('jenis_produk_nama',$query,'BOTH');
        $this->db->or_like('satuan_nama',$query,'BOTH');
        $this->db->group_end();
        if(isset($_SESSION['redpos_discount']['discount_id'])){
            $this->db->where('discount_id',$_SESSION['redpos_discount']['discount_id']);
        }
        return $this->db->count_all_results('produk');
    }
    function produk_detail_list($start,$length,$query){
        $sql = 'produk.produk_id,produk_nama,jenis_produk_nama,satuan_nama,produk_kode';
        $this->db->select($sql);
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('discount','discount.produk_id like concat("%",concat("|",produk.produk_id,"|"),"%")');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        $this->db->group_start();
        $this->db->or_like('produk_nama',$query,'BOTH');
        $this->db->or_like('jenis_produk_nama',$query,'BOTH');
        $this->db->or_like('satuan_nama',$query,'BOTH');
        $this->db->group_end();
        if(isset($_SESSION['redpos_discount']['discount_id'])){
            $this->db->where('discount_id',$_SESSION['redpos_discount']['discount_id']);
        }
        return $this->db->get('produk',$length,$start)->result();
    }
    function get_all_produk($query){
        $sql = 'GROUP_CONCAT(concat("|",produk.produk_id,"|") SEPARATOR "" ) as list_data';
        $this->db->select($sql);
        $this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan','produk.produk_satuan_id = satuan.satuan_id');
        $this->db->group_start();
        $this->db->or_like('produk_nama',$query,'BOTH');
        $this->db->or_like('jenis_produk_nama',$query,'BOTH');
        $this->db->or_like('satuan_nama',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get('produk')->row();
    }
    function get_discount_by_produk($produk_id=''){
//        $this->db->group_start();
//            $this->db->group_start();
//                $this->db->where('mulai_tanggal <=',date('Y-m-d'));
//                $this->db->where('akhir_tanggal >=',date('Y-m-d'));
//            $this->db->group_end();
//            $this->db->or_where('jangka_waktu','Tidak');
//        $this->db->group_end();
        if($produk_id!=''&&$produk_id!=null){
            $this->db->like('produk_id','|'.$produk_id.'|','BOTH');
        }
        return $this->db->get('discount')->result();
    }
}

/* End of file Patern.php */
/* Location: ./application/models/Patern.php */