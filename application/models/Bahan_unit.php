<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bahan_unit extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "bahan_unit";
    }
    public  function unit_count_all($id){
        $this->db->where('bahan_id',$id);
        return $this->db->get('bahan_unit')->num_rows();
    }
    public  function unit_($id){
        $this->db->join('satuan','satuan.satuan_id = bahan_unit.satuan_id');
        $this->db->where('bahan_id',$id);
        return $this->db->count_all_results('bahan_unit');

    }
    public  function unit_count_filter($query,$id){
        $this->db->join('satuan','satuan.satuan_id = bahan_unit.satuan_id');
        $this->db->group_start();
        $this->db->or_like('satuan.satuan_nama',$query);
        $this->db->or_like('satuan.satuan_kode',$query);
        $this->db->group_end();
        $this->db->where('bahan_id',$id);
        return $this->db->count_all_results('bahan_unit');
    }
    public  function unit_list($start,$length,$query,$id){
        $this->db->select('bahan_unit.*,satuan.satuan_nama,satuan.satuan_kode');
        $this->db->join('satuan','satuan.satuan_id = bahan_unit.satuan_id');
        $this->db->group_start();
        $this->db->or_like('satuan.satuan_nama',$query);
        $this->db->or_like('satuan.satuan_kode',$query);
        $this->db->group_end();
        $this->db->where('bahan_id',$id);
        return $this->db->get('bahan_unit',$length,$start)->result();
    }
}