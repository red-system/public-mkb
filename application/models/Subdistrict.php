<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subdistrict extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "subdistrict";
    }
    function initialize($data){
        $this->db->insert_batch('subdistrict', $data);
    }
    function getCitySubdistrict($city_id){
        return $this->db->where('city_id',$city_id)->get('subdistrict')->result();
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */