<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "bonus";
    }
    function insert($data)
    {
        return parent::insert($data); // TODO: Change the autogenerated stub
    }

    function bonus_list($start,$length,$query,$reseller_id){
        $this->db->select("reseller.nama,bonus.*");
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->order_by('bonus_id', 'desc');
        $this->db->where('to_reseller_id',$reseller_id);
        if(isset($_GET['estimasi_start'])&&$this->input->get('estimasi_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('estimasi_start'));
        }
        if(isset($_GET['estimasi_end'])&&$this->input->get('estimasi_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('estimasi_end'));
        }
        if(isset($_GET['pengiriman_start'])&&$this->input->get('pengiriman_start')!=""){
            $this->db->where('tanggal_pengiriman >=', $this->input->get('pengiriman_start'));
        }
        if(isset($_GET['pengiriman_end'])&&$this->input->get('pengiriman_end')!=""){
            $this->db->where('tanggal_pengiriman <=', $this->input->get('pengiriman_end'));
        }
        return $this->db->get('bonus', $length, $start)->result();
    }
    function bonus_all($reseller_id){
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('to_reseller_id',$reseller_id);

        return $this->db->count_all_results('bonus');
    }
    function bonus_filter($query,$reseller_id){

        $this->db->like('bonus_id', $query, 'BOTH');

        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('to_reseller_id',$reseller_id);
        if(isset($_GET['estimasi_start'])&&$this->input->get('estimasi_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('estimasi_start'));
        }
        if(isset($_GET['estimasi_end'])&&$this->input->get('estimasi_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('estimasi_end'));
        }
        if(isset($_GET['pengiriman_start'])&&$this->input->get('pengiriman_start')!=""){
            $this->db->where('tanggal_pengiriman >=', $this->input->get('pengiriman_start'));
        }
        if(isset($_GET['pengiriman_end'])&&$this->input->get('pengiriman_end')!=""){
            $this->db->where('tanggal_pengiriman <=', $this->input->get('pengiriman_end'));
        }
        return $this->db->count_all_results('bonus');
    }

    function bonus_admin_list($start,$length,$query){
        $this->db->select("reseller.nama,bonus.*,b.nama as nama_penerima");
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->join('reseller as b','b.reseller_id = bonus.to_reseller_id');
        $this->db->order_by('bonus_id', 'desc');
        $this->db->where('bonus.status',"On Process");
        $this->db->where('bonus.manage_status',"waiting");
        $this->db->where('bonus.jumlah_bonus !=',0);
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('bonus.type',$query);
        $this->db->or_like('bonus.jumlah_deposit',$query);
        $this->db->or_like('bonus.persentase',$query);
        $this->db->or_like('bonus.jumlah_bonus',$query);
        $this->db->or_like('bonus.status',$query);
        $this->db->or_like('bonus.manage_status',$query);
        $this->db->group_end();
        return $this->db->get('bonus', $length, $start)->result();
    }
    function bonus_admin_all(){
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('bonus.status',"On Process");
        $this->db->where('bonus.manage_status',"waiting");
        $this->db->where('bonus.jumlah_bonus !=',0);
        return $this->db->count_all_results('bonus');
    }
    function bonus_admin_filter($query){

        $this->db->like('bonus_id', $query, 'BOTH');
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('bonus.status',"On Process");
        $this->db->where('bonus.manage_status',"waiting");
        $this->db->where('bonus.jumlah_bonus !=',0);
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('bonus.type',$query);
        $this->db->or_like('bonus.jumlah_deposit',$query);
        $this->db->or_like('bonus.persentase',$query);
        $this->db->or_like('bonus.jumlah_bonus',$query);
        $this->db->or_like('bonus.status',$query);
        $this->db->or_like('bonus.manage_status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('bonus');
    }
    function bonus_admin_list_success($start,$length,$query){
        $this->db->select("reseller.nama,bonus.*,b.nama as nama_penerima");
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->join('reseller as b','b.reseller_id = bonus.to_reseller_id');
        $this->db->order_by('bonus_id', 'desc');
        $this->db->where('bonus.status',"Terkirim");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('bonus.type',$query);
        $this->db->or_like('bonus.jumlah_deposit',$query);
        $this->db->or_like('bonus.persentase',$query);
        $this->db->or_like('bonus.jumlah_bonus',$query);
        $this->db->or_like('bonus.status',$query);
        $this->db->or_like('bonus.manage_status',$query);
        $this->db->group_end();
        return $this->db->get('bonus', $length, $start)->result();
    }
    function bonus_admin_all_success(){
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('bonus.status',"Terkirim");
        return $this->db->count_all_results('bonus');
    }
    function bonus_admin_filter_success($query){

        $this->db->like('bonus_id', $query, 'BOTH');
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('bonus.status',"Terkirim");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('bonus.type',$query);
        $this->db->or_like('bonus.jumlah_deposit',$query);
        $this->db->or_like('bonus.persentase',$query);
        $this->db->or_like('bonus.jumlah_bonus',$query);
        $this->db->or_like('bonus.status',$query);
        $this->db->or_like('bonus.manage_status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('bonus');
    }
    function bonus_admin_list_hold($start,$length,$query){
        $this->db->select("reseller.nama,bonus.*,b.nama as nama_penerima");
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->join('reseller as b','b.reseller_id = bonus.to_reseller_id');
        $this->db->order_by('bonus_id', 'desc');
        $this->db->where('bonus.status',"Hold");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('bonus.type',$query);
        $this->db->or_like('bonus.jumlah_deposit',$query);
        $this->db->or_like('bonus.persentase',$query);
        $this->db->or_like('bonus.jumlah_bonus',$query);
        $this->db->or_like('bonus.status',$query);
        $this->db->or_like('bonus.manage_status',$query);
        $this->db->group_end();
        return $this->db->get('bonus', $length, $start)->result();
    }
    function bonus_admin_all_hold(){
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('bonus.status',"Hold");
        return $this->db->count_all_results('bonus');
    }
    function bonus_admin_filter_hold($query){
        $this->db->like('bonus_id', $query, 'BOTH');
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->where('bonus.status',"Hold");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('bonus.type',$query);
        $this->db->or_like('bonus.jumlah_deposit',$query);
        $this->db->or_like('bonus.persentase',$query);
        $this->db->or_like('bonus.jumlah_bonus',$query);
        $this->db->or_like('bonus.status',$query);
        $this->db->or_like('bonus.manage_status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('bonus');
    }
    function change_hold_bonus($reseller_id,$estimasi_bonus){
        $this->db->where('to_reseller_id',$reseller_id);
        $this->db->where('status',"Hold");
        $data = array();
        $data["status"] = "On Process";
        $data["tanggal"] = $estimasi_bonus;
        return $this->db->update('bonus',$data);
    }
    function bonus_hari_ini(){
        $this->db->select("reseller.nama,bonus.*,b.nama as nama_penerima");
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->join('reseller as b','b.reseller_id = bonus.to_reseller_id');
        $this->db->order_by('bonus_id', 'desc');
        $this->db->where('bonus.tanggal <=',date("Y-m-d"));
        $this->db->where('bonus.status',"On Process");
        return $this->db->get('bonus')->result();
    }
    function cekBonus($po_produk_id,$to_reseller_id,$type="referal bonus",$po_produk_detail_id = null){
        $this->db->where('reseller_id',$to_reseller_id);
        $reseller = $this->db->get('reseller')->row();
        if($reseller->is_frezze==1){
            return false;
        }
        $this->db->where('to_reseller_id',$to_reseller_id);
        $this->db->where('po_produk_id',$po_produk_id);
        if($po_produk_detail_id!=null){
            $this->db->where('po_produk_detail_id',$po_produk_detail_id);
        }
        $this->db->where('type',$type);
        $check = $this->db->get('bonus')->row();
        $result = false;
        if($check==null){
            $result = true;
        }
        return $result;
    }
    function cekBonusDetail($po_produk_detail,$to_reseller_id,$type="referal bonus"){
        $this->db->where('to_reseller_id',$to_reseller_id);
        $this->db->where('po_produk_id',$po_produk_id);
        $this->db->where('type',$type);
        $check = $this->db->get('bonus')->row();
        $result = false;
        if($check==null){
            $result = true;
        }
        return $result;
    }
    function cekBonusWithdraw($pembayaran_hutang_produk_id,$to_reseller_id,$type="referal bonus"){
        $this->db->where('to_reseller_id',$to_reseller_id);
        $this->db->where('pembayaran_hutang_produk_id',$pembayaran_hutang_produk_id);
        $this->db->where('type',$type);
        $check = $this->db->get('bonus')->row();
        $result = false;
        if($check==null){
            $result = true;
        }
        return $result;
    }
    function changeToProcessing($arr_id){
        $data["manage_status"] = "processing";
        $this->db->where_in('bonus_id',$arr_id);
        return $this->db->update("bonus",$data);
    }
    function changeToSuccess($arr_id){
        $data["manage_status"] = "success";
        $data["tanggal_pengiriman"] = date("Y-m-d");
        $data["status"] = "Terkirim";
        $this->db->where_in('send_bonus_id',$arr_id);
        return $this->db->update("bonus",$data);
    }
    function changeTowaiting($arr_id){
        $data["manage_status"] = "waiting";
        $this->db->where_in('send_bonus_id',$arr_id);
        return $this->db->update("bonus",$data);
    }

    function changeToSuccessWithDate($arr_id, $date){
        $data["manage_status"] = "success";
        $data["tanggal_pengiriman"] = $date;
        $data["status"] = "Terkirim";
        $this->db->where_in('send_bonus_id',$arr_id);
        return $this->db->update("bonus",$data);
    }
    function getResellerId($bonus_id){
        $this->db->select('group_concat(to_reseller_id) as reseller_ids');
        $this->db->where_in('send_bonus_id',$bonus_id);
        return $this->db->get('bonus')->row()->reseller_ids;
    }
    function changeToFail($arr_id){
        $data["manage_status"] = "failed";
        $this->db->where_in('bonus_id',$arr_id);
        return $this->db->update("bonus",$data);
    }
    function changeToTurnback($arr_id){
        $data["manage_status"] = "waiting";
        $data["tanggal_pengiriman"] = date("Y-m-d");
        $data["status"] = "On Process";
        $this->db->where_in('bonus_id',$arr_id);
        return $this->db->update("bonus",$data);
    }
    function getTotalBonus($reseller_id){
        $this->db->select('sum(bonus.jumlah_bonus) as total');
        $this->db->where('to_reseller_id',$reseller_id);
        $row = $this->db->get('bonus')->row();
        if($row==null){
            return 0;
        }else{
            return $row->total;
        }
    }
    function bonus_lunas($reselller_id){
        $this->db->select('sum(bonus.jumlah_bonus) as bonus_total');
        $this->db->where('manage_status','success');
        $this->db->from('bonus');
        if(isset($_GET['start_date'])&&$this->input->get('start_date')!=""){
            $this->db->where('tanggal >=', $this->input->get('start_date'));
        }
        if(isset($_GET['end_date'])&&$this->input->get('end_date')!=""){
            $this->db->where('tanggal <=', $this->input->get('end_date'));
        }
//        if($reselller_id!=null){
//            $lokasi_id = $_SESSION['redpos_login']['lokasi_id'];
//            $this->db->where('lokasi_id',$lokasi_id);
//        }
        $bonus = 0;
        $bonus_arr = $this->db->get('');

        if ($bonus_arr->row()!=null){
            $bonus = $bonus_arr->row()->bonus_total;
        }
        return $bonus;
    }

    function bonusTotal($bonus_id){
        $this->db->select("sum(bonus.jumlah_bonus) as total_bonus");
        $this->db->join('reseller','reseller.reseller_id = bonus.to_reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->where_in("bonus_id",$bonus_id);
        $bonus = $this->db->get("bonus")->row();
        if($bonus==null){
            return 0;
        }else{
            return $bonus->total_bonus;
        }
    }
    function bonusListPrint($bonus_id){
        $this->db->select("bonus.*,sum(bonus.jumlah_bonus) as total_bonus,reseller.bank_atas_nama,nama_bank,kode_bank,reseller.email,reseller.bank_rekening,if(npwp.case is null,2,npwp.case) as case_res");
        $this->db->join('reseller','reseller.reseller_id = bonus.to_reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->join('npwp','reseller.reseller_id = npwp.reseller_id','left');
        $this->db->where_in("bonus_id",$bonus_id);
        $this->db->where("reseller.demo",0);
        $this->db->group_by("to_reseller_id");
        return $this->db->get("bonus")->result();
    }

    // accounting
    function accounting_bonus_all(){
        $this->db->select('reseller.nama as penerima,b.nama as pemberi, bonus.created_at, bonus.tanggal_pengiriman, bonus.jumlah_deposit, bonus.type,bonus.persentase,bonus.jumlah_bonus,bonus.status');
        $this->db->join('reseller', 'bonus.to_reseller_id = reseller.reseller_id');
        $this->db->join('reseller as b', 'bonus.from_reseller_id = b.reseller_id');
        return $this->db->count_all_results('bonus');
    }
    function accounting_bonus_filter($query){
        $this->db->select('a.nama as penerima, b.nama as pemberi, bonus.created_at, bonus.tanggal_pengiriman, bonus.jumlah_deposit, bonus.type, bonus.persentase, bonus.jumlah_bonus, bonus.status');
        $this->db->join('reseller as a', 'bonus.to_reseller_id = a.reseller_id');
        $this->db->join('reseller as b', 'bonus.from_reseller_id = b.reseller_id');

        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('bonus.created_at >=', $this->input->get('start_tanggal'));
            $this->db->where('bonus.created_at <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('a.nama', $query, 'BOTH');
        $this->db->or_like('b.nama', $query, 'BOTH');
        $this->db->or_like('bonus.created_at', $query, 'BOTH');
        $this->db->or_like('bonus.tanggal_pengiriman', $query, 'BOTH');
        $this->db->or_like('bonus.type', $query, 'BOTH');
        $this->db->or_like('bonus.status', $query, 'BOTH');
        $jumlah_deposit = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_deposit', $jumlah_deposit, 'BOTH');
        $persentase = $this->string_to_number($query);
        $this->db->or_like('bonus.persentase', $persentase, 'BOTH');
        $jumlah_bonus = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_bonus', $jumlah_bonus, 'BOTH');
        $this->db->group_end();

        return $this->db->count_all_results('bonus');
    }
    function accounting_bonus_list($start,$length,$query){
        $this->db->select('a.nama as penerima, b.nama as pemberi, bonus.created_at, bonus.tanggal_pengiriman, bonus.jumlah_deposit, bonus.type, bonus.persentase, bonus.jumlah_bonus, bonus.status');
        $this->db->join('reseller as a', 'bonus.to_reseller_id = a.reseller_id');
        $this->db->join('reseller as b', 'bonus.from_reseller_id = b.reseller_id');

        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('bonus.created_at >=', $this->input->get('start_tanggal'));
            $this->db->where('bonus.created_at <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('a.nama', $query, 'BOTH');
        $this->db->or_like('b.nama', $query, 'BOTH');
        $this->db->or_like('bonus.created_at', $query, 'BOTH');
        $this->db->or_like('bonus.tanggal_pengiriman', $query, 'BOTH');
        $this->db->or_like('bonus.type', $query, 'BOTH');
        $this->db->or_like('bonus.status', $query, 'BOTH');
        $jumlah_deposit = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_deposit', $jumlah_deposit, 'BOTH');
        $persentase = $this->string_to_number($query);
        $this->db->or_like('bonus.persentase', $persentase, 'BOTH');
        $jumlah_bonus = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_bonus', $jumlah_bonus, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('bonus.bonus_id', 'desc');
        return $this->db->get('bonus', $length, $start)->result();
    }

    function bonus_withdraw_all(){
        $this->db->select('a.nama as penerima, b.nama as pemberi, bonus.created_at, bonus.jumlah_deposit, bonus.type, bonus.persentase, bonus.jumlah_bonus, bonus.status');
        $this->db->join('reseller as a', 'bonus.to_reseller_id = a.reseller_id');
        $this->db->join('reseller as b', 'bonus.from_reseller_id = b.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = bonus.pembayaran_hutang_produk_id');
        return $this->db->count_all_results('bonus');
    }
    function bonus_withdraw_filter($query){
        $this->db->select('a.nama as penerima, b.nama as pemberi, bonus.created_at, bonus.jumlah_deposit, bonus.type, bonus.persentase, bonus.jumlah_bonus, bonus.status');
        $this->db->join('reseller as a', 'bonus.to_reseller_id = a.reseller_id');
        $this->db->join('reseller as b', 'bonus.from_reseller_id = b.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = bonus.pembayaran_hutang_produk_id');

        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('bonus.created_at >=', $this->input->get('start_tanggal'));
            $this->db->where('bonus.created_at <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('a.nama', $query, 'BOTH');
        $this->db->or_like('b.nama', $query, 'BOTH');
        $this->db->or_like('bonus.created_at', $query, 'BOTH');
        $this->db->or_like('bonus.type', $query, 'BOTH');
        $this->db->or_like('bonus.status', $query, 'BOTH');
        $jumlah_deposit = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_deposit', $jumlah_deposit, 'BOTH');
        $persentase = $this->string_to_number($query);
        $this->db->or_like('bonus.persentase', $persentase, 'BOTH');
        $jumlah_bonus = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_bonus', $jumlah_bonus, 'BOTH');
        $this->db->group_end();

        return $this->db->count_all_results('bonus');
    }
    function bonus_withdraw_list($start,$length,$query){
        $this->db->select('a.nama as penerima, b.nama as pemberi, bonus.created_at, bonus.jumlah_deposit, bonus.type, bonus.persentase, bonus.jumlah_bonus, bonus.status');
        $this->db->join('reseller as a', 'bonus.to_reseller_id = a.reseller_id');
        $this->db->join('reseller as b', 'bonus.from_reseller_id = b.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = bonus.pembayaran_hutang_produk_id');

        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('bonus.created_at >=', $this->input->get('start_tanggal'));
            $this->db->where('bonus.created_at <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('a.nama', $query, 'BOTH');
        $this->db->or_like('b.nama', $query, 'BOTH');
        $this->db->or_like('bonus.created_at', $query, 'BOTH');
        $this->db->or_like('bonus.type', $query, 'BOTH');
        $this->db->or_like('bonus.status', $query, 'BOTH');
        $jumlah_deposit = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_deposit', $jumlah_deposit, 'BOTH');
        $persentase = $this->string_to_number($query);
        $this->db->or_like('bonus.persentase', $persentase, 'BOTH');
        $jumlah_bonus = $this->string_to_number($query);
        $this->db->or_like('bonus.jumlah_bonus', $jumlah_bonus, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('bonus.bonus_id', 'desc');
        return $this->db->get('bonus', $length, $start)->result();
    }

    function listForDPP($bonus_id){
        $this->db->select("bonus.*,group_concat(bonus.bonus_id) as bonus_ids,sum(bonus.jumlah_bonus) as total_bonus,reseller.bank_atas_nama,nama_bank,kode_bank,reseller.email,reseller.bank_rekening,if(npwp.case is null,2,npwp.case) as case_res,if(npwp.ptkp is null,0,npwp.ptkp) as ptkp ");
        $this->db->join('reseller','reseller.reseller_id = bonus.to_reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->join('npwp','reseller.reseller_id = npwp.reseller_id','left');
        $this->db->where_in("bonus_id",$bonus_id);
        $this->db->group_by("to_reseller_id");
        return $this->db->get("bonus")->result();
    }

    function update_bonus_send($send_bonus_id,$bonus_ids){
        $data['send_bonus_id'] = $send_bonus_id;
        $this->db->where_in('bonus_id',$bonus_ids);
        return $this->db->update('bonus',$data);
    }

    function bonusBySendBonusId($send_bonus_id){
        $this->db->select('reseller.nama,bonus.*');
        $this->db->where('send_bonus_id',$send_bonus_id);
        $this->db->join('reseller','bonus.from_reseller_id = reseller.reseller_id');
        return $this->db->get('bonus')->result();
    }
    function bonus_harian_list($start,$length,$query){
        $this->db->select("send_bonus.*");
        $this->db->group_by("no_generate");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('no_generate',$query);
        $this->db->or_like('tanggal_proccess',$query);
        $this->db->group_end();
        $this->db->order_by("tanggal_proccess","desc");
        return $this->db->get('send_bonus', $length, $start)->result();
    }
    function bonus_harian_all(){
        $this->db->select("send_bonus.*");
        $this->db->group_by("no_generate");
        return $this->db->count_all_results('send_bonus');
    }
    function bonus_harian_filter($query){
        $this->db->select("send_bonus.*");
        $this->db->group_by("no_generate");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('no_generate',$query);
        $this->db->or_like('tanggal_proccess',$query);
        $this->db->group_end();
        return $this->db->count_all_results('send_bonus');
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */