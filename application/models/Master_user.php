<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_user extends CI_Model {
    var $db = null;
    var $table_name = '';
    public function __construct()
    {
        parent::__construct();
        $this->table_name= 'login';
        $this->db = $this->load->database('master', TRUE);
    }
    function list($start,$length,$query){
        $this->db->select('login.id,reseller.reseller_id,reseller.nama,login.username');
        $this->db->join('mykindofbeauty_kemiri.reseller as reseller','reseller.reseller_id = login.reseller_id');
        $this->db->group_start();
        $this->db->or_like('id', $query, 'BOTH');
        $this->db->or_like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('login.username', $query, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('id', 'desc');
        return $this->db->get('login', $length, $start)->result();
    }
    function count_filter($query){
        $this->db->like('id', $query, 'BOTH');
        $this->db->order_by('id', 'desc');
        return $this->db->count_all_results('login');
    }
    function count_all(){
        return $this->db->count_all_results('login');
    }
}

/* End of file User.php */
/* Location: ./application/models/User.php */