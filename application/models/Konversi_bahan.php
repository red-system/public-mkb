<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konversi_bahan extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "konversi_bahan";
	}
	function konversi_bahan_count_all(){
		$this->db->join('produk','produk.produk_id = konversi_bahan.produk_id');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('bahan','bahan.bahan_id = konversi_bahan.bahan_id');
		return $this->db->count_all_results('konversi_bahan');
	}
	function konversi_bahan_count_filter($query){
		$this->db->join('produk','produk.produk_id = konversi_bahan.produk_id');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('bahan','bahan.bahan_id = konversi_bahan.bahan_id');
		$this->db->join('size','size.size_id = produk.size_id','left');
		$this->db->join('color','color.color_id = produk.color_id','left');
		$this->db->join('fabric','fabric.fabric_id = produk.fabric_id','left');
		$this->db->join('patern','patern.patern_id = produk.patern_id','left');
		$this->db->group_start();
			$this->db->or_like('size_nama',$query);
			$this->db->or_like('color_nama',$query);
			$this->db->or_like('fabric_nama',$query);
			$this->db->or_like('patern_nama',$query);
			$this->db->or_like('bahan_nama',$query);
		$this->db->group_end();
		return $this->db->count_all_results('konversi_bahan');
	}
	function konversi_bahan_list($start,$length,$query){
		$this->db->select('konversi_bahan.*,produk.produk_nama');
		$this->db->join('produk','produk.produk_id = konversi_bahan.produk_id');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('bahan','bahan.bahan_id = konversi_bahan.bahan_id');
		$this->db->group_start();
			$this->db->or_like('bahan_nama',$query);
		$this->db->group_end();
		return $this->db->get('konversi_bahan',$length,$start)->result();
	}
	function konversi_bahan_by_produk($produk_id,$bahan_id){
		$this->db->where('produk_id',$produk_id);
		$this->db->where('bahan_id',$bahan_id);
		return $this->db->get('konversi_bahan')->row();
	}
}

/* End of file Log_kasir.php */
/* Location: ./application/models/Jenis_bahan.php */
