<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meeting extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "meeting";
        $this->load->library('main');
    }
    
    function meeting_count_all(){
        $this->db->select('*');
        $this->db->order_by('meeting_date', 'DESC');
        $this->db->order_by('meeting_id', 'DESC');
        return $this->db->count_all_results('meeting');
    }
    function meeting_count_filter($query){
        $this->db->select('*');
        $this->db->group_start();
        $this->db->like('meeting_name', $query, 'BOTH');
        $this->db->or_like('meeting_date', $query, 'BOTH');
        $this->db->or_like('meeting_description', $query, 'BOTH');
        $this->db->group_end();
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('meeting_date >=', $this->input->get('start_tanggal'));
            $this->db->where('meeting_date <=', $this->input->get('end_tanggal'));
        };
        $this->db->order_by('meeting_date', 'DESC');
        $this->db->order_by('meeting_id', 'DESC');
        return $this->db->count_all_results('meeting');
    }
    function meeting_list($start,$length,$query){
        $this->db->select('*');
        $this->db->group_start();
        $this->db->like('meeting_name', $query, 'BOTH');
        $this->db->or_like('meeting_date', $query, 'BOTH');
        $this->db->or_like('meeting_description', $query, 'BOTH');
        $this->db->group_end();
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('meeting_date >=', $this->input->get('start_tanggal'));
            $this->db->where('meeting_date <=', $this->input->get('end_tanggal'));
        };
        $this->db->order_by('meeting_date', 'DESC');
        $this->db->order_by('meeting_id', 'DESC');
        return $this->db->get('meeting', $length, $start)->result();
    }

    function meeting_check($where){
        $this->db->select('*');
        $this->db->where($where);
        return $this->db->get('kehadiran_meeting');
    }

    function meeting_notif($reseller_id){
 
        $last_meeting = $this->db->select('*')
                        ->order_by('meeting_id', 'DESC')
                        ->limit(1)
                        ->get('meeting')
                        ->row();
        
        $meeting_id = $last_meeting->meeting_id;
        $meeting_date = $last_meeting->meeting_date;

        $kehadiran = $this->db->select('*')
                    ->where('reseller_id', $reseller_id)
                    ->where('meeting_id', $meeting_id)
                    ->get('kehadiran_meeting')->num_rows();

        if ($kehadiran == 0) {
            if ($meeting_date >= date('Y-m-d')) {
                $notif = true;
            }else{
                $notif = false;
            }
        }else{
            $notif = false;
        }

        return $notif;


    }
    

}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */