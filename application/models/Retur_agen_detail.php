<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_agen_detail extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "retur_agen_detail";
    }
    function data_by_id($retur_agen_id){
        $this->db->select('retur_agen_detail.*,produk.produk_nama');
        $this->db->join('produk','retur_agen_detail.produk_id = produk.produk_id');
        $this->db->where("retur_agen_id",$retur_agen_id);
        return $this->db->get('retur_agen_detail')->result();
    }
    function stock_produk_by_id($lokasi_id,$produk_id){
        $this->db->where('stock_produk_lokasi_id',$lokasi_id);
        $this->db->where('produk_id',$produk_id);
        $cek = $this->db->get('display_stock_produk_lokasi')->row();
        if($cek==null){
            return 0;
        }else{
            return $cek->jumlah;
        }
    }


}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */