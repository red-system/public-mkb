<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "jenis_produk";
	}	
	function jenis_produk_list($start,$length,$query){
		$this->db->like('jenis_produk_id', $query, 'BOTH'); 
		$this->db->or_like('jenis_produk_kode', $query, 'BOTH'); 
		$this->db->or_like('jenis_produk_nama', $query, 'BOTH'); 
		$this->db->order_by('jenis_produk_id', 'desc');
		return $this->db->get('jenis_produk', $length, $start)->result();
	}
	function jenis_produk_count_filter($query){
		$this->db->like('jenis_produk_id', $query, 'BOTH'); 
		$this->db->or_like('jenis_produk_kode', $query, 'BOTH'); 
		$this->db->or_like('jenis_produk_nama', $query, 'BOTH');
		return $this->db->count_all_results('jenis_produk');
	}
	function jenis_produk_count_all(){
		return $this->db->count_all('jenis_produk');
    }
	function is_ready_kode($jenis_produk_id,$kode){
		$this->db->where('jenis_produk_kode', $kode);
		$data = $this->db->get('jenis_produk')->row();
		if($data != null){
			if($data->jenis_produk_id == $jenis_produk_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}


}

/* End of file Jenis_produk.php */
/* Location: ./application/models/Jenis_produk.php */
