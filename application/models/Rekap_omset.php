<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_omset extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "rekap_omset";
    }

    function get_rekap_omset($date,$reseller_id){
        $first_date = date("Y-m-01",$date);
        $last_date = date("Y-m-t",$date);
        $this->db->where("reseller_id",$reseller_id);
        $this->db->where("waktu >=",$first_date);
        $this->db->where("waktu <=",$last_date);
        return $this->db->get('rekap_omset')->row();
    }


}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
