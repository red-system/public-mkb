<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_login extends CI_Model {
    var $db = null;

    var $table_name = '';
    public function __construct()
    {
        parent::__construct();
        $this->table_name= 'login';
        $this->db = $this->load->database('master', TRUE);
    }
    function delete_by_id($field_name,$field_id){
        $this->db->where($field_name, $field_id);
        return $this->db->delete($this->table_name);
    }
    function delete_all(){
        return $this->db->empty_table($this->table_name);
    }
    function insert($data){

        return $this->db->insert($this->table_name, $data);
    }
    function last_id(){
        return $this->db->insert_id();
    }
    function update_by_id($field_name,$field_id,$data){
        $this->db->where($field_name, $field_id);
        return $this->db->update($this->table_name, $data);
    }
    function detail($field_name,$field_id){
        $this->db->where($field_name, $field_id);
        return $this->db->get($this->table_name);
    }
    function all_list(){
        return $this->db->get($this->table_name)->result();
    }
    function row_by_id($id){
        $this->db->where("id", $id);
        return $this->db->get($this->table_name)->row();
    }
    function row_by_field($field_name,$value){
        $this->db->where($field_name, $value);
        return $this->db->get($this->table_name)->row();
    }
    function string_to_number($string){
        $temp = str_replace(".", "", $string);
        $temp = str_replace(",", "", $temp);
        return $temp;
    }
    function string_to_decimal($string){
        $temp = str_replace(",", "", $string);
        return $temp;
    }
    function start_trans(){
        $this->db->trans_begin();
    }
    function result_trans(){
        if ($this->db->trans_status() === FALSE)
            return FALSE;

        $this->db->trans_commit();
        return TRUE;
    }
    function get_exists($field_name,$field_id){
        $this->db->from($this->table_name);
        $this->db->where($field_name, $field_id);
        $query = $this->db->count_all_results();
        if($query>0) {
            $exist = 1;
        } else {
            $exist = 0;
        }
        return $exist;
    }
    function auth_login($username){
        $this->db->select('login.*,company.*,company.id as company_id,login.id as id');
        $this->db->where('login.username', $username);
        $this->db->join('company','login.company_id = company.id');
        $this->db->where('login.company_id is not null');
        $this->db->from('login');
        return $this->db->get()->row();
    }
    function auth_login_forget($email){
        $this->db->where("reseller.email",$email);
        $this->db->from("mykindofbeauty_kemiri.reseller");
        return $this->db->get()->row();
    }
    function get_id_by_reseller($reseller_id){
        $this->db->where('login.reseller_id',$reseller_id);
        $this->db->from('login');
        return $this->db->get()->row();
    }
    function auth_login_master($username){
        $this->db->select('login.*');
        $this->db->where('username', $username);
        $this->db->where('company_id is null');
        $this->db->from('login');
        return $this->db->get()->row();
    }
    function change_password($id,$password){
        $sql = 'update login set password = "'.$password.'" where id = '.$id;
        return  $this->db->query($sql);
    }

    function nonactive_user($id, $username){
        $sql = 'update login set username = "'.$username.'" where id = '.$id;
        return  $this->db->query($sql);
    }

    function user_by_id($user_id){
        $this->db->where('mykindofbeauty_master.login.id', $user_id);
        $this->db->select('login.*,staff.*,user_role.user_role_name,reseller.referal_code');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.staff', $_SESSION['redpos_login']['db_name'].'.staff.staff_id = mykindofbeauty_master.login.user_staff_id');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.user_role', $_SESSION['redpos_login']['db_name'].'.user_role.user_role_id = mykindofbeauty_master.login.user_role_id');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.reseller', $_SESSION['redpos_login']['db_name'].'.reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        return $this->db->get('mykindofbeauty_master.login')->row();
    }
    function is_ready_email($email){
        $this->db->where('email', $email);
        $data = $this->db->get('user')->row();
        if($data != null){
            return false;
        } else {
            return true;
        }
    }
    function user_list($start,$length,$query){
        $this->db->select('login.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,user_role.user_role_name');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.staff', $_SESSION['redpos_login']['db_name'].'.staff.staff_id = mykindofbeauty_master.login.user_staff_id');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.user_role', $_SESSION['redpos_login']['db_name'].'.user_role.user_role_id = mykindofbeauty_master.login.user_role_id');
        $this->db->where('mykindofbeauty_master.login.company_id',$_SESSION['redpos_login']['company_id']);
        $this->db->group_start();
            $this->db->like('staff.staff_id', $query, 'BOTH');
            $this->db->or_like('login.id', $query, 'BOTH');
            $this->db->or_like('staff.staff_nama', $query, 'BOTH');
            $this->db->or_like('staff.staff_email', $query, 'BOTH');
            $this->db->or_like('staff.staff_phone_number', $query, 'BOTH');
            $this->db->or_like('user_role.user_role_name', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->get('mykindofbeauty_master.login', $length, $start)->result();
    }
    function user_count_filter($query){
        $this->db->select('login.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,user_role.user_role_name');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.staff', $_SESSION['redpos_login']['db_name'].'.staff.staff_id = mykindofbeauty_master.login.user_staff_id');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.user_role', $_SESSION['redpos_login']['db_name'].'.user_role.user_role_id = mykindofbeauty_master.login.user_role_id');
        $this->db->where('mykindofbeauty_master.login.company_id',$_SESSION['redpos_login']['company_id']);
        $this->db->group_start();
        $this->db->like('staff.staff_id', $query, 'BOTH');
        $this->db->or_like('login.id', $query, 'BOTH');
        $this->db->or_like('staff.staff_nama', $query, 'BOTH');
        $this->db->or_like('staff.staff_email', $query, 'BOTH');
        $this->db->or_like('staff.staff_phone_number', $query, 'BOTH');
        $this->db->or_like('user_role.user_role_name', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('mykindofbeauty_master.login');
    }
    function user_count_all(){
        $this->db->select('login.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,user_role.user_role_name');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.staff', $_SESSION['redpos_login']['db_name'].'.staff.staff_id = mykindofbeauty_master.login.user_staff_id');
        $this->db->join($_SESSION['redpos_login']['db_name'].'.user_role', $_SESSION['redpos_login']['db_name'].'.user_role.user_role_id = mykindofbeauty_master.login.user_role_id');
        $this->db->where('mykindofbeauty_master.login.company_id',$_SESSION['redpos_login']['company_id']);
        return $this->db->count_all_results('mykindofbeauty_master.login');
    }
    function change_avatar($user_id,$url){
        $this->db->where('id', $user_id);
        $updated_at = date('Y-m-d H:i:s');
        $data = array("avatar"=>$url,"updated_at"=>$updated_at);
        return $this->db->update('mykindofbeauty_master.login', $data);
    }
    function forget_session_check($email,$code){
        $this->db->where('forget_request_email', $email);
        $this->db->where('code', $code);
        $this->db->where('UNIX_TIMESTAMP() < timeout', null, false);
        return $this->db->get('mykindofbeauty_kemiri.forget_request')->result();
    }

}

/* End of file User.php */
/* Location: ./application/models/User.php */