<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_agen extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "retur_agen";
    }
    function get_no_retur(){
        $year = date("y");
        $month = date("m");
        $lokasi_kode = isset($_SESSION['redpos_login']['lokasi_id']) ? $_SESSION['redpos_login']['lokasi_id'] : "00";
        $prefix = "RE/".$month.$year."/".$lokasi_kode."/";
        $this->db->like('no_retur', $prefix, 'BOTH');
        $this->db->select('(max(urutan)+1) as kode');
        $this->db->from('retur_agen');
        $result = $this->db->get()->row()->kode;
        if ($result != null){
            return $prefix.($result);
        } else {
            return $prefix."1";
        }
    }
    function retur_all($reseller_id){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->where('retur_agen.reseller_id',$reseller_id);
        return $this->db->count_all_results('retur_agen');
    }
    function retur_filter($query,$reseller_id){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->where('retur_agen.reseller_id',$reseller_id);
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('retur_agen');
    }
    function retur_list($start,$length,$query,$reseller_id){
        $this->db->select('retur_agen.*');
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->where('retur_agen.reseller_id',$reseller_id);
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->group_end();
        return $this->db->get('retur_agen',$length,$start)->result();
    }
    function retur_request_all(){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->where('retur_agen.status',"waiting");
        return $this->db->count_all_results('retur_agen');
    }
    function retur_request_filter($query){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->or_like('reseller.type',$query);
        $this->db->group_end();
        $this->db->where('retur_agen.status',"waiting");
        return $this->db->count_all_results('retur_agen');
    }
    function retur_request_list($start,$length,$query){
        $this->db->select('retur_agen.*,reseller.nama,reseller.type');
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->or_like('reseller.type',$query);
        $this->db->group_end();
        $this->db->where('retur_agen.status',"waiting");
        return $this->db->get('retur_agen',$length,$start)->result();
    }

    function retur_approve_all(){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->where('retur_agen.status',"approve");
        return $this->db->count_all_results('retur_agen');
    }
    function retur_approve_filter($query){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->group_end();
        $this->db->where('retur_agen.status',"approve");
        return $this->db->count_all_results('retur_agen');
    }
    function retur_approve_list($start,$length,$query){
        $this->db->select('retur_agen.*,reseller.nama,reseller.type');
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->group_end();
        $this->db->where('retur_agen.status',"approve");
        return $this->db->get('retur_agen',$length,$start)->result();
    }
    function retur_refuse_all(){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->where('retur_agen.status',"refuse");
        return $this->db->count_all_results('retur_agen');
    }
    function retur_refuse_filter($query){
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->group_end();
        $this->db->where('retur_agen.status',"refuse");
        return $this->db->count_all_results('retur_agen');
    }
    function retur_refuse_list($start,$length,$query){
        $this->db->select('retur_agen.*,reseller.nama,reseller.type');
        $this->db->join('reseller','retur_agen.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_retur',$query);
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('retur_agen.status',$query);
        $this->db->group_end();
        $this->db->where('retur_agen.status',"refuse");
        return $this->db->get('retur_agen',$length,$start)->result();
    }
    function insertData($reseller_id)
    {
        $retur_agen = array();
        $retur_agen['tanggal'] = date("Y-m-d");
        $no_retur = $this->get_no_retur();
        $year = date("y");
        $month = date("m");
        $lokasi_kode = isset($_SESSION['redpos_login']['lokasi_id']) ? $_SESSION['redpos_login']['lokasi_id'] : "00";
        $prefix = "RE/".$month.$year."/".$lokasi_kode."/";
        $retur_agen['no_retur'] = $no_retur;
        $urutan = str_replace($prefix,"",$no_retur);
        $retur_agen['urutan'] = $urutan;
        $retur_agen['reseller_id'] = $reseller_id;
        $this->db->insert('retur_agen',$retur_agen);
        $retur_agen_id = $this->db->insert_id();
        $retur_agen_detail_input = $this->input->post('item_produk');
        $retur_agen_detail = array();
        foreach ($retur_agen_detail_input as $itemretur){
            $item = array();

            $item['retur_agen_id'] = $retur_agen_id;
            $item['produk_id'] = $itemretur['produk_id'];
            $item['jumlah'] = $itemretur['jumlah'];
            $item['keterangan'] = $itemretur['keterangan'];
            $item['created_at'] = date("Y-m-d H:i:s");
            array_push($retur_agen_detail,$item);
        }
        $insert = $this->db->insert_batch('retur_agen_detail',$retur_agen_detail);
        if($insert>0){
            return true;
        }else{
            return false;
        }

    }
    function editData()
    {
        $retur_agen_id = $this->input->post('retur_agen_id');
        $retur_agen_detail_input = $this->input->post('item_produk');
        $retur_agen_detail = array();
        $this->db->where('retur_agen_id',$retur_agen_id);
        $this->db->delete('retur_agen_detail');
        foreach ($retur_agen_detail_input as $itemretur){
            $item = array();
            $item['retur_agen_id'] = $retur_agen_id;
            $item['produk_id'] = $itemretur['produk_id'];
            $item['jumlah'] = $itemretur['jumlah'];
            $item['keterangan'] = $itemretur['keterangan'];
            $item['created_at'] = date("Y-m-d H:i:s");
            array_push($retur_agen_detail,$item);
        }
        $insert = $this->db->insert_batch('retur_agen_detail',$retur_agen_detail);
        if($insert>0){
            return true;
        }else{
            return false;
        }

    }
    function retur_agen_detail_by_id($retur_agen_id){
        $this->db->select('retur_agen_detail.*,produk.produk_nama');
        $this->db->join('produk','retur_agen_detail.produk_id = produk.produk_id');
        $this->db->where("retur_agen_id",$retur_agen_id);
        return $this->db->get('retur_agen_detail')->result();
    }

}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */