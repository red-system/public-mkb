<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif_detail extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "notif_detail";
    }

}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */