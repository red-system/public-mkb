<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "city";
    }
    function geProvinceCity($id){
        return $this->db->where('province_id',$id)->get('city')->result();
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */