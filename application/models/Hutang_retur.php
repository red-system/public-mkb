<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang_retur extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "hutang_retur";
    }
    function count_all(){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        return $this->db->count_all_results('hutang_retur');
    }
    function count_filter($query){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.retur_agen_id',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        return $this->db->count_all_results('hutang_retur');
    }
    function _list($start,$lenth,$query){
        $this->db->select('hutang_retur.*,no_retur,voucher_code,produk.produk_nama,pembayaran_hutang_retur.no_pembayaran');
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.retur_agen_id',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        return $this->db->get('hutang_retur',$lenth,$start)->result();
    }

    function count_all_res($reseller_id){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->where("reseller.reseller_id",$reseller_id);
        return $this->db->count_all_results('hutang_retur');
    }
    function count_filter_res($query,$reseller_id){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.retur_agen_id',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $this->db->where("reseller.reseller_id",$reseller_id);
        return $this->db->count_all_results('hutang_retur');
    }
    function _list_res($start,$lenth,$query,$reseller_id){
        $this->db->select('hutang_retur.*,no_retur,voucher_code,produk.produk_nama,pembayaran_hutang_retur.no_pembayaran');
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.retur_agen_id',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $this->db->where("reseller.reseller_id",$reseller_id);
        return $this->db->get('hutang_retur',$lenth,$start)->result();
    }

    function pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $data["pembayaran_hutang_retur_id"] = $pembayaran_hutang_produk_id;
        $this->db->where_in('hutang_retur_id',$hutang_produk_id);
        return $this->db->update('hutang_retur',$data);

    }
    function  pembayaran_hutang_retur($pembayaran_hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $this->db->where("pembayaran_hutang_retur_id",$pembayaran_hutang_produk_id);
        return $this->db->update("hutang_retur",$data);
    }
    function hutang_retur_ids($pembayaran_hutang_retur_id){
        $this->db->select("hutang_retur.*,produk_nama,reseller.nama as nama_reseller,no_retur,voucher_code");
        $this->db->where("pembayaran_hutang_retur_id",$pembayaran_hutang_retur_id);
        $this->db->join('produk','hutang_retur.produk_id = produk.produk_id');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id');
        $this->db->join('retur_agen','hutang_retur.retur_agen_id = retur_agen.retur_agen_id');
        $this->db->join('voucher_retur','hutang_retur.voucher_retur_id = voucher_retur.voucher_retur_id');
        return $this->db->get("hutang_retur")->result();
    }

}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */