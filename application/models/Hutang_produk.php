<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang_produk extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "hutang_produk";
    }
    function count_all(){
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        return $this->db->count_all_results('hutang_produk');
    }
    function count_filter($query){
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->group_start();
        $this->db->or_like('po_produk.po_produk_no',$query,'both');
        $this->db->or_like('voucher_produk.voucher_code',$query,'both');
        $this->db->or_like('hutang_produk.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_produk.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        return $this->db->count_all_results('hutang_produk');
    }
    function _list($start,$lenth,$query){
        $this->db->select('po_produk_no,voucher_produk.voucher_code,produk.produk_nama,hutang_produk.status,hutang_produk.jumlah,tanggal_pelunasan,pembayaran_hutang_produk.no_pembayaran,hutang_produk.created_at,hutang_produk.updated_at,hutang_produk_id,po_produk.po_produk_id');
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->group_start();
        $this->db->or_like('po_produk.po_produk_no',$query,'both');
        $this->db->or_like('voucher_produk.voucher_code',$query,'both');
        $this->db->or_like('hutang_produk.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_produk.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        return $this->db->get('hutang_produk',$lenth,$start)->result();
    }

    function count_piutang_all($reseller_id){
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->where("reseller.reseller_id",$reseller_id);
        return $this->db->count_all_results('hutang_produk');
    }
    function count_piutang_filter($query,$reseller_id){
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->group_start();
        $this->db->or_like('po_produk.po_produk_no',$query,'both');
        $this->db->or_like('voucher_produk.voucher_code',$query,'both');
        $this->db->or_like('hutang_produk.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_produk.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $this->db->where("reseller.reseller_id",$reseller_id);
        return $this->db->count_all_results('hutang_produk');
    }
    function _piutang_list($start,$lenth,$query,$reseller_id){
        $this->db->select('po_produk_no,voucher_produk.voucher_code,produk.produk_nama,hutang_produk.status,hutang_produk.jumlah,tanggal_pelunasan,pembayaran_hutang_produk.no_pembayaran,hutang_produk.created_at,hutang_produk.updated_at,hutang_produk_id,po_produk.po_produk_id');
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->group_start();
        $this->db->or_like('po_produk.po_produk_no',$query,'both');
        $this->db->or_like('voucher_produk.voucher_code',$query,'both');
        $this->db->or_like('hutang_produk.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_produk.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $this->db->where("reseller.reseller_id",$reseller_id);
        $this->db->order_by('hutang_produk.hutang_produk_id','desc');
        return $this->db->get('hutang_produk',$lenth,$start)->result();
    }
    function pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $data["pembayaran_hutang_produk_id"] = $pembayaran_hutang_produk_id;
        $this->db->where_in('hutang_produk_id',$hutang_produk_id);
        return $this->db->update('hutang_produk',$data);

    }
    function  pembayaran_hutang_produk($pembayaran_hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        return $this->db->update("hutang_produk",$data);
    }
    function hutang_produk_ids($pembayaran_hutang_produk_id){
        $this->db->select("hutang_produk.*,produk_nama,reseller.nama as nama_reseller,po_produk_no,voucher_produk.voucher_code");
        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        $this->db->join('produk','hutang_produk.produk_id = produk.produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->join('po_produk','hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('voucher_produk','hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id');
        return $this->db->get("hutang_produk")->result();
    }

    // accounting
    function pendapatan_produk_all(){
        $this->db->select('pendapatan_penukaran_produk.*');
        return $this->db->count_all_results('pendapatan_penukaran_produk');
    }
    function pendapatan_produk_filter($query){
        $this->db->select('pendapatan_penukaran_produk.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('tanggal_pemesanan ', $query, 'BOTH');
        $this->db->or_like('tanggal_pelunasan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('tanggal_pelunasan', 'desc');
        return $this->db->count_all_results('pendapatan_penukaran_produk');
    }
    function pendapatan_produk_list($start,$length,$query){
        $this->db->select('pendapatan_penukaran_produk.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('tanggal_pemesanan ', $query, 'BOTH');
        $this->db->or_like('tanggal_pelunasan', $query, 'BOTH');
        $total = $this->string_to_number($query);
        $this->db->or_like('grand_total', $total, 'BOTH');
        $this->db->group_end();
        $this->db->order_by('tanggal_pelunasan', 'desc');
        return $this->db->get('pendapatan_penukaran_produk', $length, $start)->result();
    }

    function hutang_produk_keseluruhan_all(){
        $this->db->distinct();
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total/55000) as pcs, hutang_produk.tanggal_pelunasan, hutang_produk.created_at');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id', 'left');
        $this->db->order_by('pembayaran_hutang_produk.no_pembayaran');
        $this->db->order_by('hutang_produk.tanggal_pelunasan');
        return $this->db->count_all_results('hutang_produk');
    }
    function hutang_produk_keseluruhan_filter($query){
        $this->db->distinct();
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total/55000) as pcs, hutang_produk.tanggal_pelunasan, hutang_produk.created_at');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id', 'left');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('hutang_produk.tanggal_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang_produk.created_at', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pembayaran_hutang_produk.no_pembayaran');
        $this->db->order_by('hutang_produk.tanggal_pelunasan');

        return $this->db->count_all_results('hutang_produk');
    }
    function hutang_produk_keseluruhan_list($start,$length,$query){
        $this->db->distinct();
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total/55000) as pcs, hutang_produk.tanggal_pelunasan, hutang_produk.created_at');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id', 'left');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('hutang_produk.tanggal_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang_produk.created_at', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pembayaran_hutang_produk.no_pembayaran');
        $this->db->order_by('hutang_produk.tanggal_pelunasan');
        
        return $this->db->get('hutang_produk', $length, $start)->result();
    }


    function produk_keluar_by_hutang_produk_all(){
        $this->db->select('produk_keluar_by_penukaran.*');
        return $this->db->count_all_results('produk_keluar_by_penukaran');
    }
    function produk_keluar_by_hutang_produk_filter($query){
        $this->db->select('produk_keluar_by_penukaran.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('tanggal_pelunasan', $query, 'BOTH');
        $this->db->group_end();

        return $this->db->count_all_results('produk_keluar_by_penukaran');
    }
    function produk_keluar_by_hutang_produk_list($start,$length,$query){
        $this->db->select('produk_keluar_by_penukaran.*');
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };
        $this->db->group_start();
        $this->db->like('nama', $query, 'BOTH');
        $this->db->or_like('tanggal_pelunasan', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->get('produk_keluar_by_penukaran', $length, $start)->result();
    }

    function sisa_hutang_produk_all(){
        $this->db->select('reseller.nama, po_produk.po_produk_no, (po_produk.grand_total/55000) as pcs, hutang_produk.created_at');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->where('hutang_produk.status', 'Hutang');
        $this->db->order_by('reseller.nama');
        $this->db->order_by('hutang_produk.created_at');
        return $this->db->count_all_results('hutang_produk');
    }
    function sisa_hutang_produk_filter($query){
        $this->db->select('reseller.nama, po_produk.po_produk_no, (po_produk.grand_total/55000) as pcs, hutang_produk.created_at');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->where('hutang_produk.status', 'Hutang');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.created_at >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.created_at <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('hutang_produk.created_at', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('reseller.nama');
        $this->db->order_by('hutang_produk.created_at');

        return $this->db->count_all_results('hutang_produk');
    }
    function sisa_hutang_produk_list($start,$length,$query){
        $this->db->select('reseller.nama, po_produk.po_produk_no, (po_produk.grand_total/55000) as pcs, hutang_produk.created_at');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->where('hutang_produk.status', 'Hutang');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.created_at >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.created_at <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('hutang_produk.created_at', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('reseller.nama');
        $this->db->order_by('hutang_produk.created_at');
        
        return $this->db->get('hutang_produk', $length, $start)->result();
    }

    function withdraw_produk_all(){
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total/55000) as pcs, hutang_produk.tanggal_pelunasan');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->where('pembayaran_hutang_produk.status', 'approve');
        $this->db->where('hutang_produk.status', 'lunas');
        $this->db->where('pembayaran_hutang_produk.cash', '0');
        $this->db->order_by('pembayaran_hutang_produk.no_pembayaran');
        $this->db->order_by('hutang_produk.tanggal_pelunasan');
        return $this->db->count_all_results('hutang_produk');
    }
    function withdraw_produk_filter($query){
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total/55000) as pcs, hutang_produk.tanggal_pelunasan');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->where('pembayaran_hutang_produk.status', 'approve');
        $this->db->where('hutang_produk.status', 'lunas');
        $this->db->where('pembayaran_hutang_produk.cash', '0');
        
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('hutang_produk.tanggal_pelunasan', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pembayaran_hutang_produk.no_pembayaran');
        $this->db->order_by('hutang_produk.tanggal_pelunasan');

        return $this->db->count_all_results('hutang_produk');
    }
    function withdraw_produk_list($start,$length,$query){
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total/55000) as pcs, hutang_produk.tanggal_pelunasan');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('pembayaran_hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->where('pembayaran_hutang_produk.status', 'approve');
        $this->db->where('hutang_produk.status', 'lunas');
        $this->db->where('pembayaran_hutang_produk.cash', '0');
        
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('hutang_produk.tanggal_pelunasan', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pembayaran_hutang_produk.no_pembayaran');
        $this->db->order_by('hutang_produk.tanggal_pelunasan');
        
        return $this->db->get('hutang_produk', $length, $start)->result();
    }


}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */