<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komposisi_produk_bahan extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'komposisi_produk_bahan';
    }
    function komposisi_list($start,$length,$query,$produk_id){
        $this->db->select('komposisi_produk_bahan.*,bahan_nama,satuan_nama');
        $this->db->join('bahan', 'bahan.bahan_id = komposisi_produk_bahan.bahan_id');
        $this->db->join('satuan', 'satuan.satuan_id = komposisi_produk_bahan.satuan_id');
        $this->db->group_start();
        $this->db->like('bahan.bahan_nama', $query, 'BOTH');
        $this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
        $this->db->group_end();
        $this->db->where('komposisi_produk_bahan.produk_id', $produk_id);
        $this->db->order_by('komposisi_produk_bahan.komposisi_id', 'desc');
        return $this->db->get('komposisi_produk_bahan', $length, $start)->result();
    }
    function komposisi_count_all($produk_id){
        $this->db->join('bahan', 'bahan.bahan_id = komposisi_produk_bahan.bahan_id');
        $this->db->join('satuan', 'satuan.satuan_id = komposisi_produk_bahan.satuan_id');
        $this->db->where('komposisi_produk_bahan.produk_id', $produk_id);
        return $this->db->count_all_results('komposisi_produk_bahan');
    }
    function komposisi_count_filter($query,$produk_id){
        $this->db->join('bahan', 'bahan.bahan_id = komposisi_produk_bahan.bahan_id');
        $this->db->join('satuan', 'satuan.satuan_id = komposisi_produk_bahan.satuan_id');
        $this->db->where('komposisi_produk_bahan.produk_id', $produk_id);
        $this->db->group_start();
        $this->db->like('bahan.bahan_nama', $query, 'BOTH');
        $this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('komposisi_produk_bahan');
    }

}

/* End of file Komposisi_produk.php */
/* Location: ./application/models/Komposisi_produk.php */