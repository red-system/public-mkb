<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "province";
    }

    function getProvinceAll(){
        return $this->db->get('province')->result();
    }


}

/* End of file Color.php */
/* Location: ./application/models/Color.php */