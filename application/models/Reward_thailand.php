<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Reward_thailand extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "reward_thailand";
    }
    function _list($start,$length,$query){
        $this->db->select('reward_thailand.*');
        $this->db->group_start();
            $this->db->or_like('nama',$query,'both');
        $this->db->group_end();
        return $this->db->get('reward_thailand', $length, $start)->result();
    }
    function _count_all(){
        $this->db->select('reward_thailand.*');
        return $this->db->count_all_results('reward_thailand');
    }
    function _count_filter($query){
        $this->db->select('reward_thailand.*');
        $this->db->group_start();
        $this->db->or_like('nama',$query,'both');
        $this->db->group_end();
        return $this->db->count_all_results('reward_thailand');
    }

    function spesial_list($start,$length,$query){
        $this->db->select('spesial_tiket.*');
        $this->db->group_start();
        $this->db->or_like('nama',$query,'both');
        $this->db->group_end();
        return $this->db->get('spesial_tiket', $length, $start)->result();
    }
    function spesial_count_all(){
        $this->db->select('spesial_tiket.*');
        return $this->db->count_all_results('spesial_tiket');
    }
    function spesial_count_filter($query){
        $this->db->select('spesial_tiket.*');
        $this->db->group_start();
        $this->db->or_like('nama',$query,'both');
        $this->db->group_end();
        return $this->db->count_all_results('spesial_tiket');
    }
}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */