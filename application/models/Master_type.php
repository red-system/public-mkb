<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_type extends CI_Model {
    var $db = null;
    var $table_name = '';
    public function __construct()
    {
        parent::__construct();
        $this->table_name= 'type';
        $this->db = $this->load->database('master', TRUE);
    }
    function delete_by_id($field_name,$field_id){
        $this->db->where($field_name, $field_id);
        return $this->db->delete($this->table_name);
    }
    function delete_all(){
        return $this->db->empty_table($this->table_name);
    }
    function insert($data){

        return $this->db->insert($this->table_name, $data);
    }
    function last_id(){
        return $this->db->insert_id();
    }
    function update_by_id($field_name,$field_id,$data){
        $this->db->where($field_name, $field_id);
        return $this->db->update($this->table_name, $data);
    }
    function detail($field_name,$field_id){
        $this->db->where($field_name, $field_id);
        return $this->db->get($this->table_name);
    }
    function all_list(){
        return $this->db->get($this->table_name)->result();
    }
    function row_by_id($id){
        $this->db->where("id", $id);
        return $this->db->get($this->table_name)->row();
    }
    function string_to_number($string){
        $temp = str_replace(".", "", $string);
        $temp = str_replace(",", "", $temp);
        return $temp;
    }
    function string_to_decimal($string){
        $temp = str_replace(",", "", $string);
        return $temp;
    }
    function start_trans(){
        $this->db->trans_begin();
    }
    function result_trans(){
        if ($this->db->trans_status() === FALSE)
            return FALSE;

        $this->db->trans_commit();
        return TRUE;
    }
    function get_exists($field_name,$field_id){
        $this->db->from($this->table_name);
        $this->db->where($field_name, $field_id);
        $query = $this->db->count_all_results();
        if($query>0) {
            $exist = 1;
        } else {
            $exist = 0;
        }
        return $exist;
    }
    function list($start,$length,$query){
        $this->db->like('id', $query, 'BOTH');
        $this->db->like('type_name', $query, 'BOTH');
        $this->db->order_by('id', 'desc');
        return $this->db->get('type', $length, $start)->result();
    }
    function count_filter($query){
        $this->db->like('id', $query, 'BOTH');
        $this->db->like('type_name', $query, 'BOTH');
        $this->db->order_by('id', 'desc');
        return $this->db->count_all_results('type');
    }
    function count_all(){
        return $this->db->count_all_results('type');
    }
    function all_menu(){
        $this->db->order_by('urutan','asc');
        return $this->db->get('menu')->result();
    }
    function submenu_by_menu($menu_id){
        $this->db->where('menu_id',$menu_id);
        return $this->db->get('sub_menu')->result();
    }
    function all_submenu(){
        return $this->db->get('sub_menu')->result();
    }
    function get_akses_role($id){
        $this->db->select('type_role');
        $this->db->where('id', $id);
        return $this->db->get('type')->row()->type_role;
    }
}

/* End of file User.php */
/* Location: ./application/models/User.php */