<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey_response_item extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "survey_response_item";
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */