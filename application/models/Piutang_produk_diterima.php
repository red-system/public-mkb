<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_produk_diterima extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "piutang_produk_diterima";
	}

}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */