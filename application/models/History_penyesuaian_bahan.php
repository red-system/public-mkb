<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_penyesuaian_bahan extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'history_penyesuaian_bahan';
	}
	function history_list($start,$length,$query){
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('history_penyesuaian_bahan_id', 'desc');
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}		
		return $this->db->get('history_penyesuaian_bahan', $length, $start)->result();
	}
	function history_count_filter($query){
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('bahan_kode', $query, 'BOTH'); 
			$this->db->or_like('bahan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}		
		return $this->db->count_all_results('history_penyesuaian_bahan');
	}
	function history_count(){
		return $this->db->count_all_results('history_penyesuaian_bahan');
	}	
	function first_date(){
		$this->db->order_by('tanggal');
		$get = $this->db->get('history_penyesuaian_bahan');
		$date = date("Y-m-d");
		if($get->num_rows()>0){
			$date = $get->row()->tanggal;
		}
		return $date;
	}
}

/* End of file Histori_penyesuaian_bahan.php */
/* Location: ./application/models/Histori_penyesuaian_bahan.php */