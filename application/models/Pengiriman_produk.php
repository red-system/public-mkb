<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengiriman_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "pengiriman_produk";
	}
    
	function pengiriman_by_id($po_produk_id){

		$jumlah_order = 'po_produk_detail.jumlah_qty';
        $sisa = '(if(sum(po_produk_detail.sisa_qty)is null, 0, sum(po_produk_detail.sisa_qty)))';
        $terkirim = '('. $jumlah_order.' - '. $sisa .')';

        // $this->db->select('po_produk.*, po_produk_detail.jumlah_qty as jumlah_order, '. $terkirim .' as terkirim, (po_produk_detail.jumlah - '.$terkirim.') as sisa');
        $this->db->select('po_produk.*, '.$jumlah_order.' as jumlah_order, '. $terkirim .' as terkirim, '.$sisa.' as sisa, po_produk_detail.po_produk_detail_id, po_produk_detail.produk_id');
        // $this->db->join('pengiriman_produk', 'po_produk.po_produk_id = pengiriman_produk.po_produk_id','left');
        $this->db->join('po_produk_detail', 'po_produk.po_produk_id = po_produk_detail.po_produk_id', 'left');
        $this->db->where('po_produk.po_produk_id ', $po_produk_id);
        return $this->db->get('po_produk')->row();
	}
    function pengiriman_by_pengiriman_id($pengiriman_produk_id){

        $jumlah_order = 'po_produk_detail.jumlah_qty';
        $sisa = '(if(sum(po_produk_detail.sisa_qty)is null, 0, sum(po_produk_detail.sisa_qty)))';
        $terkirim = '('. $jumlah_order.' - '. $sisa .')';

        // $this->db->select('po_produk.*, po_produk_detail.jumlah_qty as jumlah_order, '. $terkirim .' as terkirim, (po_produk_detail.jumlah - '.$terkirim.') as sisa');
        $this->db->select('po_produk.*, '.$jumlah_order.' as jumlah_order, '. $terkirim .' as terkirim, '.$sisa.' as sisa, po_produk_detail.po_produk_detail_id, po_produk_detail.produk_id');
        // $this->db->join('pengiriman_produk', 'po_produk.po_produk_id = pengiriman_produk.po_produk_id','left');
        $this->db->join('po_produk_detail', 'po_produk.po_produk_id = po_produk_detail.po_produk_id', 'left');
        $this->db->join('pengiriman_produk', 'po_produk.po_produk_id = pengiriman_produk.po_produk_id');
        $this->db->where('pengiriman_produk.pengiriman_produk_id ', $pengiriman_produk_id);
        return $this->db->get('po_produk')->row();
    }
	function item_po($po_produk_id){
	    $this->db->select('produk.produk_nama,po_produk_detail.*');
	    $this->db->join('produk','produk.produk_id = po_produk_detail.produk_id');
	    $this->db->where('po_produk_detail.po_produk_id',$po_produk_id);
	    return $this->db->get('po_produk_detail')->result();
    }

	function pengiriman_produk_count($po_produk_id){
		$this->db->select('pengiriman_produk.*');
		$this->db->where('pengiriman_produk.po_produk_id', $po_produk_id);
		return $this->db->count_all_results('pengiriman_produk');
	}
	function pengiriman_produk_filter($po_produk_id,$query){
		$this->db->select('pengiriman_produk.*');
		$this->db->where('pengiriman_produk.po_produk_id', $po_produk_id);
		return $this->db->count_all_results('pengiriman_produk');
	}
	function pengiriman_produk_list($start,$length,$query,$po_produk_id){
		$this->db->select('pengiriman_produk.*,pengiriman_produk.total_qty_pengiriman as "old_jumlah",produk.produk_nama');
        $this->db->join('produk', 'produk.produk_id = pengiriman_produk.produk_id','left');
		$this->db->order_by('pengiriman_produk.pengiriman_produk_id', 'desc');
		$this->db->where('pengiriman_produk.po_produk_id', $po_produk_id);
		return $this->db->get('pengiriman_produk',$length,$start)->result();		
	}

	function get_last_batch($po_produk_id, $pembayaran_hutang_produk_id){
		$this->db->select('pengiriman_produk.batch');
        $this->db->join('po_produk', 'pengiriman_produk.po_produk_id = po_produk.po_produk_id', 'left');
		$this->db->join('hutang_produk', 'po_produk.po_produk_id = hutang_produk.po_produk_id', 'left');
        $this->db->join('pembayaran_hutang_produk', 'hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id', 'left');
		
		if ($po_produk_id != null) {
			$this->db->where('pengiriman_produk.po_produk_id', $po_produk_id);
		}

		if ($pembayaran_hutang_produk_id != null) {
			$this->db->where('pembayaran_hutang_produk.pembayaran_hutang_produk_id', $pembayaran_hutang_produk_id);
		}

		$this->db->order_by('pengiriman_produk.batch', 'desc');
		$this->db->limit(1);

		$data = $this->db->get('pengiriman_produk')->row();

		if ($data->batch != null) {
			$batch = $data->batch + 1;
		}else{
			$batch = 1;
		}
		
		return $batch;		
	}

	function pengiriman_by_pembayaran_hutang_id($pembayaran_hutang_produk_id){

        $jumlah_order = 'display_jumlah_order.jumlah_order';
        $terkirim = '(if((pengiriman_produk.total_qty_pengiriman)is null, 0, (pengiriman_produk.total_qty_pengiriman)))';
        $sisa = '('. $jumlah_order.' - '. $terkirim .')';

        $this->db->select('pengiriman_produk.total_qty_pengiriman,pembayaran_hutang_produk.pembayaran_hutang_produk_id, po_produk_detail.produk_id, pembayaran_hutang_produk.no_pembayaran, '. $jumlah_order .' as jumlah_order, '. $terkirim .' as terkirim, '.$sisa.' as sisa');
        $this->db->join('display_jumlah_order', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = display_jumlah_order.pembayaran_hutang_produk_id', 'left');
        $this->db->join('hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id', 'left');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('po_produk_detail', 'po_produk.po_produk_id = po_produk_detail.po_produk_id', 'left');
        $this->db->join('pengiriman_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = pengiriman_produk.pembayaran_hutang_produk_id', 'left');
        $this->db->where('pembayaran_hutang_produk.pembayaran_hutang_produk_id ', $pembayaran_hutang_produk_id);
        $this->db->where('hutang_produk.produk_id = po_produk_detail.produk_id',null,false);
        return $this->db->get('pembayaran_hutang_produk')->row();
	}

	function pengiriman_by_hutang_produk_count($pembayaran_hutang_produk_id){
		$this->db->select('pengiriman_produk.*');
		$this->db->where('pengiriman_produk.pembayaran_hutang_produk_id', $pembayaran_hutang_produk_id);
		return $this->db->count_all_results('pengiriman_produk');
	}
	function pengiriman_by_hutang_produk_filter($pembayaran_hutang_produk_id,$query){
		$this->db->select('pengiriman_produk.*');
		$this->db->where('pengiriman_produk.pembayaran_hutang_produk_id', $pembayaran_hutang_produk_id);
		return $this->db->count_all_results('pengiriman_produk');
	}
	function pengiriman_by_hutang_produk_list($start,$length,$query,$pembayaran_hutang_produk_id){
		$this->db->select('pengiriman_produk.*,pengiriman_produk.total_qty_pengiriman as "old_jumlah"');
		$this->db->order_by('pengiriman_produk.pengiriman_produk_id', 'desc');
		$this->db->where('pengiriman_produk.pembayaran_hutang_produk_id', $pembayaran_hutang_produk_id);
		return $this->db->get('pengiriman_produk',$length,$start)->result();		
	}

	function get_produk_detail_by_hutang($pembayaran_hutang_produk_id){
		$this->db->select('pembayaran_hutang_produk.no_pembayaran, po_produk.po_produk_id, po_produk.status_penerimaan as status_po, po_produk_detail.po_produk_detail_id, po_produk_detail.jumlah_qty, po_produk_detail.sisa_qty, po_produk_detail.produk_id, reseller.nama as reseller_nama');
		$this->db->join('po_produk', 'po_produk_detail.po_produk_id = po_produk.po_produk_id','left');
		$this->db->join('hutang_produk', 'po_produk.po_produk_id = hutang_produk.po_produk_id','left');
        $this->db->join('pembayaran_hutang_produk', 'hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller', 'hutang_produk.reseller_id = reseller.reseller_id','left');
		$this->db->order_by('po_produk.po_produk_id', 'asc');
        $this->db->where('pembayaran_hutang_produk.pembayaran_hutang_produk_id ', $pembayaran_hutang_produk_id);
        $this->db->where('po_produk_detail.sisa_qty !=', 0);
		return $this->db->get('po_produk_detail')->result();		
	}

	function get_produk_detail_by_po($po_produk_id){
		$this->db->select('po_produk.po_produk_id, po_produk.status_penerimaan as status_po, po_produk_detail.po_produk_detail_id, po_produk_detail.jumlah_qty, po_produk_detail.sisa_qty, lokasi.lokasi_nama');
		$this->db->join('po_produk', 'po_produk_detail.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('lokasi', 'po_produk.lokasi_reseller = lokasi.lokasi_id','left');
		$this->db->order_by('po_produk.po_produk_id', 'asc');
        $this->db->where('po_produk.po_produk_id', $po_produk_id);
        $this->db->where('po_produk_detail.sisa_qty !=', 0);
		return $this->db->get('po_produk_detail')->result();		
	}
    function get_produk_detail_by_pengiriman($po_produk_id,$produk_id){
        $this->db->select('po_produk.po_produk_id, po_produk.status_penerimaan as status_po, po_produk_detail.po_produk_detail_id, po_produk_detail.jumlah_qty, po_produk_detail.sisa_qty, lokasi.lokasi_nama');
        $this->db->join('po_produk', 'po_produk_detail.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('lokasi', 'po_produk.lokasi_reseller = lokasi.lokasi_id','left');
        $this->db->order_by('po_produk.po_produk_id', 'asc');
        $this->db->where('po_produk.po_produk_id', $po_produk_id);
        $this->db->where('po_produk_detail.sisa_qty !=', 0);
        if($produk_id!=null){
            $this->db->where('po_produk_detail.produk_id', $produk_id);
        }
        return $this->db->get('po_produk_detail')->result();
    }

	function count_total_sisa($pembayaran_hutang_produk_id){
        $this->db->select('(if(sum(po_produk_detail.sisa_qty)is null, 0, sum(po_produk_detail.sisa_qty))) as sisa');
        $this->db->join('hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id', 'left');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('po_produk_detail', 'po_produk.po_produk_id = po_produk_detail.po_produk_id', 'left');
        $this->db->where('pembayaran_hutang_produk.pembayaran_hutang_produk_id ', $pembayaran_hutang_produk_id);
        return $this->db->get('pembayaran_hutang_produk')->row();
	}

    function insert_pengiriman($data){
		return $this->db->insert('pengiriman_produk', $data);
	}
	function edit_pengiriman($data,$pengiriman_id)	{
		$this->db->where('pengiriman_id', $pengiriman_id);
		return $this->db->update('pengiriman_produk', $data);
	}
	function delete_pengiriman($pengiriman_produk_id){
		$this->db->where('pengiriman_produk_id', $pengiriman_produk_id);
		return $this->db->delete('pengiriman_produk');
	}

	function detail_pengiriman($pengiriman_produk_id){
		$this->db->where('pengiriman_produk_id', $pengiriman_produk_id);
		$this->db->from('pengiriman_produk');
		return $this->db->get()->row();
	}

	// accounting
	function pengiriman_by_po_all(){
        $this->db->select('po_produk.po_produk_no, pengiriman_produk.tanggal_pengiriman, pengiriman_produk.total_qty_pengiriman, pengiriman_produk.keterangan_pengiriman');
        $this->db->join('po_produk', 'pengiriman_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->where('pengiriman_produk.status_pengiriman', '1');
        $this->db->order_by('pengiriman_produk.tanggal_pengiriman', 'DESC');
        return $this->db->count_all_results('pengiriman_produk');
    }
    function pengiriman_by_po_filter($query){
        
        $this->db->select('po_produk.po_produk_no, pengiriman_produk.tanggal_pengiriman, pengiriman_produk.total_qty_pengiriman, pengiriman_produk.keterangan_pengiriman');
        $this->db->join('po_produk', 'pengiriman_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->where('pengiriman_produk.status_pengiriman', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('pengiriman_produk.tanggal_pengiriman >=', $this->input->get('start_tanggal'));
            $this->db->where('pengiriman_produk.tanggal_pengiriman <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.tanggal_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.total_qty_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.keterangan_pengiriman', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pengiriman_produk.tanggal_pengiriman', 'DESC');

        return $this->db->count_all_results('pengiriman_produk');
    }
    function pengiriman_by_po_list($start,$length,$query){
        $this->db->select('po_produk.po_produk_no, pengiriman_produk.tanggal_pengiriman, pengiriman_produk.total_qty_pengiriman, pengiriman_produk.keterangan_pengiriman');
        $this->db->join('po_produk', 'pengiriman_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->where('pengiriman_produk.status_pengiriman', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('pengiriman_produk.tanggal_pengiriman >=', $this->input->get('start_tanggal'));
            $this->db->where('pengiriman_produk.tanggal_pengiriman <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.tanggal_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.total_qty_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.keterangan_pengiriman', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pengiriman_produk.tanggal_pengiriman', 'DESC');
        
        return $this->db->get('pengiriman_produk', $length, $start)->result();
    }


	function pengiriman_by_pembayaran_hutang_all(){
        $this->db->select('pembayaran_hutang_produk.no_pembayaran, pengiriman_produk.tanggal_pengiriman, pengiriman_produk.total_qty_pengiriman, pengiriman_produk.keterangan_pengiriman');
        $this->db->join('pembayaran_hutang_produk', 'pengiriman_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id');
        $this->db->where('pengiriman_produk.status_pengiriman', '1');
        $this->db->order_by('pengiriman_produk.tanggal_pengiriman', 'DESC');
        return $this->db->count_all_results('pengiriman_produk');
    }
    function pengiriman_by_pembayaran_hutang_filter($query){
        $this->db->select('pembayaran_hutang_produk.no_pembayaran, pengiriman_produk.tanggal_pengiriman, pengiriman_produk.total_qty_pengiriman, pengiriman_produk.keterangan_pengiriman');
        $this->db->join('pembayaran_hutang_produk', 'pengiriman_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id');
        $this->db->where('pengiriman_produk.status_pengiriman', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('pengiriman_produk.tanggal_pengiriman >=', $this->input->get('start_tanggal'));
            $this->db->where('pengiriman_produk.tanggal_pengiriman <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.total_qty_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.tanggal_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.keterangan_pengiriman', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pengiriman_produk.tanggal_pengiriman', 'DESC');

        return $this->db->count_all_results('pengiriman_produk');
    }
    function pengiriman_by_pembayaran_hutang_list($start,$length,$query){
        $this->db->select('pembayaran_hutang_produk.no_pembayaran, pengiriman_produk.tanggal_pengiriman, pengiriman_produk.total_qty_pengiriman, pengiriman_produk.keterangan_pengiriman');
        $this->db->join('pembayaran_hutang_produk', 'pengiriman_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id');
        $this->db->where('pengiriman_produk.status_pengiriman', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('pengiriman_produk.tanggal_pengiriman >=', $this->input->get('start_tanggal'));
            $this->db->where('pengiriman_produk.tanggal_pengiriman <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.total_qty_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.tanggal_pengiriman', $query, 'BOTH');
        $this->db->or_like('pengiriman_produk.keterangan_pengiriman', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('pengiriman_produk.tanggal_pengiriman', 'DESC');
        
        return $this->db->get('pengiriman_produk', $length, $start)->result();
    }

    function mail_by_hutang_id($pembayaran_hutang_produk_id){

        $this->db->select('pembayaran_hutang_produk.no_pembayaran, SUM( po_produk_detail.jumlah_qty ) AS jumlah_order, SUM( po_produk_detail.sisa_qty ) AS sisa_order, hutang_produk.reseller_id');
        $this->db->join('hutang_produk', 'pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id', 'left');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('po_produk_detail', 'po_produk.po_produk_id = po_produk_detail.po_produk_id', 'left');
        $this->db->where('pembayaran_hutang_produk.pembayaran_hutang_produk_id ', $pembayaran_hutang_produk_id);
        return $this->db->get('pembayaran_hutang_produk')->row();
	}
	
}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */