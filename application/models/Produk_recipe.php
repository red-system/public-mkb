<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_recipe extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "produk_recipe";
    }
    function count_all(){
        $this->db->join('produk','produk.produk_id='.$this->table_name.'.produk_id');
        $this->db->join('jenis_produk','produk.produk_jenis_id=jenis_produk.jenis_produk_id');
        return $this->db->count_all_results($this->table_name);
    }
    function count_filter($query){
        $this->db->join('produk','produk.produk_id='.$this->table_name.'.produk_id');
        $this->db->join('jenis_produk','produk.produk_jenis_id=jenis_produk.jenis_produk_id');
        $this->db->group_start();
            $this->db->or_like('produk_kode',$query);
            $this->db->or_like('produk_nama',$query);
            $this->db->or_like('jenis_produk_nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results($this->table_name);
    }
    function list($start,$length,$query){
        $this->db->select($this->table_name.'.*,produk_kode,produk_nama,jenis_produk_nama');
        $this->db->join('produk','produk.produk_id='.$this->table_name.'.produk_id');
        $this->db->join('jenis_produk','produk.produk_jenis_id=jenis_produk.jenis_produk_id');
        $this->db->group_start();
        $this->db->or_like('produk_kode',$query);
        $this->db->or_like('produk_nama',$query);
        $this->db->or_like('jenis_produk_nama',$query);
        $this->db->group_end();
        return $this->db->get($this->table_name,$length,$start)->result();
    }
    function produk_all(){
        $this->db->join('jenis_produk','jenis_produk.jenis_produk_id = produk.produk_jenis_id');
        $this->db->join('produk_recipe','produk_recipe.produk_id = produk.produk_id','left');
        $this->db->group_start();
            $this->db->where('produk_recipe_id is null');
            if(isset($_SESSION["redpos_recipe"]["produk_recipe_id"])){
                $this->db->or_where('produk_recipe_id is null');
             }
        $this->db->group_end();
        return $this->db->count_all_results('produk');
    }
    function produk_filter($query){
        $this->db->join('jenis_produk','jenis_produk.jenis_produk_id = produk.produk_jenis_id');
        $this->db->join('produk_recipe','produk_recipe.produk_id = produk.produk_id','left');
        $this->db->group_start();
            $this->db->where('produk_recipe_id is null');
            if(isset($_SESSION["redpos_recipe"]["produk_recipe_id"])){
                $this->db->or_where('produk_recipe_id is null');
            }
        $this->db->group_end();
        $this->db->group_start();
            $this->db->or_like('produk_kode',$query);
            $this->db->or_like('produk_nama',$query);
            $this->db->or_like('jenis_produk_nama',$query);
        $this->db->group_end();
        return $this->db->count_all_results('produk');
    }
    function produk_list($start,$length,$query){
        $this->db->select('produk.produk_id,produk_kode,produk_nama,jenis_produk_nama,jenis_produk_nama');
        $this->db->join('jenis_produk','jenis_produk.jenis_produk_id = produk.produk_jenis_id');
        $this->db->join('produk_recipe','produk_recipe.produk_id = produk.produk_id','left');
        $this->db->group_start();
            $this->db->where('produk_recipe_id is null');
            if(isset($_SESSION["redpos_recipe"]["produk_recipe_id"])){
                $this->db->or_where('produk_recipe_id is null');
            }
        $this->db->group_end();
        $this->db->group_start();
            $this->db->or_like('produk_kode',$query);
            $this->db->or_like('produk_nama',$query);
            $this->db->or_like('jenis_produk_nama',$query);
        $this->db->group_end();
        return $this->db->get('produk',$length,$start)->result();
    }
    function row_by_id($id){
        $this->db->select('produk_recipe.*,produk_nama');
        $this->db->join('produk','produk.produk_id = produk_recipe.produk_id');
        $this->db->where('produk_recipe_id',$id);
        return $this->db->get('produk_recipe')->row();
    }

    function recipe_by_id($id){
		$this->db->select('produk_recipe.*, produk_recipe_detail.produk_recipe_detail_id, produk.produk_nama, bahan.bahan_nama, bahan.bahan_harga, produk_recipe_detail.takaran , satuan.satuan_nama, produk_recipe_detail.harga_pokok');
		$this->db->join('produk', 'produk_recipe.produk_id = produk.produk_id');
		$this->db->join('produk_recipe_detail', 'produk_recipe.produk_recipe_id = produk_recipe_detail.produk_recipe_id');
		$this->db->join('bahan', 'produk_recipe_detail.bahan_id = bahan.bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->where('produk_recipe.produk_recipe_id', $id);
		return $this->db->get('produk_recipe')->result();
	}

}

/* End of file Size.php */
/* Location: ./application/models/Size.php */
