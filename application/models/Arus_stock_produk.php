<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arus_stock_produk extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "arus_stock_produk";
    }
    function row_by_id($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('arus_stock_produk')->row();
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */