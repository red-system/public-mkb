<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey_response extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "survey_response";
    }
    function pernah_isi_survey($reseller_id){
        $result = 1;
        $this->db->where('reseller_id',$reseller_id);
        $queryResult = $this->db->get('survey_response')->result();
        if(sizeof($queryResult)<1){
            $result = 0;
        }
        return $result;
    }
    function jumlah_responden(){
        $this->db->select("count(survey_response_id)as jumlah");
        $result = $this->db->get("survey_response")->row();
        if($result!=null){
            return $result->jumlah;
        }else{
            return 0;
        }
    }
    function reseller_pindah(){
        $this->db->select("count(survey_response_id)as jumlah");
        $this->db->where("pindah","ya");
        $result = $this->db->get("survey_response")->row();
        if($result!=null){
            return $result->jumlah;
        }else{
            return 0;
        }
    }
    function reseller_tetap(){
        $this->db->select("count(survey_response_id)as jumlah");
        $this->db->where("pindah","tidak");
        $result = $this->db->get("survey_response")->row();
        if($result!=null){
            return $result->jumlah;
        }else{
            return 0;
        }
    }
    function total_nilai(){
        $this->db->select("sum(nilai)as jumlah");
        $result = $this->db->get("survey_response_item")->row();
        if($result!=null){
            return $result->jumlah;
        }else{
            return 0;
        }
    }
    function list($start,$length,$query){
        $this->db->select("reseller.nama,old_leader.nama as leader_lama,new_leader.nama as leader_baru,survey_response.alasan_pindah");
        $this->db->join("reseller","reseller.reseller_id = survey_response.reseller_id");
        $this->db->join("reseller as old_leader","reseller.leader = old_leader.reseller_id");
        $this->db->join("reseller as new_leader","survey_response.leader_id = new_leader.reseller_id");
        $this->db->group_start();
        $this->db->or_like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('old_leader.nama', $query, 'BOTH');
        $this->db->or_like('new_leader.nama', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->get('survey_response', $length, $start)->result();
    }
    function filter($query){
        $this->db->select("reseller.nama,old_leader.nama as leader_lama,new_leader.nama as new_leader");
        $this->db->join("reseller","reseller.reseller_id = survey_response.reseller_id");
        $this->db->join("reseller as old_leader","reseller.leader = old_leader.reseller_id");
        $this->db->join("reseller as new_leader","survey_response.leader_id = new_leader.reseller_id");
        $this->db->group_start();
        $this->db->or_like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('old_leader.nama', $query, 'BOTH');
        $this->db->or_like('new_leader.nama', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('survey_response');
    }
    function all(){
        $this->db->select("reseller.nama,old_leader.nama as leader_lama,new_leader.nama as leader_baru");
        $this->db->join("reseller","reseller.reseller_id = survey_response.reseller_id");
        $this->db->join("reseller as old_leader","reseller.leader = old_leader.reseller_id");
        $this->db->join("reseller as new_leader","survey_response.leader_id = new_leader.reseller_id");
        return $this->db->count_all('survey_response');
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */