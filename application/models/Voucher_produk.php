<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher_produk extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "voucher_produk";
    }
    function insert($data)
    {
        if(!isset($data['po_produk_detail_id'])){
            return false;
        }
        return parent::insert($data);
    }

    function po_produk_by_id($po_produk_id){
        $this->db->where('voucher_produk.po_produk_id', $po_produk_id);
        $this->db->select('po_produk.*,reseller.nama as pemilik,suplier.suplier_nama,hutang.tenggat_pelunasan,b.nama as ditukarkan,voucher_produk.voucher_code,piutang_produk.status as status_piutang,hutang_produk.status as status_hutang');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('hutang', 'hutang.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('piutang_produk', 'piutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id', 'left');
        $this->db->join('hutang_produk', 'hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id', 'left');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        $this->db->join('reseller','reseller.reseller_id = voucher_produk.reseller_pemilik');
        return $this->db->get('po_produk')->row();
    }
    function po_by_voucher_code($code){
        $this->db->where('voucher_produk.voucher_code', $code);
        $this->db->select('po_produk.*,reseller.nama as pemilik,suplier.suplier_nama,hutang.tenggat_pelunasan,b.nama as ditukarkan,voucher_produk.voucher_code,piutang_produk.status as status_piutang,hutang_produk.status as status_hutang');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('hutang', 'hutang.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('piutang_produk', 'piutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id', 'left');
        $this->db->join('hutang_produk', 'hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id', 'left');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        $this->db->join('reseller','reseller.reseller_id = voucher_produk.reseller_pemilik');
        return $this->db->get('po_produk')->row();
    }
    function po_produk_by_voucher_id($po_produk_id){
        $this->db->where('voucher_produk.voucher_produk_id', $po_produk_id);
        $this->db->select('po_produk.*,suplier.suplier_nama,hutang.tenggat_pelunasan,b.nama as ditukarkan,voucher_produk.voucher_code,piutang_produk.status as status_piutang,hutang_produk.status as status_hutang');
        $this->db->join('suplier', 'suplier.suplier_id = po_produk.suplier_id','left');
        $this->db->join('hutang', 'hutang.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('voucher_produk', 'voucher_produk.po_produk_id = po_produk.po_produk_id', 'left');
        $this->db->join('piutang_produk', 'piutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id', 'left');
        $this->db->join('hutang_produk', 'hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id', 'left');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        return $this->db->get('po_produk')->row();
    }
    function cek_voucher_po($po_produk){
        $this->db->where('po_produk_id',$po_produk);
        $voucher = $this->db->get('voucher_produk')->row();
        if($voucher==null){
            return true;
        }else {
            return false;
        }
    }
    function voucher_all(){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_produk.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");

        return $this->db->count_all_results('voucher_produk');
    }
    function voucher_filter($query){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_produk.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        $this->db->group_start();
            $this->db->or_like('voucher_produk.voucher_code',$query);
            $this->db->or_like('b.nama',$query);
            $this->db->or_like('voucher_produk.status_penukaran',$query);
        $this->db->group_end();
        return $this->db->count_all_results('voucher_produk');
    }
    function voucher_list($start,$length,$query){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_produk.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->select('voucher_produk.*,owner.*,po_produk.grand_total as total,b.nama as ditukarkan');
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        $this->db->group_start();
        $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('voucher_produk.status_penukaran',$query);
        $this->db->group_end();
        $this->db->order_by('voucher_produk.voucher_produk_id','desc');
        return $this->db->get('voucher_produk',$length,$start)->result();
    }

    function voucher_all_new(){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_produk.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('po_produk_detail','voucher_produk.po_produk_detail_id = po_produk_detail.po_produk_detail_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");

        return $this->db->count_all_results('voucher_produk');
    }
    function voucher_filter_new($query){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_produk.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('po_produk_detail','voucher_produk.po_produk_detail_id = po_produk_detail.po_produk_detail_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        $this->db->group_start();
        $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('voucher_produk.status_penukaran',$query);
        $this->db->group_end();
        return $this->db->count_all_results('voucher_produk');
    }
    function voucher_list_new($start,$length,$query){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_produk.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->select('voucher_produk.*,owner.*,po_produk.grand_total as total,b.nama as ditukarkan');
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('po_produk_detail','voucher_produk.po_produk_detail_id = po_produk_detail.po_produk_detail_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar',"left");
        $this->db->group_start();
        $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('voucher_produk.status_penukaran',$query);
        $this->db->group_end();
        $this->db->order_by('voucher_produk.voucher_produk_id','desc');
        return $this->db->get('voucher_produk',$length,$start)->result();
    }

    function cekVoucher($po_produk_id,$reseller_id){
        $this->db->where('reseller_pemilik',$reseller_id);
        $this->db->where('po_produk_id',$po_produk_id);
        $check = $this->db->get('voucher_produk')->row();
        $result = true;
        if($check!=null){
            $result = false;
        }
        return $result;
    }
    function laporan_penukaran_produk_agen_all($reseller_id){
        $this->db->where("reseller_penukar",$reseller_id);
        $this->db->join('reseller','reseller.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');

        return $this->db->count_all_results('voucher_produk');
    }
    function laporan_penukaran_produk_agen_filter($query,$reseller_id){
        $this->db->where("reseller_penukar",$reseller_id);
        $this->db->join('reseller','reseller.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_penukaran >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_penukaran <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->or_like('voucher_produk.tanggal_penukaran',$query);
        $this->db->group_end();
        return $this->db->count_all_results('voucher_produk');
    }
    function laporan_penukaran_produk_agen_list($start,$length,$query,$reseller_id){
        $this->db->where("reseller_penukar",$reseller_id);
        $this->db->select('voucher_produk.*,reseller.nama as pemilik,po_produk.grand_total,(po_produk.grand_total * 0.05) as potensi_bonus');
        $this->db->join('reseller','reseller.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('po_produk','voucher_produk.po_produk_id = po_produk.po_produk_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_penukaran >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_penukaran <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->or_like('voucher_produk.tanggal_penukaran',$query);
        $this->db->group_end();
        return $this->db->get('voucher_produk',$length,$start)->result();
    }
    function history_penukaran_voucher_count($reseller_id){
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');

        $this->db->where('voucher_produk.reseller_penukar',$reseller_id);

        return $this->db->count_all_results('voucher_produk');
    }
    function history_penukaran_voucher_filter($query,$reseller_id){
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->where('voucher_produk.reseller_penukar',$reseller_id);
        $this->db->group_start();
            $this->db->or_like('a.nama',$query);
            $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->group_end();
        if(isset($_GET['estimasi_start'])&&$this->input->get('estimasi_start')!=""){
            $this->db->where('tanggal_penukaran >=', $this->input->get('estimasi_start'));
        }
        if(isset($_GET['estimasi_end'])&&$this->input->get('estimasi_end')!=""){
            $this->db->where('tanggal_penukaran <=', $this->input->get('estimasi_end'));
        }
        return $this->db->count_all_results('voucher_produk');
    }
    function history_penukaran_voucher_list($start,$length,$query,$reseller_id){
        $this->db->select("voucher_produk.tanggal_penukaran,a.nama,voucher_produk.voucher_code,sum(po_produk_detail.jumlah_qty) as total_produk,po_produk.grand_total");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_produk.reseller_penukar');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->join('po_produk_detail','po_produk.po_produk_id = po_produk_detail.po_produk_id');
        $this->db->where('voucher_produk.reseller_penukar',$reseller_id);
        $this->db->group_start();
        $this->db->or_like('a.nama',$query);
        $this->db->or_like('voucher_produk.voucher_code',$query);
        $this->db->group_end();
        if(isset($_GET['estimasi_start'])&&$this->input->get('estimasi_start')!=""){
            $this->db->where('tanggal_penukaran >=', $this->input->get('estimasi_start'));
        }
        if(isset($_GET['estimasi_end'])&&$this->input->get('estimasi_end')!=""){
            $this->db->where('tanggal_penukaran <=', $this->input->get('estimasi_end'));
        }
        $this->db->group_by('po_produk.po_produk_id');
        return $this->db->get('voucher_produk',$length,$start)->result();
    }

    function manage_voucher_list($start,$length,$query){
        $this->db->select("a.nama,voucher_produk.voucher_code, b.nama as leader, po_produk.po_produk_no, voucher_produk.created_at");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','a.leader = b.reseller_id');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->group_start();
        $this->db->or_like('a.nama',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('voucher_produk.created_at',$query);
        $this->db->or_like('po_produk.po_produk_no',$query);
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('voucher_produk.created_at >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('voucher_produk.created_at <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('voucher_produk.status_penukaran', 'Tersedia');
        return $this->db->get('voucher_produk',$length,$start)->result();
    }
    function manage_voucher_all(){
        $this->db->select("a.nama,voucher_produk.voucher_code, b.nama as leader, po_produk.po_produk_no, voucher_produk.created_at");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','a.leader = b.reseller_id');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->where('voucher_produk.status_penukaran', 'Tersedia');
        return $this->db->count_all_results('voucher_produk');
    }
    function manage_voucher_filter($query){

        $this->db->select("a.nama,voucher_produk.voucher_code, b.nama as leader, po_produk.po_produk_no, voucher_produk.created_at");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik');
        $this->db->join('reseller as b','a.leader = b.reseller_id');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->group_start();
        $this->db->or_like('a.nama',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('po_produk.po_produk_no',$query);
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('voucher_produk.created_at >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('voucher_produk.created_at <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('voucher_produk.status_penukaran', 'Tersedia');
        return $this->db->count_all_results('voucher_produk');
    }

    function manage_voucher_list_terpakai($start,$length,$query){
        $this->db->select("a.nama,voucher_produk.voucher_code, b.nama as leader, po_produk.po_produk_no, voucher_produk.created_at, c.nama as penukar, voucher_produk.tanggal_penukaran");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik', 'left');
        $this->db->join('reseller as b','a.leader = b.reseller_id', 'left');
        $this->db->join('reseller as c','c.reseller_id = voucher_produk.reseller_penukar', 'left');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->group_start();
        $this->db->or_like('a.nama',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('c.nama',$query);
        $this->db->or_like('voucher_produk.created_at',$query);
        $this->db->or_like('voucher_produk.tanggal_penukaran',$query);
        $this->db->or_like('po_produk.po_produk_no',$query);
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('voucher_produk.tanggal_penukaran >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('voucher_produk.tanggal_penukaran <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('voucher_produk.status_penukaran', 'Terpakai');
        return $this->db->get('voucher_produk',$length,$start)->result();
    }
    function manage_voucher_all_terpakai(){
        $this->db->select("a.nama,voucher_produk.voucher_code, b.nama as leader, po_produk.po_produk_no, voucher_produk.created_at, c.nama as penukar, voucher_produk.tanggal_penukaran");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik', 'left');
        $this->db->join('reseller as b','a.leader = b.reseller_id', 'left');
        $this->db->join('reseller as c','c.reseller_id = voucher_produk.reseller_penukar', 'left');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->where('voucher_produk.status_penukaran', 'Terpakai');
        return $this->db->count_all_results('voucher_produk');
    }
    function manage_voucher_filter_terpakai($query){

        $this->db->select("a.nama,voucher_produk.voucher_code, b.nama as leader, po_produk.po_produk_no, voucher_produk.created_at, c.nama as penukar, voucher_produk.tanggal_penukaran");
        $this->db->join('reseller as a','a.reseller_id = voucher_produk.reseller_pemilik', 'left');
        $this->db->join('reseller as b','a.leader = b.reseller_id', 'left');
        $this->db->join('reseller as c','c.reseller_id = voucher_produk.reseller_penukar', 'left');
        $this->db->join('po_produk','po_produk.po_produk_id = voucher_produk.po_produk_id');
        $this->db->group_start();
        $this->db->or_like('a.nama',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('c.nama',$query);
        $this->db->or_like('voucher_produk.created_at',$query);
        $this->db->or_like('voucher_produk.tanggal_penukaran',$query);
        $this->db->or_like('po_produk.po_produk_no',$query);
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('voucher_produk.tanggal_penukaran >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('voucher_produk.tanggal_penukaran <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('voucher_produk.status_penukaran', 'Terpakai');
        return $this->db->count_all_results('voucher_produk');
    }

}

/* End of file Tipe_pembayaran.php */
/* Location: ./application/models/Tipe_pembayaran.php */