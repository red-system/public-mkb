<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_retur extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "piutang_retur";
    }
    function count_all(){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = piutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = piutang_retur.voucher_retur_id');
        return $this->db->count_all_results('piutang_retur');
    }
    function count_filter($query){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = piutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = piutang_retur.voucher_retur_id');
        $this->db->group_start();
        $this->db->or_like('retur_agen.no_retur',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('piutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('piutang_retur.status',$query,'both');
        $this->db->group_end();
        return $this->db->count_all_results('piutang_retur');
    }
    function _list($start,$lenth,$query){
        $this->db->select('piutang_retur.*,no_retur,voucher_code');
        $this->db->join('retur_agen','retur_agen.retur_agen_id = piutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = piutang_retur.voucher_retur_id');
        $this->db->group_start();
        $this->db->or_like('retur_agen.no_retur',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('piutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('piutang_retur.status',$query,'both');
        $this->db->group_end();
        return $this->db->get('piutang_retur',$lenth,$start)->result();
    }
    function pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $data["pembayaran_hutang_retur_id"] = $pembayaran_hutang_produk_id;
        $this->db->where_in('piutang_retur_id',$hutang_produk_id);
        return $this->db->update('piutang_retur',$data);
    }

}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */