<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_bahan_diterima extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "piutang_bahan_diterima";
	}

}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */