<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe_pembayaran extends MY_Model {

public function __construct()
	{
		parent::__construct();
		$this->table_name = "tipe_pembayaran";
	}	
	function tipe_pembayaran_list($start,$length,$query){

		$this->db->like('tipe_pembayaran_id', $query, 'BOTH'); 
		$this->db->or_like('tipe_pembayaran_kode', $query, 'BOTH'); 
		$this->db->or_like('tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->order_by('tipe_pembayaran_id', 'desc');
		return $this->db->get('tipe_pembayaran', $length, $start)->result();
	}
	function tipe_pembayaran_count_filter($query){
		$this->db->like('tipe_pembayaran_id', $query, 'BOTH'); 
		$this->db->or_like('tipe_pembayaran_kode', $query, 'BOTH'); 
		$this->db->or_like('tipe_pembayaran_nama', $query, 'BOTH');
		return $this->db->count_all_results('tipe_pembayaran');
	}
	function tipe_pembayaran_count_all(){
		return $this->db->count_all_results('tipe_pembayaran');
	}	
	function is_ready_kode($tipe_pembayaran_id,$kode){
		$this->db->where('tipe_pembayaran_kode', $kode);
		$data = $this->db->get('tipe_pembayaran')->row();
		if($data != null){
			if($data->tipe_pembayaran_id == $tipe_pembayaran_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	function all_kas(){
		$this->db->where('jenis_pembayaran', "kas");
		return $this->db->get('tipe_pembayaran')->result();
	}
	function all_kredit(){
		$this->db->where('jenis_pembayaran', "kredit");
		return $this->db->get('tipe_pembayaran')->result();
	}
	function all_po(){
        $this->db->where('used_on_po', 1);
        return $this->db->get('tipe_pembayaran')->result();
    }
}

/* End of file Tipe_pembayaran.php */
/* Location: ./application/models/Tipe_pembayaran.php */