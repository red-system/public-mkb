<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'produk';
	}
	function produk_list($start,$length,$query){
		
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
		if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->select($sql);
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH');
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}

	function produk_stock_list($start,$length,$query){
		
		$sql = 'produk.*,jenis_produk_nama, sum(stock_produk.stock_produk_qty) as stock, satuan.satuan_nama';

		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');

		$this->db->select($sql);
		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH');
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->where('lokasi.lokasi_tipe', 'Gudang');
		$this->db->where('stock_produk.delete_flag', "0"); 
		// $this->db->where('stock_produk.stock_produk_qty >', "0"); 
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}

    function produk_list_money_back($query,$lokasi_id){
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
        $sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
        $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
        $this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$lokasi_id.') a', 'a.produk_id = produk.produk_id', 'left');
        $this->db->select($sql);
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH');
        $this->db->or_like('produk.harga_eceran', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');
        return $this->db->get('produk')->result();
    }
    function produk_list_money_back_display($start,$length,$query,$lokasi_id){
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
        $sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
        $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
        $this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$lokasi_id.') a', 'a.produk_id = produk.produk_id', 'left');
        $this->db->select($sql);
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH');
        $this->db->or_like('produk.harga_eceran', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');
        return $this->db->get('produk',$length,$start)->result();
    }
    function produk_count_all_money_back($lokasi_id){
        $this->db->select('produk.*');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
        $this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$lokasi_id.') a', 'a.produk_id = produk.produk_id', 'left');
        $this->db->group_by('produk.produk_id');
        return $this->db->count_all_results('produk');
    }
    function produk_count_filter_money_back($query,$lokasi_id){
        $this->db->select('produk.*');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
        $this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$lokasi_id.') a', 'a.produk_id = produk.produk_id', 'left');
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH');
        $this->db->or_like('produk.harga_eceran', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        return $this->db->count_all_results('produk');
    }
	function all_list(){
		$this->db->select('produk.*,satuan_nama');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		return $this->db->get('produk')->result();
	}
	function konversi_produk_list(){
		$this->db->select('produk.produk_id,produk.produk_kode');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		return $this->db->get('produk')->result();
	}
	function row_by_id($id){
		$this->db->select('produk.*');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->where('produk_id', $id);
		return $this->db->get('produk')->row();
	}
	function produk_count_all(){
	    $this->db->select('produk.*');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}

	function produk_stock_count_all(){
		$this->db->select('produk.*,jenis_produk_nama, sum(stock_produk.stock_produk_qty) as stock, satuan.satuan_nama');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->where('lokasi.lokasi_tipe', 'Gudang');
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}

	function produk_count_filter($query){
        $this->db->select('produk.*');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		if(isset($_SESSION["redpos_login"]['lokasi_id'])){
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
			
		}		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH'); 
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}

	function produk_count_stock_filter($query){
        $this->db->select('produk.*,jenis_produk_nama, sum(stock_produk.stock_produk_qty) as stock, satuan.satuan_nama');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->where('lokasi.lokasi_tipe', 'Gudang');
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH'); 
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}

    function low_produk_list($start,$length,$query){
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'display_stock_produk_lokasi.produk_id = produk.produk_id','left');
        $sql = 'produk.*,jenis_produk_nama, display_stock_produk_lokasi.jumlah as stock, satuan_nama';
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');

        } else {
            $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
        }
        $this->db->select($sql);
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->where('a.jumlah <= produk.produk_minimal_stock','',false);
        $this->db->or_where('a.jumlah is null','',false);
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');

        return $this->db->get('produk', $length, $start)->result();
    }
    function low_produk_count_filter($query){
	    $this->db->select('produk.*');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'display_stock_produk_lokasi.produk_id = produk.produk_id','left');
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');

        } else {
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
        }
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->where('a.jumlah <= produk.produk_minimal_stock','',false);
        $this->db->or_where('a.jumlah is null','',false);
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');
        return $this->db->count_all_results('produk');
    }
    function low_produk_count_all(){
        $this->db->select('produk.*');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'display_stock_produk_lokasi.produk_id = produk.produk_id','left');
        if(isset($_SESSION["redpos_login"]['lokasi_id'])){
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION["redpos_login"]['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');

        } else {
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
        }
        $this->db->group_start();
        $this->db->where('a.jumlah <= produk.produk_minimal_stock','',false);
        $this->db->or_where('a.jumlah is null','',false);
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');
        return $this->db->count_all_results('produk');
    }
	function produk_by_id($produk_id){
		$this->db->select('produk.*,jenis_produk.jenis_produk_nama,jenis_produk.jenis_produk_kode');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->where('produk.produk_id', $produk_id);
		return $this->db->get('produk')->row();
	}
	function laporan_produk_list($start,$length,$query){
		
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
		$this->db->select($sql);		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_GET['jenis_produk_id'])){
			$this->db->where('jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}			
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function laporan_produk_count_all(){
        $this->db->select('produk.*');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}
	function laporan_produk_count_filter($query){
        $this->db->select('produk.*');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_GET['jenis_produk_id'])){
			$this->db->where('jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}			
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}
	function laporan_produk_count_filter_print($query,$jenis_produk_id,$satuan_id){
        $this->db->select('produk.*');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if($jenis_produk_id!=null){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if($satuan_id!=null){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}
	function laporan_produk_list_print($start,$length,$query,$jenis_produk_id,$satuan_id){
		
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
		$this->db->select($sql);		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if($jenis_produk_id!=null){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if($satuan_id!=null){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}	
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function produk_produksi_count_all(){
        $this->db->select('produk.*');
		$this->db->join('produk_recipe','produk.produk_id = produk_recipe.produk_id','left');
        $this->db->join('produk_recipe_detail','produk_recipe_detail.produk_recipe_id = produk_recipe.produk_recipe_id','left');
		$this->db->join('bahan','produk_recipe_detail.bahan_id = bahan.bahan_id','left');
		$this->db->group_by('produk.produk_id');
	   	return $this->db->count_all_results('produk');
	}
	function produk_produksi_count_filter($query){
        $this->db->select('produk.*');
        $this->db->join('produk_recipe','produk.produk_id = produk_recipe.produk_id','left');
        $this->db->join('produk_recipe_detail','produk_recipe_detail.produk_recipe_id = produk_recipe.produk_recipe_id','left');
        $this->db->join('bahan','produk_recipe_detail.bahan_id = bahan.bahan_id','left');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->group_start();
			$this->db->or_like('jenis_produk_nama',$query);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->count_all_results('produk');
	}
	function produk_produksi_list($start,$length,$query){
		$this->db->select('produk.produk_id,produk.produk_kode,produk.produk_nama,display_stock_produk.jumlah,group_concat(produk_recipe_detail.konversi_takaran separator "|") as potong,group_concat(bahan.bahan_nama separator "|") as bahan_nama, group_concat(produk_recipe_detail.bahan_id separator "|") as bahan_id, group_concat(bahan.bahan_harga separator "|") as bahan_harga ');
        $this->db->join('produk_recipe','produk.produk_id = produk_recipe.produk_id','left');
        $this->db->join('produk_recipe_detail','produk_recipe_detail.produk_recipe_id = produk_recipe.produk_recipe_id','left');
        $this->db->join('bahan','produk_recipe_detail.bahan_id = bahan.bahan_id','left');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_start();
			$this->db->or_like('jenis_produk_nama',$query);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('display_stock_produk.jumlah');
		return $this->db->get('produk',$length,$start)->result();
	}

	function get_exist_with_id($id,$kode_barang){
	    $this->db->from('produk');
	    $this->db->where('produk_id !=', $id);
	    $this->db->where('produk_kode', $kode_barang);
	    $query = $this->db->count_all_results();
	    if($query>0) {
	      	$exist = 1;
	    } else {
	      	$exist = 0;
	    }
	    return $exist;
	}
	function get_produk_by_barcode($barcode){
	    $this->db->where('barcode',$barcode);
	    return $this->db->get('produk')->row();
    }

    function history_beli_produk($reseller_id){
	    $po_detail = "(SELECT po_produk_detail.produk_id,sum(po_produk_detail.jumlah_qty) as total,reseller.type from po_produk_detail
INNER JOIN po_produk on po_produk.po_produk_id = po_produk_detail.po_produk_id
INNER JOIN mykindofbeauty_master.login as login on po_produk.lokasi_reseller = login.lokasi_id
INNER JOIN reseller on reseller.reseller_id = login.reseller_id
WHERE reseller.reseller_id = ".$reseller_id."
and po_produk.tanggal_uang_diterima is not null
GROUP BY po_produk_detail.produk_id)";
	    $this->db->select('produk.produk_id,produk.produk_nama,produk.hpp_global,produk.minimal_termin_res,produk.minimal_termin_super,produk.minimal_rede_res,produk.minimal_rede_super,produk.fullpayment_res,produk.fullpayment_super,produk.old_fullpayment_res,produk.old_fullpayment_super,if(po_detail.total is null,0,po_detail.total) as total,po_detail.type,if(po_detail.type="super agen",if(produk.fullpayment_super>if(po_detail.total is null,0,po_detail.total),0,1),if(produk.fullpayment_res>if(po_detail.total is null,0,po_detail.total),0,1)) as fullpayment');
	    $this->db->join($po_detail." as po_detail",'po_detail.produk_id = produk.produk_id','left');
	    return $this->db->get('produk')->result();
    }
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
