<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "notif";
        $this->load->library('main');
    }
    
    function notif_count_all(){
        $this->db->select('*');
        $this->db->order_by('notif_date', 'DESC');
        $this->db->order_by('notif_id', 'DESC');
        return $this->db->count_all_results('notif');
    }
    function notif_count_filter($query){
        $this->db->select('*');
        $this->db->group_start();
        $this->db->like('notif_title', $query, 'BOTH');
        $this->db->or_like('notif_date', $query, 'BOTH');
        $this->db->or_like('notif_description', $query, 'BOTH');
        $this->db->group_end();
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('notif_date >=', $this->input->get('start_tanggal'));
            $this->db->where('notif_date <=', $this->input->get('end_tanggal'));
        };
        $this->db->order_by('notif_date', 'DESC');
        $this->db->order_by('notif_id', 'DESC');
        return $this->db->count_all_results('notif');
    }
    function notif_list($start,$length,$query){
        $this->db->select('*');
        $this->db->group_start();
        $this->db->like('notif_title', $query, 'BOTH');
        $this->db->or_like('notif_date', $query, 'BOTH');
        $this->db->or_like('notif_description', $query, 'BOTH');
        $this->db->group_end();
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('notif_date >=', $this->input->get('start_tanggal'));
            $this->db->where('notif_date <=', $this->input->get('end_tanggal'));
        };
        $this->db->order_by('notif_date', 'DESC');
        $this->db->order_by('notif_id', 'DESC');
        return $this->db->get('notif', $length, $start)->result();
    }
    
    function notif_user($reseller_id){
        $notifs = $this->db->select('notif.*')
                    ->where('notif.notif_status', 'publish')
                    ->order_by('notif_date', 'DESC')
                    ->order_by('notif_id', 'DESC')
                    ->get('notif')
                    ->result();
        
        $i = 0;
        $notif_user = array();
        foreach ($notifs as $key) {
            $detail = $this->db->select('notif_detail_id')
                        ->where('reseller_id', $reseller_id)
                        ->where('notif_id', $key->notif_id)
                        ->get('notif_detail')->num_rows();
            if ($detail == 0) {
                if($key->notif_date != null){
                    $time = strtotime($key->notif_date);
                    $key->notif_date = date('d-m-Y',$time);
                }
                $key->notif_title_lbl = (strlen($key->notif_title) >= 25 ) ? $this->main->short_desc($key->notif_title) : $key->notif_title;
                $key->notif_description_lbl = (strlen($key->notif_description) >= 20 ) ? $this->main->short_desc($key->notif_description) : $key->notif_description;
                $key->view_url = base_url().'notification/detail-notification/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->notif_id));
                $key->no = $i;
                $i++;
                array_push($notif_user, $key);

                if ($i == 3) {
                    break;
                }
            }
        }

        return $notif_user;
    }

    function notif_user_count($reseller_id){
        $notifs = $this->db->select('notif.*')
                    ->where('notif.notif_status', 'publish')
                    ->order_by('notif_date', 'DESC')
                    ->order_by('notif_id', 'DESC')
                    ->get('notif')
                    ->result();
        
        $notif_user = array();
        $i = 0;
        foreach ($notifs as $key) {
            $detail = $this->db->select('notif_detail_id')
                        ->where('reseller_id', $reseller_id)
                        ->where('notif_id', $key->notif_id)
                        ->get('notif_detail')->num_rows();
            if ($detail == 0) {
                $i++;
                array_push($notif_user, $i);
            }
        }

        return $notif_user;
    }

    function notif_check($where){
        $this->db->select('notif_detail.notif_detail_id');
        $this->db->where($where);
        return $this->db->get('notif_detail');
    }

}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */