<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_reward_reseller extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "rule_reward_reseller";
    }
    function _list($start,$length,$query,$reseller_id){
        $this->db->join('reseller','reward_reseller.reseller_id = reseller.reseller_id');
        $this->db->where('reseller.reseller_id',$reseller_id);
        return $this->db->get('reward_reseller', $length, $start)->result();
    }
    function _count_all($reseller_id){
        $this->db->join('reseller','reward_reseller.reseller_id = reseller.reseller_id');
        $this->db->where('reseller.reseller_id',$reseller_id);
        return $this->db->count_all_results('reward_reseller');
    }
    function _count_filter($query,$reseller_id){
        $this->db->join('reseller','reward_reseller.reseller_id = reseller.reseller_id');
        $this->db->where('reseller.reseller_id',$reseller_id);
        return $this->db->count_all_results('reward_reseller');
    }
    function min_penjualan(){
        $this->db->select('if(penjualan is null,0,penjualan) as penjualan');

        $result = $this->db->get('rule_reward_reseller',1,0)->row();
        if(is_null($result)){
            return 0;
        }else{
            return $result->penjualan;
        }
    }
}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */