<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_bonus extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "send_bonus";
    }
    function get_no_generate(){
        $prefix = 'KRM-'.date('ymd');
        $this->db->select('no_generate');
        $this->db->like('no_generate',$prefix,'both');
        $this->db->order_by('no_generate','desc');
        $result = $this->db->get('send_bonus')->row();
        if($result==null){
            $prefix = $prefix.'-1';
        }else{
            $temp  = $result->no_generate;
            $temp = str_replace($prefix.'-','',$temp);
            $temp = $temp +1;
            $prefix = $prefix.'-'.$temp;
        }
        return $prefix;
    }
    function bonusListPrint($no_generate){
        $this->db->select("send_bonus.*,send_bonus.bonus_net as total_bonus,reseller.bank_atas_nama,nama_bank,kode_bank,reseller.email,reseller.bank_rekening,if(npwp.case is null,2,npwp.case) as case_res");
        $this->db->join('reseller','reseller.reseller_id = send_bonus.reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->join('npwp','reseller.reseller_id = npwp.reseller_id','left');
        $this->db->where("no_generate",$no_generate);
        $this->db->where('reseller.demo','0');
        return $this->db->get("send_bonus")->result();
    }
    function bonusTotal($no_generate){
        $this->db->select("sum(send_bonus.bonus_net) as total_bonus");
        $this->db->join('reseller','reseller.reseller_id = send_bonus.reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->where_in("no_generate",$no_generate);
        $bonus = $this->db->get("send_bonus")->row();
        if($bonus==null){
            return 0;
        }else{
            return $bonus->total_bonus;
        }
    }
    function changeToCancel($arr_id){
        $data["status"] = "cancel";
        $this->db->where_in('send_bonus_id',$arr_id);
        return $this->db->update("send_bonus",$data);
    }
    function changeToSuccessWithDate($arr_id, $date){
        $data["status"] = "success";
        $data["tanggal_success"] = $date;
        $this->db->where_in('send_bonus_id',$arr_id);
        return $this->db->update("send_bonus",$data);
    }
    function send_prosessing_bonus_list($start,$length,$search,$type='bonus'){
        $this->db->select('send_bonus.send_bonus_id,send_bonus.no_generate,reseller.nama,send_bonus.tanggal_proccess as tanggal,send_bonus.jumlah_bonus as gross,send_bonus.admin,send_bonus.pajak,send_bonus.bonus_net');
        $this->db->join('reseller','send_bonus.reseller_id = reseller.reseller_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_proccess >=', $this->input->get('tanggal_start').' 00:00:00');
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_proccess <=', $this->input->get('tanggal_end').' 23:59:59');
        }
        $this->db->where('send_bonus.status','proccess');
        $this->db->where('send_bonus.type',$type);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$search,'both');
        $this->db->or_like('send_bonus.no_generate',$search,'both');
        $this->db->or_like('send_bonus.tanggal_proccess',$search,'both');
        $temp = str_replace(',','',$search);
        $temp = str_replace('.','',$temp);
        $this->db->or_like('send_bonus.jumlah_bonus',$temp,'both');
        $this->db->or_like('send_bonus.admin',$temp,'both');
        $this->db->or_like('send_bonus.pajak',$temp,'both');
        $this->db->or_like('send_bonus.bonus_net',$temp,'both');
        $this->db->group_end();
        return $this->db->get('send_bonus', $length, $start)->result();

    }
    function send_prosessing_bonus_filter($search,$type='bonus'){
        $this->db->join('reseller','send_bonus.reseller_id = reseller.reseller_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_proccess >=', $this->input->get('tanggal_start').' 00:00:00');
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_proccess <=', $this->input->get('tanggal_end').' 23:59:59');
        }
        $this->db->where('send_bonus.status','proccess');
        $this->db->where('send_bonus.type',$type);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$search,'both');
        $this->db->or_like('send_bonus.no_generate',$search,'both');
        $this->db->or_like('send_bonus.tanggal_proccess',$search,'both');
        $temp = str_replace(',','',$search);
        $temp = str_replace('.','',$temp);
        $this->db->or_like('send_bonus.jumlah_bonus',$temp,'both');
        $this->db->or_like('send_bonus.admin',$temp,'both');
        $this->db->or_like('send_bonus.pajak',$temp,'both');
        $this->db->or_like('send_bonus.bonus_net',$temp,'both');
        $this->db->group_end();
        return $this->db->count_all_results('send_bonus');
    }
    function send_prosessing_bonus_all($type='bonus'){
        $this->db->join('reseller','send_bonus.reseller_id = reseller.reseller_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_proccess >=', $this->input->get('tanggal_start').' 00:00:00');
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_proccess <=', $this->input->get('tanggal_end').' 23:59:59');
        }
        $this->db->where('send_bonus.status','proccess');
        $this->db->where('send_bonus.type',$type);
        return $this->db->count_all_results('send_bonus');
    }

    function terkirim_bonus_list($start,$length,$search,$type='bonus'){
        $this->db->select('send_bonus.send_bonus_id,send_bonus.no_generate,reseller.nama,send_bonus.tanggal_proccess as tanggal,send_bonus.tanggal_success as tanggal_terkirim,send_bonus.jumlah_bonus as gross,send_bonus.admin,send_bonus.pajak,send_bonus.bonus_net');
        $this->db->join('reseller','send_bonus.reseller_id = reseller.reseller_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_proccess >=', $this->input->get('tanggal_start').' 00:00:00');
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_proccess <=', $this->input->get('tanggal_end').' 23:59:59');
        }
        $this->db->where('send_bonus.status','success');
        $this->db->where('send_bonus.type',$type);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$search,'both');
        $this->db->or_like('send_bonus.no_generate',$search,'both');
        $this->db->or_like('send_bonus.tanggal_proccess',$search,'both');
        $temp = str_replace(',','',$search);
        $temp = str_replace('.','',$temp);
        $this->db->or_like('send_bonus.jumlah_bonus',$temp,'both');
        $this->db->or_like('send_bonus.admin',$temp,'both');
        $this->db->or_like('send_bonus.pajak',$temp,'both');
        $this->db->or_like('send_bonus.bonus_net',$temp,'both');
        $this->db->group_end();
        return $this->db->get('send_bonus', $length, $start)->result();

    }
    function terkirim_bonus_filter($search,$type='bonus'){
        $this->db->join('reseller','send_bonus.reseller_id = reseller.reseller_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_proccess >=', $this->input->get('tanggal_start').' 00:00:00');
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_proccess <=', $this->input->get('tanggal_end').' 23:59:59');
        }
        $this->db->where('send_bonus.status','success');
        $this->db->where('send_bonus.type',$type);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$search,'both');
        $this->db->or_like('send_bonus.no_generate',$search,'both');
        $this->db->or_like('send_bonus.tanggal_proccess',$search,'both');
        $temp = str_replace(',','',$search);
        $temp = str_replace('.','',$temp);
        $this->db->or_like('send_bonus.jumlah_bonus',$temp,'both');
        $this->db->or_like('send_bonus.admin',$temp,'both');
        $this->db->or_like('send_bonus.pajak',$temp,'both');
        $this->db->or_like('send_bonus.bonus_net',$temp,'both');
        $this->db->group_end();
        return $this->db->count_all_results('send_bonus');
    }
    function terkirim_bonus_all($type='bonus'){
        $this->db->join('reseller','send_bonus.reseller_id = reseller.reseller_id');
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_proccess >=', $this->input->get('tanggal_start').' 00:00:00');
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_proccess <=', $this->input->get('tanggal_end').' 23:59:59');
        }
        $this->db->where('send_bonus.status','success');
        $this->db->where('send_bonus.type',$type);
        return $this->db->count_all_results('send_bonus');
    }

}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */