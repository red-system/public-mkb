<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_produk_detail extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "retur_produk_detail";
    }

    function insert($data)
    {
        $data["created_at"] = Date("Y-m-d H:i:s");
        return parent::insert($data); // TODO: Change the autogenerated stub
    }
    function jumlah_subtotal_retur_detail($retur_produk_id,$produk_id){
        $this->db->select('jumlah,sub_total_retur,keterangan');
        $this->db->where('retur_produk_id',$retur_produk_id);
        $this->db->where('produk_id',$produk_id);
        return $this->db->get($this->table_name)->row();
    }
    function data_by_retur_id($retur_produk_id){
        $this->db->where('retur_produk_id',$retur_produk_id);
        return $this->db->get($this->table_name)->result();
    }
    function all(){
        $this->db->join('retur_produk','retur_produk.id=retur_produk_detail.retur_produk_id');
        $this->db->join('staff','retur_produk.staff_id=staff.staff_id','left');
        $this->db->join('produk','retur_produk_detail.produk_id=produk.produk_id','left');
        $this->db->join('lokasi','retur_produk.lokasi_id=lokasi.lokasi_id','left');
        $this->db->join('po_produk','retur_produk_detail.po_produk_id=po_produk.po_produk_id');
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        $this->db->where("retur_produk_detail.jumlah <>",0);
        return $this->db->count_all_results('retur_produk_detail');
    }
    function filter($query){
        $this->db->join('retur_produk','retur_produk.id=retur_produk_detail.retur_produk_id');
        $this->db->join('staff','retur_produk.staff_id=staff.staff_id','left');
        $this->db->join('produk','retur_produk_detail.produk_id=produk.produk_id','left');
        $this->db->join('lokasi','retur_produk.lokasi_id=lokasi.lokasi_id','left');
        $this->db->join('po_produk','retur_produk_detail.po_produk_id=po_produk.po_produk_id');
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        if($this->input->get('staff_id')!=''){
            $this->db->where('retur_produk.staff_id',$this->input->get('staff_id'));
        }
        if($this->input->get('po_produk_no')!=''){
            $this->db->like('po_produk.po_produk_no',$this->input->get('po_produk_no'));
        }
        if($this->input->get('suplier_nama')!=''){
            $this->db->like('suplier.suplier_nama',$this->input->get('suplier_nama'));
        }
        if($this->input->get('produk_nama')!=''){
            $this->db->like('produk.produk_nama',$this->input->get('produk_nama'));
        }
        if($this->input->get('lokasi_id')!=''){
            $this->db->where('retur_produk.lokasi_id',$this->input->get('lokasi_id'));
        }
        if($this->input->get('tanggal_start')!=""){
            $this->db->where('retur_produk.tanggal >=', $this->input->get('tanggal_start')." 00:00:00");
        }
        if($this->input->get('tanggal_end')!=""){
            $this->db->where('retur_produk.tanggal <=', $this->input->get('tanggal_end')." 23:59:59");
        }
        $this->db->group_start();
        $this->db->or_like('produk.produk_nama',$query);
        $this->db->or_like('lokasi.lokasi_nama',$query);
        $this->db->or_like('staff.staff_nama',$query);
        $this->db->or_like('staff.staff_nama',$query);
        $this->db->or_like('lokasi.lokasi_nama',$query);
        $this->db->or_like('produk.produk_nama',$query);
        $this->db->or_like('po_produk.po_produk_no',$query);
        $this->db->or_like('suplier.suplier_nama',$query);
        $num = $this->string_to_number($query);
        $this->db->or_like('retur_produk_detail.jumlah',$num);
        $this->db->or_like('retur_produk_detail.sub_total_retur',$num);
        $this->db->or_like('retur_produk_detail.harga',$num);
        $this->db->group_end();
        $this->db->where("retur_produk_detail.jumlah <>",0);
        return $this->db->count_all_results('retur_produk_detail');
    }
    function list($start,$length,$query){
        $this->db->select('retur_produk_detail.*,retur_produk_detail.sub_total_retur as total_retur,retur_produk.tanggal, produk.produk_nama,lokasi.lokasi_nama,staff.staff_nama,suplier.suplier_nama,po_produk.po_produk_no');
        $this->db->join('retur_produk','retur_produk.id=retur_produk_detail.retur_produk_id');
        $this->db->join('staff','retur_produk.staff_id=staff.staff_id','left');
        $this->db->join('produk','retur_produk_detail.produk_id=produk.produk_id','left');
        $this->db->join('lokasi','retur_produk.lokasi_id=lokasi.lokasi_id','left');
        $this->db->join('po_produk','retur_produk_detail.po_produk_id=po_produk.po_produk_id');
        $this->db->join('suplier','po_produk.suplier_id=suplier.suplier_id');
        if($this->input->get('staff_id')!=''){
            $this->db->where('retur_produk.staff_id',$this->input->get('staff_id'));
        }
        if($this->input->get('po_produk_no')!=''){
            $this->db->like('po_produk.po_produk_no',$this->input->get('po_produk_no'));
        }
        if($this->input->get('suplier_nama')!=''){
            $this->db->like('suplier.suplier_nama',$this->input->get('suplier_nama'));
        }
        if($this->input->get('produk_nama')!=''){
            $this->db->like('produk.produk_nama',$this->input->get('produk_nama'));
        }
        if($this->input->get('lokasi_id')!=''){
            $this->db->where('retur_produk.lokasi_id',$this->input->get('lokasi_id'));
        }
        if($this->input->get('tanggal_start')!=""){
            $this->db->where('retur_produk.tanggal >=', $this->input->get('tanggal_start')." 00:00:00");
        }
        if($this->input->get('tanggal_end')!=""){
            $this->db->where('retur_produk.tanggal <=', $this->input->get('tanggal_end')." 23:59:59");
        }
        $this->db->group_start();
        $this->db->or_like('produk.produk_nama',$query);
        $this->db->or_like('lokasi.lokasi_nama',$query);
        $this->db->or_like('staff.staff_nama',$query);
        $this->db->or_like('staff.staff_nama',$query);
        $this->db->or_like('lokasi.lokasi_nama',$query);
        $this->db->or_like('produk.produk_nama',$query);
        $this->db->or_like('po_produk.po_produk_no',$query);
        $this->db->or_like('suplier.suplier_nama',$query);
        $num = $this->string_to_number($query);
        $this->db->or_like('retur_produk_detail.jumlah',$num);
        $this->db->or_like('retur_produk_detail.sub_total_retur',$num);
        $this->db->or_like('retur_produk_detail.harga',$num);
        $this->db->group_end();
        $this->db->where("retur_produk_detail.jumlah <>",0);
        return $this->db->get('retur_produk_detail',$length,$start)->result();
    }
}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */