<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_hutang_produk extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "pembayaran_hutang_produk";
    }
    function getPembayaranNo($reseller_id){
        $prefix = "RPP/".date("ymd")."/".$reseller_id."/";
        $this->db->select("no_pembayaran");
        $this->db->or_like("no_pembayaran",$prefix);
        $this->db->order_by("no_pembayaran","desc");
        $row = $this->db->get("pembayaran_hutang_produk")->row();
        if($row==null){
            return $prefix."1";
        }else {
            return $prefix.(str_replace($prefix,"",$row->no_pembayaran)+1);
        }
    }
    function _list($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,reseller.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');

        $this->db->group_start();
            $this->db->or_like('no_pembayaran',$query,'both');
            $this->db->or_like('reseller.nama',$query,'both');
            $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","waiting");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","waiting");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->where("pembayaran_hutang_produk.status","waiting");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_approve($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,reseller.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","approve");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        $this->db->order_by('pembayaran_hutang_produk.tanggal','desc');
        $this->db->order_by('pembayaran_hutang_produk.pembayaran_hutang_produk_id','asc');
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_approve($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","approve");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_approve(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->where("pembayaran_hutang_produk.status","approve");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_refuse($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,b.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","refuse");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_refuse($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","refuse");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_refuse(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->where("pembayaran_hutang_produk.status","refuse");
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_cash($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,reseller.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","waiting");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_cash($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","waiting");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_cash(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->where("pembayaran_hutang_produk.status","waiting");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_approve_cash($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,reseller.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","approve");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_approve_cash($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","approve");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_approve_cash(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id');
        $this->db->where("pembayaran_hutang_produk.status","approve");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_refuse_cash($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,b.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","refuse");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_refuse_cash($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.status","refuse");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_refuse_cash(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->where("pembayaran_hutang_produk.status","refuse");
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_res($start,$length,$query,$reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,reseller.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->or_where("hutang_produk.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_piutang.reseller_id",$reseller_id);
        $this->db->group_end();
        $this->db->order_by('pembayaran_hutang_produk.pembayaran_hutang_produk_id','desc');
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_res($query,$reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();

        $this->db->group_start();
        $this->db->or_where("hutang_produk.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_piutang.reseller_id",$reseller_id);
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_res($reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_where("hutang_produk.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_piutang.reseller_id",$reseller_id);
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.cash",0);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _list_res_cash($start,$length,$query,$reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*,reseller.nama");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->or_where("hutang_produk.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_piutang.reseller_id",$reseller_id);
        $this->db->group_end();
        $this->db->order_by('pembayaran_hutang_produk.pembayaran_hutang_produk_id','desc');
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->get('pembayaran_hutang_produk',$length,$start)->result();
    }
    function _filter_res_cash($query,$reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_produk.tanggal',$query,'both');
        $this->db->group_end();

        $this->db->group_start();
        $this->db->or_where("hutang_produk.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_piutang.reseller_id",$reseller_id);
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function _all_res_cash($reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_produk.*");
        $this->db->join('hutang_produk','pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->join('refuse_pembayaran_piutang','pembayaran_hutang_produk.pembayaran_hutang_produk_id = refuse_pembayaran_piutang.pembayaran_hutang_produk_id','left');
        $this->db->join('reseller','hutang_produk.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_piutang.reseller_id','left');
        $this->db->group_start();
        $this->db->or_where("hutang_produk.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_piutang.reseller_id",$reseller_id);
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_produk.cash",1);
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }

    function _hutang_all_res($reseller_id){
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->where("hutang_produk.reseller_id",$reseller_id);
        $this->db->where("hutang_produk.pembayaran_hutang_produk_id is null",null,true);
        $this->db->group_start();
        $this->db->where("hutang_produk.status","Hutang");

        if(isset($_SESSION['po_produk']["pembayaran_hutang_produk"])){
            $this->db->or_where("hutang_produk.pembayaran_hutang_produk_id",$_SESSION['po_produk']["pembayaran_hutang_produk"]);
        }
        $this->db->group_end();
        return $this->db->count_all_results('hutang_produk');
    }
    function _hutang_filter_res($query,$reseller_id){
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->group_start();
        $this->db->or_like('po_produk.po_produk_no',$query,'both');
        $this->db->or_like('voucher_produk.voucher_code',$query,'both');
        $this->db->or_like('hutang_produk.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_produk.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();

        $this->db->where("hutang_produk.reseller_id",$reseller_id);
        $this->db->where("hutang_produk.pembayaran_hutang_produk_id is null",null,true);
        $this->db->group_start();
        $this->db->where("hutang_produk.status","Hutang");
        if(isset($_SESSION['po_produk']["pembayaran_hutang_produk"])){
            $this->db->or_where("hutang_produk.pembayaran_hutang_produk_id",$_SESSION['po_produk']["pembayaran_hutang_produk"]);
        }
        $this->db->group_end();
        return $this->db->count_all_results('hutang_produk');
    }
    function _hutang_list_res($start,$length,$query,$reseller_id){
        $this->db->select('po_produk_no,voucher_produk.voucher_code,voucher_produk.voucher_produk_id,produk.produk_id,produk.min_wd,produk.produk_nama,hutang_produk.status,hutang_produk.jumlah,tanggal_pelunasan,pembayaran_hutang_produk.no_pembayaran,hutang_produk.created_at,hutang_produk.updated_at,hutang_produk_id');
        $this->db->join('po_produk','po_produk.po_produk_id = hutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = hutang_produk.voucher_produk_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_produk.produk_id');
        $this->db->join('pembayaran_hutang_produk','hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id','left');
        $this->db->group_start();
        $this->db->or_like('po_produk.po_produk_no',$query,'both');
        $this->db->or_like('voucher_produk.voucher_code',$query,'both');
        $this->db->or_like('hutang_produk.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_produk.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $this->db->where("hutang_produk.reseller_id",$reseller_id);
        $this->db->where("hutang_produk.pembayaran_hutang_produk_id is null",null,true);
        $this->db->group_start();
        $this->db->where("hutang_produk.status","Hutang");
        if(isset($_SESSION['po_produk']["pembayaran_hutang_produk"])){
            $this->db->or_where("hutang_produk.pembayaran_hutang_produk_id",$_SESSION['po_produk']["pembayaran_hutang_produk"]);
        }
        $this->db->group_end();
        return $this->db->get('hutang_produk',$length,$start)->result();
    }
    function penarikan_stock($pembayaran_hutang_produk_id){

        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        $hutang = $this->db->get("hutang_produk")->result();
        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        $piutang = $this->db->get("piutang_produk")->result();
        $this->load->library("lokasi");
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        $query = "UPDATE hutang_produk SET pembayaran_hutang_produk_id = null WHERE pembayaran_hutang_produk_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);

        $query = "UPDATE piutang_produk SET pembayaran_hutang_produk_id = null WHERE pembayaran_hutang_produk_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
        foreach ($hutang as $item){
            $this->db->where("mykindofbeauty_master.login.reseller_id",$item->reseller_id);
            $login = $this->db->get("mykindofbeauty_master.login")->row();
            $lokasi_id = $login->lokasi_id;
            $reseller_id = $login->reseller_id;
            $produk_id = $item->produk_id;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty - $item->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("stock_produk",$stock_update);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty + $item->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update("stock_produk",$stock_update);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("stock_produk")->row();
        }

    }
    function pembayaran_hutang_produk_detail($pembayaran_hutang_produk_id){
        $this->db->select("reseller.nama as nama_reseller, pembayaran_hutang_produk.pembayaran_hutang_produk_id, pembayaran_hutang_produk.no_pembayaran, pembayaran_hutang_produk.tanggal, hutang_produk.*, produk.produk_nama, reseller.reseller_id, po_produk.po_produk_no, voucher_produk.voucher_code, hutang_produk.voucher_produk_id, rp.nama as nama_reseller_pemilik");
        $this->db->where("pembayaran_hutang_produk.pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        $this->db->join("hutang_produk","pembayaran_hutang_produk.pembayaran_hutang_produk_id = hutang_produk.pembayaran_hutang_produk_id");
        $this->db->join("produk","produk.produk_id = hutang_produk.produk_id");
        $this->db->join("reseller","reseller.reseller_id = hutang_produk.reseller_id");
        $this->db->join("po_produk","hutang_produk.po_produk_id = po_produk.po_produk_id");
        $this->db->join("voucher_produk","hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id");
        $this->db->join("reseller as rp","voucher_produk.reseller_pemilik = rp.reseller_id");
        return $this->db->get("pembayaran_hutang_produk")->result();
    }

    function insert_data($reseller_id){
        $tanggal = $this->input->post("tanggal");
        $data = array();
        $no_pembayaran = $this->getPembayaranNo($reseller_id);
        $data["no_pembayaran"] = $no_pembayaran;
        $data["tanggal"] = $tanggal;
        $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
        $data["created_at"] = date("Y-m-d H:i:s");
        $this->db->trans_start();
        $this->db->insert("pembayaran_hutang_produk",$data);
        $pembayaran_hutang_produk_id = $this->db->insert_id();
        $change = array();
        $voucher_produk_id = $this->input->post("hutang_produk_id");
        $change['pembayaran_hutang_produk_id'] = $pembayaran_hutang_produk_id;
        $this->db->where_in("hutang_produk_id",$voucher_produk_id);
        $this->db->update("hutang_produk",$change);

        $this->db->where_in("piutang_produk_id",$voucher_produk_id);
        $this->db->update("piutang_produk",$change);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    function insert_data_cash($reseller_id){
        $tanggal = $this->input->post("tanggal");
        $data = array();
        $no_pembayaran = $this->getPembayaranNo($reseller_id);
        $data["no_pembayaran"] = $no_pembayaran;
        $data["tanggal"] = $tanggal;
        $data["cash"] = 1;
        $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
        $data["created_at"] = date("Y-m-d H:i:s");
        $this->db->trans_start();
        $this->db->insert("pembayaran_hutang_produk",$data);
        $pembayaran_hutang_produk_id = $this->db->insert_id();
        $change = array();
        $voucher_produk_id = $this->input->post("hutang_produk_id");
        $change['pembayaran_hutang_produk_id'] = $pembayaran_hutang_produk_id;
        $this->db->where_in("hutang_produk_id",$voucher_produk_id);
        $this->db->update("hutang_produk",$change);

        $this->db->where_in("piutang_produk_id",$voucher_produk_id);
        $this->db->update("piutang_produk",$change);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    function edit_data($reseller_id){
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $tanggal = $this->input->post("tanggal");
        $data = array();
        $data["tanggal"] = $tanggal;
        $data["staff_updated"] = $_SESSION["redpos_login"]["staff_id"];
        $data["updated_at"] = date("Y-m-d H:i:s");
        $this->db->trans_start();
        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        $this->db->update("pembayaran_hutang_produk",$data);
        $query = "update hutang_produk set pembayaran_hutang_produk_id = null where pembayaran_hutang_produk_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
        $query = "update piutang_produk set pembayaran_hutang_produk_id = null where pembayaran_hutang_produk_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
        $change = array();

        $voucher_produk_id = $this->input->post("hutang_produk_id");
        $change['pembayaran_hutang_produk_id'] = $pembayaran_hutang_produk_id;

        $this->db->where_in("voucher_produk_id",$voucher_produk_id);
        $this->db->update("hutang_produk",$change);

        $this->db->where_in("voucher_produk_id",$voucher_produk_id);
        $this->db->update("piutang_produk",$change);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    function setPembayaranNull($pembayaran_hutang_produk_id){
        $query = "update hutang_produk set pembayaran_hutang_produk_id = null where pembayaran_hutang_produk_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
        $query = "update piutang_produk set pembayaran_hutang_produk_id = null where pembayaran_hutang_produk_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
    }
    function approve($pembayaran_hutang_produk_id){
        $change = array();
        $change["status"] = "approve";
        $change["status_penerimaan"] = "Belum Diterima";
        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id,$change);
        return $this->db->update("pembayaran_hutang_produk",$change);
    }

    // accounting
    function withdraw_cash_all(){
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total) as total, hutang_produk.tanggal_pelunasan');
        $this->db->join('hutang_produk', 'hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->where('pembayaran_hutang_produk.status', 'approve');
        $this->db->where('pembayaran_hutang_produk.cash', '1');
        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function withdraw_cash_filter($query){
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total) as total, hutang_produk.tanggal_pelunasan');
        $this->db->join('hutang_produk', 'hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->where('pembayaran_hutang_produk.status', 'approve');
        $this->db->where('pembayaran_hutang_produk.cash', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('hutang_produk.tanggal_pelunasan', $query, 'BOTH');
        $this->db->group_end();

        return $this->db->count_all_results('pembayaran_hutang_produk');
    }
    function withdraw_cash_list($start,$length,$query){
        $this->db->select('reseller.nama, po_produk.po_produk_no, pembayaran_hutang_produk.no_pembayaran, (po_produk.grand_total) as total, hutang_produk.tanggal_pelunasan');
        $this->db->join('hutang_produk', 'hutang_produk.pembayaran_hutang_produk_id = pembayaran_hutang_produk.pembayaran_hutang_produk_id');
        $this->db->join('po_produk', 'hutang_produk.po_produk_id = po_produk.po_produk_id');
        $this->db->join('reseller', 'reseller.reseller_id = hutang_produk.reseller_id');
        $this->db->where('pembayaran_hutang_produk.status', 'approve');
        $this->db->where('pembayaran_hutang_produk.cash', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('hutang_produk.tanggal_pelunasan >=', $this->input->get('start_tanggal'));
            $this->db->where('hutang_produk.tanggal_pelunasan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('po_produk.po_produk_no', $query, 'BOTH');
        $this->db->or_like('pembayaran_hutang_produk.no_pembayaran', $query, 'BOTH');
        $this->db->or_like('hutang_produk.tanggal_pelunasan', $query, 'BOTH');
        $this->db->group_end();
        
        return $this->db->get('pembayaran_hutang_produk', $length, $start)->result();
    }

}

/* End of file Patern.php */
/* Location: ./application/models/Patern.php */