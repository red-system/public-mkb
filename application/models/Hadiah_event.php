<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hadiah_event extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "pengiriman_hadiah_event";
    }
    function hadiah_event_list($start,$length,$query,$reseller_id){
        $this->db->select("reseller.nama,pengiriman_hadiah_event.*,event.nama as 'nama_event'");
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.to_reseller_id',$reseller_id);
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.jumlah',$query);
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.status',$query);
        $this->db->or_like('pengiriman_hadiah_event.manage_status',$query);
        $this->db->group_end();
        return $this->db->get('pengiriman_hadiah_event', $length, $start)->result();
    }
    function hadiah_event_all($reseller_id){
        $this->db->select("reseller.nama,pengiriman_hadiah_event.*,event.nama as 'nama_event'");
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.to_reseller_id',$reseller_id);
        return $this->db->count_all_results('pengiriman_hadiah_event');
    }
    function hadiah_event_filter($query,$reseller_id){
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.to_reseller_id',$reseller_id);
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.jumlah',$query);
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.status',$query);
        $this->db->or_like('pengiriman_hadiah_event.manage_status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('pengiriman_hadiah_event');
    }
}