<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktivitas_group extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "reseller";
    }
    public function all($reseller_id){

        $sql = "SELECT * from (SELECT reseller.reseller_id,reseller.nama,'deposit/redeposit' as type,tanggal_uang_diterima as waktu,po_produk.grand_total as total from po_produk
INNER JOIN mykindofbeauty_kemiri.login as login on login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on reseller.reseller_id = login.reseller_id
WHERE reseller.reseller_id = ".$reseller_id."
and tanggal_uang_diterima is not null) as a
UNION
SELECT * from (SELECT reseller.reseller_id,reseller.nama,'penukaran voucher' as type,voucher_produk.tanggal_penukaran as waktu,(hutang_produk.jumlah*produk.hpp_global) as total from voucher_produk
INNER JOIN reseller on reseller.reseller_id = voucher_produk.reseller_penukar
INNER JOIN hutang_produk on hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id
INNER JOIN produk on hutang_produk.produk_id = produk.produk_id
WHERE reseller.reseller_id = ".$reseller_id.") as b
UNION
SELECT * from (SELECT reseller.reseller_id,reseller.nama,'wd produk' as type,hutang_produk.tanggal_pelunasan as waktu,sum(hutang_produk.jumlah*produk.hpp_global) as total from hutang_produk
INNER JOIN reseller on reseller.reseller_id = hutang_produk.reseller_id
INNER JOIN produk on produk.produk_id = hutang_produk.produk_id
WHERE reseller.reseller_id = ".$reseller_id."
AND hutang_produk.status = 'Lunas'
AND pembayaran_hutang_produk_id is not null
and tanggal_pelunasan is not null
GROUP BY pembayaran_hutang_produk_id) as c
ORDER BY waktu
;";
        return $this->db->query($sql)->num_rows();
    }
    public function list($start,$length,$reseller_id){

        $sql = "SELECT * from (SELECT reseller.reseller_id,reseller.nama,'deposit/redeposit' as type,tanggal_uang_diterima as waktu,po_produk.grand_total as total from po_produk
INNER JOIN mykindofbeauty_kemiri.login as login on login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on reseller.reseller_id = login.reseller_id
WHERE reseller.reseller_id = ".$reseller_id."
and tanggal_uang_diterima is not null) as a
UNION
SELECT * from (SELECT reseller.reseller_id,reseller.nama,'penukaran voucher' as type,voucher_produk.tanggal_penukaran as waktu,(hutang_produk.jumlah*produk.hpp_global) as total from voucher_produk
INNER JOIN reseller on reseller.reseller_id = voucher_produk.reseller_penukar
INNER JOIN hutang_produk on hutang_produk.voucher_produk_id = voucher_produk.voucher_produk_id
INNER JOIN produk on hutang_produk.produk_id = produk.produk_id
WHERE reseller.reseller_id = ".$reseller_id.") as b
UNION
SELECT * from (SELECT reseller.reseller_id,reseller.nama,'wd produk' as type,hutang_produk.tanggal_pelunasan as waktu,sum(hutang_produk.jumlah*produk.hpp_global) as total from hutang_produk
INNER JOIN reseller on reseller.reseller_id = hutang_produk.reseller_id
INNER JOIN produk on produk.produk_id = hutang_produk.produk_id
WHERE reseller.reseller_id = ".$reseller_id."
AND hutang_produk.status = 'Lunas'
AND pembayaran_hutang_produk_id is not null
and tanggal_pelunasan is not null
GROUP BY pembayaran_hutang_produk_id) as c
ORDER BY waktu desc LIMIT ".$length." OFFSET ".$start."
;";
        return $this->db->query($sql)->result();
    }
}

/* End of file Color.php */
/* Location: ./application/models/Color.php */