<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fabric extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "fabric";
	}	
	function fabric_list($start,$length,$query){
		$this->db->like('fabric_id', $query, 'BOTH'); 
		$this->db->or_like('fabric_nama', $query, 'BOTH'); 
		$this->db->order_by('fabric_id', 'desc');
		return $this->db->get('fabric', $length, $start)->result();
	}
	function fabric_count_all(){
		return $this->db->count_all_results('fabric');
	}
	function fabric_count_filter($query){
		$this->db->like('fabric_id', $query, 'BOTH'); 
		$this->db->or_like('fabric_nama', $query, 'BOTH'); 
		return $this->db->count_all_results('fabric');
	}	
	function is_ready_kode($fabric_id,$kode){
		$this->db->where('fabric_kode', $kode);
		$data = $this->db->get('fabric')->row();
		if($data != null){
			if($data->fabric_id == $fabric_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}

/* End of file Fabric.php */
/* Location: ./application/models/Fabric.php */