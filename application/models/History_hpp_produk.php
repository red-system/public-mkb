<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_hpp_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "history_hpp_produk";
	}

    function get_hpp_produk_last($produk_id){
        $this->db->select('sum(stock_produk_qty*hpp) as total_harga, sum(stock_produk_qty) as total_qty, ceil(sum( stock_produk_qty * hpp ) / sum( stock_produk_qty )) AS hpp');
        $this->db->where('produk_id',$produk_id);
        return $this->db->get('stock_produk')->row();
    }

    function get_total_stock_produk_last($produk_id){
        $this->db->select('sum(stock_produk_qty) as total_stock_gudang');
        $this->db->where('produk_id',$produk_id);
        $this->db->where('stock_produk_lokasi_id', '28');
		$this->db->where('delete_flag', "0");
        return $this->db->get('stock_produk')->row();
    }

    function get_total_hpp_bahan($produk_id){
        $this->db->select('sum(bahan.bahan_harga * produk_recipe_detail.takaran) as total_hpp_bahan');
        $this->db->join('produk_recipe_detail', 'bahan.bahan_id = produk_recipe_detail.bahan_id');
        $this->db->join('produk_recipe', 'produk_recipe_detail.produk_recipe_id = produk_recipe.produk_recipe_id');
        $this->db->where('produk_recipe.produk_id',$produk_id);
        return $this->db->get('bahan')->row();
    }

}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */