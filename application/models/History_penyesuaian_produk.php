<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_penyesuaian_produk extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'history_penyesuaian_produk';
	}
	function history_list($start,$length,$query){
	    $this->db->select('history_penyesuaian_produk.*,if(history_penyesuaian_produk.hpp is null,if(stock_produk.hpp is null or stock_produk.hpp=0,produk.hpp_global,stock_produk.hpp),history_penyesuaian_produk.hpp)as hpp');
	    $this->db->join('stock_produk','stock_produk.stock_produk_id=history_penyesuaian_produk.stock_produk_id','left');
        $this->db->join('produk','produk.produk_id=history_penyesuaian_produk.produk_id','left');
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('history_penyesuaian_produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('history_penyesuaian_produk.produk_nama', $query, 'BOTH');
		$this->db->group_end();
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->where('history_penyesuaian_produk.lokasi_id', $_SESSION["redpos_login"]["lokasi_id"]);
		}					
		$this->db->order_by('history_penyesuaian_produk_id', 'desc');
		return $this->db->get('history_penyesuaian_produk', $length, $start)->result();
	}
	function history_count_filter($query){
        $this->db->join('stock_produk','stock_produk.stock_produk_id=history_penyesuaian_produk.stock_produk_id','left');
        $this->db->join('produk','produk.produk_id=history_penyesuaian_produk.produk_id','left');
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('history_penyesuaian_produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('history_penyesuaian_produk.produk_nama', $query, 'BOTH');
		$this->db->group_end();
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->where('history_penyesuaian_produk.lokasi_id', $_SESSION["redpos_login"]["lokasi_id"]);
		}				
		return $this->db->count_all_results('history_penyesuaian_produk');
	}
	function history_count(){
        $this->db->join('stock_produk','stock_produk.stock_produk_id=history_penyesuaian_produk.stock_produk_id','left');
        $this->db->join('produk','produk.produk_id=history_penyesuaian_produk.produk_id','left');
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->where('history_penyesuaian_produk.lokasi_id', $_SESSION["redpos_login"]["lokasi_id"]);
		}		
		return $this->db->count_all_results('history_penyesuaian_produk');
	}
	function first_date(){
		$this->db->order_by('tanggal');
		$get = $this->db->get('history_penyesuaian_produk');
		$date = date("Y-m-d");
		if($get->num_rows()>0){
			$date = $get->row()->tanggal;
		}
		return $date;
	}	

}

/* End of file Histori_penyesuaian_produk.php */
/* Location: ./application/models/Histori_penyesuaian_produk.php */