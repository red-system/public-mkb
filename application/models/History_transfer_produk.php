<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_transfer_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "history_transfer_produk";
	}	
	function need_confirm_list($start,$length,$query){
        $this->db->select('history_transfer_produk.*,satuan_nama');
        $this->db->join('produk', 'history_transfer_produk.produk_id = produk.produk_id');
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
	    $this->db->where('status', 'Menunggu Konfirmasi');
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('history_transfer_produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('history_transfer_produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["redpos_login"]["lokasi_nama"]);
		}		
		$this->db->order_by('history_transfer_produk_id', 'desc');
		return $this->db->get('history_transfer_produk', $length, $start)->result();
	}
	function need_confirm_count_filter($query){
		$this->db->where('status', 'Menunggu Konfirmasi');
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk_nama', $query, 'BOTH'); 
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["redpos_login"]["lokasi_nama"]);
		}		
		return $this->db->count_all_results('history_transfer_produk');
	}
	function need_confirm_count_all(){
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->where('history_transfer_produk.tujuan', $_SESSION["redpos_login"]["lokasi_nama"]);
		}		
		$this->db->where('status', 'Menunggu Konfirmasi');
		return $this->db->count_all_results('history_transfer_produk');
	}	
	function history_list($start,$length,$query){
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk_nama', $query, 'BOTH'); 
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}	
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->group_start();
				$this->db->where('history_transfer_produk.tujuan', $_SESSION["redpos_login"]["lokasi_nama"]);
				$this->db->or_where('history_transfer_produk.dari', $_SESSION["redpos_login"]["lokasi_nama"]);
			$this->db->group_end();
		}				
		$this->db->order_by('history_transfer_produk_id', 'desc');
		return $this->db->get('history_transfer_produk', $length, $start)->result();
	}
	function history_count_filter($query){
		$this->db->group_start();
			$this->db->like('tanggal', $query, 'BOTH'); 
			$this->db->or_like('produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk_nama', $query, 'BOTH'); 
			$this->db->or_like('dari', $query, 'BOTH'); 
			$this->db->or_like('tujuan', $query, 'BOTH'); 
			$this->db->or_like('status', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->group_start();
				$this->db->where('history_transfer_produk.tujuan', $_SESSION["redpos_login"]["lokasi_nama"]);
				$this->db->or_where('history_transfer_produk.dari', $_SESSION["redpos_login"]["lokasi_nama"]);
			$this->db->group_end();
		}		
		return $this->db->count_all_results('history_transfer_produk');
	}
	function history_count_all(){
		if(isset($_SESSION["redpos_login"]["lokasi_id"])){
			$this->db->group_start();
				$this->db->where('history_transfer_produk.tujuan', $_SESSION["redpos_login"]["lokasi_nama"]);
				$this->db->or_where('history_transfer_produk.dari', $_SESSION["redpos_login"]["lokasi_nama"]);
			$this->db->group_end();
		}		
		return $this->db->count_all_results('history_transfer_produk');
	}
	function first_date(){
		$this->db->order_by('tanggal');
		$get = $this->db->get('history_transfer_produk');
		$date = date("Y-m-d");
		if($get->num_rows()>0){
			$date = $get->row()->tanggal;
		}
		return $date;
	}		
}

/* End of file Histori_transfer_produk.php */
/* Location: ./application/models/Histori_transfer_produk.php */