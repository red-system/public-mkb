<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_produk extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "piutang_produk";
    }
    function count_all(){
        $this->db->join('po_produk','po_produk.po_produk_id = piutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = piutang_produk.voucher_produk_id');
        return $this->db->count_all_results('piutang_produk');
    }
    function count_filter($query){
        $this->db->join('po_produk','po_produk.po_produk_id = piutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = piutang_produk.voucher_produk_id');
        $this->db->group_start();
            $this->db->or_like('po_produk.po_produk_no',$query,'both');
            $this->db->or_like('voucher_produk.voucher_code',$query,'both');
            $this->db->or_like('piutang_produk.tanggal_pelunasan',$query,'both');
            $this->db->or_like('piutang_produk.status',$query,'both');
        $this->db->group_end();
        return $this->db->count_all_results('piutang_produk');
    }
    function _list($start,$lenth,$query){
        $this->db->join('po_produk','po_produk.po_produk_id = piutang_produk.po_produk_id');
        $this->db->join('voucher_produk','voucher_produk.voucher_produk_id = piutang_produk.voucher_produk_id');
        $this->db->group_start();
            $this->db->or_like('po_produk.po_produk_no',$query,'both');
            $this->db->or_like('voucher_produk.voucher_code',$query,'both');
            $this->db->or_like('piutang_produk.tanggal_pelunasan',$query,'both');
            $this->db->or_like('piutang_produk.status',$query,'both');
        $this->db->group_end();
        return $this->db->get('piutang_produk',$lenth,$start)->result();
    }
    function pembayaran($pembayaran_hutang_produk_id,$hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $data["pembayaran_hutang_produk_id"] = $pembayaran_hutang_produk_id;
        $this->db->where_in('piutang_produk_id',$hutang_produk_id);
        return $this->db->update('piutang_produk',$data);
    }
    function  pembayaran_hutang_produk($pembayaran_hutang_produk_id){
        $data = array();
        $data["status"] = "Lunas";
        $data["tanggal_pelunasan"] = date("Y-m-d");
        $this->db->where("pembayaran_hutang_produk_id",$pembayaran_hutang_produk_id);
        return $this->db->update("piutang_produk",$data);
    }
}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */