<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Money_back extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "money_back_guarantee";
    }
    function request_list($start,$length,$query){
        $this->db->select("money_back_guarantee.*,reseller.nama,reseller.no_ktp");
        $this->db->join('reseller','reseller.reseller_id = money_back_guarantee.reseller_id');
        $this->db->group_start();
        $this->db->or_like('reseller.nama');
        $this->db->or_like('reseller.no_ktp');
        $this->db->or_like('money_back_guarantee.tanggal_request');
        $this->db->or_like('money_back_guarantee.uang_dikembalikan');
        $this->db->or_like('money_back_guarantee.total_pengembalian');
        $this->db->or_like('money_back_guarantee.potongan');
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_request >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_request <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('status_pengiriman','On Process');
        return $this->db->get('money_back_guarantee', $length, $start)->result();
    }
    function request_all(){
        $this->db->join('reseller','reseller.reseller_id = money_back_guarantee.reseller_id');
        $this->db->where('status_pengiriman','On Process');
        return $this->db->count_all_results('money_back_guarantee');
    }
    function request_filter($query){
        $this->db->join('reseller','reseller.reseller_id = money_back_guarantee.reseller_id');
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('reseller.no_ktp',$query);
        $this->db->or_like('money_back_guarantee.tanggal_request',$query);
        $this->db->or_like('money_back_guarantee.uang_dikembalikan',$query);
        $this->db->or_like('money_back_guarantee.total_pengembalian',$query);
        $this->db->or_like('money_back_guarantee.potongan',$query);
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_request >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_request <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('status_pengiriman','On Process');
        return $this->db->count_all_results('money_back_guarantee');
    }
    function approve_list($start,$length,$query){
        $this->db->select("money_back_guarantee.*,reseller.nama,reseller.no_ktp");
        $this->db->join('reseller','reseller.reseller_id = money_back_guarantee.reseller_id');
        $this->db->group_start();
        $this->db->or_like('reseller.nama');
        $this->db->or_like('reseller.no_ktp');
        $this->db->or_like('money_back_guarantee.tanggal_request');
        $this->db->or_like('money_back_guarantee.uang_dikembalikan');
        $this->db->or_like('money_back_guarantee.total_pengembalian');
        $this->db->or_like('money_back_guarantee.potongan');
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_request >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_request <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('status_pengiriman','Terkirim');
        return $this->db->get('money_back_guarantee', $length, $start)->result();
    }
    function approve_all(){
        $this->db->join('reseller','reseller.reseller_id = money_back_guarantee.reseller_id');
        $this->db->where('status_pengiriman','Terkirim');
        return $this->db->count_all_results('money_back_guarantee');
    }
    function approve_filter($query){
        $this->db->join('reseller','reseller.reseller_id = money_back_guarantee.reseller_id');
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('reseller.no_ktp',$query);
        $this->db->or_like('money_back_guarantee.tanggal_request',$query);
        $this->db->or_like('money_back_guarantee.uang_dikembalikan',$query);
        $this->db->or_like('money_back_guarantee.total_pengembalian',$query);
        $this->db->or_like('money_back_guarantee.potongan',$query);
        $this->db->group_end();
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal_request >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal_request <=', $this->input->get('tanggal_end'));
        }
        $this->db->where('status_pengiriman','Terkirim');
        return $this->db->count_all_results('money_back_guarantee');
    }
}

/* End of file Patern.php */
/* Location: ./application/models/Patern.php */