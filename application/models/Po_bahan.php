<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_bahan extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "po_bahan";
	}
	function get_kode_po_bahan(){
		$year = date("y");
		$month = date("m");
		$prefix = "POB/".$month.$year."/";
		$this->db->like('po_bahan_no', $prefix, 'BOTH');
		$this->db->select('(max(urutan)+1) as kode');
		$this->db->from('po_bahan');
		$result = $this->db->get()->row()->kode;
		if ($result != null){
			return $prefix.($result);
		} else {
			return $prefix."1";
		}		
	}
	function po_bahan_count_all(){
        $this->db->select('po_bahan.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->group_by('po_bahan.po_bahan_id');
		if($this->uri->segment(1)=="po-bahan"){
			$this->db->where('status_penerimaan !=', "Diterima");
		}

		return $this->db->count_all_results('po_bahan');
	}
	function po_bahan_count_filter($query){
		$this->db->select('po_bahan.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->group_start();
		$this->db->like('po_bahan.po_bahan_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_bahan.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_bahan.grand_total', $total, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="po-bahan"){
			$this->db->where('status_penerimaan !=', "Diterima");
		}
		if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
			$this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
			$this->db->where('po_bahan.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
		}
		if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
			$this->db->where('po_bahan.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
			$this->db->where('po_bahan.status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
			$this->db->where('po_bahan.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
			$this->db->where('po_bahan.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_bahan.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_bahan.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};		
		$this->db->group_by('po_bahan.po_bahan_id');
		return $this->db->count_all_results('po_bahan');
	}
	function po_bahan_list($start,$length,$query){
		$this->db->select('po_bahan.*,suplier.suplier_nama,tipe_pembayaran.tipe_pembayaran_nama as kas_nama,tipe_pembayaran.no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_bahan.tipe_pembayaran','left');
		$this->db->group_start();
		$this->db->like('po_bahan.po_bahan_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_bahan.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_bahan.grand_total', $total, 'BOTH');
		$this->db->group_end();
		if($this->uri->segment(1)=="po-bahan"){
			$this->db->where('status_penerimaan !=', "Diterima");
		}
		if(isset($_GET['suplier_id'])&&$this->input->get('suplier_id')!=""){
			$this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['jenis_pembayaran'])&&$this->input->get('jenis_pembayaran')!=""){
			$this->db->where('po_bahan.jenis_pembayaran', $this->input->get('jenis_pembayaran'));
		}
		if(isset($_GET['status_pembayaran'])&&$this->input->get('status_pembayaran')!=""){
			$this->db->where('po_bahan.status_pembayaran', $this->input->get('status_pembayaran'));
		}
		if(isset($_GET['status_penerimaan'])&&$this->input->get('status_penerimaan')!=""){
			$this->db->where('po_bahan.status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if(isset($_GET['start_pemesanan'])&&$this->input->get('start_pemesanan')!=""){
			$this->db->where('po_bahan.tanggal_pemesanan >=', $this->input->get('start_pemesanan'));
			$this->db->where('po_bahan.tanggal_pemesanan <=', $this->input->get('end_pemesanan'));
		}
		if(isset($_GET['start_penerimaan'])&&$this->input->get('start_penerimaan')!=""){
			$this->db->where('po_bahan.tanggal_penerimaan >=', $this->input->get('start_penerimaan'));
			$this->db->where('po_bahan.tanggal_penerimaan <=', $this->input->get('end_penerimaan'));
		};		
		$this->db->group_by('po_bahan.po_bahan_id');
		$this->db->order_by('po_bahan.po_bahan_id', 'desc');
		return $this->db->get('po_bahan', $length, $start)->result();		
	}
	function po_bahan_count_all_history(){
        $this->db->select('po_bahan.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->group_by('po_bahan.po_bahan_id');
		$this->db->where('status_penerimaan', "Diterima");
		return $this->db->count_all_results('po_bahan');
	}
	function po_bahan_count_filter_history($query){
		$this->db->select('po_bahan.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->group_start();
		$this->db->like('po_bahan.po_bahan_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_bahan.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_bahan.grand_total', $total, 'BOTH');
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->group_by('po_bahan.po_bahan_id');
		return $this->db->count_all_results('po_bahan');
	}
	function po_bahan_list_history($start,$length,$query){
		$this->db->select('po_bahan.*,suplier.suplier_nama');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->group_start();
		$this->db->like('po_bahan.po_bahan_no', $query, 'BOTH');
		$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
		$this->db->or_like('po_bahan.jenis_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.tipe_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_pembayaran', $query, 'BOTH');
		$this->db->or_like('po_bahan.status_penerimaan', $query, 'BOTH');
		$total = $this->string_to_number($query);
		$this->db->or_like('po_bahan.grand_total', $total, 'BOTH');
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->group_by('po_bahan.po_bahan_id');
		$this->db->order_by('po_bahan.po_bahan_id', 'desc');
		return $this->db->get('po_bahan', $length, $start)->result();		
	}
	function insert_po_bahan(){
		$this->db->trans_begin();
		$po_no = $this->input->post('po_bahan_no');
		$temp = explode("/", $po_no);
		$urutan = $temp[sizeof($temp)-1];
		$data['po_bahan_no'] = $po_no;
		$data['urutan'] = $urutan;
		$data['suplier_id'] = $this->input->post('suplier_id');
		$temp = strtotime($this->input->post('tanggal_pemesanan'));
		$tanggal_pemesanan = date("Y-m-d",$temp);
		$data['tanggal_pemesanan'] = $tanggal_pemesanan;
		$data['keterangan'] = $this->input->post('keterangan');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		$data['jenis_pembayaran'] = $jenis_pembayaran;
		if($jenis_pembayaran == "kas"){
			$data['tipe_pembayaran'] = $this->input->post('tipe_pembayaran_id');
			$data['tipe_pembayaran_no'] = $this->input->post('tipe_pembayaran_no');
			$data['tipe_pembayaran_keterangan'] = $this->input->post('tipe_pembayaran_keterangan');
			$data['status_pembayaran'] = "Lunas";
		} else {
			$data['tipe_pembayaran'] = null;
			$data['tipe_pembayaran_nama'] = "Kredit";
			$data['status_pembayaran'] = "Hutang";
		}
		$data["total"] = $this->input->post('total_item');
		$data["grand_total"] = $this->input->post('grand_total');
		$data["potongan"] = $this->string_to_number($this->input->post('potongan'));
		$data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
		$data["status_penerimaan"] = "Belum Diterima";
		$this->db->insert('po_bahan', $data);
		$po_bahan_id = $this->db->insert_id();
		$item = $this->input->post('item_bahan');
		foreach ($item as $key) {
			$data = array();
			$data["po_bahan_id"] = $po_bahan_id;
			$data["bahan_id"] = $key["bahan_id"];
			$data["harga"] = $this->string_to_number($key["harga"]);
			$data["jumlah"] = $this->string_to_number($key["jumlah"]);
			$data["sisa"] = $this->string_to_number($key["jumlah"]);
			$data["sub_total"] = $this->string_to_number($key["subtotal"]);
			$this->db->insert('po_bahan_detail', $data);
		}
		if($jenis_pembayaran != "kas"){
			$data = array();
			$data['po_bahan_id'] = $po_bahan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
			$tenggat_pelunasan = date("Y-m-d",$temp);
			$data['tenggat_pelunasan'] = $tenggat_pelunasan;
			$this->db->insert('hutang', $data);
		}
		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;			
	}
	function edit_po_bahan(){
		$this->db->trans_begin();
		$po_bahan_id = $this->input->post('po_bahan_id');
		$this->db->where('po_bahan_id', $po_bahan_id);
		$this->db->delete('po_bahan_detail');
		$po_no = $this->input->post('po_bahan_no');
		$temp = explode("/", $po_no);
		$urutan = $temp[sizeof($temp)-1];
		$data['po_bahan_no'] = $po_no;
		$data['urutan'] = $urutan;
		$data['suplier_id'] = $this->input->post('suplier_id');
		$temp = strtotime($this->input->post('tanggal_pemesanan'));
		$tanggal_pemesanan = date("Y-m-d",$temp);
		$data['tanggal_pemesanan'] = $tanggal_pemesanan;
		$data['keterangan'] = $this->input->post('keterangan');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		$data['jenis_pembayaran'] = $jenis_pembayaran;
		if($jenis_pembayaran == "kas"){
			$data['tipe_pembayaran'] = $this->input->post('tipe_pembayaran_id');
			$data['tipe_pembayaran_no'] = $this->input->post('tipe_pembayaran_no');
			$data['tipe_pembayaran_keterangan'] = $this->input->post('tipe_pembayaran_keterangan');
			$data['status_pembayaran'] = "Lunas";
		} else {
			$data['tipe_pembayaran'] = null;
			$data['tipe_pembayaran_nama'] = "Kredit";
			$data['status_pembayaran'] = "Hutang";
		}
		$data["total"] = $this->input->post('total_item');
		$data["grand_total"] = $this->input->post('grand_total');
		$data["potongan"] = $this->string_to_number($this->input->post('potongan'));
		$data["tambahan"] = $this->string_to_number($this->input->post('tambahan'));
		$data["status_penerimaan"] = "Belum Diterima";
		$this->db->where('po_bahan_id', $po_bahan_id);
		$this->db->update('po_bahan', $data);
		$item = $this->input->post('item_bahan');
		foreach ($item as $key) {
			$data = array();
			$data["po_bahan_id"] = $po_bahan_id;
			$data["bahan_id"] = $key["bahan_id"];
			$data["harga"] = $this->string_to_number($key["harga"]);
			$data["jumlah"] = $this->string_to_number($key["jumlah"]);
			$data["sisa"] = $this->string_to_number($key["jumlah"]);
			$data["sub_total"] = $this->string_to_number($key["subtotal"]);
			$this->db->insert('po_bahan_detail', $data);
		}
		$data = array();
		$this->db->where('po_bahan_id', $po_bahan_id);
		$hutang = $this->db->get('hutang')->row();
		if($jenis_pembayaran != "kas"){
			if($hutang != null){
				$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
				$tenggat_pelunasan = date("Y-m-d",$temp);
				$data['tenggat_pelunasan'] = $tenggat_pelunasan;
				$this->db->where('hutang_id', $hutang->hutang_id);
				$this->db->update('hutang', $data);
			} else {
				$temp = strtotime($this->input->post('tenggat_pelunasan')) ;
				$tenggat_pelunasan = date("Y-m-d",$temp);
				$data['po_bahan_id'] = $po_bahan_id;
				$data['tenggat_pelunasan'] = $tenggat_pelunasan;
				$this->db->insert('hutang', $data);			
			}			
		} else {
			$this->db->where('po_bahan_id', $po_bahan_id);
			$this->db->delete('hutang');
		}

		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;			
	}
	function po_bahan_detail_by_id($po_bahan_id){
		$this->db->where('po_bahan_detail.po_bahan_id', $po_bahan_id);
		$this->db->select('po_bahan_detail.*,bahan.bahan_nama,suplier.suplier_nama');
		$this->db->join('bahan', 'bahan.bahan_id = po_bahan_detail.bahan_id');
		$this->db->join('po_bahan', 'po_bahan.po_bahan_id = po_bahan_detail.po_bahan_id');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		return $this->db->get('po_bahan_detail')->result();
	}
	function po_bahan_by_id($po_bahan_id){
		$this->db->where('po_bahan.po_bahan_id', $po_bahan_id);
		$this->db->select('po_bahan.*,if(tipe_pembayaran.tipe_pembayaran_nama is null,"kredit",tipe_pembayaran.tipe_pembayaran_nama) as tipe_pembayaran_nama,suplier.suplier_nama,hutang.tenggat_pelunasan,tipe_pembayaran.tipe_pembayaran_nama');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
        $this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_bahan.tipe_pembayaran','left');
		$this->db->join('hutang', 'hutang.po_bahan_id = po_bahan.po_bahan_id', 'left');
		return $this->db->get('po_bahan')->row();
	}
	function laporan_po_bahan_count_all(){
        $this->db->select('po_bahan.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->join('bahan', 'bahan.bahan_id = po_bahan_detail.bahan_id');
		return $this->db->count_all_results('po_bahan');

	}
	function laporan_po_bahan_count_filter($query){
        $this->db->select('po_bahan.*');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->join('bahan', 'bahan.bahan_id = po_bahan_detail.bahan_id');
		$this->db->group_start();
			$this->db->like('po_bahan_no', $query, 'BOTH');
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH');
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $temp);
			$this->db->or_like('po_bahan_detail.jumlah', $temp, 'BOTH');
			$this->db->or_like('po_bahan_detail.harga', $temp, 'BOTH');
			$this->db->or_like('po_bahan_detail.sub_total', $temp, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['po_bahan_no']) && $this->input->get('po_bahan_no')!=""){
			$this->db->like('po_bahan.po_bahan_no', $this->input->get('po_bahan_no'),'BOTH');
		}
		if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
			$this->db->where('po_bahan.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
			$this->db->where('po_bahan.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
		}
		if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
			$this->db->where('po_bahan.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['bahan_id'])&& $this->input->get('bahan_id')!=""){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
			if($this->input->get('tipe_pembayaran_id')!="kredit"){
				$this->db->where('po_bahan.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
			} else {
				$this->db->where('po_bahan.tipe_pembayaran is null', null, false);
			}
			
		}	
		return $this->db->count_all_results('po_bahan');

	}
	function laporan_po_bahan_list($start,$length,$query){
		$this->db->select('po_bahan.jenis_pembayaran,po_bahan.po_bahan_id,po_bahan.po_bahan_no,po_bahan.tanggal_pemesanan,suplier.suplier_nama,bahan.bahan_kode,bahan.bahan_nama,po_bahan_detail.jumlah,po_bahan_detail.harga,po_bahan_detail.sub_total,tipe_pembayaran.tipe_pembayaran_nama as "kas_nama" ,tipe_pembayaran.no_akun');
		$this->db->join('suplier', 'suplier.suplier_id = po_bahan.suplier_id');
		$this->db->join('po_bahan_detail', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id');
		$this->db->join('bahan', 'bahan.bahan_id = po_bahan_detail.bahan_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = po_bahan.tipe_pembayaran','left');
		$this->db->group_start();
			$this->db->like('po_bahan_no', $query, 'BOTH');
			$this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
			$this->db->or_like('bahan.bahan_kode', $query, 'BOTH');
			$this->db->or_like('bahan.bahan_nama', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $temp);
			$this->db->or_like('po_bahan_detail.jumlah', $temp, 'BOTH');
			$this->db->or_like('po_bahan_detail.harga', $temp, 'BOTH');
			$this->db->or_like('po_bahan_detail.sub_total', $temp, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['po_bahan_no']) && $this->input->get('po_bahan_no')!=""){
			$this->db->like('po_bahan.po_bahan_no', $this->input->get('po_bahan_no'),'BOTH');
		}
		if(isset($_GET['pemesanan_start'])&& $this->input->get('pemesanan_start')!=""){
			$this->db->where('po_bahan.tanggal_pemesanan >=', $this->input->get('pemesanan_start'));
			$this->db->where('po_bahan.tanggal_pemesanan <=', $this->input->get('pemesanan_end'));
		}
		if(isset($_GET['suplier_id'])&& $this->input->get('suplier_id')!=""){
			$this->db->where('po_bahan.suplier_id', $this->input->get('suplier_id'));
		}
		if(isset($_GET['bahan_id'])&& $this->input->get('bahan_id')!=""){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&& $this->input->get('tipe_pembayaran_id')!=""){
			if($this->input->get('tipe_pembayaran_id')!="kredit"){
				$this->db->where('po_bahan.tipe_pembayaran', $this->input->get('tipe_pembayaran_id'));
			} else {
				$this->db->where('po_bahan.tipe_pembayaran is null', null, false);
			}
			
		}	
		$this->db->order_by('po_bahan.po_bahan_id', 'desc');
		return $this->db->get('po_bahan', $length, $start)->result();
	}
}

/* End of file Po_bahan.php */
/* Location: ./application/models/Po_bahan.php */