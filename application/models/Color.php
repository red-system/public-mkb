<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Color extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->table_name = "color";
	}	
	function color_list($start,$length,$query){
		$this->db->like('color_id', $query, 'BOTH'); 
		$this->db->or_like('color_nama', $query, 'BOTH'); 
		$this->db->order_by('color_id', 'desc');
		return $this->db->get('color', $length, $start)->result();
	}
	function color_count_all(){

		return $this->db->count_all_results('color');
	}
	function color_count_filter($query){
		$this->db->like('color_id', $query, 'BOTH');  
		$this->db->or_like('color_nama', $query, 'BOTH'); 
		return $this->db->count_all_results('color');
	}	
	function is_ready_kode($color_id,$kode){
		$this->db->where('color_kode', $kode);
		$data = $this->db->get('color')->row();
		if($data != null){
			if($data->color_id == $color_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */