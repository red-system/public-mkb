<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_hutang_retur extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "pembayaran_hutang_retur";
    }
    function getPembayaranNo($reseller_id){
        $prefix = "RPR/".date("ymd")."/".$reseller_id."/";
        $this->db->select("no_pembayaran");
        $this->db->or_like("no_pembayaran",$prefix);
        $this->db->order_by("no_pembayaran","desc");
        $row = $this->db->get("pembayaran_hutang_retur")->row();

        if($row==null){
            return $prefix."1";
        }else {
            return $prefix.(str_replace($prefix,"",$row->no_pembayaran)+1);
        }
    }
    function _list($start,$length,$query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_retur.*,reseller.nama");
        $this->db->join('hutang_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_retur.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_retur.status","waiting");
        return $this->db->get('pembayaran_hutang_retur',$length,$start)->result();
    }
    function _filter($query){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_retur.*,reseller.nama");
        $this->db->join('hutang_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id');

        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_retur.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->where("pembayaran_hutang_retur.status","waiting");
        return $this->db->count_all_results('pembayaran_hutang_retur');
    }
    function _all(){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_retur.*,reseller.nama");
        $this->db->join('hutang_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id');

        $this->db->where("pembayaran_hutang_retur.status","waiting");
        return $this->db->count_all_results('pembayaran_hutang_retur');
    }

    function _list_res($start,$length,$query,$reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_retur.*,reseller.nama");
        $this->db->join('hutang_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->join('refuse_pembayaran_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = refuse_pembayaran_retur.pembayaran_hutang_retur_id','left');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_retur.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_retur.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->or_where("hutang_retur.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_retur.reseller_id",$reseller_id);
        $this->db->group_end();
        return $this->db->get('pembayaran_hutang_retur',$length,$start)->result();
    }
    function _filter_res($query,$reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_retur.*");
        $this->db->join('hutang_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->join('refuse_pembayaran_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = refuse_pembayaran_retur.pembayaran_hutang_retur_id','left');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_retur.reseller_id','left');
        $this->db->group_start();
        $this->db->or_like('no_pembayaran',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->or_like('pembayaran_hutang_retur.tanggal',$query,'both');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->or_where("hutang_retur.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_retur.reseller_id",$reseller_id);
        $this->db->group_end();
        return $this->db->count_all_results('pembayaran_hutang_retur');
    }
    function _all_res($reseller_id){
        $this->db->distinct();
        $this->db->select("pembayaran_hutang_retur.*");
        $this->db->join('hutang_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->join('refuse_pembayaran_retur','pembayaran_hutang_retur.pembayaran_hutang_retur_id = refuse_pembayaran_retur.pembayaran_hutang_retur_id','left');
        $this->db->join('reseller','hutang_retur.reseller_id = reseller.reseller_id','left');
        $this->db->join('reseller as b','b.reseller_id = refuse_pembayaran_retur.reseller_id','left');
        $this->db->group_start();
        $this->db->or_where("hutang_retur.reseller_id",$reseller_id);
        $this->db->or_where("refuse_pembayaran_retur.reseller_id",$reseller_id);
        $this->db->group_end();
        return $this->db->count_all_results('pembayaran_hutang_retur');
    }


    function _hutang_all_res($reseller_id){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->where("hutang_retur.reseller_id",$reseller_id);
        $this->db->where("hutang_retur.status","Hutang");
        $this->db->group_start();
        $this->db->where("hutang_retur.pembayaran_hutang_retur_id is null",null,true);
        if(isset($_SESSION['po_produk']["pembayaran_hutang_produk"])){
            $this->db->or_where("hutang_retur.pembayaran_hutang_retur_id",$_SESSION['po_produk']["pembayaran_hutang_produk"]);
        }
        $this->db->group_end();
        return $this->db->count_all_results('hutang_retur');
    }
    function _hutang_filter_res($query,$reseller_id){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.no_retur',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();

        $this->db->where("hutang_retur.reseller_id",$reseller_id);
        $this->db->where("hutang_retur.status","Hutang");
        $this->db->group_start();
        $this->db->where("hutang_retur.pembayaran_hutang_retur_id is null",null,true);
        if(isset($_SESSION['po_produk']["pembayaran_hutang_produk"])){
            $this->db->or_where("hutang_retur.pembayaran_hutang_retur_id",$_SESSION['po_produk']["pembayaran_hutang_produk"]);
        }
        $this->db->group_end();
        return $this->db->count_all_results('hutang_retur');
    }

    function _hutang_list_res($start,$length,$query,$reseller_id){
        $this->db->select('no_retur,voucher_code,voucher_retur.voucher_retur_id,produk.produk_nama,hutang_retur.status,hutang_retur.jumlah,tanggal_pelunasan,pembayaran_hutang_retur.no_pembayaran,hutang_retur.created_at,hutang_retur.updated_at,hutang_retur_id');
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.no_retur',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $this->db->where("hutang_retur.reseller_id",$reseller_id);
        $this->db->where("hutang_retur.status","Hutang");
        $this->db->group_start();
        $this->db->where("hutang_retur.pembayaran_hutang_retur_id is null",null,true);
        if(isset($_SESSION['po_produk']["pembayaran_hutang_produk"])){
            $this->db->or_where("hutang_retur.pembayaran_hutang_retur_id",$_SESSION['po_produk']["pembayaran_hutang_produk"]);
        }
        $this->db->group_end();
        return $this->db->get('hutang_retur',$length,$start)->result();
    }
    function insert_data($reseller_id){
        $tanggal = $this->input->post("tanggal");
        $data = array();
        $no_pembayaran = $this->getPembayaranNo($reseller_id);
        $data["no_pembayaran"] = $no_pembayaran;
        $data["tanggal"] = $tanggal;
        $data["staff_created"] = $_SESSION["redpos_login"]["staff_id"];
        $data["created_at"] = date("Y-m-d H:i:s");
        $this->db->trans_start();
        $this->db->insert("pembayaran_hutang_retur",$data);
        $pembayaran_hutang_produk_id = $this->db->insert_id();
        $change = array();
        $voucher_produk_id = $this->input->post("hutang_produk_id");
        $change['pembayaran_hutang_retur_id'] = $pembayaran_hutang_produk_id;
        $this->db->where_in("voucher_retur_id",$voucher_produk_id);
        $this->db->update("hutang_retur",$change);

        $this->db->where_in("voucher_retur_id",$voucher_produk_id);
        $this->db->update("piutang_retur",$change);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    function _hutang_all(){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $reseller_id = isset($_SESSION['po_produk']["reseller_id"]) ? $_SESSION['po_produk']["reseller_id"] :0;
        $this->db->where("hutang_retur.reseller_id",$reseller_id);
        $this->db->group_start();
        $this->db->where("hutang_retur.status","Hutang");
        if(isset($_SESSION['po_produk']["pembayaran_hutang_retur"])){
            $this->db->or_where("hutang_retur.pembayaran_hutang_retur_id",$_SESSION['po_produk']["pembayaran_hutang_retur"]);
        }
        $this->db->group_end();
        return $this->db->count_all_results('hutang_retur');
    }
    function _hutang_filter($query){
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.no_retur',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $reseller_id = isset($_SESSION['po_produk']["reseller_id"]) ? $_SESSION['po_produk']["reseller_id"] :0;
        $this->db->where("hutang_retur.reseller_id",$reseller_id);
        $this->db->group_start();
        $this->db->where("hutang_retur.status","Hutang");
        if(isset($_SESSION['po_produk']["pembayaran_hutang_retur"])){
            $this->db->or_where("hutang_retur.pembayaran_hutang_retur_id",$_SESSION['po_produk']["pembayaran_hutang_retur"]);
        }
        $this->db->group_end();
        return $this->db->count_all_results('hutang_retur');
    }
    function _hutang_list($start,$length,$query){
        $this->db->select('no_retur,voucher_code,produk.produk_nama,hutang_retur.status,hutang_retur.jumlah,tanggal_pelunasan,pembayaran_hutang_retur.no_pembayaran,hutang_retur.created_at,hutang_retur.updated_at,hutang_retur_id');
        $this->db->join('retur_agen','retur_agen.retur_agen_id = hutang_retur.retur_agen_id');
        $this->db->join('voucher_retur','voucher_retur.voucher_retur_id = hutang_retur.voucher_retur_id');
        $this->db->join('reseller','reseller.reseller_id = hutang_retur.reseller_id');
        $this->db->join('produk','produk.produk_id = hutang_retur.produk_id');
        $this->db->join('pembayaran_hutang_retur','hutang_retur.pembayaran_hutang_retur_id = pembayaran_hutang_retur.pembayaran_hutang_retur_id','left');
        $this->db->group_start();
        $this->db->or_like('retur_agen.no_retur',$query,'both');
        $this->db->or_like('voucher_retur.voucher_code',$query,'both');
        $this->db->or_like('hutang_retur.tanggal_pelunasan',$query,'both');
        $this->db->or_like('hutang_retur.status',$query,'both');
        $this->db->or_like('reseller.nama',$query,'both');
        $this->db->group_end();
        $reseller_id = isset($_SESSION['po_produk']["reseller_id"]) ? $_SESSION['po_produk']["reseller_id"] :0;
        $this->db->where("hutang_retur.reseller_id",$reseller_id);
        $this->db->group_start();
        $this->db->where("hutang_retur.status","Hutang");
        if(isset($_SESSION['po_produk']["pembayaran_hutang_retur"])){
            $this->db->or_where("hutang_retur.pembayaran_hutang_retur_id",$_SESSION['po_produk']["pembayaran_hutang_retur"]);
        }
        $this->db->group_end();
        return $this->db->get('hutang_retur',$length,$start)->result();
    }
    function penarikan_stock($pembayaran_hutang_retur_id){

        $this->db->where("pembayaran_hutang_retur_id",$pembayaran_hutang_retur_id);
        $hutang = $this->db->get("hutang_retur")->result();
        $this->db->where("pembayaran_hutang_retur_id",$pembayaran_hutang_retur_id);
        $piutang = $this->db->get("piutang_retur")->result();
        $this->load->library("lokasi");
        $gudang = $this->lokasi->all_gudang();
        $gudang = $gudang[0];
        $query = "UPDATE hutang_retur SET pembayaran_hutang_retur_id = null WHERE pembayaran_hutang_retur_id = ".$pembayaran_hutang_retur_id;
        $this->db->query($query);

        $query = "UPDATE piutang_produk SET pembayaran_hutang_retur_id = null WHERE pembayaran_hutang_retur_id = ".$pembayaran_hutang_retur_id;
        $this->db->query($query);
        foreach ($hutang as $item){
            $this->db->where("mykindofbeauty_master.login.reseller_id",$item->reseller_id);
            $login = $this->db->get("mykindofbeauty_master.login")->row();
            $lokasi_id = $login->lokasi_id;
            $reseller_id = $login->reseller_id;
            $produk_id = $item->produk_id;
            $this->db->where("stock_produk_lokasi_id",$lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_produk = $this->db->get("stock_produk")->row();
            $jumlah = $stock_produk->stock_produk_qty - $item->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_produk->stock_produk_id);
            $this->db->update("stock_produk",$stock_update);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("stock_produk")->row();
            $jumlah = $stock_gudang->stock_produk_qty + $item->jumlah;
            $stock_update = array();
            $stock_update["stock_produk_qty"] = $jumlah;
            $this->db->where("stock_produk_id",$stock_gudang->stock_produk_id);
            $this->db->update("stock_produk",$stock_update);

            $this->db->where("stock_produk_lokasi_id",$gudang->lokasi_id);
            $this->db->where("produk_id",$produk_id);
            $this->db->order_by("stock_produk_id");
            $stock_gudang = $this->db->get("stock_produk")->row();
        }

    }
    function pembayaran_hutang_retur_detail($pembayaran_hutang_retur_id){
        $this->db->select("reseller.nama as nama_reseller,pembayaran_hutang_retur.pembayaran_hutang_retur_id,pembayaran_hutang_retur.no_pembayaran,pembayaran_hutang_retur.tanggal,hutang_retur.*,produk.produk_nama,reseller.reseller_id,retur_agen.no_retur,voucher_code");
        $this->db->where("pembayaran_hutang_retur.pembayaran_hutang_retur_id",$pembayaran_hutang_retur_id);
        $this->db->join("hutang_retur","pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id");
        $this->db->join("produk","produk.produk_id = hutang_retur.produk_id");
        $this->db->join("reseller","reseller.reseller_id = hutang_retur.reseller_id");
        $this->db->join("retur_agen","hutang_retur.retur_agen_id = retur_agen.retur_agen_id");
        $this->db->join("voucher_retur","hutang_retur.voucher_retur_id = voucher_retur.voucher_retur_id");
        return $this->db->get("pembayaran_hutang_retur")->result();
    }
    function edit_data($reseller_id){
        $pembayaran_hutang_produk_id = $this->input->post("pembayaran_hutang_produk_id");
        $tanggal = $this->input->post("tanggal");
        $data = array();
        $data["tanggal"] = $tanggal;
        $data["staff_updated"] = $_SESSION["redpos_login"]["staff_id"];
        $data["updated_at"] = date("Y-m-d H:i:s");
        $this->db->trans_start();
        $this->db->where("pembayaran_hutang_retur_id",$pembayaran_hutang_produk_id);
        $this->db->update("pembayaran_hutang_retur",$data);
        $query = "update hutang_retur set pembayaran_hutang_retur_id = null where pembayaran_hutang_retur_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
        $query = "update piutang_retur set pembayaran_hutang_retur_id = null where pembayaran_hutang_retur_id = ".$pembayaran_hutang_produk_id;
        $this->db->query($query);
        $change = array();

        $voucher_produk_id = $this->input->post("hutang_produk_id");
        $change['pembayaran_hutang_retur_id'] = $pembayaran_hutang_produk_id;

        $this->db->where_in("voucher_retur_id",$voucher_produk_id);
        $this->db->update("hutang_retur",$change);

        $this->db->where_in("voucher_retur_id",$voucher_produk_id);
        $this->db->update("piutang_retur",$change);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }
    function setPembayaranNull($id){
        $query = "update hutang_retur set pembayaran_hutang_retur_id = null where pembayaran_hutang_retur_id = ".$id;
        $this->db->query($query);
        $query = "update piutang_retur set pembayaran_hutang_retur_id = null where pembayaran_hutang_retur_id = ".$id;
        $this->db->query($query);
    }
    function pembayaran_hutang_produk_detail($pembayaran_hutang_retur_id){
        $this->db->select("reseller.nama as nama_reseller,pembayaran_hutang_retur.pembayaran_hutang_retur_id,pembayaran_hutang_retur.no_pembayaran,pembayaran_hutang_retur.tanggal,hutang_retur.*,produk.produk_nama,reseller.reseller_id,retur_agen.no_retur,voucher_code,hutang_retur.voucher_retur_id");
        $this->db->where("pembayaran_hutang_retur.pembayaran_hutang_retur_id",$pembayaran_hutang_retur_id);
        $this->db->join("hutang_retur","pembayaran_hutang_retur.pembayaran_hutang_retur_id = hutang_retur.pembayaran_hutang_retur_id");
        $this->db->join("produk","produk.produk_id = hutang_retur.produk_id");
        $this->db->join("reseller","reseller.reseller_id = hutang_retur.reseller_id");
        $this->db->join("retur_agen","hutang_retur.retur_agen_id = retur_agen.retur_agen_id");
        $this->db->join("voucher_retur","hutang_retur.voucher_retur_id = voucher_retur.voucher_retur_id");
        return $this->db->get("pembayaran_hutang_retur")->result();
    }

    function approve($pembayaran_hutang_produk_id){
        $change = array();
        $change["status"] = "approve";
        $this->db->where("pembayaran_hutang_retur_id",$pembayaran_hutang_produk_id,$change);
        return $this->db->update("pembayaran_hutang_retur",$change);
    }

}

/* End of file Patern.php */
/* Location: ./application/models/Patern.php */