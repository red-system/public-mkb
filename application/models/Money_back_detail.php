<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Money_back_detail extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "money_back_guarantee_detail";
    }
    function data_by_id($money_back_guarantee_id){
        $this->db->select('money_back_guarantee_detail.*,produk.produk_nama');
        $this->db->join('produk','produk.produk_id = money_back_guarantee_detail.produk_id');
        $this->db->where('money_back_guarantee_id',$money_back_guarantee_id);
        return $this->db->get('money_back_guarantee_detail')->result();
    }

}

/* End of file Patern.php */
/* Location: ./application/models/Patern.php */