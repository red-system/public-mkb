<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_recipe_detail extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "produk_recipe_detail";
    }
    function bahan_all(){
        $this->db->select('bahan.*');
        // $this->db->join('jenis_bahan','jenis_bahan.jenis_bahan_id = bahan.bahan_jenis_id');
        $this->db->join('satuan','satuan.satuan_id = bahan.bahan_satuan_id');
        $this->db->join('bahan_unit','bahan.bahan_id = bahan_unit.bahan_id','left');
        $this->db->join('satuan b','b.satuan_id = bahan_unit.satuan_id','left');
        $this->db->group_by('bahan.bahan_id');
        return $this->db->count_all_results('bahan');
    }
    function bahan_filter($query){
        $this->db->select('bahan.*');
        // $this->db->join('jenis_bahan','jenis_bahan.jenis_bahan_id = bahan.bahan_jenis_id');
        $this->db->join('satuan','satuan.satuan_id = bahan.bahan_satuan_id');
        $this->db->join('bahan_unit','bahan.bahan_id = bahan_unit.bahan_id','left');
        $this->db->join('satuan b','b.satuan_id = bahan_unit.satuan_id','left');
        $this->db->group_start();
        $this->db->or_like('bahan_kode',$query);
        $this->db->or_like('bahan_nama',$query);
        // $this->db->or_like('jenis_bahan_nama',$query);
        $this->db->group_end();
        $this->db->group_by('bahan.bahan_id');
        return $this->db->count_all_results('bahan');
    }
    function bahan_list($start,$length,$query){
        $select = 'bahan.bahan_id,bahan_nama,bahan_harga,satuan.satuan_nama,'.
                    'CONCAT_WS("|",satuan.satuan_nama,GROUP_CONCAT(b.satuan_nama SEPARATOR "|" )) as unit_name,'.
                    'CONCAT_WS("|",satuan.satuan_id,GROUP_CONCAT(b.satuan_id SEPARATOR "|" )) as unit_id,'.
                    'CONCAT_WS("|","1",GROUP_CONCAT(bahan_unit.jumlah_satuan_unit SEPARATOR "|" )) as unit_konversi,';
        // $select = 'bahan.bahan_id,bahan_kode,bahan_nama,bahan_harga,jenis_bahan_nama,satuan.satuan_nama,'.
        //             'CONCAT_WS("|",satuan.satuan_nama,GROUP_CONCAT(b.satuan_nama SEPARATOR "|" )) as unit_name,'.
        //             'CONCAT_WS("|",satuan.satuan_id,GROUP_CONCAT(b.satuan_id SEPARATOR "|" )) as unit_id,'.
        //             'CONCAT_WS("|","1",GROUP_CONCAT(bahan_unit.jumlah_satuan_unit SEPARATOR "|" )) as unit_konversi,';
        $this->db->select($select);
        // $this->db->join('jenis_bahan','jenis_bahan.jenis_bahan_id = bahan.bahan_jenis_id');
        $this->db->join('satuan','satuan.satuan_id = bahan.bahan_satuan_id');
        $this->db->join('bahan_unit','bahan.bahan_id = bahan_unit.bahan_id','left');
        $this->db->join('satuan b','b.satuan_id = bahan_unit.satuan_id','left');
        $this->db->group_start();
        $this->db->or_like('bahan_kode',$query);
        $this->db->or_like('bahan_nama',$query);
        // $this->db->or_like('jenis_bahan_nama',$query);
        $this->db->group_end();
        $this->db->group_by('bahan.bahan_id');
        return $this->db->get('bahan',$length,$start)->result();
    }
    function recipe_by_produk($produk_recipe_id){
        $select = 'produk_recipe_detail.*,bahan.bahan_id,bahan_nama,bahan_harga,satuan.satuan_nama,'.
            'CONCAT_WS("|",satuan.satuan_nama,GROUP_CONCAT(b.satuan_nama SEPARATOR "|" )) as unit_name,'.
            'CONCAT_WS("|",satuan.satuan_id,GROUP_CONCAT(b.satuan_id SEPARATOR "|" )) as unit_id,'.
            'CONCAT_WS("|","1",GROUP_CONCAT(bahan_unit.jumlah_satuan_unit SEPARATOR "|" )) as unit_konversi,';
        // $select = 'produk_recipe_detail.*,bahan.bahan_id,bahan_kode,bahan_nama,bahan_harga,jenis_bahan_nama,satuan.satuan_nama,'.
        //     'CONCAT_WS("|",satuan.satuan_nama,GROUP_CONCAT(b.satuan_nama SEPARATOR "|" )) as unit_name,'.
        //     'CONCAT_WS("|",satuan.satuan_id,GROUP_CONCAT(b.satuan_id SEPARATOR "|" )) as unit_id,'.
        //     'CONCAT_WS("|","1",GROUP_CONCAT(bahan_unit.jumlah_satuan_unit SEPARATOR "|" )) as unit_konversi,';
        $this->db->select($select);
        $this->db->join('bahan','bahan.bahan_id = produk_recipe_detail.bahan_id');
        // $this->db->join('jenis_bahan','jenis_bahan.jenis_bahan_id = bahan.bahan_jenis_id');
        $this->db->join('satuan','satuan.satuan_id = bahan.bahan_satuan_id');
        $this->db->join('bahan_unit','bahan.bahan_id = bahan_unit.bahan_id','left');
        $this->db->join('satuan b','b.satuan_id = bahan_unit.satuan_id','left');
        $this->db->group_by('bahan.bahan_id');
        $this->db->where('produk_recipe_id',$produk_recipe_id);
        return $this->db->get($this->table_name)->result();
    }
}

/* End of file Size.php */
/* Location: ./application/models/Size.php */
