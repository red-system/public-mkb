<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Unit extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "unit";
    }
    public  function unit_count_all($id){
        $this->db->where('produk_id',$id);
        return $this->db->count_all_results('unit');
    }
    public  function unit_($id){
        $this->db->join('satuan','satuan.satuan_id = unit.satuan_id');
        $this->db->where('produk_id',$id);
        return $this->db->count_all_results('unit');
    }
    public  function unit_count_filter($query,$id){
        $this->db->join('satuan','satuan.satuan_id = unit.satuan_id');
        $this->db->group_start();
            $this->db->or_like('satuan.satuan_nama',$query);
            $this->db->or_like('satuan.satuan_kode',$query);
            $temp = $this->string_to_number($query);
            $this->db->or_like('harga_jual',$temp);
        $this->db->group_end();
        $this->db->where('produk_id',$id);
        return $this->db->count_all_results('unit');
    }
    public  function unit_list($start,$length,$query,$id){
        $this->db->select('unit.*,satuan.satuan_nama,satuan.satuan_kode');
        $this->db->join('satuan','satuan.satuan_id = unit.satuan_id');
        $this->db->group_start();
            $this->db->or_like('satuan.satuan_nama',$query);
            $this->db->or_like('satuan.satuan_kode',$query);
            $temp = $this->string_to_number($query);
            $this->db->or_like('harga_jual',$temp);
        $this->db->group_end();
        $this->db->where('produk_id',$id);
        return $this->db->get('unit',$length,$start)->result();
    }
}