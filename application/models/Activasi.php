<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activasi extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "activasi";
    }
    function check($link,$reseller_id){
        $this->db->where("token",$link);
        $this->db->where("reseller_id",$reseller_id);
        $row = $this->db->get("activasi")->row();
        if($row == null){
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */