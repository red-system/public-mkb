<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Npwp extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "npwp";
    }

    function count($id){
        $this->db->select('*');
        $this->db->where("reseller_id = ",$id);
        return $this->db->count_all_results('npwp');
    }

    function pilih_by_id($id){
        $this->db->select('*');
        $this->db->where("npwp_id = ",$id);
        return $this->db->get('npwp')->row();
    }
    function request_list($start,$length,$query){
        $this->db->select('npwp.*,npwp.npwp_id,reseller.nama,reseller.phone,reseller.alamat,npwp.no_npwp,npwp.foto_npwp,npwp.foto_selfi,if(npwp.penghasilan_lain=1,"Ya","Tidak") as lain,keterangan_lain');
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.approved',0);
        $this->db->group_start();
            $this->db->like('reseller.nama', $query, 'BOTH');
            $this->db->or_like('reseller.phone', $query, 'BOTH');
            $this->db->or_like('reseller.alamat', $query, 'BOTH');
            $this->db->or_like('npwp.keterangan_lain', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller', $length, $start)->result();
    }
    function request_filter($query){
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.approved',0);
        $this->db->group_start();
            $this->db->like('reseller.nama', $query, 'BOTH');
            $this->db->or_like('reseller.phone', $query, 'BOTH');
            $this->db->or_like('reseller.alamat', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function request_count(){
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.approved',0);

        return $this->db->count_all_results('reseller');
    }

    function approve_list($start,$length,$query){
        $this->db->select('npwp.npwp_id,npwp.reseller_id,reseller.nama,reseller.phone,reseller.alamat,npwp.no_npwp,npwp.foto_npwp,npwp.foto_npwp as data_foto,npwp.foto_selfi,if(npwp.penghasilan_lain=1,"Ya","Tidak") as lain,keterangan_lain,npwp.penghasilan_lain as input_penghasilan_lain_base,npwp.keterangan_lain as input_keterangan_lain_base,npwp.case');
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.approved',1);
        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('reseller.phone', $query, 'BOTH');
        $this->db->or_like('reseller.alamat', $query, 'BOTH');
        $this->db->or_like('npwp.keterangan_lain', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller', $length, $start)->result();
    }
    function approve_filter($query){
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.approved',1);
        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('reseller.phone', $query, 'BOTH');
        $this->db->or_like('reseller.alamat', $query, 'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function approve_count(){
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.approved',1);

        return $this->db->count_all_results('reseller');
    }

    function detailData($npwp_id){
        $this->db->select('npwp.npwp_id,reseller.email,reseller.nama,reseller.phone,reseller.alamat,npwp.no_npwp,npwp.foto_npwp,npwp.foto_selfi,if(npwp.penghasilan_lain=1,"Ya","Tidak") as lain,keterangan_lain');
        $this->db->join('npwp','npwp.reseller_id = reseller.reseller_id');
        $this->db->where('npwp.npwp_id',$npwp_id);
        return $this->db->get('reseller')->row();
    }
    function find_npwp($reseller_id){
        $this->db->where('reseller_id',$reseller_id);
        $find_data = $this->db->get('npwp')->row();
        $find = false;
        if($find_data!=null){
            $find = true;
        }
        return $find;
    }
    function cekapprove($reseller_id){
        $this->db->where('reseller_id',$reseller_id);
        $find_data = $this->db->get('npwp')->row();
        $approve = false;
        if(@$find_data->approved==1){
            $approve = true;
        }
        return $approve;
    }
}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
