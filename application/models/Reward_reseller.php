<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reward_reseller extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "reward_reseller";
    }
    function _list($start,$length,$query,$reseller_id = null){
        $this->db->select('reward_reseller.*,reseller.nama');
        $this->db->join('reseller','reward_reseller.reseller_id = reseller.reseller_id');
        if($reseller_id!=null) {
            $this->db->where('reseller.reseller_id', $reseller_id);
        }
        $this->db->where('reward_reseller.status <>', 'terkirim');
        $this->db->where('reward_reseller.status <>', 'refuse');
        return $this->db->get('reward_reseller', $length, $start)->result();
    }
    function _count_all($reseller_id=null){
        $this->db->join('reseller','reward_reseller.reseller_id = reseller.reseller_id');
        if($reseller_id!=null) {
            $this->db->where('reseller.reseller_id', $reseller_id);
        }
        $this->db->where('reward_reseller.status <>', 'terkirim');
        $this->db->where('reward_reseller.status <>', 'refuse');
        return $this->db->count_all_results('reward_reseller');
    }
    function _count_filter($query,$reseller_id=null){
        $this->db->join('reseller','reward_reseller.reseller_id = reseller.reseller_id');
        if($reseller_id!=null) {
            $this->db->where('reseller.reseller_id', $reseller_id);
        }
        $this->db->where('reward_reseller.status <>', 'terkirim');
        $this->db->where('reward_reseller.status <>', 'refuse');
        return $this->db->count_all_results('reward_reseller');
    }
    function bintang_reseller($reseller_id){
        $this->db->select('if(sum(jumlah_bintang) is null,0,sum(jumlah_bintang)) as jumlah');
        $this->db->where('reseller_id',$reseller_id);
        $this->db->group_start();
        $this->db->or_where('status','approve');
        $this->db->or_where('status','terkirim');
        $this->db->group_end();
        return $this->db->get('reward_reseller')->row()->jumlah;
    }
    function changeToSuccessWithDate($arr_id,$date){
        $data["status"] = "terkirim";
        $this->db->where_in('reward_reseller_id',$arr_id);
        return $this->db->update("reward_reseller",$data);
    }
    function jumlahBintang($reseller_id){
        $this->db->select('if(sum(jumlah_bintang) is null,0,sum(jumlah_bintang)) as jumlah');
        $this->db->where('reseller_id',$reseller_id);
        $this->db->group_start();
        $this->db->or_where('status','request');
        $this->db->or_where('status','approve');
        $this->db->or_where('status','terkirim');
        $this->db->group_end();
        $result = $this->db->get('reward_reseller')->row()->jumlah;
        if($result==null){
            return 0;
        }else {
            return $result;
        }
    }
}

/* End of file Satuan.php */
/* Location: ./application/models/Satuan.php */