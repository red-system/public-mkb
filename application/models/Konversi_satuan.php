<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konversi_satuan extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "konversi_satuan";
    }
    function konversi_satuan_count_all($id){
        $this->db->join('satuan','satuan.satuan_id = konversi_satuan.id_satuan_akhir');
        $this->db->where('id_satuan_awal',$id);
        return $this->db->count_all_results('konversi_satuan');
    }
    function konversi_satuan_count_filter($query,$id){
        $this->db->join('satuan','satuan.satuan_id = konversi_satuan.id_satuan_akhir');
        $this->db->where('id_satuan_awal',$id);
        $this->db->group_start();
            $this->db->or_like('satuan.satuan_nama',$query);
            $this->db->or_like('satuan.satuan_kode',$query);
        $this->db->group_end();
        return $this->db->count_all_results('konversi_satuan');
    }
    function konversi_satuan_list($start,$length,$query,$id){
        $this->db->select('konversi_satuan.*,satuan.satuan_kode,satuan.satuan_nama');
        $this->db->join('satuan','satuan.satuan_id = konversi_satuan.id_satuan_akhir');
        $this->db->where('id_satuan_awal',$id);
        $this->db->group_start();
            $this->db->or_like('satuan.satuan_nama',$query);
            $this->db->or_like('satuan.satuan_kode',$query);
        $this->db->group_end();
        return $this->db->get('konversi_satuan',$length,$start)->result();
    }
}

/* End of file Log_kasir.php */
/* Location: ./application/models/Jenis_bahan.php */
