<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'produksi';
	}
	function produksi_list($start,$length,$query){
		$this->db->select('produksi.*, sum(produksi_item.jumlah) as "jumlah_item"');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->group_start();
			$this->db->like('produksi.produksi_id', $query, 'BOTH'); 
			$this->db->or_like('produksi_kode', $query, 'BOTH'); 
			$this->db->or_like('tanggal_mulai', $query, 'BOTH'); 
			$this->db->or_like('estimasi_selesai', $query, 'BOTH'); 
			$this->db->or_like('tanggal_selesai', $query, 'BOTH'); 
			$this->db->or_like('status_produksi', $query, 'BOTH'); 
			$this->db->or_like('status_penerimaan', $query, 'BOTH'); 
		$this->db->group_end();
		// $this->db->where('status_penerimaan', "Belum Diterima");
		$this->db->where('status_penerimaan !=', "Diterima");
		$this->db->or_where('status_produksi', "Dikerjakan");
		$this->db->group_by('produksi.produksi_id');
		$this->db->order_by('produksi_id', 'desc');
		return $this->db->get('produksi', $length, $start)->result();
	}
	function produksi_count_filter($query){
	    $this->db->select('produksi.*');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->group_start();
			$this->db->like('produksi.produksi_id', $query, 'BOTH'); 
			$this->db->or_like('produksi_kode', $query, 'BOTH'); 
			$this->db->or_like('tanggal_mulai', $query, 'BOTH'); 
			$this->db->or_like('estimasi_selesai', $query, 'BOTH'); 
			$this->db->or_like('tanggal_selesai', $query, 'BOTH'); 
			$this->db->or_like('status_produksi', $query, 'BOTH'); 
			$this->db->or_like('status_penerimaan', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Belum Diterima");
		$this->db->or_where('status_produksi', "Dikerjakan");
		$this->db->group_by('produksi.produksi_id');
		return $this->db->count_all_results('produksi');
	}
	function produksi_count_all(){
        $this->db->select('produksi.*');
		$this->db->where('status_penerimaan', "Belum Diterima");
		$this->db->or_where('status_produksi', "Dikerjakan");
		return $this->db->count_all_results('produksi');
	}
	function history_produksi_list($start,$length,$query){
		$this->db->select('produksi.*, sum(produksi_item.jumlah) as "jumlah_item"');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->group_start();
			$this->db->like('produksi.produksi_id', $query, 'BOTH'); 
			$this->db->or_like('produksi_kode', $query, 'BOTH'); 
			$this->db->or_like('tanggal_mulai', $query, 'BOTH'); 
			$this->db->or_like('estimasi_selesai', $query, 'BOTH'); 
			$this->db->or_like('tanggal_selesai', $query, 'BOTH'); 
			$this->db->or_like('status_produksi', $query, 'BOTH'); 
			$this->db->or_like('status_penerimaan', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->where('status_produksi', "Selesai");
		$this->db->group_by('produksi.produksi_id');
		$this->db->order_by('produksi_id', 'desc');
		return $this->db->get('produksi', $length, $start)->result();
	}
	function history_produksi_count_filter($query){
        $this->db->select('produksi.*');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->group_start();
			$this->db->like('produksi.produksi_id', $query, 'BOTH'); 
			$this->db->or_like('produksi_kode', $query, 'BOTH'); 
			$this->db->or_like('tanggal_mulai', $query, 'BOTH'); 
			$this->db->or_like('estimasi_selesai', $query, 'BOTH'); 
			$this->db->or_like('tanggal_selesai', $query, 'BOTH'); 
			$this->db->or_like('status_produksi', $query, 'BOTH'); 
			$this->db->or_like('status_penerimaan', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->where('status_produksi', "Selesai");
		$this->db->group_by('produksi.produksi_id');
		return $this->db->count_all_results('produksi');
	}
	function history_produksi_count_all(){
        $this->db->select('produksi.*');
		$this->db->where('status_penerimaan', "Diterima");
		$this->db->where('status_produksi', "Selesai");
		return $this->db->count_all_results('produksi');
	}	
	function get_kode_produksi(){
		$this->db->select('(max(urutan)+1) as kode');
		$this->db->from('produksi');
		$result = $this->db->get()->row()->kode;
		if ($result != null){
			return "BLP-".$result;
		} else {
			return "BLP-1";
		}
	}
	function produksi_by_recipe_id($id){
		$this->db->select('produksi.*,produksi_item.produksi_item_id,produksi_item.produk_id,produk.produk_nama,produksi_item.keterangan as "produk_keterangan",produksi_item.jumlah,produksi_item_bahan.bahan_id,produksi_item_bahan.jumlah as "jumlah_bahan", bahan.bahan_nama, produk_recipe_detail.takaran, bahan.bahan_harga, produksi_item.hpp');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produksi_item_bahan', 'produksi_item_bahan.produksi_item_id = produksi_item.produksi_item_id');
		$this->db->join('produk', 'produk.produk_id = produksi_item.produk_id');
		$this->db->join('bahan', 'bahan.bahan_id = produksi_item_bahan.bahan_id');
		$this->db->join('produk_recipe_detail', 'bahan.bahan_id = produk_recipe_detail.bahan_id');
		$this->db->where('produksi.produksi_id', $id);
		return $this->db->get('produksi')->result();
	}

	function produksi_by_id($id){
		$this->db->select('produksi.*,produksi_item.produksi_item_id,produksi_item.produk_id,produk.produk_nama,produksi_item.keterangan as "produk_keterangan",produksi_item.jumlah,produksi_item_bahan.bahan_id,produksi_item_bahan.jumlah as "jumlah_bahan", bahan.bahan_nama');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produksi_item_bahan', 'produksi_item_bahan.produksi_item_id = produksi_item.produksi_item_id');
		$this->db->join('produk', 'produk.produk_id = produksi_item.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('bahan', 'bahan.bahan_id = produksi_item_bahan.bahan_id');
		$this->db->where('produksi.produksi_id', $id);
		return $this->db->get('produksi')->result();
	}

	
	function insert_produksi(){
		$this->db->trans_begin();
		$item = $this->input->post('item');

		$lokasi_bahan = $this->input->post('lokasi_bahan_id');
		foreach ($item as $bahan) {
			foreach ($bahan["item_bahan"] as $bahan_item) {
				$this->db->select('SUM(stock_bahan_qty) as total_stock_bahan');
				$this->db->where('stock_bahan_lokasi_id', $lokasi_bahan);
				$this->db->where('bahan_id', $bahan_item["bahan_id"]);
				$this->db->where('delete_flag', "0");
				$stock_bahan_lokasi = $this->db->get('stock_bahan')->row();
				$total_stock = $stock_bahan_lokasi->total_stock_bahan;

				if ($bahan_item["jumlah"] > $total_stock) {
					return FALSE;
				}
			}

		}

		$data['lokasi_bahan_id'] = $this->input->post('lokasi_bahan_id');
		$data['produksi_kode'] = $this->input->post('produksi_kode');
		$data['tanggal_mulai'] = $this->input->post('tanggal_mulai');
		$data['estimasi_selesai'] = $this->input->post('estimasi_selesai');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['status_produksi'] = "Dikerjakan";
		$data['status_penerimaan'] = "Belum Diterima";
		$data['urutan'] = str_replace("BLP-", "", $this->input->post('produksi_kode'));
		$this->db->insert('produksi', $data);
		$produksi_id = $this->db->insert_id();
		
		foreach ($item as $key) {
			$data = array();
			$data['produk_id'] = $key["produk_id"];
			$data['jumlah'] = $this->string_to_number($key["jumlah"]);
			$data['sisa'] = $this->string_to_number($key["jumlah"]);
			$data['hpp'] = $key["total_hpp_produk"];
			if(isset($key['keterangan'])){
				$data['keterangan'] = $key["keterangan"];
			}
			$data["produksi_id"] = $produksi_id;
			$this->db->insert('produksi_item', $data);
			$produksi_item_id = $this->db->insert_id();
			foreach ($key["item_bahan"] as $key2) {
				$data = array();
				$data['produksi_item_id'] = $produksi_item_id;
				$data['bahan_id'] = $key2["bahan_id"];
				// $data['jumlah'] = $key2["jumlah_potong"];
				$data['jumlah'] = $key2["jumlah"];
				$this->db->insert('produksi_item_bahan', $data);
			}
		}
		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;		
	}
	function edit_produksi(){
		$this->db->trans_begin();
		$item = $this->input->post('item');
		$lokasi_bahan = $this->input->post('lokasi_bahan_id');
		
		foreach ($item as $bahan) {
			foreach ($bahan["item_bahan"] as $bahan_item) {
				$this->db->select('SUM(stock_bahan_qty) as total_stock_bahan');
				$this->db->where('stock_bahan_lokasi_id', $lokasi_bahan);
				$this->db->where('bahan_id', $bahan_item["bahan_id"]);
				$this->db->where('delete_flag', "0");
				$stock_bahan_lokasi = $this->db->get('stock_bahan')->row();
				$total_stock = $stock_bahan_lokasi->total_stock_bahan;

				if ($bahan_item["jumlah"] > $total_stock) {
					return FALSE;
				}
			}

		}

		$produksi_id = $this->input->post('produksi_id');
		$this->db->where('produksi_id', $produksi_id);
		$this->db->delete('produksi_item');
		$data['lokasi_bahan_id'] = $this->input->post('lokasi_bahan_id');
		$data['tanggal_mulai'] = $this->input->post('tanggal_mulai');
		$data['estimasi_selesai'] = $this->input->post('estimasi_selesai');
		$data['keterangan'] = $this->input->post('keterangan');
		$this->db->where('produksi_id', $produksi_id);
		$this->db->update('produksi', $data);
		
		foreach ($item as $key) {
			$data = array();
			$data['produk_id'] = $key["produk_id"];
			$data['jumlah'] = $this->string_to_number($key["jumlah"]);
			$data['sisa'] = $this->string_to_number($key["jumlah"]);
			$data['hpp'] = $key["total_hpp_produk"];
			if(isset($key['keterangan'])){
				$data['keterangan'] = $key["keterangan"];
			}
			$data["produksi_id"] = $produksi_id;
			$this->db->insert('produksi_item', $data);
			$produksi_item_id = $this->db->insert_id();
			foreach ($key["item_bahan"] as $key2) {
				$data = array();
				$data['produksi_item_id'] = $produksi_item_id;
				$data['bahan_id'] = $key2["bahan_id"];
				$data['jumlah'] = $key2["jumlah"];
				$this->db->insert('produksi_item_bahan', $data);
			}
		}
		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;		
	}
	function produksi_item_list($produksi_id){
		$this->db->where('produksi_id', $produksi_id);
		$this->db->order_by('produksi_id', 'desc');
		return $this->db->get('produksi_item')->result();
	}
	function item_bahan_in_produksi($produksi_id){
		$this->db->select('produksi_item.*,produksi_item_bahan.bahan_id,produksi_item_bahan.jumlah as "jumlah_bahan"');
		$this->db->join('produksi_item_bahan', 'produksi_item_bahan.produksi_item_id = produksi_item.produksi_item_id');
		$this->db->where('produksi_id', $produksi_id);
		return $this->db->get('produksi_item')->result();
	}
	function laporan_produksi_all(){
        $this->db->select('produksi.*');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produk', 'produksi_item.produk_id = produk.produk_id');
		return $this->db->count_all_results('produksi');
	}
	function laporan_produksi_filter($query){
        $this->db->select('produksi.*');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produk', 'produksi_item.produk_id = produk.produk_id');
		$this->db->group_start();
			$this->db->like('produksi_kode', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $query);
			$this->db->or_like('produksi_item.jumlah', $temp, 'BOTH');
		$this->db->group_end();
		if($this->input->get('produksi_kode')!=""){
			$this->db->like('produksi_kode', $this->input->get('produksi_kode'), 'BOTH');
		}
		if($this->input->get('mulai_start')!=""){
			$this->db->where('tanggal_mulai >=', $this->input->get('mulai_start'));
		}
		if($this->input->get('mulai_end')!=""){
			$this->db->where('tanggal_mulai <=', $this->input->get('mulai_end'));
		}
		if($this->input->get('status_produksi')!=""){
			$this->db->where('status_produksi', $this->input->get('status_produksi'));
		}
		if($this->input->get('penerimaan_start')!=""){
			$this->db->where('tanggal_penerimaan >=', $this->input->get('penerimaan_start'));
		}
		if($this->input->get('penerimaan_end')!=""){
			$this->db->where('tanggal_penerimaan <=', $this->input->get('penerimaan_end'));
		}
		if($this->input->get('status_penerimaan')!=""){
			$this->db->where('status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if($this->input->get('produk_id')!=""){
			$this->db->where('produksi_item.produk_id', $this->input->get('produk_id'));
		}
		return $this->db->count_all_results('produksi');
	}
	function laporan_produksi_list($start,$length,$query){
		$this->db->select('produksi.*,produksi_item.jumlah,produk.produk_nama');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produk', 'produksi_item.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->group_start();
			$this->db->like('produksi_kode', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $query);
			$this->db->or_like('produksi_item.jumlah', $temp, 'BOTH');
		$this->db->group_end();
		if($this->input->get('produksi_kode')!=""){
			$this->db->like('produksi_kode', $this->input->get('produksi_kode'), 'BOTH');
		}
		if($this->input->get('mulai_start')!=""){
			$this->db->where('tanggal_mulai >=', $this->input->get('mulai_start'));
		}
		if($this->input->get('mulai_end')!=""){
			$this->db->where('tanggal_mulai <=', $this->input->get('mulai_end'));
		}
		if($this->input->get('status_produksi')!=""){
			$this->db->where('status_produksi', $this->input->get('status_produksi'));
		}
		if($this->input->get('penerimaan_start')!=""){
			$this->db->where('tanggal_penerimaan >=', $this->input->get('penerimaan_start'));
		}
		if($this->input->get('penerimaan_end')!=""){
			$this->db->where('tanggal_penerimaan <=', $this->input->get('penerimaan_end'));
		}
		if($this->input->get('status_penerimaan')!=""){
			$this->db->where('status_penerimaan', $this->input->get('status_penerimaan'));
		}
		if($this->input->get('produk_id')!=""){
			$this->db->where('produksi_item.produk_id', $this->input->get('produk_id'));
		}
		$this->db->order_by('produksi.produksi_id', 'desc');
		return $this->db->get('produksi',$length,$start)->result();
	}
	function laporan_produksi_detail_all(){
        $this->db->select('produksi.*');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produk', 'produksi_item.produk_id = produk.produk_id');
		$this->db->join('produksi_item_bahan', 'produksi_item_bahan.produksi_item_id = produksi_item.produksi_item_id');
		$this->db->join('bahan', 'produksi_item_bahan.bahan_id = bahan.bahan_id');
		return $this->db->count_all_results('produksi');
	}
	function laporan_produksi_detail_filter($query){
        $this->db->select('produksi.*');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produk', 'produksi_item.produk_id = produk.produk_id');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->join('produksi_item_bahan', 'produksi_item_bahan.produksi_item_id = produksi_item.produksi_item_id');
		$this->db->join('bahan', 'produksi_item_bahan.bahan_id = bahan.bahan_id');
		$this->db->group_start();
			$this->db->like('produksi_kode', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $query);
			$this->db->or_like('produksi_item.jumlah', $temp, 'BOTH');
		$this->db->group_end();
		if($this->input->get('produksi_kode')!=""){
			$this->db->like('produksi_kode', $this->input->get('produksi_kode'), 'BOTH');
		}
		if($this->input->get('mulai_start')!=""){
			$this->db->where('tanggal_mulai >=', $this->input->get('mulai_start'));
		}
		if($this->input->get('mulai_end')!=""){
			$this->db->where('tanggal_mulai <=', $this->input->get('mulai_end'));
		}
		if($this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if($this->input->get('bahan_id')!=""){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		return $this->db->count_all_results('produksi');
	}
	function laporan_produksi_detail_list($start,$length,$query){
		$this->db->select('produksi.*,produk.produk_nama,produksi_item_bahan.jumlah,bahan.bahan_nama');
		$this->db->join('produksi_item', 'produksi_item.produksi_id = produksi.produksi_id');
		$this->db->join('produk', 'produksi_item.produk_id = produk.produk_id');
		$this->db->join('produksi_item_bahan', 'produksi_item_bahan.produksi_item_id = produksi_item.produksi_item_id');
		$this->db->join('bahan', 'produksi_item_bahan.bahan_id = bahan.bahan_id');
		$this->db->group_start();
			$this->db->like('produksi_kode', $query, 'BOTH');
			$temp = str_replace(",", "", $query);
			$temp = str_replace(".", "", $query);
			$this->db->or_like('produksi_item.jumlah', $temp, 'BOTH');
		$this->db->group_end();
		if($this->input->get('produksi_kode')!=""){
			$this->db->like('produksi_kode', $this->input->get('produksi_kode'), 'BOTH');
		}
		if($this->input->get('mulai_start')!=""){
			$this->db->where('tanggal_mulai >=', $this->input->get('mulai_start'));
		}
		if($this->input->get('mulai_end')!=""){
			$this->db->where('tanggal_mulai <=', $this->input->get('mulai_end'));
		}
		if($this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if($this->input->get('bahan_id')!=""){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		$this->db->order_by('produksi.produksi_id', 'desc');
		return $this->db->get('produksi',$length,$start)->result();
	}

	function konversi_bahan_by_produk($produksi_item_id,$bahan_id){
		$this->db->where('produksi_item_id',$produksi_item_id);
		$this->db->where('bahan_id',$bahan_id);
		return $this->db->get('produksi_item_bahan')->row();
	}

}

/* End of file Produksi.php */
/* Location: ./application/models/Produksi.php */
