<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reseller extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "reseller";
    }
    function prospective_all(){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where('reseller.status','request');
        $this->db->where("reseller.type","agent");
        return $this->db->count_all_results('reseller');
    }
    function prospective_filter($query){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where('reseller.status','request');
        $this->db->where("reseller.type","agent");
        $this->db->group_start();
            $this->db->or_like('reseller.nama',$query,'BOTH');
            $this->db->or_like('reseller.email',$query,'BOTH');
            $this->db->or_like('reseller.phone',$query,'BOTH');
            $this->db->or_like('reseller.no_ktp',$query,'BOTH');
            $this->db->or_like('reseller.alamat',$query,'BOTH');
            $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
            $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function prospective_list($start,$length,$query){
        $this->db->select('reseller.*,outlet.subdistrict_name as outlet_subdistrict_name,outlet.province as outlet_province,outlet.city as outlet_city,subdistrict.province,subdistrict.city,subdistrict.subdistrict_name,b.nama as referal_nama');
        $this->db->join('subdistrict as outlet','reseller.subdistrict_id = outlet.subdistrict_id');
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('reseller as b','reseller.referal_id = b.reseller_id','left');
        $this->db->where('reseller.status','request');
        $this->db->where("reseller.type","agent");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller')->result();
    }

    function reseller_downline($reseller_id){
        $this->db->select('*');
        $this->db->where("reseller.referal_id", $reseller_id);
        return $this->db->get('reseller')->result();
    }

    function agen_status_all($status){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');

        if($status!="accept") {
            $this->db->not_like("mykindofbeauty_master.login.username", "#", 'both');
        }
        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","agent");
        $this->db->where("reseller.demo",0);
        return $this->db->count_all_results('reseller');
    }
    function agen_status_filter($query, $status){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        if($status!="accept") {
            $this->db->not_like("mykindofbeauty_master.login.username", "#", 'both');
        }
        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","agent");
        $this->db->where("reseller.demo",0);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();

        if(isset($_GET['province_id'])&&$this->input->get('province_id')!=""){
			$this->db->where('reseller.outlet_province_id', $this->input->get('province_id'));
		}
        if(isset($_GET['city_id'])&&$this->input->get('city_id')!=""){
			$this->db->where('reseller.outlet_city_id', $this->input->get('city_id'));
		}
        if(isset($_GET['subdistrict_id'])&&$this->input->get('subdistrict_id')!=""){
			$this->db->where('reseller.outlet_subdistrict_id', $this->input->get('subdistrict_id'));
		}

        return $this->db->count_all_results('reseller');
    }
    function agen_status_list($start,$length,$query,$status){
        $this->db->select('reseller.*,mykindofbeauty_master.login.username,outlet.subdistrict_name as outlet_subdistrict_name,outlet.province as outlet_province,outlet.city as outlet_city,subdistrict.province,subdistrict.city,subdistrict.subdistrict_name,b.nama as referal_nama');
        $this->db->join('subdistrict as outlet','reseller.outlet_subdistrict_id = outlet.subdistrict_id');
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('reseller as b','reseller.referal_id = b.reseller_id','left');
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","agent");
        $this->db->where("reseller.demo",0);
        if($status!="accept") {
            $this->db->not_like("mykindofbeauty_master.login.username", "#", 'both');
        }
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        if(isset($_GET['province_id'])&&$this->input->get('province_id')!=""){
			$this->db->where('reseller.outlet_province_id', $this->input->get('province_id'));
		}
        if(isset($_GET['city_id'])&&$this->input->get('city_id')!=""){
			$this->db->where('reseller.outlet_city_id', $this->input->get('city_id'));
		}
        if(isset($_GET['subdistrict_id'])&&$this->input->get('subdistrict_id')!=""){
			$this->db->where('reseller.outlet_subdistrict_id', $this->input->get('subdistrict_id'));
		}
        return $this->db->get('reseller',$length,$start)->result();
    }

    function resseler_by_referal_code($referal_code){
        return $this->db->where("referal_code",$referal_code)->get("reseller")->row();
    }


    function super_all(){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("request"));
        $this->db->where("reseller.type","super agen");
        return $this->db->count_all_results('reseller');
    }
    function super_filter($query){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("request"));
        $this->db->where("reseller.type","super agen");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function super_list($start,$length,$query){
        $this->db->select('reseller.*,outlet.subdistrict_name as outlet_subdistrict_name,outlet.province as outlet_province,outlet.city as outlet_city,subdistrict.province,subdistrict.city,subdistrict.subdistrict_name,b.nama as referal_nama');
        $this->db->join('subdistrict as outlet','reseller.outlet_subdistrict_id = outlet.subdistrict_id');
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('reseller as b','reseller.referal_id = b.reseller_id','left');
        $this->db->where_in('reseller.status',array("request"));
        $this->db->where("reseller.type","super agen");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller',$length,$start)->result();
    }

    function pemetaan_super_all(){
        $this->db->select('subdistrict.province as provinsi, subdistrict.city as kabupaten, subdistrict.subdistrict_name as kecamatan, COUNT(*) as jumlah');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","super agen");
        $this->db->where("reseller.demo != ","1");
        $this->db->group_by("subdistrict.subdistrict_name");
        $this->db->order_by("subdistrict.province", "ASC");
        $this->db->order_by("subdistrict.city", "ASC");
        return $this->db->count_all_results('reseller');
    }
    function pemetaan_super_filter($query){
        $this->db->select('subdistrict.province as provinsi, subdistrict.city as kabupaten, subdistrict.subdistrict_name as kecamatan, COUNT(*) as jumlah');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","super agen");
        $this->db->where("reseller.demo != ","1");
        $this->db->group_start();
        $this->db->or_like('subdistrict.province',$query,'BOTH');
        $this->db->or_like('subdistrict.city',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        $this->db->group_by("subdistrict.subdistrict_name");
        $this->db->order_by("subdistrict.province", "ASC");
        $this->db->order_by("subdistrict.city", "ASC");
        return $this->db->count_all_results('reseller');
    }
    function pemetaan_super_list($start,$length,$query){
        $this->db->select('subdistrict.province as provinsi, subdistrict.city as kabupaten, subdistrict.subdistrict_name as kecamatan, COUNT(*) as jumlah');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","super agen");
        $this->db->where("reseller.demo != ","1");
        $this->db->group_start();
        $this->db->or_like('subdistrict.province',$query,'BOTH');
        $this->db->or_like('subdistrict.city',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        $this->db->group_by("subdistrict.subdistrict_name");
        $this->db->order_by("subdistrict.province", "ASC");
        $this->db->order_by("subdistrict.city", "ASC");
        return $this->db->get('reseller',$length,$start)->result();
    }

    function pemetaan_reseller_all(){
        $this->db->select('subdistrict.province as provinsi, subdistrict.city as kabupaten, subdistrict.subdistrict_name as kecamatan, COUNT(*) as jumlah');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","agent");
        $this->db->where("reseller.demo != ","1");
        $this->db->group_by("subdistrict.subdistrict_name");
        $this->db->order_by("subdistrict.province", "ASC");
        $this->db->order_by("subdistrict.city", "ASC");
        return $this->db->count_all_results('reseller');
    }
    function pemetaan_reseller_filter($query){
        $this->db->select('subdistrict.province as provinsi, subdistrict.city as kabupaten, subdistrict.subdistrict_name as kecamatan, COUNT(*) as jumlah');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","agent");
        $this->db->where("reseller.demo != ","1");
        $this->db->group_start();
        $this->db->or_like('subdistrict.province',$query,'BOTH');
        $this->db->or_like('subdistrict.city',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        $this->db->group_by("subdistrict.subdistrict_name");
        $this->db->order_by("subdistrict.province", "ASC");
        $this->db->order_by("subdistrict.city", "ASC");
        return $this->db->count_all_results('reseller');
    }
    function pemetaan_reseller_list($start,$length,$query){
        $this->db->select('subdistrict.province as provinsi, subdistrict.city as kabupaten, subdistrict.subdistrict_name as kecamatan, COUNT(*) as jumlah');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","agent");
        $this->db->where("reseller.demo != ","1");
        $this->db->group_start();
        $this->db->or_like('subdistrict.province',$query,'BOTH');
        $this->db->or_like('subdistrict.city',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        $this->db->group_by("subdistrict.subdistrict_name");
        $this->db->order_by("subdistrict.province", "ASC");
        $this->db->order_by("subdistrict.city", "ASC");
        return $this->db->get('reseller',$length,$start)->result();
    }

    function active_agen_all(){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","agent");
        return $this->db->count_all_results('reseller');
    }
    function active_agen_filter($query){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","agent");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.referal_code',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function active_agen_list($start,$length,$query){
        $this->db->select('reseller.*,subdistrict.subdistrict_name as outlet_subdistrict_name, reseller.nama as guest_nama,reseller.alamat as guest_alamat, reseller.phone as guest_telepon');
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.type","agent");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.referal_code',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller',$length,$start)->result();
    }

    function res_active_count_all(){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.demo",0);
        return $this->db->count_all_results('reseller');
    }
    function res_active_count_filter($query){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where_in('reseller.status',array("active"));
        $this->db->where("reseller.demo",0);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.referal_code',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function res_active_list($start,$length,$query){
        $this->db->select('reseller.*,subdistrict.subdistrict_name as outlet_subdistrict_name, reseller.nama as guest_nama,reseller.alamat as guest_alamat, reseller.phone as guest_telepon');
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where("reseller.demo",0);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.referal_code',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller',$length,$start)->result();
    }

    function super_agen_status_all($status){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","super agen");
        $this->db->where('reseller.demo',0);
        return $this->db->count_all_results('reseller');
    }
    function super_agen_status_filter($query, $status){
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->where('reseller.status',$status);
        $this->db->where('reseller.demo',0);
        $this->db->where("reseller.type","super agen");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();

        if(isset($_GET['province_id'])&&$this->input->get('province_id')!=""){
			$this->db->where('reseller.outlet_province_id', $this->input->get('province_id'));
		}
        if(isset($_GET['city_id'])&&$this->input->get('city_id')!=""){
			$this->db->where('reseller.outlet_city_id', $this->input->get('city_id'));
		}
        if(isset($_GET['subdistrict_id'])&&$this->input->get('subdistrict_id')!=""){
			$this->db->where('reseller.outlet_subdistrict_id', $this->input->get('subdistrict_id'));
		}

        return $this->db->count_all_results('reseller');
    }
    function super_agen_status_list($start,$length,$query,$status){
        $this->db->select('reseller.*,mykindofbeauty_master.login.username,outlet.subdistrict_name as outlet_subdistrict_name,outlet.province as outlet_province,outlet.city as outlet_city,subdistrict.province,subdistrict.city,subdistrict.subdistrict_name,b.nama as referal_nama');
        $this->db->join('subdistrict as outlet','reseller.outlet_subdistrict_id = outlet.subdistrict_id');
        $this->db->join('subdistrict','reseller.outlet_subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('reseller as b','reseller.referal_id = b.reseller_id','left');
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","super agen");
        $this->db->where('reseller.demo',0);
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->or_like('subdistrict.subdistrict_name',$query,'BOTH');
        $this->db->group_end();

        if(isset($_GET['province_id'])&&$this->input->get('province_id')!=""){
			$this->db->where('reseller.outlet_province_id', $this->input->get('province_id'));
		}
        if(isset($_GET['city_id'])&&$this->input->get('city_id')!=""){
			$this->db->where('reseller.outlet_city_id', $this->input->get('city_id'));
		}
        if(isset($_GET['subdistrict_id'])&&$this->input->get('subdistrict_id')!=""){
			$this->db->where('reseller.outlet_subdistrict_id', $this->input->get('subdistrict_id'));
		}
        
        return $this->db->get('reseller',$length,$start)->result();
    }

    function redgroup_all(){

        $this->db->where('reseller.status','request');
        $this->db->where("reseller.type","redgroup");
        return $this->db->count_all_results('reseller');
    }
    function redgroup_filter($query){

        $this->db->where('reseller.status','request');
        $this->db->where("reseller.type","redgroup");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function redgroup_list($start,$length,$query){
        $this->db->select('reseller.*,b.nama as referal_nama');

        $this->db->join('reseller as b','reseller.referal_id = b.reseller_id','left');
        $this->db->where('reseller.status','request');
        $this->db->where("reseller.type","redgroup");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->group_end();
        return $this->db->get('reseller',$length,$start)->result();
    }

    function redgroup_status_all($status){

        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","redgroup");
        return $this->db->count_all_results('reseller');
    }
    function redgroup_status_filter($query,$status){

        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","redgroup");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');
        $this->db->group_end();
        return $this->db->count_all_results('reseller');
    }
    function redgroup_status_list($start,$length,$query,$status){
        $this->db->select('reseller.*,b.nama as referal_nama');

        $this->db->join('reseller as b','reseller.referal_id = b.reseller_id','left');
        $this->db->where('reseller.status',$status);
        $this->db->where("reseller.type","redgroup");
        $this->db->group_start();
        $this->db->or_like('reseller.nama',$query,'BOTH');
        $this->db->or_like('reseller.email',$query,'BOTH');
        $this->db->or_like('reseller.phone',$query,'BOTH');
        $this->db->or_like('reseller.no_ktp',$query,'BOTH');
        $this->db->or_like('reseller.alamat',$query,'BOTH');
        $this->db->or_like('reseller.outlet_alamat',$query,'BOTH');

        $this->db->group_end();
        return $this->db->get('reseller',$length,$start)->result();
    }
    function group_count_all($reseller_id){
        $this->db->where("referal_id",$reseller_id);
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        $this->db->where('reseller.status','active');
        $this->db->not_like('mykindofbeauty_master.login.username', '#','both');
        return $this->db->count_all_results('reseller');
    }
    function group_count_filter($query,$reseller_id){
        $this->db->where("referal_id",$reseller_id);
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        $this->db->group_start();
            $this->db->or_like("nama",$query);
            $this->db->or_like("subdistrict_name",$query);
            $this->db->or_like("subdistrict_name",$query);
        $this->db->group_end();
        $this->db->where('reseller.status','active');
        $this->db->not_like('mykindofbeauty_master.login.username', '#','both');
        return $this->db->count_all_results('reseller');
    }
    function group_list($start,$length,$query,$reseller_id){
        $this->db->select("reseller.*,subdistrict.subdistrict_name, mykindofbeauty_master.login.username as username, if(b.total_bonus is null,0,b.total_bonus) as total_bonus,if(c.grand_total is null,0,c.grand_total) as total_penjualan, if(d.grand_total is null,0,d.grand_total) as total_po");
        $whereB = "";
        $whereC = "";
        $whereD = "";
        $tanggalStart = $this->input->get('tanggal_start');
        $tanggalEnd = $this->input->get('tanggal_end');
        if($tanggalStart!=""&&$tanggalEnd!=""){
            $whereB = " where (bonus.tanggal >= ".$tanggalStart." and bonus.tanggal <= ".$tanggalEnd.") ";
            $whereC = " where (penjualan.tanggal >= ".$tanggalStart." and penjualan.tanggal <= ".$tanggalEnd.") ";
            $whereD = " where (po_produk.tanggal_pemesanan >= ".$tanggalStart." and po_produk.tanggal_pemesanan <= ".$tanggalEnd.") ";
        }
        $b = "(SELECT sum(bonus.jumlah_bonus) as total_bonus,to_reseller_id from bonus ".$whereB." GROUP BY bonus.to_reseller_id)" ;
        $c = "(SELECT sum(penjualan.grand_total) as grand_total,mykindofbeauty_master.login.reseller_id as reseller_id
from penjualan 
INNER join mykindofbeauty_master.login on mykindofbeauty_master.login.lokasi_id = penjualan.lokasi_id ".$whereC."
GROUP BY penjualan.lokasi_id)";
        $d = "(SELECT sum(po_produk.grand_total) as grand_total,mykindofbeauty_master.login.reseller_id as reseller_id
from po_produk 
INNER JOIN mykindofbeauty_master.login on mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller ".$whereD."
GROUP BY po_produk.lokasi_reseller)";
        $this->db->where("referal_id",$reseller_id);
        $this->db->join('mykindofbeauty_master.login','reseller.reseller_id = mykindofbeauty_master.login.reseller_id','left');
        $this->db->join('subdistrict','reseller.subdistrict_id = subdistrict.subdistrict_id');
        $this->db->join($b." as b",'b.to_reseller_id = reseller.reseller_id','left');
        $this->db->join($c." as c",'c.reseller_id = reseller.reseller_id','left');
        $this->db->join($d." as d",'d.reseller_id = reseller.reseller_id','left');
        
        $this->db->group_start();
        $this->db->or_like("nama",$query);
        $this->db->or_like("subdistrict_name",$query);
        $this->db->or_like("subdistrict_name",$query);
        $this->db->group_end();
        $this->db->where('reseller.status','active');
        $this->db->not_like('mykindofbeauty_master.login.username', '#','both');
        return $this->db->get("reseller",$length,$start)->result();
    }
    function prospective_maps(){
        $this->db->select('count(reseller_id) as jumlah');
        $this->db->where("reseller.longitude is null",null,true);
        $this->db->where("reseller.latitude is null",null,true);
        $this->db->where("reseller.type",'agent');
        $jumlah = $this->db->get("reseller")->row();
        if($jumlah!=null){
            return $jumlah->jumlah;
        } else {
            return '';
        }

    }
    function super_maps(){
        $this->db->select('count(reseller_id) as jumlah');
        $this->db->where("reseller.longitude is null",null,true);
        $this->db->where("reseller.latitude is null",null,true);
        $this->db->where("reseller.type",'super agen');
        $jumlah = $this->db->get("reseller")->row();
        if($jumlah!=null){
            return $jumlah->jumlah;
        } else {
            return '';
        }

    }

    function count_super_agen(){
        $this->db->select('count(reseller_id) as jumlah');
        $this->db->where("reseller.status",'request');
        $this->db->where("reseller.type",'super agen');
        return $this->db->get("reseller")->row()->jumlah;
    }
    function count_agen(){
        $this->db->select('count(reseller_id) as jumlah');
        $this->db->where("reseller.status",'request');
        $this->db->where("reseller.type",'agent');
        return $this->db->get("reseller")->row()->jumlah;
    }


    function reseller_refernsi($referal_id){
        $this->db->select('if(count(reseller_id) is null,0,count(reseller_id)) as jumlah,type');
        $this->db->where('referal_id',$referal_id);
        $this->db->where('status','active');
        $this->db->group_by('type');
        return $this->db->get('reseller')->result();
    }

    // accounting

    function daftar_reseller_aktif_all(){
        $this->db->select("reseller.nama, if(reseller.type = 'super agen','super reseller','reseller') as tipe");
        $this->db->where('status', 'active');
        $this->db->where('demo', 0);
        return $this->db->count_all_results('reseller');
    }
    function daftar_reseller_aktif_filter($query){
        $this->db->select("reseller.nama, if(reseller.type = 'super agen','super reseller','reseller') as tipe");
        $this->db->where('status', 'active');
        $this->db->where('demo', 0);

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('reseller.type', $query, 'BOTH');
        $this->db->group_end();

        return $this->db->count_all_results('reseller');
    }
    function daftar_reseller_aktif_list($start,$length,$query){
        $this->db->select("reseller.nama, if(reseller.type = 'super agen','super reseller','reseller') as tipe");
        $this->db->where('status', 'active');
        $this->db->where('demo', 0);

        $this->db->group_start();
        $this->db->like('reseller.nama', $query, 'BOTH');
        $this->db->or_like('reseller.type', $query, 'BOTH');
        $this->db->group_end();

        return $this->db->get('reseller', $length, $start)->result();
    }
    function cek_leader($reseller_id){
        $sql = 'call cek_super_res('.$reseller_id.','.$reseller_id.',0)';
        $this->db->query($sql);
    }

    function super_reseller_active(){
        $this->db->select('reseller.*');
        // $this->db->join('mykindofbeauty_master.login','po_produk.lokasi_reseller = mykindofbeauty_master.login.lokasi_id');
        $this->db->where('reseller.status',"active");
        $this->db->where("reseller.type","super agen");
        $this->db->where('reseller.demo',0);
        return $this->db->get('reseller')->result();
    }

    function list_reseller_hangus($date){
        $this->db->select("reseller.*,login.lokasi_id");
        $this->db->join("mykindofbeauty_master.login as login","login.reseller_id = reseller.reseller_id");
        $this->db->where("next_deposit_date",$date);
        $this->db->where("type","super agen");
        return $this->db->get("reseller")->result();
    }

    function leaderLain($leader_id){
        $this->db->select('reseller_id,nama');
        $this->db->where('type','super agen');
        $this->db->where('first_deposit_date is not null',null,false);
        $this->db->where('demo',0);
        $this->db->where('reseller_id !=',$leader_id);
        return $this->db->get('reseller')->result();
    }
}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
