<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengiriman_hadiah_event extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "pengiriman_hadiah_event";
    }


    function pengiriman_hadiah_event_list($start,$length,$query){
        $this->db->select("reseller.nama,pengiriman_hadiah_event.*,event.nama as 'nama_event'");
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.status',"On Process");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.jumlah',$query);
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.status',$query);
        $this->db->or_like('pengiriman_hadiah_event.manage_status',$query);
        $this->db->group_end();
        return $this->db->get('pengiriman_hadiah_event', $length, $start)->result();
    }
    function pengiriman_hadiah_event_all(){
        $this->db->select("reseller.nama,pengiriman_hadiah_event.*,event.nama as 'nama_event'");
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.status',"On Process");
        return $this->db->count_all_results('pengiriman_hadiah_event');
    }
    function pengiriman_hadiah_event_filter($query){
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.status',"On Process");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.jumlah',$query);
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.status',$query);
        $this->db->or_like('pengiriman_hadiah_event.manage_status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('pengiriman_hadiah_event');
    }


    function pengiriman_hadiah_event_list_success($start,$length,$query){
        $this->db->select("reseller.nama,pengiriman_hadiah_event.*,event.nama as 'nama_event'");
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.status',"Terkirim");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.jumlah',$query);
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.status',$query);
        $this->db->or_like('pengiriman_hadiah_event.manage_status',$query);
        $this->db->group_end();
        return $this->db->get('pengiriman_hadiah_event', $length, $start)->result();
    }
    function pengiriman_hadiah_event_all_success(){
        $this->db->select("reseller.nama,pengiriman_hadiah_event.*,event.nama as 'nama_event'");
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.status',"Terkirim");
        return $this->db->count_all_results('pengiriman_hadiah_event');
    }
    function pengiriman_hadiah_event_filter_success($query){
        $this->db->join('event','pengiriman_hadiah_event.event_id = event.event_id');
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->order_by('pengiriman_hadiah_event_id', 'desc');
        $this->db->where('pengiriman_hadiah_event.status',"Terkirim");
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('tanggal >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('tanggal <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('tanggal',$query);
        $this->db->or_like('reseller.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.jumlah',$query);
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('pengiriman_hadiah_event.status',$query);
        $this->db->or_like('pengiriman_hadiah_event.manage_status',$query);
        $this->db->group_end();
        return $this->db->count_all_results('pengiriman_hadiah_event');
    }


    function bonus_hari_ini(){
        $this->db->select("reseller.nama,bonus.*,b.nama as nama_penerima");
        $this->db->join('reseller','reseller.reseller_id = bonus.from_reseller_id');
        $this->db->join('reseller as b','b.reseller_id = bonus.to_reseller_id');
        $this->db->order_by('bonus_id', 'desc');
        $this->db->where('bonus.tanggal <=',date("Y-m-d"));
        $this->db->where('pengiriman_hadiah_event.status',"On Process");
        return $this->db->get('bonus')->result();
    }
    function cekBonus($po_produk_id,$to_reseller_id,$type="referal bonus"){
        $this->db->where('to_reseller_id',$to_reseller_id);
        $this->db->where('po_produk_id',$po_produk_id);
        $this->db->where('type',$type);
        $check = $this->db->get('bonus')->row();
        $result = false;
        if($check==null){
            $result = true;
        }
        return $result;
    }
    function cekBonusWithdraw($pembayaran_hutang_produk_id,$to_reseller_id,$type="referal bonus"){
        $this->db->where('to_reseller_id',$to_reseller_id);
        $this->db->where('pembayaran_hutang_produk_id',$pembayaran_hutang_produk_id);
        $this->db->where('type',$type);
        $check = $this->db->get('bonus')->row();
        $result = false;
        if($check==null){
            $result = true;
        }
        return $result;
    }
    function changeToProcessing($arr_id){
        $data["manage_status"] = "processing";
        $this->db->where_in('pengiriman_hadiah_event_id',$arr_id);
        return $this->db->update("pengiriman_hadiah_event",$data);
    }
    function changeToSuccess($arr_id){
        $data["manage_status"] = "success";
        $data["tanggal_pengiriman"] = date("Y-m-d");
        $data["status"] = "Terkirim";
        $this->db->where_in('pengiriman_hadiah_event_id',$arr_id);
        return $this->db->update("pengiriman_hadiah_event",$data);
    }
    function changeToFail($arr_id){
        $data["manage_status"] = "failed";
        $this->db->where_in('pengiriman_hadiah_event_id',$arr_id);
        return $this->db->update("pengiriman_hadiah_event",$data);
    }
    function changeToTurnback($arr_id){
        $data["manage_status"] = "waiting";
        $data["tanggal_pengiriman"] = date("Y-m-d");
        $data["status"] = "On Process";
        $this->db->where_in('pengiriman_hadiah_event_id',$arr_id);
        return $this->db->update("pengiriman_hadiah_event",$data);
    }

    function bonusListPrint($bonus_id){
        $this->db->select("pengiriman_hadiah_event.*,sum(pengiriman_hadiah_event.jumlah) as total_bonus,reseller.bank_atas_nama,nama_bank,kode_bank,reseller.email,reseller.bank_rekening,if(npwp.case is null,2,npwp.case) as case_res");
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->join('npwp','reseller.reseller_id = npwp.reseller_id','left');
        $this->db->where_in("pengiriman_hadiah_event_id",$bonus_id);
        $this->db->group_by("to_reseller_id");
        return $this->db->get("pengiriman_hadiah_event")->result();
    }
    function bonusTotal($bonus_id){
        $this->db->select("sum(pengiriman_hadiah_event.jumlah) as total_bonus");
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->where_in("pengiriman_hadiah_event_id",$bonus_id);
        $bonus = $this->db->get("pengiriman_hadiah_event")->row();
        if($bonus==null){
            return 0;
        }else{
            return $bonus->total_bonus;
        }
    }
    function listForDPPKontes($pengiriman_hadiah_event_id){
        $this->db->select("pengiriman_hadiah_event.*,group_concat(pengiriman_hadiah_event.pengiriman_hadiah_event_id) as pengiriman_hadiah_event_ids,sum(pengiriman_hadiah_event.jumlah) as total_bonus,reseller.bank_atas_nama,nama_bank,kode_bank,reseller.email,reseller.bank_rekening,if(npwp.case is null,2,npwp.case) as case_res,if(npwp.ptkp is null,0,npwp.ptkp) as ptkp ");
        $this->db->join('reseller','reseller.reseller_id = pengiriman_hadiah_event.to_reseller_id');
        $this->db->join('bank','bank.bank_id = reseller.bank_id');
        $this->db->join('npwp','reseller.reseller_id = npwp.reseller_id','left');
        $this->db->where_in("pengiriman_hadiah_event_id",$pengiriman_hadiah_event_id);
        $this->db->group_by("to_reseller_id");
        return $this->db->get("pengiriman_hadiah_event")->result();
    }
    function update_bonus_send($send_bonus_id,$bonus_ids){
        $data['send_bonus_id'] = $send_bonus_id;
        $this->db->where_in('pengiriman_hadiah_event_id',$bonus_ids);
        return $this->db->update('pengiriman_hadiah_event',$data);
    }

    function generate_winner_reseller(){
        $select = 'reseller.reseller_id,reseller.nama as "nama",b.grand_total as "total_po",c.total as "total_po_keseluruhan",d.qty as "jumlah_pos",(b.grand_total/55000) as "jumlah_order",if((b.grand_total/55000) <= d.qty,(b.grand_total/55000),d.qty) as "jumlah_diakui"';
        $b = '(SELECT reseller.reseller_id,sum( po_produk.grand_total) as grand_total from po_produk
INNER JOIN mykindofbeauty_master.login on mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on mykindofbeauty_master.login.reseller_id = reseller.reseller_id
where po_produk.updated_at >="2022-01-05 00:00:00"
and po_produk.updated_at <="2022-01-11 00:00:00"
and status_pembayaran = "Lunas"
and reseller.type = "agent" GROUP BY reseller.reseller_id)';
        $c = '(SELECT reseller.reseller_id,(sum( po_produk.grand_total)) as total 
from po_produk
INNER JOIN mykindofbeauty_master.login on mykindofbeauty_master.login.lokasi_id = po_produk.lokasi_reseller
INNER JOIN reseller on mykindofbeauty_master.login.reseller_id = reseller.reseller_id
where status_pembayaran = "Lunas"
and reseller.type = "agent" GROUP BY reseller.reseller_id)';
        $d = '(SELECT reseller.reseller_id,(sum(penjualan_produk.qty)) as qty FROM penjualan
INNER JOIN penjualan_produk on penjualan_produk.penjualan_id = penjualan.penjualan_id
INNER JOIN mykindofbeauty_master.login on penjualan.lokasi_id = mykindofbeauty_master.login.lokasi_id
INNER JOIN reseller on mykindofbeauty_master.login.reseller_id = reseller.reseller_id
where reseller.type = "agent"
and penjualan.tanggal >= "2022-01-05"
and penjualan.tanggal <= "2022-01-10"
GROUP BY reseller.reseller_id)';
        $this->db->select($select);
        $this->db->join($b.' as b','on reseller.reseller_id = b.reseller_id');
        $this->db->join($c.' as c','on reseller.reseller_id = c.reseller_id');
        $this->db->join($d.' as d','on reseller.reseller_id = d.reseller_id');
        $this->db->where('reseller.type','agent');
        $this->db->where(' reseller.status','active');
        $this->db->where(' c.total >',0);
        $this->db->where(' d.qty >=',50);
        $this->db->where(' b.grand_total >',0);
        $this->db->order_by('jumlah_diakui','desc');
        return $this->db->get('reseller')->result();
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */