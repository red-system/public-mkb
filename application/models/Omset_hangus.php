<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Omset_hangus extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "omset_hangus";
    }
    function omset_hangus_reseller($reseller_id){
        $this->db->select("sum(jumlah) as total");
        $this->db->where("reseller_id",$reseller_id);
        $result = $this->db->get("omset_hangus")->row()->total;
        if($result==null){
            return 0;
        }else {
            return $result;
        }
    }
}

/* End of file Color.php */
/* Location: ./application/models/Color.php */
