<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fullpayment extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "total_order_produk";
    }
    function is_fullpayment ($reseller_id){

        $total_batas = 0;
        $total_deposit = 0;
        $type = 'agent';
        $total_payment_reseller = 2750000;
        $total_payment_super = 27500000;
        $data = $this->db->where('reseller_id',$reseller_id)
                    ->get($this->table_name)->result();
        foreach ($data as $item){
            $total_batas = $item->batas+$total_batas;
            $total_deposit = $item->total + $total_deposit;
            $type = $item->type;
        }
        if($total_batas>=2){
            $result = true;
        }else if($type=="agent"&&$total_deposit>=$total_payment_reseller){
            $result = true;
        }else if($type=="super agen"&&$total_deposit>=$total_payment_super){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }
}

/* End of file Lokasi.php */
/* Location: ./application/models/Lokasi.php */