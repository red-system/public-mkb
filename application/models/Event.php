<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "event";
    }
    function event_list($start,$length,$query){
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('waktu_mulai >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('waktu_selesai <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('event.deskripsi',$query);
        $this->db->group_end();
        return $this->db->get('event', $length, $start)->result();
    }
    function event_all(){

        return $this->db->count_all_results('event');
    }
    function event_filter($query){
        if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
            $this->db->where('waktu_mulai >=', $this->input->get('tanggal_start'));
        }
        if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
            $this->db->where('waktu_selesai <=', $this->input->get('tanggal_end'));
        }
        $this->db->group_start();
        $this->db->or_like('event.nama',$query);
        $this->db->or_like('event.deskripsi',$query);
        $this->db->group_end();
        return $this->db->count_all_results('event');
    }
}