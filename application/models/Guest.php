<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "guest";
	}
	function guest_count_all(){
        $this->db->select('guest.*');

		return $this->db->count_all_results('guest');
	}
	function guest_count_filter($query){
		$this->db->select('guest.*');

		$this->db->group_start();
			$this->db->like('guest_nama', $query, 'BOTH');
			$this->db->or_like('guest_alamat', $query, 'BOTH');
			$this->db->or_like('guest_telepon', $query, 'BOTH');
		$this->db->group_end();
		return $this->db->count_all_results('guest');
	}
	function guest_list($start,$length,$query){
		$this->db->select('guest.*');
		$this->db->group_start();
			$this->db->like('guest_nama', $query, 'BOTH');
			$this->db->or_like('guest_alamat', $query, 'BOTH');
			$this->db->or_like('guest_telepon', $query, 'BOTH');

		$this->db->group_end();
		$this->db->order_by('tanggal', 'desc');
		$this->db->order_by('guest_id', 'desc');
		//echo $this->db->_compile_select();
		return $this->db->get('guest',$length,$start)->result();		
	}
    function country_list($start,$length,$query,$search=null){
        $this->db->select('kewarganegaraan,COUNT(kewarganegaraan) as qty');
        $this->db->from('guest');
        $this->db->group_start();
        $this->db->like('kewarganegaraan', $query, 'BOTH');
        $this->db->group_end();
        if($search!=null){
            $this->db->where('tanggal >=',$search["min_tanggal"]." 00:00:00");
            $this->db->where('tanggal <=',$search["max_tanggal"]." 23:59:00");
            if($search["lokasi_id"]!="All"){
                $this->db->where('lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('kewarganegaraan');
        $this->db->order_by('qty', 'desc');
        return $this->db->get('', $length, $start)->result();
    }
    function country_count_all($search=null){
        $this->db->select('kewarganegaraan,COUNT(kewarganegaraan) as qty');
        if($search!=null){
            $this->db->where('tanggal >=',$search["min_tanggal"]." 00:00:00");
            $this->db->where('tanggal <=',$search["max_tanggal"]." 23:59:00");
            if($search["lokasi_id"]!="All"){
                $this->db->where('lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('kewarganegaraan');
        $this->db->order_by('qty', 'desc');
        return $this->db->count_all_results('guest');
    }
    function country_count_filter($query,$search=null){
        $this->db->select('kewarganegaraan,COUNT(kewarganegaraan) as qty');
        $this->db->group_start();
        $this->db->like('kewarganegaraan', $query, 'BOTH');
        $this->db->group_end();
        if($search!=null){
            $this->db->where('tanggal >=',$search["min_tanggal"]." 00:00:00");
            $this->db->where('tanggal <=',$search["max_tanggal"]." 23:59:00");
            if($search["lokasi_id"]!="All"){
                $this->db->where('lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('kewarganegaraan');
        $this->db->order_by('qty', 'desc');
        return $this->db->count_all_results('guest');
    }
	

}

/* End of file Guest.php */
/* Location: ./application/models/Guest.php */