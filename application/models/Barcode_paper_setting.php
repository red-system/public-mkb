<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barcode_paper_setting extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "barcode_paper_setting";
    }
}

/* End of file Guest.php */
/* Location: ./application/models/Guest.php */