<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "penerimaan_produk";
	}
    
	function penerimaan_by_id($po_kutus_kutus_id){

		$jumlah_order = 'po_kutus_kutus_detail.jumlah';
        $sisa = '(if(sum(po_kutus_kutus_detail.sisa)is null, 0, sum(po_kutus_kutus_detail.sisa)))';
        $terkirim = '('. $jumlah_order.' - '. $sisa .')';

        $this->db->select('po_kutus_kutus.*, '.$jumlah_order.' as jumlah_order, '. $terkirim .' as terkirim, '.$sisa.' as sisa, po_kutus_kutus_detail.po_kutus_kutus_detail_id');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus.po_kutus_kutus_id = po_kutus_kutus_detail.po_kutus_kutus_id', 'left');
        $this->db->where('po_kutus_kutus.po_kutus_kutus_id ', $po_kutus_kutus_id);
        return $this->db->get('po_kutus_kutus')->row();
	}

	function penerimaan_produk_count($po_kutus_kutus_id){
		$this->db->select('penerimaan_produk.*');
		$this->db->where('penerimaan_produk.po_kutus_kutus_id', $po_kutus_kutus_id);
		return $this->db->count_all_results('penerimaan_produk');
	}
	function penerimaan_produk_filter($po_kutus_kutus_id,$query){
		$this->db->select('penerimaan_produk.*');
		$this->db->where('penerimaan_produk.po_kutus_kutus_id', $po_kutus_kutus_id);
		return $this->db->count_all_results('penerimaan_produk');
	}
	function penerimaan_produk_list($start,$length,$query,$po_kutus_kutus_id){
		$this->db->select('penerimaan_produk.*,penerimaan_produk.total_qty_penerimaan as "old_jumlah"');
		$this->db->order_by('penerimaan_produk.penerimaan_produk_id', 'desc');
		$this->db->where('penerimaan_produk.po_kutus_kutus_id', $po_kutus_kutus_id);
		return $this->db->get('penerimaan_produk',$length,$start)->result();		
	}

    function penerimaan_by_produksi($produksi_id){

		$jumlah_order = 'produksi_item.jumlah';
        $sisa = '(if(sum(produksi_item.sisa)is null, 0, sum(produksi_item.sisa)))';
        $terkirim = '('. $jumlah_order.' - '. $sisa .')';

        $this->db->select('produksi.*, '.$jumlah_order.' as jumlah_order, '. $terkirim .' as terkirim, '.$sisa.' as sisa, produksi_item.produksi_item_id');
        $this->db->join('produksi_item', 'produksi.produksi_id = produksi_item.produksi_id', 'left');
        $this->db->where('produksi.produksi_id ', $produksi_id);
        return $this->db->get('produksi')->row();
	}

    function penerimaan_produk_produksi_count($produksi_id){
		$this->db->select('penerimaan_produk.*');
		$this->db->where('penerimaan_produk.produksi_id', $produksi_id);
		return $this->db->count_all_results('penerimaan_produk');
	}
	function penerimaan_produk_produksi_filter($produksi_id,$query){
		$this->db->select('penerimaan_produk.*');
		$this->db->where('penerimaan_produk.produksi_id', $produksi_id);
		return $this->db->count_all_results('penerimaan_produk');
	}
	function penerimaan_produk_produksi_list($start,$length,$query,$produksi_id){
		$this->db->select('penerimaan_produk.*,penerimaan_produk.total_qty_penerimaan as "old_jumlah"');
		$this->db->order_by('penerimaan_produk.penerimaan_produk_id', 'desc');
		$this->db->where('penerimaan_produk.produksi_id', $produksi_id);
		return $this->db->get('penerimaan_produk',$length,$start)->result();		
	}

	function get_produk_detail_by_po($po_kutus_kutus_id){
		$this->db->select('po_kutus_kutus.po_kutus_kutus_id, po_kutus_kutus.status_penerimaan as status_po, po_kutus_kutus_detail.po_kutus_kutus_detail_id, po_kutus_kutus_detail.jumlah, po_kutus_kutus_detail.sisa, po_kutus_kutus_detail.produk_id');
		$this->db->join('po_kutus_kutus', 'po_kutus_kutus_detail.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id','left');
		$this->db->order_by('po_kutus_kutus.po_kutus_kutus_id', 'asc');
        $this->db->where('po_kutus_kutus.po_kutus_kutus_id', $po_kutus_kutus_id);
        $this->db->where('po_kutus_kutus_detail.sisa !=', 0);
		return $this->db->get('po_kutus_kutus_detail')->result();		
	}

    function get_produk_detail_by_produksi($produksi_id){
		$this->db->select('produksi.produksi_id, produksi.status_penerimaan as status_po, produksi_item.produksi_item_id, produksi_item.jumlah, produksi_item.sisa, produksi_item.produk_id');
		$this->db->join('produksi', 'produksi_item.produksi_id = produksi.produksi_id','left');
		$this->db->order_by('produksi.produksi_id', 'asc');
        $this->db->where('produksi.produksi_id', $produksi_id);
        $this->db->where('produksi_item.sisa !=', 0);
		return $this->db->get('produksi_item')->result();		
	}

	function count_total_sisa($po_kutus_kutus_id){
        $this->db->select('(if(sum(po_kutus_kutus_detail.sisa)is null, 0, sum(po_kutus_kutus_detail.sisa))) as sisa');
        $this->db->join('po_kutus_kutus_detail', 'po_kutus_kutus.po_kutus_kutus_id = po_kutus_kutus_detail.po_kutus_kutus_id', 'left');
        $this->db->where('po_kutus_kutus.po_kutus_kutus_id ', $po_kutus_kutus_id);
        return $this->db->get('po_kutus_kutus')->row();
	}

    function count_total_sisa_produksi($produksi_id){
        $this->db->select('(if(sum(produksi_item.sisa)is null, 0, sum(produksi_item.sisa))) as sisa');
        $this->db->join('produksi_item', 'produksi.produksi_id = produksi_item.produksi_id', 'left');
        $this->db->where('produksi.produksi_id ', $produksi_id);
        return $this->db->get('produksi')->row();
	}

	function delete_penerimaan($penerimaan_produk_id){
		$this->db->where('penerimaan_produk_id', $penerimaan_produk_id);
		return $this->db->delete('penerimaan_produk');
	}

	function detail_penerimaan($penerimaan_produk_id){
		$this->db->where('penerimaan_produk_id', $penerimaan_produk_id);
		$this->db->from('penerimaan_produk');
		return $this->db->get()->row();
	}

	// accounting
	function penerimaan_produk_produsen_all(){
        $this->db->select('po_kutus_kutus.po_kutus_kutus_no, penerimaan_produk.tanggal_penerimaan, penerimaan_produk.total_qty_penerimaan, penerimaan_produk.keterangan_penerimaan');
        $this->db->join('po_kutus_kutus', 'penerimaan_produk.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->where('penerimaan_produk.status_penerimaan', '1');
        $this->db->order_by('penerimaan_produk.tanggal_penerimaan', 'DESC');
        return $this->db->count_all_results('penerimaan_produk');
    }

    function penerimaan_produk_produsen_filter($query){
        $this->db->select('po_kutus_kutus.po_kutus_kutus_no, penerimaan_produk.tanggal_penerimaan, penerimaan_produk.total_qty_penerimaan, penerimaan_produk.keterangan_penerimaan');
        $this->db->join('po_kutus_kutus', 'penerimaan_produk.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->where('penerimaan_produk.status_penerimaan', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('penerimaan_produk.tanggal_penerimaan >=', $this->input->get('start_tanggal'));
            $this->db->where('penerimaan_produk.tanggal_penerimaan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('penerimaan_produk.total_qty_penerimaan', $query, 'BOTH');
        $this->db->or_like('penerimaan_produk.tanggal_penerimaan', $query, 'BOTH');
        $this->db->or_like('penerimaan_produk.keterangan_penerimaan', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('penerimaan_produk.tanggal_penerimaan', 'DESC');

        return $this->db->count_all_results('penerimaan_produk');
    }
    function penerimaan_produk_produsen_list($start,$length,$query){
        $this->db->select('po_kutus_kutus.po_kutus_kutus_no, penerimaan_produk.tanggal_penerimaan, penerimaan_produk.total_qty_penerimaan, penerimaan_produk.keterangan_penerimaan');
        $this->db->join('po_kutus_kutus', 'penerimaan_produk.po_kutus_kutus_id = po_kutus_kutus.po_kutus_kutus_id');
        $this->db->where('penerimaan_produk.status_penerimaan', '1');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('penerimaan_produk.tanggal_penerimaan >=', $this->input->get('start_tanggal'));
            $this->db->where('penerimaan_produk.tanggal_penerimaan <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('po_kutus_kutus.po_kutus_kutus_no', $query, 'BOTH');
        $this->db->or_like('penerimaan_produk.total_qty_penerimaan', $query, 'BOTH');
        $this->db->or_like('penerimaan_produk.tanggal_penerimaan', $query, 'BOTH');
        $this->db->or_like('penerimaan_produk.keterangan_penerimaan', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('penerimaan_produk.tanggal_penerimaan', 'DESC');
        
        return $this->db->get('penerimaan_produk', $length, $start)->result();
    }

    function history_hpp_produk_all(){
        $this->db->select('produk.produk_nama, history_hpp_produk.*');
        $this->db->join('produk', 'history_hpp_produk.produk_id = produk.produk_id');
        $this->db->order_by('history_hpp_produk.tanggal', 'DESC');
        return $this->db->count_all_results('history_hpp_produk');
    }

    function history_hpp_produk_filter($query){
        $this->db->select('produk.produk_nama, history_hpp_produk.*');
        $this->db->join('produk', 'history_hpp_produk.produk_id = produk.produk_id');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('history_hpp_produk.tanggal >=', $this->input->get('start_tanggal'));
            $this->db->where('history_hpp_produk.tanggal <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('history_hpp_produk.tanggal', $query, 'BOTH');
        $this->db->or_like('history_hpp_produk.produk_qty', $query, 'BOTH');
        $this->db->or_like('history_hpp_produk.hpp', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('history_hpp_produk.tanggal', 'DESC');

        return $this->db->count_all_results('history_hpp_produk');
    }
    function history_hpp_produk_list($start,$length,$query){
        $this->db->select('produk.produk_nama, history_hpp_produk.*');
        $this->db->join('produk', 'history_hpp_produk.produk_id = produk.produk_id');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('history_hpp_produk.tanggal >=', $this->input->get('start_tanggal'));
            $this->db->where('history_hpp_produk.tanggal <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('produk.produk_nama', $query, 'BOTH');
        $this->db->or_like('history_hpp_produk.tanggal', $query, 'BOTH');
        $this->db->or_like('history_hpp_produk.produk_qty', $query, 'BOTH');
        $this->db->or_like('history_hpp_produk.hpp', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('history_hpp_produk.tanggal', 'DESC');
        
        return $this->db->get('history_hpp_produk', $length, $start)->result();
    }
	
}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */