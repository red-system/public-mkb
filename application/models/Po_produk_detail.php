<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_produk_detail extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "po_produk_detail";
	}

	function get_produk_detail_by_po($po_produk_id){
		$this->db->select('*');
		$this->db->where('po_produk_id', $po_produk_id);
        $this->db->order_by('po_produk_detail_id', 'DESC');
        $this->db->limit(1);
		return $this->db->get('po_produk_detail')->result();		
	}
	function cek_sisa($po_produk_id){
	    $this->db->select('if(sum(sisa_qty) is null,0,sum(sisa_qty)) as sisa_qty');
	    $this->db->where('po_produk_id',$po_produk_id);
	    return $this->db->get('po_produk_detail')->row();
    }
}

/* End of file Po_produk.php */
/* Location: ./application/models/Po_produk.php */