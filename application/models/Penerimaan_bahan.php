<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_bahan extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "penerimaan_bahan";
	}
    
	function penerimaan_by_id($po_bahan_id){

		// $jumlah_order = 'po_bahan_detail.jumlah';
        // $sisa = '(if(sum(po_bahan_detail.sisa)is null, 0, sum(po_bahan_detail.sisa)))';
        // $terkirim = '('. $jumlah_order.' - '. $sisa .')';

        $this->db->select('po_bahan.*');
        $this->db->where('po_bahan.po_bahan_id ', $po_bahan_id);
        return $this->db->get('po_bahan')->row();
	}

    function get_bahan_detail($po_bahan_id){
		$this->db->select('po_bahan_detail.*, bahan.bahan_nama');
        $this->db->join('bahan', 'po_bahan_detail.bahan_id = bahan.bahan_id');
		$this->db->where('po_bahan_detail.po_bahan_id', $po_bahan_id);
		return $this->db->get('po_bahan_detail')->result();
	}

    function get_po_bahan_detail($po_bahan_detail_id){
		$this->db->select('po_bahan_detail.*');
		$this->db->where('po_bahan_detail_id', $po_bahan_detail_id);
		return $this->db->get('po_bahan_detail')->row();
	}

	function penerimaan_bahan_count($po_bahan_id){
		$this->db->select('penerimaan_bahan.*');
		$this->db->where('penerimaan_bahan.po_bahan_id', $po_bahan_id);
		return $this->db->count_all_results('penerimaan_bahan');
	}
	function penerimaan_bahan_filter($po_bahan_id,$query){
		$this->db->select('penerimaan_bahan.*');
		$this->db->where('penerimaan_bahan.po_bahan_id', $po_bahan_id);
		return $this->db->count_all_results('penerimaan_bahan');
	}
	function penerimaan_bahan_list($start,$length,$query,$po_bahan_id){

        $this->db->select('penerimaan_bahan.*, bahan.bahan_nama, po_bahan_detail.jumlah, po_bahan_detail.sisa, (po_bahan_detail.jumlah - po_bahan_detail.sisa) as terkirim');
        $this->db->join('po_bahan_detail', 'penerimaan_bahan.po_bahan_detail_id = po_bahan_detail.po_bahan_detail_id');
        $this->db->join('bahan', 'po_bahan_detail.bahan_id = bahan.bahan_id');
        $this->db->where('penerimaan_bahan.po_bahan_id ', $po_bahan_id);
        $this->db->order_by('penerimaan_bahan.penerimaan_bahan_id', 'desc');
		return $this->db->get('penerimaan_bahan',$length,$start)->result();		
	}

	function get_produk_detail_by_po($po_bahan_id){
		$this->db->select('po_bahan.po_bahan_id, po_bahan.status_penerimaan as status_po, po_bahan_detail.po_bahan_detail_id, po_bahan_detail.jumlah, po_bahan_detail.sisa');
		$this->db->join('po_bahan', 'po_bahan_detail.po_bahan_id = po_bahan.po_bahan_id','left');
		$this->db->order_by('po_bahan.po_bahan_id', 'asc');
        $this->db->where('po_bahan.po_bahan_id', $po_bahan_id);
        $this->db->where('po_bahan_detail.sisa !=', 0);
		return $this->db->get('po_bahan_detail')->result();		
	}

	function count_total_sisa($po_bahan_id){
        $this->db->select('(if(sum(po_bahan_detail.sisa)is null, 0, sum(po_bahan_detail.sisa))) as sisa');
        $this->db->join('po_bahan_detail', 'po_bahan.po_bahan_id = po_bahan_detail.po_bahan_id');
        $this->db->where('po_bahan.po_bahan_id ', $po_bahan_id);
        return $this->db->get('po_bahan')->row();
	}

	function delete_penerimaan($penerimaan_bahan_id){
		$this->db->where('penerimaan_bahan_id', $penerimaan_bahan_id);
		return $this->db->delete('penerimaan_bahan');
	}

	function detail_penerimaan($penerimaan_bahan_id){
        $this->db->select('penerimaan_bahan.*, po_bahan_detail.*');
		$this->db->from('penerimaan_bahan');
        $this->db->join('po_bahan_detail', 'penerimaan_bahan.po_bahan_detail_id = po_bahan_detail.po_bahan_detail_id');
		$this->db->where('penerimaan_bahan.penerimaan_bahan_id', $penerimaan_bahan_id);
		return $this->db->get()->row();
	}

	// accounting
    function history_hpp_bahan_all(){
        $this->db->select('bahan.bahan_nama, history_hpp_bahan.*');
        $this->db->join('bahan', 'history_hpp_bahan.bahan_id = bahan.bahan_id');
        $this->db->order_by('history_hpp_bahan.tanggal', 'DESC');
        return $this->db->count_all_results('history_hpp_bahan');
    }

    function history_hpp_bahan_filter($query){
        $this->db->select('bahan.bahan_nama, history_hpp_bahan.*');
        $this->db->join('bahan', 'history_hpp_bahan.bahan_id = bahan.bahan_id');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('history_hpp_bahan.tanggal >=', $this->input->get('start_tanggal'));
            $this->db->where('history_hpp_bahan.tanggal <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('bahan.bahan_nama', $query, 'BOTH');
        $this->db->or_like('history_hpp_bahan.tanggal', $query, 'BOTH');
        $this->db->or_like('history_hpp_bahan.bahan_qty', $query, 'BOTH');
        $this->db->or_like('history_hpp_bahan.hpp', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('history_hpp_bahan.tanggal', 'DESC');

        return $this->db->count_all_results('history_hpp_bahan');
    }
    function history_hpp_bahan_list($start,$length,$query){
        $this->db->select('bahan.bahan_nama, history_hpp_bahan.*');
        $this->db->join('bahan', 'history_hpp_bahan.bahan_id = bahan.bahan_id');
        
        if(isset($_GET['start_tanggal'])&&$this->input->get('start_tanggal')!=""){
            $this->db->where('history_hpp_bahan.tanggal >=', $this->input->get('start_tanggal'));
            $this->db->where('history_hpp_bahan.tanggal <=', $this->input->get('end_tanggal'));
        };

        $this->db->group_start();
        $this->db->like('bahan.bahan_nama', $query, 'BOTH');
        $this->db->or_like('history_hpp_bahan.tanggal', $query, 'BOTH');
        $this->db->or_like('history_hpp_bahan.bahan_qty', $query, 'BOTH');
        $this->db->or_like('history_hpp_bahan.hpp', $query, 'BOTH');
        $this->db->group_end();
        
        $this->db->order_by('history_hpp_bahan.tanggal', 'DESC');
        
        return $this->db->get('history_hpp_bahan', $length, $start)->result();
    }

	
}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */