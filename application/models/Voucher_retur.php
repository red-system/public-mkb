<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher_retur extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "voucher_retur";
    }

    function retur_by_id($retur_agen_id){
        $this->db->where('retur_agen.retur_agen_id', $retur_agen_id);
        $this->db->select('retur_agen.*,b.nama as ditukarkan,voucher_retur.voucher_code');
        $this->db->join('voucher_retur', 'voucher_retur.retur_agen_id = retur_agen.retur_agen_id', 'left');
        $this->db->join('reseller as b','b.reseller_id = voucher_retur.reseller_penukar',"left");
        return $this->db->get('retur_agen')->row();
    }
    function voucher_all(){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_retur.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->join('retur_agen','voucher_retur.retur_agen_id = voucher_retur.voucher_retur_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_retur.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_retur.reseller_penukar',"left");

        return $this->db->count_all_results('voucher_retur');
    }
    function voucher_filter($query){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_retur.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->join('retur_agen','voucher_retur.retur_agen_id = voucher_retur.voucher_retur_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_retur.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_retur.reseller_penukar',"left");
        $this->db->group_start();
        $this->db->or_like('voucher_retur.voucher_code',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('voucher_retur.status_penukaran',$query);
        $this->db->group_end();
        return $this->db->count_all_results('voucher_retur');
    }
    function voucher_list($start,$length,$query){
        $this->load->model("reseller","",true);
        $login = $this->db->where("lokasi_id",$_SESSION['redpos_login']['lokasi_id'])->get("mykindofbeauty_master.login")->row();
        $reseller_id = $login->reseller_id;
        $reseller = $this->reseller->row_by_id($reseller_id);
        if($reseller!=null){
            $this->db->where("voucher_retur.reseller_pemilik",$reseller->reseller_id);
        }
        $this->db->select('voucher_retur.*,owner.*,b.nama as ditukarkan');
        $this->db->join('retur_agen','voucher_retur.retur_agen_id = retur_agen.retur_agen_id');
        $this->db->join('reseller as owner','owner.reseller_id = voucher_retur.reseller_pemilik');
        $this->db->join('reseller as b','b.reseller_id = voucher_retur.reseller_penukar',"left");
        $this->db->group_start();
        $this->db->or_like('voucher_retur.voucher_code',$query);
        $this->db->or_like('b.nama',$query);
        $this->db->or_like('voucher_retur.status_penukaran',$query);
        $this->db->group_end();
        return $this->db->get('voucher_retur',$length,$start)->result();
    }
    function cekVoucher($retur_agen_id,$reseller_id){
        $this->db->where('reseller_pemilik',$reseller_id);
        $this->db->where('retur_agen_id',$retur_agen_id);
        $check = $this->db->get('voucher_retur')->row();
        $result = false;
        if($check!=null){
            $result = true;
        }
        return $result;
    }
    function checkByCode($kode_po){
        $this->db->where('voucher_code',$kode_po);
        $this->db->where('status_penukaran',"Tersedia");
        return $this->db->get('voucher_retur')->row();
    }

}

/* End of file Tipe_pembayaran.php */
/* Location: ./application/models/Tipe_pembayaran.php */