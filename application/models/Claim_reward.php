<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Claim_reward extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "claim_reward";
    }
    function request_list($start,$length,$query)
    {
        $this->db->select('claim_reward_id,reseller.nama,reward.rank_id,claim_reward.tanggal_claim');
        $this->db->join('reward','reward.reward_id = claim_reward.reward_id');
        $this->db->join('reseller','reseller.reseller_id = claim_reward.reseller_id');
        $this->db->group_start();
            $this->db->like('reseller.nama',$query,'both');
            $this->db->or_like('rank_id',$query,'both');
        $this->db->group_end();
        $this->db->where('claim_reward.status','requested');
        return $this->db->get('claim_reward',$length,$start)->result();
    }
    function request_filter($query)
    {
        $this->db->join('reward','reward.reward_id = claim_reward.reward_id');
        $this->db->join('reseller','reseller.reseller_id = claim_reward.reseller_id');
        $this->db->group_start();
        $this->db->like('reseller.nama',$query,'both');
        $this->db->or_like('rank_id',$query,'both');
        $this->db->group_end();
        $this->db->where('claim_reward.status','requested');
        return $this->db->count_all_results('claim_reward');
    }
    function request_all(){
        $this->db->join('reward','reward.reward_id = claim_reward.reward_id');
        $this->db->join('reseller','reseller.reseller_id = claim_reward.reseller_id');
        $this->db->where('claim_reward.status','requested');
        return $this->db->count_all_results('claim_reward');
    }
    function approve_list($start,$length,$query)
    {
        $this->db->select('claim_reward_id,reseller.nama,reward.rank_id,claim_reward.tanggal_claim');
        $this->db->join('reward','reward.reward_id = claim_reward.reward_id');
        $this->db->join('reseller','reseller.reseller_id = claim_reward.reseller_id');
        $this->db->group_start();
        $this->db->like('reseller.nama',$query,'both');
        $this->db->or_like('rank_id',$query,'both');
        $this->db->group_end();
        $this->db->where('claim_reward.status','approve');
        return $this->db->get('claim_reward',$length,$start)->result();
    }
    function approve_filter($query)
    {
        $this->db->join('reward','reward.reward_id = claim_reward.reward_id');
        $this->db->join('reseller','reseller.reseller_id = claim_reward.reseller_id');
        $this->db->group_start();
        $this->db->like('reseller.nama',$query,'both');
        $this->db->or_like('rank_id',$query,'both');
        $this->db->group_end();
        $this->db->where('claim_reward.status','approve');
        return $this->db->count_all_results('claim_reward');
    }
    function approve_all(){
        $this->db->join('reward','reward.reward_id = claim_reward.reward_id');
        $this->db->join('reseller','reseller.reseller_id = claim_reward.reseller_id');
        $this->db->where('claim_reward.status','approve');
        return $this->db->count_all_results('claim_reward');
    }
    function cek_base($reseller_id){
        $this->db->where('reseller_id',$reseller_id);
        $this->db->where('base','1');
        $result = $this->db->get('claim_reward')->row();
        return $result == null ? 0 : $result->base;
    }
    function get_base_date($reseller_id){
        $this->db->where('base','1');
        $this->db->where('reseller_id',$reseller_id);
        return $result = $this->db->get('claim_reward')->row()->tanggal_claim;
    }

    function get_bintang($reseller_id){
        $this->db->select('max(reward_id) as bintang');
        $this->db->where('reseller_id',$reseller_id);
        $result = $this->db->get('claim_reward')->row();
        return $result->bintang == null ? 0: $result->bintang;
    }

}

/* End of file Fabric.php */
/* Location: ./application/models/Fabric.php */