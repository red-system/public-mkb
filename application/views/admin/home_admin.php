<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body ">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-md-12 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="start_date" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="1" value="<?=$min_tanggal?>" data-field="tanggal_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="end_date" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="1" value="<?=$max_tanggal?>" data-field="tanggal_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-info akses-filter_data">
                                <i class="la la-search"></i> Filter Data
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="btn-group btn-group btn-pill btn-group-sm">


            </div>
        </div>
    </div>

    <div class="col-md-12 row">
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Jumlah MKB Terjual - <?=date("d/m/Y",strtotime($min_tanggal))." - ".date("d/m/Y",strtotime($max_tanggal))?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($mkb_terjual->total)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Jumlah LS Terjual - <?=date("d/m/Y",strtotime($min_tanggal))." - ".date("d/m/Y",strtotime($max_tanggal))?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($ls_terjual->total)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Total Po - <?=date("d/m/Y",strtotime($min_tanggal))." - ".date("d/m/Y",strtotime($max_tanggal))?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($total_po->grand_total)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Aktif Reseller
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2> <?=$aktif_agen?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-success">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Aktif Super Reseller
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=$aktif_super_agen?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-success">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Total Bonus - <?=date("d/m/Y",strtotime($min_tanggal))." - ".date("d/m/Y",strtotime($max_tanggal))?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($bonus_lunas)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Omset Reward Super Res -  <?=$startMonthLbl.'('.$startYear.') - '. $curMonthLbl.'('.$curYear.')'?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($total_omset_reward)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Budget Reward Super Res -  <?=$startMonthLbl.'('.$startYear.') - '. $curMonthLbl.'('.$curYear.')'?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($budget_yang_keluar)?></h2>
                </div>
            </div>
        </div>


    </div>
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--tab">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon kt-hidden">
													<i class="la la-gear"></i>
												</span>
                        <h3 class="kt-portlet__head-title">
                            PO Chart
                            <textarea style="display: none;" id="chart-data"><?=json_encode($chartData)?></textarea>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="kt_morris_1" style="height:500px;"></div>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-12">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <?php
                    $sufix = '';
                    if($this->input->get('start_date')!=''){
                        $sufix = '?start_date='.$this->input->get('start_date').'&end_date='.$this->input->get('end_date');
                    } ?>
                    <input type="hidden" id="reward_thailand_url" value="<?=base_url()."reward-thailand".$sufix?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Reward Thailand
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="dropdown dropdown-inline">
                                <div class="btn-group btn-group btn-pill btn-group-sm">
                                    <?php
                                    $uri = $this->uri->segment(1);
                                    $getUrl = "";
                                    foreach (array_keys($this->input->get()) as $key) {
                                        $getUrl .=$key."=".$this->input->get($key)."&";
                                    }
                                    $getUrl = rtrim($getUrl,"& ");
                                    ?>
                                    <a href="<?=base_url()?>reward-thailand/excel" class="btn btn-success akses-excel" id="akses-excel">
                                        <i class="la la-file-excel-o"></i> Print Excel
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="reward-thailand-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Poin Reseller</th>
                            <th>Poin Super</th>
                            <th>Reseller Baru</th>
                            <th>Super Reseller Baru</th>
                            <th>Tambahan Point LS</th>
                            <th>Point Redeposit SR</th>
                            <th>Total Poin</th>
                            <th>Total Join</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">

                    <input type="hidden" id="spesial_thailand_url" value="<?=base_url()."spesial-thailand"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Spesial Tiket
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="dropdown dropdown-inline">
                                <div class="btn-group btn-group btn-pill btn-group-sm">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="spesial-thailand-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Jumlah</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <?php
                    $sufix = '';
                    if($this->input->get('start_date')!=''){
                      $sufix = '?start_date='.$this->input->get('start_date').'&end_date='.$this->input->get('end_date');
                    } ?>
                    <input type="hidden" id="rekap_omset_leader_url" value="<?=base_url()."home/rekap-leader".$sufix?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Rekap Omset Leader
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="rekap-leader-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>

                            <th>Omset Group</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <?php
                    $sufix = '';
                    if($this->input->get('start_date')!=''){
                        $sufix = '?start_date='.$this->input->get('start_date').'&end_date='.$this->input->get('end_date');
                    } ?>
                    <input type="hidden" id="rekap_po_provinsi_url" value="<?=base_url()."home/po_perprovinsi".$sufix?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Rekap PO Per Provinsi
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="rekap_po_provinsi_table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Provinsi</th>
                            <th>Omset</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

