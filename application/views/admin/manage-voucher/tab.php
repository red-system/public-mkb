<div class="kt-portlet__head-toolbar">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='manage-voucher') ? 'active' : '')?>" href="<?=base_url()?>manage-voucher">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Voucher Tersedia</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='manage-voucher-terpakai') ? 'active' : '')?>" href="<?=base_url()?>manage-voucher-terpakai">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Voucher Terpakai</span>
            </a>
        </li>
    </ul>
</div>