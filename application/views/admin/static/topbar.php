				<input type="hidden" id="page" value="<?=$page?>" name="">
				<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-subheader--enabled kt-subheader--transparent kt-grid--hor kt-wrapper"
					id="kt_wrapper">
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
								class="la la-close"></i></button>
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper" style="padding: 15px">
							<h4><?=strtoupper($_SESSION["redpos_login"]["user_role_name"])?></h4>
						</div>
						<div class="kt-header__topbar">
							<?php
                                if(!isset($_SESSION["status_notif"])){
                                    $_SESSION["status_notif"] = true;
                                }
                            ?>
							<?php if ($_SESSION["status_notif"] == true) { ?>
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
									<div class="kt-header__topbar-user">
										<div class="kt-notification__item-icon">
											<i class="flaticon2-bell kt-font-success"></i> <span
												class="badge badge-pill badge-info"><?= $this->count_notif ?></span>
										</div>
									</div>
								</div>
								<div
									class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
									<div class="kt-notification">
										<?php foreach ($this->data_notifs as $notif) {?>
										<a href="<?= $notif->view_url ?>" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-bell kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													<span class="badge badge-pill badge-danger">Baru</span>
													<?= $notif->notif_title_lbl ?>
												</div>
												<div class="kt-notification__item-time">
													<?= $notif->notif_description_lbl  ?>
												</div>
											</div>
										</a>
										<?php } ?>

										<div class="kt-notification__custom">
											<a href="<?=base_url()?>/notification">Lihat Semua Notifikasi</a>
										</div>

									</div>
								</div>
							</div>
							<?php } ?>

							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
									<div class="kt-header__topbar-user">
										<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
										<span
											class="kt-header__topbar-username kt-hidden-mobile account_name"><?=$_SESSION["redpos_login"]['user_name']?></span>
										<img class="kt-image account_avatar" alt="Pic"
											src="<?=$this->config->item('dev_storage_image').$_SESSION["redpos_login"]['avatar']?>" />
									</div>
								</div>
								<div
									class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
									<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
										style="background-image: url(<?=base_url()?>assets/media/misc/bg-1.jpg)">
										<div class="kt-user-card__avatar">
											<img class="account_avatar" alt="Pic"
												src="<?=$this->config->item('dev_storage_image').$_SESSION["redpos_login"]['avatar']?>" />
										</div>
										<div class="kt-user-card__name account_name">
											<?=$_SESSION["redpos_login"]['user_name']?>
										</div>
									</div>
									<div class="kt-notification">
										<a href="<?=base_url()?>profile" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-calendar-3 kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Profile
												</div>
												<div class="kt-notification__item-time">
													Account settings and more
												</div>
											</div>
										</a>

										<div class="kt-notification__custom">
											<a href="#" id="logout" class="btn btn-label-brand btn-sm btn-bold">Log Out</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">