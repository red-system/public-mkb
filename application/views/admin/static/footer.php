					</div>

					<!-- Modal -->
					<div class="modal fade show" id="modalEvent" tabindex="-1" role="dialog" aria-labelledby="modalEventTitle" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="modalEventTitle">Meeting Notification</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								Hai, mohon diisi terlebih dahulu ya.
								<br>
								Klik tombol Gabung Meeting untuk mengisi formulir kehadiran.
								<br>
								<br>

								Terima kasih.
							</div>
							<div class="modal-footer">
								<a class="btn btn-secondary" data-dismiss="modal">Tutup</a>
								<a href="<?= base_url() ?>meeting" class="btn btn-primary">Gabung Meeting</a>
							</div>
							</div>
						</div>
					</div>

					<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
						<div class="kt-footer__copyright">
							2020&nbsp;&copy;&nbsp;<a href="http://mykindofbeauty.co.id" target="_blank" class="kt-link"><?=$_SESSION["redpos_company"]['company_name']?></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<span id="base-value" data-base-url="<?php echo base_url(); ?>"></span>
			<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<script src="<?=base_url()?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/custom/components/vendors/sweetalert2/init.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/vendors/general/cleave/cleave.min.js" type="text/javascript"></script>


		
		<?php if(isset($js)){
			foreach ($js as $key) {
				?>
					<script src="<?=base_url().'assets/'.$key?>" type="text/javascript"></script>				
				<?php
			}
		} ?>
		<?php if(isset($external_js)){
			foreach ($external_js as $key) { 
				?>
					<script src="<?=$key?>" type="text/javascript" async defer></script>				
				<?php
			}
		} ?>
		<script src="<?=base_url()?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>

		<script>
			$(document).ready(function () {
			    var base_url = $("#base_url").val();
                $('.wrapper-loading').fadeOut().addClass('hidden');
				$("#logout").click(function (e) {
                    $.ajax({
                        type: "POST",
                        url: base_url+'logout',
                        cache: false,
                        success: function(response){
                            var data = jQuery.parseJSON(response)
							if(data.success){
							    window.location.href = base_url+'login'
							} else {
                                Swal.fire({
                                    type: 'error',
                                    title: 'Logout Error',
                                    text:'Lakukan closing di POS terlebih dahulu'
                                }).then(function() {
                                    window.location.href = base_url+'pos'
                                });
							}

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                })
                setInterval(function() {
                    $.ajax({
                        url: base_url+"home/cek-order-reseller",
                        success: function (data) {
                            var temp = JSON.parse(data);
                            var text = '<span class="kt-menu__link-badge"><span class="kt-badge kt-badge--rounded kt-badge--danger">';
                            var text2 = '</span></span>';
                            if(temp['db_po']>0){
                                $("._order-super").html(text+temp['db_po']+text2);
                            }else{
                                $("._order-super").html('');
                            }
                            if(temp['reseller']>0){
                                $("._reseller").html(text+temp['reseller']+text2);
                            }else{
                                $("._reseller").html('');
                            }
                            if(temp['super_agen']>0){
                                $("._super-agen").html(text+temp['super_agen']+text2);
                            }else{
                                $("._super-agen").html('');
                            }
                            if(temp['agen']>0){
                                $("._prospective-reseller").html(text+temp['agen']+text2);
                            }else{
                                $("._prospective-reseller").html('');
                            }
                            if(temp['pembayaran_hutang_produk']>0){
                                $("._pembayaran-hutang").html(text+temp['pembayaran_hutang_produk']+text2);
                            } else {
                                $("._pembayaran-hutang").html('');
                            }
                            if(temp['hutang_piutang']>0){
                                $("._hutang_piutang").html(text+temp['hutang_piutang']+text2);
                            } else {
                                $("._hutang_piutang").html('');
                            }
                        }
                    });
                }, 5000);
            })
		</script>			
	</body>
</html>
