<!-- begin:: Content -->
<style type="text/css">
    .select2-container .select2-selection--single{
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        position: fixed;
    }
</style>
<input type="hidden" id="type" value="retur" name="">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Retur</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Quality Control</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=current_url()?>" class="kt-subheader__breadcrumbs-link">Retur</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Retur
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">

            </div>
        </div>
        <form class="form-send" action="<?=base_url()."quality-control/retur"?>" method="post" data-redirect="<?=base_url()."quality-control"?>" data-alert-show="true" data-alert-field-message="Setelah diproses data tidak dapat dikembalikan lagi">
            <div class="kt-portlet__body">
                <div class="col-12 row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-form-label col-3">Produk</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="produk_nama"><?=$produk->produk_nama?></label>
                            <input type="hidden" name="produk_id" value="<?=$produk->produk_id?>">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Lokasi</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="lokasi_nama"><?=$lokasi->lokasi_nama?></label>
                            <input type="hidden" name="lokasi_id" value="<?=$lokasi->lokasi_id?>">
                            <input type="hidden" name="stock_produk_lokasi_id" id="stock_produk_lokasi_id">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Po Produk</label>
                            <div class="input-group col-9">
                                <input type="text" class="form-control" readonly="" id="po_produk_no">
                                <div class="input-group-append"><button class="btn btn-primary po-search" type="button" ><i class="flaticon-search"></i></button></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Suplier</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="suplier"></label>
                            <input type="hidden" name="input_po_id" data-name="po_id">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Jumlah saat pembelian</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="jumlah_item_po"></label>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-form-label col-3">Jumlah stock</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="jumlah_item_po"><?=number_format($stock_produk_lokasi)?></label>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Harga Item</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="hpp_stock">Rp. 0</label>
                            <input type="hidden" id="input_hpp" name="hpp">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Jumlah Item Retur</label>
                            <input type="text" class="form-control input-numeral col-9" id="jumlah_retur" data-max1="<?=$stock_produk_lokasi?>" data-max2="0"  value="0" name="jumlah_retur">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Total Retur</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <label class="col-form-label col-8" id="total_retur">Rp. 0</label>
                            <input type="hidden" name="total_retur" id="input_total_retur">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Keterangan</label>
                            <textarea class="form-control col-9" name="keterangan"></textarea>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Tanggal retur</label>
                            <input class="form-control col-9 tanggal" name="tanggal" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <a href="<?=base_url()."quality-control"?>" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i>Save</button>
            </div>
        </form>
    </div>
</div>
<div class="modal" id="kt_modal_search_po"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>quality-control/po/<?=$produk->produk_id.'/'.$lokasi->lokasi_id?>" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control" id="generalSearch">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="datatable table table-striped- table-hover table-checkable" >
                    <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>PO No</th>
                        <th>Suplier</th>
                        <th>Jumlah Item</th>
                        <th>Harga</th>
                        <th>Tanggal Pesan</th>
                        <th>Tanggal Terima</th>
                        <th width="200">Action</th>
                    </tr>
                    </thead>
                    <tbody id="child_data_ajax"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

