<!-- begin:: Content -->
<style type="text/css">
    .select2-container .select2-selection--single{
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        position: fixed;
    }
</style>
<input type="hidden" id="type" value="rugi" name="">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Rugi</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Quality Control</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=current_url()?>" class="kt-subheader__breadcrumbs-link">Rugi</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Kerugian
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">

            </div>
        </div>
            <form class="form-send" action="<?=base_url()."quality-control/rugi"?>" method="post" data-redirect="<?=base_url()."quality-control"?>" data-alert-show="true" data-alert-field-message="Setelah diproses data tidak dapat dikembalikan lagi">
                <div class="kt-portlet__body">
                    <div class="col-12 row">
                        <div class="col-6">
                            <div class="form-group row">
                                <label class="col-form-label col-3">Produk</label>
                                <label class="col-form-label col-1" style="text-align: center">:</label>
                                <label class="col-form-label col-8" id="produk_nama"><?=$produk->produk_nama?></label>
                                <input type="hidden" name="produk_id" value="<?=$produk->produk_id?>">
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-3">Lokasi</label>
                                <label class="col-form-label col-1" style="text-align: center">:</label>
                                <label class="col-form-label col-8" id="lokasi_nama"><?=$lokasi->lokasi_nama?></label>
                                <input type="hidden" name="lokasi_id" value="<?=$lokasi->lokasi_id?>">
                                <input type="hidden" name="stock_produk_lokasi_id" id="stock_produk_lokasi_id">
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-3">Stok Seri</label>
                                <div class="input-group col-9">
                                <input type="text" class="form-control" readonly="" id="stock_produk_seri">
                                <div class="input-group-append"><button class="btn btn-primary item-search" type="button" ><i class="flaticon-search"></i></button></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-3">Jumlah Stok</label>
                                <label class="col-form-label col-1" style="text-align: center">:</label>
                                <label class="col-form-label col-8" id="jumlah_stock"></label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <label class="col-form-label col-3">Hpp</label>
                                <label class="col-form-label col-1" style="text-align: center">:</label>
                                <label class="col-form-label col-8" id="hpp_stock">Rp. 0</label>
                                <input type="hidden" id="input_hpp" name="hpp">
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-3">Jumlah Kerugian</label>
                                <input type="text" class="form-control input-numeral col-9" id="jumlah_kerugian" data-max="0" value="0" name="jumlah_kerugian">
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-3">Total Kerugian</label>
                                <label class="col-form-label col-1" style="text-align: center">:</label>
                                <label class="col-form-label col-8" id="total_kerugian">Rp. 0</label>
                                <input type="hidden" name="total_kerugian" id="input_total_kerugian">
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-3">Keterangan</label>
                                <textarea class="form-control col-9" name="keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <a href="<?=base_url()."quality-control"?>" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i>Save</button>
                </div>
            </form>
    </div>
</div>
<div class="modal" id="kt_modal_search_stock"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>quality-control/stock/<?=$produk->produk_id.'/'.$lokasi->lokasi_id?>" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control" id="generalSearch">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="datatable table table-striped- table-hover table-checkable" >
                    <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Seri</th>
                        <th>HPP</th>
                        <th>Jumlah</th>
                        <th width="200">Action</th>
                    </tr>
                    </thead>
                    <tbody id="child_data_ajax"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

