<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Piutang</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>piutang" class="kt-subheader__breadcrumbs-link">Hutang/Piutang</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>piutang" class="kt-subheader__breadcrumbs-link">Piutang</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Piutang
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>piutang/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
                <div style="display: none;" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?></div>
            </div>
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?=($this->uri->segment(1)=='piutang' ? 'active' : '')?>" href="<?=base_url()."piutang"?>">
                            Piutang
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=($this->uri->segment(1)=='history-piutang' ? 'active' : '')?>" href="<?=base_url()."history-piutang"?>">
                            History Piutang
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tempo</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select class="form-control searchInput" data-col-index="7" data-field="tipe_pembayaran_id">
                                            <option value="0">All</option>
                                            <option value="1">Akan Jatuh Tempo</option>
                                            <option value="2">Jatuh Tempo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Pelanggan</th>
                    <th>No Faktur</th>
                    <th>Total</th>
                    <th>Terbayar</th>
                    <th>Sisa</th>
                    <th>Status</th>
                    <th>Tenggat Pelunasan</th>
                    <th width="100">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot>
                <tr>
                    <td colspan="3" style="text-align: right;">Total</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Piutang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>piutang/add" method="post" id="kt_add_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label ">Kode <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="piutang_kode" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="piutang_nama" class="form-control" value="" required="">
                            </div>

                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Piutang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Kode</label>
                            <div class="col-9">
                                <label name="piutang_kode" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Nama</label>
                            <div class="col-9">
                                <label name="piutang_nama" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Created At</label>
                            <div class="col-9">
                                <label name="created_at" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Updated At</label>
                            <div class="col-9">
                                <label name="updated_at" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Piutang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>piutang/edit" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label ">Kode <b class="label--required">*</b></label>
                                <input type="hidden" name="piutang_id">
                                <input type="text" placeholder="" name="piutang_kode" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="piutang_nama" class="form-control" value="" required="">
                            </div>

                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
					