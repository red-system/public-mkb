<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Money Back Guarantee</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>money-back" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>money-back" class="kt-subheader__breadcrumbs-link">Money Back Guarantee</a>
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Money Back Guarantee
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="money-back-contain col-12">
                <table class="table table-bordered table-hover table-checkable">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td><h5>Berlaku Setelah</h5></td>
                            <td><h5><?=$reseller->money_back_guarantee?></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Total Deposit</h5></td>
                            <td><h5>Rp. <?=number_format($total_deposit)?></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Total Bonus</h5></td>
                            <td><h5>Rp. <?=number_format($total_bonus)?></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Uang dikembalikan</h5></td>
                            <td><h5>Rp.<?=number_format($total)?></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Potongan</h5></td>
                            <td><h5><?=($potongan_lbl)?>%</h5></td>
                        </tr>
                        <tr>
                            <td><h5>Total Pengembalian Uang</h5></td>
                            <td><h5>Rp.<?=number_format($grand_total)?></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Syarat Terpenuhi</h5></td>
                            <td><h5><?=$syarat?></h5></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <h4>Sisa Produk</h4>
                <table class="table table-bordered table-hover table-checkable">
                    <thead>
                        <tr>
                            <th>Produk</th>
                            <th>Stock</th>
                            <th>Harga</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($produk as $item){
                                ?>
                                <tr>
                                    <td><?=$item->produk_nama?></td>
                                    <td><?=number_format($item->jumlah_lokasi)?></td>
                                    <td><?=number_format($item->hpp_global)?></td>
                                    <td><?=number_format($item->subtotal)?></td>
                                </tr>
                                <?php
                            }
                        ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" style="text-align: center">Total</td>
                            <td><?=number_format($total)?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <di class="kt-portlet__foot">
            <?php if($money==null&&$syarat=="Iya") {?>
            <button class="btn btn-primary" id="ajukan">Ajukan</button>
            <?php } ?>
            <?php if($money!=null) {?>
            <button class="btn btn-danger" id="batalkan">Batalkan</button>
            <button class="btn btn-success" id="print">Print</button>
            <?php } ?>
        </di>
    </div>
</div>
