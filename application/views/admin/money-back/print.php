<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Print Money Back Guarantee</title>



    <!-- Normalize or reset CSS with your favorite library -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">



    <!-- Load paper.css for happy printing -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">



    <!-- Set page size here: A5, A4 or A3 -->

    <!-- Set also "landscape" if you need -->

    <style>

        @page { size: 210mm; margin: 0 0 0 0;} /* output size */

        body.receipt .sheet { width: 210mm;padding: 0px} /* sheet size */

        @media print { body.receipt .sheet { width: 210mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */

        p{font-size: 12px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }
        td{font-size: 12px}

        @media screen {

            body { background: #e0e0e0 }

        }

    </style>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>

</head>



<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


    <h5 style="text-align: center;margin-bottom: 10px;font-size: 12pt"><?=$_SESSION['redpos_company']['company_name']?></h5>
    <div style="width: 100%;border-bottom: 1px solid #000;margin-top: 10px"></div>

    <table class="table table-bordered table-hover table-checkable">
        <thead>
        </thead>
        <tbody>

        <tr>
            <td><span>Nama Agen/Super Agen</span></td>
            <td><span><?=$reseller->nama?></span></td>
        </tr>
        <tr>
            <td><span>Alamat </span></td>
            <td><span><?=$reseller->alamat?></span></td>
        </tr>
        <tr>
            <td><span>NIK </span></td>
            <td><span><?=$reseller->no_ktp?></span></td>
        </tr>
        <tr>
            <td><span>Total Deposit</span></td>
            <td><span>Rp. <?=number_format($total_deposit)?></span></td>
        </tr>
        <tr>
            <td><span>Total Bonus</span></td>
            <td><span>Rp. <?=number_format($total_bonus)?></span></td>
        </tr>
        <tr>
            <td><span>Uang dikembalikan</span></td>
            <td><span>Rp.<?=number_format($total)?></span></td>
        </tr>
        <tr>
            <td><span>Potongan</span></td>
            <td><span><?=($potongan_lbl)?>%</span></td>
        </tr>
        <tr>
            <td><span>Total Pengembalian Uang</span></td>
            <td><span>Rp.<?=number_format($grand_total)?></span></td>
        </tr>
        </tbody>
    </table>
    <h5 style="margin-bottom: 10px">Sisa Produk</h5>
    <table class="table table-bordered table-hover table-checkable">
        <thead>
        <tr>
            <th>Produk</th>
            <th>Stock</th>
            <th>Harga</th>
            <th>Subtotal</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($produk as $item){
            ?>
            <tr>
                <td><?=$item->produk_nama?></td>
                <td><?=number_format($item->jumlah_lokasi)?></td>
                <td><?=number_format($item->hpp_global)?></td>
                <td><?=number_format($item->subtotal)?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3" style="text-align: center">Total</td>
            <td><?=number_format($total)?></td>
        </tr>
        </tfoot>
    </table>
    <br style="margin-top: 10px">
    <span>Sertakan bukti ini saat melakukan pengembalian produk. Uang akan ditransferkan segera setelah verifikasi selesai.</span>

</section>

</body>

</html>
