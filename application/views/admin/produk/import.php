<!-- begin:: Content -->
<style type="text/css">
    .select2-container .select2-selection--single{
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        position: fixed;
    }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Import Produk</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Produk</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Import Produk
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">

            </div>
        </div>
        <form action="<?=base_url()?>produk/import-save" method="post" id="kt_add_staff_form" enctype="multipart/form-data">
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                        </div>
                    </div>
                </div>
                <div class="col-12 row">
                    <div class="col-3"></div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-12">Data</label>
                            <div class="col-md-9 col-sm-12">
                                <input type="file" class="form-control-file" name="data" id="exampleFormControlFile1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 row">
                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i>Save</button>
                </div>
            </div>


        </div>
        </form>
    </div>
</div>

