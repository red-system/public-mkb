						<form action="<?=base_url()?>po-produk/save-edit" method="post" id="kt_add">
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-subheader__main">

									<h3 class="kt-subheader__title">Order Produk</h3>
									<span class="kt-subheader__separator kt-hidden"></span>
									<div class="kt-subheader__breadcrumbs">
										<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="<?=base_url()?>produksi" class="kt-subheader__breadcrumbs-link">Order Produk</a>
										<input type="hidden" id="base_url" name="" value="<?=base_url()?>">
                                        <input type="hidden" id="first_deposit" value="<?=$reseller->first_deposit?>">
                                        <input type="hidden" id="termin" value="<?=$po_produk->termin?>">
                                        <input type="hidden" id="minimal_po" value="<?=$minimal_po?>">
                                        <input type="hidden" id="first_termin" value="<?=$first_termin?>">
                                        <input type="hidden" id="is_frezze" value="<?=$is_frezze?>">
                                        <input type="hidden" id="nominal_frezze" value="<?=$termin?>">
                                        <input type="hidden" id="user_role" value="<?=$_SESSION["redpos_login"]["user_role_id"]?>">
									</div>

								</div>
							</div>
							<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__head kt-portlet__head--lg">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												Tambah Order Produk
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="modal-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Kode Order Produk</label>
														<div class="col-9">
															<input type="hidden" id="input_po_no" name="po_produk_id" value="<?=$po_produk->po_produk_id?>">

															<label name="produksi_kode" id="display_po_no" class="col-form-label"><?=$po_produk->po_produk_no?></label>
														</div>
													</div>

                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-3 col-form-label">Alamat Pengiriman</label>
                                                        <div class="col-9">
                                                            <textarea class="form-control" rows="3" name="alamat_pengiriman"><?=$po_produk->alamat_pengiriman?></textarea>
                                                        </div>
                                                    </div>
												</div>
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
														<div class="col-9">
															<textarea class="form-control" rows="3" name="keterangan"><?=$po_produk->keterangan?></textarea>
														</div>
													</div>															
												</div>
												<div class="col-md-8"><h5><strong>List Produk</strong></h5></div>
												<div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
											</div>
											<div class="row" style="margin-top: 20px">
												<div class="col-md-12" id="item-container">
													<?php
													$no = 0;
													foreach ($po_produk_detail as $key) {
														?>
														<div class="row" id="produk_item_<?=$no?>">
															<div class="col-md-3">
																<div class="form-group row">
																	<label for="example-text-input" class="col-3 col-form-label">Produk
																		<b class="label--required">*</b>
																	</label>
																	<div class="col-9">
																		<div class="input-group col-12">
																			<input type="text" class="form-control readonly" name="item_produk_nama_<?=$no?>" id="item_produk_nama<?=$no?>" required="" autocomplete="off" value="<?=$key->produk_nama?>">
																			<input type="hidden" class="form-control" id="item_produk_id_<?=$no?>" name="item_produk[produk_<?=$no?>][produk_id]" value="<?=$key->produk_id?>">
                                                                            <input type="hidden" class="form-control" id="input_last_hpp_<?=$no?>" name="item_produk[produk_<?=$no?>][last_hpp]" value="<?=$key->last_hpp?>">
                                                                            <input type="hidden" class="form-control" id="input_produk_harga_form_<?=$no?>" name="item_produk[produk_<?=$no?>][harga]" value="<?=$key->harga?>">
																			<div class="input-group-append">
																				<button class="btn btn-primary produk-search" type="button" data-toggle="modal" data-no="<?=$no?>">
																					<i class="flaticon-search"></i>
																				</button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-2">
																<div class="form-group row">
																	<label for="example-text-input" class="col-6 col-form-label">Harga

																	</label>
                                                                    <label for="example-text-input" class="col-6 col-form-label" data-no="<?=$no?>"  id="item_produk_harga_<?=$no?>"><?=number_format($key->harga)?></label>
																</div>
															</div>
															<div class="col-md-2">
																<div class="form-group row">
																	<label for="example-text-input" class="col-6 col-form-label">Jumlah
																		<b class="label--required">*</b>
																	</label>
																	<div class="col-6">
																		<input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off" data-no="<?=$no?>" value="<?=$key->jumlah_qty?>" id="item_produk_jumlah_<?=$no?>" name="item_produk[produk_<?=$no?>][jumlah]">
																	</div>
																</div>
															</div>
															<div class="col-md-2">
																<div class="form-group row">
																	<label for="example-text-input" class="col-6 col-form-label">Subtotal</label>
																	<div class="col-6">
																		<label class="col-form-label" id="item_produk_subtotal_display<?=$no?>"><?=number_format($key->sub_total)?></label>
																		<input type="hidden" class="form-control input-numeral subtotal" autocomplete="off" data-no="<?=$no?>" value="<?=$key->sub_total?>" id="item_produk_subtotal_<?=$no?>" name="item_produk[produk_<?=$no?>][subtotal]" value="<?=$key->sub_total?>">
																	</div>
																</div>
															</div>
															<div class="col-md-1">
																<button type="button" class="btn btn-danger btn-sm delete-produk" data-no="<?=$no?>" data-item-produk="produk_item_<?=$no?>">
																	<i class="flaticon2-trash"></i>
																</button>
															</div>
														</div>															
														<?php
														$no++;
													}
													?>
													<input type="hidden" id="index_no" value="<?=$no?>" name="">
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__body">
										<div class="row">
											<div class="col-md-6">
                                                <strong style="display: none"><span class="total" id="total_item">0</span></strong>
                                                <input type="hidden" name="total_item" id="input_total_item" class="" value="<?=$po_produk->total?>">
                                                <input type="hidden" name="tambahan" id="tambahan" class="" value="0">
                                                <input type="hidden" name="potongan" id="potongan" class="" value="0">
                                                <div class="form-group kt-form__group row">
                                                    <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                                    <div class="col-8 col-form-label">
                                                        <input type="hidden" id="input_grand_total" name="grand_total" value="<?=$po_produk->grand_total?>">
                                                        <strong><span class="" id="grand_total"><?=number_format($po_produk->grand_total)?></span></strong>
                                                    </div>
                                                </div>
											</div>
                                            <?php if($reseller->first_deposit=="0") { ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Termin</label>
                                                        <div class="kt-radio-inline">
                                                            <label class="kt-radio">
                                                                <input type="radio" id="terminTidak" name="termin" value="0" <?=($po_produk->termin==0?"checked":"")?>> Tidak
                                                                <span></span>
                                                            </label>
                                                            <label class="kt-radio">
                                                                <input type="radio" id="terminYa" name="termin" value="1" <?=($po_produk->termin==1?"checked":"")?>> Iya
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
										</div>

									</div>
								</div>									
							</div>
							<div class="modal" id="kt_modal_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Data Produk</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<input type="hidden" id="list_produk" value="<?=base_url()?>po-produk/utility/list-produk">
												<div class="col-md-12">
													<table class="table table-striped- table-hover table-checkable" id="produk-table">
														<thead>
															<tr>
																<th>Kode Produk</th>
																<th>Nama Produk</th>
																<th width="60">Aksi</th>
															</tr>
														</thead>
														<tbody id="produk_child"></tbody>
													</table>												
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>		
							<div class="pos-floating-button">
								<a href="<?=base_url()?>po-produk" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
									<span>
										<i class="la la-angle-double-left"></i>
										<span>Kembali ke Daftar</span>
									</span>
								</a>
								<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
							</div>				

						</form>