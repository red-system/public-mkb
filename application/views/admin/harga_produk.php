<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title"><?=$produk->produk_nama?></h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Produk</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url().'harga-produk/'.$this->uri->segment(2)?>" class="kt-subheader__breadcrumbs-link">Harga</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Produk
				</h3>
				<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
				<input type="hidden" id="list_url" value="<?=base_url()?>harga-produk/list/<?=$id?>" name="">
				<div style="display: none;" id="table_column"><?=$column?></div>
				<?php if(isset($columnDef)) {  ?>
				<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
				<?php } ?>
				<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
				<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<a href="<?=base_url()?>produk" class="btn btn-brand btn-icon-sm btn-warning"><i class="la la-angle-double-left"></i> Kembali</a>
						<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
							<i class="flaticon2-plus"></i> Tambah Data
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>									
			<table class="datatable table table-striped- table-hover table-checkable" >
				<thead>
					<tr>
						<th width="30">No</th>
						<th>Minimal Pembeilan</th>
                        <th>Satuan</th>
                        <th>Harga</th>
						<th width="150">Action</th>
					</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>

<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?=base_url().'harga-produk/add/'.$id?>" method="post" id="kt_add_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Kode Produk</label>
								<div class="col-9">
									<input type="hidden" name="produk_id" value="<?=$produk->produk_id?>">
									<label name="suplier_kode" class="col-form-label"><?=$produk->produk_kode?></label>

								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Nama Produk</label>
								<div class="col-9">
									<label name="suplier_kode" class="col-form-label"><?=$produk->produk_nama?></label>
								</div>
							</div>
                            <div class="col-12 row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-control-label ">Minimal Pembelian <b class="label--required">*</b></label>
                                        <div class="row">
                                            <input type="text" placeholder="" name="minimal_pembelian" class="input-numeral form-control col-md-8" value="" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-control-label ">Satuan <b class="label--required">*</b></label>
                                        <div class="col-9">
                                            <select class="form-control" name="satuan_id">
                                                <option value="<?=$produk->produk_satuan_id?>"><?=$nama_satuan?></option>
                                                <?php
                                                foreach ($unit_satuan as $item){
                                                    ?>
                                                    <option value="<?=$item->satuan_id?>"><?=$item->satuan_nama?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


							<div class="form-group">
								<label class="form-control-label ">Harga<b class="label--required">*</b></label>
								<input type="text" placeholder="" name="harga" class="input-numeral form-control" value="" required="">
							</div>
						</div>



					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?=base_url().'harga-produk/edit/'.$id?>" method="post" id="kt_edit_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Kode Produk</label>
								<div class="col-9">
									<label name="suplier_kode" class="col-form-label"><?=$produk->produk_kode?></label>
                                    <input type="hidden" name="harga_produk_id">
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Nama Produk</label>
								<div class="col-9">
									<label name="suplier_kode" class="col-form-label"><?=$produk->produk_nama?></label>
								</div>
							</div>
                            <div class="col-12 row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-control-label ">Minimal Pembelian <b class="label--required">*</b></label>
                                        <div class="row">
                                            <input type="text" placeholder="" name="minimal_pembelian" class="input-numeral form-control col-md-8" value="" required="">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-control-label ">Satuan <b class="label--required">*</b></label>
                                        <div class="col-9">
                                            <select class="form-control" name="satuan_id">
                                                <option value="<?=$produk->produk_satuan_id?>"><?=$nama_satuan?></option>
                                                <?php
                                                foreach ($unit_satuan as $item){
                                                    ?>
                                                    <option value="<?=$item->satuan_id?>"><?=$item->satuan_nama?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="form-group">
								<label class="form-control-label ">Harga<b class="label--required">*</b></label>
								<input type="text" placeholder="" name="harga" class="input-numeral form-control" value="" required="">
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
				</div>
			</form>
		</div>
	</div>
</div>