
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">Marketing Tools</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>website" class="kt-subheader__breadcrumbs-link">Website</a>
                <input type="hidden" id="base_url" name="" value="<?=base_url()?>">

            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Website
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="modal-body">
                    <?php if($reseller->first_deposit_date!=null){ ?>
                    <div>
                        <h4>Web Referensi</h4>
                        <p>Web referensi adalah web yang dapat digunakan untuk mengajak orang lain bergabung dengan MKB. Setiap reseller/super reseller mendapatkan website referensi yang berbeda.
                            link dari website referensi anda adalah : <i><?=$web_referensi?></i> &nbsp;&nbsp;<button class="button btn btn-success btn-sm" id="click_referensi" data-clipboard-text="<?=$web_referensi?>"><i class="fa fa-copy"></i></button>
                        </p>
                    </div>
                    <div style="margin-top: 10px">
                        <h4>Web Retail</h4>
                        <p>Web retail adalah web yang dapat digunakan untuk promosi penjualan secara retail. Setiap reseller/super reseller mendapatkan website retail yang berbeda. Website ini akan langsung terkoneksi dengan no WhatsApp anda, jadi pastikan anda telah mencantumkan no yang terkoneksi WhatsApp
                            link dari website retail anda adalah : <i><?=$web_referensi?></i> &nbsp;&nbsp;<button class="button btn btn-primary btn-sm" id="click_retail" data-clipboard-text="<?=$web_retail?>"><i class="fa fa-copy"></i></button>
                        </p>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>


