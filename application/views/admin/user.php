<!-- begin:: Content --><style type="text/css">
							.select2-container .select2-selection--single{
								height: auto;
							}
							.select2-container--default .select2-selection--single .select2-selection__arrow {
								    position: fixed;
								}
						</style>
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">User</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>user" class="kt-subheader__breadcrumbs-link">Master Data</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>user" class="kt-subheader__breadcrumbs-link">User</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Data User
										</h3>
										<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
										<input type="hidden" id="list_url" value="<?=base_url()?>user/list" name="">
										<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
										<div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
									</div>
									<?php $akses = json_decode($action,true);if($akses["add"]){ ?>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
													<i class="flaticon2-plus"></i> Tambah Data
												</button>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Search Form -->
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-8 order-2 order-xl-1">
												<div class="row align-items-center">
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<table class="datatable table table-striped- table-hover table-checkable" >
										<thead>
											<tr>
												<th width="30">No</th>
												<th>Nama Staf</th>
												<th>Email</th>
                                                <th>Username</th>
												<th>No telepon</th>
												<th>Role</th>
												<th width="200">Action</th>
											</tr>
										</thead>
										<tbody id="child_data_ajax"></tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="modal" id="kt_modal_add"  role="dialog" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Tambah User</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<form action="<?=base_url()?>user/add" method="post" id="kt_add_staff_form" enctype="multipart/form-data">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
				                                            	<label class="form-control-label required">Staff <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12 kt_select_2" name="user_staff_id" required="">
				                                            		<option value="">Pilih Staff</option>
				                                            		<?php
				                                            			foreach ($staff as $key) {
				                                            			?>
				                                            			<option value="<?=$key->staff_id?>"><?=$key->staff_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-control-label required">Username <b class="label--required">*</b></label>
                                                            <input type="text" class="form-control" name="username">
                                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label required">Role <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12 user-role" name="user_role_id" required="">
				                                            		<option value="">Pilih Role</option>
				                                            		<?php
				                                            			foreach ($user_role as $key) {
				                                            			?>
				                                            			<option value="<?=$key->user_role_id?>"><?=$key->user_role_name?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>

				                                        <div class="form-group">
                                                                 <div class="form-group">
                                                                    <label>Avatar <b class="label--required">*</b></label>
                                                                 <div></div>
                                                                      <div class="custom-file">
                                                                            <input type="file" class="custom-file-input img-input" data-display="displayImg" name="avatar" accept="image/x-png,image/gif,image/jpeg" id="customFile" required="">
                                                                           <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                                                       </div>
                                                                 </div>
                                                             	<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg" width="200" height="150">
                                                                </div>
                                                                    
                                                        </div>
                                                        <div class="form-group">
                                                                    <label class="control-label">Password <b class="label--required">*</b></label>
                                                                    <input type="password" name="password" id="password" class="form-control" required=""> 
                                                        </div>
                                                        <div class="form-group">
                                                                    <label class="control-label">Konfirmasi password <b class="label--required">*</b></label>
                                                                    <input type="password" name="re_password" id="re_password" class="form-control" required=""> 
                                                        </div>
													</div>
													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_edit"  role="dialog" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Edit User</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<form action="<?=base_url()?>user/edit" method="post" id="kt_edit_form" enctype="multipart/form-data">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
																<input type="hidden" name="id">
				                                            	<label class="form-control-label required">Staff <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12" id="kt_select2_2" name="user_staff_id" required="">
				                                            		<option value="">Pilih Staff</option>
				                                            		<?php
				                                            			foreach ($staff as $key) {
				                                            			?>
				                                            			<option value="<?=$key->staff_id?>"><?=$key->staff_nama?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-control-label required">Username <b class="label--required">*</b></label>
                                                            <input type="text" class="form-control" name="username">
                                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label required">Role <b class="label--required">*</b></label>
				                                            	<select class="form-control col-md-12 user-role" name="user_role_id" required="">
				                                            		<option value="">Pilih Role</option>
				                                            		<?php
				                                            			foreach ($user_role as $key) {
				                                            			?>
				                                            			<option value="<?=$key->user_role_id?>"><?=$key->user_role_name?></option>
				                                            			<?php
				                                            			}
				                                            		?>
				                                            	</select>
				                                        </div>
				                                        <div class="form-group">
                                                                 <div class="form-group">
                                                                    <label>Avatar</label>
                                                                 <div></div>
                                                                      <div class="custom-file">
                                                                            <input type="file" class="custom-file-input img-input" data-display="displayImg2" name="avatar" accept="image/x-png,image/gif,image/jpeg" id="customFile2" >
                                                                           <label class="custom-file-label selected" for="customFile2" id="labelCustomFile"></label>
                                                                       </div>
                                                                 </div>
                                                             	<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="<?=base_url()?>assets/media/no_image.png" alt="" class="img-preview" id="displayImg2" width="200" height="150">
                                                                </div>
                                                                    
                                                        </div>
                                                        <div class="form-group">
                                                                    <label class="control-label">Password</label>
                                                                    <input type="password" name="password" id="edit_password" class="form-control"> 
                                                        </div>
                                                        <div class="form-group">
                                                                    <label class="control-label">Konfirmasi password </label>
                                                                    <input type="password" name="re_password" id="re_password" class="form-control"> 
                                                        </div>
													</div>
													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_detail"  role="dialog" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Detail User</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Nama User</label>
															<div class="col-9">
																<label name="staff_nama" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Email</label>
															<div class="col-9">
																<label name="staff_email" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">No Telepon</label>
															<div class="col-9">
																<label name="staff_phone_number" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Role</label>
															<div class="col-9">
																<label name="user_role_name" class="col-form-label"></label>
															</div>
														</div>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-3 col-form-label">Username</label>
                                                            <div class="col-9">
                                                                <label name="username" class="col-form-label"></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-3 col-form-label">Password</label>
                                                            <div class="col-9">
                                                                <label  class="col-form-label">****************</label>
                                                            </div>
                                                        </div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Avatar</label>
															<div class="col-9">
																<img src="<?=base_url()?>assets/media/no_image.png" class="img-preview" data-field="avatar" alt="" id="displayImg" width="200" height="150">
															</div>
														</div>
                                                        <div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Dibuat Pada</label>
															<div class="col-9">
																<label name="created_at" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Diubah Pada</label>
															<div class="col-9">
																<label name="updated_at" class="col-form-label"></label>
															</div>
														</div>
													</div>
													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
									</div>
								</div>
							</div>
					