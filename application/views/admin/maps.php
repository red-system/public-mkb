<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_profile">
    <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
        <div class="kt-portlet__body">
            <table>
                <tr>
                    <th width="200">Alamat</th>
                    <th width="10">:</th>
                    <th><span id="location"></span></th>
                </tr>
                <tr>
                    <th>Latitude</th>
                    <th>:</th>
                    <th><span id="lat"><?php echo $old_lat ?></span></th>
                    <input type="hidden" id="old_lat" name="old_lat" value="<?php echo $old_lat ?>">
                </tr>
                <tr>
                    <th>Longitude</th>
                    <th>:</th>
                    <th><span id="lon"><?php echo $old_lon ?></span></th>
                    <input type="hidden" id="old_lon" name="old_lon" value="<?php echo $old_lon ?>">
                </tr>
            </table>
            <span class="my-3 h4">Klik peta untuk mendapatkan lokasi outlet yang lebih akurat.</span>
            <div class="row mb-3">
                <div class="col-md-6">
                    <input id="pac-input" type="text" class="form-control"
                        placeholder="Input alamat outlet anda disini ..." value="<?php echo $alamat_outlet ?>">
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary" id="search">Search</button>
                    <button type="submit" class="btn btn-danger" id="clear">Clear</button>
                </div>
            </div>
            <div id="map" style="width:100%;height:100%;"></div>

            <div class="row mt-3 ">
                <div class="col-md-12 d-flex justify-content-end">
                    <form action="<?=base_url()?>maps/update" method="post" id="kt_add_maps">
                    <input type="hidden" id="lat_outlet" name="lat_outlet">
                    <input type="hidden" id="lon_outlet" name="lon_outlet">
                    <input type="hidden" id="reseller_id" name="reseller_id" value="<?php echo $reseller_id ?>">

                    <button type="submit" class="btn btn-success" id="kt_add_submit_maps">Simpan Koordinat Outlet</button>
                    <a href="<?=base_url()?>" class="btn btn-danger">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>