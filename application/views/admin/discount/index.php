<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Discount</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>discount" class="kt-subheader__breadcrumbs-link">Master Data</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>discount" class="kt-subheader__breadcrumbs-link">Discount</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Discount
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>discount/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>
            <?php $akses = json_decode($action,true);if($akses["add"]){ ?>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
                                <i class="flaticon2-plus"></i> Tambah Data
                            </button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Judul</th>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                    <th>Jangka Waktu</th>
                    <th>Tipe</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                    <th width="200">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal discount-modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Discount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>discount/add" method="post" id="kt_add_staff_form">
                <div class="modal-body">
                    <div class="col-12 row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Judul <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="discount_nama" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jangka Waktu <b class="label--required">*</b></label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="jangka_waktu" data-parent="add" value="Ya"> Ya
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="jangka_waktu" data-parent="add" value="Tidak" checked> Tidak
                                        <span></span>
                                    </label>
                                </div>
                                <input type="hidden" id="produk_id_add" name="produk_id_list" value="">
                            </div>
                            <div id="add_jangka_waktu">

                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Tipe Discount <b class="label--required">*</b></label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="tipe_discount" data-parent="add" value="Nominal" checked> Nominal
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="tipe_discount" data-parent="add" value="Percent" > Percent
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div id="add_tipe_discount">
                                <div class="form-group">
                                    <label class="form-control-label ">Nilai Diskon <b class="label--required">*</b></label>
                                    <input type="text" class="form-control" autocomplete="off" name="value_nominal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Keterangan</label>
                                <textarea class="form-control" name="keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Jumlah Item (item yang sama)<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="minimal_qty_item" class="form-control input-numeral" value="" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Jumlah Belanja <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="minimal_total_belanja" class="form-control input-numeral" value="" required="">
                            </div>
                        </div>
                    </div>
                    <h5>Item</h5>
                    <input type="hidden" id="list_produk_add" value="<?=base_url()?>discount/produk/add">
                    <div class="col-md-12">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1 searchForm">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-form__group kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label>Search:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control" id="generalSearchProduk">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped- table-hover table-checkable" id="produk-table-add">
                            <thead>
                            <tr>
                                <th width="30"><label class="kt-checkbox">
                                        <input type="checkbox" id="all-produk" value="all" > All
                                        <span></span>
                                    </label></th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Jenis Produk</th>
                                <th>Satuan</th>
                                <th width="60">Aksi</th>
                            </tr>
                            </thead>
                            <tbody id="produk_child"></tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal discount-modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Discount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="col-12 row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="form-control-label col-3">Judul </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="discount_nama"> </label>

                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-3">Jangka Waktu </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="jangka_waktu"> </label>
                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-3">Mulai Discount </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="mulai_discount"> </label>
                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-3">Akhir Discount </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="mulai_discount"> </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="form-control-label col-3">Tipe Discount </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="tipe_discount"> </label>
                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-3">Nilai Discount </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="nilai"> </label>
                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-3">Keterangan </label>
                            <label class="form-control-label col-1">: </label>
                            <label class="form-control-label col-8" name="keterangan"> </label>
                        </div>
                    </div>
                </div>

                <h5>Item</h5>
                <input type="hidden" id="list_produk_detail" value="<?=base_url()?>discount/produk/detail">
                <div class="col-md-12">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1 searchForm">
                            <div class="row align-items-center">
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="kt-form__group kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label>Search:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" id="generalSearchProdukDetail">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped- table-hover table-checkable" id="produk-table-detail">
                        <thead>
                        <tr>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Jenis Produk</th>
                            <th>Satuan</th>

                        </tr>
                        </thead>
                        <tbody id="produk_child_detail"></tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal discount-modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Discount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>discount/edit" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="col-12 row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Judul <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="discount_nama" class="form-control" value="" required="">
                                <input type="hidden" placeholder="" name="discount_id" class="form-control" value="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jangka Waktu <b class="label--required">*</b></label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="jangka_waktu" data-parent="edit" value="Ya"> Ya
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="jangka_waktu" data-parent="edit" value="Tidak" checked> Tidak
                                        <span></span>
                                    </label>
                                </div>
                                <input type="hidden" id="produk_id_add" name="produk_id_list" value="">
                            </div>
                            <div id="edit_jangka_waktu">

                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Tipe Discount <b class="label--required">*</b></label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="tipe_discount" data-parent="edit" value="Nominal" checked> Nominal
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="tipe_discount" data-parent="edit" value="Percent" > Percent
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div id="edit_tipe_discount">
                                <div class="form-group">
                                    <label class="form-control-label ">Nilai Diskon <b class="label--required">*</b></label>
                                    <input type="text" class="form-control" autocomplete="off" name="value_nominal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Keterangan</label>
                                <textarea class="form-control" name="keterangan"></textarea>
                                <textarea class="form-control" id="rangkuman" style="display: none" name="rangkuman"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Jumlah Item (item yang sama)<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="minimal_qty_item" class="form-control input-numeral" value="" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Jumlah Belanja <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="minimal_total_belanja" class="form-control input-numeral" value="" required="">
                            </div>
                        </div>
                    </div>
                    <h5>Item</h5>
                    <input type="hidden" id="list_produk_edit" value="<?=base_url()?>discount/produk/add">
                    <div class="col-md-12">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1 searchForm">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-form__group kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label>Search:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control" id="generalSearchProdukEdit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped- table-hover table-checkable" id="produk-table-edit">
                            <thead>
                            <tr>
                                <th width="30"><label class="kt-checkbox">
                                        <input type="checkbox" id="all-produk-edit" value="all" > All
                                        <span></span>
                                    </label></th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Jenis Produk</th>
                                <th>Satuan</th>
                                <th width="60">Aksi</th>
                            </tr>
                            </thead>
                            <tbody id="produk_child_edit"></tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
					