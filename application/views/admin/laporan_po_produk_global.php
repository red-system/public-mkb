<!-- begin:: Content -->
						<input type="hidden" id="current_page" value="<?=base_url()."laporan/laporan-po-produk/"?>" name="">
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Laporan Order Produk</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>laporan/laporan-po-produk" class="kt-subheader__breadcrumbs-link">Laporan</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>laporan/laporan-po-produk" class="kt-subheader__breadcrumbs-link">Laporan Order Produk</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(2)=='laporan-po-produk') ? 'active' : '')?>" href="<?=base_url()?>laporan/laporan-po-produk">
															<i class="fa fa-globe"></i> <span class="kt--visible-desktop-inline-block">Global</span>
														</a>
													</li>
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(1)=='laporan-po-produk-detail') ? 'active' : '')?>" href="<?=base_url()?>laporan/laporan-po-produk-detail">
															<i class="fa fa-grip-horizontal"></i> <span class="kt--visible-desktop-inline-block">Detail</span>
														</a>
													</li>
												</ul>
										</div>
									</div>
									<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
									<input type="hidden" id="list_url" value="<?=base_url()."laporan/laporan-po-produk".(($this->uri->segment(2) == 'laporan-po-produk') ? '/list' : '-detail/list' )?>" name="">
									<div style="display: none;" id="table_column"><?=$column?></div>
									<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
									<?php if(isset($columnDef)) {  ?>
									<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
									<?php } ?>
									<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												<div class="btn-group btn-group btn-pill btn-group-sm">
													<?php
													$uri = $this->uri->segment(1);
													$getUrl = "";
													foreach (array_keys($this->input->get()) as $key) {
														$getUrl .=$key."=".$this->input->get($key)."&";
													}
													$getUrl = rtrim($getUrl,"& ");
													?>
													<a href="<?=base_url()?>laporan/laporan-po-produk/pdf" class="btn btn-danger akses-pdf" id="akses-pdf">
														<i class="la la-file-pdf-o"></i> Print PDF
													</a>
													<a href="<?=base_url()?>laporan/laporan-po-produk/excel" class="btn btn-success akses-excel" id="akses-excel">
														<i class="la la-file-excel-o"></i> Print Excel
													</a>
												</div>

											</div>
										</div>
									</div>
																					
								</div>
								<div class="kt-portlet__body">
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-md-12 order-2 order-xl-1 searchForm">
												<div class="row align-items-center">
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
                                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                        <div class="kt-form__group kt-form__group--inline">
                                                            <div class="kt-form__label">
                                                                <label>Tanggal Pemesanan</label>
                                                            </div>
                                                            <div class="kt-form__control">
                                                                <div class="input-daterange input-group" id="kt_datepicker">
                                                                    <input type="text" class="form-control kt-input searchInput" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="2" value="" data-field="start_pemesanan"/>
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                                                    </div>
                                                                    <input type="text" class="form-control kt-input searchInput" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="2" value="" data-field="end_pemesanan"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                        <div class="kt-form__group kt-form__group--inline">
                                                            <div class="kt-form__label">
                                                                <label>Tanggal Penerimaan</label>
                                                            </div>
                                                            <div class="kt-form__control">
                                                                <div class="input-daterange input-group" id="kt_datepicker">
                                                                    <input type="text" class="form-control kt-input searchInput" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="8" value="" data-field="start_penerimaan"/>
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                                                    </div>
                                                                    <input type="text" class="form-control kt-input searchInput" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="8" value="" data-field="end_penerimaan"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
												</div>
											</div>

											<div class="col-md-12 order-2 order-xl-1 searchForm" style="margin-top: 20px">
												<div class="row align-items-center">

												</div>
											</div>
										</div>
									</div>
											<table class="datatable table table-striped- table-hover table-checkable" >
												<thead>
													<tr>
														<th width="30">No</th>
														<th>Order Produk No</th>
														<th>Tanggal Pemesanan</th>
														<th>Suplier</th>
														<th>Grand Total</th>
														<th>Jenis Pembayaran</th>
														<th>Metode Pembayaran</th>
														<th>Status Pembayaran</th>
														<th>Tanggal Penerimaan</th>
														<th>Status Pemesanan</th>
													</tr>
												</thead>
												<tbody id="child_data_ajax"></tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="4"><strong>Total</strong></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tfoot>
													</table>

									<!--end: Datatable -->
								</div>
						</div>							
						</div>
												
						<div class="modal" id="kt_modal_confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi Laporan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<form action="<?=base_url().'konfirmasi-transfer-produk/confirm'?>" method="post" id="kt_add_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
													<div class="form-group">
														<label>Status</label>
														<input type="hidden" name="history_transfer_produk_id">
														<div class="kt-radio-inline">
															<label class="kt-radio">
																<input type="radio" name="status_history" checked="" value="Diterima"> Diterima
																<span></span>
															</label>
															<label class="kt-radio">
																<input type="radio" name="status_history" value="Ditolak"> Ditolak
																<span></span>
															</label>
														</div>
													</div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
				                                            	<textarea class="form-control" id="keterangan" rows="3" name="keterangan" required=""></textarea>
				                                        </div>
			                                        </div>
											</div>
										</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
											</div>
										</form>
									</div>
								</div>
							</div>