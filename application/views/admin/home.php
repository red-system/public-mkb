<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
<input type="hidden" id="pernah_isi_survey" value="<?=$pernah_isi_survey?>">
<?php //if ($status_latlon == true) { ?>
<!--    <div class="row">-->
<!--        <div class="col-md-12">-->
<!--            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">-->
<!--                <div class="kt-portlet__body my-5" style="text-align: center;">-->
<!--                    <h1 style="text-align: center">Saat ini tim IT sedang mengembangkan sistem untuk menampilkan-->
<!--                        koordinat dari outlet Reseller/Super Reseller. Silahkan update Koordinat Lokasi Outlet anda.-->
<!--                    </h1>-->
<!--                    <div class="row mt-5">-->
<!--                        <div class="col-md-12 d-flex justify-content-center">-->
<!--                            <a href="--><?php //echo base_url() ?><!--maps" class="btn btn-success btn-pill">-->
<!--                                Update Koordinat Lokasi Outlet-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    --><?php //} ?>


    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body ">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-md-12 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput tanggal"
                                                id="start_date" name="start_date" placeholder="Dari" autocomplete="off"
                                                data-col-index="1" value="<?=$min_tanggal?>"
                                                data-field="tanggal_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput tanggal"
                                                id="end_date" name="end_date" placeholder="Sampai" autocomplete="off"
                                                data-col-index="1" value="<?=$max_tanggal?>" data-field="tanggal_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-info akses-filter_data">
                                <i class="la la-search"></i> Filter Data
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="btn-group btn-group btn-pill btn-group-sm">


            </div>
        </div>
    </div>

    <div class="col-md-12 row">
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Bintang Terkumpul
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=$jumlah_bintang_reseller?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-success">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Omset <?=date("Y-m-01")?> - <?=date("Y-m-d")?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($omset_reseller)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Peringkat Reward Tahunan
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=$reseller_annual_rank?></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 row">
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="reseller_leader_board_url" value="<?=base_url()."home/reseller-leader-board"?>" name="">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-notes"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Annual Reward Leader Board
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293"
                           id="reseller_leader_board_table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Omset</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="most_url" value="<?=base_url()."home/bonus"?>" name="">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-notes"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Bonus
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293"
                        id="most-table">
                        <thead>
                            <tr>
                                <th width="30">No</th>
                                <th>Agen/Super Agen</th>
                                <th>Type Bonus</th>
                                <th>Total</th>
                                <th>Persentase</th>
                                <th>Total Bonus</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="least_url" value="<?=base_url()."home/group"?>" name="">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-notes"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Group
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293"
                        id="least-table">
                        <thead>
                            <tr>
                                <th width="30">No</th>
                                <th>Nama</th>
                                <th>Tipe</th>
                                <th>Status</th>
                                <th>Area</th>
                            </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal" id="surveyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="form-survey" action="<?=base_url()?>simpan-survey" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Survey Kepuasan Terhadap Leader (<?=$leader_name?>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">

                    <p>
                        Kak, terima kasih banyak atas kerjasama dan dukungan Kakak sebagai reseller yang setia selama ini. Kami sangat menghargai kontribusi Kakak dalam mempromosikan produk kami dan membantu kami mencapai tujuan bersama.
                        <br>
                        <br>
                        Kami ingin memperbaiki layanan kami untuk Kakak sebagai reseller dan untuk itu kami ingin meminta pendapat Kakak melalui survei kepuasan terhadap leader Kakak di tim kami. Kami akan sangat berterima kasih jika Kakak dapat meluangkan waktu sejenak untuk mengisi survei tersebut.
                        <br>
                        <br>
                        Kami meyakini bahwa pendapat Kakak sangat berharga bagi kami dalam meningkatkan kualitas layanan kami, sehingga kami dapat terus memberikan dukungan terbaik bagi Kakak sebagai reseller.
                        <br>
                        <br>
                        Terima kasih banyak atas waktu dan perhatian Kakak dalam menjawab survei kepuasan ini. Semoga kerjasama kita dapat terus berjalan dengan baik dan sukses selalu.
                    </p>
                    <?php
                        $i = 1;
                        foreach ($pertanyaan as $item){
                            ?>
                            <div class="form-group">
                                <label for="question1"><?=$item->pertanyaan?></label>
                                <div class="kt-radio-list">
                                    <label class="kt-radio">
                                        <input class="form-check-input" type="radio" name="question[<?=$i?>]" id="question<?=$i?>_5" value="5">Sering
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input class="form-check-input" type="radio" name="question[<?=$i?>]" id="question<?=$i?>_3" value="3"> Cukup
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input class="form-check-input" type="radio" name="question[<?=$i?>]" id="question<?=$i?>_2" value="2"> Kadang Kadang tapi Tanpa Diminta
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input class="form-check-input" type="radio" name="question[<?=$i?>]" id="question<?=$i?>_1" value="1"> Hanya Jika Diminta
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input checked class="form-check-input" type="radio" name="question[<?=$i?>]" id="question<?=$i?>_0" value="0"> Tidak Pernah
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                    <?php
                            $i++;
                        }
                    ?>
                    <div class="form-group">
                        <label for="question1">Apakah anda ingin berpindah ke Super Reseller yang lain untuk menjadi Upline anda ? </label>
                        <div class="kt-radio-list">
                            <label class="kt-radio">
                                <input class="form-check-input" type="radio" name="question[11]" id="question11_5" value="5">Tidak
                                <span></span>
                            </label>
                            <label class="kt-radio">
                                <input checked class="form-check-input" type="radio" name="question[11]" id="question11_0" value="0"> Iya
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div id="ganti-leader">
                        <div class="form-group">
                            <label for="question1">Alasan untuk pindah team leader </label>
                            <textarea name="alasan" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="question1">Leader Pengganti </label>
                            <select class="form-control" name="leader">
                                <option>Pilih Leader</option>
                                <?php
                                foreach ($leader_lain as $item){
                                    ?>
                                    <option value="<?=$item->reseller_id?>"><?=$item->nama?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                <div class="form-group">
                    <label for="question1">Kritik/Saran untuk MKB</label>
                    <textarea name="saran" class="form-control"></textarea>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
    <div class="d-none" id="content-ganti-leader">
        <div class="form-group">
            <label for="question1">Alasan untuk pindah team leader </label>
            <textarea name="alasan" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="question1">Leader Pengganti </label>
            <select class="form-control" name="leader">
                <option>Pilih Leader</option>
                <?php
                foreach ($leader_lain as $item){
                    ?>
                    <option value="<?=$item->reseller_id?>"><?=$item->nama?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
</div>