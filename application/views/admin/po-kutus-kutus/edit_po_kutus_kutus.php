<form action="<?=base_url()?>order-produsen/save-edit" method="post" id="kt_add">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">Order MKB</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>order-produsen" class="kt-subheader__breadcrumbs-link">Order MKB</a>
                <input type="hidden" id="base_url" name="" value="<?=base_url()?>">
            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Tambah Order Produk
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Kode Order Produk</label>
                                <div class="col-9">
                                    <input type="hidden" id="input_po_no" name="po_kutus_kutus_id" value="<?=$po_kutus_kutus->po_kutus_kutus_id?>">
                                    <input type="hidden" id="input_po_no" name="po_kutus_kutus_no" value="<?=$po_kutus_kutus->po_kutus_kutus_no?>">
                                    <label name="produksi_kode" id="display_po_no" class="col-form-label"><?=$po_kutus_kutus->po_kutus_kutus_no?></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="form-control-label col-3">Suplier <b class="label--required">*</b></label>
                                <div class="typeahead col-9">
                                    <label class="form-control col-12">UD. Ametha Sedana Sakti</label>
                                    <input type="hidden" name="suplier_nama" value="UD. Ametha Sedana Sakti">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Pemesanan</label>
                                <div class="col-9">
                                    <input type="text" class="form-control tanggal" name="tanggal_pemesanan" required="" autocomplete="off" value="<?=date("Y-m-d",strtotime($po_kutus_kutus->tanggal_pemesanan))?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                <div class="col-9">
                                    <textarea class="form-control" rows="3" name="keterangan"><?=$po_kutus_kutus->keterangan?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8"><h5><strong>List Produk</strong></h5></div>
                        <div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
                    </div>
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12" id="item-container">
                            <?php
                            $no = 0;
                            foreach ($po_kutus_kutus_detail as $key) {
                                ?>
                                <div class="row" id="produk_item_<?=$no?>">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-3 col-form-label">Produk
                                                <b class="label--required">*</b>
                                            </label>
                                            <div class="col-9">
                                                <div class="input-group col-12">
                                                    <input type="text" class="form-control readonly" name="item_produk_nama<?=$no?>" id="item_produk_nama<?=$no?>" required="" autocomplete="off" value="<?=$key->produk_nama?>">
                                                    <input type="hidden" class="form-control" id="item_produk_id<?=$no?>" name="item_produk[produk<?=$no?>][produk_id]" value="<?=$key->produk_id?>">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary produk-search" type="button" data-toggle="modal" data-no="<?=$no?>">
                                                            <i class="flaticon-search"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <label class="col-6 col-form-label" >HPP Terakhir</label>
                                            <label class="col-6 col-form-label" id="last_hpp_<?=$no?>"><?='Rp. '.number_format($key->last_hpp)?></label>
                                            <input type="hidden" id="input_last_hpp_<?=$no?>" name="item_produk[produk<?=$no?>][last_hpp]" value="<?=$key->last_hpp?>">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-6 col-form-label">Harga
                                                <b class="label--required">*</b>
                                            </label>
                                            <div class="col-6">
                                                <input type="text" class="form-control input-numeral harga-produk" autocomplete="off" data-no="<?=$no?>" value="<?=$key->harga?>" id="item_produk_harga<?=$no?>" name="item_produk[produk<?=$no?>][harga]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-6 col-form-label">Jumlah
                                                <b class="label--required">*</b>
                                            </label>
                                            <div class="col-6">
                                                <input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off" data-no="<?=$no?>" value="<?=$key->jumlah?>" id="item_produk_jumlah<?=$no?>" name="item_produk[produk<?=$no?>][jumlah]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-6 col-form-label">Subtotal</label>
                                            <div class="col-6">
                                                <label class="col-form-label" id="item_produk_subtotal_display<?=$no?>"><?=number_format($key->sub_total)?></label>
                                                <input type="hidden" class="form-control input-numeral subtotal" autocomplete="off" data-no="<?=$no?>" value="<?=$key->sub_total?>" id="item_produk_subtotal<?=$no?>" name="item_produk[produk<?=$no?>][subtotal]" value="<?=$key->sub_total?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-danger btn-sm delete-produk" data-no="<?=$no?>" data-item-produk="produk_item<?=$no?>">
                                            <i class="flaticon2-trash"></i>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                $no++;
                            }
                            ?>
                            <input type="hidden" id="index_no" value="<?=$no?>" name="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group kt-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">Total</label>
                            <div class="col-8 col-form-label">
                                <input type="hidden" id="input_total_item" name="total_item" value="<?=$po_kutus_kutus->total?>">
                                <strong><span class="total" id="total_item"><?=number_format($po_kutus_kutus->total)?></span></strong>
                            </div>
                        </div>
                        <div class="form-group kt-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">Biaya
                                Tambahan</label>
                            <div class="col-8">
                                <input type="text" name="tambahan" id="tambahan" class="input-numeral form-control" value="<?=number_format($po_kutus_kutus->tambahan)?>">
                            </div>
                        </div>
                        <div class="form-group kt-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                            <div class="col-8">
                                <input type="text" name="potongan" id="potongan" class="input-numeral form-control" value="<?=number_format($po_kutus_kutus->potongan)?>">
                            </div>
                        </div>
                        <div class="form-group kt-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                            <div class="col-8 col-form-label">
                                <input type="hidden" id="input_grand_total" name="grand_total" value="<?=$po_kutus_kutus->grand_total?>">
                                <strong><span class="" id="grand_total"><?=number_format($po_kutus_kutus->grand_total)?></span></strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group kt-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">Jenis pembayaran <b class="label--required">*</b></label>
                            <div class="kt-radio-inline col-8 col-form-label">
                                <label class="kt-radio">
                                    <input type="radio" name="jenis_pembayaran" id="" value="kas" <?=(($po_kutus_kutus->jenis_pembayaran=="kas")?"checked":"")?>> Kas
                                    <span></span>
                                </label>
                                <label class="kt-radio">
                                    <input type="radio" name="jenis_pembayaran" id="" value="kredit" <?=(($po_kutus_kutus->jenis_pembayaran=="kredit")?"checked":"")?>> Kredit
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div id="kas_container" style="display:<?=(($po_kutus_kutus->jenis_pembayaran=="kas")?"block":"none")?>">
                            <div class="form-group kt-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Tipe Pembayaran<b class="label--required">*</b></label>
                                <div class="col-8">
                                    <select class="form-control col-md-12 tipe-kas" name="tipe_pembayaran_id" id="tipe_pembayaran_id" required="">
                                        <?php
                                        foreach ($tipe_pembayaran as $key) {
                                            ?>
                                            <option value="<?=$key->tipe_pembayaran_id?>" <?=(($key->tipe_pembayaran_id == $po_kutus_kutus->tipe_pembayaran)? "selected" : "")?>><?=$key->tipe_pembayaran_nama." ".$key->no_akun?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group kt-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Rekening/Check/Giro</label>
                                <div class="col-8">
                                    <input type="text" name="tipe_pembayaran_no" id="tipe_pembayaran_no" placeholder="" class="form-control tipe-kas" <?=(($po_kutus_kutus->jenis_pembayaran=="kas")?"":"dissabled")?> value="<?=$po_kutus_kutus->tipe_pembayaran_no?>">
                                </div>
                            </div>
                            <div class="form-group kt-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Keterangan</label>
                                <div class="col-8">
                                    <textarea class="form-control tipe-kas" name="tipe_pembayaran_keterangan" <?=(($po_kutus_kutus->jenis_pembayaran=="kas")?"":"dissabled")?> ><?=$po_kutus_kutus->tipe_pembayaran_keterangan?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="kredit_container" style="display:<?=(($po_kutus_kutus->jenis_pembayaran=="kredit")?"block":"none")?>">
                            <div class="form-group kt-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Tenggat Pelunasan<b class="label--required">*</b></label>
                                <div class="col-8">
                                    <input type="text" name="tenggat_pelunasan" id="tenggat_pelunasan" class="form-control tipe-kredit tanggal" autocomplete="off" required="" <?=(($po_kutus_kutus->jenis_pembayaran=="kredit")?"":"dissabled")?> value="<?=$po_kutus_kutus->tenggat_pelunasan?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="kt_modal_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="list_produk" value="<?=base_url()?>po-kutus-kutus/utility/list-produk">
                        <div class="col-md-12">
                            <table class="table table-striped- table-hover table-checkable" id="produk-table">
                                <thead>
                                <tr>
                                    <th>Kode Produk</th>
                                    <th>Nama Produk</th>
                                    <th>Stok</th>
                                    <th width="60">Aksi</th>
                                </tr>
                                </thead>
                                <tbody id="produk_child"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pos-floating-button">
        <a href="<?=base_url()?>po-kutus-kutus" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
									<span>
										<i class="la la-angle-double-left"></i>
										<span>Kembali ke Daftar</span>
									</span>
        </a>
        <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
    </div>

</form>