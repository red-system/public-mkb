<html><head>
    <meta http-equiv="\&quot;Content-Type\&quot;" content="\&quot;text/html;" charset="utf-8\&quot;">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: \'Open Sans\', sans-serif;
        }

        .content {
            max-width: 550px;
            margin: auto;
        }
        .title{
            width: 60%;
        }
        .data,.data th,.data td {
            border: 1px solid black;
        }
        a{
            color: #990000;
        }
        a:hover{
            color: #990000;
        }
        .btn {
            display: inline-block;
            font-weight: normal;
            color: #212529;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.65rem 1rem;
            font-size: 1rem;
            cursor: pointer;
            line-height: 1.5;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
        .btn {
            background: transparent;
            outline: none !important;
            vertical-align: middle;
        }
        .btn-success {
            color: #fff;
            background-color: #0abb87;
            border-color: #0abb87;
        }
        .btn.btn-pill {
            border-radius: 2rem;
            padding-right: 40px;
            padding-left: 40px;
            font-size: 12px;
        }
    </style>
</head>
<body style="background-color: #fff">
<div class="content" style="background-color: #fff">
    <div>
        <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">

            <tbody>
            <tr>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img src="http://app.support88.id/assets/media/logo_login.png" style="width: 35%;margin-left: auto;margin-right: auto;height: auto">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <div style="width: 450px; height: 480px;margin-top: 20px; padding: 20px; background-color: #f1f1f1; border-radius: 20px; margin-left: auto;margin-right: auto;text-align: left">
                        <h4 style="text-align: left;font-weight: 300;">Halo '.$reseller->nama.',</h4>
                        <h3 style="text-align: left;font-weight: 600">Yay, Selamat!</h3>
                        <span>Anda mendapatkan BONUS REFERAL dari pendaftaran Agen/Super Agen Support 88 a/n Bondan Noviada
                        </span>
                        <br>
                        <div style="width: 100%;text-align: center;margin-top: 30px;margin-bottom: 30px">
                            <table>
                                <tbody>
                                    <tr style="height: 25px">
                                        <td>Jumlah Deposit </td>
                                        <td>:</td>
                                        <td>Rp 800,000,000</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td>Persentase </td>
                                        <td>:</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td>Jumlah Bonus </td>
                                        <td>:</td>
                                        <td>Rp 40,000,000</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span>
                            Bonus akan di transfer ke rekening dalam estimasi 1 x 24 jam (Hari kerja)
                        </span>
                        <br>
                        <br>
                        <span style="margin-top: 20px">
                            Jika Anda tidak mengenali aktivitas ini di akun email Anda,
                            mohon menghubungi <span style="color: #900000">info@support88.id</span>
                        </span>

                        <div style="margin-top: 40px"></div>
                        <span >
                            Terima Kasih,
                        </span>
                        <div style="margin-top: 25px"></div>
                        <span >
                            Tim Support 88
                        </span>
                    </div>
                    <div style="margin-top: 20px;text-align: center">
                        <span style="font-size: 10px">Support 88. Jalan Ratna No.68H Tonja, Denpasar Utara, Bali</span>
                    </div>
                    <div style="margin-top: 10px;text-align: center">
                        <span style="font-size: 10px"><a style="margin-right: 10px;color:#900000" href="https://www.instagram.com/support.88/">Instagram</a><a style="margin-right: 10px;color:#900000" href="https://www.facebook.com/Support88-112005620684890">Facebook</a><a style="color: #900000" href="http://support88.id/">Website</a></span>
                    </div>

                </td>

            </tr>
            </tbody></table>
        <p>&nbsp;<br></p>
    </div>

</div>

</body></html>