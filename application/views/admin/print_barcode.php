<html>
<head>
	<meta charset="utf-8" />
	<title>Print Barcode</title>
	<meta name="description" content="Child datatable from remote data">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">



    <!-- Load paper.css for happy printing -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	<script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
	</script>
    <style>

        @page { size: <?=$paper->paper_width?>mm; margin: 0 0 0 0;} /* output size */

        body.receipt .sheet { width: <?=$paper->paper_width?>mm;padding: 0px} /* sheet size */

        @media print { body.receipt .sheet { width: <?=$paper->paper_width?>mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */

        p{font-size: 10px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }



        @media screen {

            body { background: #e0e0e0 }

        }

    </style>
	<link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

</head>
<body class="receipt" onload="window.print()">
<section class="sheet" style="padding: <?=$paper->row_space?>mm 0mm 0mm <?=$paper->column_space?>mm">
	<div class="row" style="margin-left: <?=$paper->column_space?>mm;margin-right: 0mm;">

	<?php
		$jumlah_cetak = $this->uri->segment(4);
		for ($i = 0; $i<$jumlah_cetak;$i++){
			?>
			<div style="padding: 0.75mm 0mm 0.75mm 0mm;background-color: #FFF;width: <?=$paper->column_width?>mm;height: <?=$paper->column_height?>mm;margin-bottom: <?=$paper->row_space?>mm;margin-right: <?=$paper->column_space?>mm">
                <div style="">
                <span style="font-size: <?=$paper->font_size?>mm;overflow-wrap: break-word;"><b><?=substr($produk->produk_nama,0,$paper->character_length)?></b></span>
                </div>
				<img src="<?=base_url().'stock-produk/barcode/'.$this->uri->segment(3)?>" style="width: 100%;height: <?=($paper->column_height-(2*$paper->font_size)-2)?>mm">
                <span style="font-size: <?=$paper->font_size?>mm;margin-right: 10px;float: right"><b><?=$produk->harga_eceran?></b></span>
			</div>
			<?php
		}
	?>
	</div>
</section>
</body>
</html>
