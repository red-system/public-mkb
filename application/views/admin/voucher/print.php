<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Print PO</title>



    <!-- Normalize or reset CSS with your favorite library -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">



    <!-- Load paper.css for happy printing -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">



    <!-- Set page size here: A5, A4 or A3 -->

    <!-- Set also "landscape" if you need -->

    <style>

        @page { size: 210mm; margin: 0 0 0 0;} /* output size */

        body.receipt .sheet { width: 210mm;padding: 0px} /* sheet size */

        @media print { body.receipt .sheet { width: 210mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */

        p{font-size: 12px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }
        td{font-size: 12px}

        @media screen {

            body { background: #e0e0e0 }

        }

    </style>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>

</head>



<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


    <h5 style="text-align: center;margin-bottom: 10px;font-size: 12pt">Voucher Produk Support 88</h5>
    <div style="width: 100%;border-bottom: 1px solid #000;margin-top: 10px"></div>

    <table style="width: 100%;margin-top: 20px">
        <tbody>
        <tr>

            <td width="10%">Voucher No</td>
            <td width="40%">: <?=$detail->voucher_code?></td>
            <td width="10%">Status Pembayaran</td>
            <td width="40%">: <?=$detail->status_pembayaran?></td>
        </tr>
        <tr>
            <td width="10%">Order No</td>
            <td width="40%">: <?=$detail->po_produk_no?></td>
            <td width="10%">Status</td>
            <td width="40%">: <?=$detail->status_penerimaan?></td>
        </tr>
        <tr>
            <td width="10%">Tanggal Pemesanan</td>
            <td width="40%">: <?=$detail->tanggal_pemesanan?></td>
            <td width="10%">Tanggal Penerimaan</td>
            <td width="40%">: <?=$detail->tanggal_penerimaan?></td>
        </tr>
        <tr>
            <td width="10%"> Ditukarkan di</td>
            <td width="40%">: <?=$detail->ditukarkan?></td>
            <td width="10%"> Grand Total</td>
            <td width="40%">: <?=$detail->grand_total?></td>
        </tr>
        </tbody>



    </table>
    <table style="width: 100%">
        <tbody>
            <tr>
                <td width="20%"></td>
                <td width="60%" style="text-align: center"><div style="margin-top: 20px;width: 100%">
                        <img src="<?=base_url()."stock-produk/barcode/".$detail->voucher_code?>" width="80%" style="margin-left: auto;margin-right: auto">
                    </div></td>
                <td width="20%"></td>
            </tr>
        </tbody>
    </table>


    <h5 style="margin-bottom: 10px">Daftar Item</h5>
    <table class="table table-bordered table-hover table-checkable" >
        <thead>
        <tr>
            <th>Nama Produk</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Subtotal</th>
        </tr>
        </thead>
        <tbody id="view_child_data">

        <?php

        foreach ($detail->item as $key){
            ?>
            <tr>
                <td><?=$key->produk_nama?></td>
                <td style="text-align: right">Rp. <?=$key->harga?></td>
                <td style="text-align: right"><?=$key->jumlah?></td>
                <td style="text-align: right">Rp. <?=$key->sub_total?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</section>

</body>

</html>
