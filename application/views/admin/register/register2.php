<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Register Reseller MKB </title>
    <meta name="description" content="Login page">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?= base_url() ?>assets/app/custom/login/login-v4.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>assets/app/custom/wizard/wizard-v1.default.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/balioz.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/style/src/sweetalert2.css" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url()?>assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url() ?>assets/style/src/custom.css" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="<?= base_url() ?>assets/media/logos/mkb.ico"/>
</head>
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <input type="hidden" id="base_url" value="<?= base_url() ?>">

    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin bg-putih" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor custom-bg-register"
             style="background-image: url(<?= base_url() ?>assets/media/bg/bg-baru.jpg);">
            <div class="col-md-8" style=" margin: 0 auto;">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper margin-minus">
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                        <div class="wrapper-loading hidden">
                            <div class='loader'>
                                <div class='loader--dot'></div>
                                <div class='loader--dot'></div>
                                <div class='loader--dot'></div>
                                <div class='loader--dot'></div>
                                <div class='loader--dot'></div>
                                <div class='loader--dot'></div>
                                <div class='loader--text'></div>
                                <div class='loader--desc'></div>
                            </div>
                        </div>
                        <div class="header-register row">
                            <div class="logo-register">
                                <img src="<?= base_url() ?>assets/media/logo_mkb_health_and_beauty.png">
                            </div>
                        </div>

                        <!--                        //here-->
                        <form action="<?= base_url() ?>registrasi-save" class="kt-form"  method="post" id="kt_add_staff_form"
                              enctype="multipart/form-data">
                            <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body kt-portlet__body--fit">
                                        <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_wizard_v1" data-ktwizard-state="step-first">
                                            <div class="kt-grid__item">

                                                <!--begin: Form Wizard Nav -->
                                                <div class="kt-wizard-v1__nav">
                                                    <div class="kt-wizard-v1__nav-items">
                                                        <a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
                                                            <div class="kt-wizard-v1__nav-body">
                                                                <div class="kt-wizard-v1__nav-icon">
                                                                    <img class="icon-tab-register" src="<?=base_url()?>assets/media/data_diri.png">
                                                                </div>
                                                                <div class="kt-wizard-v1__nav-label">
                                                                    Data Diri
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step">
                                                            <div class="kt-wizard-v1__nav-body">
                                                                <div class="kt-wizard-v1__nav-icon">
                                                                    <img class="icon-tab-register" src="<?=base_url()?>assets/media/lokasi.png">
                                                                </div>
                                                                <div class="kt-wizard-v1__nav-label">
                                                                    Alamat
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step">
                                                            <div class="kt-wizard-v1__nav-body">
                                                                <div class="kt-wizard-v1__nav-icon">
                                                                    <img class="icon-tab-register" src="<?=base_url()?>assets/media/photo.png">
                                                                </div>
                                                                <div class="kt-wizard-v1__nav-label">
                                                                    Foto
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step">
                                                            <div class="kt-wizard-v1__nav-body">
                                                                <div class="kt-wizard-v1__nav-icon">
                                                                    <img class="icon-tab-register" src="<?=base_url()?>assets/media/konfirmasi.png">
                                                                </div>
                                                                <div class="kt-wizard-v1__nav-label">
                                                                    Konfirmasi
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>

                                                <!--end: Form Wizard Nav -->
                                            </div>
                                            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper" style="display: block">

                                                <!--begin: Form Wizard Form-->
                                                <!--begin: Form Wizard Step 1-->
                                                <div class="kt-wizard-v1__content" data-num="1" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                    <div class="kt-heading kt-heading--md">Informasi Diri</div>
                                                    <div class="kt-form__section kt-form__section--first">
                                                        <div class="kt-wizard-v1__form" >
                                                            <div class="form-group">
                                                                <?php
                                                                if ($referal_code != "") {
                                                                    ?>
                                                                    <input type="hidden" name="referal_code"
                                                                           value="<?= $referal_code ?>">
                                                                    <?php
                                                                }
                                                                ?>
                                                                <input type="hidden" name="type" value="<?= $type ?>">
                                                                <label class="form-control-label ">Nama <b class="label--required">*</b></label>
                                                                <input type="text" placeholder="" autocomplete="off" name="nama" class="form-control"
                                                                       value="" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-control-label ">Jenis Kelamin <b
                                                                            class="label--required">*</b></label>
                                                                <div class="col-md-12">
                                                                    <div class="kt-radio-inline">
                                                                        <label class="kt-radio">
                                                                            <input type="radio" name="jenis_kelamin" value="perempuan" checked> Perempuan
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="kt-radio">
                                                                            <input type="radio" name="jenis_kelamin" value="laki-laki"> Laki-laki
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-control-label ">No Hp <b
                                                                            class="label--required">*</b></label>
                                                                <input type="tel" placeholder="" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" autocomplete="off" name="phone" class="form-control"
                                                                       value="" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-control-label ">E-mail <b
                                                                            class="label--required">*</b></label>
                                                                <input type="email" placeholder="" autocompl ete="off" name="email" class="form-control"
                                                                       value="" required="">
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="form-control-label ">No KTP <b
                                                                            class="label--required">*</b></label>
                                                                <input type="text" placeholder="" autocomplete="off" name="no_ktp" class="form-control"
                                                                       value="" required="">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!--                                                end: Form Wizard Step 1-->
                                                <!---->
                                                <!--                                                begin: Form Wizard Step 2-->
                                                <div class="kt-wizard-v1__content" data-num="2" data-ktwizard-type="step-content">
                                                    <div class="kt-form__section kt-form__section--first">
                                                        <div class="kt-wizard-v1__form">
                                                            <div class="form-group">
                                                                <label class="form-control-label "><b>Alamat Tinggal</b></label>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Propinsi <b
                                                                                class="label--required">*</b></label>
                                                                    <select class="form-control col-md-12 province" id="province_id"
                                                                            prefix="" name="province_id" required="">
                                                                        <option value="">Pilih Propinsi</option>
                                                                        <?php
                                                                        foreach ($province as $item) {
                                                                            ?>
                                                                            <option value="<?= $item->province_id ?>"><?= $item->province ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Kabupaten/Kota <b
                                                                                class="label--required">*</b></label>
                                                                    <select class="form-control col-md-12 city" id="city_id" prefix=""
                                                                            name="city_id" required="">
                                                                        <option value="">Pilih Kabupaten/Kota</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Kecamatan <b
                                                                                class="label--required">*</b></label>
                                                                    <select class="form-control col-md-12 subdistrict"
                                                                            id="subdistrict_id" prefix="" name="subdistrict_id"
                                                                            required="">
                                                                        <option value="">Pilih Kecamatan</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Alamat Lengkap <b
                                                                                class="label--required">*</b></label>
                                                                    <textarea class="form-control" name="alamat" required></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="form-control-label "><b> Alamat Outlet</b></label>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Propinsi <b
                                                                                class="label--required">*</b></label>
                                                                    <select class="form-control col-md-12 province"
                                                                            id="outlet_province_id" prefix="outlet_"
                                                                            name="outlet_province_id" required="">
                                                                        <option value="">Pilih Propinsi</option>
                                                                        <?php
                                                                        foreach ($province as $item) {
                                                                            ?>
                                                                            <option value="<?= $item->province_id ?>"><?= $item->province ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Kabupaten/Kota <b
                                                                                class="label--required">*</b></label>
                                                                    <select class="form-control col-md-12 city" id="outlet_city_id"
                                                                            prefix="outlet_" name="outlet_city_id" required="">
                                                                        <option value="">Pilih Kabupaten/Kota</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Kecamatan <b
                                                                                class="label--required">*</b></label>
                                                                    <select class="form-control col-md-12 subdistrict"
                                                                            id="outlet_subdistrict_id" prefix="outlet_"
                                                                            name="outlet_subdistrict_id" required="">
                                                                        <option value="">Pilih Kecamatan</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="form-control-label ">Alamat Lengkap <b
                                                                                class="label--required">*</b></label>
                                                                    <textarea class="form-control" name="outlet_alamat" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--end: Form Wizard Step 2-->

                                                <!--begin: Form Wizard Step 4-->
                                                <div class="kt-wizard-v1__content" data-num="3" data-ktwizard-type="step-content">
                                                    <div class="kt-form__section kt-form__section--first">
                                                        <div class="kt-wizard-v1__form">
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label>Foto Diri <b class="label--required">*</b></label>
                                                                    <div></div>
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input img-input"
                                                                               data-display="displayImg1custom" name="foto_profile"
                                                                               accept="image/x-png,image/gif,image/jpeg"
                                                                               id="customFile1" required="">
                                                                        <label class="custom-file-label selected" for="customFile"
                                                                               id="labelCustomFile"></label>
                                                                        <input type="hidden" name="input_foto_profile">
                                                                    </div>
                                                                </div>
                                                                <div class="fileinput-new thumbnail"
                                                                     style="width: 200px; height: 150px;">
                                                                    <img src="<?= base_url() ?>assets/media/no_image.png" alt=""
                                                                         id="displayImg1custom" width="200" height="150">
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label>Foto KTP <b class="label--required">*</b></label>
                                                                    <div></div>
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input img-input"
                                                                               data-display="displayImg2custom" name="foto_ktp"
                                                                               accept="image/x-png,image/gif,image/jpeg"
                                                                               id="customFile2" required="">
                                                                        <label class="custom-file-label selected" for="customFile"
                                                                               id="labelCustomFile"></label>
                                                                        <input type="hidden" name="input_foto_ktp">
                                                                    </div>
                                                                </div>
                                                                <div class="fileinput-new thumbnail"
                                                                     style="width: 200px; height: 150px;">
                                                                    <img src="<?= base_url() ?>assets/media/no_image.png" alt=""
                                                                         id="displayImg2custom" width="200" height="150">
                                                                </div>

                                                            </div>

<!--                                                            <div class="form-group">-->
<!--                                                                <div class="form-group">-->
<!--                                                                    <label>Swa-Foto dengan KTP <b class="label--required">*</b></label>-->
<!--                                                                    <div></div>-->
<!--                                                                    <div class="custom-file">-->
<!--                                                                        <input type="file" class="custom-file-input img-input"-->
<!--                                                                               data-display="displayImg3custom" name="selfi_ktp"-->
<!--                                                                               accept="image/x-png,image/gif,image/jpeg"-->
<!--                                                                               id="customFile3" required="">-->
<!--                                                                        <label class="custom-file-label selected" for="customFile"-->
<!--                                                                               id="labelCustomFile"></label>-->
<!--                                                                        <input type="hidden" name="input_selfi_ktp">-->
<!--                                                                    </div>-->
<!--                                                                </div>-->
<!--                                                                <div class="fileinput-new thumbnail"-->
<!--                                                                     style="width: 200px; height: 150px;">-->
<!--                                                                    <img src="--><?//= base_url() ?><!--assets/media/no_image.png" alt=""-->
<!--                                                                         id="displayImg3custom" width="200" height="150">-->
<!--                                                                </div>-->
<!---->
<!--                                                            </div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-wizard-v1__content" data-num="4" data-ktwizard-type="step-content">
                                                    <div class="kt-form__section kt-form__section--first">
                                                        <div class="kt-wizard-v1__form">
                                                            <div class="confirmation-panel">
                                                                <h5 class="" style="text-align: justify;line-height: 30px">Dengan ini saya menyatakan bahwa data yang saya berikan adalah benar adanya. Jika nantinya ditemukan  data yang tidak benar atau fiktif, maka team My Kind Of Beauty berhak melakukan take-down atau menutup akun saya.</h5>
                                                                <div class="checkbox" style="margin-top: 20px">
                                                                    <div class="checkbox" style="margin-top: 20px">
                                                                        <h5><input type="checkbox" id="checkbox" value=""> Setuju</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-form__actions">
                                                    <div class="btn btn-secondary  kt-font-transform-u" data-ktwizard-type="action-prev">
                                                        Prev
                                                    </div>
                                                    <div class="btn btn-success kt-font-transform-u" data-ktwizard-type="action-submit">
                                                        Submit
                                                    </div>
                                                    <div class="btn btn-brand kt-font-transform-u" data-ktwizard-type="action-next">
                                                        Next
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<script src="<?= base_url() ?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js"
        type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
        type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/src/sweetalert2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/jquery-validation/dist/jquery.validate.js"
        type="text/javascript"></script>
<script src="<?= base_url() ?>assets/vendors/general/jquery-form/dist/jquery.form.min.js"
        type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<!--<script src="--><?//=base_url()?><!--assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>-->
<script src="<?=base_url()?>assets/vendors/custom/components/vendors/jquery-validation/init.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/script/register.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>


</body>
</html>
