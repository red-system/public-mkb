<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Register Reseller MKB </title>
    <meta name="description" content="Login page">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?=base_url()?>assets/app/custom/login/login-v4.default.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

    <link href="<?=base_url()?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/balioz.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/style/src/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?=base_url()?>assets/media/logos/pop icon.ico" />
</head>
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <input type="hidden" id="base_url" value="<?=base_url()?>">

    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(<?=base_url()?>assets/media/bg/bg-2.jpg);">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                    <div class="wrapper-loading hidden">
                        <div class='loader'>
                            <div class='loader--dot'></div>
                            <div class='loader--dot'></div>
                            <div class='loader--dot'></div>
                            <div class='loader--dot'></div>
                            <div class='loader--dot'></div>
                            <div class='loader--dot'></div>
                            <div class='loader--text'></div>
                            <div class='loader--desc'></div>
                        </div>
                    </div>
                    <div class="header-register row">
                        <div class="logo-register">
                            <img src="<?=base_url()?>assets/media/logo2.png" >
                        </div>

                    </div>
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Formulir Registrasi
                                </h3>
                            </div>

                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <form action="<?=base_url()?>registrasi-save" method="post" id="kt_add_staff_form" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="register-body">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php
                                                    if($referal_code!=""){
                                                        ?>
                                                        <input type="hidden" name="referal_code" value="<?=$referal_code?>">
                                                        <?php
                                                    }
                                                    ?>
                                                    <input type="hidden" name="type" value="<?=$type?>">
                                                    <label class="form-control-label ">Nama <b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="nama" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">No Hp <b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="phone" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">E-mail <b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="email" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">No KTP <b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="no_ktp" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label "><b>Alamat Tinggal</b></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Propinsi <b class="label--required">*</b></label>
                                                        <select class="form-control col-md-12 province" id="province_id" prefix="" name="province_id" required="">
                                                            <option value="">Pilih Propinsi</option>
                                                            <?php
                                                            foreach ($province as $item){
                                                                ?>
                                                                <option value="<?=$item->province_id?>"><?=$item->province?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Kabupaten/Kota <b class="label--required">*</b></label>
                                                        <select class="form-control col-md-12 city" id="city_id" prefix="" name="city_id" required="">
                                                            <option value="">Pilih Kabupaten/Kota</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Kecamatan <b class="label--required">*</b></label>
                                                        <select class="form-control col-md-12 subdistrict" id="subdistrict_id" prefix="" name="subdistrict_id" required="">
                                                            <option value="">Pilih Kecamatan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Alamat Lengkap <b class="label--required">*</b></label>
                                                        <textarea class="form-control" name="alamat"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label "><b> Alamat Outlet</b></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Propinsi <b class="label--required">*</b></label>
                                                        <select class="form-control col-md-12 province" id="outlet_province_id" prefix="outlet_" name="outlet_province_id" required="">
                                                            <option value="">Pilih Propinsi</option>
                                                            <?php
                                                            foreach ($province as $item){
                                                                ?>
                                                                <option value="<?=$item->province_id?>"><?=$item->province?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Kabupaten/Kota <b class="label--required">*</b></label>
                                                        <select class="form-control col-md-12 city" id="outlet_city_id" prefix="outlet_" name="outlet_city_id" required="">
                                                            <option value="">Pilih Kabupaten/Kota</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Kecamatan <b class="label--required">*</b></label>
                                                        <select class="form-control col-md-12 subdistrict" id="outlet_subdistrict_id" prefix="outlet_" name="outlet_subdistrict_id" required="">
                                                            <option value="">Pilih Kecamatan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label ">Alamat Lengkap <b class="label--required">*</b></label>
                                                        <textarea class="form-control" name="outlet_alamat"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">Nama Bank <b class="label--required">*</b></label>
                                                    <select class="form-control" name="bank_id" required>
                                                        <option> Pilih Bank</option>
                                                        <?php
                                                        foreach ($bank as $item){
                                                            ?>
                                                            <option value="<?=$item->bank_id?>"> <?=$item->nama_bank?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">No Rekening <b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="bank_rekening" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">Atas Nama<b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="bank_atas_nama" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label ">Bank Cabang<b class="label--required">*</b></label>
                                                    <input type="text" placeholder="" name="bank_cabang" class="form-control" value="" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Foto Diri <b class="label--required">*</b></label>
                                                        <div></div>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input img-input" data-display="displayImg1" name="foto_profile" accept="image/x-png,image/gif,image/jpeg" id="customFile1" required="">
                                                            <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                                        </div>
                                                    </div>
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg1" width="200" height="150">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Foto KTP <b class="label--required">*</b></label>
                                                        <div></div>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input img-input" data-display="displayImg2" name="foto_ktp" accept="image/x-png,image/gif,image/jpeg" id="customFile2" required="">
                                                            <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                                        </div>
                                                    </div>
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg2" width="200" height="150">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Swa-Foto dengan KTP <b class="label--required">*</b></label>
                                                        <div></div>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input img-input" data-display="displayImg3" name="selfi_ktp" accept="image/x-png,image/gif,image/jpeg" id="customFile3" required="">
                                                            <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                                        </div>
                                                    </div>
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg3" width="200" height="150">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<script src="<?=base_url()?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/src/sweetalert2.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/script/register.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>
</body>
</html>
