<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Login | Redpos </title>
    <meta name="description" content="Login page">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?=base_url()?>assets/app/custom/login/login-v4.default.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

    <link href="<?=base_url()?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/balioz.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?=base_url()?>assets/media/logos/mkb.ico" />

    <link href="<?=base_url()?>assets/style/src/custom.css" rel="stylesheet" type="text/css" />

</head>
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading" style="background-color : #fff">
<div class="kt-grid kt-grid--ver kt-grid--root full-heigth"  style="background-color : #fff" >
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin"id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor custom-bg-sukses"
             >
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-register__container">
                    <div class="header-register row">
                        <div class="logo-register">
                            <img src="<?=base_url()?>assets/media/logo_mkb_health_and_beauty.png" >
                        </div>

                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head" style="color : #000">
                            <h2 class="kt-login__title text-selamat" style="text-align: center;margin-top: 40px;" >Selamat</h2>
                            <h1 class="kt-login__title text-sukses" style="text-align: center ;">Pendaftaran Berhasil Disimpan</h1>
                            <h1 class="kt-login__title text-sukses" style="text-align: center ;">Hasil verifikasi akan dikirim ke email yang sudah terdaftar</h1>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<script src="<?=base_url()?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/script/admin/login2.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>
</body>
</html>
