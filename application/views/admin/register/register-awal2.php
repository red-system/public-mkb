<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>Login | Redpos </title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="<?= base_url() ?>assets/app/custom/login/login-v6.default.css" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="<?= base_url() ?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"
        type="text/css" />

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <link href="<?= base_url() ?>assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"
        rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css"
        rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css"
        rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css"
        rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css"
        rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css"
        rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet"
        type="text/css" />

    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url() ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url() ?>assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />


    <!-- Custom -->
    <link href="<?= base_url() ?>assets/style/src/custom.css" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/media/logos/mkb.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root col-xs-12">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
            <div
                class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <div
                    class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__body">
                                <div class="kt-login__logo">
                                    <a href="#">
                                        <img class="custom-logo"
                                            src="<?= base_url() ?>assets/media/logo_mkb_health_and_beauty.png">
                                    </a>
                                </div>
                                <div class="kt-login__signin">
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">Daftar Sebagai</h3>
                                    </div>

                                    <div class="kt-login__actions">
                                        <div class="kt-login__actions">
                                            <?php if(isset($reseller)){ ?>
                                                <button class="btn btn-brand btn-pill btn-elevate" data-toggle="modal"
                                                    data-target="#modalSuperRes">
                                                    Super Reseller
                                                </button>
                                            <?php } ?>
                                        </div>
                                        <?php if($uri != ""){ ?>
                                            <button class="btn btn-brand btn-pill btn-elevate" data-toggle="modal"
                                                data-target="#modalRes">
                                                Reseller
                                            </button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content ">
                    <img src="<?=base_url()?>assets/media/bg5.jpg" style="width: 100%">
                    <div class="kt-login__section">
                        <div class="kt-login__block">
                            <!--                        For text information -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSuperRes" tabindex="-1" role="dialog" aria-labelledby="modalSuperResLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalSuperResLabel">Mau tau lebih lanjut tentang Super Reseller MKB?
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-center">
                            <a href="https://drive.google.com/file/d/1cDr16m0CYNPhx4KHd7S0YQsi3S6IdkAs/view?usp=sharing"
                                target="_blank" class="btn btn-success btn-pill btn-elevate">Lihat Penjelasan</a>
                        </div>
                        <div class="col-12 mt-3 d-flex justify-content-center">
                            <button
                                onclick="location.href = '<?= base_url() . "registrasi-superreseller" . ($uri != "" ? "/" . $uri : "") ?>';"
                                class="btn btn-brand btn-pill btn-elevate">Lanjut Registrasi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalRes" tabindex="-1" role="dialog" aria-labelledby="modalResLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalResLabel">Mau tau lebih lanjut tentang Reseller MKB?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-center">
                            <a href="https://drive.google.com/file/d/18Bw80jdoN1CwI54D3a7n6EQzz7DpP-Ey/view?usp=sharing"
                                target="_blank" class="btn btn-success btn-pill btn-elevate">Lihat Penjelasan</a>
                        </div>
                        <div class="col-12 mt-3 d-flex justify-content-center">
                            <button
                                onclick="location.href = '<?= base_url() . "registrasi-reseller" . ($uri != "" ? "/" . $uri : "") ?>';"
                                class="btn btn-brand btn-pill btn-elevate">Lanjut Registrasi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>

    <script src="<?= base_url() ?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript">
    </script>
    <script src="<?= base_url() ?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript">
    </script>
    <script src="<?= base_url() ?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
        type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/jquery-validation/dist/jquery.validate.js"
        type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript">
    </script>
    <script src="<?= base_url() ?>assets/script/admin/login2.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>

</body>

<!-- end::Body -->

</html>