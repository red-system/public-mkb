<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Bonus</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>bonus" class="kt-subheader__breadcrumbs-link">Bonus</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Bonus
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>bonus/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Estimasi Pengiriman</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput" name="start_estimasi" placeholder="Dari" autocomplete="off" data-col-index="6" value="" data-field="estimasi_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput" name="end_estimasi" placeholder="Sampai" autocomplete="off" data-col-index="6" value="" data-field="estimasi_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal Pengiriman</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput" name="start_pengiriman" placeholder="Dari" autocomplete="off" data-col-index="7" value="" data-field="pengiriman_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput" name="end_pengiriman" placeholder="Sampai" autocomplete="off" data-col-index="7" value="" data-field="pengiriman_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <?=($reseller_id==null)?"<th>Penerima Bonus</th>":""?>
                    <th>Agen/Super Agen</th>
                    <th>Type Bonus</th>
                    <th>Total</th>
                    <th>Persentase</th>
                    <th>Total Bonus</th>
                    <th>Estimasi Pengiriman</th>
                    <th>Tanggal Pengiriman</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot>
                <tr>
                    <td colspan="3" style="text-align: right;"><strong>Total</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Bonus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Penrima Bonus</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="nama_penerima" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Agen/Super Agen</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="nama" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Type Bonus</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="type" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jumlah Deposit</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="jumlah_deposit" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label"></label>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Persentase Bonus</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="persentase" class="col-form-label"></label>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group row col-12">
                            <label for="example-text-input" class="col-3 col-form-label">Total Bonus</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="jumlah_bonus" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row col-12">
                            <label for="example-text-input" class="col-3 col-form-label">Status Pengiriman</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="status" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row col-12">
                            <label for="example-text-input" class="col-3 col-form-label">Tanggal Pengiriman</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="tanggal_pengiriman" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Bukti Transfer</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="bukti_transfer_url" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran Bonus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>bonus/pembayaran" id="kt_form_pembayaran" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <input type="hidden" name="bonus_id" id="bonus_id">
                            <input type="hidden" name="bukti_tranfer_old" id="bukti_tranfer_old">
                            <label for="example-text-input" class="col-3 col-form-label">Tanggal Pembayaran</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="col-8 form-control tanggal" autocomplete="off" name="tanggal_pengiriman">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Bukti Transfer <b class="label--required">*</b></label>
                                <div></div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input img-input" data-display="displayImg3" name="bukti_transfer" accept="image/x-png,image/gif,image/jpeg" id="customFile3">
                                    <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                </div>
                            </div>
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg3" width="200" height="150">
                            </div>

                        </div>
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="kt_submit_pembayaran_bonus" >Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>