<!-- begin:: Content -->
						<input type="hidden" id="current_page" value="<?=base_url()."produk-by-location/"?>" name="">
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Produk per lokasi</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>produk-by-location" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>produk-by-location" class="kt-subheader__breadcrumbs-link">Produk per lokasi </a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Data Produk
										</h3>
										<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
										<input type="hidden" id="list_url" value="<?=base_url()?>produk-by-location/list" name="">
										<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
										<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
										<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												<div class="btn-group btn-group btn-pill btn-group-sm">
													<?php
													$uri = $this->uri->segment(1);
													$getUrl = "";
													foreach (array_keys($this->input->get()) as $key) {
														$getUrl .=$key."=".$this->input->get($key)."&";
													}
													$getUrl = rtrim($getUrl,"& ");
													?>
													<a href="<?=base_url()?>produk-by-location/pdf" class="btn btn-danger akses-pdf" id="akses-pdf">
														<i class="la la-file-pdf-o"></i> Print PDF
													</a>
													<a href="<?=base_url()?>produk-by-location/excel" class="btn btn-success akses-excel" id="akses-excel">
														<i class="la la-file-excel-o"></i> Print Excel
													</a>
												</div>

											</div>
										</div>
									</div>									
								</div>
								<div class="kt-portlet__body">
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-12 order-2 order-xl-1 searchForm">
												<div class="row align-items-center">
													<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile" style="display: <?=((isset($_SESSION["redpos_login"]['lokasi_id']))?"none" : "block")?>">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Lokasi</label>
															</div>
															<div class="kt-form__control">
																<select class="form-control searchInput" data-field="lokasi_id" data-col-index="5">
																	<?php if(!isset($_SESSION["redpos_login"]['lokasi_id'])) { ?>
																	<option value="">All</option>
																	<?php foreach ($lokasi as $key) {
																		?>
																		<option value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
																		<?php
																	} 
																} else {
																	?>
																	<option value="<?=$_SESSION["redpos_login"]['lokasi_id']?>"></option>
																	<?php
																} ?>
																</select>
															</div>
														</div>
													</div>
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Produk</label>
															</div>
															<div class="kt-form__control">
																<select class="form-control searchInput" data-col-index="2" data-field="produk_id">
																	<option value="">All</option>
																	<?php foreach ($produk as $key) {
																		?>
																		<option value="<?=$key->produk_id?>"><?=$key->produk_nama?></option>
																		<?php
																	} 
																?>
																</select>
															</div>
														</div>
													</div>													
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Jenis Produk</label>
															</div>
															<div class="kt-form__control">
																<select class="form-control searchInput" data-col-index="3" data-field="jenis_produk_id">
																	<option value="">All</option>
																	<?php foreach ($jenis_produk as $key) {
																		?>
																		<option value="<?=$key->jenis_produk_id?>"><?=$key->jenis_produk_nama?></option>
																		<?php
																	} 
																?>
																</select>
															</div>
														</div>
													</div>
													<div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-form__group kt-form__group--inline">
															<div class="kt-form__label">
																<label>Satuan</label>
															</div>
															<div class="kt-form__control">
																<select class="form-control searchInput" data-col-index="4" data-field="satuan_id">
																	<option value="">All</option>
																	<?php foreach ($satuan as $key) {
																		?>
																		<option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
																		<?php
																	} 
																?>
																</select>
															</div>
														</div>
													</div>																										
												</div>
											</div>
										</div>
									</div>
									<table class="datatable-with-decimal table table-striped- table-hover table-checkable" >
										<thead>
											<tr>
												<th width="30">No</th>
												<th>Kode Produk</th>
												<th>Nama Produk</th>
												<th>Jenis Produk</th>
												<th>Satuan</th>
												<th>Lokasi</th>
												<th>Total Stok</th>
											</tr>
										</thead>
										<tbody id="child_data_ajax"></tbody>
										<tfoot >
											<tr>
												<td colspan="6" style="text-align: right"><strong>Total</strong> </td>
												<td ><strong></strong> </td>
											</tr>
											
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						