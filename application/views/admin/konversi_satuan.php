<!-- begin:: Content -->
<style type="text/css">
    .select2-container .select2-selection--single{
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        position: fixed;
    }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Konversi Satuan</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>konversi-bahan" class="kt-subheader__breadcrumbs-link">Master Data</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>konversi-bahan" class="kt-subheader__breadcrumbs-link">Konversi Satuan</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Konversi Satuan <?=$satuan->satuan_nama?>
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url().'konversi-satuan-list/'.$this->uri->segment(2)?>" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>

                <div style="display: none;" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="dropdown dropdown-inline">
                            <a class="btn btn-brand btn-icon-sm btn-warning" href="<?=base_url()."satuan"?>">
                                <i class="flaticon2-back"></i> Tambah Data
                            </a>
                            <button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
                                <i class="flaticon2-plus"></i> Tambah Data
                            </button>
                        </div>
                    </div>
                </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Konversi Ke</th>
                    <th>Jumlah Konversi</th>
                    <th width="200">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_add" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Konversi Satuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>konversi-satuan-add" method="post" id="kt_add_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="form-control-label ">Di konversi menjadi satuan <b class="label--required">*</b></label>
                                <input type="hidden" name="id_satuan_awal" value="<?=$id?>">
                                <select class="form-control col-md-12 kt_select_2 m-select2"  name="id_satuan_akhir" >
                                    <option value="">Pilih Satuan</option>
                                    <?php
                                    foreach ($satuan_list as $key) {
                                        ?>
                                        <option value="<?=$key->satuan_id?>"><?="(".$key->satuan_kode.") ".$key->satuan_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jumlah konversi <b class="label--required">*</b></label>
                                <input type="text" class="form-control input-numeral" name="jumlah_konversi">
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Konversi Satuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>konversi-satuan-edit" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="form-control-label ">Di konversi menjadi satuan <b class="label--required">*</b></label>
                                <input type="hidden" name="konversi_satuan_id">
                                <input type="hidden" name="id_satuan_awal" value="<?=$id?>">
                                <select class="form-control col-md-12 kt_select_2 m-select2"  name="id_satuan_akhir" >
                                    <option value="">Pilih Satuan</option>
                                    <?php
                                    foreach ($satuan_list as $key) {
                                        ?>
                                        <option value="<?=$key->satuan_id?>"><?="(".$key->satuan_kode.") ".$key->satuan_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jumlah konversi <b class="label--required">*</b></label>
                                <input type="text" class="form-control input-numeral" name="jumlah_konversi">
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
