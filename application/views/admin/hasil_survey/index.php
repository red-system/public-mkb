<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
    <div class="col-md-12 row">
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Jumlah Responden
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($jumlah_responden)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Reseller Pindah
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($pindah)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Reseller Tetap
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($tetap)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Nilai Rata - Rata
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($rata_rata,2)?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-12">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="hasil_survey_url" value="<?=base_url()."hasil-survey/list"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Reseller Yang Pindah
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="hasil-survey-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Leader Saat ini </th>
                            <th>Leader Baru</th>
                            <th>Alasan Pindah</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


