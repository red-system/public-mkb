<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Meeting MKB</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>meeting" class="kt-subheader__breadcrumbs-link">Meeting MKB</a>

        </div>

    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <h3 class="kt-portlet__head-title">
                        Meeting
                    </h3>
                </div>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>meeting/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>

                <div style="display: none;" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?>
                </div>

            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable">
                <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Tanggal</th>
                        <th>Acara</th>
                        <th>Deskripsi</th>
                        <th>Biaya</th>
                        <th>Kehadiran</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal" id="kt_modal_detail_notif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Meeting MKB</h5>
                <span class="close" data-dismiss="modal" aria-label="Close">
                </span>
            </div>
            <div class="modal-body">
                <form action="<?=base_url().'meeting/kehadiran/add/'?>" method="post" id="kt_add_kehadiran">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="meeting_date" class="col-form-label"></label>
                                    <input type="hidden" name="meeting_id">
                                    <input type="hidden" name="reseller_id" value="<?= $_SESSION["redpos_login"]["reseller_id"] ?>">
                                    <input type="hidden" name="meeting_date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Acara</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="meeting_name" class="col-form-label"></label>
                                    <input type="hidden" name="meeting_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Deskripsi</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="meeting_description" class="col-form-label"></label>
                                    <input type="hidden" name="meeting_description">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Kehadiran Meeting</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status_kehadiran"
                                            id="inlineRadio2" value="Offline">
                                        <label class="form-check-label" for="inlineRadio2">Offline</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status_kehadiran"
                                            id="inlineRadio1" value="Online" checked = "checked">
                                        <label class="form-check-label" for="inlineRadio1" >Online</label>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="btn btn-secondary" data-dismiss="modal">Close</span>
                        <button id="kt_add_submit_kehadiran" type="submit" class="btn btn-primary">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>