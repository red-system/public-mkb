<form action="<?=base_url()?>notification/save-add" method="post" id="kt_add" enctype="multipart/form-data">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">Notifikasi MKB</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>notification" class="kt-subheader__breadcrumbs-link">Notifikasi MKB</a>
                <input type="hidden" id="base_url" name="" value="<?=base_url()?>">
            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Tambah Notifikasi
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Tanggal Notifikasi</label>
                                <div class="col-3">
                                    <input type="text" class="form-control tanggal" name="notif_date"
                                        autocomplete="off" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Judul</label>
                                <div class="col-9">
                                    <input type="text" class="form-control" name="notif_title" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Deskripsi</label>
                                <div class="col-9">
                                    <textarea name="notif_description" id="notif_desc"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Status</label>
                                <div class="col-3">
                                    <select name="notif_status" class="form-control">
                                        <option value="publish">Publish</option>
                                        <option value="draft">Draft</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pos-floating-button">
        <a href="<?=base_url()?>notification"
            class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
            <span>
                <i class="la la-angle-double-left"></i>
                <span>Kembali ke Daftar</span>
            </span>
        </a>
        <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
    </div>

</form>