<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='redgroup') ? 'active' : '')?>" href="<?=base_url()?>redgroup">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Request Red Group</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='aktif-redgroup') ? 'active' : '')?>" href="<?=base_url()?>aktif-redgroup">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Aktif Red Group</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='banned-redgroup') ? 'active' : '')?>" href="<?=base_url()?>banned-redgroup">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Banned Red Group</span>
            </a>
        </li>
    </ul>
</div>