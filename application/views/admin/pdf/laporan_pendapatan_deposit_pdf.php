
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Pendapatan Deposit Super Reseller</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
				<td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
				<td style="text-align: right"><strong>Laporan Pendapatan Deposit Super Reseller</strong>
					<br><span>Tanggal Pengiriman : <?=$start_tanggal?> s/d <?=$end_tanggal?> </span>
					<br><span>Pencarian : <?=$cari?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
                    <th>Order Produk No</th>
                    <th>Tanggal Order</th>
                    <th>Tanggal Pengiriman</th>
                    <th>Reseller</th>
                    <th>Pcs</th>
                    <th>Grand Total</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sum = 0;
				foreach ($list as $key) {
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->po_produk_no?></td>
					<td><?=$key->tanggal_order?></td>
					<td><?=$key->tanggal_pengiriman?></td>
					<td><?=$key->nama?></td>
					<td><?=$key->pcs?></td>
					<td style="text-align: right;"><?=$key->grand_total_lbl?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
	</table> 
</div>
 
</body>
</html>