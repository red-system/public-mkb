
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Order Bahan Detail</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
				<td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
				<td style="text-align: right"><strong>Laporan Order Bahan Detail</strong>
					<br><span>Tanggal pemesanan :  <?=(isset($pemesanan_start) ? $pemesanan_start." s/d ".$pemesanan_end : date("Y-m-d",0)." s/d ". date("Y-m-d"))?></span>
					<br><span>Order Bahan No : <?=(isset($po_bahan_no)?$po_bahan_no:'')?></span>
					<br><span>Suplier : <?=(isset($suplier)?$suplier:'Semua Suplier')?></span>
					<br><span>Bahan : <?=(isset($bahan)?$bahan:'Semua Bahan')?></span>
					<br><span>Metode Pembayaran : <?=(isset($tipe_pembayaran)?$tipe_pembayaran:'Semua Tipe Pembayaran')?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
					<th>Order Bahan No</th>
					<th>Tanggal Pemesanan</th>
					<th>Suplier</th>
					<th>Bahan Kode</th>
					<th>Bahan Nama</th>
					<th>Metode Pembayaran</th>
					<th>Harga</th>
					<th>Harga</th>
					<th>Subtotal</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sumJumlah = 0;
				$sumTotal = 0;
				foreach ($list as $key) {
					$sumJumlah +=str_replace(',','',$key->jumlah);
					$sumTotal +=str_replace(',','',$key->sub_total);
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->po_bahan_no?></td>
					<td><?=$key->tanggal_pemesanan?></td>
					<td><?=$key->suplier_nama?></td>
					<td><?=$key->bahan_kode?></td>
					<td><?=$key->bahan_nama?></td>
					<td><?=$key->tipe_pembayaran_nama?></td>
					<td style="text-align: right;"><?=$key->harga?></td>
					<td style="text-align: right;"><?=$key->jumlah?></td>
					<td style="text-align: right;"><?=$key->sub_total?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="8" style="text-align: center;"><strong>Total</strong></td>
					<td style="text-align: right;"><?=number_format($sumJumlah)?></td>
					<td style="text-align: right;"><?=number_format($sumTotal)?></td>
				</tr>
			</tfoot>
	</table> 
</div>
 
</body>
</html>