
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Penjualan Harian</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>
</head>
<body>

<div id="container">
    <table class="datatable table table-borderless">
        <thead>
        <tr>
            <th width="10"></th>
            <th width="42%"></th>
            <th width="50%"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
            <td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
            <td style="text-align: right"><strong>Laporan Penjualan Harian</strong>
                <br><span>Tanggal : <?=$tanggal_start." s/d ".$tanggal_end?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="datatable table table-striped- table-bordered table-hover table-checkable">
        <thead>
        <tr>
            <th width="30">No</th>
            <th>Tanggal</th>
            <th>Jumlah Transaksi</th>
            <th>Total Transaksi</th>
            <th>Total Pembayaran Tunai</th>
            <th>Total Pembayaran Kredit</th>
            <th>Total Pembayaran K.Debet</th>
            <th>Total Pembayaran K.Kredit</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $jum_trans = 0;
        $tot_trans = 0;
        $tot_tunai = 0;
        $tot_kredit = 0;
        $tot_k_debet = 0;
        $tot_k_kredit = 0;
        foreach ($list as $key) {
            $jum_trans += $key->jumlah_transaksi;
            $tot_trans += $key->total_transaksi;
            $tot_tunai += $key->total_tunai;
            $tot_kredit += $key->total_kredit;
            $tot_k_debet += $key->total_k_debet;
            $tot_k_kredit += $key->total_k_kredit;
            ?>
            <tr>
                <td><?=$key->no?></td>
                <td><?=$key->tanggal?></td>
                <td><?=$key->jumlah_transaksi_lbl?></td>
                <td><?=$key->total_transaksi_lbl?></td>
                <td><?=$key->total_tunai_lbl?></td>
                <td><?=$key->total_kredit_lbl?></td>
                <td><?=$key->total_k_debet_lbl?></td>
                <td><?=$key->total_k_kredit_lbl?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2" style="text-align: center"><strong>Total</strong></td>
            <td><?=number_format($jum_trans)?></td>
            <td><?='Rp. '.number_format($tot_trans)?></td>
            <td><?='Rp. '.number_format($tot_tunai)?></td>
            <td><?='Rp. '.number_format($tot_kredit)?></td>
            <td><?='Rp. '.number_format($tot_k_debet)?></td>
            <td><?='Rp. '.number_format($tot_k_kredit)?></td>
        </tr>
        </tfoot>
    </table>
</div>

</body>
</html>