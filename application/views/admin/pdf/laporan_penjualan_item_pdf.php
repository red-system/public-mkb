
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Penjualan Item</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>
</head>
<body>

<div id="container">
    <table class="datatable table table-borderless">
        <thead>
        <tr>
            <th width="10"></th>
            <th width="42%"></th>
            <th width="50%"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
            <td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
            <td style="text-align: right"><strong>Laporan Penjualan Item</strong>
                <br><span>Tanggal : <?=$tanggal_start." s/d ".$tanggal_end?></span>
                <br><span>Produk : <?=$produk?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="datatable table table-striped- table-bordered table-hover table-checkable">
        <thead>
        <tr>
            <th width="30">No</th>
            <th>Kode</th>
            <th>Nama Produk</th>
            <th>Jenis</th>
            <th>Jumlah</th>
            <th>Satuan</th>
            <th>Total Harga</th>
        </tr>
        </thead>
        <tbody id="child_data_ajax">
        <?php
        $subtotal = 0;
        $qty = 0;
        foreach ($list as $key) {
            if($key->no!=null){
                $subtotal +=$key->sub_total;
                $qty += $key->qty;
            }
            ?>
            <tr>
                <td><?=$key->no?></td>
                <td><?=$key->produk_kode?></td>
                <td><?=$key->produk_nama?></td>
                <td><?=$key->jenis_produk_nama?></td>
                <td><?=$key->jumlah_lbl?></td>
                <td><?=$key->satuan_nama?></td>
                <td><?=$key->sub_total_lbl?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4" style="text-align: right;"><strong>Total</strong></td>
            <td><strong><?=number_format($qty)?></strong></td>
            <td><strong></strong></td>
            <td><strong><?="Rp. ".number_format($subtotal)?></strong></td>
        </tr>
        </tfoot>

    </table>
</div>

</body>
</html>