
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Kerugian</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>
</head>
<body>

<div id="container">
    <table class="datatable table table-borderless">
        <thead>
        <tr>
            <th width="10"></th>
            <th width="42%"></th>
            <th width="50%"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
            <td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
            <td style="text-align: right"><strong>Laporan Kerugian</strong>
                <br><span>Tanggal Pencatatan : <?=$tanggal_start." s/d ".$tanggal_end?></span>
                <br><span>Lokasi : <?=$lokasi?></span>
                <br><span>Produk : <?=$produk?></span>
                <br><span>Staff : <?=$staff?></span>
                <br><span>Pencarian : <?=$cari?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="datatable table table-striped- table-bordered table-hover table-checkable">
        <thead>
        <tr>
            <th width="30">No</th>
            <th>Tanggal</th>
            <th>Produk</th>
            <th>Lokasi</th>
            <th>HPP</th>
            <th>Jumlah Item</th>
            <th>Total Kerugian</th>
            <th>Keterangan</th>
            <th>Staff</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sumJum = 0;
        $sumTot = 0;
        foreach ($list as $key) {
            $sumJum +=$key->jumlah;
            $sumTot +=$key->total_kerugian;
            ?>
            <tr>
                <td><?=$key->no?></td>
                <td><?=$key->created_at?></td>
                <td><?=$key->produk_nama?></td>
                <td><?=$key->lokasi_nama?></td>
                <td style="text-align: right"><?=$key->harga?></td>
                <td style="text-align: right"><?=$key->jumlah_label?></td>
                <td style="text-align: right"><?=$key->total_kerugian_label?></td>
                <td><?=$key->keterangan?></td>
                <td><?=$key->staff_nama?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5" style="text-align: right">Total</td>
                <td  style="text-align: right"><?=$sumJum?></td>
                <td  style="text-align: right"><?='Rp. '.number_format($sumTot)?></td>
                <td></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>

</body>
</html>