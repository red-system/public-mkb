
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Penjualan</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"></td>
				<td><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="100px" height="auto"><br><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
				<td style="text-align: right"><strong>Laporan Penjualan</strong>
					<br><span>Tanggal : <?=$tanggal_start." s/d ".$tanggal_end?></span>
					<br><span>No Faktur : <?=$no_faktur?></span>
					<br><span>Lokasi : <?=$lokasi?></span>
					<br><span>Tipe Pembayaran : <?=$tipe_pembayaran?></span>
                    <br><span>Staff : <?=$staff?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
					<th>Tanggal</th>
					<th>No Faktur</th>
					<th>Lokasi</th>
					<th>Pelanggan</th>
					<th>Total</th>
					<th>Biaya Tambahan</th>
					<th>Potongan Akhir</th>
					<th>Grand Total</th>
                    <th>Total HPP</th>
                    <th>Laba</th>
					<th>Tipe Pembayaran</th>
                    <th>Staff</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$grand_total = 0;
                $laba = 0;
                $total_hpp = 0;
				foreach ($list as $key) {
					$grand_total += str_replace(",", "", $key->grand_total);
                    $laba += str_replace(",", "", $key->laba);
                    $total_hpp += str_replace(",", "", $key->total_hpp);
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->tanggal?></td>
					<td><?=$key->no_faktur?></td>
					<td><?=$key->lokasi_nama?></td>
					<td><?=$key->nama_pelanggan?></td>
					<td style="text-align: right;"><?=$key->total?></td>
					<td style="text-align: right;"><?=$key->biaya_tambahan?></td>
					<td style="text-align: right;"><?=$key->potongan_akhir?></td>
					<td style="text-align: right;"><?=$key->grand_total_lbl?></td>
                    <td style="text-align: right;"><?=$key->total_hpp_lbl?></td>
                    <td style="text-align: right;"><?=$key->laba_lbl?></td>
					<td><?=$key->tipe_pembayaran_nama?></td>
                    <td><?=$key->staff_nama?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="8" style="text-align: center"><strong>Total</strong></td>
					<td><?='Rp. '.number_format($grand_total)?></td>
                    <td><?='Rp. '.number_format($total_hpp)?></td>
                    <td><?='Rp. '.number_format($laba)?></td>
					<td></td>
                    <td></td>
				</tr>
			</tfoot>
	</table> 
</div>
 
</body>
</html>