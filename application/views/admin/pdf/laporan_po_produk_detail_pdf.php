
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Order Produk Detail</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
				<td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
				<td style="text-align: right"><strong>Laporan Order Produk Detail</strong>
					<br><span>Order Produk No : <?=$po_produk_no?> </span>
					<br><span>Suplier : <?=$suplier?> </span>
					<br><span>Produk : <?=$produk?> </span>
					<br><span>Metode Pembayaran : <?=$tipe_pembayaran?></span>
					<br><span>Tanggal Pemesanan : <?=$pemesanan_start?> s/d <?=$pemesanan_end?> </span>
					<br><span>Tanggal Penerimaan : <?=$start_penerimaan?> s/d <?=$end_penerimaan?> </span>
					<br><span>Pencarian : <?=$cari?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
					<th>Order Produk No</th>
					<th>Tanggal Pemesanan</th>
					<th>Suplier</th>
					<th>Produk Kode</th>
					<th>Produk Nama</th>
					<th>Metode Pembayaran</th>
					<th>Tanggal Penerimaan</th>
					<th>Harga</th>
					<th>QTY</th>
					<th>Subtotal</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sumJumlah = 0;
				$sumTotal = 0;
				foreach ($list as $key) {
					$sumJumlah +=str_replace(',','',$key->jumlah);
					$sumTotal +=str_replace(',','',$key->sub_total);
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->po_produk_no?></td>
					<td><?=$key->tanggal_pemesanan?></td>
					<td><?=$key->suplier_nama?></td>
					<td><?=$key->produk_kode?></td>
					<td><?=$key->produk_nama?></td>
					<td><?=$key->tipe_pembayaran_nama?></td>
					<td><?=$key->tanggal_penerimaan?></td>
					<td style="text-align: right;"><?=$key->harga_lbl?></td>
					<td style="text-align: right;"><?=$key->jumlah?></td>
					<td style="text-align: right;"><?=$key->sub_total_lbl?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="9" style="text-align: center;"><strong>Total</strong></td>
					<td style="text-align: right;"><?=number_format($sumJumlah)?></td>
					<td style="text-align: right;"><?='Rp. '.number_format($sumTotal)?></td>
				</tr>
			</tfoot>
	</table> 
</div>
 
</body>
</html>