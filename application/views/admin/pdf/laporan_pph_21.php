
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan PPH 21</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>
</head>
<body>

<div id="container">
    <table class="datatable table table-borderless">
        <thead>
        <tr>
            <th width="10"></th>
            <th width="42%"></th>
            <th width="50%"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td rowspan="3"><img src="<?=base_url()?>assets/media/logos/carolinas-logo.png" width="65px" height="auto"></td>
            <td><strong><?=$_SESSION["redpos_company"]['company_name']?></strong><br><?=$_SESSION["redpos_company"]['company_address']?><br><?=$_SESSION["redpos_company"]['company_phone']?></td>
            <td style="text-align: right"><strong>Laporan PPH 21</strong>
                <br><span>Pencarian : <?=$cari?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="datatable table table-striped- table-bordered table-hover table-checkable">
        <thead>
        <tr>
            <th width="30">No</th>
            <th>Nama</th>
            <th>Tanggal</th>
            <th>Bonus</th>
            <th>DPP</th>
            <th>DPP Kumulatif</th>
            <th>PTKP</th>
            <th>Sisa PTKP</th>
            <th>PKP</th>
            <th>PKP Kumultaif</th>
            <th>Persentase</th>
            <th>PPH NPWP</th>
            <th>PPH Non NPWP</th>
            <th>Total PPH</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sum = 0;
        $jumlah_bonus_tot = 0;
        $dasar_pemotongan_tot = 0;
        $dpp_kumulatif_tot = 0;
        $ptkp_tot = 0;
        $sisa_ptkp_perbulan_tot = 0;
        $pkp_tot = 0;
        $pkp_kumulatif_tot = 0;
        $pph_npwp_tot = 0;
        $pph_non_npwp_tot = 0;
        $total_pph_tot = 0;
        foreach ($list as $key) {
            $jumlah_bonus_tot = $jumlah_bonus_tot + $key->jumlah_bonus;
            $dasar_pemotongan_tot = $dasar_pemotongan_tot + $key->dasar_pemotongan;
            $dpp_kumulatif_tot = $dpp_kumulatif_tot + $key->dpp_kumulatif;
            $ptkp_tot = $ptkp_tot + $key->ptkp;
            $sisa_ptkp_perbulan_tot = $sisa_ptkp_perbulan_tot + $key->sisa_ptkp_perbulan;
            $pkp_tot = $pkp_tot + $key->pkp;
            $pkp_kumulatif_tot = $pkp_kumulatif_tot + $key->pkp_kumulatif;
            $pph_npwp_tot = $pph_npwp_tot + $key->pph_npwp;
            $pph_non_npwp_tot = $pph_non_npwp_tot + $key->pph_non_npwp;
            $total_pph_tot = $total_pph_tot + $key->total_pph;
            ?>
            <tr>
                <td><?=$key->no?></td>
                <td><?=$key->nama?></td>
                <td><?=$key->tanggal?></td>
                <td><?=number_format($key->jumlah_bonus)?></td>
                <td ><?=number_format($key->dasar_pemotongan)?></td>
                <td><?=number_format($key->dpp_kumulatif)?></td>
                <td><?=number_format($key->ptkp)?></td>
                <td><?=number_format($key->sisa_ptkp_perbulan)?></td>
                <td><?=number_format($key->pkp)?></td>
                <td><?=number_format($key->pkp_kumulatif)?></td>
                <td><?=($key->persentase)?></td>
                <td><?=number_format($key->pph_npwp)?></td>
                <td><?=number_format($key->pph_non_npwp)?></td>
                <td><?=number_format($key->total_pph)?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"><strong>Total</strong></td>
            <td><?=number_format($jumlah_bonus_tot)?></td>
            <td><?=number_format($dasar_pemotongan_tot)?></td>
            <td><?=number_format($dpp_kumulatif_tot)?></td>
            <td><?=number_format($ptkp_tot)?></td>
            <td><?=number_format($sisa_ptkp_perbulan_tot)?></td>
            <td><?=number_format($pkp_tot)?></td>
            <td><?=number_format($pkp_kumulatif_tot)?></td>
            <td></td>
            <td><?=number_format($pph_npwp_tot)?></td>
            <td><?=number_format($pph_non_npwp_tot)?></td>
            <td><?=number_format($total_pph_tot)?></td>

        </tr>
        </tfoot>
    </table>
</div>

</body>
</html>