						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">

								<h3 class="kt-subheader__title">Order Produk</h3>
								<span class="kt-subheader__separator kt-hidden"></span>
								<div class="kt-subheader__breadcrumbs">
									<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
									<span class="kt-subheader__breadcrumbs-separator"></span>
									<a href="<?=base_url()?>po-produk" class="kt-subheader__breadcrumbs-link">Order Produk</a>
									
								</div>

							</div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head">
									<div class="kt-portlet__head-toolbar col-12">
										<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
											<li class="nav-item">
												<a class="nav-link <?=(($this->uri->segment(1)=='po-produk') ? 'active' : '')?>" href="<?=base_url()?>po-produk">
													<i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Order Produk</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link <?=(($this->uri->segment(1)=='history-po-produk') ? 'active' : '')?>" href="<?=base_url()?>history-po-produk">
													<i class="flaticon2-time"></i> <span class="kt--visible-desktop-inline-block">History Order Produk</span>
												</a>
											</li>
										</ul>
										<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
										<input type="hidden" id="list_url" value="<?=base_url().(($this->uri->segment(1)=="po-produk")?"po-produk":"history-po-produk")?>/list" name="">
										<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
										<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
										<div style="display: none;" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?></div>
									</div>
								</div>
								<div class="kt-portlet__body">
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-8">
												<div class="row align-items-center">
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-xl-4">
												<?php $akses = json_decode($action,true);if(isset($akses["add"])&&$akses["add"]&&$this->uri->segment(1)=="po-produk"){ ?>
												<div class="dropdown dropdown-inline pull-right">
												<a href="<?=base_url()?>po-produk/add" class="btn btn-brand btn-icon-sm">
													<span>
														<i class="flaticon2-plus"></i>
														<span>Beli Produk</span>
													</span>
												</a>												
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
								<table class="datatable table table-striped- table-hover table-checkable" >
									<thead>
										<tr>
											<th width="30">No</th>
											<th>No Order Produk</th>
											<th>Tanggal Pemesanan</th>
											<th>Grand Total</th>
											<th>Metode Pembayaran</th>
											<th>Status Pembayaran</th>
                                            <?php
                                                if($reseller!=null&&$reseller->type=="super agen"){
                                                    ?>
                                                <th>Estimasi Pengiriman</th>
                                                <th>Tanggal Penerimaan</th>
                                                <th>Status Penerimaan</th>
                                                    <?php
                                                }
											?>
                                            <?=($history!=""?"<th>Alasan Tolak</th>":"")?>
											<th width="200">Action</th>
										</tr>
									</thead>
									<tbody id="child_data_ajax"></tbody>
								</table>
							</div>
						</div>
					</div>
                        <div class="modal" id="kt_modale_detail_pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Detail Pembayaran</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6" id="left">
                                            </div>
                                            <div class="col-md-6" id="right">
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-3 col-form-label">Metode Pembayaran</label>
                                                    <label for="example-text-input" class="col-1 col-form-label">:</label>
                                                    <div class="col-8">
                                                        <label name="metode_pembayaran" class="col-form-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="modal" id="kt_modal_detail_po_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLongTitle">Detail Order Produk</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Order No</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="po_produk_no" class="col-form-label"></label>
												</div>
											</div>
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Tanggal Pemesanan</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="tanggal_pemesanan" class="col-form-label"></label>
												</div>
											</div>

											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Total</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="total" class="col-form-label"></label>
												</div>
											</div>
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="grand_total" class="col-form-label"></label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Status Pembayaran</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="status_pembayaran" class="col-form-label"></label>
												</div>
											</div>
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Status Penerimaan</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="status_penerimaan" class="col-form-label"></label>
												</div>
											</div>																																		<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Tanggal Penerimaan</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="tanggal_penerimaan" class="col-form-label"></label>
												</div>
											</div>					
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
												<label for="example-text-input" class="col-1 col-form-label">:</label>
												<div class="col-8">
													<label name="keterangan" class="col-form-label"></label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12">
											<h5 style="text-align: center"><strong>Data Item</strong></h5>	
										</div>
										<div class="col-12">
											<table class="table table-bordered table-hover table-checkable" >
												<thead>
													<tr>
														<th>Nama Produk</th>
														<th>Harga</th>
														<th>Jumlah</th>
														<th>Subtotal</th>
													</tr>
												</thead>
												<tbody id="view_child_data">

												</tbody>
											</table>														
										</div>
									</div>

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal" id="kt_status_produksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<form action="<?=base_url()?>po-produk/selesai-produksi" method="post" id="kt_selesai_produksi">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Status Produksi</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Tanggal Selesai</label>
													<label for="example-text-input" class="col-1 col-form-label">:</label>
													<div class="col-8">
														<input type="hidden" id="produksi_id" name="produksi_id" value="">
														<input type="hidden" id="lokasi_produk_id" name="lokasi_produk_id" value="">
														<input type="text" class="form-control kt-input tanggal" name="tanggal_selesai" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=date("d-m-Y")?>" required="" />
													</div>
												</div>																												
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button id="kt_produksi_status_submit" type="button" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="modal" id="kt_penerimaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<form action="<?=base_url()?>po-produk/penerimaan-po-produk" method="post" id="kt_penerimaan_produksi">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Penerimaan Produksi</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Tanggal Penerimaan</label>
													<label for="example-text-input" class="col-1 col-form-label">:</label>
													<div class="col-8">
														<input type="hidden" id="po_produk_id_penerimaan" name="po_produk_id" value="">
														<input type="text" class="form-control kt-input tanggal" name="tanggal_penerimaan" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=date("Y-m-d")?>" required="" />
													</div>
												</div>
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Lokasi Penerimaan</label>
													<label for="example-text-input" class="col-1 col-form-label">:</label>
													<div class="col-8">
														<select class="form-control col-md-12" name="lokasi_penerimaan_id" required="">
															<option value="">Pilih Lokasi</option>
															<?php
															foreach ($lokasi as $key) {
																?>
																<option class="lokasi_option" value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
																<?php
															}
															?>
														</select>															
													</div>
												</div>								
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button id="kt_po_penerimaan_submit" type="button" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									</div>
								</div>
							</form>
						</div>
					</div>


