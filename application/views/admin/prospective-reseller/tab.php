<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='prospective-reseller') ? 'active' : '')?>" href="<?=base_url()?>prospective-reseller">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Prospectif Agen</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='aktif-agen') ? 'active' : '')?>" href="<?=base_url()?>aktif-agen">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">
                    Aktif Agen
                    <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--rounded kt-badge--danger"><?=$empty_maps?></span></span>
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='banned-agen') ? 'active' : '')?>" href="<?=base_url()?>banned-agen">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Banned Agen
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pasif-agen') ? 'active' : '')?>" href="<?=base_url()?>pasif-agen">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Pasif Agen</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='accepted-agen') ? 'active' : '')?>" href="<?=base_url()?>accepted-agen">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Accepted Agen</span>
            </a>
        </li>
    </ul>
</div>