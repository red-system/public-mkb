<!-- begin:: Content -->
<style type="text/css">
    .select2-container .select2-selection--single{
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        position: fixed;
    }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Produk</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Produk</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Produk
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>resto-produk/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
                <div style="display: none;" data-width="150" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>
            <?php  if(isset($action))  $akses = json_decode($action,true);if ($this->uri->segment(1) == "resto-produk" && ($akses["add"])) { ?>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
                                <i class="flaticon2-plus"></i> Tambah Data
                            </button>
                        </div>
                    </div>
                </div>
            <?php }
                ?>


        </div>
        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" id="generalSearch" placeholder="Search...">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable-with-decimal table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th>Jenis Produk</th>
                    <th>Minimal Stok</th>
                    <th width="100">HPP</th>
                    <th>Harga Jual</th>
                    <th>Global Stok</th>
                    <th>Satuan</th>
                    <?=((isset($_SESSION["redpos_login"]['lokasi_id']))? '<td>Jumlah</td>' : '')?>

                    <th width="160">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot >
                <tr>
                    <td colspan="7" style="text-align: right"><strong>Total</strong> </td>
                    <td ><strong></strong> </td>
                    <td></td>
                    <td></td>
                    <?=((isset($_SESSION["redpos_login"]['lokasi_id']))? '<td></td>' : '')?>
                </tr>

                </tfoot>

            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_add" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>resto-produk/add" method="post" id="kt_add_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Kode Produk<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_kode" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama Produk<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_nama" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jenis Produk <b class="label--required">*</b></label>
                                <select class="form-control col-md-12" name="produk_jenis_id" required="">
                                    <option value="">Pilih Jenis</option>
                                    <?php
                                    foreach ($jenis_produk as $key) {
                                        ?>
                                        <option value="<?=$key->jenis_produk_id?>"><?=$key->jenis_produk_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Satuan <b class="label--required">*</b></label>
                                <select class="form-control col-md-12" name="produk_satuan_id" required="">
                                    <option value="">Pilih Satuan</option>
                                    <?php
                                    foreach ($satuan as $key) {
                                        ?>
                                        <option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="form-control-label ">Harga Pokok <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="hpp_global" class="input-numeral form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Harga Jual <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="harga_eceran" class="input-numeral form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Stok <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_minimal_stock" class="input-numeral form-control" value="" required="">
                            </div>

                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>resto-produk/edit" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label ">Kode Produk<b class="label--required">*</b></label>
                                <input type="hidden" name="produk_id">
                                <input type="text" placeholder="" name="produk_kode" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama Produk<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_nama" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jenis Produk <b class="label--required">*</b></label>
                                <select class="form-control col-md-12" name="produk_jenis_id" required="">
                                    <option value="">Pilih Jenis</option>
                                    <?php
                                    foreach ($jenis_produk as $key) {
                                        ?>
                                        <option value="<?=$key->jenis_produk_id?>"><?=$key->jenis_produk_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Satuan <b class="label--required">*</b></label>
                                <select class="form-control col-md-12" name="produk_satuan_id" required="">
                                    <option value="">Pilih Satuan</option>
                                    <?php
                                    foreach ($satuan as $key) {
                                        ?>
                                        <option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="form-control-label ">Harga Pokok <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="hpp_global" class="input-numeral form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Harga Jual<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="harga_eceran" class="input-numeral form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Stok <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_minimal_stock" class="input-numeral form-control" value="" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Kode Produk</label>
                            <div class="col-9">
                                <label name="produk_kode" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jenis Produk</label>
                            <div class="col-9">
                                <label name="jenis_produk_nama" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Satuan</label>
                            <div class="col-9">
                                <label name="satuan_nama" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Harga Normal</label>
                            <div class="col-9">
                                <label name="harga_eceran" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Minimal Stok</label>
                            <div class="col-9">
                                <label name="produk_minimal_stock" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Dibuat Pada</label>
                            <div class="col-9">
                                <label name="created_at" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Dirubah Pada</label>
                            <div class="col-9">
                                <label name="updated_at" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                    <h5>List Harga</h5>
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover table-checkable" >
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Minimal Jumlah</th>
                                <th>Harga</th>
                            </tr>
                            </thead>
                            <tbody id="view_child_data">
                            <tr>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

