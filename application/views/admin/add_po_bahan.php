						<form action="<?=base_url()?>po-bahan/save-add" method="post" id="kt_add">
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-subheader__main">

									<h3 class="kt-subheader__title">Order Bahan</h3>
									<span class="kt-subheader__separator kt-hidden"></span>
									<div class="kt-subheader__breadcrumbs">
										<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="<?=base_url()?>produksi" class="kt-subheader__breadcrumbs-link">Order Bahan</a>
										<input type="hidden" id="base_url" name="" value="<?=base_url()?>">
									</div>

								</div>
							</div>
							<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__head kt-portlet__head--lg">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												Tambah Order Bahan
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="modal-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Kode Order Bahan</label>
														<div class="col-9">
															<input type="hidden" id="input_po_no" name="po_bahan_no" value="<?=$po_bahan_no?>">
															<label name="produksi_kode" id="display_po_no" class="col-form-label"><?=$po_bahan_no?></label>
														</div>
													</div>
													 <div class="form-group row">
				                                            	<label class="form-control-label col-3">Suplier <b class="label--required">*</b></label>
				                                            	<div class="typeahead col-9">
				                                            		<input class="form-control" id="kt_typeahead_1" name="suplier_nama" type="text" dir="ltr">
				                                            	</div>
				                                        </div>												
												</div>
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Tanggal Pemesanan</label>
														<div class="col-9">
															<input type="text" class="form-control tanggal" name="tanggal_pemesanan" autocomplete="off" required="">
														</div>
													</div>
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
														<div class="col-9">
															<textarea class="form-control" rows="3" name="keterangan"></textarea>
														</div>
													</div>															
												</div>
												<div class="col-md-8"><h5><strong>List Bahan</strong></h5></div>
												<div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
											</div>
											<div class="row">
												<div class="col-md-12" id="item-container">
													
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group kt-form__group row">
													<label for="example-text-input" class="col-4 col-form-label">Total</label>
													<div class="col-8 col-form-label">
														<input type="hidden" id="input_total_item" name="total_item">
														<strong><span class="total" id="total_item">0</span></strong>
													</div>
												</div>
												<div class="form-group kt-form__group row">
													<label for="example-text-input" class="col-4 col-form-label">Biaya
													Tambahan</label>
													<div class="col-8">
														<input type="text" name="tambahan" id="tambahan" class="input-numeral form-control" value="0">
													</div>
												</div>
												<div class="form-group kt-form__group row">
													<label for="example-text-input" class="col-4 col-form-label">Potongan</label>
													<div class="col-8">
														<input type="text" name="potongan" id="potongan" class="input-numeral form-control" value="0">
													</div>
												</div>
												<div class="form-group kt-form__group row">
													<label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
													<div class="col-8 col-form-label">
														<input type="hidden" id="input_grand_total" name="grand_total" value="0">
														<strong><span class="" id="grand_total">0</span></strong>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-group kt-form__group row">
													<label for="example-text-input" class="col-4 col-form-label">Jenis pembayaran <b class="label--required">*</b></label>
													<div class="kt-radio-inline col-8 col-form-label">
														<label class="kt-radio">
															<input type="radio" name="jenis_pembayaran" id="" value="kas" checked=""> Kas
															<span></span>
														</label>
														<label class="kt-radio">
															<input type="radio" name="jenis_pembayaran" id="" value="kredit"> Kredit
															<span></span>
														</label>
													</div>	
												</div>
												<div id="kas_container">
													<div class="form-group kt-form__group row">
														<label for="example-text-input" class="col-4 col-form-label">Tipe Pembayaran<b class="label--required">*</b></label>
														<div class="col-8">
															<select class="form-control col-md-12 tipe-kas" name="tipe_pembayaran_id" id="tipe_pembayaran_id" required="">
																<?php
																foreach ($tipe_pembayaran as $key) {
																	?>
																	<option value="<?=$key->tipe_pembayaran_id?>"><?=$key->tipe_pembayaran_nama." ".$key->no_akun?></option>
																	<?php
																}
																?>
															</select>
														</div>
													</div>
													<div class="form-group kt-form__group row">
														<label for="example-text-input" class="col-4 col-form-label">No Rekening Tujuan/Check/Giro</label>
														<div class="col-8">
															<input type="text" name="tipe_pembayaran_no" id="tipe_pembayaran_no" placeholder="" class="form-control tipe-kas" >
														</div>
													</div>
													<div class="form-group kt-form__group row">
														<label for="example-text-input" class="col-4 col-form-label">Keterangan</label>
														<div class="col-8">
															<textarea class="form-control tipe-kas" name="tipe_pembayaran_keterangan"></textarea>
														</div>
													</div>
												</div>
												<div id="kredit_container" style="display: none;">
													<div class="form-group kt-form__group row">
														<label for="example-text-input" class="col-4 col-form-label">Tenggat Pelunasan<b class="label--required">*</b></label>
														<div class="col-8">
															<input type="text" name="tenggat_pelunasan" id="tenggat_pelunasan" class="form-control tanggal tipe_kredit" autocomplete="off" required="" dissabled="">
														</div>
													</div>
												</div>														
											</div>
										</div>

									</div>
								</div>									
							</div>
							<div class="modal" id="kt_modal_bahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Data Bahan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<input type="hidden" id="list_bahan" value="<?=base_url()?>po-bahan/utility/list-bahan">
												<div class="col-md-12">
													<table class="table table-striped- table-hover table-checkable" id="bahan-table">
														<thead>
															<tr>
																<!-- <th>Kode Bahan</th> -->
																<th>Nama Bahan</th>
																<th>Stok</th>
																<th width="60">Aksi</th>
															</tr>
														</thead>
														<tbody id="bahan_child"></tbody>
													</table>												
												</div>
											</div>


										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>		
							<div class="pos-floating-button">
								<a href="<?=base_url()?>po-bahan" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
									<span>
										<i class="la la-angle-double-left"></i>
										<span>Kembali ke Daftar</span>
									</span>
								</a>
								<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
							</div>				

						</form>