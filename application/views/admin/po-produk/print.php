
<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Print PO</title>



    <!-- Normalize or reset CSS with your favorite library -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">



    <!-- Load paper.css for happy printing -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">



    <!-- Set page size here: A5, A4 or A3 -->

    <!-- Set also "landscape" if you need -->

    <style>

        @page { size: 210mm; margin: 0 0 0 0;} /* output size */

        body.receipt .sheet { width: 210mm;padding: 0px} /* sheet size */

        @media print { body.receipt .sheet { width: 210mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */

        p{font-size: 12px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }
        td{font-size: 12px}

        @media screen {

            body { background: #e0e0e0 }

        }

    </style>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }

        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
    </style>

</head>



<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">

    <img src="<?=base_url()?>assets/media/sidebar.png" width="100">
    <div style="width: 100%;border-bottom: 1px solid #000;margin-top: 30px"></div>
    <h5 style="text-align: center;margin-bottom: 10px;font-size: 12pt">Purchasing Order MKB</h5>
    <div style="width: 100%;border-bottom: 1px solid #000;margin-top: 10px"></div>

    <table style="width: 100%;margin-top: 20px">
        <tbody>
            <tr>
                <td width="10%">No PO</td>
                <td width="40%">: <?=$po_produk->po_produk_no?></td>
                <td width="10%">Tanggal</td>
                <td width="40%">: <?=$po_produk->tanggal_pemesanan?></td>
            </tr>
            <tr>
                <td width="10%">Total</td>
                <td width="40%">: Rp. <?=$po_produk->total?></td>
                <td width="10%">Tambahan</td>
                <td width="40%">: Rp. <?=$po_produk->tambahan?></td>
            </tr>
            <tr>
                <td width="10%">Potongan</td>
                <td width="40%">: Rp. <?=$po_produk->potongan?></td>
                <td width="10%">Grand Total</td>
                <td width="40%">: Rp. <?=$po_produk->grand_total?></td>
            </tr>
            <tr>
                <td width="10%">Status</td>
                <td width="40%">: <?=$po_produk->status_pembayaran?></td>
                <td width="10%"></td>
                <td width="40%"></td>
            </tr>
        </tbody>



    </table>
    <h5 style="margin-bottom: 10px">Daftar Item</h5>
    <table class="table table-bordered table-hover table-checkable" >
        <thead>
        <tr>
            <th>Nama Produk</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Subtotal</th>
        </tr>
        </thead>
        <tbody id="view_child_data">
            <?php
                foreach ($po_produk->item as $key){
                    ?>
                    <tr>
                        <td><?=$key->produk_nama?></td>
                        <td style="text-align: right">Rp. <?=$key->harga?></td>
                        <td style="text-align: right"><?=$key->jumlah_qty?></td>
                        <td style="text-align: right">Rp. <?=$key->sub_total?></td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>

</section>

</body>

</html>
