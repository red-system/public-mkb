<?php

    foreach ($historyList as $item) {
    $sub_total = $item->jumlah * $item->harga;

    ?>

<div class="row" id="produk_item_<?=$item->baris?>">
    <div class="col-md-3">
        <div class="form-group row">
            <label for="example-text-input" class="col-3 col-form-label">produk</label>
            <div class="col-9">
                <div class="input-group col-12">
                    <input type="text" class="form-control readonly" name="item_produk_nama_<?=$item->baris?>" id="item_produk_nama_<?=$item->baris?>"
                           required="" autocomplete="off" value="<?=$item->produk_nama?>">
                    <input type="hidden" class="form-control row_id" id="item_produk_id_<?=$item->baris?>"
                           name="item_produk[produk_<?=$item->baris?>][produk_id]" value="<?=$item->produk_id?>">
                    <div class="input-group-append">
                        <input type="hidden" class="form-control" id="input_last_hpp_<?=$item->baris?>"
                               name="item_produk[produk_<?=$item->baris?>][last_hpp]" value="<?=$item->hpp_global?>">
                        <input type="hidden" class="form-control" id="input_produk_harga_form_<?=$item->baris?>"
                                name="item_produk[produk_<?=$item->baris?>][harga]" value="<?=$item->harga?>">
                        <button class="btn btn-primary produk-search" type="button" data-toggle="modal" data-no="<?=$item->baris?>"><i
                                    class="flaticon-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group row">
            <label for="example-text-input" class="col-6 col-form-label">Harga</label>
            <label for="example-text-input" class="col-6 col-form-label" data-no="<?=$item->baris?>" id="item_produk_harga_<?=$item->baris?>"><?=number_format($item->harga)?></label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group row">
            <label for="example-text-input" class="col-3 col-form-label">Jumlah</label>
            <div class="col-6">
                <input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off"
                       data-no="<?=$item->baris?>" data-min="<?=$item->jumlah?>" value="<?=number_format($item->jumlah)?>" id="item_produk_jumlah_<?=$item->baris?>"
                       name="item_produk[produk_<?=$item->baris?>][jumlah]">
            </div>
            <label for="example-text-input" class="col-3 col-form-label" id="item_produk_satuan_<?=$item->baris?>">Pcs</label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group row">
            <label for="example-text-input" class="col-6 col-form-label">Subtotal</label>
            <div class="col-6">
                <label class="col-form-label" id="item_produk_subtotal_display_<?=$item->baris?>"><?=number_format($sub_total)?></label>
                <input type="hidden" class="form-control input-numeral subtotal" autocomplete="off" data-no="<?=$item->baris?>"
                       value="<?=$sub_total?>" id="item_produk_subtotal_<?=$item->baris?>" name="item_produk[produk_<?=$item->baris?>][subtotal]">
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<?php } ?>