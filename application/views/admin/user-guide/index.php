<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">User Guide</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>user-guide" class="kt-subheader__breadcrumbs-link">Reseller</a>
            <a href="<?=base_url()?>user-guide" class="kt-subheader__breadcrumbs-link">User Guide</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    User Guide
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>user-guide" name="">
            </div>

        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <a href="#">
                                    <img src="<?php echo base_url();?>assets/img/manual.jpg" class="w-100">
                                </a>
                            </center>
                        </div>
                        <div class="card-footer text-center">
                            <a href="#">Manual Book</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <a href="#">
                                    <img src="<?php echo base_url();?>assets/img/proposal.jpg" class="w-100">
                                </a>
                            </center>
                        </div>
                        <div class="card-footer text-center">
                            <a href="#">Proposal Bisnis Reseller/Super Reseller</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <a href="#">
                                    <img src="<?php echo base_url();?>assets/img/grafis.jpg" class="w-100">
                                </a>
                            </center>
                        </div>
                        <div class="card-footer text-center">
                            <a href="#">Info Grafis</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>