<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title"><?=$penerimaan->po_bahan_no?></h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>po-bahan" class="kt-subheader__breadcrumbs-link">Penerimaan PO Bahan</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>po-bahan" class="kt-subheader__breadcrumbs-link">PO Bahan</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url().'po-bahan/penerimaan/'.$this->uri->segment(3)?>" class="kt-subheader__breadcrumbs-link">Penerimaan PO Bahan</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Penerimaan PO Bahan
                </h3>

                <input type="hidden" id="general_data_url" value="<?=base_url()?>po-bahan/penerimaan/detail/<?=$id?>" name="">
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>po-bahan/penerimaan/list/<?=$id?>" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
                <div style="display: none;" data-width="150" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?></div>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="dropdown dropdown-inline">
                        <a href="<?=base_url()?>po-bahan" class="btn btn-brand btn-icon-sm btn-warning"><i class="la la-angle-double-left"></i> Kembali</a>
                        <button type="button" class="btn btn-brand btn-icon-sm" id="add_btn">
                            <i class="flaticon2-plus"></i> Tambah Data
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" id="generalSearch" placeholder="Search...">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Tanggal</th>
                    <th>Bahan</th>
                    <th>Jumlah</th>
                    <th>Status</th>
                    <th>Keterangan</th>
                    <th width="150">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot >
                <tr>
                    <td colspan="3" style="text-align: right"><strong>Total</strong> </td>
                    <td ><strong></strong> </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                </tfoot>
            </table>
        </div>
    </div>
</div>


<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Penerimaan Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url().'po-bahan/penerimaan/add/'.$id?>" method="post" id="kt_add_kirm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <input type="hidden" name="po_bahan_id" value="<?=$id?>">
                                <label for="example-text-input" class="col-3 col-form-label">No Order Bahan</label>
                                <div class="col-9">
                                    <label name="po_bahan_no" class="col-form-label"><?=$penerimaan->po_bahan_no?></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Bahan<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <select class="form-control col-md-12" name="po_bahan_detail_id" id="option_bahan_add" required="">
                                        <option value="">Pilih Bahan</option>
                                        <?php
                                        foreach ($bahan as $key) {
                                            ?>
                                            <option class="bahan_option" value="<?=$key->po_bahan_detail_id?>"><?=$key->bahan_nama?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah Order</label>
                                <div class="col-9">
                                    <label name="jumlah_order" class="col-form-label">0</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Terkirim</label>
                                <div class="col-9">
                                    <label name="terkirim" class="col-form-label">0</label>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Sisa</label>
                                <div class="col-9">
                                    <label name="sisa" class="col-form-label">0</label>
                                    <input type="hidden" id="sisa" name="sisa" value="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="jumlah" class="input-numeral form-control" value="" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="tanggal" class="form-control tanggal" value="" required="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi Penerimaan<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <select class="form-control col-md-12" name="lokasi_penerimaan_id" required="">
                                        <option value="">Pilih Lokasi</option>
                                        <?php
                                        foreach ($lokasi as $key) {
                                            ?>
                                            <option class="lokasi_option" value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                <div class="col-9">
                                    <textarea class="form-control" rows="3" name="keterangan"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal" id="kt_modal_edit_hp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Penerimaan Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url().'po-bahan/penerimaan/edit/'.$id?>" method="post" id="kt_edit_kirim">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Order Produk</label>
                                <div class="col-9">
                                    <label name="po_bahan_no" class="col-form-label"><?=$penerimaan->po_bahan_no?></label>
                                    <input type="hidden" name="penerimaan_bahan_id">
                                    <input type="hidden" name="po_bahan_id">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Bahan<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <select class="form-control col-md-12" name="po_bahan_detail_id" id="option_bahan_edit" required="">
                                        <option value="">Pilih Bahan</option>
                                        <?php
                                        foreach ($bahan as $key) {
                                            ?>
                                            <option class="bahan_option" value="<?=$key->po_bahan_detail_id?>"><?=$key->bahan_nama?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah</label>
                                <div class="col-9">
                                    <label name="jumlah" class="col-form-label">0</label>
                                    <input type="hidden" name="jumlah">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Terkirim</label>
                                <div class="col-9">
                                    <label name="terkirim" class="col-form-label">0</label>
                                    <input type="hidden" name="terkirim">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Sisa</label>
                                <div class="col-9">
                                    <label name="sisa" class="col-form-label">0</label>
                                    <input type="hidden" name="sisa">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="total_qty_penerimaan" class="input-numeral form-control" value="" required="">
                                    <input type="hidden" name="old_jumlah">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="tanggal_penerimaan_label" class="form-control tanggal" required="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi Penerimaan<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <select class="form-control col-md-12" name="lokasi_penerimaan_id" required="">
                                        <option value="">Pilih Lokasi</option>
                                        <?php
                                        foreach ($lokasi as $key) {
                                            ?>
                                            <option class="lokasi_option" value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                <div class="col-9">
                                    <textarea class="form-control" rows="3" name="keterangan_penerimaan"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>