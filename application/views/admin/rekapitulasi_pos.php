<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Rekapitulasi Pos</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>rekapitulasi-pos" class="kt-subheader__breadcrumbs-link">POS</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>rekapitulasi-pos" class="kt-subheader__breadcrumbs-link">Rekapitulasi Pos</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='pos') ? 'active' : '')?>" href="<?=base_url()?>pos">
							<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penjualan</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='rekapitulasi-pos') ? 'active' : '')?>" href="<?=base_url()?>rekapitulasi-pos">
							<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Rekapitulasi POS</span>
						</a>
					</li>
				</ul>
			</div>
			<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
			<input type="hidden" id="list_url" value="<?=base_url()?>rekapitulasi-pos/list" name="">
			<div style="display: none;" id="table_column"><?=$column?></div>
			<?php if(isset($columnDef)) {  ?>
			<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
			<?php } ?>
			<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
			<div style="display: none;" data-style="dropdown" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
		</div>
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable" >
				<thead>
					<tr>
						<th width="30">No</th>
						<th>Tanggal</th>
						<th>No Faktur</th>
						<th>Pelanggan</th>
						<th>Total (Rp)</th>
						<th>Tambahan (Rp)</th>
						<th>Potongan (Rp)</th>
						<th>Grand Total (Rp)</th>
                        <th>Laba</th>
						<th>Tipe Pembayaran</th>

						<th width="160">Action</th>
					</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
				<tfoot >
					<tr>
						<td colspan="4" style="text-align: right"><strong>Total</strong> </td>
						<td><strong></strong></td>
						<td><strong></strong></td>
						<td><strong></strong></td>
						<td><strong></strong></td>
						<td><strong></strong></td>
						<td><strong></strong></td>
                        <td><strong></strong></td>
					</tr>

				</tfoot>
			</table>

			<!--end: Datatable -->
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content ">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="detail_url" value="<?=base_url()?>rekapitulasi-pos/detail" name="">
				<div class="row">
					<div class="col-md-4">
						<div class="col-md-9">
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<label name="no_faktur" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<label name="tanggal" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Lokasi</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<label name="lokasi_nama" class="col-form-label"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
								<label for="example-text-input" class="col-1 col-form-label">:</label>
								<div class="col-8">
									<label name="nama_pelanggan" class="col-form-label"></label>
								</div>
							</div>													
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Alamat</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="alamat_pelanggan" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Telepon Pelanggan</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="telepon_pelanggan" class="col-form-label"></label>
							</div>
						</div>	
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Pengiriman</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="pengiriman" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Total</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="total" class="col-form-label"></label>
							</div>
						</div>													 	
					</div>
					<div class="col-md-4">

						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Biaya Tambahan</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="biaya_tambahan" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Potongan Akhir</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="updated_at" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="grand_total" class="col-form-label"></label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran</label>
							<label for="example-text-input" class="col-1 col-form-label">:</label>
							<div class="col-8">
								<label name="tipe_pembayaran_nama" class="col-form-label"></label>
							</div>
						</div>													 	
					</div>
					<div class="col-md-12">
						<input type="hidden" id="penjualan_id" name="">
						<table class="table table-striped- table-hover table-checkable" >
							<thead>
								<tr>
									<th width="150">Kode Produk</th>
									<th>Nama Produk</th>
									<th width="80">QTY</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
									<th>Subtotal</th>
								</tr>
							</thead>
							<tbody id="item_child">

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
