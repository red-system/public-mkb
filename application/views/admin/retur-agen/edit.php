<form action="<?=base_url()?>retur/save-edit" method="post" id="kt_add" enctype="multipart/form-data">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Retur</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>retur" class="kt-subheader__breadcrumbs-link">Retur</a>
                <input type="hidden" id="base_url" name="" value="<?=base_url()?>">
                <input type="hidden" id="retur_agen_id" name="retur_agen_id" value="<?=$retur_agen_id?>">
                <input type="hidden" id="index_no" name="" value="<?=$index_no?>">
            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Retur
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8"><h5><strong>List Produk</strong></h5></div>
                        <div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
                    </div>
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12" id="item-container">
                            <?php
                            $i = 0;
                            foreach ($retur_agen_detail as $item){
                                ?>
                                <div class="row" id="produk_item_<?=$i?>">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-3 col-form-label">produk<b class="label--required">*</b></label>
                                            <div class="col-9">
                                                <div class="input-group col-12">
                                                    <input type="text" class="form-control readonly" name="item_produk_nama_<?=$i?>" id="item_produk_nama_<?=$i?>" value="<?=$item->produk_nama?>" required="" autocomplete="off">
                                                    <input type="hidden" class="form-control" id="item_produk_id_<?=$i?>" name="item_produk[produk_<?=$i?>][produk_id]" value="<?=$item->produk_id?>">
                                                    <div class="input-group-append">
                                                        <input type="hidden" class="form-control" id="input_last_hpp_<?=$i?>" name="item_produk[produk_<?=$i?>][last_hpp]">
                                                        <input type="hidden" class="form-control" id="input_produk_harga_form_<?=$i?>" name="item_produk[produk_<?=$i?>][harga]">
                                                        <button class="btn btn-primary produk-search" type="button" data-toggle="modal" data-no="0"><i class="flaticon-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-6 col-form-label">Jumlah Stok<b class="label--required">*</b></label>
                                            <div class="col-4">
                                                <label for="example-text-input" class="col-6 col-form-label" id="jumlah_stock_<?=$i?>"><?=number_format($item->jumlah_stok)?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-3 col-form-label">Jumlah Retur<b class="label--required">*</b></label>
                                            <div class="col-6">
                                                <input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off" data-maksimal="<?=$item->jumlah_stok?>" data-no="0" value="<?=number_format($item->jumlah)?>" id="item_produk_jumlah_<?=$i?>" name="item_produk[produk_<?=$i?>][jumlah]">
                                            </div>
                                            <label for="example-text-input" class="col-3 col-form-label" id="item_produk_satuan_<?=$i?>"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-3 col-form-label">Keterangan<b class="label--required">*</b></label>
                                            <div class="col-9">
                                                <textarea class="form-control keterangan" name="item_produk[produk_<?=$i?>][keterangan]"><?=$item->keterangan?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-danger btn-sm delete-produk" data-no="0" data-item-produk="produk_item_<?=$i?>"><i class="flaticon2-trash"></i></button>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="kt_modal_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="list_produk" value="<?=base_url()?>retur/list-produk">
                        <div class="col-md-12">
                            <table class="table table-striped- table-hover table-checkable" id="produk-table">
                                <thead>
                                <tr>
                                    <th>Kode Produk</th>
                                    <th>Nama Produk</th>
                                    <th>Jumlah</th>
                                    <th width="60">Aksi</th>
                                </tr>
                                </thead>
                                <tbody id="produk_child">

                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pos-floating-button">
        <a href="<?=base_url()?>retur" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
									<span>
										<i class="la la-angle-double-left"></i>
										<span>Kembali ke Daftar</span>
									</span>
        </a>
        <button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
    </div>

</form>