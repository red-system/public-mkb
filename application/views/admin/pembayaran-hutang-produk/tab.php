<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(2)=='') ? 'active' : '')?>" href="<?=base_url()?>pembayaran-hutang-produk">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">request</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(2)=='approve-page') ? 'active' : '')?>" href="<?=base_url()?>pembayaran-hutang-produk/approve-page">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Approve</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(2)=='refuse-page') ? 'active' : '')?>" href="<?=base_url()?>pembayaran-hutang-produk/refuse-page">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Refuse</span>
            </a>
        </li>
    </ul>
</div>