<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title"><?=$pengiriman->no_pembayaran?></h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>pembayaran-hutang-produk" class="kt-subheader__breadcrumbs-link">Pengiriman Order Produk</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>pembayaran-hutang-produk" class="kt-subheader__breadcrumbs-link">Hutang/Piutang</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url().'pembayaran-hutang-produk/pengiriman/'.$this->uri->segment(3)?>" class="kt-subheader__breadcrumbs-link">Pengiriman Order Produk</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Pengiriman Order Produk
                </h3>

                <input type="hidden" id="general_data_url" value="<?=base_url()?>pembayaran-hutang-produk/pengiriman/detail/<?=$id?>" name="">
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>pembayaran-hutang-produk/pengiriman/list/<?=$id?>" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
                <div style="display: none;" data-width="150" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?></div>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="dropdown dropdown-inline">
                        <a href="<?=base_url()?>pembayaran-hutang-produk/approve-page" class="btn btn-brand btn-icon-sm btn-warning"><i class="la la-angle-double-left"></i> Kembali</a>
                        <button type="button" class="btn btn-brand btn-icon-sm" id="add_btn">
                            <i class="flaticon2-plus"></i> Tambah Data
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" id="generalSearch" placeholder="Search...">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Tanggal</th>
                    <th>Jumlah</th>
                    <th>Status</th>
                    <th>Keterangan</th>
                    <th width="150">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot >
                <tr>
                    <td colspan="2" style="text-align: right"><strong>Total</strong> </td>
                    <td ><strong></strong> </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                </tfoot>
            </table>
        </div>
    </div>
</div>


<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Pengiriman Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url().'pembayaran-hutang-produk/pengiriman/add/'.$id?>" method="post" id="kt_add_kirm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <input type="hidden" name="pembayaran_hutang_produk_id" value="<?=$id?>">
                                <input type="hidden" name="produk_id" value="<?=$pengiriman->produk_id?>">
                                <label for="example-text-input" class="col-3 col-form-label">No Pembayaran Hutang Produk</label>
                                <div class="col-9">
                                    <label name="no_pembayaran" class="col-form-label"><?=$pengiriman->no_pembayaran?></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah Order</label>
                                <div class="col-9">
                                    <label name="jumlah_order" class="col-form-label"><?=$pengiriman->jumlah_order?></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Terkirim</label>
                                <div class="col-9">
                                    <label name="terkirim" class="col-form-label"><?=$pengiriman->terkirim ?></label>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Sisa</label>
                                <div class="col-9">
                                    <label name="sisa" class="col-form-label"><?=$pengiriman->sisa?></label>
                                    <input type="hidden" name="sisa" value="<?=$pengiriman->sisa?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="jumlah" class="input-numeral form-control" value="" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="tanggal" class="form-control tanggal" value="" required="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                <div class="col-9">
                                    <textarea class="form-control" rows="3" name="keterangan"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal" id="kt_modal_edit_hp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Pengiriman Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url().'pembayaran-hutang-produk/pengiriman/edit/'.$id?>" method="post" id="kt_edit_kirim">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Order Produk</label>
                                <div class="col-9">
                                    <label name="po_produk_no" class="col-form-label"><?=$pengiriman->no_pembayaran?></label>
                                    <input type="hidden" name="pengiriman_produk_id">
                                    <input type="hidden" name="produk_id" value="<?=$pengiriman->produk_id?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah Hutang</label>
                                <div class="col-9">
                                    <label name="jumlah_order" class="col-form-label"><?= $pengiriman->jumlah_order ?></label>

                                    <input type="hidden" name="jumlah_order">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Terkirim</label>
                                <div class="col-9">
                                    <label name="terkirim" class="col-form-label"><?=$pengiriman->terkirim?></label>
                                    <input type="hidden" name="terkirim">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Sisa</label>
                                <div class="col-9">
                                    <label name="sisa" class="col-form-label"><?=$pengiriman->sisa?></label>
                                    <input type="hidden" name="sisa" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="total_qty_pengiriman" class="input-numeral form-control" value="" required="">
                                    <input type="hidden" name="old_jumlah">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" placeholder="" name="tanggal_pengiriman" class="form-control tanggal" value="" required="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                <div class="col-9">
                                    <textarea class="form-control" rows="3" name="keterangan_pengiriman"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>