<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='super-agen') ? 'active' : '')?>" href="<?=base_url()?>super-agen">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Request Super Agen</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='aktif-super-agen') ? 'active' : '')?>" href="<?=base_url()?>aktif-super-agen">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">
                    Aktif Super Agen
                    <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--rounded kt-badge--danger"><?=$empty_maps?></span></span>
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='banned-super-agen') ? 'active' : '')?>" href="<?=base_url()?>banned-super-agen">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Banned Super Agen</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pasif-super-agen') ? 'active' : '')?>" href="<?=base_url()?>pasif-super-agen">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Pasif Super Agen</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='accepted-super-agen') ? 'active' : '')?>" href="<?=base_url()?>accepted-super-agen">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">
                    Accepted Super Agen
                </span>
            </a>
        </li>
    </ul>
</div>