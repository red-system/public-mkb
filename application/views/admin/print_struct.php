<!DOCTYPE html>

<html lang="en">



<head>

	<meta charset="utf-8">

	<title>Receipt</title>



	<!-- Normalize or reset CSS with your favorite library -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">



	<!-- Load paper.css for happy printing -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">



	<!-- Set page size here: A5, A4 or A3 -->

	<!-- Set also "landscape" if you need -->

	<style>

		@page { size: 58mm; margin: 0 0 0 0;} /* output size */

		body.receipt .sheet { width: 58mm;padding: 0px} /* sheet size */

		@media print { body.receipt .sheet { width: 58mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */

		p{font-size: 10px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }



		@media screen {

			body { background: #e0e0e0 }

		}

	</style>

</head>



<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


	<h5 style="text-align: center;margin-bottom: 10px"><?=$_SESSION['redpos_company']['company_name']?></h5>
    <table>
        <tbody>
        <tr><td><p>Kode</p></td><td><p>:</p></td><td><p><?=$trans->no_faktur?></p></td></tr>
        <tr><td><p>Tanggal</p></td><td><p>:</p></td><td><p><?=$trans->tanggal?></p></td></tr>
        <tr><td><p>Pelanggan</p></td><td><p>:</p></td><td><p><?=$trans->nama_pelanggan?></p></td></tr>
        <tr><td><p>Alamat</p></td><td><p>:</p></td><td><p><?= (empty($trans->alamat_pelanggan) ? '-' : $trans->alamat_pelanggan) ?></p></td></tr>
        <tr><td><p>No. Telepon</p></td><td><p>:</p></td><td><p><?= (empty($trans->telepon_pelanggan) ? '-' : $trans->telepon_pelanggan) ?></p></td></tr>
        <tr><td><p><?=$reseller!=null?ucwords($reseller->type):"Staff"?></p></td><td><p>:</p></td><td><p><?=$_SESSION["redpos_login"]['user_name']?></p></td></tr>
		<tr><td><p>Bank Agent</p></td><td><p>:</p></td><td><p><?=$trans->nama_bank?></p></td></tr>
		<tr><td><p>No. Rekening</p></td><td><p>:</p></td><td><p><?=$trans->bank_rekening?></p></td></tr>
	</tbody>
    </table>

	<p style="margin:0px">------------------------------------------------------</p>

	<table style="width: 100%">

		<tbody>


			<?php
				$count = 0;
				foreach ($trans->detail_transaksi as $key) {
					$count +=$key->qty;
					?>
					<tr ><td colspan="3"><p><?=$key->produk_nama?></p></td></tr>

					<tr><td><p><?=$key->qty." ".$key->satuan_nama?></p></td><td><p>x <?=number_format($key->harga)?></p></td><td style="text-align: right"><p><?=number_format($key->sub_total)?></p></td></tr>
			<?php
				}
			?>




		</tbody>



	</table>

	<p style="margin:0px">------------------------------------------------------</p>

	<table style="width: 100%">

		<thead><th></th><th></th><th></th></thead>

		<tbody>

		
		<tr ><td colspan="2" style="text-align: right"><p>Total Harga :</p></td><td style="text-align: right"><p><?=number_format($trans->grand_total)?></p></td></tr>

		<tr ><td colspan="2" style="text-align: right"><p>Uang Diterima :</p></td><td style="text-align: right"><p><?=number_format($trans->terbayar)?></p></td></tr>
        <tr ><td colspan="2" style="text-align: right"><p>Tambahan :</p></td><td style="text-align: right"><p><?=number_format($trans->biaya_tambahan)?></p></td></tr>
		<tr ><td colspan="2" style="text-align: right"><p>Kembalian :</p></td><td style="text-align: right"><p><?=number_format($trans->kembalian)?></p></td></tr>

		</tbody>



	</table>

	<p style="margin:0px;text-align: center">Terima kasih telah berkunjung</p>

	<br>

	<p></p>

	<p></p>

	<?php $time = time()+3600; $date = date_create(date("Y-m-d H:i:s",$time), timezone_open('Asia/Kuala_Lumpur'));?>

	<p>Log : <?=$_SESSION["redpos_login"]['user_name']." ".date_format($date, 'Y-m-d H:i:s P')?></p>

</section>

</body>

</html>
