<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='reward') ? 'active' : '')?>" href="<?=base_url()?>reward">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Reward</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='claimed-reward') ? 'active' : '')?>" href="<?=base_url()?>claimed-reward">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">
                    Claimed Reward

                </span>
            </a>
        </li>
    </ul>
</div>