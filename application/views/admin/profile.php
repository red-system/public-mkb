<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_profile">

    <div class="row">
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">

                <div class="kt-portlet__body">
                    <div class="kt-profile-pic">
                        <img src="<?=$this->config->item('dev_storage_image').$_SESSION['redpos_login']['avatar']?>" style='height:auto;width: 40%;' class="account_avatar">
                    </div>
                    <h5 style="text-align: center;margin-top: 20px"><b class="account_name"><?=$user->staff_nama?></b></h5>
                    <span style="text-align: center;"><?=$user->user_role_name?></span>
                    <span style="text-align: center;"><?=$user->staff_email?></span>
                    <span style="text-align: center;"><?=$user->staff_phone_number?></span>
                    <?php if($reseller != null && ($reseller->type == 'super agen' || $reseller->type == 'agent')) {?>
                    <a href="<?php base_url() ?>maps" class="btn btn-pill btn-success mt-5">Update Koordinat Outlet</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <ul class="nav nav-tabs  nav-tabs-line" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tabs_1_1" role="tab">Personal Info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_2" role="tab">Change Avatar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab">Change Password</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_4" role="tab">NPWP</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel" style="padding: 10px">
                            <form role="form" action="<?=base_url()?>profile/personal-info" id="edit_personal_form" method="post">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" placeholder="John" name="user_name" class="form-control" value="<?=$user->staff_nama?>" readonly="" id="personal_name"> </div>
                                <div class="form-group" style="display: none">
                                    <label class="control-label">Email</label>
                                    <input type="text" placeholder="Doe" name="email" class="form-control" value="<?=$user->staff_email?>" readonly="" id="personal_email"> </div>
                                <div class="form-group">
                                    <label class="control-label">Mobile Number</label>
                                    <input type="tel" placeholder="+1 646 580 DEMO (6284)" name="phone_number" class="form-control" value="<?=$user->staff_phone_number?>" readonly="" id="personal_phone">
                                </div>
                                <div class="form-group row">
                                    <div class="form-group col-6">
                                        <label class="control-label">Tempat lahir</label>
                                        <div class="col-md-12">
                                            <input type="text"  name="tempat_lahir" class="form-control" value="<?=$user->tempat_lahir?>" id="tempat_lahir">
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="control-label">Tanggal Lahir <b class="label--required">*</b></label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="21-01-1987" name="tanggal_lahir" class="form-control tanggal" value="<?=$user->tanggal_lahir?>"  id="tanggal_lahir">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">No KTP</label>
                                    <input type="text" name="no_ktp" class="form-control" value="<?=($reseller!=null&&($reseller->no_ktp))?>" readonly="" id="no_ktp">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Alamat Tinggal</label>
                                    <div class="form-group">
                                        <label class="form-control-label ">Propinsi <b class="label--required">*</b></label>
                                        <select class="form-control col-md-12 province" id="province_id" prefix="" name="province_id" required="">
                                            <option value="">Pilih Propinsi</option>
                                            <?php
                                            foreach ($province as $item){
                                                ?>
                                                <option <?=(($reseller!=null)&&($item->province_id == $reseller->province_id))?"selected":""?> value="<?=$item->province_id?>"><?=$item->province?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label ">Kabupaten/kota <b class="label--required">*</b></label>
                                        <select class="form-control col-md-12 city" id="city_id" prefix="" name="city_id" required="">
                                            <option value="">Pilih Kabupaten/kota</option>
                                            <?php
                                            if(isset($city)){
                                                foreach ($city as $item){
                                                    ?>
                                                    <option <?=(($reseller!=null)&&$item->city_id == $reseller->city_id)?"selected":""?> value="<?=$item->city_id?>"><?=$item->city_name?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label ">Kecamatan <b class="label--required">*</b></label>
                                        <select class="form-control col-md-12 subdistrict" id="subdistrict_id" prefix="" name="subdistrict_id" required="">
                                            <option value="">Pilih Kecamatan</option>
                                            <?php
                                            if(isset($sub_district)) {
                                                foreach ($sub_district as $item) {
                                                    ?>
                                                    <option <?= (($reseller != null) && $item->subdistrict_id == $reseller->subdistrict_id) ? "selected" : "" ?>
                                                            value="<?= $item->subdistrict_id ?>"><?= $item->subdistrict_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Alamat</label>
                                        <textarea class="form-control" id="staff_alamat" rows="3" name="staff_alamat" readonly=""><?=$user->staff_alamat?></textarea>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label">Bank</label>
                                    <select class="form-control col-md-12" id="bank" prefix="" name="bank_id" required="">
                                        <option value="">Pilih Bank</option>
                                        <?php
                                        foreach ($bank as $item){
                                            ?>
                                            <option <?=($reseller != null && $item->bank_id == $reseller->bank_id)?"selected":""?> value="<?=$item->bank_id?>"><?=$item->nama_bank?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label ">No Rekening <b class="label--required">*</b></label>
                                        <input type="text" placeholder="" name="bank_rekening" class="form-control" value="<?=( $reseller != null? $reseller->bank_rekening:"")?>" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label ">Atas Nama<b class="label--required">*</b></label>
                                        <input type="text" placeholder="" name="bank_atas_nama" class="form-control" value="<?=( $reseller != null? $reseller->bank_atas_nama:"")?>" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label ">Link Tokopedia</label>
                                        <input type="text" placeholder="" name="tokopedia" class="form-control" value="<?=( $reseller != null? $reseller->tokopedia:"")?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label ">Link Shopee</label>
                                        <input type="text" placeholder="" name="shopee" class="form-control" value="<?=( $reseller != null? $reseller->shopee:"")?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label ">Link Tiktok</label>
                                        <input type="text" placeholder="" name="tiktok" class="form-control" value="<?=( $reseller != null? $reseller->tiktok:"")?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label ">Link Google Maps</label>
                                        <input type="text" placeholder="" name="maps_url" class="form-control" value="<?=( $reseller != null? $reseller->maps:"")?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Kode Referal Anda</label>
                                    <label class="form-control col-8"><?=$user->referal_code?></label>
                                </div>
                                <div class="form-group" class="" id="personal_edit_group" style="display: none;">
                                    <button id="kt_personal_info_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                    <button type="button" class="btn btn-danger pull-right" style="margin: 5px" id="cancel_personal_btn"><i class="fa fa-window-close"></i> Cancel</button>
                                </div>
                                <div class="form-group" class="" id="personal_cancel_group" >
                                    <?php if($reseller != null &&($reseller->first_deposit+$reseller->is_red)>0) {?>
                                        <button type="button" id="copy_link" data-clipboard-text="<?=$user->referal_link?>" class="btn btn-success pull-right" style="margin: 5px"><i class="fa fa-paste"></i> Copy Referal Link</button>
                                    <?php } ?>

                                    <button type="button" class="btn btn-warning pull-right" style="margin: 5px" id="edit_personal_btn"><i class="fa fa-edit"></i> Edit</button>

                                    <?php if($reseller != null &&($reseller->first_deposit+$reseller->is_red)>0 && $reseller->type == 'super agen') {?>
                                    <button type="button" id="copy_link_upgrade" data-clipboard-text="<?=$user->referal_link_upgrade?>" class="btn btn-danger pull-right" style="margin: 5px"><i class="fa fa-paste"></i> Copy Upgrade Super Reseller Link</button>
                                    <?php } ?>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel" style="padding: 10px">
                            <form role="form" action="<?=base_url()?>profile/change-avatar" id="edit_avatar_form" method="post" enctype='multipart/form-data'>
                                <div class="form-group">
                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg" width="200" height="150"> </div>
                                        <div class="form-group">
                                            <label>File Browser</label>
                                            <div></div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="avatar" accept="image/x-png,image/gif,image/jpeg" id="customFile">
                                                <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10">
                                    <button id="kt_avatar_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>

                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel" style="padding: 10px">
                            <form role="form" action="<?=base_url()?>profile/change-password" id="edit_password_form" method="post">
                                <div class="form-group">
                                    <label class="control-label">Current Password</label>
                                    <input type="password" name="old_pass" id="old_pass" class="form-control"> </div>
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" name="new_pass" id="new_pass" class="form-control"> </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password</label>
                                    <input type="password" name="re_pass" id="re_pass" class="form-control"> </div>
                                <div class="margin-top-10">
                                    <button id="kt_password_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="kt_tabs_1_4" role="tabpanel" style="padding: 10px">
                            <?php
                                if($is_save==$true){
                            ?>
                            <form role="form" action="<?=base_url()?>profile/change-npwp" id="edit_npwp_form" method="post" enctype='multipart/form-data'>
                                <div class="form-group">
                                    <label class="control-label">No NPWP <b class="label--required">*</b></label>
                                    <input type="text" name="no_npwp" id="no_npwp" class="form-control" required=""> </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Foto NPWP <b class="label--required">*</b></label>
                                        <div></div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input img-input"
                                                   data-display="displayImg2" name="foto_npwp"
                                                   accept="image/x-png,image/gif,image/jpeg"
                                                   id="customFile2" required="">
                                            <label class="custom-file-label selected" for="customFile"
                                                   id="labelCustomFile"></label>
                                            <input type="hidden" name="input_foto_npwp">
                                        </div>
                                    </div>
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 150px;">
                                        <img src="<?= base_url() ?>assets/media/no_image.png" alt=""
                                             id="displayImg2" width="200" height="150">
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Swa-foto dengan NPWP <b class="label--required">*</b></label>
                                        <div></div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input img-input"
                                                   data-display="displayImg1" name="foto_selfi"
                                                   accept="image/x-png,image/gif,image/jpeg"
                                                   id="customFile1" required="">
                                            <label class="custom-file-label selected" for="customFile"
                                                   id="labelCustomFile"></label>
                                            <input type="hidden" name="input_foto_selfi">
                                        </div>
                                    </div>
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 150px;">
                                        <img src="<?= base_url() ?>assets/media/no_image.png" alt=""
                                             id="displayImg1" width="200" height="150">
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="form-group">

                                        <label for="vehicle1"> Pekerjaan Tetap lain</label>
                                        <div class="radio">
                                            <label class="radio-inline" style="margin: 10px"><input type="radio"  value="0" name="penghasilan_lain" > Pekerjaan tetap lain (penerima upah)</label>
                                        </div>
                                        <div class="radio">
                                            <label class="radio-inline" style="margin: 10px"><input type="radio" value="1" name="penghasilan_lain" > Wirausaha</label>
                                        </div>
                                        <div class="radio disabled">
                                            <label class="radio-inline" style="margin: 10px"><input type="radio" value="1" name="penghasilan_lain" checked> Penghasilan hanya dari MKB</label>
                                        </div>



                                        <div class="form-group" id="penghasilan_text" style="display : none">
                                            <input type="hidden" name="input_penghasilan_lain" id="penghasilan_val" value="false">
                                            <label>Keteranagn penghasilan lain <b class="label--required">*</b></label>
                                            <textarea class="form-control" name="keterangan_lain"></textarea>
                                        </div>
                                    </div>


                                </div>
                                <div id="case3">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Kelamin </label>
                                        <label class="radio-inline" style="margin: 10px"><input type="radio" value="laki-laki" name="kelamin" checked> Laki-laki</label>
                                        <label class="radio-inline" style="margin: 10px"><input type="radio" value="perempuan" name="kelamin"> Perempuan</label>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Status Pernikahan </label>
                                        <label class="radio-inline" style="margin: 10px"><input type="radio" value="single" name="status_pernikahan" checked> Single</label>
                                        <label class="radio-inline" style="margin: 10px"><input type="radio" value="menikah" name="status_pernikahan"> Menikah</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Tanggungan </label>
                                       <input class="form-control" type="text" name="jumlah_tanggungan" value="0">
                                    </div>
                                </div>
                                <div class="margin-top-10">
                                    <button id="kt_npwp_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                            <?php
                                } else {
                                    ?>
                                    <div class="">
                                        <h5><?=$text_npwp?></h5>
                                    </div>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                    
