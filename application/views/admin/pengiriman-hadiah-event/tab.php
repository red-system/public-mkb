<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-hadiah-event') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-hadiah-event">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Hadiah Event List</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-hadiah-event-proses') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-hadiah-event-proses">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Hadiah Event Ter-proses</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-hadiah-event-terkirim') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-hadiah-event-terkirim">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Hadiah Event Terkirim</span>
            </a>
        </li>
    </ul>
</div>