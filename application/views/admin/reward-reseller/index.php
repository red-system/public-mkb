<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Reward</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>reward" class="kt-subheader__breadcrumbs-link">Reward</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>reward" class="kt-subheader__breadcrumbs-link"> Reward Reseller</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<input type="hidden" id="google-form" value="<?=$this->config->item('form-reward')?>">
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>reward-reseller/list" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Reward Reseller
                </h3>
            </div>
            <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <input type="hidden" id="reseller_id" value="<?=$reseller_id?>">
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                            <input type="hidden" id="total_penjualan" value="<?=$penjualan_perbulan?>">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <h5>Total Penjualan 1 bulan : Rp. <?=number_format($penjualan_perbulan)?> </h5>
                            </div>
                            <div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
                                <?php if($passBintang){ ?>
                                    <button class="btn btn-primary float-right" id="claimTambahan">Tukar Bintang</button>
                                <?php } ?>
                            </div>
                            <div class="col-md-2 kt-margin-b-20-tablet-and-mobile">
                                <?php if($pass){ ?>
                                <button class="btn btn-primary float-right" id="claimReward">Claim</button>
                                 <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Periode</th>
                    <th>Jumlah Omset</th>
                    <th>Jumlah Bintang</th>
                    <th>Jumlah Reward</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_claim" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Claim Reward</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="form-group row">
                            <label class="col-5">Total Omset</label>
                            <label class="col-1">:</label>
                            <label class="col-6" name="penjualan"></label>
                        </div>
                        <div class="form-group row">
                            <label class="col-5">Omset diakui</label>
                            <label class="col-1">:</label>
                            <label class="col-6" name="diakui"></label>
                        </div>
                        <div class="form-group row">
                            <label class="col-5">Potensi Bintang</label>
                            <label class="col-1">:</label>
                            <label class="col-6" name="bintang"></label>
                        </div>
                        <div class="form-group row">
                            <label class="col-5">Potensi reward</label>
                            <label class="col-1">:</label>
                            <label class="col-6" name="jumlah_reward"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="claim_btn" type="button" class="btn btn-primary">Claim</button>
                </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_search_produk"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" id="tambahan_url" value="<?=base_url()?>reward-reseller/list-tambahan" name="">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control" id="generalSearch">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped- table-hover table-checkable" id="tambahan-table">
                    <thead>
                    <tr>
                        <th>Nama Reward</th>
                        <th>Jumlah Bintang</th>
                        <th width="60">Aksi</th>

                    </tr>
                    </thead>
                    <tbody id="reward_tambahan_child"></tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



