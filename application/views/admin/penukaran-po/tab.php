<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='penukaran-po') ? 'active' : '')?>" href="<?=base_url()?>penukaran-po">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Penukaran Produk</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='history-penukaran-po') ? 'active' : '')?>" href="<?=base_url()?>history-penukaran-po">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">History</span>
            </a>
        </li>
    </ul>
</div>