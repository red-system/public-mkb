<div class="kt-portlet__head-toolbar">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='manage-bonus') ? 'active' : '')?>" href="<?=base_url()?>manage-bonus">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Bonus List</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='manage-bonus-proses') ? 'active' : '')?>" href="<?=base_url()?>manage-bonus-proses">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Bonus Ter-proses</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='manage-bonus-terkirim') ? 'active' : '')?>" href="<?=base_url()?>manage-bonus-terkirim">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Bonus Terkirim</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='manage-bonus-harian') ? 'active' : '')?>" href="<?=base_url()?>manage-bonus-harian">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Bonus Harian</span>
            </a>
        </li>
    </ul>
</div>