						<form action="<?=base_url()?>po-produk/save-add" method="post" id="kt_add" enctype="multipart/form-data">
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-subheader__main">

									<h3 class="kt-subheader__title">Order Produk</h3>
									<span class="kt-subheader__separator kt-hidden"></span>
									<div class="kt-subheader__breadcrumbs">
										<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="<?=base_url()?>produksi" class="kt-subheader__breadcrumbs-link">Order Produk</a>
										<input type="hidden" id="base_url" name="" value="<?=base_url()?>">
                                        <input type="hidden" id="first_deposit" value="<?=$reseller->first_deposit?>">
                                        <input type="hidden" id="minimal_po" value="<?=$minimal_po?>">
                                        <input type="hidden" id="first_termin" value="<?=$first_termin?>">
                                        <input type="hidden" id="is_frezze" value="<?=$is_frezze?>">
                                        <input type="hidden" id="nominal_frezze" value="<?=$termin?>">
                                        <input type="hidden" id="user_role" value="<?=$_SESSION["redpos_login"]["user_role_id"]?>">
									</div>

								</div>
							</div>
							<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__head kt-portlet__head--lg">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												Tambah Order Produk
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="modal-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Kode Order Produk</label>
														<div class="col-9">
															<input type="hidden" id="input_po_no" name="po_produk_no" value="<?=$po_produk_no?>">
															<label name="produksi_kode" id="display_po_no" class="col-form-label"><?=$po_produk_no?></label>
														</div>
													</div>
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-3 col-form-label">Alamat Pengiriman</label>
                                                        <div class="col-9">
                                                            <textarea class="form-control" rows="3" name="alamat_pengiriman"><?=$reseller->outlet_alamat?></textarea>
                                                        </div>
                                                    </div>
												</div>
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
														<div class="col-9">
															<textarea class="form-control" rows="3" name="keterangan"></textarea>
														</div>
													</div>															
												</div>
												<div class="col-md-8"><h5><strong>List Produk</strong></h5></div>
												<div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
											</div>
											<div class="row" style="margin-top: 20px">
												<div class="col-md-12" id="item-container">
													<?php $this->load->view('admin/po-produk/history_list'); ?>
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__body">
										<div class="row">
											<div class="col-md-6">
                                                <strong style="display: none"><span class="total" id="total_item">0</span></strong>
                                                <input type="hidden" name="total_item" id="input_total_item" class="" value="0">
                                                <input type="hidden" name="tambahan" id="tambahan" class="" value="0">
                                                <input type="hidden" name="potongan" id="potongan" class="" value="0">
												<div class="form-group kt-form__group row">
													<label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
													<div class="col-8 col-form-label">
														<input type="hidden" id="input_grand_total" name="grand_total" value="<?=$grand_total?>">
														<strong><span class="" id="grand_total"><?=number_format($grand_total)?></span></strong>
													</div>
												</div>
											</div>
                                            <?php if($reseller->first_deposit=="0") { ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Termin</label>
                                                        <div class="kt-radio-inline">
                                                            <label class="kt-radio">
                                                                <input type="radio" id="terminTidak" name="termin" value="0" checked> Tidak
                                                                <span></span>
                                                            </label>
                                                            <label class="kt-radio">
                                                                <input type="radio" id="terminYa" name="termin" value="1"> Iya
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
										</div>

									</div>
								</div>									
							</div>
							<div class="modal" id="kt_modal_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Data Produk</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<input type="hidden" id="list_produk" value="<?=base_url()?>po-produk/utility/list-produk">
												<div class="col-md-12">
													<table class="table table-striped- table-hover table-checkable" id="produk-table">
														<thead>
															<tr>
																<th>Kode Produk</th>
																<th>Nama Produk</th>
																<th width="60">Aksi</th>
															</tr>
														</thead>
														<tbody id="produk_child"></tbody>
													</table>												
												</div>
											</div>


										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>		
							<div class="pos-floating-button">
								<a href="<?=base_url()?>po-produk" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
									<span>
										<i class="la la-angle-double-left"></i>
										<span>Kembali ke Daftar</span>
									</span>
								</a>
								<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
							</div>				

						</form>