
<input type="hidden" id="type" value="retur" name="">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Recipe Form</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>produk-recipe" class="kt-subheader__breadcrumbs-link">Recipe</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=current_url()?>" class="kt-subheader__breadcrumbs-link">Recipe Form</a>
            <input type="hidden" id="last-no" value="<?=$last_no?>">
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Recipe
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">

            </div>
        </div>
        <form class="form-send" action="<?=base_url()."produk-recipe/save"?>" method="post" data-redirect="<?=base_url()."produk-recipe"?>">
            <div class="kt-portlet__body">
                <div class="col-12 row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-form-label col-3">Produk</label>
                            <div class="input-group col-9">
                                <input type="text" class="form-control" readonly="" id="produk_nama" value="<?=($produk_recipe==null ? '' : $produk_recipe->produk_nama)?>">
                                <input type="hidden" class="required" name="produk_id" id="produk_id" required value="<?=($produk_recipe==null ? '' : $produk_recipe->produk_id)?>">
                                <input type="hidden" name="produk_recipe_id" value="<?=$produk_recipe_id?>">
                                <div class="input-group-append"><button class="btn btn-primary produk-search" type="button" ><i class="flaticon-search"></i></button></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Total Pokok</label>
                            <div class="col-9">
                                <label class="col-form-label col-1">: </label>
                                <label class="col-form-label col-9" id="total_pokok"><?=($produk_recipe==null ? 'Rp. 0' : $produk_recipe->total_pokok_lbl)?></label>
                                <input type="hidden" id="input_total_pokok" name="total_pokok" value="<?=($produk_recipe==null ? '0' : $produk_recipe->total_pokok)?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-primary pull-right" id="add-item"><i class="fa fa-plus"></i> Tambah Bahan</button>
                    </div>

                </div>
                <div class="col-12">
                    <table class="table table-bordered table-hover table-checkable" >
                        <thead>
                        <tr>
                            <th width="30%">Nama Bahan</th>
                            <th width="15%">Satuan</th>
                            <th width="15%">Jumlah</th>
                            <th width="15%">Harga Pokok</th>
                            <th width="15%">Keterangan</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="view_child_data">
                            <?php
                                foreach ($produk_recipe_detail as $item){
                                    ?>
                                    <tr id="row-<?=$item->produk_recipe_detail_id?>">
                                        <td>
                                            <input type="hidden" id="bahan_id_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>" name="recipe[_<?=$item->produk_recipe_detail_id?>][bahan_id]" value="<?=$item->bahan_id?>">
                                            <div class="input-group">
                                                <input type="text" class="form-control" readonly="" id="bahan_nama_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>" value="<?=$item->bahan_nama?>">
                                                <div class="input-group-append">
                                                    <button data-no="<?=$item->produk_recipe_detail_id?>" class="btn btn-primary bahan-search" type="button"><i class="flaticon-search"></i></button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <select class="form-control unit" data-no="<?=$item->produk_recipe_detail_id?>" id="satuan_<?=$item->produk_recipe_detail_id?>" name="recipe[_<?=$item->produk_recipe_detail_id?>][satuan_id]">
                                                <?php
                                                    $satuan_id = explode('|',$item->unit_id);
                                                    $satuan_nama = explode('|',$item->unit_name);
                                                    $satuan_konversi = explode('|',$item->unit_konversi);
                                                     $konversi = 1;
                                                    foreach ($satuan_id as $key=>$value){
                                                        if($satuan_id[$key]==$item->satuan_id){
                                                            $konversi = $satuan_id[$key];
                                                        }
                                                        ?>
                                                        <option value="<?=$satuan_id[$key]?>" <?=($satuan_id[$key]==$item->satuan_id?'selected':'')?> data-konversi="<?=$satuan_konversi[$key]?>"><?=$satuan_nama[$key]?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-numeral" id="jumlah_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>" name="recipe[_<?=$item->produk_recipe_detail_id?>][takaran]" value="<?=number_format($item->takaran)?>">
                                            <input type="hidden" id="konversi_takaran_id_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>" name="recipe[_<?=$item->produk_recipe_detail_id?>][konversi_takaran]" value="<?=$konversi?>">
                                        </td>
                                        <td>
                                            <input type="hidden" class="harga-pokok" id="harga_pokok_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>" name="recipe[_<?=$item->produk_recipe_detail_id?>][harga_pokok]" value="<?=$item->harga_pokok?>">
                                            <input type="hidden" class="harga-pokok-statis" id="harga_statis_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>"  value="<?=$item->bahan_harga?>">
                                            <span id="harga_pokok_label_<?=$item->produk_recipe_detail_id?>" data-no="<?=$item->produk_recipe_detail_id?>">Rp. <?=(number_format($item->harga_pokok,2,'.',','))?></span>
                                        </td>
                                        <td>
                                            <textarea class="form-control" name="recipe[_<?=$item->produk_recipe_detail_id?>][keterangan]"></textarea>
                                        </td>
                                        <td>
                                            <button type="button" data-no="<?=$item->produk_recipe_detail_id?>" class="btn btn-danger center delete-btn"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <a href="<?=base_url()."produk-recipe"?>" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i>Save</button>
            </div>
        </form>
    </div>
</div>
<div class="modal" id="kt_modal_search_product"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="produk_url" value="<?=base_url()?>produk-recipe/list-produk" name="">
            <div style=" display: none;" id="produk_column"><?=json_encode(array("data"=>"no","data"=>"produk_kode","data"=>"produk_nama"))?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="produk_columnDef"><?=json_encode(array("className"=>"text__right","targets"=>array(0)))?></div>
            <?php } ?>
            <div style="display: none;" id="produk_action"><?=json_encode(array("chose"=>true))?></div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control" id="produkSearch">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="datatable table table-striped- table-hover table-checkable" >
                    <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jenis</th>
                        <th width="200">Action</th>
                    </tr>
                    </thead>
                    <tbody id="produk_data_ajax"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_search_bahan"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" id="bahan_url" value="<?=base_url()?>produk-recipe/list-bahan" name="">
            <div style=" display: none;" id="produk_column"><?=json_encode(array("data"=>"no","data"=>"produk_kode","data"=>"produk_nama"))?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="produk_columnDef"><?=json_encode(array("className"=>"text__right","targets"=>array(0)))?></div>
            <?php } ?>
            <div style="display: none;" id="produk_action"><?=json_encode(array("chose"=>true))?></div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control" id="bahanSearch">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="bahan-datatable table table-striped- table-hover table-checkable" >
                    <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jenis</th>
                        <th width="200">Action</th>
                    </tr>
                    </thead>
                    <tbody id="bahan_data_ajax"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

