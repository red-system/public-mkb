<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Notifikasi MKB</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>notification" class="kt-subheader__breadcrumbs-link">Notifikasi MKB</a>

        </div>

    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <h3 class="kt-portlet__head-title">
                        Notifikasi
                    </h3>
                </div>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>notification/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>

                <div style="display: none;" id="table_action" data-style="dropdown"><?=(isset($action) ? $action : "")?>
                </div>

            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <?php $akses = json_decode($action,true);if(isset($akses["add"])&&$akses["add"]&&$this->uri->segment(1)=="notification"){ ?>
                        <div class="dropdown dropdown-inline pull-right">
                            <a href="<?=base_url()?>notification/add" class="btn btn-brand btn-icon-sm">
                                <span>
                                    <i class="flaticon2-plus"></i>
                                    <span>Tambah Data</span>
                                </span>
                            </a>
                        </div>
                        <?php } ?>
                        <!-- <div class="dropdown dropdown-inline pull-right">
                            <a href="<?=base_url()?>notification/add" class="btn btn-brand btn-icon-sm">
                                <span>
                                    <i class="flaticon2-plus"></i>
                                    <span>Tambah Data</span>
                                </span>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable">
                <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Deskripsi</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal" id="kt_modal_detail_notif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Notifikasi MKB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Tanggal</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-9">
                                <label name="notif_date" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Judul</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-9">
                                <label name="notif_title" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Deskripsi</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-9">
                                <label name="notif_description" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>