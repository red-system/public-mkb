
<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Penukaran Retur</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>pos" class="kt-subheader__breadcrumbs-link">Penukaran Retur</a>
            <input type="hidden" id="penukaran">
            <input type="hidden" id="penukaran_link">
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
        </div>
        <form action="<?=base_url()?>penukaran-retur-agen/check" method="post" class="form-save-penukaran" novalidate>
            <div class="kt-portlet__body">

                <div class="row" id="guest-form">


                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Kode Penukaran </label>
                            <div class="input-group col-10">
                                <input type="text" name="po_kode" id="po_kode" class="form-control" value="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <button type="submit" class="btn btn-primary pull-right" ><i class="flaticon2-safe"></i> Save</button>
                </div>

            </div>

        </form>
    </div>
</div>
<div class="modal" id="kt_modal_detail_voucher" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Voucher</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Voucher No</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="voucher_code" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Retur No</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="no_retur" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Ditukarkan di</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="ditukarkan" class="col-form-label"></label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5 style="text-align: center"><strong>Data Item</strong></h5>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered table-hover table-checkable" >
                            <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Jumlah</th>
                                <th>Stock Saat ini</th>

                            </tr>
                            </thead>
                            <tbody id="view_child_data">

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="process_penukaran" >Process</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

