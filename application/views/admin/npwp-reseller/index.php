<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Request Reseller</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>npwp-reseller" class="kt-subheader__breadcrumbs-link">Reseller</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>npwp-reseller" class="kt-subheader__breadcrumbs-link">Request Reseller</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">


            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>npwp-reseller/list" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <?=$this->load->view('admin/npwp-reseller/tab') ?>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Nama</th>
                    <th>No HP</th>
                    <th>Alamat Tinggal</th>
                    <th>No NPWP</th>
                    <th>Penghasilan Lain</th>
                    <th>Pekerjaan</th>
                    <th width="200">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Reseller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Nama</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="nama" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">No Hp</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="phone" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">E-mail</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="email" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">No KTP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="no_ktp" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Alamat Tinggal</label>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Propinsi</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="province" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kabupaten</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="city" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kecamatan</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="subdistrict_name" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="alamat" class="col-form-label"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Alamat Outlet</label>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Propinsi</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_province" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kabupaten</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_city" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kecamatan</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_subdistrict_name" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_alamat" class="col-form-label"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto Diri</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="link_foto_profile" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto KTP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="link_foto_ktp" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto Selfie KTP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="link_selfi_ktp" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_accept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pilihan Case</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>npwp-reseller/approved" method="post" id="kt_form_approve_npwp">
            <div class="modal-body">
                <div class="col-md-12 row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="npwp_id" value="">
                            <label for="example-text-input" class="col-3 col-form-label">Punya NPWP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="punya_npwp_lbl" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">NPWP Sendiri</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="atasnama_sendiri_lbl" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jenis Kelamin</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="kelamin" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">No NPWP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="no_npwp" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto NPWP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <img src=""  width="200" id="foto_npwp" height="auto" data-field="foto_npwp" class="img-preview">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto Selfi NPWP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <img src=""  width="200" id="foto_selfi" height="auto" data-field="foto_selfi" class="img-preview">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Mempunyai Pekerjaan Tetap Lain</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="penghasilan_lain_lbl" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Status Pernikahan</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="status_pernikahan" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Mempunyai Tanggungan</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="punya_tanggungan_lbl" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jumlah Tanggungan</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="jumlah_tanggungan" class="col-form-label"></label>
                            </div>
                        </div>
                         <div class="form-group">
                                    <input type="hidden" name="ptkp_lbl" id="ptkp_lbl">
									<label class="form-control-label ">Case PPH <b class="label--required">*</b></label>
									<select class="form-control col-md-12" name="case" required="" id="select-case">
										<option value="1">Case 1</option>
                                        <option value="3">Case 3</option>
									</select>
							</div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">PTKP/bulan</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control input-numeral" name="ptkp" id="input_ptkp">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-primary" id="kt_submit_approve">Approve</button>
                <button type="button"  class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="kt_modal_refuse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <form method="post" id="kt_form_refuse" action="<?=base_url()?>npwp-reseller/refuse">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">NPWP Reseller</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="npwp_id" id="reseller_id_refuse">
                        <div class="form-group col-12">
                            <label class="col-form-label">Alasan Ditolak</label>
                            <textarea class="form-control" name="alasan_banned" required></textarea>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="kt_refuse_submit_btn" >Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>