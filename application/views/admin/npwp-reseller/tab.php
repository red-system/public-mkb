<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='npwp-reseller') ? 'active' : '')?>" href="<?=base_url()?>npwp-reseller">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Request Reseller</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='npwp-reseller-approve') ? 'active' : '')?>" href="<?=base_url()?>npwp-reseller-approve">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">
                    Approve NPWP
                </span>
            </a>
        </li>
    </ul>
</div>