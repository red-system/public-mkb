<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Request Reseller</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>npwp-reseller-approve" class="kt-subheader__breadcrumbs-link">Reseller</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>npwp-reseller-approve" class="kt-subheader__breadcrumbs-link">Request Reseller</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">


            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>npwp-reseller-approve/list" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <?=$this->load->view('admin/npwp-reseller/tab') ?>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <button id="btn_add" type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
                                    <i class="flaticon2-plus"></i> Tambah Data
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable table-approved" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Nama</th>
                    <th>No HP</th>
                    <th>Alamat Tinggal</th>
                    <th>No NPWP</th>
                    <th>Penghasilan Lain</th>
                    <th>Pekerjaan</th>
                    <th>Case</th>
                    <th width="200">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Reseller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Nama</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="nama" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">No Hp</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="phone" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">E-mail</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="email" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">No KTP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="no_ktp" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Alamat Tinggal</label>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Propinsi</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="province" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kabupaten</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="city" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kecamatan</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="subdistrict_name" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="alamat" class="col-form-label"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Alamat Outlet</label>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Propinsi</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_province" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kabupaten</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_city" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Kecamatan</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_subdistrict_name" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row col-12">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="outlet_alamat" class="col-form-label"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto Diri</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="link_foto_profile" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto KTP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="link_foto_ktp" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Foto Selfie KTP</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <a href="" name="link_selfi_ktp" target="__blank" class="col-form-label btn btn-primary">View</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_reseller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Data Reseller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="list_reseller" value="<?=base_url()?>npwp-reseller-approve/list-reseller">
                    <div class="col-md-12">
                        <table class="table table-striped- table-hover table-checkable table-reseller" id="reseller-table">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Tipe</th>
                                <th width="60">Aksi</th>
                            </tr>
                            </thead>
                            <tbody id="reseller_child"></tbody>
                        </table>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_add" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah NPWP Reseller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>npwp-reseller-approve/add" method="post" id="kt_add_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="form-control-label ">Reseller<b class="label--required">*</b></label>
                                    <div class="input-group col-12" style="padding: 0px">
                                        <input type="text" class="form-control readonly" name="nama" id="add_reseller_nama" required="" autocomplete="off">
                                        <input type="hidden" class="form-control row_id" id="add_reseller_id" name="reseller_id">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary reseller-search" data-modal="add" type="button" data-toggle="modal" data-no=""><i class="flaticon-search"></i></button>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">No NPWP<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="no_npwp" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Foto NPWP <b class="label--required">*</b></label>
                                    <div></div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input img-input"
                                               data-display="displayImg2" name="foto_npwp"
                                               accept="image/x-png,image/gif,image/jpeg"
                                               id="customFile2" required="">
                                        <label class="custom-file-label selected" for="customFile"
                                               id="labelCustomFile"></label>
                                        <input type="hidden" name="input_foto_npwp">
                                    </div>
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 150px;">
                                        <img src="<?= base_url() ?>assets/media/no_image.png" alt=""
                                             id="displayImg2" width="200" height="150">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="checkbox" name="penghasilan_lain" value="1">
                                    <label for="vehicle1"> Penghasilan lain</label>

                                    <div class="form-group" id="penghasilan_text" style="display : none">
                                        <input type="hidden" name="input_penghasilan_lain" id="penghasilan_val" value="false">
                                        <label>Keterangan penghasilan lain <b class="label--required">*</b></label>
                                        <textarea class="form-control" name="keterangan_lain"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Case <b class="label--required">*</b></label>
                                <label class="form-control-label ">Case <b class="label--required">*</b></label>
                                <div>
                                    <input type="radio" id="1" name="case" value="1"
                                           checked>
                                    <label for="1">1</label>
                                </div>

                                <div>
                                    <input type="radio" id="3" name="case" value="3">
                                    <label for="3">3</label>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit NPWP Reseller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>npwp-reseller-approve/edit" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="npwp_id">
                            <div class="form-group ">
                                <label class="form-control-label ">Reseller<b class="label--required">*</b></label>
                                <div class="input-group col-12" style="padding: 0px">
                                    <input type="text" class="form-control readonly" name="nama" id="edit_reseller_nama" required="" autocomplete="off">
                                    <input type="hidden" class="form-control row_id" id="edit_reseller_id" name="reseller_id">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary reseller-search" data-modal="edit" type="button" data-toggle="modal" data-no=""><i class="flaticon-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">No NPWP<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="no_npwp" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Foto NPWP <b class="label"></b></label>
                                    <div></div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input img-input"
                                               data-display="displayImg3" name="label_foto_npwp"
                                               accept="image/x-png,image/gif,image/jpeg"
                                               id="customFile3">
                                        <label class="custom-file-label selected" for="customFile3"
                                               id="labelCustomFile"></label>
                                        <input type="hidden" name="input_foto_npwp">
                                        <input type="hidden" name="data_foto">
                                        <input type="hidden" name="link_foto" value="http://dev-storage.redsystem.id/redpos/">
                                        <input type="hidden" name="link_base" value="<?= base_url() ?>assets/media/">
                                    </div>
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 150px;">
                                        <img src="<?= base_url() ?>assets/media/no_image.png" alt=""
                                             id="displayImg3" width="200" height="150">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="checkbox" name="penghasilan_lain" value="1">
                                    <label for="vehicle1"> Penghasilan lain</label>

                                    <div class="form-group" id="penghasilan_text" style="display : none">
                                        <input type="hidden" name="input_penghasilan_lain" id="penghasilan_val" value="false">
                                        <input type="hidden" name="input_penghasilan_lain_base" value="">
                                        <input type="hidden" name="input_keterangan_lain_base" value="">
                                        <label>Keterangan penghasilan lain <b class="label--required">*</b></label>
                                        <textarea class="form-control" name="keterangan_lain"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Case <b class="label--required">*</b></label>
                                <div>
                                    <input type="radio" id="1" name="case" value="1"
                                           checked>
                                    <label for="1">1</label>
                                </div>

                                <div>
                                    <input type="radio" id="3" name="case" value="3">
                                    <label for="3">3</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_update_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

