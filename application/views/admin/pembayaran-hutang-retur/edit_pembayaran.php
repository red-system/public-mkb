<form action="<?=base_url()?>pembayaran-hutang-produk/save-edit" method="post" id="kt_add" enctype="multipart/form-data">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">Order Produk</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>pembayaran-hutang-produk" class="kt-subheader__breadcrumbs-link">Hutang Piutang</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>pembayaran-hutang-produk" class="kt-subheader__breadcrumbs-link">Pembayaran Hutang Produk</a>
                <input type="hidden" id="base_url" name="" value="<?=base_url()?>">
                <input type="hidden" id="last_number" value="<?=$size?>">
                <input type="hidden" name="pembayaran_hutang_produk_id" value="<?=$pembayaran_hutang_produk_detail[0]->pembayaran_hutang_produk_id?>">

            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Tambah Order Produk
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Super Agen</label>
                                <div class="col-9">
                                    <div class="input-group col-12">
                                        <input type="text" class="form-control readonly" name="reseller_nama" id="reseller_nama_lbl" required="" value="<?=$pembayaran_hutang_produk_detail[0]->nama_reseller?>" autocomplete="off">
                                        <input type="hidden" class="form-control" id="reseller_id" name="reseller_id" value="<?=$pembayaran_hutang_produk_detail[0]->reseller_id?>">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary reseller-search" type="button" data-toggle="modal" ><i class="flaticon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                                <div class="col-9">
                                    <input type="text" class="form-control tanggal" autocomplete="off" name="tanggal" value="<?=$pembayaran_hutang_produk_detail[0]->tanggal?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8"><h5><strong>List Hutang</strong></h5></div>
                        <div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
                    </div>
                    <div class="row" style="margin-top: 20px">
                        <table class="table table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="20%">No Po</th>
                                <th width="20%">Kode Voucher</th>
                                <th width="20%">Nama Produk</th>
                                <th width="20%">Jumlah</th>
                                <th width="20%">Aksi</th>
                            </tr>
                            </thead>
                            <tbody id="item-container">
                                <?php
                                    $i = 0;
                                    foreach ($pembayaran_hutang_produk_detail as $item){
                                        ?>
                                        <tr class="" id="produk_item_<?=$i?>">
                                            <td>
                                                <label for="example-text-input" id="item_po_produk_no_<?=$i?>" class="col-12 col-form-label"><?=$item->po_produk_no?></label>
                                            </td>
                                            <td>
                                                <label for="example-text-input" id="item_voucher_no_<?=$i?>" class="col-12 col-form-label"><?=$item->voucher_code?></label>
                                            </td>
                                            <td>
                                                <label for="example-text-input" id="item_nama_produk_no_<?=$i?>" class="col-12 col-form-label"><?=$item->produk_nama?></label>
                                            </td>
                                            <td>
                                                <label for="example-text-input" id="item_jumlah_no_<?=$i?>" class="col-12 col-form-label"><?=$item->jumlah?></label>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="hidden" name="hutang_produk_id[]" id="hutang_produk_id_no_<?=$i?>" value="<?=$item->hutang_produk_id?>">
                                                <button type="button" class="btn btn-danger btn-sm delete-produk" data-no="<?=$i?>" data-item-produk="produk_item_<?=$i?>"><i class="flaticon2-trash"></i></button>
                                                <div id="check_no_<?=$i?>" style="display: none">
                                                    <div id="hutang_no_<?=$item->hutang_produk_id?>"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                ?>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="modal" id="kt_modal_hutang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="list_hutang" value="<?=base_url()?>pembayaran-hutang-produk/utility/list-hutang">
                        <div class="col-md-12">
                            <table class="table table-striped- table-hover table-checkable" id="hutang-table">
                                <thead>
                                <tr>
                                    <th>No Po</th>
                                    <th>Nama Produk</th>
                                    <th>Jumlah</th>
                                    <th width="60">Aksi</th>
                                </tr>
                                </thead>
                                <tbody id="hutang_child"></tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="kt_modal_reseller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Data Reseller</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="list_reseller" value="<?=base_url()?>pembayaran-hutang-produk/utility/list-reseller">
                        <div class="col-md-12">
                            <table class="table table-striped- table-hover table-checkable" id="reseller-table">
                                <thead>
                                <tr>
                                    <th>Referal Kode</th>
                                    <th>Nama Reseller</th>
                                    <th width="60">Aksi</th>
                                </tr>
                                </thead>
                                <tbody id="reseller_child"></tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="pos-floating-button">
        <a href="<?=base_url()?>po-produk" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
									<span>
										<i class="la la-angle-double-left"></i>
										<span>Kembali ke Daftar</span>
									</span>
        </a>
        <button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
    </div>

</form>