<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
    <?php if($warning_redeposit) {
        ?>
        <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
            <div class="kt-portlet__head ">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">

                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h3></h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <h1 style="text-align: center"><?=$warning_message?></h1>
            </div>
        </div>
        <?php
    }
    ?>
<?php if ($status_latlon == true) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__body my-5" style="text-align: center;">
                    <h1 style="text-align: center">Saat ini tim IT sedang mengembangkan sistem untuk menampilkan
                        koordinat dari outlet Reseller/Super Reseller. Silahkan update Koordinat Lokasi Outlet anda.
                    </h1>
                    <div class="row mt-5">
                        <div class="col-md-12 d-flex justify-content-center">
                            <a href="<?php echo base_url() ?>maps" class="btn btn-success btn-pill">Update Koordinat
                                Lokasi Outlet</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

<div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body ">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-md-12 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="start_date" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="1" value="<?=$min_tanggal?>" data-field="tanggal_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="end_date" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="1" value="<?=$max_tanggal?>" data-field="tanggal_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-info akses-filter_data">
                                <i class="la la-search"></i> Filter Data
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="btn-group btn-group btn-pill btn-group-sm">


            </div>
        </div>
    </div>


    <div class="col-md-12 row">
<!--  waiting to display  -->
<!--   start edit here     -->
        <?php
            if(date("Y-m-d")<=$this->config->item('tanggal_akhir_thailand')){
                ?>
                <div class="col-md-12">
                    <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                        <div class="kt-portlet__head ">
                            <div class="kt-portlet__head-label w-100">
                                <h3 class="kt-portlet__head-title text-center w-100">
                                    Reward Thailand
                                </h3>
                            </div>

                        </div>
                        <div class="kt-portlet__body row">
                            <div class="col-md-12 row">
                                <div class="col-md-6">
                                    <div class="w-100 p-2">
                                        <h4 class="text-center w-100"> Total Poin </h4>
                                    </div>
                                    <div class="w-100 p-2">
                                        <h4 class="text-center w-100"> <?=number_format($thailand_poin)?> </h4>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="w-100 p-2">
                                        <h4 class="text-center w-100"> Reseller/Super Reseller Baru </h4>
                                    </div>
                                    <div class="w-100 p-2">
                                        <h4 class="text-center w-100"> <?=number_format($thailand_join)?> </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        ?>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Bintang
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h1><?=$rangking?></h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Omset Group  <?=$startMonthLbl.'('.$startYear.') - '. $curMonthLbl.'('.$curYear.')'?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($omset_group)?></h2>
                </div>
            </div>
        </div>


<!-- end waiting to display   -->

<!--        <div class="col-md-4">-->
<!--            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-success">-->
<!--                <div class="kt-portlet__head ">-->
<!--                    <div class="kt-portlet__head-label">-->
<!--                        <h3 class="kt-portlet__head-title">-->
<!--                            Omset referensi-->
<!--                        </h3>-->
<!--                    </div>-->
<!--                    <div class="kt-portlet__head-toolbar">-->
<!--                        <h3></h3>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="kt-portlet__body" style="text-align: center;">-->
<!--                    <h2>Rp. --><?//=number_format($total_po)?><!--</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Reseller Referensi
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($ref_res)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-success">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           Super Reseller Referensi
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=number_format($ref_sup)?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Peringkat Reward Tahunan
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=$annual_rank?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-primary">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Jumlah Re-deposit Tahun Ini
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=number_format($qty_deposit)?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <?php
                    $sufix = '';
                    if($this->input->get('start_date')!=''){
                        $sufix = '?start_date='.$this->input->get('start_date').'&end_date='.$this->input->get('end_date');
                    } ?>
                    <input type="hidden" id="rekap_performa_group_url" value="<?=base_url()."home/performa_group".$sufix?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Rekap Performa Group <?=($this->input->get('start_date')!="")?$this->input->get('start_date'):date('Y-m-01')?>
                             s/d <?=($this->input->get('end_date')!="")?$this->input->get('end_date'):date('Y-m-t')?>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="rekap-performa-group-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Downline</th>
                            <th>Bonus</th>
                            <th>PO Produk</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <?php
                    $sufix = '';
                    if($this->input->get('start_date')!=''){
                        $sufix = '?start_date='.$this->input->get('start_date').'&end_date='.$this->input->get('end_date');
                    } ?>
                    <input type="hidden" id="rekap_performa_super_url" value="<?=base_url()."home/performa_super".$sufix?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Rekap Performa Downline Super Res <?=($this->input->get('start_date')!="")?$this->input->get('start_date'):date('Y-m-01')?>
                            s/d <?=($this->input->get('end_date')!="")?$this->input->get('end_date'):date('Y-m-t')?>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="rekap-performa-super-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Total PO (Rupiah)</th>
                            <th>Penukaran Voucher (kali)</th>
                            <th>Withdraw (kali)</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
<!-- waiting to display       -->
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="reward_url" value="<?=base_url()."home/reward"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Potensi reward
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="reward-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Bintang</th>
                            <th>Omset Group</th>
                            <th>Potensi Reward</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="leader_board_url" value="<?=base_url()."home/leader-board"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Annual Reward Leader Board
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="leader_board_table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>
                            <th>Omset</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="rekap_url" value="<?=base_url()."home/rekap-bulanan"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Rekap Bulan <?=$startMonthLbl.'('.$startYear.') - '. $curMonthLbl.'('.$curYear.')'?>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="rekap-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Tahun</th>
                            <th>Bulan</th>
                            <th>PO</th>
                            <th>Group</th>
                            <th>Claimed</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<!--  end waiting to display      -->



    </div>
</div>
<?php
if($npwp==0){
?>
    <div class="modal" id="kt_npwp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="<?=base_url()?>npwp-home-save" method="post" id="kt_npwp_form">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">NPWP</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Halo Kak, berkenaan dengan system pemotongan pajak dalam pengiriman bonus, kami dari team mkb perlu beberapa data dari kakak :)</h5>
                            </div>
                            <div class="col-md-12" style="margin-top: 20px">
                                <?php
                                if($jenis_kelamin!=""){
                                    ?>
                                    <input type="hidden" id="jenis_kelamin" value="<?=$jenis_kelamin?>">
                                    <?php
                                }else{
                                    ?>
                                    <div class="form-group">
                                        <label class="form-control-label ">Jenis Kelamin</label>
                                        <div class="kt-radio-inline">
                                            <label class="kt-radio">
                                                <input type="radio" name="kelamin" value="perempuan" id="rb-perempuan" checked> Perempuan
                                                <span></span>
                                            </label>
                                            <label class="kt-radio">
                                                <input type="radio" name="kelamin" value="laki-laki"  id="rb-laki-laki"> Laki-laki
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>;
                                    <?php
                                }
                                ?>
                                <div class="form-group">
                                    <label class="form-control-label ">Apakah Kakak punya NPWP (atasnama sendiri maupun atasnama suami/istri) ?</label>
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio">
                                            <input type="radio" name="punya_npwp" value="1" id="rb-punya"> Ya
                                            <span></span>
                                        </label>
                                        <label class="kt-radio">
                                            <input type="radio" name="punya_npwp" value="0" checked id="rb-tidak-punya"> Tidak
                                            <span></span>
                                        </label>
                                    </div>

                                </div>
                                <div id="ada_npwp" style="display: none">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="kt_npwp_submit" type="button" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
}

?>

