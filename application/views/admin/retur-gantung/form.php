<!-- begin:: Content -->
<style type="text/css">
    .select2-container .select2-selection--single{
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        position: fixed;
    }
</style>
<input type="hidden" id="type" value="retur" name="">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Retur Form</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>retur-gantung" class="kt-subheader__breadcrumbs-link">Retur Gantung</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=current_url()?>" class="kt-subheader__breadcrumbs-link">Retur Form</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Retur
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">

            </div>
        </div>
        <form class="form-send" action="<?=base_url()."retur-gantung/save"?>" method="post" data-redirect="<?=base_url()."retur-gantung"?>" data-alert-show="true" data-alert-field-message="Setelah diproses data tidak dapat dikembalikan lagi">
            <div class="kt-portlet__body">
                <div class="col-12 row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-form-label col-3">Tanggal Retur</label>
                            <input class="form-control col-9 tanggal" autocomplete="off" name="tanggal" required value="<?=($retur_gantung!=null?$retur_gantung->tanggal:'')?>">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Lokasi</label>
                            <select class="form-control col-9" name="lokasi_id" id="id_lokasi">
                                <?php
                                foreach ($lokasi as $item){
                                    ?>
                                    <option value="<?=$item->lokasi_id?>" <?=($retur_gantung==null?'':($item->lokasi_id==$retur_gantung->lokasi_id?'selected':''))?>><?=$item->lokasi_nama?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-form-label col-3">Suplier</label>
                            <label class="col-form-label col-1" style="text-align: center">:</label>
                            <div class="typeahead col-8">
                                <input class="form-control searchInput" id="kt_typeahead_1" name="suplier_nama" data-col-index="1" type="text" dir="ltr" value="<?=($retur_gantung!=null?$retur_gantung->suplier_nama:'')?>">
                            </div>
                            <input type="hidden" name="input_po_id" data-name="po_id" value="<?=($retur_gantung!=null?$retur_gantung->po_produk_id:'')?>">
                            <input type="hidden" name="retur_gantung_id" value="<?=$id?>">
                        </div>

                    </div>

                </div>
                <div class="col-12" style="margin-bottom: 10px">
                    <button type="button" class="btn btn-primary float-right" id="add_item"><i class="fa fa-plus"></i>Tambah Item</button>
                </div>
                <div class="col-12">
                    <table class="table table-bordered table-hover table-checkable" >
                        <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Jumlah Stock</th>
                            <th>Jumlah Retur</th>
                            <th>Harga</th>
                            <th>Subtotal</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody id="view_child_data">
                        <?php
                        if($retur_gantung_detail!=null){
                            foreach ($retur_gantung_detail as $item){
                                ?>
                                <tr id="row_<?=$item->id?>">
                                    <td><?=$item->produk_nama?></td>
                                    <td id="jumlah_stock_<?=$item->id?>"><?=$item->jumlah_stock?></td>
                                    <td><input type="text" class="input-numeral form-control jumlah-retur" data-no="<?=$item->id?>" id="jumlah_retur_<?=$item->id?>" name="item[_<?=$item->id?>][jumlah_retur]" value="<?=number_format($item->jumlah)?>"></td>
                                    <td><input type="text" class="input-numeral form-control harga-retur" data-no="<?=$item->id?>" id="harga_retur_<?=$item->id?>" name="item[_<?=$item->id?>][harga_retur]" value="<?=number_format($item->harga)?>"></td>
                                    <td id="subtotal_<?=$item->id?>" class="subtotal">Rp. <?=number_format($item->sub_total_retur)?></td>
                                    <td><textarea class="form-control" data-no="<?=$item->id?>" id="keterangan_retur_<?=$item->id?>" name="item[_<?=$item->id?>][keterangan]"><?=$item->keterangan?></textarea>
                                        <input type="hidden" class="input-numeral form-control " data-no="<?=$item->id?>" id="produk_id_<?=$item->id?>" name="item[_<?=$item->id?>][produk_id]" value="<?=$item->produk_id?>">
                    <input type="hidden" class="input-numeral form-control " data-no="<?=$item->id?>" id="subtotal_retur_<?=$item->id?>" name="item[_<?=$item->id?>][subtotal]" value="<?=$item->sub_total_retur?>">
                                    </td>
                                    <td><button type="button" class="btn btn-danger float-right delete-item" data-no="<?=$item->id?>"><i class="fa fa-trash"></i></button></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4" style="text-align: right">Total Retur</td>
                            <td colspan="" id="total_retur">Rp. <?=($retur_gantung==null?0:number_format($retur_gantung->total_retur))?></td>
                            <td colspan=""><input type="hidden" id="input_total_retur" name="total_retur" value="<?=($retur_gantung==null?0:$retur_gantung->total_retur)?>"></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <a href="<?=base_url()."retur-gantung"?>" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i>Save</button>
            </div>
        </form>
    </div>
</div>
<div class="modal" id="kt_modal_search_po"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="no_item" value="<?=$no?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>retur-gantung/list-po" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control" id="generalSearch">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="datatable table table-striped- table-hover table-checkable">
                    <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Produk</th>
                        <th>Kode</th>
                        <th>Jumlah</th>
                        <th width="200">Action</th>
                    </tr>
                    </thead>
                    <tbody id="child_data_ajax"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

