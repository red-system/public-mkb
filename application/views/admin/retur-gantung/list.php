<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Retur Gantung</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>fabric" class="kt-subheader__breadcrumbs-link">Retur Gantung</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
        <input type="hidden" id="type" value="list">
    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Retur
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>retur-gantung/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
                <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>
            <?php $akses = json_decode($action,true);if($akses["add"]){ ?>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="dropdown dropdown-inline">
                            <a href="<?=base_url().'retur-gantung/retur-form'?>" class="btn btn-brand btn-icon-sm">
                                <i class="flaticon2-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>PO No</th>
                    <th>Suplier</th>
                    <th>Tanggal</th>
                    <th>Total Retur</th>
                    <th>Staff</th>
                    <th width="200">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot>
                <tr>
                    <td colspan="4" style="text-align: right">Total</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail_retur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Retur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                            <div class="col-9">
                                <label name="tanggal" class="col-form-label" id="detail_tanggal"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Lokasi</label>
                            <div class="col-9">
                                <label name="jenis_produk_nama" class="col-form-label" id="detail_lokasi"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Po No</label>
                            <div class="col-9">
                                <label name="produk_minimal_stock" class="col-form-label" id="detail_po"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Suplier</label>
                            <div class="col-9">
                                <label name="created_at" class="col-form-label" id="detail_suplier"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Total retur</label>
                            <div class="col-9">
                                <label name="updated_at" class="col-form-label" id="detail_total"></label>
                            </div>
                        </div>
                    </div>
                    <h5>List Item</h5>
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover table-checkable" >
                            <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Harga</th>
                                <th>Jumlah Retur</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody id="view_child_data">
                            </tbody>
                        </table>
                    </div>
                    <h5 class="item_produk_display" style="display: none">List Item</h5>
                    <div class="col-md-12 item_produk_display" style="display: none">
                        <table class="table table-bordered table-hover table-checkable" >
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Produk</th>
                                <th>Jumlah Komposisi</th>
                            </tr>
                            </thead>
                            <tbody id="view_child_data_item">
                            <tr>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Potong Hutang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>retur-gantung/potong" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <input type="hidden" name="retur_gantung_id">
                                <input type="hidden" name="po_produk_id_old">
                                <input type="hidden" name="total_retur">
                                <label class="form-control-label col-3">Suplier </label>
                                <label class="form-control-label col-9" id="suplier_nama">: </label>
                            </div>
                            <div class="form-group row">
                                <label class="form-control-label col-3">PO Number </label>
                                <div class="col-9">
                                    <select name="po_produk_id" class="form-control" id="po_produk_id">

                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

					