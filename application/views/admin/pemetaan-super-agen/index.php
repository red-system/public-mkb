<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Pemetaan Super Reseller</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>pemetaan-super-reseller" class="kt-subheader__breadcrumbs-link">Reseller</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>pemetaan-super-reseller" class="kt-subheader__breadcrumbs-link">Pemetaan Super Reseller</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Pemetaan Super Reseller
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>pemetaan-super-reseller/list" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable">
                <thead>
                    <tr>
                        <th width="30">No</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kecamatan</th>
                        <th width="200">Jml. Reseller</th>
                    </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>
    </div>
</div>