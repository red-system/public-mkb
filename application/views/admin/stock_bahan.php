<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title"><?=$nama_bahan?></h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>bahan" class="kt-subheader__breadcrumbs-link">Inventori</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>bahan" class="kt-subheader__breadcrumbs-link">Bahan</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url().'stock-bahan/'.$this->uri->segment(2)?>" class="kt-subheader__breadcrumbs-link">Stok</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Data Bahan
				</h3>
				<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
				<input type="hidden" id="list_url" value="<?=base_url()?>stock-bahan/list/<?=$id?>" name="">
				<div style="display: none;" id="table_column"><?=$column?></div>
				<?php if(isset($columnDef)) {  ?>
				<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
				<?php } ?>
				<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
				<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<a href="<?=base_url()?>bahan" class="btn btn-brand btn-icon-sm btn-warning"><i class="la la-angle-double-left"></i> Kembali</a>
						<button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
							<i class="flaticon2-plus"></i> Tambah Data
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1 searchForm">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" id="generalSearch" placeholder="Search...">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="datatable table table-striped- table-hover table-checkable" >
				<thead>
					<tr>
						<th width="30">No</th>
						<th>Lokasi</th>
						<th>No Seri Bahan</th>
						<th style="text-center">Jumlah</th>
						<th width="150">Action</th>
					</tr>
				</thead>
				<tbody id="child_data_ajax"></tbody>
				<tfoot >
					<tr>
						<td colspan="3" style="text-align: right"><strong>Total</strong> </td>
						<td ><strong></strong> </td>
						<td></td>
					</tr>

				</tfoot>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Stok</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?=base_url().'stock-bahan/add/'.$id?>" method="post" id="kt_add_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<?php if(!isset($_SESSION["redpos_login"]['lokasi_id'])) { ?>
							<div class="form-group">
								<label class="form-control-label ">lokasi<b class="label--required">*</b></label>
								<select class="form-control col-md-12" name="stock_bahan_lokasi_id" required="">
									<option value="">Pilih Lokasi</option>
									<?php
									foreach ($lokasi as $key) {
										?>
										<option value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
										<?php
									}
									?>
								</select>
							</div>
							<?php } ?>
							<div class="form-group">
								<input type="hidden" name="bahan_id" value="<?=$id?>">
								<label class="form-control-label ">No Seri <b class="label--required">*</b></label>
								<input type="text" placeholder="" name="stock_bahan_seri" class="form-control" value="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">jumlah <b class="label--required">*</b></label>
								<div class="row">
									<input type="text" placeholder="" name="stock_bahan_qty" class="input-numeral form-control col-md-8" value="" required="">
									<span class="col-md-4"><strong><?=$nama_satuan?></strong></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Stok</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?=base_url().'stock-bahan/edit/'.$id?>" method="post" id="kt_edit_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<?php if(!isset($_SESSION["redpos_login"]['lokasi_id'])) { ?>
							<div class="form-group">
								<label class="form-control-label ">lokasi<b class="label--required">*</b></label>
								<select class="form-control col-md-12" name="stock_bahan_lokasi_id" required="">
									<option value="">Pilih Lokasi</option>
									<?php
									foreach ($lokasi as $key) {
										?>
										<option value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
										<?php
									}
									?>
								</select>
							</div>
							<?php } ?>
							<div class="form-group">
								<label class="form-control-label ">No Seri <b class="label--required">*</b></label>
								<input type="hidden" name="stock_bahan_id">
								<input type="hidden" name="bahan_id" value="<?=$id?>">
								<input type="text" placeholder="" name="stock_bahan_seri" class="form-control" value="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">jumlah <b class="label--required">*</b></label>
								<div class="row">
									<input type="text" placeholder="" name="stock_bahan_qty" class="input-numeral form-control col-md-8" value="" required="">
									<span class="col-md-4"><strong><?=$nama_satuan?></strong></span>
								</div>
							</div>


						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_edit_submit" type="submit" class="btn btn-primary">Simpan Data</button>
				</div>
			</form>
		</div>
	</div>
</div>