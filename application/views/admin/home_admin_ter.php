<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body ">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-md-12 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="start_date" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="1" value="<?=$min_tanggal?>" data-field="tanggal_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="end_date" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="1" value="<?=$max_tanggal?>" data-field="tanggal_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-info akses-filter_data">
                                <i class="la la-search"></i> Filter Data
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="btn-group btn-group btn-pill btn-group-sm">


            </div>
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Aktif Reseller
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2> <?=$aktif_agen?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-success">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Aktif Super Reseller
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2><?=$aktif_super_agen?></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 row">
        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="agen_url" value="<?=base_url()."home/request-agen"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Request Reseller
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="agen-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>

                            <th>No HP</th>
                            <th>Email</th>
                            <th>No KTP</th>
                            <th>Alamat Tinggal</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="super_url" value="<?=base_url()."home/request-super"?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Request Super Reseller
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="super-table">
                        <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Nama</th>

                            <th>No HP</th>
                            <th>Email</th>
                            <th>No KTP</th>
                            <th>Alamat Tinggal</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

