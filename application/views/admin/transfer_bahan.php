<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Transfer Stock Bahan</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>bahan" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>bahan" class="kt-subheader__breadcrumbs-link">Transfer Stock Barang</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet">
								<div class="kt-portlet__body kt-portlet__body--fit">
									<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
										<div class="kt-grid__item">

											<!--begin: Form Wizard Nav -->
											<div class="kt-wizard-v3__nav">
												<div class="kt-wizard-v3__nav-items">
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>transfer-bahan" data-ktwizard-type="step" data-ktwizard-state="current">
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>1&nbsp;</span> <span><i class="flaticon-truck"></i></span> Transfer
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>konfirmasi-transfer-bahan" data-ktwizard-type="step" >
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>2&nbsp;</span><span><i class="flaticon2-checking"></i></span> Konfirmasi Kiriman
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>history-transfer-bahan" data-ktwizard-type="step">
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>3&nbsp;</span><span><i class="flaticon2-time"></i></span> Histori Transfer
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
												</div>
											</div>

											<!--end: Form Wizard Nav -->
										</div>
										<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
											<div class="col-12">
												<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
														<input type="hidden" id="list_url" value="<?=base_url()?>transfer-bahan/list" name="">
														<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
														<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
														<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
												<div class="kt-portlet__body">

													<!--begin: Search Form -->
													<div class="">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="row align-items-center">
																	<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
																		<div class="kt-input-icon kt-input-icon--left">
																			<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
																			<span class="kt-input-icon__icon kt-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<table class="datatable table table-striped- table-hover table-checkable" >
														<thead>
															<tr>
																<th width="30">No</th>
																<th>Kode Bahan</th>
																<th>Nama Bahan</th>
																<th>Jenis Bahan</th>
																<th>Satuan</th>
																<th>Global Stok</th>
																<?=((isset($_SESSION["redpos_login"]['lokasi_id'])) ? '<th>Jumlah</th>': "")?>
																<th>Suplier</th><th width="60">Action</th>
																
															</tr>
														</thead>
														<tbody id="child_data_ajax"></tbody>
														<tfoot >
															<tr>
																<td colspan="5" style="text-align: right"><strong>Total</strong> </td>
																<td ><strong></strong> </td>
																<td></td>
																<td></td>
																<?=((isset($_SESSION["redpos_login"]['lokasi_id'])) ? "<td></td>": "")?>
															</tr>
															
														</tfoot>
													</table>

													<!--end: Search Form -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>