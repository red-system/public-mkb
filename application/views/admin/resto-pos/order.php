<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Rekapitulasi Pos</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>order-resto-pos" class="kt-subheader__breadcrumbs-link">POS</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>order-resto-pos" class="kt-subheader__breadcrumbs-link">Rekapitulasi Pos</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?=(($this->uri->segment(1)=='resto-pos') ? 'active' : '')?>" href="<?=base_url()?>resto-pos">
                            <i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penjualan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(($this->uri->segment(1)=='order-resto-pos') ? 'active' : '')?>" href="<?=base_url()?>order-resto-pos">
                            <i class="flaticon-notepad"></i> <span class="kt--visible-desktop-inline-block">Order</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(($this->uri->segment(1)=='rekapitulasi-resto-pos') ? 'active' : '')?>" href="<?=base_url()?>rekapitulasi-resto-pos">
                            <i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Rekapitulasi POS</span>
                        </a>
                    </li>
                </ul>
            </div>
            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>order-resto-pos/list" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
            <div style="display: none;" data-width="150" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" id="generalSearch" placeholder="Search...">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Nama Order</th>
                    <th>lokasi</th>
                    <th>Pelanggan</th>
                    <th>Staff</th>
                    <th>Catatan</th>
                    <th>Tanggal</th>
                    <th width="160">Action</th>
<!--                    <th></th>-->
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>

            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="detail_url" value="<?=base_url()?>order-resto-pos/detail" name="">
                <div class="row">
                    <div class="col-md-4">
                        <div class="col-md-9">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="no_faktur" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="tanggal" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="lokasi_nama" class="col-form-label"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                                <label for="example-text-input" class="col-1 col-form-label">:</label>
                                <div class="col-8">
                                    <label name="nama_pelanggan" class="col-form-label"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="alamat_pelanggan" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Telepon Pelanggan</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="telepon_pelanggan" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Pengiriman</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="pengiriman" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Total</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="total" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Biaya Tambahan</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="biaya_tambahan" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Potongan Akhir</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="updated_at" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="grand_total" class="col-form-label"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran</label>
                            <label for="example-text-input" class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <label name="tipe_pembayaran_nama" class="col-form-label"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" id="penjualan_id" name="">
                        <table class="table table-striped- table-hover table-checkable" >
                            <thead>
                            <tr>
                                <th width="150">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th width="80">QTY</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody id="item_child">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div class="modal" id="kt_modal_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form method="post" id="payment-form" action="<?=base_url()?>order-resto-pos/process">
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Total</label>
                            <div class="col-9">
                                <label class="col-form-label" id="total-bayar">0</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Tambahan</label>
                            <div class="col-9">
                                <input name="tambahan" class="form-control input-numeral" value="0" id="tambahan-bayar">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Potongan</label>
                            <div class="col-9">
                                <input name="potongan" class="form-control input-numeral" value="0" id="potongan-bayar">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
                            <div class="col-9">
                                <label name="" class="col-form-label" id="grand-bayar">0</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran</label>
                            <div class="col-9">
                                <select class="form-control col-md-12" name="tipe_pembayaran_id" id="tipe_pembayaran" required="">
                                    <?php
                                    foreach ($tipe_pemabayaran as $key) {
                                        ?>
                                        <option class="" value="<?=$key->tipe_pembayaran_id?>" data-additional="<?=$key->additional?>" data-kembalian="<?=$key->kembalian?>" data-jenis="<?=$key->jenis_pembayaran?>"><?=$key->tipe_pembayaran_nama.' '.$key->no_akun?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <textarea name="transaksi" id="transaksi" style="display: none"></textarea>

                        <div class="form-group row col-md-12" id="terbayar_container">
                            <label for="example-text-input" class="col-3 col-form-label">Terbayar</label>
                            <div class="col-9">
                                <input type="hidden" name="penjualan_id" id="input_penjualan_id">
                                <input type="text" name="terbayar" class="form-control input-numeral readonly numeral-display" value="0" id="terbayar">
                                <input type="hidden" name="terbayar" class=" numeral-input" value="0">
                            </div>
                        </div>
                        <div id="kredit_container" style="display: none;">
                            <div class="form-group kt-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tenggat Pelunasan<b class="label--required">*</b></label>
                                <div class="col-9">
                                    <input type="text" name="tenggat_pelunasan" id="tenggat_pelunasan" class="form-control tanggal tipe_kredit"  autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div id="additional_container" style="display: none">
                            <div class="form-group row col-md-12" >
                                <label for="example-text-input" class="col-3 col-form-label">No ref / No Check / No BG</label>
                                <div class="col-9">
                                    <input type="text" name="additional_no" class="form-control additional">
                                </div>
                            </div>
                            <div class="form-group row col-md-12" >
                                <label for="example-text-input" class="col-3 col-form-label">tanggal</label>
                                <div class="col-9">
                                    <input type="text" name="additional_tanggal" id="additional_tanggal" class="form-control tanggal additional"  autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row col-md-12" >
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                <div class="col-9">
                                    <textarea class="form-control additional" name="additional_keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary"  id="save-payment"><i class="fa fa-save"></i> Save</button>
            </div>
            </form>
        </div>

    </div>
</div>
<?php
