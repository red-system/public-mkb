<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Order Pos Detail</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>order-resto-pos" class="kt-subheader__breadcrumbs-link">POS</a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>order-resto-pos/" class="kt-subheader__breadcrumbs-link">Order POS</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <input type="hidden" id="detail-page">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?=(($this->uri->segment(1)=='resto-pos') ? 'active' : '')?>" href="<?=base_url()?>resto-pos">
                            <i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penjualan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=((($this->uri->segment(1)=='order-resto-pos')||($this->uri->segment(1)=='order-resto-pos-detail')) ? 'active' : '')?>" href="<?=base_url()?>order-resto-pos">
                            <i class="flaticon-notepad"></i> <span class="kt--visible-desktop-inline-block">Order</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(($this->uri->segment(1)=='rekapitulasi-resto-pos') ? 'active' : '')?>" href="<?=base_url()?>rekapitulasi-resto-pos">
                            <i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Rekapitulasi POS</span>
                        </a>
                    </li>
                </ul>
            </div>
            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>order-resto-pos/list" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
            <div style="display: none;" data-width="150" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
        </div>
        <div class="kt-portlet__body">
            <div class="col-12 row">
                <div class="col-6">
                    <div class="form-group row">
                        <input type="hidden" id="penjualan_id" value="<?=$penjualan->penjualan_id?>">
                        <label class="form-control-label col-3">Tanggal</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8"><?=date("Y-m-d",strtotime($penjualan->created_at))?></label>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-3">Pelanggan</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8"><?=$penjualan->nama_pelanggan?></label>
                    </div>

                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="form-control-label col-3">Nama Orde</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8"><?=$penjualan->order_name?></label>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-3">Catatan</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8"><?=$penjualan->order_note?></label>
                    </div>
                </div>
            </div>
            <div class="col-12" style="margin-top: 20px">
                <h5>List Item</h5>
            </div>
            <div class="col-12">
                <table class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr>
                            <th style="text-align: center" width="10"><input type="checkbox" class="parent"></th>
                            <th>No</th>
                            <th>Produk</th>
                            <th>Satuan</th>
                            <th>Jumlah</th>
                            <th>Catatan</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                        foreach ($detailPenjualan as $key=>$item){
                            ?>
                            <tr>
                                <td style="text-align: center"><input type="checkbox" value="<?=$item->penjualan_produk_id?>" class="child"></td>
                                <td><?=($key+1)?></td>
                                <td><?=$item->produk_nama?></td>
                                <td><?=$item->satuan_nama?></td>
                                <td><?=$item->qty?></td>
                                <td><?=$item->keterangan?></td>
                            </tr>
                            <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<div class="pos-floating-button">
    <a class="btn btn-warning btn-pill " href="<?=base_url()?>order-resto-pos"><i class="fa fa-reply"></i>&nbsp; Kembali</a>
    <a class="btn btn-primary btn-pill " href="<?=base_url().'resto-pos/add-item/'.$this->uri->segment(2)?>"><i class="fa fa-plus"></i>&nbsp; Tambah Item</a>
    <button class="btn btn-danger btn-pill  cancel-item-btn" data-item="" id="cancel-item-btn"><i class="fa fa-trash"></i>&nbsp; Delete Item</button>
    <button class="btn btn-success btn-pill  order-process-btn" ><i class="fa fa-money-bill"></i>&nbsp; Process</button>
    <button class="btn btn-success btn-pill " data-url="<?=site_url()?>resto-pos/print-order/" data-static="<?=site_url()?>resto-pos/print-order/" id="print-button"><i class="fa fa-print"></i>&nbsp; print</button>
</div>
<div class="modal" id="kt_modal_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form method="post" id="payment-form" action="<?=base_url()?>order-resto-pos/process">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Total</label>
                                <div class="col-9">
                                    <label class="col-form-label" id="total-bayar">0</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tambahan</label>
                                <div class="col-9">
                                    <input name="tambahan" class="form-control input-numeral" value="0" id="tambahan-bayar">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Potongan</label>
                                <div class="col-9">
                                    <input name="potongan" class="form-control input-numeral" value="0" id="potongan-bayar">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
                                <div class="col-9">
                                    <label name="" class="col-form-label" id="grand-bayar">0</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran</label>
                                <div class="col-9">
                                    <select class="form-control col-md-12" name="tipe_pembayaran_id" id="tipe_pembayaran" required="">
                                        <?php

                                        foreach ($tipe_pembayaran as $key) {
                                            ?>
                                            <option class="" value="<?=$key->tipe_pembayaran_id?>" data-additional="<?=$key->additional?>" data-kembalian="<?=$key->kembalian?>" data-jenis="<?=$key->jenis_pembayaran?>"><?=$key->tipe_pembayaran_nama.' '.$key->no_akun?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <textarea name="transaksi" id="transaksi" style="display: none"></textarea>

                            <div class="form-group row col-md-12" id="terbayar_container">
                                <label for="example-text-input" class="col-3 col-form-label">Terbayar</label>
                                <div class="col-9">
                                    <input type="hidden" name="penjualan_id" id="input_penjualan_id">
                                    <input type="text" name="terbayar" class="form-control input-numeral readonly numeral-display" value="0" id="terbayar">
                                    <input type="hidden" name="terbayar" class=" numeral-input" value="0">
                                </div>
                            </div>
                            <div id="kredit_container" style="display: none;">
                                <div class="form-group kt-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Tenggat Pelunasan<b class="label--required">*</b></label>
                                    <div class="col-9">
                                        <input type="text" name="tenggat_pelunasan" id="tenggat_pelunasan" class="form-control tanggal tipe_kredit"  autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div id="additional_container" style="display: none">
                                <div class="form-group row col-md-12" >
                                    <label for="example-text-input" class="col-3 col-form-label">No ref / No Check / No BG</label>
                                    <div class="col-9">
                                        <input type="text" name="additional_no" class="form-control additional">
                                    </div>
                                </div>
                                <div class="form-group row col-md-12" >
                                    <label for="example-text-input" class="col-3 col-form-label">tanggal</label>
                                    <div class="col-9">
                                        <input type="text" name="additional_tanggal" id="additional_tanggal" class="form-control tanggal additional"  autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row col-md-12" >
                                    <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                                    <div class="col-9">
                                        <textarea class="form-control additional" name="additional_keterangan"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"  id="save-payment"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>

    </div>
</div>
<?php
