<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Order</title>



    <!-- Normalize or reset CSS with your favorite library -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">



    <!-- Load paper.css for happy printing -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">



    <!-- Set page size here: A5, A4 or A3 -->

    <!-- Set also "landscape" if you need -->

    <style>

        @page { size: 58mm; margin: 0 0 0 0;} /* output size */

        body.receipt .sheet { width: 58mm;padding: 0px} /* sheet size */

        @media print { body.receipt .sheet { width: 58mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */

        p{font-size: 10px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }



        @media screen {

            body { background: #e0e0e0 }

        }

    </style>

</head>



<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


    <h5 style="text-align: center;margin-bottom: 10px"><?=$_SESSION['redpos_company']['company_name']?></h5>
    <p style="margin:0px;text-align: center">Data Order</p>
    <p style="margin:0px;text-align: center"><?=$penjualan->order_name?></p>

    <p style="margin:0px">------------------------------------------------------</p>

    <table style="width: 100%">
        <tbody>
            <?php
                foreach ($penjualan_produk as $item){
                    ?>
                    <tr>
                        <td><p><?=$item->produk_nama.($item->keterangan==''?'':'('.$item->keterangan.')').' x '.$item->qty?></p></td>
                    </tr>
                    <?php
                }
            ?>

        </tbody>



    </table>
    <p style="margin:0px">------------------------------------------------------</p>
    <p style="margin:0px;">catatan : <?=$penjualan->order_note?></p>
</section>

</body>

</html>
