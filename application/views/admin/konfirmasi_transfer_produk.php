<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title">Transfer Stock Produk</h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Transfer Stock Barang</a>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet">
								<div class="kt-portlet__body kt-portlet__body--fit">
									<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
										<div class="kt-grid__item">

											<!--begin: Form Wizard Nav -->
											<div class="kt-wizard-v3__nav">
												<div class="kt-wizard-v3__nav-items">
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>transfer-produk" data-ktwizard-type="step" >
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>1&nbsp;</span><span><i class="flaticon-truck"></i></span> Transfer
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>konfirmasi-transfer-produk" data-ktwizard-type="step" <?=(($this->uri->segment(1) == 'konfirmasi-transfer-produk') ? 'data-ktwizard-state="current"' : '' )?>>
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>2&nbsp;</span><span><i class="flaticon2-checking"></i></span> Konfirmasi Kiriman
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
													<a class="kt-wizard-v3__nav-item" href="<?=base_url()?>history-transfer-produk" data-ktwizard-type="step" <?=(($this->uri->segment(1) == 'history-transfer-produk') ? 'data-ktwizard-state="current"' : '' )?>>
														<div class="kt-wizard-v3__nav-body">
															<div class="kt-wizard-v3__nav-label">
																<span>3&nbsp;</span><span><i class="flaticon2-time"></i></span> Histori Transfer
															</div>
															<div class="kt-wizard-v3__nav-bar"></div>
														</div>
													</a>
												</div>
											</div>

											<!--end: Form Wizard Nav -->
										</div>
										<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
											<div class="col-12">
												<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
														<input type="hidden" id="list_url" value="<?=base_url().(($this->uri->segment(1) == 'konfirmasi-transfer-produk') ? 'konfirmasi' : 'history' )?>-transfer-produk/list" name="">
														<div style="display: none;" id="table_column"><?=$column?></div>
														<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
														<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
												<div class="kt-portlet__body">

													<!--begin: Search Form -->
													<div class="">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="row align-items-center">
																	<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
																		<div class="kt-input-icon kt-input-icon--left">
																			<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
																			<span class="kt-input-icon__icon kt-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<?php if($this->uri->segment(1) == "konfirmasi-transfer-produk") { ?>
													<table class="datatable-with-decimal table table-striped- table-hover table-checkable" >
														<thead>
															<tr>
																<th width="30">No</th>
																<th>Tanggal</th>
																<th>Kode Produk</th>
																<th>Nama Produk</th>
																<th>Dari</th>
																<th>Tujuan</th>
																<th>Jumlah</th>
																<th>Satuan</th>
																<th width="150">Status</th>
																<th>Action</th>
															</tr>
														</thead>
														<tbody id="child_data_ajax"></tbody>
														<tfoot >
															<tr>
																<td colspan="7" style="text-align: right"><strong>Total</strong> </td>
																<td ><strong></strong> </td>
																<td></td>
																<td></td>
															</tr>
															
														</tfoot>
													</table>
													<?php } else { ?>
													<table class="datatable-with-decimal table table-striped- table-hover table-checkable" >
														<thead>
															<tr>
																<th width="30">No</th>
																<th>Tanggal</th>
																<th>Tanggal Konfirmasi</th>
																<th>Kode Produk</th>
																<th>Nama Produk</th>
																<th>Dari</th>
																<th>Tujuan</th>
																<th>Jumlah</th>
																<th>Status</th>
															</tr>
														</thead>
														<tbody id="child_data_ajax"></tbody>
														<tfoot >
															<tr>
																<td colspan="7" style="text-align: right"><strong>Total</strong> </td>
																<td ><strong></strong> </td>
																<td></td>
															</tr>
															
														</tfoot>
													</table>													
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal" id="kt_modal_confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi Transfer</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<form action="<?=base_url().'konfirmasi-transfer-produk/confirm'?>" method="post" id="kt_add_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
													<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Kode Produk</label>
															<div class="col-9">
																<label name="produk_kode" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Nama Produk</label>
															<div class="col-9">
																<label name="produk_nama" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Dari</label>
															<div class="col-9">
																<label name="dari" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Tujuan</label>
															<div class="col-9">
																<label name="tujuan" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Jumlah</label>
															<div class="col-9">
																<input type="hidden" name="history_transfer_qty">
																<label name="history_transfer_qty" class="col-form-label"></label>
															</div>
														</div>
													<div class="form-group">
														<label>Status</label>
														<input type="hidden" name="history_transfer_produk_id">
														<div class="kt-radio-inline">
															<label class="kt-radio">
																<input type="radio" name="status_history" checked="" value="Diterima Semua"> Diterima Semua
																<span></span>
															</label>
															<label class="kt-radio">
																<input type="radio" name="status_history" checked="" value="Diterima Sebagian"> Diterima Sebagian
																<span></span>
															</label>
															<label class="kt-radio">
																<input type="radio" name="status_history" value="Ditolak"> Ditolak
																<span></span>
															</label>
														</div>
													</div>

				                                     <div class="form-group" id="qty_terima_group">
				                                            	<label class="form-control-label ">Jumlah diterima <b class="label--required">*</b></label>
				                                            	<input type="text" class="form-control input-numeral" id="qty_terima" rows="3" name="qty_terima" required="" value="1">
				                                        </div>

				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
				                                            	<textarea class="form-control" id="keterangan" name="keterangan" required=""></textarea>
				                                        </div>
			                                        </div>
											</div>
										</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
											</div>
										</form>
									</div>
								</div>
							</div>