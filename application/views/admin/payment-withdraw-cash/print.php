<?php
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=withdraw cash ".date("d M Y").".xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">
    <title>Withdraw Cash </title>
    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page { size: 210mm; margin: 0 0 0 0;} /* output size */
        body.receipt .sheet { width: 210mm;padding: 0px} /* sheet size */
        @media print { body.receipt .sheet { width: 210mm;padding: 0px ;margin:0 0 0 0;} @page { margin: 0 0 0 0} } /* fix for Chrome */
        p{font-size: 12px;margin:0px;font-family: Tahoma,Verdana,Segoe,sans-serif; }
        td{font-size: 12px}
        @media screen {
            body { background: #e0e0e0 }
        }
    </style>
    <style type="text/css">
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent;
            border-collapse: collapse; }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #ebedf2; }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #ebedf2; }
        .table tbody + tbody {
            border-top: 1px solid #ebedf2; }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem; }
        .table-bordered {
            border: 1px solid #ebedf2; }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ebedf2; }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px; }

        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0; }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #f7f8fa; }

        .table-hover tbody tr:hover {
            color: #212529;
            background-color: #fafbfc; }
        body {
            font-family: 'Poppins';font-size: 12px;
        }
        .str{ mso-number-format:\@; }
    </style>

</head>



<body class="receipt" >

<section class="">


    <h5 style="text-align: center;margin-bottom: 10px;font-size: 12pt"></h5>
    <div style="width: 100%;border-bottom: 1px solid #000;margin-top: 10px"></div>

    <table style="width: 100%;margin-top: 20px" border="1">
        <tbody>
        <tr>
            <td >P</td>
            <td ><?=date("Ymd")?></td>
            <td  class="str"><?=$this->config->item('mandiri-rekening')?></td>
            <td ><?=sizeof($bonus)?></td>
            <td ><?=$total_bonus?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php
            foreach ($bonus as $item) {
                ?>
                <tr>
                    <td class="str"><?=$item->bank_rekening?></td>
                    <td><?=$item->bank_atas_nama?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>IDR</td>
                    <td><?=$item->total_bonus-$potongan?></td>
                    <td></td>
                    <td></td>
                    <td><?=($item->bank_id==2?"IBU":"LBU")?></td>
                    <td class="str"><?=($item->bank_id==2?"":$item->kode_bank)?></td>
                    <td><?=$item->nama_bank?></td>
                    <td><?=$item->bank_cabang?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Y</td>
                    <td><?=$item->email?></td>
                    <td></td>
                    <td>OUR</td>
                    <td>1</td>
                    <td>WIthdraw Cash</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
            }
        ?>
        </tbody>



    </table>
</section>

</body>

</html>

