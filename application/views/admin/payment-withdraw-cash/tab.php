<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-withdraw-cash') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-withdraw-cash">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Withdraw Cash List</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-withdraw-cash-success') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-withdraw-cash-success">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Withdraw Cash Success</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-withdraw-cash-hold') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-withdraw-cash-hold">
                <i class="fa fa-window-close"></i> <span class="kt--visible-desktop-inline-block">Withdraw Cash hold</span>
            </a>
        </li>
    </ul>
</div>