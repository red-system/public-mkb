<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Pengiriman Withdraw Cash</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>pengiriman-withdraw-cash" class="kt-subheader__breadcrumbs-link">Pengiriman Withdraw Cash</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">

            <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
            <input type="hidden" id="list_url" value="<?=base_url()?>pengiriman-withdraw-cash-success/list" name="">
            <div style="display: none;" id="table_column"><?=$column?></div>
            <div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
            <?php if(isset($columnDef)) {  ?>
                <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
            <?php } ?>
            <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            <?=$this->load->view('admin/payment-withdraw-cash/tab') ?>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="6" value="" data-field="tanggal_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="6" value="" data-field="tanggal_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="10"></th>
                    <th width="30">No</th>
                    <th>Tanggal</th>
                    <th>Super Agen</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Status Pengiriman</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
                <tfoot>
                <tr>
                    <td colspan="4" style="text-align: right;"><strong>Total</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="pos-floating-button">
    <button id="turnback" type="button" class="btn btn-warning">Turn Back</button>
</div>


