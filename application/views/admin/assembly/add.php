<form action="<?=base_url()?>assembly/add/save" method="post" id="kt_add">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">Add Assembly</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Produk</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>produk" class="kt-subheader__breadcrumbs-link">Assembly</a>
                <input type="hidden" id="base_url" name="" value="<?=base_url()?>">
            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Tambah Assembly Produk
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="dropdown dropdown-inline">
                            <a href="<?=base_url()."produk"?>" type="button" class="btn btn-warning btn-brand btn-icon-sm">
                                <i class="flaticon2-back"></i> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-control-label ">Kode Produk<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_kode" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Barcode<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="barcode" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama Produk<b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_nama" class="form-control" value="" required="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Jenis Produk <b class="label--required">*</b></label>
                                <select class="form-control col-md-12" name="produk_jenis_id" required="">
                                    <option value="">Pilih Jenis</option>
                                    <?php
                                    foreach ($jenis_produk as $key) {
                                        ?>
                                        <option value="<?=$key->jenis_produk_id?>"><?=$key->jenis_produk_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-control-label ">Satuan <b class="label--required">*</b></label>
                                <select class="form-control col-md-12" name="produk_satuan_id" required="">
                                    <option value="">Pilih Satuan</option>
                                    <?php
                                    foreach ($satuan as $key) {
                                        ?>
                                        <option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Harga Pokok <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="hpp_global" class="input-numeral form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Harga Jual <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="harga_eceran" class="input-numeral form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Minimal Stok <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="produk_minimal_stock" class="input-numeral form-control" value="" required="">
                                <textarea name="transaksi" id="textTransaksi" style="display: none;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                    </div>  
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">                                      
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-brand btn-icon-sm" id="button_add_trans">
                                    <i class="flaticon2-plus"></i> Tambah Produk
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" >
                        <thead>
                            <tr>
                                <th width="150">Kode Produk</th>
                                <th width="150">Nama Produk</th>
                                <th width="150">Satuan</th>
                                <th width="80">Jumlah</th>
                                <th width="30">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="item_child"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="pos-floating-button">
        <button id="kt_add_submit" type="submit" class="btn btn-primary">Simpan Data</button>
    </div>

</form>
<span id="base-value" data-base-url="<?php echo base_url(); ?>"></span>
<div class="modal" id="kt_modal_search_produk"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" id="produk_url" value="<?=base_url()?>assembly/produk-list" name="">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Search:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control searchInput" id="generalSearch">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Barcode:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <input type="text" class="form-control searchInput textSearch" id="barcode" data-col-index="3" data-field="barcode">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped- table-hover table-checkable" id="produk-table">
                    <thead>
                        <tr>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Jenis Produk</th>
                            <th width="60">Aksi</th>

                        </tr>
                    </thead>
                    <tbody id="produk_child"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>