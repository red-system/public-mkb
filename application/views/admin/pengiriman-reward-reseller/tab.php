<div class="kt-portlet__head-toolbar col-12">
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-reward-reseller') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-reward-reseller">
                <i class="flaticon2-list-2"></i> <span class="kt--visible-desktop-inline-block">Reward Reseller List</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-reward-reseller-proses') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-reward-reseller-proses">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Reward Reseller Ter-proses</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?=(($this->uri->segment(1)=='pengiriman-reward-reseller-terkirim') ? 'active' : '')?>" href="<?=base_url()?>pengiriman-reward-reseller-terkirim">
                <i class="fa fa-check"></i> <span class="kt--visible-desktop-inline-block">Reward Reseller Terkirim</span>
            </a>
        </li>
    </ul>
</div>