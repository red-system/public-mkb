<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">

        <h3 class="kt-subheader__title">Submenu</h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="<?=base_url()?>master" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?=base_url()?>master/menu" class="kt-subheader__breadcrumbs-link">Submenu</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>

    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Data Submenu
                </h3>
                <input type="hidden" id="base_url" value="<?=base_url()?>" name="">
                <input type="hidden" id="list_url" value="<?=base_url()?>master/submenu-list/<?=$id?>" name="">
                <div style="display: none;" id="table_column"><?=$column?></div>
                <?php if(isset($columnDef)) {  ?>
                    <div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
                <?php } ?>
                <div style="display: none;" data-style="dropdown" id="table_action"><?=(isset($action) ? $action : "")?></div>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="dropdown dropdown-inline">
                        <a href="<?=base_url()?>master/menu" type="button" class="btn btn-brand btn-icon-sm btn-warning">
                            <i class="fa fa-reply"></i> Kembali
                        </a>
                        <button type="button" class="btn btn-brand btn-icon-sm" data-target="#kt_modal_add" data-toggle="modal">
                            <i class="flaticon2-plus"></i> Tambah Data
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable table table-striped- table-hover table-checkable" >
                <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Action</th>
                    <th>Url</th>
                    <th>Keterangan</th>
                    <th>Data</th>
                    <th>Urutan</th>
                    <th width="280">Action</th>
                </tr>
                </thead>
                <tbody id="child_data_ajax"></tbody>
            </table>
        </div>

    </div>
</div>
<div class="modal" id="kt_modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Submenu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>master/submenu-add" method="post" id="kt_add_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label ">Kode <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="sub_menu_kode" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="sub_menu_nama" class="form-control" value="" required="">
                                <input type="hidden" name="menu_id" value="<?=$id?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Action </label>
                                <input type="text" placeholder="" name="action" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Url <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="url" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Data </label>
                                <input type="text" placeholder="" name="data" class="form-control" value="" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
                                <textarea class="form-control" name="keterangan"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Urutan <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="urutan" class="form-control" value="" required="">
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="kt_modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit User Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="<?=base_url()?>master/submenu-edit" method="post" id="kt_edit_staff_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" name="sub_menu_id">
                                <label class="form-control-label ">Kode <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="sub_menu_kode" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Nama <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="sub_menu_nama" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Action</label>
                                <input type="text" placeholder="" name="action" class="form-control" value="" >
                                <input type="hidden" name="menu_id" value="<?=$id?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Url <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="url" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Data </label>
                                <input type="text" placeholder="" name="data" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
                                <textarea class="form-control" name="keterangan"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label ">Urutan <b class="label--required">*</b></label>
                                <input type="text" placeholder="" name="urutan" class="form-control" value="" required="">
                            </div>

                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="kt_edit_submit" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
