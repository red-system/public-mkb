<!-- begin:: Content -->
<form action="<?=base_url().'master/type/access/edit/'.$id?>" method="post" id="form-send">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">User Role</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="<?=base_url()?>/master" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=base_url()?>master/type" class="kt-subheader__breadcrumbs-link">Type</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="<?=current_url()?>" class="kt-subheader__breadcrumbs-link">User Role</a>
                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>

        </div>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data Akses Role
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <table class="table table-striped table-bordered table-hover datatable-no-pagination dataTable no-footer dtr-inline" id="role_table">
                    <thead>
                    <tr >
                        <th width="30">No</th>
                        <th width="250">Nama Menu</th>
                        <th width="250">Hak Akses</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no = 1;
                        $sub_parent_no=100;
                        foreach ($menu as $key=> $itemMenu){
                            ?>
                            <tr role="row">
                                <td><?=$no?></td>
                                <td><strong><?=$itemMenu->menu_nama?></strong></td>
                                <td>
                                    <label class="m-checkbox m-checkbox--success">
                                        <input type="checkbox" class="check-akses parent-super" data-no="<?=$no?>" name="role[menu][_<?=$itemMenu->menu_id?>]" value="true" <?=(isset($akses_role["menu"]["_".$itemMenu->menu_id])?'checked':'')?>>
                                        Tampilkan
                                        <span></span>
                                    </label>
                                </td>

                            </tr>

                            <?php
                            foreach ($submenu["_".$itemMenu->menu_id] as $key2=> $itemSubmenu){
                                ?>
                                <tr role="row" class="even">
                                    <td></td>
                                    <td><i class="la la-arrow-circle-o-right"></i>&nbsp;<?=$itemSubmenu->sub_menu_nama?></td>
                                    <td>
                                        <label class="m-checkbox m-checkbox--success">
                                            <input type="checkbox" class="check-akses parent" data-no="<?=$sub_parent_no?>" data-no-parent="<?=$no?>" name="role[submenu][_<?=$itemSubmenu->menu_id?>][_<?=$itemSubmenu->sub_menu_id?>]" value="true" <?=(isset($akses_role["submenu"]["_".$itemMenu->menu_id]['_'.$itemSubmenu->sub_menu_id])?'checked':'')?>>
                                            Tampilkan
                                            <span></span>
                                        </label>
                                    </td>

                                </tr>
                                <?php
                                $sub_parent_no ++;
                            }
                            $no ++;
                        }
                    ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="pos-floating-button" style="left: 40%">
        <button type="button" class="btn btn-primary btn-lg m-btn btn--custom btn--pill btn--icon btn--air" id="submit-btn">
                                            <span>
                                                <i class="la la-check"></i>
                                                <span>Perbarui Role User</span>
                                            </span>
        </button>

        <a href="<?=base_url()?>user-role" class="btn-produk-add btn btn-warning btn-lg btn btn--custom btn--pill btn--icon btn--air">
                                            <span>
                                                <i class="la la-angle-double-left"></i>
                                                <span>Kembali ke Daftar</span>
                                            </span>
        </a>
    </div>
</form>