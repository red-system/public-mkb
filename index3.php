<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Kind Of Beauty</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
    <div class="container">
        <div class="row pt-lg-5 my-lg-0 pb-lg-0 my-5 pb-5">
            <div
                class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-lg-end justify-content-sm-center justify-content-center">
                <img src="assets/img/logo.png" class="logo">
            </div>
        </div>


        <div class="d-none d-xl-block d-lg-block d-sm-none d-md-none py-5">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                    <!-- <div class="section-content"> -->
                        <!-- <span class="text-content-1 float-right">OUR SYSTEM IS UNDER MAINTENANCE</span> -->
                        <img src="assets/img/errors.png" class="w-100 img-content">
                        <!-- <span class="text-content-2">WE WILL BE BACK!</span> -->
                    <!-- </div> -->
                </div>
            </div>
        </div>

        <div class="row d-flex justify-content-center  d-xl-none d-lg-none d-md-block d-sm-block my-5 px-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 px-0">
                <div class="text-center">
                    <span class="text-content-3">OUR SYSTEM<br>IS UNDER MAINTENANCE</span>
                    <img src="assets/img/errors-mobile.png" class="w-100">
                    <span class="text-content-4">WE WILL BE BACK!</span>
                </div>
            </div>
        </div>


        <div class="row pt-lg-0 pt-5 mt-5 mt-lg-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                <!-- <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-6 d-flex justify-content-end"> -->
                <a href="https://api.whatsapp.com/send?phone=6281337663109&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB"
                    target="_blank" class="text-sosmed mr-4"><i class="fa fa-whatsapp"></i> 081 337 663 109</a>
                <!-- </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-6"> -->

                <a href="https://www.facebook.com/Mkbhealthandbeauty-101394608499775" class="text-sosmed"><i
                        class="fa fa-facebook"></i></a>
                &nbsp;
                <a href="https://www.instagram.com/mkb.mykindofbeauty" class="text-sosmed"><i
                        class="fa fa-instagram"></i></a>
                <a href="https://www.instagram.com/mkb.mykindofbeauty" class="text-sosmed"><span
                        class="pl-1">mkb.mykindofbeauty</span></a>

                <!-- </div>
                </div> -->
            </div>
        </div>
    </div>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/bootstrap/js/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>