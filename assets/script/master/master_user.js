$("#child_data_ajax").on("click",".enter-btn",function (){
    var base_url = $("#base_url").val();
    var json = $(this).siblings('textarea').val();
    var data = JSON.parse(json)
    var id = data.username;
    var url = base_url+"master/user/enter/"+id;
    $.ajax({
        url:url,
        success:function (response){
            var data = jQuery.parseJSON(response);
            if (data.success) {
                window.location.href = base_url;
            } else {
                console.log("error");
            }
        }
    })
});