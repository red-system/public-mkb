"use strict";

// Class Definition
var KTCompany = function() {
    var general = function(){
        var base_url = $("#base_url").val();

        $("#child_data_ajax").on('click','.enter-btn',function(e){
            e.preventDefault()
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var url = $(this).data('route');

            $.ajax({
                url: url,
                type: "post",
                data:{'id':object.id},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    console.log(response)
                    if (data.success){
                        window.location.href = base_url
                    }else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });
        })
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTCompany.init();
});