$(document).ready(function () {
    var base_url = $("#base_url").val()
    $("#child_data_ajax").on("click",".approve-btn",function () {
        var json = $(this).siblings("textarea").val()
        var object = JSON.parse(json);
        $.ajax({
            url:base_url+"manage-money-back/approve",
            type:"post",
            data:{"id":object.money_back_guarantee_id},
            success:function (response) {
                var data = jQuery.parseJSON(response);
                if (data.success) {
                    window.location.reload();
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }
        })
    })
    $("#child_data_ajax").on("click",".refuse-btn",function () {
        var json = $(this).siblings("textarea").val()
        var object = JSON.parse(json);
        $.ajax({
            url:base_url+"manage-money-back/refuse",
            type:"post",
            data:{"id":object.money_back_guarantee_id},
            success:function (response) {
                var data = jQuery.parseJSON(response);
                if (data.success) {
                    window.location.reload();
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }
        })
    })
    $("#child_data_ajax").on("click",".pengiriman-btn",function () {
        var json = $(this).siblings("textarea").val()
        var object = JSON.parse(json);
        $("#money_back_guarantee").val(object.money_back_guarantee_id)
        $("#kt_penerimaan").modal("show");
    })
    $("#kt_penerimaan_submit").click(function (e) {
        var form = $("#kt_penerimaan_produksi")
        var btn = $(this)
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload();
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    })
})