$(document).ready(function () {
    var base_url = $("#base_url").val();
    $("#child_data_ajax").on('click','.claim-btn',function () {
        var enc_json = $(this).siblings('textarea').val();
        var data = JSON.parse(enc_json);
        swal.fire({
            title: "Perhatian ...",
            text: "Yakin akan mengkalim reward ini? Omset akan dikurangi sesudah diklaim",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#0abb87",
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                claim(data.reward_id);
            }
        });
    });
    function claim(reward_id) {
        $.ajax({
           type:'post',
           data:{'reward_id':reward_id},
           url:base_url+'reward/claim',
           success:function (response) {
                var data = JSON.parse(response);
                if(data.success){
                    swal.fire({
                        type: 'success',
                        title: 'Berhasil',
                        text: data.message
                    }).then(function () {
                        window.location.reload();
                    });
                }else {
                    swal.fire({
                        type: 'error',
                        title: 'Gagal',
                        text: data.message
                    });
                }
           }
        });
    }
});