

// Class Definition
var KTMaps = function () {
  var general = function () {
    var base_url = $("#base_url").val();

    $("#kt_add_submit_maps").click(function () {
      var btn = $(this);
      var form = $("#kt_add_maps");

      form.validate({})
      if (!form.valid()) {
        return false;
      }
      btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
      form.ajaxSubmit({
        url: form.attr("action"),
        beforeSend: function () {
          $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
        },
        success: function (response, status, xhr, $form) {
          console.log(response);
          var data = jQuery.parseJSON(response);
          if (data.success) {
            swal.fire({
              type: 'success',
              title: 'Koordinat Outlet berhasil disimpan',
            }).then(function () {
              window.location.href = base_url + "profile";
            });
          } else {
            swal.fire({
              type: 'error',
              text: data.message,
              showConfirmButton: false,
              timer: 1500
            });
          }
          $('.wrapper-loading').fadeOut().addClass('hidden');
        },
        error: function (xhr, ajaxOptions, thrownError) {
          $('.wrapper-loading').fadeOut().addClass('hidden');
          console.log(xhr.status);
          console.log(xhr.responseText);
          console.log(thrownError);
        }
      });

    })

  }
  // Public Functions
  return {
    // public functions
    init: function () {
      general();
    }
  };
}();

// Class Initialization
jQuery(document).ready(function () {
  loading_start();
  KTMaps.init();
});
document.addEventListener('DOMContentLoaded', () => {
  $('.input-numeral').toArray().forEach(function (field) {
    new Cleave(field, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    })
  });
});

function loading_start() {
  $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
}

function loading_finish() {
  $('.wrapper-loading').fadeOut().addClass('hidden');
}