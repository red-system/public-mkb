"use strict";

// Class Definition
var KTPoProduk = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var min_qty = $("#minimal_po").val();
        var first_termin = $("#first_termin").val();
        var no = 0;
        var termin = $("#termin").length > 0 ? (intVal($("#termin").val())+1) :  1;
        var user_role = $("#user_role").val();
        var first_deposit = $("#first_deposit").val();
        var is_frezze = $("#is_frezze").length > 0 ? $("#is_frezze").val() : 0;
        var nominal_frezze = $("#nominal_frezze").length > 0 ? $("#nominal_frezze").val() : 11000000;
        if($("#index_no").length>0){
            no = $("#index_no").val();
        }
        var produk_no = 0;
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $("#terminTidak").click(function () {
            termin = 1;
        })
        $("#terminYa").click(function () {
            termin = 2;
        })

        $("#suplier_id").change(function(){
            var code= $(this).find(':selected').data('suplier');
            $.ajax({
                type: "POST",
                url: base_url+'po-produk/utility/get-po-no',
                data:{'suplier_kode':code},
                cache: false,
                success: function(response){
                    $("#display_po_no").html(response);
                    $("#input_po_no").val(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        })
        $("#child_data_ajax").on('click','.stock-btn',function () {
            var text = $(this).siblings("textarea").val();
            var object = JSON.parse(text);
            var po_produk = object
            console.log(object)
            $.ajax({
                url:base_url+'po-produk/pay',
                type:'post',
                data:{'po_produk_id':object.po_produk_id},
                success:function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        snap.pay(data.token,{
                            onSuccess: function(result){
                                console.log("success");
                                response_pay("success",po_produk.po_produk_id,result);
                            },
                            onPending: function(result){
                                console.log(text);
                                response_pay("pending",po_produk.po_produk_id,result);
                            },
                            onError: function(result){
                                response_pay("error",po_produk.po_produk_id,result);
                            },
                            onClose:function () {
                            }
                        })    
                    } else{
                        var response_payment = data.response_payment;
                        var object = JSON.parse(response_payment.json);
                        var count = 1;
                        var mid = Math.round(Object.keys(object).length/2)
                        $("#left").html('')
                        $("#right").html('')
                        $.each(object,function (i,value) {
                            if(!validURL(value)){
                                if(typeof value == "object"){
                                } else {
                                    var label = i.toUpperCase();
                                    label = label.replace('_', ' ');
                                    console.log(typeof value)
                                    var val = value.toUpperCase();
                                    val = val.replace('_', ' ');
                                    var text = '<div class="form-group row">' +
                                        '<label for="example-text-input" class="col-3 col-form-label">'+label+'</label>' +
                                        '<label for="example-text-input" class="col-1 col-form-label">:</label>' +
                                        '<div class="col-8">' +
                                        '<label name="jenis_pembayaran" class="col-form-label" style="word-break: break-all">'+val+'</label>' +
                                        '</div>' +
                                        '</div>';
                                    if(count<=mid){
                                        $("#left").append(text)
                                    }else{
                                        $("#right").append(text)
                                    }
                                }
                            }
                            count++;
                        })
                        $("#kt_modale_detail_pembayaran").modal("show");
                    }
                }
            })
        })
        function validURL(str) {
            try {
                new URL(str);
            } catch (_) {
                return false;
            }
            return true;
        }
        function detail_response() {
        }
        function response_pay(status,po_produk_id,json){
            $.ajax({
                url:base_url+"po-produk/response-payment",
                type:"post",
                data:{'status':status,'po_produk_id':po_produk_id,'json':json},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success:function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    window.location.reload()
                },
            })
        }
        $("#add_item").click(function(){
            var text = '<div class="row" id="produk_item_'+no+'">' +
                '<div class="col-md-3">' +
                '<div class="form-group row">' +
                    '<label for="example-text-input" class="col-3 col-form-label">produk</label>' +
                    '<div class="col-9">' +
                        '<div class="input-group col-12">'+
                            '<input type="text" class="form-control readonly" name="item_produk_nama_'+no+'" id="item_produk_nama_'+no+'" required="" autocomplete="off">'+
                            '<input type="hidden" class="form-control row_id" id="item_produk_id_'+no+'" name="item_produk[produk_'+no+'][produk_id]"><div class="input-group-append">'+
                            '<input type="hidden" class="form-control" id="input_last_hpp_'+no+'" name="item_produk[produk_'+no+'][last_hpp]">'+
                            '<input type="hidden" class="form-control" id="input_produk_harga_form_'+no+'" name="item_produk[produk_'+no+'][harga]">'+
                            '<button class="btn btn-primary produk-search" type="button" data-toggle="modal" data-no="'+no+'"><i class="flaticon-search"></i></button></div></div>'+
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-md-2">' +
                    '<div class="form-group row"><label for="example-text-input" class="col-6 col-form-label">Harga'+
            '</b></label>' +
                '<label for="example-text-input" class="col-6 col-form-label" data-no="'+no+'"  id="item_produk_harga_'+no+'"></label>'+
            '</div></div>' +
                '<div class="col-md-2"><div class="form-group row"><label for="example-text-input" class="col-3 col-form-label">Jumlah'+
            '</label><div class="col-6"><input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_jumlah_'+no+'" name="item_produk[produk_'+no+'][jumlah]">'+
            '</div><label for="example-text-input" class="col-3 col-form-label" id="item_produk_satuan_'+no+'">'+
            '</label></div></div><div class="col-md-2"><div class="form-group row"><label for="example-text-input" class="col-6 col-form-label">Subtotal</label><div class="col-6">'+
            '<label class="col-form-label" id="item_produk_subtotal_display_'+no+'">0</label><input type="hidden" class="form-control input-numeral subtotal" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_subtotal_'+no+'" name="item_produk[produk_'+no+'][subtotal]">'+
            '</div></div></div><div class="col-md-1"><button type="button" class="btn btn-danger btn-sm delete-produk" data-no="'+no+'" data-item-produk="produk_item_'+no+'"><i class="flaticon2-trash"></i>'+
            '</button></div></div>';
            $("#item-container").append(text);
            new Cleave($("#item_produk_jumlah_"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            produk_no =no;
            $("#kt_modal_produk").modal("show");
            no++;
        })
        $("#kt_add_submit").click(function(e){
            e.preventDefault();
            var btn = $(this);
            var form = $("#kt_add")
            var is_zero = false;
            var ceckBelanja = cekTotalBelanja()
            if(!ceckBelanja){
                return false;
            }
            $("#item-container .input-numeral").each(function(){
                if($(this).val()==0){
                    is_zero = true;
                }
            })
            if(is_zero){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Tidak boleh ada jumlah maupun harga yang bernilai 0',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            if($("#item-container").children().length == 0 ){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Order harus memiliki setidaknya satu produk',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            form.validate({})
            if(!form.valid()){
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        window.location.href = base_url+"po-produk";
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })
        $("#kt_modal_produk").on("hide.bs.modal",function () {
            if($("#item_produk_id_"+produk_no).val()==""){
                $("#produk_item_"+produk_no).remove();
            }
        })
        $("#item-container").on('keydown','.readonly',function(e){
            e.preventDefault();
        })
        $('input[type=radio][name=jenis_pembayaran]').change(function() {
            if($(this).val() == "kas"){
                $("#kas_container").css('display','block');
                $(".tipe-kas").prop('dissabled',false);
                $("#kredit_container").css('display','none');
                $(".tipe-kredit").prop('dissabled',true);                
            } else {
                $("#kas_container").css('display','none');
                $(".tipe-kas").prop('dissabled',true);
                $("#kredit_container").css('display','block');
                $(".tipe-kredit").prop('dissabled',false);                  
            }
        })
        $(".tipe-kas").change(function () {
            var atas_nama = $(this).find("option:selected").data("atasnama")
            var no_akun = $(this).find("option:selected").data("noakun")
            $("#atas_nama").html(atas_nama)
            $("#no_akun").html(no_akun)
        })
        $(".input-numeral").keyup(function(){
            if($(this).val()== ""){
                $(this).val(0)
            }
        })
        $("#item-container").on("click",".delete-produk",function(){
            var index = $(this).data("no");
            $("#produk_item_"+index).remove();
            get_subtotal();
        })
        function intVal(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };         
        $("#item-container").on('keyup','.jumlah-produk',function(){
            var val = intVal($(this).val())
            if((val)<0){
                val = -1*val
            }
            $(this).val(KTUtil.numberString(val))
            var index = $(this).data('no');
            var subtotal = intVal($(this).val()) * intVal($("#item_produk_harga_"+index).html())
            $("#item_produk_subtotal_"+index).val(subtotal);
            $("#item_produk_subtotal_display_"+index).html(KTUtil.numberString(subtotal.toFixed(0)));
            get_subtotal();
        })
        $("#item-container").on('change','.jumlah-produk',function(){

        })
        $("#item-container").on('keyup','.harga-produk',function(){
            var index = $(this).data('no');
            var subtotal = intVal($(this).val()) * intVal($("#item_produk_jumlah_"+index).val())
            $("#item_produk_subtotal_"+index).val(subtotal);
            $("#item_produk_subtotal_display_"+index).html(KTUtil.numberString(subtotal.toFixed(0)));
            get_subtotal();
        })
        function get_subtotal(){
            var total = 0;
            $( "#item-container .subtotal" ).each(function() {
                total += intVal($(this).val());
            })
            $("#total_item").html(KTUtil.numberString(total.toFixed(0)));
            $("#input_total_item").val(total);
            total = total + intVal($("#tambahan").val());
            total = total - intVal($("#potongan").val());            
            $("#grand_total").html(KTUtil.numberString(total.toFixed(0)));
            $("#input_grand_total").val(total);
        }
        function cekTotalBelanja(){
            var total = $("#input_grand_total").val() == "" ? 0 : $("#input_grand_total").val();
            total = intVal(total)
            var total_termin = (min_qty)
            if(is_frezze==0){
                if(termin == 2){
                    total_termin = first_termin;
                }
            }else{

                total_termin = nominal_frezze;
                console.log(total_termin)
            }

            if(total_termin>total){
                swal.fire({
                    type: 'error',
                    text:"Total belanja kurang dari minimal belanja yakni Rp." +KTUtil.numberString(total_termin),
                    showConfirmButton: true,
                });
                return false;
            }else{
                return true
            }
        }
        $("#potongan").keyup(function(){
            get_subtotal();
        })
        $("#tambahan").keyup(function(){
            get_subtotal();
        })
        $('#item-container').on('click','.produk-search',function(){ 
            produk_no = $(this).data('no');
            $("#kt_modal_produk").modal("show");
        });
        function checkProduk(produk_id){
            var result = true;
            $.each($(".row_id"),function (i,val) {
                if($(val).val()==produk_id){
                    result = false
                    return false;
                }
            })
            return result;
        }
        $('#produk_child').on('click','.chose-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            if(checkProduk(object.produk_id)){
                $("#item_produk_nama_"+produk_no).val(object.produk_nama);
                $("#item_produk_harga_"+produk_no).html(KTUtil.numberString(object.last_hpp));
                $("#item_produk_id_"+produk_no).val(object.produk_id);
                $("#item_produk_satuan_"+produk_no).html(object.satuan_nama);
                $("#last_hpp_"+produk_no).html('Rp. '+KTUtil.numberString(object.last_hpp));
                $("#input_last_hpp_"+produk_no).val(object.last_hpp);
                $("#input_produk_harga_form_"+produk_no).val(object.last_hpp);

                $("#item_produk_jumlah_"+produk_no).val(1);
                var subtotal = intVal(object.last_hpp) * intVal($("#item_produk_jumlah_"+produk_no).val())
                $("#item_produk_subtotal_"+produk_no).val(subtotal);
                $("#item_produk_subtotal_display_"+produk_no).html(KTUtil.numberString(subtotal.toFixed(0)));
                get_subtotal();
            }
            $("#kt_modal_produk").modal("hide");
        });
        $(".img-input").change(function(){
            var display = $(this).data('display');
            readURL(this,display);
        })
        function readURL(input,display) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+display).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        var url_produk = $("#list_produk").val();
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: url_produk,
            columns:[{data:'produk_kode'},{data:'produk_nama'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        }); 
        $('#child_data_ajax').on('click','.view-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.po_produk_id;
            get_detail(id)
        });
        function get_detail(id,type = 0){
            $.ajax({
                type: "POST",
                url: base_url+'po-produk/detail',
                data:{'po_produk_id':id},
                cache: false,
                success: function(response){
                    if(type==0){
                        parseDetail(response);    
                    } else {
                       // display_penerimaan(response)
                    } 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        } 

        $('#child_data_ajax').on('click','.penerimaan-status-btn',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            var id = object.enc_row_id;
            var url = base_url + 'order-super-agen/penerimaan/'+id;
            window.location.href = url;
        });  

        $('#child_data_ajax').on('click','.penerimaan-status-btn-hide',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda yakin customer sudah menerima produk?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87",
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {

                    $.ajax({
                        url: base_url+"order-super-agen/accept",
                        type: "post",
                        data:{"id":object.row_id},
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function (response) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                                window.location.reload()
                            }else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        });        
        function parseDetail(response){
            var object = JSON.parse(response);
            $.each(object,function(key,value){
                $('#kt_modal_detail_po_produk label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_po_produk [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_po_produk .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_po_produk .img-preview').attr("src",base_url+value);
                }
            })
            $("#show_bukti").attr("data-url",object.bukti_url)

            $("#view_child_data").html("");
            $.each(object.item,function(key,value){
                var text = '<tr>'+
                '<td >'+value.produk_nama+'</td>'+
                '<td >'+value.harga+'</td>'+
                '<td >'+value.jumlah_qty+'</td>'+
                '<td >'+value.sub_total+'</td></tr>';
                $("#view_child_data").append(text);
            })
            $("#kt_modal_detail_po_produk").modal("show");            
        }
        $("#show_bukti").click(function () {
            window.open($(this).data("url"),'__blank');
        })
        $('#child_data_ajax').on('click','.accept-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda sudah memeriksa dengan seksama?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87",
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {

                    $.ajax({
                        url: base_url+"order-super-agen/accept",
                        type: "post",
                        data:{"id":object.row_id},
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function (response) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                               window.location.reload()
                            }else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        });
        $('#child_data_ajax').on('click','.refuse-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#po_produk_id_refuse").val(object.row_id);
            $("#kt_refuse").modal("show");
        });
        $('#child_data_ajax').on('click','.detail-pembayaran-btn',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            $.ajax({
                url:base_url+'po-produk/pay',
                type:'post',
                data:{'po_produk_id':object.po_produk_id},
                success:function (response) {
                    var data = JSON.parse(response)
                    var response_payment = data.response_payment;
                    var object = JSON.parse(response_payment.json);
                    var count = 1;
                    var mid = Math.round(Object.keys(object).length/2)

                    $("#left").html('')
                    $("#right").html('')
                    $.each(object,function (i,value) {
                        if(!validURL(value)){
                            if(typeof value == "object"){

                            } else {
                                var label = i.toUpperCase();
                                label = label.replace('_', ' ');
                                console.log(typeof value)
                                var val = value.toUpperCase();
                                val = val.replace('_', ' ');
                                var text = '<div class="form-group row">' +
                                    '<label for="example-text-input" class="col-3 col-form-label">'+label+'</label>' +
                                    '<label for="example-text-input" class="col-1 col-form-label">:</label>' +
                                    '<div class="col-8">' +
                                    '<label name="jenis_pembayaran" class="col-form-label" style="word-break: break-all">'+val+'</label>' +
                                    '</div>' +
                                    '</div>';
                                if(count<=mid){
                                    $("#left").append(text)
                                }else{
                                    $("#right").append(text)
                                }
                            }

                        }
                        count++;

                    })
                    $("#kt_modale_detail_pembayaran").modal("show");
                }
            })
        });
        $('#child_data_ajax').on('click','.ulangi-pembayaran-btn',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            var po_produk = object
            //here
            $.ajax({
                url:base_url+'po-produk/ulangi-pembayaran',
                type:'post',
                data:{'po_produk_id':object.po_produk_id},
                success:function (response) {
                    var data = JSON.parse(response)
                    snap.pay(data.token,{
                        onSuccess: function(result){
                            console.log("success");
                            // Swal.fire({
                            //     type: 'success',
                            //     title: 'Success',
                            //     text: 'Proses Pembayaran Berhasil'
                            // }).then(function() {
                            //     form.clearForm();
                            //     form.validate().resetForm();
                            //     var url = data.url;
                            //     window.open(url,"_blank");
                            //     clearForm();
                            // });
                            response_pay("success",po_produk.po_produk_id,result);
                        },
                        onPending: function(result){
                            console.log(text);
                            response_pay("pending",po_produk.po_produk_id,result);
                            // Swal.fire({
                            //     type: 'success',
                            //     title: 'Pending',
                            //     text: 'Menunggu Pembayaran'
                            // }).then(function() {
                            //     form.clearForm();
                            //     form.validate().resetForm();
                            //     var url = data.url;
                            //     window.open(url,"_blank");
                            //     clearForm();
                            // });
                        },
                        onError: function(result){
                            response_pay("error",po_produk.po_produk_id,result);
                            // Swal.fire({
                            //     type: 'error',
                            //     title: 'Error',
                            //     text: 'Proses Pembayaran Gagal'
                            // })
                        },
                        onClose:function () {

                        }
                    })
                }
            })
        });
        $("#kt_produksi_penerimaan_submit").click(function(){
            var form = $("#kt_penerimaan_produksi") ;
            form.submit();
        })

        $('#kt_po_penerimaan_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    password:{
                        required: true,
                        minlength:5,
                    }, 
                    re_password:{
                        required: true,
                        minlength:5,
                        equalTo:'#password'
                    }
                }
            });
            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                     btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        window.location.reload();
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        $('#kt_po_refuse_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),

                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        // window.location.reload();
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        $.ajax({
                                url: base_url+"suplier/option",
                                type: "post",
                                success: function (response) {
                                    states = jQuery.parseJSON(response);
                                    $('#kt_typeahead_1').typeahead({
                                        hint: true,
                                        highlight: true,
                                        minLength: 1
                                    }, {
                                        name: 'states',
                                        source: substringMatcher(states)
                                    });
                                     $('#kt_typeahead_2').typeahead({
                                        hint: true,
                                        highlight: true,
                                        minLength: 1
                                    }, {
                                        name: 'states',
                                        source: substringMatcher(states)
                                    });
                                },
                                error: function(request) {
                                }
                            });                                  
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPoProduk.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});