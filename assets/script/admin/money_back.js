$(document).ready(function () {
    var base_url = $("#base_url").val();
    $("#ajukan").click(function () {
        swal.fire({
            title: "Perhatian ...",
            text: "Apakah anda yakin mengajukan money back guarantee ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#0abb87",
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url:base_url+'money-back/submit',
                    type:'get',
                    success:function (response) {
                        var data = JSON.parse(response)
                        if(data.success){
                            window.location.reload()
                        }
                    }
                })
            }
        });
    })
    $("#batalkan").click(function () {
        $.ajax({
            url:base_url+'money-back/batal',
            type:'get',
            success:function (response) {
                var data = JSON.parse(response)
                if(data.success){
                    window.location.reload()
                }
            }
        })
    })
    $("#print").click(function () {
        window.open(base_url+'money-back/print')
    })
})