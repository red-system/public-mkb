$(document).ready(function () {
    var base_url = $("#base_url").val()
    $("#child_data_ajax").on("click",".approve-btn",function () {
        var json = $(this).siblings("textarea").val()
        var object = JSON.parse(json);

        $.ajax({
            url:base_url+"manage-retur/approve",
            type:"post",
            data:{"id":object.retur_agen_id},
            success:function (response) {
                var data = jQuery.parseJSON(response);
                if (data.success) {
                    window.location.reload();
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }
        })
    })
    $("#child_data_ajax").on("click",".refuse-btn",function () {

        var json = $(this).siblings("textarea").val()
        var object = JSON.parse(json);
        $.ajax({
            url:base_url+"manage-retur/refuse",
            type:"post",
            data:{"id":object.retur_agen_id},
            success:function (response) {
                var data = jQuery.parseJSON(response);
                if (data.success) {
                    window.location.reload();
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }
        })
    })
})