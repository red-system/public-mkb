"use strict";

// Class Definition
var KTPenerimaanProduk = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var general_url = $("#general_data_url").val();
        $("#add_btn").click(function(){
            get_general_data();
            
        })
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });        
        function get_general_data(type = 0,sibling = null){
            $.ajax({
                async:false,
                type: "POST",
                url: general_url,
                cache: false,
                success: function(response){
                    if(type==0){
                        parse_add(response);
                    } else {
                        parse_edit(response,sibling)
                    }
                    
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });             
        } 
        function parse_add(response){
            var data = jQuery.parseJSON(response);
            var penerimaan = data.result;
            $("#hpp_container").html("");
            $.each(penerimaan,function(key,value){

                if($.isNumeric(value)){
                    value = KTUtil.numberString(parseInt(value).toFixed(0))
                }
                $('#kt_modal_add label[name="' + key + '"]').html(value);
                $('#kt_modal_add input[name="' + key + '"]').val(value);

            })
            $("#kt_modal_add").modal("show");
        }

        function display_penerimaan(response){
            var object = JSON.parse(response);
            $("#hpp_container").html("");
            $.each(object.produksi.item,function(key,value){
                var text = '<div class="form-group row">'+
                '<label for="example-text-input" class="col-3 col-form-label">HPP '+value.produk_nama+'</label>'+
                '<label for="example-text-input" class="col-1 col-form-label">:</label>'+
                '<div class="col-8">'+
                // '<input type="hidden" name="jumlah[produk_'+value.produk_id+']" value="'+value.jumlah+'" />'+
                '<input type="text" class="form-control kt-input input-numeral" id="hpp_'+value.produk_id+'" name="hpp[produk_'+value.produk_id+']" autocomplete="off" data-col-index="5" value="0" required="" />'+
                '</div>'+
                '</div>';
                $("#hpp_container").append(text);
                new Cleave($("#hpp_"+value.produk_id), {
                    numeral: true,
                    numeralThousandsGroupStyle: 'thousand'
                })                

            })
            $("#kt_penerimaan").modal("show");
        }   

        function parse_edit(response,sibling){
            var json = sibling.siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                var type = $('[name="' + key + '"]').attr('type');
                if (type == 'file') {
                    $('#kt_modal_edit_hp .img-preview').attr('src', base_url+value);
                } else if (type == 'radio') {
                    $('#kt_modal_edit_hp [name="' + key + '"][value=' + value + ']').attr('checked', 'checked');
                } else {
                    if(key !="password"){
                        $('#kt_modal_edit_hp [name="' + key + '"]').val(value);
                        $('#kt_modal_edit_hp [name="' + key + '"]').trigger('change');
                        $(".input-numeral").trigger('change');
                    }
                    
                }
            }) 
            var data = jQuery.parseJSON(response);
            var hutang = data.result;
            $.each(hutang,function(key,value){

                if($.isNumeric(value)){
                    value = KTUtil.numberString(parseInt(value).toFixed(0))
                }
                $('#kt_modal_edit_hp label[name="' + key + '"]').html(value);
                $('#kt_modal_edit_hp input[name="' + key + '"]').val(value);

            })
            $("#kt_modal_edit_hp").modal("show");
        }        
        $("#child_data_ajax").on('click','.edit-btn',function(){
            get_general_data(1,$(this));

        })

        $('#child_data_ajax').on('click','.submit-btn',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda yakin customer sudah menerima produk?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87",
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {

                    $.ajax({
                        url: base_url+"produksi/accept-stock",
                        type: "post",
                        data:{"id":object.row_id},
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function (response) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                                window.location.reload()
                                // console.log(data)
                            }else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        });

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPenerimaanProduk.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});