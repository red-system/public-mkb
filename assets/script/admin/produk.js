"use strict";

// Class Definition
var KTProduk = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        $("#child_data_ajax").on('click','.view-btn',function(){

            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#kt_modal_detail_produk label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_produk [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_produk .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_produk .img-preview').attr("src",base_url+value);
                }
            })
            getdetail(object.produk_id);
            getdetail_item(object.produk_id)
            
        })
        function getdetail(id){
            $.ajax({
                type: "POST",
                url: base_url+'produk/harga/'+id,
                cache: false,
                success: function(response){
                    parseHarga(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function getdetail_item(id){
            $.ajax({
                type: "POST",
                url: base_url+'produk/item_produk/'+id,
                cache: false,
                success: function(response){
                    parseItem(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function parseHarga(response){
            var object = JSON.parse(response);
            $("#view_child_data").html("");
            if(object.length > 0){
                $.each(object,function(i,value){
                    var text = "<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td>"+(value.minimal_pembelian)+"</td>"+
                                    "<td>"+(value.harga)+"</td>"+
                               "</tr>";
                    $("#view_child_data").append(text);
                })
            } else {
                $("#view_child_data").append('<tr><td colspan="3" style="text-align: center;">Tidak ada data</td></tr>');
            }
            $("#kt_modal_detail_produk").modal("show");
        }
        function parseItem(response){
            var object = JSON.parse(response);
            console.log(object);
            $("#view_child_data_item").html("");
            if(object.length > 0){
                $.each(object,function(i,value){
                    var text = "<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td width='60%'>"+(value.produk_nama)+"</td>"+
                                    "<td>"+(value.jumlah)+"</td>"+
                               "</tr>";
                    $('.item_produk_display').css('display','block');
                    $("#view_child_data_item").append(text);
                })
            } else {
                $('.item_produk_display').css('display','none');
                $("#view_child_data_item").append('<tr><td colspan="3" style="text-align: center;">Tidak ada data</td></tr>');
            }
            $("#kt_modal_detail_produk").modal("show");
        }
        $("#child_data_ajax").on('click','.barcode-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#barcode_seri").val(object.barcode);
            $("#barcode_modal").modal("show");
        })
        $("#print_barcode").click(function () {
            var url = base_url+"stock-produk/print-barcode/"+$("#barcode_seri").val()+"/"+$("#jumlah_cetak").val()+'/'+$("#printer_setting").val();
            window.open(url, '_blank');
            $("#barcode_modal").modal("hide");
            $("#jumlah_cetak").val("0");
        })
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();
$('#child_data_ajax').on('click', '.edit-assembly-btn', function (e) {
    e.preventDefault();

    var json = $(this).siblings('textarea').val();
    var object = JSON.parse(json);
    var direct = $(this).data('direct');
    var check = $(this).data('check');
    var href = $(this).attr('href');
    console.log(direct)
    if (check != '' || check != 0) {
        swal.fire({
            title: "Perhatian",
            html: "Produk Ini Masih Memiliki Stock, Jika Anda Melakukan Pengeditan, Maka Akan Dilakukan Penghapusan Stock Saat Menekan Tombol Save. <br>Apakah anda yakin akan melanjutkan?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Tidak",
        }).then(function (e) {
            if (e.value) {
                window.location.href = direct;
            } else {

            }
        });
    } else {
        if (direct != "") {
            window.location.href = direct;
        }
    }
}); 

// Class Initialization
jQuery(document).ready(function() {
    KTProduk.init();
});