"use strict";

// Class Definition
var KTRetur = function() {
    var base_url = $("#base_url").val();
    var type = $("#type").val();
    function intVal(i) {
        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
    };
    function intValIdr(i) {
        var rplc =  typeof i === 'string' ? i.replace('Rp. ','') : i;
        var temp  = typeof rplc === 'string' ? rplc.replace(/[\$,]/g, '') * 1 : typeof rplc === 'number' ? rplc : 0;

        return temp
    };
    var retur = function(){
        var no_item = $("#no_item").val();

        var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };
        var list_url = $("#list_url").val();
        var po_table = $(".datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: list_url,
            columns:[{data:"no"},{data:'produk_nama'},{data:'produk_kode'},{data:'jumlah'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $("#generalSearch").on('keyup', function(e){
            po_table.search($("#generalSearch").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                po_table.column(i).search(val ? val : '', false, false);
            });
            po_table.table().draw();
        })
        if ($.fn.datepicker) {

            $('.tanggal').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                format: 'yyyy-mm-dd',
            });
        }

        $("#id_lokasi").change(function () {
            var lokasi_id = $(this).val()
            $("#po_produk_no").val('')
            $("[name=input_po_id]").val('')
            $("#suplier").html('')
            $("#jumlah_item_po").html(KTUtil.numberString("0"))
            $("#hpp_stock").html(KTUtil.numberString("0"))
            $("#input_hpp").val('')
            $("#view_child_data").html('')
            $("#total_retur").html('Rp. 0')
            $("#input_total_retur").val('0')
            $.ajax({
                type: "POST",
                url: base_url+'retur-gantung/change-location',
                data:{'lokasi_id':lokasi_id},
                cache: false,
                success: function(response){

                    //po_table.ajax.reload();
                    $("#kt_typeahead_1").trigger('change');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })

        $(".po-search").click(function () {
            $("#kt_modal_search_po").modal('show');
        })
        function itemDisplay(item) {
            $("#view_child_data").html('')
            $.each(item,function (i,val) {
                var text = '<tr>' +
                    '<td>'+val.produk_nama+'</td>'+
                    '<td style="text-align: right" id="harga_produk_'+val.po_produk_detail_id+'" data-no="'+val.po_produk_detail_id+'" class="harga-item-retur">Rp. '+KTUtil.numberString(val.harga)+'</td>'+
                    '<td style="text-align: right" data-no="'+val.po_produk_detail_id+'" class="jumlah-item-po">'+val.jumlah+'</td>'+
                    '<td style="text-align: right" data-no="'+val.po_produk_detail_id+'" class="jumlah-item-stock">'+val.jumlah_stock+'</td>'+
                    '<td><input type="text" name="item[_'+val.po_produk_detail_id+'][jumlah]" data-no="'+val.po_produk_detail_id+'" class="form-control jumlah-item-retur" data-max-po="'+val.jumlah+'"  data-max-stock="'+val.jumlah_stock+'" id="jumlah_retur_'+val.po_produk_detail_id+'" value="0"></td>'+
                    '<td class="subtotal"  id="subtotal_'+val.po_produk_detail_id+'">Rp. 0</td>'+
                    '<td>' +
                    '<textarea class="form-control" name="item[_'+val.po_produk_detail_id+'][keterangan]"></textarea>' +
                    '<input type="hidden" data-no="'+val.po_produk_detail_id+'" id="input_produk_id_'+val.po_produk_detail_id+'" name="item[_'+val.po_produk_detail_id+'][produk_id]" value="'+val.produk_id+'">'+
                    '<input type="hidden" data-no="'+val.po_produk_detail_id+'" id="input_subtotal_'+val.po_produk_detail_id+'" name="item[_'+val.po_produk_detail_id+'][subtotal]">'+
                    '<input type="hidden" data-no="'+val.po_produk_detail_id+'" id="input_harga_'+val.po_produk_detail_id+'" name="item[_'+val.po_produk_detail_id+'][harga]" value="'+val.harga+'">'+
                    '</td>'+
                    '</tr>';
                $("#view_child_data").append(text);
                new Cleave($("#jumlah_retur_"+val.po_produk_detail_id), {
                    numeral: true,
                    numeralThousandsGroupStyle: 'thousand',
                })
                subtotal(val.po_produk_detail_id)
            })

        }
        function subtotal(no){
            var harga = intVal($("#harga_retur_"+no).val());
            var jumlah = intVal($("#jumlah_retur_"+no).val());
            var subtotal = harga * jumlah
            $("#subtotal_"+no).html("Rp. "+KTUtil.numberString(subtotal));
            $("#subtotal_retur_"+no).val(subtotal);
            total()
        }
        function total() {
            var total_temp = 0
            $.each($(".subtotal"),function (i,val) {
                total_temp += parseFloat(intValIdr($(val).html()));
            })
            $("#total_retur").html("Rp. "+KTUtil.numberString(total_temp))
            $("#input_total_retur").val(total_temp)
        }
        $("#view_child_data").on('keyup','.jumlah-item-retur',function () {
            var no = $(this).data('no')
            var val = intVal($(this).val()),
                maxPO = $(this).data('max-po'),
                maxStok = $(this).data('max-stock'),
                max = maxStok < maxPO ? maxStok : maxPO
            if(val>maxPO||val>maxStok){
                $(this).val(KTUtil.numberString(max))
                swal.fire({
                    title:'Error',
                    type:"error",
                    text:"Jumlah Melebihi Jumlah pembelian atau jumlah stok saat ini",
                    showConfirmButton:false,
                    timer:1500
                })
            }
            subtotal(no)
        })
        if($.fn.typeahead){
            var loadTypeahead = function (){
                $.ajax({
                    url: base_url+"suplier/option",
                    type: "post",
                    success: function (response) {
                        states = jQuery.parseJSON(response);
                        $('#kt_typeahead_1').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        }, {
                            name: 'states',
                            source: substringMatcher(states)
                        });
                        $('#kt_typeahead_2').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        }, {
                            name: 'states',
                            source: substringMatcher(states)
                        });
                    },
                    error: function(request) {
                    }
                });
            }
            loadTypeahead()
        }
        $("#kt_typeahead_1").change(function () {
            po_table.search($("#generalSearch").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }


            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                po_table.column(i).search(val ? val : '', false, false);
            });
            po_table.table().draw();
        })
        $("#add_item").click(function () {
            $("#kt_modal_search_po").modal('show');
        })
        $("#child_data_ajax").on('click','.chose-btn',function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var temp ='<tr id="row_'+no_item+'">' +
                        '<td>'+object.produk_nama+'</td>'+
                '<td id="jumlah_stock_'+no_item+'">'+object.jumlah+'</td>'+
                '<td><input type="text" class="input-numeral form-control jumlah-retur" data-no="'+no_item+'" id="jumlah_retur_'+no_item+'" name="item[_'+no_item+'][jumlah_retur]" value="0"></td>'+
                '<td><input type="text" class="input-numeral form-control harga-retur" data-no="'+no_item+'" id="harga_retur_'+no_item+'" name="item[_'+no_item+'][harga_retur]" value="0"></td>'+
                '<td id="subtotal_'+no_item+'" class="subtotal">Rp. 0</td>'+
                '<td><textarea class="form-control" data-no="'+no_item+'" id="keterangan_retur_'+no_item+'" name="item[_'+no_item+'][keterangan]"></textarea>' +
                    '<input type="hidden" class="input-numeral form-control " data-no="'+no_item+'" id="produk_id_'+no_item+'" name="item[_'+no_item+'][produk_id]" value="'+object.produk_id+'">'+
                    '<input type="hidden" class="input-numeral form-control " data-no="'+no_item+'" id="subtotal_retur_'+no_item+'" name="item[_'+no_item+'][subtotal]">'+
                '</td>'+
                '<td><button type="button" class="btn btn-danger float-right delete-item" data-no="'+no_item+'"><i class="fa fa-trash"></i></button></td>'+
                '</tr>'
            $("#view_child_data").append(temp)
            console.log()

            new Cleave($("#jumlah_retur_"+no_item), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
            })
            new Cleave($("#harga_retur_"+no_item), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
            })
            no_item++;
            $("#kt_modal_search_po").modal('hide');
        })
        $("#view_child_data").on('keyup','.jumlah-retur',function () {
            var no = $(this).data('no')
            var value = intVal($(this).val())
            var jumlah_stock = intVal($("#jumlah_stock_"+no).html())
            console.log(jumlah_stock)
            if(value>jumlah_stock){
                $(this).val(KTUtil.numberString(jumlah_stock))
                swal.fire({
                    title:'Error',
                    type:"error",
                    text:"Jumlah melebihi jumlah stok saat ini",
                    showConfirmButton:false,
                    timer:1500
                })
            }
            subtotal(no)

        })
        $("#view_child_data").on('keyup','.harga-retur',function () {
            var no = $(this).data('no')
            subtotal(no)

        })
        $("#view_child_data").on('click','.delete-item',function () {
            var no = $(this).data("no");
            $("#row_"+no).remove();
        })
        $("#kt_typeahead_1").keyup(function () {
            $(this).trigger('change');
            $("#view_child_data").html('')
            $("#total_retur").html('Rp. 0')
            $("#input_total_retur").val('0')
        })
        $('#kt_typeahead_1').on('typeahead:selected', function(evt, item) {
            $("#kt_typeahead_1").trigger('change')
        })
    }
    var list_retur = function () {
        $("#child_data_ajax").on('click','.posting-btn',function(e){
            e.preventDefault()
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "get",
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    if (data.success){
                        window.location.reload();
                    }else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });
        })
        $("#child_data_ajax").on('click','.view-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.ajax({
                url: base_url+"retur-gantung/detail/"+object.id,
                type: "get",
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    var data = JSON.parse(response)
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    $("#detail_tanggal").html(data.retur_gantung.tanggal)
                    $("#detail_lokasi").html(data.retur_gantung.lokasi_nama)
                    $("#detail_po").html(data.retur_gantung.po_produk_no)
                    $("#detail_suplier").html(data.retur_gantung.suplier_nama)
                    $("#detail_total").html("Rp. "+KTUtil.numberString(data.retur_gantung.total_retur))
                    $("#view_child_data").html('')
                    console.log(data.retur_gantung_detail)
                    $.each(data.retur_gantung_detail,function (i,val) {

                        var text = '<tr>' +
                            '<td>'+val.produk_nama+'</td>'+
                            '<td style="text-align: right">Rp. '+KTUtil.numberString(val.harga)+'</td>'+
                            '<td style="text-align: right">'+KTUtil.numberString(val.jumlah)+'</td>'+
                            '<td style="text-align: right">Rp. '+KTUtil.numberString(val.sub_total_retur)+'</td>'+
                            '</tr>';
                        $("#view_child_data").append(text)
                    })


                    $("#kt_modal_detail_retur").modal('show')
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });

        })
        $("#child_data_ajax").on('click','.potong-hutang-btn',function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#suplier_nama").html(': '+object.suplier_nama)
            $("#kt_modal_edit [name=retur_gantung_id]").val(object.id)
            $("#kt_modal_edit [name=po_produk_id_old]").val(object.po_produk_id)
            $("#kt_modal_edit [name=total_retur]").val(intValIdr(object.total_retur))
            $.ajax({
                url: base_url+"retur-gantung/po-hutang/"+object.suplier_id,
                type: "get",
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = JSON.parse(response)
                    $("#kt_modal_edit [name=po_produk_id]").html('<option value="">Pilih No PO</option>');
                    $.each(data,function (i,val) {

                        $("#kt_modal_edit [name=po_produk_id]").append('<option value="'+val.po_produk_id+'">'+val.po_produk_no+'</option>');
                    })
                    $("#kt_modal_edit [name=po_produk_id]").val(object.po_produk_id)
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });
            $("#kt_modal_edit").modal('show')
        })
    }

    var send = function () {
        $(".form-send").submit(function (e) {
            e.preventDefault();
            var self = $(this)
            var alert_show = self.data('alert-show');
            var alert_field_message = self.data('alert-field-message');
            if (!self.valid()) {
                return;
            }
            if(alert_show){
                swal.fire({
                    title: 'Apakah anda yakin?',
                    text: alert_field_message,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText:'Ya',
                    cancelButtonText:'Tidak'
                }).then((result) => {
                    if (result.value) {
                        form_send(self);
                    }
                })
            } else {
                form_send(self)
            }

        })
        function form_send(self) {
            var action = self.attr('action');
            var method = self.attr('method');
            var redirect = self.data('redirect');

            var data = self.serialize()
            $.ajax({
                url:action,
                type:method,
                data: data,
                success: function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        swal.fire({
                            text:data.message,
                            type:'success',
                        }).then((result) => {
                            if(result.value){
                                if(redirect!=undefined){
                                    window.location.href = redirect
                                }else{
                                    window.location.href = redirect
                                }
                            }
                        })
                    } else {
                        swal.fire({
                            title:'Error',
                            text:data.message,
                            type:'error',
                            showConfirmButton: false,
                            timer:1000
                        })
                    }
                },
                error:function (response) {

                }
            })
        }

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            if(type=='list'){
                list_retur()

            }else{
                retur()
                send()
            }

        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTRetur.init();
});