$(document).ready(function () {

    var base_url = $("#base_url").val();
    $("#child_data_ajax").on("click",".stock-btn",function () {
        var object = $(this).siblings("textarea").val()
        var data = JSON.parse(object)
        $("#bonus_id").val(data.bonus_id)
        $("#bukti_tranfer_old").val(data.bukti_transfer)
        $("[name=tanggal]").val(data.tanggal_pengiriman == null ? "" : data.tanggal_pengiriman)
        $("#displayImg3").attr("src",data.bukti_transfer == null ? base_url+"assets/media/no_image.png" : data.bukti_transfer_url)
        $("#kt_modal_pembayaran").modal("show");
    })
    $("#kt_submit_pembayaran_bonus").click(function (e) {
        e.preventDefault()
        var btn = $(this);
        var form = $("#kt_form_pembayaran");

        form.validate({
            rules: {
                password:{
                    required: true,
                    minlength:5,
                },
                re_password:{
                    required: true,
                    minlength:5,
                    equalTo:'#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                $('.error-message').remove()
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload();
                    $("input[type=number]").val(0);

                } else {
                    $.each(data.errors,function (i,val) {
                        console.log($('[name='+i+']').parent())
                        $('[name='+i+']').parent().append(val)
                    })
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    })

})