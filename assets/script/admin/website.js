"use strict";

// Class Definition
var KTProfileGeneral = function() {


    var generalForm = function(){

        var clipboard1 = new Clipboard('#click_referensi');

        clipboard1.on('success', function(e) {
            swal.fire({
                type: 'success',
                text:"link copied to clipboard",
                showConfirmButton: false,
                timer: 1500
            });
        });

        var clipboard2 = new Clipboard('#click_retail');

        clipboard2.on('success', function(e) {
            swal.fire({
                type: 'success',
                text:"link copied to clipboard",
                showConfirmButton: false,
                timer: 1500
            });
        });

    }

    // Public Functions
    return {
        // public functions
        init: function() {

            generalForm();
        }
    };

}();


// Class Initialization
jQuery(document).ready(function() {
    KTProfileGeneral.init();

});