"use strict";

// Class Definition
var KTRecipe = function() {
    var base_url = $("#base_url").val();
    var type = $("#type").val();
    function intVal(i) {
        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
    };
    function intValIdr(i) {
        var rplc =  typeof i === 'string' ? i.replace('Rp. ','') : i;
        var temp  = typeof rplc === 'string' ? rplc.replace(/[\$,]/g, '') * 1 : typeof rplc === 'number' ? rplc : 0;

        return temp
    };
    var recipe = function(){
        var no = $("#last-no").val();
        var noChose = 0;
        var produk_url = $("#produk_url").val();
        var produk_table = $(".datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_url,
            columns:[{data:"no"},{data:'produk_kode'},{data:'produk_nama'},{data:'jenis_produk_nama'},{data:null}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-produk" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $("#produkSearch").on('keyup', function(e){
            produk_table.search($("#produkSearch").val());
            var params = {};
            $('.searchProduk').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_table.column(i).search(val ? val : '', false, false);
            });
            produk_table.table().draw();
        })
        if ($.fn.datepicker) {

            $('.tanggal').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                format: 'yyyy-mm-dd',
            });
        }
        $("#id_lokasi").change(function () {
            var lokasi_id = $(this).val()
            $("#po_produk_no").val('')
            $("[name=input_po_id]").val('')
            $("#suplier").html('')
            $("#jumlah_item_po").html(KTUtil.numberString("0"))
            $("#hpp_stock").html(KTUtil.numberString("0"))
            $("#input_hpp").val('')
            $("#view_child_data").html('')
            $("#total_retur").html('Rp. 0')
            $("#input_total_retur").val('0')
            $.ajax({
                type: "POST",
                url: base_url+'retur-produk/change-location',
                data:{'lokasi_id':lokasi_id},
                cache: false,
                success: function(response){
                    console.log(response)
                    po_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })

        $(".produk-search").click(function () {
            $("#kt_modal_search_product").modal('show');
        })
        $("#produk_data_ajax").on("click",".chose-produk",function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#produk_id").val(object.produk_id)
            $("#produk_nama").val(object.produk_nama)
            $("#kt_modal_search_product").modal("hide")
        })
        var bahan_url = $("#bahan_url").val();
        var bahan_table = $(".bahan-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: bahan_url,
            // columns:[{data:"no"},{data:'bahan_kode'},{data:'bahan_nama'},{data:'jenis_bahan_nama'},{data:null}],
            columns:[{data:"no"},{data:'bahan_nama'},{data:null}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-bahan" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $("#add-item").click(function () {
            var text = "<tr id='row-"+no+"'>" +
                        "<td>" +
                            "<input type='hidden' id='bahan_id_"+no+"' data-no='"+no+"' name='recipe[_"+no+"][bahan_id]'>" +
                            '<div class="input-group">' +
                                '<input type="text" class="form-control" readonly="" id="bahan_nama_'+no+'" data-no="'+no+'" value="">' +
                                '<div class="input-group-append"><button data-no="'+no+'" class="btn btn-primary bahan-search" type="button" ><i class="flaticon-search"></i></button></div>' +
                            '</div>' +
                        "</td>"+
                        "<td>" +
                            "<select class='form-control unit' data-no='"+no+"' id='satuan_"+no+"' name='recipe[_"+no+"][satuan_id]' >" +
                            "</select>"+
                        "</td>"+
                        "<td>" +
                            "<input type='text' class='form-control input-numeral' id='jumlah_"+no+"' data-no='"+no+"' name='recipe[_"+no+"][takaran]' value='0'>" +
                            "<input type='hidden' id='konversi_takaran_id_"+no+"' data-no='"+no+"' name='recipe[_"+no+"][konversi_takaran]'>" +
                        "</td>"+
                        "<td>" +
                            "<input type='hidden' class='harga-pokok' id='harga_pokok_"+no+"' data-no='"+no+"' name='recipe[_"+no+"][harga_pokok]'>" +
                            "<input type='hidden' class='harga-pokok-statis' id='harga_statis_"+no+"' data-no='"+no+"' '>" +
                            "<span id='harga_pokok_label_"+no+"' data-no='"+no+"'></span>" +
                        "</td>"+
                        "<td>" +
                            "<textarea class='form-control' name='recipe[_"+no+"][keterangan]'></textarea>" +
                        "</td>"+
                        "<td>" +
                            "<button type='button' data-no='"+no+"' class='btn btn-danger center delete-btn'><i class='fa fa-trash'></i></button>" +
                        "</td>"+
                        "</tr>";
            $("#view_child_data").append(text);
            new Cleave($("#jumlah_"+no), {
                numeral: true,
                numeralDecimalScale: 4
            })
            noChose = no
            $("#kt_modal_search_bahan").modal("show")
            no++;
        })
        $("#view_child_data").on('click','.bahan-search',function () {
            var no = $(this).data('no')
            noChose = no
            $("#kt_modal_search_bahan").modal("show");
        })
        $("#bahan_data_ajax").on("click",".chose-bahan",function () {
            $("#kt_modal_search_bahan").modal("hide");

            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            console.log(object)
            var satuan_id = (""+object.unit_id).split("|")
            var satuan_nama = (""+object.unit_name).split("|")
            var satuan_konversi = (""+object.unit_konversi).split("|")
            $("#bahan_nama_"+noChose).val(object.bahan_nama)
            $("#bahan_id_"+noChose).val(object.bahan_id)
            $("#satuan_"+noChose).html('')
            $.each(satuan_id,function (i,val) {
                var text = "<option value='"+satuan_id[i]+"' data-konversi='"+satuan_konversi[i]+"'>"+satuan_nama[i]+"</option>"
                $("#satuan_"+noChose).append(text)
            })
            $("#harga_statis_"+noChose).val(object.bahan_harga);
            $("#konversi_takaran_id_"+noChose).val($("#satuan_"+noChose).find(':selected').data('konversi'))
            get_harga_pokok(noChose)
        })
        function get_harga_pokok(index){
            var jumlah = intVal($("#jumlah_"+index).val())
            var konversi =  parseFloat($("#satuan_"+index).find(':selected').data('konversi'))
            var pokok =  parseFloat( $("#harga_statis_"+index).val());
            pokok = pokok * konversi * jumlah
            $("#harga_pokok_"+index).val(pokok)
            $("#harga_pokok_label_"+index).html('Rp. '+KTUtil.numberString(pokok.toFixed(2)))
            get_total_pokok()
        }
        function get_total_pokok(){
            var total = 0;
            $.each($(".harga-pokok"),function (i,val) {
                total +=parseFloat($(val).val())
            })
            $("#input_total_pokok").val(total)
            $("#total_pokok").html("Rp. "+KTUtil.numberString(total.toFixed(2)))
        }
        $("#view_child_data").on("keyup",".input-numeral",function () {
            var value  = $(this).val()
            value = value == '' ? 0 : value
            $(this).val(value)
            var index = $(this).data("no")
            get_harga_pokok(index)
        })
        $("#view_child_data").on("change",".unit",function () {
            var index = $(this).data("no")
            $("#konversi_takaran_id_"+index).val($("#satuan_"+index).find(':selected').data('konversi'))
            get_harga_pokok(index)
        })
        $("#view_child_data").on('click','.delete-btn',function () {
            var index = $(this).data('no')
            $("#row-"+index).remove()
            get_total_pokok()
        })

    }
    var list_retur = function () {
        $("#child_data_ajax").on('click','.posting-btn',function(e){
            e.preventDefault()
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "get",
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    if (data.success){
                        window.location.reload();
                    }else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });
        })
        $("#child_data_ajax").on('click','.view-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.ajax({
                url: base_url+"retur-produk/detail/"+object.id,
                type: "get",
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    var data = JSON.parse(response)
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    $("#detail_tanggal").html(data.retur_produk.tanggal)
                    $("#detail_lokasi").html(data.retur_produk.lokasi_nama)
                    $("#detail_po").html(data.retur_produk.po_produk_no)
                    $("#detail_suplier").html(data.retur_produk.suplier_nama)
                    $("#detail_total").html("Rp. "+KTUtil.numberString(data.retur_produk.total_retur))
                    $("#view_child_data").html('')
                    $.each(data.retur_produk_detail,function (i,val) {
                        var text = '<tr>' +
                            '<td>'+val.produk_nama+'</td>'+
                            '<td style="text-align: right">Rp. '+KTUtil.numberString(val.harga)+'</td>'+
                            '<td style="text-align: right">'+KTUtil.numberString(val.item.jumlah)+'</td>'+
                            '<td style="text-align: right">Rp. '+KTUtil.numberString(val.item.sub_total_retur)+'</td>'+
                            '</tr>';
                        $("#view_child_data").append(text)
                    })


                    $("#kt_modal_detail_retur").modal('show')
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });

        })
    }

    var send = function () {
        $(".form-send").submit(function (e) {
            e.preventDefault();
            var self = $(this)
            var alert_show = self.data('alert-show');
            var alert_field_message = self.data('alert-field-message');
            if (!self.valid()) {
                return;
            }
            if ($("#produk_id").val()=='') {
                swal.fire({
                            title: 'Produk tidak boleh kosong',
                            icon: 'warning',
                            type: 'error',
                            timer:1000,
                            showConfirmButton:false
                        })
                return;
            }
            if($(".unit").length < 1){
                swal.fire({
                    title: 'Tidak ada bahan yang disimpan',
                    icon: 'warning',
                    type: 'error',
                    timer:1000,
                    showConfirmButton:false
                })
                return;
            }
            if(alert_show){
                swal.fire({
                    title: 'Apakah anda yakin?',
                    text: alert_field_message,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText:'Ya',
                    cancelButtonText:'Tidak'
                }).then((result) => {
                    if (result.value) {
                        form_send(self);
                    }
                })
            } else {
                form_send(self)
            }

        })
        function form_send(self) {
            var action = self.attr('action');
            var method = self.attr('method');
            var redirect = self.data('redirect');

            var data = self.serialize()
            $.ajax({
                url:action,
                type:method,
                data: data,
                success: function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        swal.fire({
                            text:data.message,
                            type:'success',
                        }).then((result) => {
                            if(result.value){
                                if(redirect!=undefined){
                                    window.location.href = redirect
                                }else{
                                    window.location.href = redirect
                                }
                            }
                        })
                    } else {
                        swal.fire({
                            title:'Error',
                            text:data.message,
                            type:'error',
                            showConfirmButton: false,
                            timer:1000
                        })
                    }
                },
                error:function (response) {

                }
            })
        }

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            if(type=='list'){
                list_retur()

            }else{
                recipe()
                send()
            }

        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTRecipe.init();
});