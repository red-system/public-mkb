"use strict";

// Class Definition
var KTPenerimaanBahan = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var general_url = $("#general_data_url").val();
        $("#add_btn").click(function(){
            get_general_data();
            
        })
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        
        $("#option_bahan_add").change(function(){
            var code= $(this).find(':selected').val();
            $.ajax({
                type: "POST",
                url: base_url+'po-bahan/utility/get-bahan-detail',
                data:{'po_bahan_detail_id':code},
                cache: false,
                success: function(response){
                    var data = jQuery.parseJSON(response);
                    var jumlah_order = parseInt(data.jumlah);
                    var terkirim = parseInt(data.jumlah) - parseInt(data.sisa);
                    var sisa = parseInt(data.sisa);
                    $('#kt_modal_add label[name="jumlah_order"]').html(jumlah_order);
                    $('#kt_modal_add label[name="terkirim"]').html(terkirim);
                    $('#kt_modal_add label[name="sisa"]').html(sisa);
                    $('#kt_modal_add #sisa').val(sisa);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        })

        $("#option_bahan_edit").change(function(){
            var code= $(this).find(':selected').val();
            $.ajax({
                type: "POST",
                url: base_url+'po-bahan/utility/get-bahan-detail',
                data:{'po_bahan_detail_id':code},
                cache: false,
                success: function(response){
                    var data = jQuery.parseJSON(response);
                    var jumlah_order = parseInt(data.jumlah);
                    var terkirim = parseInt(data.jumlah) - parseInt(data.sisa);
                    var sisa = parseInt(data.sisa);
                    $('#kt_modal_edit_hp label[name="jumlah"]').html(jumlah_order);
                    $('#kt_modal_edit_hp input[name="jumlah"]').val(jumlah_order);
                    $('#kt_modal_edit_hp label[name="terkirim"]').html(terkirim);
                    $('#kt_modal_edit_hp input[name="terkirim"]').val(terkirim);
                    $('#kt_modal_edit_hp label[name="sisa"]').html(sisa);
                    $('#kt_modal_edit_hp input[name="sisa"]').val(sisa);
                    $('#kt_modal_edit_hp #sisa').val(sisa);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        })

        function get_general_data(type = 0,sibling = null){
            $.ajax({
                async:false,
                type: "POST",
                url: general_url,
                cache: false,
                success: function(response){
                    if(type==0){
                        parse_add(response);
                    } else {
                        parse_edit(response,sibling)
                    }
                    
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });             
        } 
        function parse_add(response){
            var data = jQuery.parseJSON(response);
            var hutang = data.result;
            $.each(hutang,function(key,value){

                if($.isNumeric(value)){
                    value = KTUtil.numberString(parseInt(value).toFixed(0))
                }
                $('#kt_modal_add label[name="' + key + '"]').html(value);
                $('#kt_modal_add input[name="' + key + '"]').val(value);

            })
            $("#kt_modal_add").modal("show");
        }
        function parse_edit(response,sibling){
            var json = sibling.siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                var type = $('[name="' + key + '"]').attr('type');
                if (type == 'file') {
                    $('#kt_modal_edit_hp .img-preview').attr('src', base_url+value);
                } else if (type == 'radio') {
                    $('#kt_modal_edit_hp [name="' + key + '"][value=' + value + ']').attr('checked', 'checked');
                } else {
                    if(key !="password"){
                        $('#kt_modal_edit_hp label[name="' + key + '"]').html(value);
                        $('#kt_modal_edit_hp [name="' + key + '"]').val(value);
                        $('#kt_modal_edit_hp [name="' + key + '"]').trigger('change');
                        $(".input-numeral").trigger('change');
                    }
                    
                }
            }) 
            var data = jQuery.parseJSON(response);
            var hutang = data.result;
            $.each(hutang,function(key,value){

                if($.isNumeric(value)){
                    value = KTUtil.numberString(parseInt(value).toFixed(0))
                }
                $('#kt_modal_edit_hp label[name="' + key + '"]').html(value);
                $('#kt_modal_edit_hp input[name="' + key + '"]').val(value);

            })
            $("#kt_modal_edit_hp").modal("show");
        }        
        $("#child_data_ajax").on('click','.edit-btn',function(){
            get_general_data(1,$(this));

        })

        $('#child_data_ajax').on('click','.submit-btn',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda yakin customer sudah menerima produk?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87",
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {

                    $.ajax({
                        url: base_url+"po-bahan/accept-stock",
                        type: "post",
                        data:{"id":object.row_id},
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function (response) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                                // console.log(data);
                                window.location.reload()
                            }else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        });

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPenerimaanBahan.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});