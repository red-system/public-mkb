"use strict";

// Class Definition
var KTPoProduk = function() {
    console.log(134)
    var general = function(){
        var base_url = $("#base_url").val();
        var no = 0;
        if($("#index_no").length>0){
            no = $("#index_no").val();
        }
        var produk_no = 0;
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $("#suplier_id").change(function(){
            var code= $(this).find(':selected').data('suplier');
            $.ajax({
                type: "POST",
                url: base_url+'order-produsen/utility/get-po-no',
                data:{'suplier_kode':code},
                cache: false,
                success: function(response){
                    $("#display_po_no").html(response);
                    $("#input_po_no").val(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })
        $("#add_item").click(function(){
            var text = '<div class="row" id="produk_item_'+no+'">' +
                '<div class="col-md-3">' +
                '<div class="form-group row">' +
                '<label for="example-text-input" class="col-3 col-form-label">produk<b class="label--required">*</b></label>' +
                '<div class="col-9">' +
                '<div class="input-group col-12">'+
                '<input type="text" class="form-control readonly" name="item_produk_nama_'+no+'" id="item_produk_nama_'+no+'" required="" autocomplete="off">'+
                '<input type="hidden" class="form-control" id="item_produk_id_'+no+'" name="item_produk[produk_'+no+'][produk_id]"><div class="input-group-append">'+
                '<button class="btn btn-primary produk-search" type="button" data-toggle="modal" data-no="'+no+'"><i class="flaticon-search"></i></button></div></div>'+
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2">' +
                '<div class="form-group row">' +
                '<label class="col-6 col-form-label">HPP Terakhir</label>'+
                '<label class="col-6 col-form-label" id="last_hpp_'+no+'">Rp.0</label>'+
                '<input type="hidden" id="input_last_hpp_'+no+'" name="item_produk[produk_'+no+'][last_hpp]">'+
                '</div>'+
                '</div>' +
                '<div class="col-md-2">' +
                '<div class="form-group row"><label for="example-text-input" class="col-6 col-form-label">Harga<b class="label--required">*'+
                '</b></label>' +
                '<div class="col-6">' +
                '<input type="hidden" class="form-control harga-produk" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_harga_'+no+'" name="item_produk[produk_'+no+'][harga]">'+
                '<label class="form-control" autocomplete="off" data-no="'+no+'"  id="item_produk_harga_lbl_'+no+'">0</label>'+
                '</div></div></div>' +
                '<div class="col-md-2"><div class="form-group row"><label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b>'+
                '</label><div class="col-6"><input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_jumlah_'+no+'" name="item_produk[produk_'+no+'][jumlah]">'+
                '</div><label for="example-text-input" class="col-3 col-form-label" id="item_produk_satuan_'+no+'">'+
                '</label></div></div><div class="col-md-2"><div class="form-group row"><label for="example-text-input" class="col-6 col-form-label">Subtotal</label><div class="col-6">'+
                '<label class="col-form-label" id="item_produk_subtotal_display_'+no+'">0</label><input type="hidden" class="form-control input-numeral subtotal" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_subtotal_'+no+'" name="item_produk[produk_'+no+'][subtotal]">'+
                '</div></div></div><div class="col-md-1"><button type="button" class="btn btn-danger btn-sm delete-produk" data-no="'+no+'" data-item-produk="produk_item_'+no+'"><i class="flaticon2-trash"></i>'+
                '</button></div></div>';
            $("#item-container").append(text);
            new Cleave($("#item_produk_jumlah_"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            new Cleave($("#item_produk_harga_"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            no++;
        })
        $("#kt_add_submit").click(function(){
            var btn = $(this);
            var form = $("#kt_add")
            var is_zero = false;
            $("#item-container .input-numeral").each(function(){
                if($(this).val()==0){
                    is_zero = true;
                }
            })
            if(is_zero){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Tidak boleh ada jumlah maupun harga yang bernilai 0',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            if($("#item-container").children().length == 0 ){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Order harus memiliki setidaknya satu produk',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            form.validate({})
            if(!form.valid()){
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        window.location.href = base_url+"order-produsen";
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })
        $("#item-container").on('keydown','.readonly',function(e){
            e.preventDefault();
        })
        $('input[type=radio][name=jenis_pembayaran]').change(function() {
            if($(this).val() == "kas"){
                $("#kas_container").css('display','block');
                $(".tipe-kas").prop('dissabled',false);
                $("#kredit_container").css('display','none');
                $(".tipe-kredit").prop('dissabled',true);
            } else {
                $("#kas_container").css('display','none');
                $(".tipe-kas").prop('dissabled',true);
                $("#kredit_container").css('display','block');
                $(".tipe-kredit").prop('dissabled',false);
            }
        })
        $(".input-numeral").keyup(function(){
            if($(this).val()== ""){
                $(this).val(0)
            }
        })
        $("#item-container").on("click",".delete-produk",function(){
            var index = $(this).data("no");
            $("#produk_item_"+index).remove();
            get_subtotal();
        })
        function intVal(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        $("#item-container").on('keyup','.jumlah-produk',function(){
            var index = $(this).data('no');
            var subtotal = intVal($(this).val()) * intVal($("#item_produk_harga_"+index).val())
            $("#item_produk_subtotal_"+index).val(subtotal);
            $("#item_produk_subtotal_display_"+index).html(KTUtil.numberString(subtotal.toFixed(0)));
            get_subtotal();
        })
        $("#item-container").on('keyup','.harga-produk',function(){
            var index = $(this).data('no');
            var subtotal = intVal($(this).val()) * intVal($("#item_produk_jumlah_"+index).val())
            $("#item_produk_subtotal_"+index).val(subtotal);
            $("#item_produk_subtotal_display_"+index).html(KTUtil.numberString(subtotal.toFixed(0)));
            get_subtotal();
        })
        function get_subtotal(){
            var total = 0;
            $( "#item-container .subtotal" ).each(function() {
                total += intVal($(this).val());
            })
            $("#total_item").html(KTUtil.numberString(total.toFixed(0)));
            $("#input_total_item").val(total);
            total = total + intVal($("#tambahan").val());
            total = total - intVal($("#potongan").val());
            $("#grand_total").html(KTUtil.numberString(total.toFixed(0)));
            $("#input_grand_total").val(total);

        }
        $("#potongan").keyup(function(){
            get_subtotal();
        })
        $("#tambahan").keyup(function(){
            get_subtotal();
        })
        $('#item-container').on('click','.produk-search',function(){
            produk_no = $(this).data('no');
            $("#kt_modal_produk").modal("show");
        });
        $('#produk_child').on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#item_produk_nama_"+produk_no).val(object.produk_nama);
            $("#item_produk_id_"+produk_no).val(object.produk_id);
            $("#item_produk_satuan_"+produk_no).html(object.satuan_nama);
            $("#last_hpp_"+produk_no).html('Rp. '+KTUtil.numberString(object.hpp_kutus));
            $("#input_last_hpp_"+produk_no).val(object.hpp_kutus);
            $("#item_produk_harga_"+produk_no).val(object.hpp_kutus);
            $("#item_produk_harga_lbl_"+produk_no).html(KTUtil.numberString(object.hpp_kutus));
            $("#kt_modal_produk").modal("hide");
        });
        var url_produk = $("#list_produk").val();
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: url_produk,
            columns:[{data:'produk_kode'},{data:'produk_nama'},{data:'stock'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $('#child_data_ajax').on('click','.view-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.po_kutus_kutus_id;
            get_detail(id)
        });
        function get_detail(id,type = 0){
            $.ajax({
                type: "POST",
                url: base_url+'order-produsen/detail',
                data:{'po_kutus_kutus_id':id},
                cache: false,
                success: function(response){
                    if(type==0){
                        parseDetail(response);
                    } else {
                        // display_penerimaan(response)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        $('#child_data_ajax').on('click','.penerimaan-status-btn',function(){
            // var json = $(this).siblings('textarea').val();
            // var object = JSON.parse(json);
            // var id = object.po_kutus_kutus_id;

            // $("#po_kutus_kutus_id_penerimaan").val(id);
            // $("#kt_penerimaan").modal("show");

            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            var id = object.enc_row_id;
            var url = base_url + 'order-produsen/penerimaan/'+id;
            window.location.href = url;

        });
        function parseDetail(response){
            var object = JSON.parse(response);
            $.each(object,function(key,value){
                $('#kt_modal_detail_po_kutus_kutus label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_po_kutus_kutus [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_po_kutus_kutus .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_po_kutus_kutus .img-preview').attr("src",base_url+value);
                }
            })
            $("#view_child_data").html("");
            $.each(object.item,function(key,value){
                var text = '<tr>'+
                    '<td >'+value.produk_nama+'</td>'+
                    '<td >'+value.harga+'</td>'+
                    '<td >'+value.jumlah+'</td>'+
                    '<td >'+value.sub_total+'</td></tr>';
                $("#view_child_data").append(text);
            })
            $("#kt_modal_detail_po_kutus_kutus").modal("show");
        }
        $("#kt_produksi_penerimaan_submit").click(function(){
            var form = $("#kt_penerimaan_produksi") ;
            form.submit();
        })
        $('#kt_po_penerimaan_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    password:{
                        required: true,
                        minlength:5,
                    },
                    re_password:{
                        required: true,
                        minlength:5,
                        equalTo:'#password'
                    }
                }
            });
            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        window.location.reload();window.location.reload();
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };
        $.ajax({
            url: base_url+"suplier/option",
            type: "post",
            success: function (response) {
                states = jQuery.parseJSON(response);
                $('#kt_typeahead_1').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'states',
                    source: substringMatcher(states)
                });
                $('#kt_typeahead_2').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'states',
                    source: substringMatcher(states)
                });
            },
            error: function(request) {
            }
        });
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPoProduk.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});