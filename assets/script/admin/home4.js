"use strict";

// Class Definition
var KTHome = function() {

    $("#question11_5").on('click',function (){
        $("#ganti-leader").html("");
    })
    $("#question11_0").on('click',function (){
        $("#ganti-leader").html($("#content-ganti-leader").html());
    })
    var pernah_isi_survey = $("#pernah_isi_survey").length>0?$("#pernah_isi_survey").val():0;
    if(pernah_isi_survey==0){
        $("#surveyModal").modal("show");
    }
    var base_url = $("#base_url").val();
    var count = 0;
    var jenis_kelamin = "";
    var general = function(){
        if ($.fn.datepicker) {
            console.log(123);
            $('.tanggal').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                format: 'yyyy-mm-dd',
            });
        }
        if($("#piutang_akan").length>0){
            var url_piutang_akan = $("#piutang_akan").val();
            var piutang_akan_table = $("#piutang-akan-table").DataTable({
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                pageLength: 5,
                ajax: url_piutang_akan,
                columns:[{data:'no'},{data:'nama_pelanggan'},{data:'no_faktur'},{data:'sisa'},{data:'tenggat_pelunasan'}]
            });
        }
        if($("#piutang_jatuh").length>0){
            var url_piutang_jatuh = $("#piutang_jatuh").val();
            var piutang_jatuh_table = $("#piutang-jatuh-table").DataTable({
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                pageLength: 5,
                ajax: url_piutang_jatuh,
                columns:[{data:'no'},{data:'nama_pelanggan'},{data:'no_faktur'},{data:'sisa'},{data:'tenggat_pelunasan'}]
            });
        }

        if($("#hutang_akan").length>0){
            var url_hutang_akan = $("#hutang_akan").val();
            var hutang_akan_table = $("#hutang-akan-table").DataTable({
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                pageLength: 5,
                ajax: url_hutang_akan,
                columns:[{data:'no'},{data:'suplier_nama'},{data:'po_bahan_no'},{data:'sisa'},{data:'tenggat_pelunasan'}]
            });
        }

        if($("#hutang_jatuh").length>0){
            var url_hutang_jatuh = $("#hutang_jatuh").val();
            var hutang_jatuh_table = $("#hutang-jatuh-table").DataTable({
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                pageLength: 5,
                ajax: url_hutang_jatuh,
                columns:[{data:'no'},{data:'suplier_nama'},{data:'po_bahan_no'},{data:'sisa'},{data:'tenggat_pelunasan'}]
            });
        }

        if($("#country_url").length>0){
            var url_guest = $("#country_url").val();
            var guest_table = $("#guest-table").DataTable({
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                pageLength: 5,
                ajax: url_guest,
                columns:[{data:'kewarganegaraan'},{data:'qty'}]
            });
        }

        if($("#most_url").length>0){
            var url_most = $("#most_url").val();
            var most_table = $("#most-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: false,
                info: false,
                pageLength: 5,
                ajax: url_most,
                columns:[{data:'no'},{data:'nama'},{data:'type'},{data:'jumlah_deposit'},{data:'persentase'},{data:'jumlah_bonus'},{data:'status'}]
            });
        }

        if($("#bonus_url").length>0){
            var url_bonus = $("#bonus_url").val();
            var bonus_table = $("#bonus-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: false,
                info: false,
                pageLength: 5,
                ajax: url_bonus,
                columns:[{data:'no'},{data:'nama_penerima'},{data:'nama'},{data:'type'},{data:'jumlah_deposit'},{data:'persentase'},{data:'jumlah_bonus'},{data:'status'}]
            });
        }

        if($("#po_url").length>0){
            var url_po = $("#po_url").val();
            var po_table = $("#po-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: false,
                info: false,
                pageLength: 5,
                ajax: url_po,
                columns:[{data:'no'},{data:'lokasi_nama'},{data:'po_produk_no'},{data:'tanggal_pemesanan'},{data:'grand_total_lbl'},{data:'status_pembayaran'}]
            });
        }

        if($("#least_url").length>0){
            var url_least = $("#least_url").val();
            var least_table = $("#least-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 5,
                ajax: url_least,
                columns:[{data:'no'},{data:'nama'},{data:'type_group'},{data:'status'},{data:'subdistrict_name'}]
            });
        }
        if($("#agen_url").length>0){
            var agen_url = $("#agen_url").val();
            var agen_table = $("#agen-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: agen_url,
                columns:[{data:'no'},{data:'nama'},{data:'phone'},{data:'email'},{data:'no_ktp'},{data:'alamat'}]
            });
        }

        if($("#super_url").length>0){
            var super_url = $("#super_url").val();
            var super_table = $("#super-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: super_url,
                columns:[{data:'no'},{data:'nama'},{data:'phone'},{data:'email'},{data:'no_ktp'},{data:'alamat'}]
            });
        }
        if($("#rekap_url").length>0){
            var rekap_url = $("#rekap_url").val();
            var rekap_table = $("#rekap-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: rekap_url,
                columns:[{data:'no'},{data:'year'},{data:'monthLbl'},{data:'total_po'},{data:'total_omset_reseller'},{data:'claimed'}]
            });
        }
        if($("#reward_url").length>0){
            var reward_url = $("#reward_url").val();
            var reward_table = $("#reward-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: reward_url,
                columns:[{data:'no'},{data:'rank_id'},{data:'omset'},{data:'keterangan'}]
            });
        }
        if($("#rekap_omset_leader_url").length>0){
            var rekap_omset_leader_url = $("#rekap_omset_leader_url").val();
            var rekap_leader_table = $("#rekap-leader-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: false,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: rekap_omset_leader_url,
                columns:[{data:'no'},{data:'nama'},{data:'total'}]
            });
        }
        if($("#rekap_performa_group_url").length>0){
            var rekap_performa_group_url = $("#rekap_performa_group_url").val();
            var rekap_performa_group_table = $("#rekap-performa-group-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: rekap_performa_group_url,
                columns:[{data:'no'},{data:'nama'},{data:'total_downline'},{data:'jumlah_bonus'},{data:'total_po'}]
            });
        }
        if($("#rekap_performa_super_url").length>0){
            var rekap_performa_super_url = $("#rekap_performa_super_url").val();
            var rekap_performa_super_table = $("#rekap-performa-super-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: rekap_performa_super_url,
                columns:[{data:'no'},{data:'nama'},{data:'total_po'},{data:'jumlah_penukaran'},{data:'jumlah_withdraw'}]
            });
        }
        if($("#rekap_po_provinsi_url").length>0){
            var rekap_po_provinsi_url = $("#rekap_po_provinsi_url").val();
            var rekap_po_provinsi = $("#rekap_po_provinsi_table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 10,
                ajax: rekap_po_provinsi_url,
                columns:[{data:'no'},{data:'province'},{data:'total'}]
            });
        }
        if($("#leader_board_url").length>0){
            var leader_board_url = $("#leader_board_url").val();
            var leader_board = $("#leader_board_table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 20,
                ajax: leader_board_url,
                columns:[{data:'no'},{data:'nama'},{data:'omset'}]
            });
        }
        if($("#reseller_leader_board_url").length>0){
            var reseller_leader_board_url = $("#reseller_leader_board_url").val();
            var reseller_leader_board = $("#reseller_leader_board_table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 20,
                ajax: reseller_leader_board_url,
                columns:[{data:'no'},{data:'nama'},{data:'omset'}]
            });
        }
        if($("#reward_thailand_url").length>0){
            var reward_thailand_url = $("#reward_thailand_url").val();
            var reward_thailand = $("#reward-thailand-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 200,
                ajax: reward_thailand_url,
                columns:[{data:'no'},{data:'nama'},{data:'reseller_poin'},{data:'super_poin'},{data:'reseller_bergabung'},{data:'super_bergabung'},{data:'tambahan_poin_ls'},{data:'point_redeposit'},{data:'total_poin'},{data:'total_join'}]
            });
        }
        if($("#spesial_thailand_url").length>0){
            var spesial_thailand_url = $("#spesial_thailand_url").val();
            var spesial_thailand = $("#spesial-thailand-table").DataTable({
                responsive: true,
                searching: false,
                processing: true,
                serverSide: true,
                ordering: true,
                lengthChange: false,
                bPaginate: true,
                info: false,
                pageLength: 200,
                ajax: spesial_thailand_url,
                columns:[{data:'no'},{data:'nama'},{data:'jumlah'}]
            });
        }
        if($("#kt_npwp").length>0){
            $("#kt_npwp").modal("show");
        }
        if($("#jenis_kelamin").length>0){
            jenis_kelamin = $("#jenis_kelamin").val();
        }
        $("#rb-punya").on('click',function () {
            var text = '<div class="form-group">' +
                '                                    <label class="form-control-label ">NPWP Atasnama sendiri?</label>' +
                '                                    <div class="kt-radio-inline">' +
                '                                        <label class="kt-radio">' +
                '                                            <input type="radio" name="atasnama_sendiri" value="1" id="rb-sendiri" checked> Ya' +
                '                                            <span></span>' +
                '                                        </label>' +
                '                                        <label class="kt-radio">' +
                '                                            <input type="radio" name="atasnama_sendiri" value="0"  id="rb-tidak-sendiri"> Tidak' +
                '                                            <span></span>' +
                '                                        </label>' +
                '                                    </div>' +
                '                                </div>';
             text = text +'<div class="form-group">' +
                '                                    <label class="control-label">No NPWP <b class="label--required">*</b></label>' +
                '                                    <input type="text" name="no_npwp" id="no_npwp" class="form-control" required=""> </div>' +
                '                                <div class="form-group">' +
                '                                    <div class="form-group">' +
                '                                        <label>Foto NPWP <b class="label--required">*</b></label>' +
                '                                        <div></div>' +
                '                                        <div class="custom-file">' +
                '                                            <input type="file" class="custom-file-input img-input"' +
                '                                                   data-display="displayImg2" name="foto_npwp"' +
                '                                                   accept="image/x-png,image/gif,image/jpeg"' +
                '                                                   id="customFile2" required="">' +
                '                                            <label class="custom-file-label selected" for="customFile"' +
                '                                                   id="labelCustomFile"></label>' +
                '                                            <input type="hidden" name="input_foto_npwp">' +
                '                                        </div>' +
                '                                    </div>' +
                '                                    <div class="fileinput-new thumbnail"' +
                '                                         style="width: 200px; height: 150px;">' +
                '                                        <img src="'+base_url+'assets/media/no_image.png" alt=""' +
                '                                             id="displayImg2" width="200" height="150">' +
                '                                    </div>' +
                '                                </div>' +
                '                                <div class="form-group">' +
                '                                    <div class="form-group">' +
                '                                        <label>Swa-foto dengan NPWP <b class="label--required">*</b></label>' +
                '                                        <div></div>' +
                '                                        <div class="custom-file">' +
                '                                            <input type="file" class="custom-file-input img-input"' +
                '                                                   data-display="displayImg1" name="foto_selfi"' +
                '                                                   accept="image/x-png,image/gif,image/jpeg"' +
                '                                                   id="customFile1" required="">' +
                '                                            <label class="custom-file-label selected" for="customFile"' +
                '                                                   id="labelCustomFile"></label>' +
                '                                            <input type="hidden" name="input_foto_selfi">' +
                '                                        </div>' +
                '                                    </div>' +
                '                                    <div class="fileinput-new thumbnail"' +
                '                                         style="width: 200px; height: 150px;">' +
                '                                        <img src="'+base_url+'assets/media/no_image.png" alt=""' +
                '                                             id="displayImg1" width="200" height="150">' +
                '                                    </div>' +
                '                                </div>'
             ;
                var text_pekerjaan = '<div class="form-group">' +
                    '                                    <label class="form-control-label ">Memiliki Pekerjaan Tetap Lain Selain MKB?</label>' +
                    '                                    <div class="kt-radio-inline">' +
                    '                                        <label class="kt-radio">' +
                    '                                            <input type="radio" name="penghasilan_lain" value="1" id="rb-tetap-ya" checked> Ya' +
                    '                                            <span></span>' +
                    '                                        </label>' +
                    '                                        <label class="kt-radio">' +
                    '                                            <input type="radio" name="penghasilan_lain" value="0"  id="rb-laki-laki"> Tidak' +
                    '                                            <span></span>' +
                    '                                        </label>' +
                    '                                    </div>' +
                    '                                </div>';
                text = text + text_pekerjaan;
                var text_status_pernikahan = '<div class="form-group">' +
                    '                                    <label class="form-control-label ">Status Pernikahan saat ini?</label>' +
                    '                                    <div class="kt-radio-inline">' +
                    '                                        <label class="kt-radio">' +
                    '                                            <input type="radio" name="status_pernikahan" value="lajang" id="rb-lajang" checked> Lajang' +
                    '                                            <span></span>' +
                    '                                        </label>' +
                    '                                        <label class="kt-radio">' +
                    '                                            <input type="radio" name="status_pernikahan" value="menikah"  id="rb-menikah"> Menikah' +
                    '                                            <span></span>' +
                    '                                        </label>' +
                    '                                        <label class="kt-radio">' +
                    '                                            <input type="radio" name="status_pernikahan" value="janda/duda"  id="rb-cerai"> Janda/Duda' +
                    '                                            <span></span>' +
                    '                                        </label>' +
                    '                                    </div>' +
                    '                                </div>';
                    text = text + text_status_pernikahan;
                    var text_tanggungan = '<div class="form-group">' +
                        '                                    <label class="form-control-label ">Apakah Kakak punya tanggungan?</label>' +
                        '                                    <div class="kt-radio-inline">' +
                        '                                        <label class="kt-radio">' +
                        '                                            <input type="radio" name="punya_tanggungan" value="1" id="rb-tanggungan-ya" > Ya' +
                        '                                            <span></span>' +
                        '                                        </label>' +
                        '                                        <label class="kt-radio">' +
                        '                                            <input type="radio" name="punya_tanggungan" value="0"  id="rb-tanggungan-tidak" checked> Tidak' +
                        '                                            <span></span>' +
                        '                                        </label>' +
                        '                                    </div>' +
                        '                                   <span class="form-text text-muted">Tanggungan (anak kandung/ angkat (sah), orang tua/ mertua)</span>'+
                        '                                </div>';
                    text = text + text_tanggungan;
                    var text_jumlah =   '  <div id="jumlah_tanggungan" style="display: none">'+
                        '                   </div>';
                    text = text + text_jumlah;

                $("#ada_npwp").html(text)
                $("#ada_npwp").slideDown()
        })
        $("#rb-tidak-punya").on('click',function () {
            $("#ada_npwp").slideUp()
            setTimeout(function() {
                $("#ada_npwp").html('')
            }, 1000);

        })
        $("#ada_npwp").on('click','#rb-tanggungan-ya',function () {
            count = count + 1;
            var text = '<div class="form-group"><label class="form-control-label ">Jumlah Tanggungan </label>' +
                '                                                                <input type="text" placeholder="" id="input-numeral-'+count+'" autocomplete="off" name="jumlah_tanggungan" class="form-control input-numeral" value="" required="">'+
                '</div>';
            $("#jumlah_tanggungan").html(text)
            new Cleave($("#input-numeral-"+count), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalScale: 4
            })
            $("#jumlah_tanggungan").slideDown();
        });
        $("#ada_npwp").on('click','#rb-tanggungan-tidak',function () {
            $("#jumlah_tanggungan").slideUp()
            setTimeout(function() {
                $("#jumlah_tanggungan").html('')
            }, 1000);
        });

        $('#kt_npwp_submit').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    user_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        Swal.fire({
                            type: 'success',
                            title: data.message,
                        }).then(function() {
                            window.location.reload()
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        });
        $(".akses-filter_data").click(function () {
            window.location.href = $("#base_url").val()+"?start_date="+$("#start_date").val()+"&end_date="+$("#end_date").val()+"&lokasi_id="+$("#lokasi_id").val();
        })
        if($("#chart-data").length>0){
            var chart = function() {
                // LINE CHART
                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'kt_morris_1',
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: jQuery.parseJSON($("#chart-data").val()),
                    // The name of the data record attribute that contains x-values.
                    xkey: 'y',
                    parseTime: false,
                    ykeys: ['a'],
                    labels: ['Sales'],
                    lineColors: ['#a0d0e0'],
                    hideHover: 'auto'
                });

            }
            chart();
        }
        $('#form-survey').on('submit',function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    password:{
                        required: true,
                        minlength:5,
                    },
                    re_password:{
                        required: true,
                        minlength:5,
                        equalTo:'#password'
                    }
                }
            });
            if (!form.valid()) {
                swal.fire({
                    type: 'error',
                    text:"Masukan email anda",
                    showConfirmButton: false,
                    timer: 1500
                });
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                       window.location.reload()
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });

    }
    var handleAvatarChange = function(){
        console.log(123)
        $("#customFile").change(function(){
            readURL(this);
            console.log('custome file');
        })
        $("#ada_npwp").on('change','.img-input',function(){
            var display = $(this).data('display');
            var name = $(this).attr("name");
            console.log('input change');
            upload_image(this,name)

            readURL(this,display);
        })
        function readURL(input,display) {
            console.log('read url');
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+display).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function upload_image(input,name) {
            console.log('upload image');
            var base_url = $("#base_url").val();
            var data = new FormData();
            jQuery.each(jQuery('[name='+name+']')[0].files, function(i, file) {
                data.append(name, file);
            });
            $.ajax({
                url: base_url+"profile/upload-npwp",
                type: "post",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                    $('.error-message').remove();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    console.log(data.url)
                    $('[name=input_'+name+']').val(data.url)
                    $('[name='+name+']').siblings('label').html(data.url);

                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
            handleAvatarChange();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTHome.init();
});