"use strict";

// Class Definition
var KTProfileGeneral = function() {

    var login = $('#kt_profile');
    var base_url = $("#base_url").val();
    var email = $("#personal_email").val();
    var user_name = $("#personal_name").val();
    var phone = $("#personal_phone").val();
    var tempat_tanggal_lahir = $("#tempat_tanggal_lahir").val();
    var alamat = $("#staff_alamat").val();
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var noImage = baseUrl+"/assets/media/no_image.png";
    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }
    var generalForm = function(){
        if ($.fn.datepicker) {
            $('.tanggal').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                format: 'yyyy-mm-dd',
            });
        }
        if ($.fn.select2) {
            $('#kt_select2_1, #kt_select2_2, .kt_select_2').select2({
                width:'100%'
            });
        }
        var clipboard = new Clipboard('#copy_link');

        clipboard.on('success', function(e) {
            swal.fire({
                type: 'success',
                text:"link copied to clipboard",
                showConfirmButton: false,
                timer: 1500
            });
        });

        var clipboard_upgrade = new Clipboard('#copy_link_upgrade');

        clipboard_upgrade.on('success', function(e) {
            swal.fire({
                type: 'success',
                text:"link copied to clipboard",
                showConfirmButton: false,
                timer: 1500
            });
        });

    }
    var editPersonal = function(){
        $("#kt_tabs_1_1 > input").prop("readonly",false);
        console.log(123);
    }
    var handleSwitchPersonalInfo = function(index = 0) {
        if (index == 0) {
            $("form#edit_personal_form input").prop("readonly",false);
            $("form#edit_personal_form textarea").prop("readonly",false);
            $("#personal_cancel_group").slideUp();
            $("#personal_edit_group").slideDown();
        } else {
            $("form#edit_personal_form input").prop("readonly",true);
            $("form#edit_personal_form textarea").prop("readonly",true);
            $("#personal_edit_group").slideUp();
            $("#personal_cancel_group").slideDown();
            $("#personal_email").val(email);
            $("#personal_name").val(user_name);
            $("#personal_phone").val(phone);
            $("#tempat_tanggal_lahir").val(tempat_tanggal_lahir);
            $("#staff_alamat").val(alamat);
        }
    }
    var handleFormSwitch = function() {
        $("#edit_personal_btn").click(function(){
            handleSwitchPersonalInfo(0);
        });
        $("#cancel_personal_btn").click(function(){
            handleSwitchPersonalInfo(1);
        });
    }
    var handlePersonalInfoSubmit = function() {
        var base_url = $("#base_url").val();
        var city_url = base_url+'register-city';
        var subdistrict_url = base_url+'register-subdistrict';
        $('.province').change(function () {
            var province_ = $(this).val();
            var prefix = $(this).attr('prefix');
            var city_ = prefix+'city_id';
            var subdistrict_ = prefix+'subdistrict_id';
            if(province_!==''){
                getCity(province_,city_,subdistrict_)
            }

        })
        $('.city').change(function () {
            var city_ = $(this).val();
            var prefix = $(this).attr('prefix');
            var subdistrict_ = prefix+'subdistrict_id';
            if(city_!==''){
                getSubdistrict(city_,subdistrict_)
            }
        })
        function getCity(province_,city_,subdistrict_)
        {

            $.ajax({
                url:city_url,
                type:'post',
                data:{'province_id':province_},
                success:function (response) {
                    var result = JSON.parse(response);

                    $("#"+city_).html('<option value="">Pilih Kabupaten/Kota</option>');
                    $.each(result.data,function (i,val) {
                        $("#"+city_).append('<option value="'+val.city_id+'">'+val.city_name+'</option>')
                    })
                    $("#"+subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                }
            })
        }
        function getSubdistrict(city_,subdistrict_) {
            $.ajax({
                url:subdistrict_url,
                type:'post',
                data:{'city_id':city_},
                success:function (response) {
                    var result = JSON.parse(response);
                    $("#"+subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                    $.each(result.data,function (i,val) {
                        $("#"+subdistrict_).append('<option value="'+val.subdistrict_id+'">'+val.subdistrict_name+'</option>')
                    })

                }
            })
        }
        $('#kt_personal_info_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    user_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        $(".account_name").html($("#personal_name").val());
                        user_name = $("#personal_name").val()
                        email = $("#personal_email").val()
                        phone = $("#personal_phone").val()
                        handleSwitchPersonalInfo(1);
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        });
    }
    var handleChangePasswordSubmit = function() {
        $('#kt_password_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    old_pass: {
                        required: true,
                        minlength:5
                    },
                    new_pass: {
                        required: true,
                        minlength: 5
                    },
                    re_pass: {
                        required: true,
                        minlength : 5,
                        equalTo : "#new_pass"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        form.clearForm();
                        form.validate().resetForm();
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        });
    }
    var handleAvatarSubmit = function() {
        $('#kt_avatar_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    avatar: {
                        required: true,
                        minlength:5
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        $("#customFile").val("");
                        $("#labelCustomFile").html("");
                        $('#displayImg').attr('src', noImage);
                        $(".account_avatar").attr('src', data.url);
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        window.location.reload()
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        });
    }
    var handleAvatarChange = function(){
        $("#customFile").change(function(){
            readURL(this);
        })
        $(".img-input").change(function(){
            var display = $(this).data('display');
            var name = $(this).attr("name");
            upload_image(this,name)
            readURL(this,display);
        })
        function readURL(input,display) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+display).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function upload_image(input,name) {

            var base_url = $("#base_url").val();
            var data = new FormData();
            jQuery.each(jQuery('[name='+name+']')[0].files, function(i, file) {
                data.append(name, file);
            });
            $.ajax({
                url: base_url+"profile/upload-npwp",
                type: "post",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                    $('.error-message').remove();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    console.log(data.url)
                    $('[name=input_'+name+']').val(data.url)
                    $('[name='+name+']').siblings('label').html(data.url);

                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    }

    var handleNpwpSubmit = function() {
        $('#kt_npwp_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    avatar: {
                        required: true,
                        minlength:5
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        $("#customFile").val("");
                        $("#labelCustomFile").html("");
                        $('#displayImg').attr('src', noImage);
                        $(".account_avatar").attr('src', data.url);
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        window.location.reload()
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        });
    }
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#displayImg').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#upgrade_super_reseller').on('click',function(){
        // var json = $(this).siblings('textarea').val()
        // var object = JSON.parse(json)
        swal.fire({
            title: "Perhatian ...",
            text: "Jika anda ingin meng-upgrade akun anda ke Super Reseller, maka akun ini akan dinonaktifkan sementara. Lanjutkan?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#0abb87",
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {

                $.ajax({
                    url: base_url+"profile/upgrade",
                    type: "post",
                    data:{},
                    beforeSend: function () {
                        $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                    },
                    success: function (response) {
                        $('.wrapper-loading').fadeOut().addClass('hidden');
                        var data = jQuery.parseJSON(response);
                        if (data.success){
                            swal.fire({
                                type: 'success',
                                text:data.message,
                                showConfirmButton: true,
                            }).then(function (ex) {
                                if (ex.value) {
                                    window.location.href = base_url;
                                }
                            })
                            
                        }else {
                            swal.fire({
                                type: 'error',
                                text:data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    },
                    error: function(request) {
                        $('.wrapper-loading').fadeOut().addClass('hidden');
                        swal.fire({
                            title: "Ada yang Salah",
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
    });

    // Public Functions
    return {
        // public functions
        init: function() {
            handleFormSwitch();
            generalForm();
            handlePersonalInfoSubmit();
            handleChangePasswordSubmit();
            handleAvatarChange();
            handleAvatarSubmit();
            handleNpwpSubmit();
        }
    };

}();


// Class Initialization
jQuery(document).ready(function() {
    KTProfileGeneral.init();
    penghasilan_lain();

});

function penghasilan_lain() {
    $('[name="penghasilan_lain"]').on('change', function () {

        if ($(this).val()==0){
            $('#penghasilan_text').show();
            $('[name="keterangan_lain"]').prop('required', true).val('');
            $('#penghasilan_val').val(1);
            $('#case3').hide()
        }else {
            $('#case3').show()
            $('#penghasilan_text').hide();
            $('[name="keterangan_lain"]').prop('required', false).val('');
            $('#penghasilan_val').val(0);
        }
    });
};