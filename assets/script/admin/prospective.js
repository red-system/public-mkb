$(document).ready(function () {
    var base_url = $("#base_url").val()
    $("#child_data_ajax").on('click','.accept-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);
        $.ajax({
            url:base_url+'prospective-reseller/accept',
            type:'post',
            data:{'reseller_id':data.reseller_id},
            beforeSend:function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success:function (response) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var result = JSON.parse(response)
                if(result.success){
                    swal.fire({
                        type: 'success',
                        text:data.message,
                    }).then(() => {
                        window.location.reload()
                    });
                }
            },

        })
    })
    $("#child_data_ajax").on('click','.complate-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);
        $("#maps_url").attr("href",data.maps)
        console.log(data);
        $("#maps_reseller_id").val(data.reseller_id)
        $("#longitude").val(data.longitude)
        $("#latitude").val(data.latitude)
        $("#kt_modal_complate").modal("show");

    })
    $("#submit_maps").click(function (e) {
        e.preventDefault();

        var btn = $(this);
        var form = $(this).closest('form');
        if (!form.valid()) {
            return;
        }
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload()
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    })
    $("#child_data_ajax").on('click','.refuse-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);

        $("#reseller_id_refuse").val(data.reseller_id)
        $("#kt_modal_refuse").modal("show");
    })
    $("#child_data_ajax").on('click','.activate-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);
        $.ajax({
            url:base_url+'prospective-reseller/aktif-save',
            type:'post',
            data:{'reseller_id':data.reseller_id},
            beforeSend:function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success:function (response) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var result = JSON.parse(response)
                if(result.success){
                    swal.fire({
                        type: 'success',
                        text:data.message,
                    }).then(() => {
                        window.location.reload()
                    });
                }
            },

        })
    })
    $("#child_data_ajax").on('click','.resend-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);
        $.ajax({
            url:base_url+'prospective-reseller/resend',
            type:'post',
            data:{'reseller_id':data.reseller_id},
            beforeSend:function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success:function (response) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var result = JSON.parse(response)
                if(result.success){
                    swal.fire({
                        type: 'success',
                        text:data.message,
                    }).then(() => {
                        window.location.reload()
                    });
                }
            },

        })
    })
    $("#child_data_ajax").on('click','.banned-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);
        $("#reseller_id_banned").val(data.reseller_id)
        $("#kt_modal_banned").modal("show");
    })
    
    $("#kt_banned_submit_btn").click(function (e) {
        e.preventDefault();

        var btn = $(this);
        var form = $("#kt_form_banned");

        form.validate({
            rules: {
                password:{
                    required: true,
                    minlength:5,
                },
                re_password:{
                    required: true,
                    minlength:5,
                    equalTo:'#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload();
                    $("input[type=number]").val(0);
                    if(btn.data('page')!= undefined && btn.data('page')=="transfer-stock"){
                        swal.fire({
                            type: 'success',
                            title: 'Dalam proses tranfer',
                            text: 'Menunggu konfirmasi penerimaan'
                        });
                    }
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    })
    $("#kt_refuse_submit_btn").click(function (e) {
        e.preventDefault();

        var btn = $(this);
        var form = $("#kt_form_refuse");

        form.validate({
            rules: {
                password:{
                    required: true,
                    minlength:5,
                },
                re_password:{
                    required: true,
                    minlength:5,
                    equalTo:'#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload();
                    $("input[type=number]").val(0);

                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });

    $("#child_data_ajax").on('click','.update-btn',function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);
        $("#reseller_id_update").val(data.reseller_id)
        $("#kt_modal_update").modal("show");
    })

    $("#kt_update_submit_btn").click(function (e) {
        e.preventDefault();

        var btn = $(this);
        var form = $("#kt_form_update");

        form.validate({
            rules: {
                password:{
                    required: true,
                    minlength:5,
                },
                re_password:{
                    required: true,
                    minlength:5,
                    equalTo:'#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    
                    swal.fire({
                        type: 'success',
                        title: 'Update email berhasil'
                    }).then(function (e) {
                        if (e.value) {
                            form.clearForm();
                            form.validate().resetForm();
                            window.location.reload();
                            $("input[type=number]").val(0);
                        }
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    })
    
})