"use strict";

// Class Definition
var KTDiscount = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var general_url = $("#general_data_url").val();
        var produk_add_url = $("#list_produk_add").val();
        var produk_edit_url = $("#list_produk_edit").val();
        var produk_detail_url = $("#list_produk_detail").val();
        var list_data = '';
        var set_id_val = 0;
        var produk_add_table = $("#produk-table-add").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_add_url,
            columns:[{data:"check"},
                {data:"produk_kode"},
                {data:'produk_nama'},
                {data:'jenis_produk_nama'},
                {data:'satuan_nama'},
                // {data:'stock_produk_qty'},
                {data:'aksi'}],
            createdRow:function( row, data, dataIndex){
                var key = list_data.indexOf("|"+data.produk_id+"|")
                if(key!=-1){
                    $('input[type="checkbox"]', row).prop('checked', true);
                }
                var temp = list_data.replace(/\|/g," ");
                var arrTemp = temp.split("  ")
                var count_checked = arrTemp.length
                var total_display = produk_add_table.page.info().recordsDisplay
                if(count_checked>=total_display && total_display!=0){
                    $("#all-produk").prop('checked',true);
                } else {
                    $("#all-produk").prop('checked',false);
                }
            },
            columnDefs:[{
                targets: 0,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta){

                    return '<input type="checkbox" id="produk_check_'+full.produk_id+'" class="child-check" name="produk_id[]" value="' +full.produk_id+ '">';
                }},{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {

                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }],

        });
        var produk_edit_table = $("#produk-table-edit").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,

            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_edit_url,
            columns:[{data:"check"},
                {data:"produk_kode"},
                {data:'produk_nama'},
                {data:'jenis_produk_nama'},
                {data:'satuan_nama'},
                // {data:'stock_produk_qty'},
                {data:'aksi'}],
            createdRow:function( row, data, dataIndex){
                var key = list_data.indexOf("|"+data.produk_id+"|")
                if(key!=-1){
                    $('input[type="checkbox"]', row).prop('checked', true);
                }
                var temp = list_data.replace(/\|/g," ");
                var arrTemp = temp.split("  ")
                var count_checked = arrTemp.length
                var total_display = produk_edit_table.page.info().recordsDisplay
                if(count_checked>=total_display && total_display!=0){
                    $("#all-produk-edit").prop('checked',true);

                } else {
                    $("#all-produk-edit").prop('checked',false);
                }
            },
            columnDefs:[{
                targets: 0,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta){

                    return '<input type="checkbox" id="produk_check_'+full.produk_id+'" class="child-check" name="produk_id[]" value="' +full.produk_id+ '">';
                }},{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {

                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }],

        });
        var produk_detail_table = $("#produk-table-detail").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,

            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_detail_url,
            columns:[
                {data:"produk_kode"},
                {data:'produk_nama'},
                {data:'jenis_produk_nama'},
                {data:'satuan_nama'}],

        });
        $("#generalSearchProduk").on('keyup', function(e){

            produk_add_table.search($("#generalSearchProduk").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_add_table.column(i).search(val ? val : '', false, false);
            });
            produk_add_table.table().draw();

        })
        $("#generalSearchProdukEdit").on('keyup', function(e){

            produk_edit_table.search($("#generalSearchProdukEdit").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_edit_table.column(i).search(val ? val : '', false, false);
            });
            produk_edit_table.table().draw();

        })
        $("#generalSearchProdukDetail").on('keyup', function(e){

            produk_detail_table.search($("#generalSearchProdukDetail").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_detail_table.column(i).search(val ? val : '', false, false);
            });
            produk_detail_table.table().draw();

        })
        $("#all-produk").click(function () {
            var value = $(this).prop('checked');
            if(value){
                getAllProduk()
            } else {
                $(".child-check").prop('checked',false);
                list_data = '';
                parse_list_data();
            }

        })
        $("#all-produk-edit").click(function () {
            var value = $(this).prop('checked');
            if(value){
                getAllProduk()
            } else {
                $(".child-check").prop('checked',false);
                list_data = '';
                parse_list_data();
            }

        })
        function getAllProduk() {
            $.ajax({
                url:base_url+'discount/all-produk',
                data:{query:$("#generalSearchProduk").val()},
                type:'post',
                success: function (response) {
                    var data = JSON.parse(response)
                    list_data = data.list_data != null ? data.list_data :'';
                    $(".child-check").prop('checked',true);
                    if(list_data==''){
                        $("#all-produk").prop('checked',false);
                    }
                    parse_list_data()
                },
                error:function () {

                }
            });
        }
        $("#produk_child").on('click','.child-check',function(){

            var value = $(this).prop('checked');
            var id = $(this).val()
            if(value){
                list_data +="|"+id+"|";
                var temp = list_data.replace(/\|/g," ");
                var arrTemp = temp.split("  ")
                var count_checked = arrTemp.length
                var total_display = produk_add_table.page.info().recordsDisplay
                if(count_checked==total_display){
                    $("#all-produk").prop('checked',true);
                }
            }else{
                $("#all-produk").prop('checked',false);
                list_data = list_data.replace("|"+id+"|","")
            }
            parse_list_data()

        })
        $("#produk_child_edit").on('click','.child-check',function(){

            var value = $(this).prop('checked');
            var id = $(this).val()
            if(value){
                list_data +="|"+id+"|";
                var temp = list_data.replace(/\|/g," ");
                var arrTemp = temp.split("  ")
                var count_checked = arrTemp.length
                var total_display = produk_edit_table.page.info().recordsDisplay
                if(count_checked==total_display){
                    $("#all-produk-edit").prop('checked',true);
                }
            }else{

                $("#all-produk-edit").prop('checked',false);
                list_data = list_data.replace("|"+id+"|","")
            }
            parse_list_data()

        })
        function parse_list_data() {
            $("input[name=produk_id_list]").val(list_data)
        }
        function change_jangka_waktu(parent,status="Tidak") {
            if(status=="Ya"){
                var text = ' <div class="form-group">' +
                                '<label class="form-control-label ">Tanggal Mulai <b class="label--required">*</b></label>' +
                                    '<input type="text" class="form-control tanggal" autocomplete="off" name="mulai_tanggal">' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="form-control-label ">Tanggal Selesai <b class="label--required">*</b></label>' +
                                '<input type="text" class="form-control tanggal" autocomplete="off" name="selesai_tanggal">' +
                            '</div>';
                $("#"+parent+"_jangka_waktu").html(text);
                $('.tanggal').datepicker({
                    rtl: KTUtil.isRTL(),
                    todayHighlight: true,
                    orientation: "bottom left",
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                });
            } else {
                $("#"+parent+"_jangka_waktu").html('');
            }
        }
        $("input:radio[name=jangka_waktu]").click(function () {
            var parent = $(this).data('parent');
            var value = $(this).val();
            change_jangka_waktu(parent,value)
        })
        function change_tipe_discount(parent,status="Nominal") {
            if(status=="Nominal"){
                var text = ' <div class="form-group">' +
                    '<label class="form-control-label ">Nilai Diskon<b class="label--required">*</b></label>' +
                    '<input type="text" class="form-control nilai" autocomplete="off" name="value_nominal">' +
                    '</div>'
                $("#"+parent+"_tipe_discount").html(text);
            } else {
                var text = ' <div class="form-group">' +
                    '<label class="form-control-label ">Nilai Diskon<b class="label--required">*</b></label>' +
                    '<input type="text" class="form-control nilai" autocomplete="off" name="value_percent">' +
                    '</div>'
                $("#"+parent+"_tipe_discount").html(text);
            }
            if ($("#"+parent+"_tipe_discount .nilai").length > 0 ){
                new Cleave($("#"+parent+"_tipe_discount .nilai"), {
                    numeral: true,
                    numeralDecimalScale: 4
                })
            }
        }
        $("input:radio[name=tipe_discount]").click(function () {
            var parent = $(this).data('parent');
            var value = $(this).val();
            change_tipe_discount(parent,value)
        })
        change_tipe_discount("add","Nominal");
        change_tipe_discount("edit","Nominal");
        change_jangka_waktu("add","Tidak");
        change_jangka_waktu("edit","Tidak");
        $(".discount-modal").on('hide.bs.modal',function () {
            change_tipe_discount("add","Nominal");
            change_tipe_discount("edit","Nominal");
            // change_jangka_waktu("add","Tidak");
            // change_jangka_waktu("edit","Tidak");
            list_data = '';
            parse_list_data()
            $("input:checkbox").prop('checked',false)
            $("#generalSearchProduk").val('')
            produk_add_table.search($("#generalSearchProduk").val());
            produk_add_table.table().draw();
            $("#generalSearchProdukEdit").val('')
            produk_add_table.search($("#generalSearchProdukEdit").val());
            produk_edit_table.table().draw();
            if(set_id_val>0){
                unset_id()
            }
        })
        $("#kt_modal_edit").on('show.bs.modal',function () {
            var object = $("#rangkuman").val();
            var data = JSON.parse(object);
            list_data = data.produk_id;
            produk_edit_table.table().draw();
            change_tipe_discount("edit",data.tipe_discount)
            change_jangka_waktu("edit",data.jangka_waktu)
            $("#kt_modal_edit [name=value_percent]").val(KTUtil.numberString(data.value_percent))
            $("#kt_modal_edit [name=value_nominal]").val(KTUtil.numberString(data.value_nominal))
            $("#kt_modal_edit [name=mulai_tanggal]").val((data.mulai_tanggal))
            $("#kt_modal_edit [name=selesai_tanggal]").val((data.akhir_tanggal))
            $("#kt_modal_edit [name=minimal_total_belanja]").val(KTUtil.numberString(data.minimal_total_belanja))
            $("#kt_modal_edit [name=minimal_qty_item]").val(KTUtil.numberString(data.minimal_qty_item))
            parse_list_data()
        })
        function set_id(produk_id) {
            set_id_val = 1;
            $.ajax({
                url:base_url+"discount/set-id",
                type:'post',
                data:{'produk_id':produk_id},
                success:function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        produk_edit_table.table().draw()
                        $("#kt_modal_edit").modal("show")
                    }

                },
                error:function () {

                }
            })
        }

        function detail_set_id(discount_id) {
            set_id_val = 1;
            $.ajax({
                url:base_url+"discount/produk/detail/set-id",
                type:'post',
                data:{'discount_id':discount_id},
                success:function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        produk_detail_table.table().draw()
                        $("#kt_modal_detail").modal("show")
                    }

                },
                error:function () {

                }
            })
        }
        function unset_id() {
            set_id_val = 0;
            $.ajax({
                url:base_url+"discount/set-id",
                type:'post',
                success:function (response) {

                    $("#generalSearchProduk").val('')
                    produk_add_table.search($("#generalSearchProduk").val());
                    produk_add_table.table().draw();
                    $("#generalSearchProdukEdit").val('')
                    produk_add_table.search($("#generalSearchProdukEdit").val());
                    produk_edit_table.table().draw();
                },
                error:function () {

                }
            })
        }
        $("#child_data_ajax").on('click','.edit-btn',function (e) {
            e.preventDefault()
            $("#kt_modal_edit").modal('hide');
            var object = $("#rangkuman").val();
            var data = JSON.parse(object);
            set_id(data.produk_id)
        })
        $("#child_data_ajax").on('click','.view-btn',function (e) {
            e.preventDefault()
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#kt_modal_detail").modal('hide');
            detail_set_id(object.discount_id)
        })

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTDiscount.init();
});
