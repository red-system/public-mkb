$(document).ready(function () {
    var base_url = $('#base_url').val();
    var penjualan = $("#total_penjualan").val();
    var reseller_id = $("#reseller_id").val();
    var google_form = $("#google-form").val();
    var tambahan_url = $("#tambahan_url").val();
    var tambahan = $("#tambahan-table").DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        ordering: false,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
        pageLength: 10,
        ajax: tambahan_url,
        columns:[{data:"nama_reward"},{data:'jumlah_bintang'},{data:'aksi'}],
        columnDefs:[{
            targets: -1,
            responsivePriority: 1,
            title: 'Actions',
            orderable: false,
            render: function(data, type, full, meta) {
                var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                temp += '</div>'
                return temp;
            }
        }]

    });


   $("#claimReward").on('click',function () {
        $.ajax({
            url:base_url+'reward-reseller/claim-data',
            method:'post',
            data:{'penjualan':penjualan},
            success:function (response) {
                var data = JSON.parse(response);
                $.each(data,function(key,value){

                    $('#kt_modal_claim label[name="' + key + '"]').html(new Intl.NumberFormat().format(value));

                });
                $("#kt_modal_claim").modal('show');
            }
        });
   }) ;
    $("#claim_btn").on('click',function () {
        $.ajax({
            url:base_url+'reward-reseller/claim',
            method:'post',
            data:{'penjualan':penjualan,'reseller_id':reseller_id},
            success:function (response) {
                var data = JSON.parse(response)
                if(data.success){
                    swal.fire({
                        type: 'success',
                        title: 'Claim sudah diproses silakan mengisi form reward untuk melengkapi claim',
                    }).then(function() {
                        window.open(google_form,'_blank');
                        window.location.reload();
                    });
                }else{
                    swal.fire({
                        type: 'error',
                        title: 'Claim Gagal',
                    }).then(function() {
                        window.open(google_form,'_blank');
                        window.location.reload();
                    });
                }
            }
        });
    });
    $("#claimTambahan").on('click',function () {
       $("#kt_modal_search_produk").modal("show");
    });
    $("#reward_tambahan_child").on("click",".chose-btn",function () {
       var json = $(this).siblings("textarea").val();
       var data = JSON.parse(json);
        claim(data.reseller_id,data.row_id);
    });
    function claim(reseller_id,row_id) {
        $.ajax({
           url:base_url+"reward-reseller/claim-tambahan",
           type:'post',
           data:{'reseller_id':reseller_id,'row_id':row_id},
           success:function (response) {
                var data = JSON.parse(response);
               if(data.success){
                   swal.fire({
                       type: 'success',
                       title: 'Claim sudah diproses',
                   }).then(function() {
                       window.location.reload();
                   });
               }else{
                    swal.fire({
                        type: 'error',
                        title: 'Claim Gagal',
                    }).then(function() {
                        window.open(google_form,'_blank');
                        window.location.reload();
                    });
                }
           }
        });
    }

});