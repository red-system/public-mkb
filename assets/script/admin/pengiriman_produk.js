"use strict";

// Class Definition
var KTPengirimanProduk = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var general_url = $("#general_data_url").val();
        var detail_data_url = $("#detail_data_url").val();
        $("#add_btn").click(function(){
            $("#kt_modal_add").modal("show");
        });
        $("#pilih-produk-add").change(function (){
            var value = $(this).val();
            if(value=="pilih"){
                $("#add-jumlah-order").html("-");
                $("#add-terkirim").html("-");
                $("#add-sisa").html("-");
                $("#add-input-sisa").val(0);
            }else{
                $("#add-jumlah-order").html($("#pilih-produk-add option:selected").data("jumlah"));
                $("#add-terkirim").html($("#pilih-produk-add option:selected").data("terkirim"));
                $("#add-sisa").html($("#pilih-produk-add option:selected").data("sisa"));
                $("#add-input-sisa").val($("#pilih-produk-add option:selected").data("sisa"));
            }
        })
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });        
        function get_general_data(type = 0,sibling = null){
            // var
            $.ajax({
                async:false,
                type: "POST",
                url: general_url,
                // data:{'pengiriman_produk_id'},
                cache: false,
                success: function(response){
                    parse_edit(response,sibling);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });             
        } 
        function parse_add(response){
            var data = jQuery.parseJSON(response);
            var hutang = data.result;
            // $.each(hutang,function(key,value){
            //
            //     if($.isNumeric(value)){
            //         value = KTUtil.numberString(parseInt(value).toFixed(0))
            //     }
            //     $('#kt_modal_add label[name="' + key + '"]').html(value);
            //     $('#kt_modal_add input[name="' + key + '"]').val(value);
            //
            // })
            $("#kt_modal_add").modal("show");
        }
        function parse_edit(response){
            // var json = sibling.siblings('textarea').val();
            var object = JSON.parse(response);
            $.each(object,function(key,value){
                var type = $('[name="' + key + '"]').attr('type');
                if (type == 'file') {
                    $('#kt_modal_edit_hp .img-preview').attr('src', base_url+value);
                } else if (type == 'radio') {
                    $('#kt_modal_edit_hp [name="' + key + '"][value=' + value + ']').attr('checked', 'checked');
                } else {
                    if(key !="password"){
                        $('#kt_modal_edit_hp [name="' + key + '"]').val(value);
                        $('#kt_modal_edit_hp [name="' + key + '"]').trigger('change');
                        $(".input-numeral").trigger('change');
                    }
                    
                }
            }) 
            // var data = jQuery.parseJSON(response);
            // var hutang = data.result;
            // $.each(hutang,function(key,value){
            //
            //     if($.isNumeric(value)){
            //         value = KTUtil.numberString(parseInt(value).toFixed(0))
            //     }
            //     $('#kt_modal_edit_hp label[name="' + key + '"]').html(value);
            //     $('#kt_modal_edit_hp input[name="' + key + '"]').val(value);
            //
            // })
            $("#kt_modal_edit_hp").modal("show");
        }        
        $("#child_data_ajax").on('click','.edit-btn',function(){
            // get_general_data(1,$(this));
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            $.ajax({
                async:false,
                type: "POST",
                url: detail_data_url+object.pengiriman_produk_id,
                // data:{'pengiriman_produk_id'},
                cache: false,
                success: function(response){
                    console.log(response)
                    parse_edit(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })

        $('#child_data_ajax').on('click','.submit-btn',function(){
            var json = $(this).siblings('textarea').val()
            var object = JSON.parse(json)
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda yakin customer sudah menerima produk?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87",
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {

                    $.ajax({
                        url: base_url+"order-super-agen/accept-stock",
                        type: "post",
                        data:{"id":object.row_id},
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function (response) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                                window.location.reload()
                            }else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        });
        $("#kt_add_submit_pengiriman").click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if( $("#pilih-produk-add").val()=="pilih"){
                return;
            }
            form.validate({
                rules: {
                    password:{
                        required: true,
                        minlength:5,
                    },
                    re_password:{
                        required: true,
                        minlength:5,
                        equalTo:'#password'
                    }
                }
            });
            if (!form.valid()) {
                swal.fire({
                    type: 'error',
                    text:"Masukan email anda",
                    showConfirmButton: false,
                    timer: 1500
                });
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        window.location.reload();
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPengirimanProduk.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});