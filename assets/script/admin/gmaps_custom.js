"use strict";

let map;
let marker;
let geocoder;
let old_lat;
let old_lon;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: {
            lat: -1.035721,
            lng: 118.436931
        },
        zoom: 5,
        mapTypeId: "roadmap",
    });
    geocoder = new google.maps.Geocoder();

    const inputText = document.getElementById("pac-input");

    const submitButton = document.getElementById("search");

    const clearButton = document.getElementById("clear");

    marker = new google.maps.Marker({
        map,
    });

    google.maps.event.addListener(map, 'click', function (event) {
        taruhMarker(this, event.latLng);
    });
    // map.addListener("click", (e) => {
    //   geocode({
    //     location: e.latLng
    //   });
    // });
    window.onload = function () {
        loading_start();
        geocode({
            address: inputText.value
        })
    }
    submitButton.addEventListener("click", () =>
        geocode({
            address: inputText.value
        })
    );
    clearButton.addEventListener("click", () => {
        clear();
    });
    clear();
}

function clear() {
    marker.setMap(null);
    document.getElementById('location').innerHTML = '-';
    document.getElementById('lat').innerHTML = '-';
    document.getElementById('lon').innerHTML = '-';
    document.getElementById('lat_outlet').value = '-';
    document.getElementById('lon_outlet').value = '-';
    // responseDiv.style.display = "none";
}

function geocode(request) {
    clear();
    // loading_start();
    geocoder
        .geocode(request)
        .then((result) => {
            const {
                results
            } = result;

            setTimeout(function() {
                loading_finish();
            }, 1000);

            old_lat = document.getElementById('old_lat').value;
            old_lon = document.getElementById('old_lon').value;

            if (old_lat === '-') {
                marker.setPosition(results[0].geometry.location);
                map.setCenter(results[0].geometry.location);
                document.getElementById('lat').innerHTML = results[0].geometry.location.lat();
                document.getElementById('lon').innerHTML = results[0].geometry.location.lng();
                document.getElementById('lat_outlet').value = results[0].geometry.location.lat();
                document.getElementById('lon_outlet').value = results[0].geometry.location.lng();
            }else{
                marker.setPosition(new google.maps.LatLng(old_lat,old_lon));
                map.setCenter(new google.maps.LatLng(old_lat,old_lon));
                document.getElementById('lat').innerHTML = old_lat;
                document.getElementById('lon').innerHTML = old_lon;
                document.getElementById('lat_outlet').value = old_lat;
                document.getElementById('lon_outlet').value = old_lon;
            }
            
            marker.setMap(map);
            map.setZoom(20);
            document.getElementById('location').innerHTML = results[0].formatted_address;
            
            return results;
        })
        .catch((e) => {
            alert("Geocode was not successful for the following reason: " + e);
        });
}

function taruhMarker(peta, posisiTitik) {

    if (marker) {
        // pindahkan marker
        marker.setPosition(posisiTitik);
    } else {
        // buat marker baru
        marker = new google.maps.Marker({
            position: posisiTitik,
            map: peta
        });
    }

    document.getElementById('lat').innerHTML = posisiTitik.lat();
    document.getElementById('lon').innerHTML = posisiTitik.lng();
    document.getElementById('lat_outlet').value = posisiTitik.lat();
    document.getElementById('lon_outlet').value = posisiTitik.lng();

}

function loading_start() {
    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
}

function loading_finish() {
    $('.wrapper-loading').fadeOut().addClass('hidden');
}