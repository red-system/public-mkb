"use strict";
// Class Definition
var KTPOS = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var noCount = 1;
        var noChose = 0;
        var totalItem = 0;
        var potongan = 0;
        var tambahan = 0;
        var grandTotal = 0;
        var transaksi = {};
        var textTrans = {};
        var terbayar = 0;
        var city_url = base_url+'registrasi-city';
        var subdistrict_url = base_url+'registrasi-subdistrict';
        var oldIndex = 0;
        var guest_url = $("#guest_url").val();
        var produk_url = $("#produk_url").val();
        var is_pos_agen = $("#is_pos_agen").length > 0 ? true : false
        $('.input-numeral').keyup(function()
        {

            var val = intVal($(this).val())
            if((val)<0){
                val = -1*val
            }
            $(this).val(KTUtil.numberString(val))
            if($(this).val() == ""){
                $(this).val("0");
            }
        });
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        }); 
        var guest_table = $("#guest-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: guest_url,
            columns:[{data:"no"},{data:'guest_nama'},{data:'guest_alamat'},{data:'guest_telepon'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
		$('.modal').on('hidden.bs.modal', function () {
			$("#process-button").css('display','block');
		})
		$('.modal').on('show.bs.modal', function () {
			$("#process-button").css('display','none');
		})
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_url,
            columns:[{data:"produk_kode"},{data:'produk_nama'},{data:'jenis_produk_nama'},{data:'stock_produk_seri'},{data:'stock_produk_qty'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
		$("#generalSearch").on('keyup', function(e){
			produk_table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function() {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				produk_table.column(i).search(val ? val : '', false, false);
			});
			produk_table.table().draw();
		})
		$(".textSearch").keyup(function(){
			$(this).trigger('change');
		})
		$(".searchInput").change(function(){
			produk_table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function() {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}

			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				produk_table.column(i).search(val ? val : '', false, false);
			});
			produk_table.table().draw();
		})
		$("#barcode").keypress(function(event){
		    var nilai_konversi = 1;
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == 13){
				var barcode = $(this).val();
				$.ajax({
					type: "POST",
					url: produk_url+"?stock_produk_seri="+barcode,
					cache: false,
					success: function(response){
						var result = jQuery.parseJSON(response);
						var object = result.aaData[0]

						$("#produk_nama_"+noChose).html(object.produk_nama);
                        transaksi["trans"+noChose].harga_eceran = is_pos_agen ? object.hpp : object.harga_eceran;
						if(transaksi["trans"+noChose].tipe_penjualan == "eceran"){
							$("#harga_"+noChose).html(object.harga_eceran);
                            $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_eceran)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
						} else{
							$("#harga_"+noChose).html(object.harga_grosir);
                            $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_eceran)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
						}

						$("#kode_"+noChose).val(object.produk_kode);
						get_satuan(object.produk_id,noChose,object.stock_produk_qty)
						transaksi["trans"+noChose].produk_nama = object.produk_nama;
						transaksi["trans"+noChose].produk_kode = object.produk_kode;
                        transaksi["trans"+noChose].satuan_id = object.satuan_id;
                        transaksi["trans"+noChose].satuan_nama = object.satuan_nama;
						transaksi["trans"+noChose].produk_id = object.produk_id;

						transaksi["trans"+noChose].harga_grosir = object.harga_grosir;
						transaksi["trans"+noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
						transaksi["trans"+noChose].ar_harga = object.ar_harga;
						transaksi["trans"+noChose].hpp = object.hpp;
						transaksi["trans"+noChose].stock_produk_id = object.stock_produk_id;
                        transaksi["trans"+noChose].stock_produk_qty = object.stock_produk_qty;
                        transaksi["trans"+noChose].stock_ready = object.stock_produk_qty;
						transaksi["trans"+noChose].lokasi_id = object.lokasi_id;
                        transaksi["trans"+noChose].nilai_konversi = nilai_konversi;
                        transaksi["trans"+noChose].jumlah = 1;
                        $("#jumlah_"+noChose).val(1)
						$("#kt_modal_search_produk").modal("hide");
						getSubtotal(noChose);
						$("#jumlah_"+noChose).focus();

					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}

		})
        $("#kt_modal_search_produk").on("hide.bs.modal",function () {
            if(transaksi["trans"+noChose].produk_id==null){
                $("#row_"+noChose).remove();
                delete transaksi["trans"+noChose];
            }
        })
        $("#guest_child").on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#guest-form [name="' + key + '"]').val(value);
            })
            $("#kt_modal_search_pelanggan").modal("hide");
        })
        $("#item_child").on('click','.item-search',function(){
            noChose = $(this).data('no');
            $("#kt_modal_search_produk").modal("show");
			$( "#barcode" ).focus();
        })
        function is_same_produk(produk_id){
		    var result = true;
		    $.each(transaksi,function (i,val) {
                if(produk_id==val.produk_id){
                    var nI = val.noChose
                    var jum = val.jumlah

                    jum = jum+1;
                    var nilai_maxs = val.stock_ready;
                    if (intVal(jum) > intVal(nilai_maxs)){
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:'Jumlah Stock Pada Seri Produk Ini: '+nilai_maxs,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $(this).val(nilai_maxs);
                    } else {
                        transaksi["trans"+nI].jumlah = jum
                        $("#jumlah_"+nI).val(KTUtil.numberString(jum))
                    }
                    result = false;
                    //here
                    return false;
                }
            })
            return result;
        }
        $("#produk_child").on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);

            var id = object.produk_id;
           if (is_same_produk(id)){
               var nilai_konversi = 1;
               var stock_ready = object.stock_produk_qty;
               $("#produk_nama_"+noChose).html(object.produk_nama);
               object.harga_eceran = is_pos_agen ? object.hpp : object.harga_eceran;
               transaksi["trans"+noChose].ar_harga = is_pos_agen ? object.hpp: object.ar_harga;
               if(transaksi["trans"+noChose].tipe_penjualan == "eceran"){
                   $("#harga_"+noChose).html(object.harga_eceran);
                   console.log( $("#harga_"+noChose))
                   $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_eceran)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
               } else{
                   $("#harga_"+noChose).html(object.harga_grosir);
                   $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_grosir)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
               }
               $("#nilai_konversi_"+noChose).html(nilai_konversi);
               $("#kode_"+noChose).val(object.produk_kode);

               get_satuan(id,noChose,stock_ready)

               transaksi["trans"+noChose].produk_nama = object.produk_nama;
               transaksi["trans"+noChose].produk_kode = object.produk_kode;
               transaksi["trans"+noChose].satuan_nama = object.satuan_nama;
               transaksi["trans"+noChose].satuan_id = object.satuan_id;
               transaksi["trans"+noChose].produk_id = object.produk_id;
               transaksi["trans"+noChose].harga_eceran = is_pos_agen ? object.hpp : object.harga_eceran;
               transaksi["trans"+noChose].harga_grosir = object.harga_grosir;
               transaksi["trans"+noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;

               transaksi["trans"+noChose].hpp = object.hpp;
               transaksi["trans"+noChose].stock_produk_id = object.stock_produk_id;
               transaksi["trans"+noChose].stock_produk_qty = object.stock_produk_qty;
               transaksi["trans"+noChose].lokasi_id = object.lokasi_id;
               transaksi["trans"+noChose].stock_ready = object.stock_produk_qty;
               transaksi["trans"+noChose].nilai_konversi = nilai_konversi;
               transaksi["trans"+noChose].jumlah = 1;
               transaksi["trans"+noChose].noChose = noChose;
               $("#jumlah_"+noChose).val(1)
               $("#kt_modal_search_produk").modal("hide");
               getSubtotal(noChose);
           } else {
               $("#kt_modal_search_produk").modal("hide");
           }


        })
        function get_promo() {

        }
        function get_satuan(id,index,stock_produk_qty){
            var nilai_konversi = 1;
            var stock_ready = stock_produk_qty;
            $.ajax({
                url : base_url+"pos/get-unit-satuan",
                method : "POST",
                data : {'id': id},
                async : true,
                dataType : 'json',
                success: function(data){

                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        var jumlah_satuan = data[i].jumlah_satuan_unit;
                        var stock_ready_konversi = (nilai_konversi/jumlah_satuan)*stock_ready;
                        html += '<option data-harga='+data[i].harga_jual+' data-satuan_nama='+data[i].satuan_nama+' data-konversi='+data[i].jumlah_satuan_unit+' data-stock_ready="'+stock_ready_konversi+'" data-satuan_id="'+data[i].satuan_id+'" value='+data[i].unit_id+' data-ar-min="'+data[i].ar_minimal_pembelian+'" data-ar-harga="'+data[i].ar_harga+'">'+data[i].satuan_nama+'</option>';
                        console.log(data)
                    }
                    $("#satuan_nama_"+index).append(html);
                }
            });
        }
        $("#button_add_trans").click(function(){
            var item = {produk_id:null,produk_nama:null,satuan_nama:null,produk_kode:null,tipe_penjualan:"eceran",jumlah:0,harga_eceran:0,harga_grosir:0,hpp:0,subtotal:0,noChose:null}
            transaksi["trans"+noCount] = item;
            var tr = itemRow();
            $("#item_child").append(tr);
            new Cleave($("#jumlah_"+noCount), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalScale: 4
            })
			noChose = noCount;
			$("#kt_modal_search_produk").modal("show");
			$( "#barcode" ).focus();
            noCount++;
        })
        var itemRow = function(){
            var text = '<div class="col-12 row" id="row_'+noCount+'">';
            text +=  '<div class="col-sm-12 col-md-12 col-xl-3 form-group row" style="margin-top: 10px;padding-left: 10px;"><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label">Kode Produk</label><div class="input-group col-xs-6 col-sm-6 col-md-6 col-xl-12 ">'+
            '<input type="text" class="form-control" id="kode_'+noCount+'" readonly="">'+
            '<div class="input-group-append" style="display:inline"><button class="btn btn-primary item-search" type="button" data-no="'+noCount+'"><i class="flaticon-search"></i></button></div>'+
            '</div></div>';
            text +=  '<div class="col-sm-12 col-md-12 col-xl-3 form-group row" style="margin-top: 10px;padding-left: 10px;"><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label">Produk</label><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label" id="produk_nama_'+noCount+'"></label></div>';
            // text +=  '<div class="col-sm-12 col-md-12 col-xl-2 form-group row" style="margin-top: 10px;padding-left: 10px;">'+
            //             '<span class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label">Satuan</span>'+
            //             '<select class="col-xs-6 col-sm-6 col-md-6 col-xl-12  m-select form-control sel-satuan" name="" data-no="'+noCount+'" id="satuan_nama_'+noCount+'">'+
            //
            //             '</select>'+
            //          '</div>';
            text +=  '<div class="col-sm-12 col-md-12 col-xl-2 form-group row" style="margin-top: 10px;padding-left: 10px;"><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label">Jumlah</label><input type="text" class="form-control input-numeral input-decimal col-xs-6 col-sm-6 col-md-6 col-xl-12 " data-no="'+noCount+'" value="0" id="jumlah_'+noCount+'"></div>';
            text +=  '<div class="col-sm-12 col-md-12 col-xl-2 form-group row" style="margin-top: 10px;padding-left: 10px;"><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label">Harga</label> <label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label" id="harga_'+noCount+'">0</label></div>';
            text +=  '<div class="col-sm-12 col-md-12 col-xl-2 form-group row" style="margin-top: 10px;padding-left: 10px;"><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label">Subtotal</label><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label" id="subtotal_'+noCount+'">0</label></div>';
            text +=  '<div class=""  style="display:none;margin-top: 10px"><span id="nilai_konversi_'+noCount+'"></span></div>';
            text +=  '<div class="col-sm-12 col-md-12 col-xl-1 form-group row" style="margin-top: 10px;padding-left: 10px;"><label class="col-xs-6 col-sm-6 col-md-6 col-xl-12  col-form-label"></label><div class="col-xs-6 col-sm-6 col-md-6 col-xl-12 "><button class="btn btn-danger delete-btn" data-no="'+noCount+'"><i class="flaticon2-trash"></i></button></div></div>';

            text += '</div>';
            return text;
        }
        $("#item_child").on('change','.sel-satuan',function(){
            var harga_satuan = ($(this).find(':selected').data('harga'));
            var stock_ready = ($(this).find(':selected').data('stock_ready'));
            var nilai_konversi = ($(this).find(':selected').data('konversi'));
            var satuan_nama = ($(this).find(':selected').data('satuan_nama'));
            var satuan_id = ($(this).find(':selected').data('satuan_id'));
            var ar_min = ($(this).find(':selected').data('ar-min'));
            var ar_harga = ($(this).find(':selected').data('ar-harga'));
            var no = $(this).data('no');
            var reset_value = 0;
            transaksi["trans"+no].tipe_penjualan = $(this).val();
            transaksi["trans"+no].harga_eceran = harga_satuan;
            transaksi["trans"+no].satuan_nama = satuan_nama;
            transaksi["trans"+no].stock_ready = stock_ready;
            transaksi["trans"+no].ar_minimal_pembelian = ar_min;
            transaksi["trans"+no].ar_harga = ar_harga;

            getSubtotal(no);
            $("#nilai_konversi_"+no).html(nilai_konversi);
            transaksi["trans"+no].nilai_konversi = nilai_konversi;
            transaksi["trans"+noChose].satuan_id = satuan_id;
            var harga_satuan_number_format = harga_satuan.toLocaleString(
                undefined, // leave undefined to use the browser's locale,
                            // or use a string like 'en-US' to override it.
            );
            if(transaksi["trans"+no].tipe_penjualan == "eceran"){
                $("#harga_"+no).html(harga_satuan_number_format );  
                $("#jumlah_"+no).val(reset_value);
                $("#subtotal_"+no).html(reset_value);
                transaksi["trans"+no].jumlah = reset_value;  
            } else{
                $("#harga_"+no).html(harga_satuan_number_format );
                $("#jumlah_"+no).val(reset_value);
                $("#subtotal_"+no).html(reset_value);  
                transaksi["trans"+no].jumlah = reset_value;  
            }
            getSubtotal(no);
            getTotal();
        })
        $('#item_child').on('click','.delete-btn',function(){
            var no = $(this).data("no");
            $("#row_"+no).remove();
            delete transaksi["trans"+no];
            getTotal()
        })
        $('#item_child').on('keyup','.input-numeral',function(){
            var val = intVal($(this).val())
            if((val)<0){
                val = -1*val
            }
            $(this).val(KTUtil.numberString(val))
            var no = $(this).data("no");
            var nilai_maxs =  transaksi["trans"+no].stock_ready;

            var nilai_input = $(this).val();
            if (intVal(nilai_input) > intVal(nilai_maxs)){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:'Jumlah Stock Pada Seri Produk Ini: '+nilai_maxs,
                    showConfirmButton: false,
                    timer: 1500
                });
                $(this).val(nilai_maxs);
            }
            var element = $(this);
            var id = $(this).attr('id');
            var no = id.replace("jumlah_","");
            
            if(transaksi["trans"+no].ar_minimal_pembelian != undefined){
                var ar_min = (transaksi["trans"+no].ar_minimal_pembelian+"").split("|");
                var ar_harga = (transaksi["trans"+no].ar_harga+"").split("|");
                console.log(ar_harga)
                $.each(ar_min,function(i,value){
                    if(element.val() >= value && value != 0){
                        transaksi["trans"+no].harga_eceran = ar_harga[i];
                        $("#harga_"+noChose).html( KTUtil.numberString(transaksi["trans"+no].harga_eceran));
                    }
                })
                transaksi["trans"+no].jumlah = intVal($(this).val());
                getSubtotal(no);
            } else {
                $(this).val("0");
            } 

        })
        $('#item_child').on('change','input[type=radio]',function(){
            var id = $(this).attr('name');
            var no = id.replace("radio","");
            transaksi["trans"+no].tipe_penjualan = $(this).val();

            if(transaksi["trans"+no].tipe_penjualan == "eceran"){
                $("#harga_"+no).html(transaksi["trans"+no].harga_eceran);    
            } else{
                $("#harga_"+no).html(transaksi["trans"+no].harga_grosir);
            }              
            getSubtotal(no);
        })
        var getSubtotal = function(index){
            var total = 0;
            total = intVal(transaksi["trans"+index].jumlah) * intVal(transaksi["trans"+index].harga_eceran);
            transaksi["trans"+index].subtotal =  KTUtil.numberString(total.toFixed(0))
            $("#subtotal_"+index).html(transaksi["trans"+index].subtotal);
            getTotal(index);
        }
        var intVal = function(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        var getTotal = function(index){
            totalItem = 0;         
            $.each(transaksi,function(i,data){
                totalItem += intVal(data.subtotal)
            })
            $("#total-item").html(KTUtil.numberString(totalItem.toFixed(0)));
            $("#total-bayar").html(KTUtil.numberString(totalItem.toFixed(0)));
            grandTotal = (totalItem + tambahan) - potongan
            $("#grand-total").html(KTUtil.numberString(grandTotal.toFixed(0)));
            $("#grand-bayar").html(KTUtil.numberString(grandTotal.toFixed(0)));
            textTrans = {potongan:potongan,tambahan:tambahan,total_item:totalItem,grand_total:grandTotal,terbayar:terbayar,item:transaksi}
            $("#textTransaksi").html(JSON.stringify(textTrans));
            
        }
        $("#potongan").keyup(function(){
            $("#potongan-bayar").html($(this).val());
            potongan = intVal($(this).val())
            getTotal()
        })
        $("#terbayar").keyup(function(){
            terbayar = intVal($(this).val())
            getTotal()
        })        
        $("#tambahan").keyup(function(){
            $("#tambahan-bayar").html($(this).val());
            tambahan = intVal($(this).val())
            getTotal()
        })
        function identity_fail(){
            var count = 0;
            $.each($(".identity"),function (i,val) {
                if($(val).val()==""){
                    count = count+1;
                }
            })
            if (count>0){
                return true;
            }else{
                return false;
            }

        }
        $("#process-button").click(function(){
            var identity = identity_fail();
            if(identity){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Identitas pelanggan harus lengkap",
                    showConfirmButton: false,
                    timer: 1500
                });
                return
            }
            if(grandTotal == 0 ){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Total transaksi 0",
                    showConfirmButton: false,
                    timer: 1500
                });               
                return
            }

            $("#terbayar").val(KTUtil.numberString(grandTotal.toFixed(0)));
            var form = $("#save-form");
            terbayar = grandTotal;
            if($("#tipe_pembayaran").find(':selected').data('kembalian') == 1){
                $("#terbayar").removeClass("readonly");
                $("#terbayar").val(0);
            }
            if($("#lokasi_id").val() != undefined && $("#lokasi_id").val() == "" ){
                form.validate().element("#lokasi_id");
                return
            }
            $("#kt_modal_pembayaran").modal("show")
        })
        $("#btn-save").click(function(){
            var btn = $(this);
            var form = $("#save-form");
            var enough = true;
            if($("#terbayar").prop("disabled") == false){
                if(intVal($("#terbayar").val()) < grandTotal){
                    enough = false;
                }
            }
            if(!enough && ($("#tipe_pembayaran").find(':selected').data('jenis') != "kredit")){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Uang terbayar kurang",
                    showConfirmButton: false,
                    timer: 1500
                });                 
                return false;
            }
            if($("#tipe_pembayaran").find(':selected').data('jenis') == "kredit" && $("#tenggat_pelunasan").val() == ""){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Tenggat pembayaran tidak boleh kosong",
                    showConfirmButton: false,
                    timer: 1500
                }); 
                return false;
            }
            form.validate({});
            if (!form.valid()) {
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
						console.log(data)
                        $("#kt_modal_pembayaran").modal("hide");
                        if($("#tipe_pembayaran").find(":selected").data("kembalian")==1){
                            // Swal.fire({
                            //     type: 'success',
                            //     title: 'Rp. '+data.kembalian,
                            //     text: 'sisa kembalian'
                            // }).then(function() {

                            //     form.clearForm();
                            //     form.validate().resetForm();
                            //     var url = data.url;
                            //     window.open(url,"_blank");
                            //     clearForm();

                            // });

                            Swal.fire({
                                type: 'success',
                                title: 'Rp. '+data.kembalian,
                                text: 'sisa kembalian',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: `Print Struk`,
                                cancelButtonText: `Download Struk`,
                              }).then((result) => {
                                  if (result.value == true) {
                                    form.clearForm();
                                    form.validate().resetForm();
                                    var url = data.url;
                                    window.open(url,"_blank");
                                    clearForm();
                                    // console.log('print')
                                  }else if(result.dismiss == 'cancel'){
                                    form.clearForm();
                                    form.validate().resetForm();
                                    var download = data.download;
                                    window.open(download,"_blank");
                                    clearForm();
                                    // console.log('download')
                                  }else{
                                    form.clearForm();
                                    form.validate().resetForm();
                                    var url = data.url;
                                    window.open(url,"_blank");
                                    clearForm();
                                    // console.log('cancel')
                                  }
                              })

                        } else {
                            if($("#tipe_pembayaran").val()==-13){
                                snap.pay(data.token,{
                                    onSuccess: function(result){
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Success',
                                            text: 'Proses Pembayaran Berhasil'
                                        }).then(function() {
                                            form.clearForm();
                                            form.validate().resetForm();
                                            var url = data.url;
                                            window.open(url,"_blank");
                                            clearForm();
                                        });
                                    },
                                    onPending: function(result){
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Pending',
                                            text: 'Menunggu Pembayaran'
                                        }).then(function() {
                                            form.clearForm();
                                            form.validate().resetForm();
                                            var url = data.url;
                                            window.open(url,"_blank");
                                            clearForm();
                                        });
                                    },
                                    onError: function(result){
                                        Swal.fire({
                                            type: 'error',
                                            title: 'Error',
                                            text: 'Proses Pembayaran Gagal'
                                        })
                                    },
                                    onClose:function () {
                                        $.ajax({
                                            url:base_url+"/pos/delete",
                                            data:{'id':data.id},
                                            type:'post',
                                            success:function (response) {

                                                console.log(response)
                                                console.log(data.id)
                                            }

                                        })
                                    }
                                })
                            }else{
                                Swal.fire({
                                    type: 'success',
                                    title: 'Transaksi Berhasil',
                                }).then(function() {
                                    form.clearForm();
                                    form.validate().resetForm();
                                    var url = data.url;
                                    window.open(url,"_blank");
                                    clearForm();
                                });
                            }


                        }
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        })
		$("#btn-save-kas").on('click', function () {
			var btn = $(this);
			var form = $("#save-form");
			form.validate({});
			if (!form.valid()) {
				return false;
			}
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.ajaxSubmit({
				url: form.attr("action"),
				beforeSend: function () {
					$('.wrapper-loading').hide().removeClass('hidden').fadeIn();
				},
				success: function(response, status, xhr, $form) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					var data = jQuery.parseJSON(response);
					btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
					if (data.success) {
					    console.log($("#penukaran").length>0 )
						swal.fire({
							type: 'success',
							title: $("#penukaran").length>0 ? 'Berhasil menukarkan voucher' : 'Berhasil melakukan opening POS',
						}).then(function() {
							window.location.reload()
						});
					} else {
						swal.fire({
							type: 'error',
							title: 'Peringatan',
							text:data.message,
							showConfirmButton: false,
							timer: 1500
						});
					}
				},error: function (xhr, ajaxOptions, thrownError) {
					$('.wrapper-loading').fadeOut().addClass('hidden');
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		});

        $(".form-save-kas").on('submit', function (e) {
            e.preventDefault();
            var btn = $(this);
            var method = btn.attr('method');
            var formData = btn.serialize();

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $.ajax({
                url: $(this).attr("action"),
                type: method,
                data: formData,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                   $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        swal.fire({
                            type: 'success',
                            title: 'Berhasil melakukan opening POS',
                        }).then(function() {
                            window.location.reload()
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                   $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
            return false;
        })


		$("#btn-closing").click(function(){

            $.ajax({
                type:'get',
                url:base_url+'pos/info-closing',
                cache:false,
                success: function (response) {
                    var data = JSON.parse(response)
                    $.each(data,function (i,val) {
                        $("#kt_modal_closing [name="+i+"]").html(': '+val)
                    })
                    $("#kt_modal_closing").modal('show');
                }
            })

		})
        $("#kt_closing_submit").click(function () {
            $.ajax({
            	type: "POST",
            	url: base_url+'pos/closing',
            	cache: false,
            	success: function(response){
            		var data = JSON.parse(response)
            		if(data.success){
            			window.open(base_url+'pos/print-closing/'+data.log_kasir_id,'__blank')
                        window.location.reload()
            		} else {
            			swal.fire({
            				type: 'error',
            				title: 'Peringatan',
            				text:data.message,
            				showConfirmButton: false,
            				timer: 1500
            			});
            		}
            	},
            	error: function (xhr, ajaxOptions, thrownError) {
            		console.log(xhr.status);
            		console.log(xhr.responseText);
            		console.log(thrownError);
            	}
            });
        })
        var clearForm = function(){
            $(".input-numeral").val(0);
            $("#total-item").html(0);
            $("#total-bayar").html(0);
            $("#grand-total").html(0);
            $("#grand-bayar").html(0);
            $("#tambahan").html(0);
            $("#tambahan-bayar").html(0);
            $("#potongan").html(0);
            $("#potongan-bayar").html(0);
            $("#item_child").html("");
            noCount = 1;
            noChose = 0;
            totalItem = 0;
            potongan = 0;
            tambahan = 0;
            grandTotal = 0;
            transaksi = {};
            textTrans = {};
            terbayar = 0; 
            window.location.reload();          
        }
        $("#tipe_pembayaran").change(function(){
            $("#input_tipe_pembayaran").val($(this).val());
            $("#input_jenis_pembayaran").val($(this).find(':selected').data('jenis'));
            if($(this).find(':selected').data('kembalian') == 1){
                $("#terbayar").removeClass("read");
                $("#terbayar").addClass("input-numeral");
                $("#terbayar").val(0);
            } else {
                $("#terbayar").val(KTUtil.numberString(grandTotal.toFixed(0)));
                terbayar = grandTotal;
                getTotal()
                $("#terbayar").addClass("read"); 
                $("#terbayar").removeClass("input-numeral"); 
            }
            if($(this).find(':selected').data('jenis') == "kredit"){
                $("#kredit_container").css('display','block');
                $("#terbayar_container").css('display','none');

            } else {
                $("#kredit_container").css('display','none');
                $("#terbayar_container").css('display','flex');
            }
			if($(this).find(':selected').data('additional') == 1){
				$(".additional").removeClass("read");
				$("#additional_container").css('display','block');
			} else {
				console.log(321)
				$(".additional").addClass("read");
				$(".additional").val("");
				$("#additional_container").css('display','none');
			}
        })
        $('#tenggat_pelunasan').change(function(){
            $('#input_tenggat_pelunasan').val($(this).val());
        })
        $('#item-container').on('keydown','.readonly',function(e){
            e.preventDefault();
        });
        $(".readonly").keydown(function(e){
            if($("#tipe_pembayaran").find(":selected").data("kembalian")==0){
                e.preventDefault()
            }
            
        })
		$('.additional').change(function(){
			var name = $(this).attr("name");
			var value = $(this).val();
			$("input[name="+name+"]").val(value)
		})
        $("#lokasi_id").change(function(){
            var id = $(this).val();
            sess_change_lokasi(id)
            $(".input-numeral").val(0);
            $("#total-item").html(0);
            $("#total-bayar").html(0);
            $("#grand-total").html(0);
            $("#grand-bayar").html(0);
            $("#tambahan").html(0);
            $("#tambahan-bayar").html(0);
            $("#potongan").html(0);
            $("#potongan-bayar").html(0);
            $("#item_child").html("");
            noCount = 1;
            noChose = 0;
            totalItem = 0;
            potongan = 0;
            tambahan = 0;
            grandTotal = 0;
            transaksi = {};
            textTrans = {};
            terbayar = 0;             
        })        
        function sess_change_lokasi(key){
            $.ajax({
                type: "POST",
                url: base_url+'pos/utility/change-location',
                data:{'lokasi_id':key},
                cache: false,
                success: function(response){
                    console.log(response)
                    produk_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        $('.province').change(function () {
            var province_ = $(this).val();
            var prefix = $(this).attr('prefix');
            var city_ = prefix+'city_id';
            var subdistrict_ = prefix+'subdistrict_id';
            if(province_!==''){

                getCity(province_,city_,subdistrict_)
            }

        })
        $('.city').change(function () {
            var city_ = $(this).val();
            var prefix = $(this).attr('prefix');
            var subdistrict_ = prefix+'subdistrict_id';
            if(city_!==''){
                getSubdistrict(city_,subdistrict_)
            }
        })
        function getCity(province_,city_,subdistrict_) {
            $.ajax({
                url:city_url,
                type:'post',
                data:{'province_id':province_},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success:function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var result = JSON.parse(response);
                    $("#"+city_).html('<option value="">Pilih Kabupaten/Kota</option>');
                    $.each(result.data,function (i,val) {
                        $("#"+city_).append('<option value="'+val.city_id+'">'+val.city_name+'</option>')
                    })

                    $("#"+subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                }
            })
        }
        function getSubdistrict(city_,subdistrict_) {
            $.ajax({
                url:subdistrict_url,
                type:'post',
                data:{'city_id':city_},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success:function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var result = JSON.parse(response);
                    $("#"+subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                    $.each(result.data,function (i,val) {
                        $("#"+subdistrict_).append('<option value="'+val.subdistrict_id+'">'+val.subdistrict_name+'</option>')
                    })

                }
            })
        }
    }
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPOS.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
        })
    });
    $('.input-decimal').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralDecimalScale: 4
        })

    });
});
