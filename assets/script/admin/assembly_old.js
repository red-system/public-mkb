var no_index = 0;
if($("#index_no").length>0){
  console.log(12389)
    no_index = $("#index_no").val();
}
$(".input-numeral").val(0);
$('.input-numeral').toArray().forEach(function(field){
      new Cleave(field, {
        numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  })

});
$('.edit-numeral').toArray().forEach(function(field){
      new Cleave(field, {
        numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  })

});
var no = $("#no_item").length>0 ? $("#no_item").val():0;
$("#add_item").click(function () {
  no ++;
    var temp ="";
    var produk = JSON.parse($("#get_produk").val());
    $.each (produk, function(index,val){
      temp += '<option value="'+val.produk_id+'" data-idproduk="'+val.produk_id+'" data-idsatuan="'+val.produk_satuan_id+'" data-nama="'+val.satuan_nama+'">'+val.produk_nama+'</option>'
    });
    var text =  '<div class="col-12 row" style="padding: 10px" id="container-'+no+'">' +
                  '<div class="col-4 form-group">' +
                    '<label class="form-control-label">Produk</label>'+
                    '<select class="col-12 kt_select_2 form-control sel-produk" name="item[item_'+no+'][produk_id]" data-no="'+no+'" id="select_'+no+'">' +
                      temp
                      +
                    '</select>'+
                  '</div>'+
                  '<div class="col-3">' +
                    '<label class="form-control-label">Jumlah</label>'+
                    '<input type="text" class="form-control input-numeral" value="0" id="jumlah_'+no+'" name="item[item_'+no+'][jumlah]">'+
                  '</div>'+
                  '<div class="col-3 form-group">' +
                    '<label class="form-control-label">unit</label>'+
                    '<select class="col-12 m-select form-control sel-satuan" data-no_unit="'+no+'" name="item[item_'+no+'][unit_id]" id="selectunit_'+no+'">' +
                    '</select>' +
                  '</div>'+
                  '<div class="col-2" style="display:none;" id="idsatuanunit_'+no+'">'+
                  '</div>'+
                  '<div class="col-2" style="padding-top: 25px">' +
                    '<button type="button" class="btn btn-danger pull-right item-delete" data-no="'+no+'"><i class="fa fa-trash"></i></button> '+
                  '</div>'+
                '</div>';
    $("#item-container").append(text);
    new Cleave($("#jumlah_"+no), {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    })
    
});
$("#item-container").on('change','.sel-produk',function(){
  var nm_satuan = ($(this).find(':selected').data('nama'));
  var id_satuan = ($(this).find(':selected').data('idsatuan'));
  var no_unit = $(this).data('no');
  var nilai_satuan_def = 0;
  var id = ($(this).find(':selected').data('idproduk'));
  var base_url = $('#base-value').data('base-url');
  console.log(nilai_satuan_def)

  $("#selectunit_"+no_unit).html('<option value="'+nilai_satuan_def+'" data-idsatuanunit="'+id_satuan+'">'+nm_satuan+'</option>')
  $("#idsatuanunit_"+no_unit).html('<input type="text" class="form-control" value="'+id_satuan+'" name="item[item_'+no+'][satuan_id_unit]">')
  $.ajax({
      url : base_url+"assembly/get-unit-satuan",
      method : "POST",
      data : {id: id},
      async : true,
      dataType : 'json',
      success: function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
              html += '<option value='+data[i].unit_id+' data-idsatuanunit="'+data[i].satuan_id+'">'+data[i].satuan_nama+'</option>';
          }
          $("#selectunit_"+no_unit).append(html);
      }
  });
});
$("#item-container").on('change','.sel-satuan',function(){
  var idsatuanunit = ($(this).find(':selected').data('idsatuanunit'));
  var no_unit = $(this).data('no_unit');
  $("#idsatuanunit_"+no_unit).html('<input type="text" class="form-control" value="'+idsatuanunit+'" name="item[item_'+no_unit+'][satuan_id_unit]">')
});
$("#item-container").on('click','.item-delete',function () {
    var index = $(this).data('no');
    $("#container-"+index).remove();
})
$("#kt_add_submit").click(function(){
    var btn = $(this);
    var form = $("#kt_add")
    var is_zero = false;
    var base_url = $("#base_url").val();
    $("#item-container .input-numeral").each(function(){
        if($(this).val()==0){
            is_zero = true;
        }
    })
    if(is_zero){
        swal.fire({
            type: 'error',
            title:'Peringatan',
            text:'Tidak boleh ada jumlah maupun harga yang bernilai 0',
            showConfirmButton: false,
            timer: 1500
        });
        return false;                 
    }
    if($("#item-container").children().length == 0 ){
        swal.fire({
            type: 'error',
            title:'Peringatan',
            text:'Item harus memiliki setidaknya satu produk',
            showConfirmButton: false,
            timer: 1500
        });                 
        return false;                
    }            
    form.validate({})
    if(!form.valid()){
        return false;
    }
    btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
    form.ajaxSubmit({
        url: form.attr("action"),
        beforeSend: function () {
            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
        },
        success: function(response, status, xhr, $form) {
            console.log(response);
            var data = jQuery.parseJSON(response);
            if(data.success){
                window.location.href = base_url+"produk";
            } else {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                swal.fire({
                    type: 'error',
                    text:data.message,
                    showConfirmButton: false,
                    timer: 1500
                });                        
            }
            $('.wrapper-loading').fadeOut().addClass('hidden');
        },error: function (xhr, ajaxOptions, thrownError) {
            $('.wrapper-loading').fadeOut().addClass('hidden');
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });

});
