"use strict";
// Class Definition
var KTPOS = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var noCount = 1;
        var noChose = 0;
        var totalItem = 0;
        var potongan = 0;
        var tambahan = 0;
        var grandTotal = 0;
        var transaksi = {};
        var textTrans = {};
        var terbayar = 0;
        var promo_saved = $("#promo_saved").length>0?JSON.parse($("#promo_saved").val()): {};
        var previous = $("#previous").length>0? JSON.parse($("#previous").val()):'';
        var guest_url = $("#guest_url").val();
        var produk_url = $("#produk_url").val();
        $('.input-numeral').keyup(function(){
            if($(this).val() == ""){
                $(this).val("0");
            }
        });
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        var guest_table = $("#guest-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: guest_url,
            columns:[{data:"no"},{data:'guest_nama'},{data:'guest_alamat'},{data:'guest_telepon'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $('.modal').on('hidden.bs.modal', function () {
            $("#process-button").css('display','inline');
            $("#order-button").css('display','inline');
        })
        $('.modal').on('show.bs.modal', function () {
            $("#process-button").css('display','none');
            $("#order-button").css('display','none');
        })
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_url,
            columns:[{data:"produk_kode"},
                {data:'produk_nama'},
                {data:'jenis_produk_nama'},
                {data:'stock_produk_seri'},
                // {data:'stock_produk_qty'},
                {data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $("#generalSearch").on('keyup', function(e){
            produk_table.search($("#generalSearch").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_table.column(i).search(val ? val : '', false, false);
            });
            produk_table.table().draw();
        })
        $(".textSearch").keyup(function(){
            $(this).trigger('change');
        })
        $(".searchInput").change(function(){
            produk_table.search($("#generalSearch").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }

            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_table.column(i).search(val ? val : '', false, false);
            });
            produk_table.table().draw();
        })
        $("#barcode").keypress(function(event){
            var nilai_konversi = 1;
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == 13){
                var barcode = $(this).val();
                $.ajax({
                    type: "POST",
                    url: produk_url+"?stock_produk_seri="+barcode,
                    cache: false,
                    success: function(response){
                        var result = jQuery.parseJSON(response);
                        var object = result.aaData[0]
                        $("#produk_nama_"+noChose).val(object.produk_nama);
                        if(transaksi["trans"+noChose].tipe_penjualan == "eceran"){
                            $("#harga_"+noChose).html(object.harga_eceran);
                            $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_eceran)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
                        } else{
                            $("#harga_"+noChose).html(object.harga_grosir);
                            $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_eceran)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
                        }

                        $("#kode_"+noChose).val(object.produk_kode);
                        get_satuan(object.produk_id,noChose,object.stock_produk_qty)
                        transaksi["trans"+noChose].produk_nama = object.produk_nama;
                        transaksi["trans"+noChose].produk_kode = object.produk_kode;
                        transaksi["trans"+noChose].satuan_id = object.satuan_id;
                        transaksi["trans"+noChose].satuan_nama = object.satuan_nama;
                        transaksi["trans"+noChose].produk_id = object.produk_id;
                        transaksi["trans"+noChose].harga_eceran = object.harga_eceran;
                        transaksi["trans"+noChose].harga_grosir = object.harga_grosir;
                        transaksi["trans"+noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
                        transaksi["trans"+noChose].ar_harga = object.ar_harga;
                        transaksi["trans"+noChose].hpp = object.hpp;
                        transaksi["trans"+noChose].stock_produk_id = object.stock_produk_id;
                        transaksi["trans"+noChose].stock_produk_qty = object.stock_produk_qty;
                        transaksi["trans"+noChose].stock_ready = object.stock_produk_qty;
                        transaksi["trans"+noChose].lokasi_id = object.lokasi_id;
                        transaksi["trans"+noChose].nilai_konversi = nilai_konversi;
                        transaksi["trans"+noChose].jumlah = 1;
                        $("#jumlah_"+noChose).val(1)
                        $("#kt_modal_search_produk").modal("hide");
                        getSubtotal(noChose);
                        $("#jumlah_"+noChose).focus();
                        get_promo(object.produk_id)

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }

        })
        $("#guest_child").on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#guest-form [name="' + key + '"]').val(value);
            })
            $("#kt_modal_search_pelanggan").modal("hide");
        })
        $("#item_child").on('click','.item-search',function(){
            noChose = $(this).data('no');
            $("#kt_modal_search_produk").modal("show");
            $( "#barcode" ).focus();
        })
        $("#produk_child").on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            console.log(object)
            var id = object.produk_id;
            var nilai_konversi = 1;
            var stock_ready = object.stock_produk_qty;
            $("#produk_nama_"+noChose).val(object.produk_nama);
            if(transaksi["trans"+noChose].tipe_penjualan == "eceran"){
                $("#harga_"+noChose).html(object.harga_eceran);
                $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_eceran)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
            } else{
                $("#harga_"+noChose).html(object.harga_grosir);
                $("#satuan_nama_"+noChose).html('<option data-harga="'+intVal(object.harga_grosir)+'" data-konversi="'+nilai_konversi+'" data-stock_ready="'+object.stock_produk_qty+'" data-satuan_id="'+object.satuan_id+'" value="'+object.satuan_id+'" data-ar-min="'+object.ar_minimal_pembelian+'" data-ar-harga="'+object.ar_harga+'">'+object.satuan_nama+'</option>');
            }
            $("#nilai_konversi_"+noChose).html(nilai_konversi);
            $("#kode_"+noChose).val(object.produk_kode);

            get_satuan(id,noChose,stock_ready)

            transaksi["trans"+noChose].produk_nama = object.produk_nama;
            transaksi["trans"+noChose].produk_kode = object.produk_kode;
            transaksi["trans"+noChose].satuan_nama = object.satuan_nama;
            transaksi["trans"+noChose].satuan_id = object.satuan_id;
            transaksi["trans"+noChose].produk_id = object.produk_id;
            transaksi["trans"+noChose].harga_eceran = object.harga_eceran;
            transaksi["trans"+noChose].harga_grosir = object.harga_grosir;
            transaksi["trans"+noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
            transaksi["trans"+noChose].ar_harga = object.ar_harga;
            transaksi["trans"+noChose].hpp = object.hpp;
            transaksi["trans"+noChose].stock_produk_id = object.stock_produk_id;
            transaksi["trans"+noChose].stock_produk_qty = object.stock_produk_qty;
            transaksi["trans"+noChose].lokasi_id = object.lokasi_id;
            transaksi["trans"+noChose].stock_ready = object.stock_produk_qty;
            transaksi["trans"+noChose].nilai_konversi = nilai_konversi;
            transaksi["trans"+noChose].jumlah = 1;
            $("#jumlah_"+noChose).val(1)
            $("#kt_modal_search_produk").modal("hide");
            getSubtotal(noChose);
            //get_promo(object.produk_id)
        })
        function get_promo(produk_id) {
            $.ajax({
                url:base_url+"resto-pos/get-promo",
                type:'post',
                data:{'produk_id':produk_id},
                success:function (response) {
                    var data = JSON.parse(response)
                    if(data.data!=null){
                        promo_saved['promo_'+produk_id] = {}
                        $("#promo_child").html('');
                        $.each(data.data,function (i,val) {
                            var text = '<tr>' +
                                            '<td>'+val.discount_nama+'</td>'+
                                            '<td>'+val.keterangan+'</td>'+
                                            '<td>' +
                                                '<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">'+
                                                '<textarea style="display:none">'+JSON.stringify(val)+'</textarea>'+
                                                    '<a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >'+
                                                    '<i class="fa fa-calendar-check"></i>&nbsp; Pilih'+
                                                    '</a>'+
                                            '</div>'+
                                '</td>'+
                                        '</tr>';
                            $("#promo_child").append(text)
                        })
                        //$("#kt_modal_promo").modal('show');
                        console.log(promo_saved)
                    }
                },
                error:function (response) {
                    console.log(response)
                }
            })
        }
        function get_satuan(id,index,stock_produk_qty){
            var nilai_konversi = 1;
            var stock_ready = stock_produk_qty;
            $.ajax({
                url : base_url+"pos/get-unit-satuan",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){

                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        var jumlah_satuan = data[i].jumlah_satuan_unit;
                        var stock_ready_konversi = (nilai_konversi/jumlah_satuan)*stock_ready;
                        html += '<option data-harga='+data[i].harga_jual+' data-satuan_nama='+data[i].satuan_nama+' data-konversi='+data[i].jumlah_satuan_unit+' data-stock_ready="'+stock_ready_konversi+'" data-satuan_id="'+data[i].satuan_id+'" value='+data[i].unit_id+' data-ar-min="'+data[i].ar_minimal_pembelian+'" data-ar-harga="'+data[i].ar_harga+'">'+data[i].satuan_nama+'</option>';
                        console.log(data)
                    }
                    $("#satuan_nama_"+index).append(html);
                }
            });
        }
        $("#button_add_trans").click(function(){
            var item = {produk_id:null,produk_nama:null,satuan_nama:null,produk_kode:null,tipe_penjualan:"eceran",jumlah:0,harga_eceran:0,harga_grosir:0,hpp:0,subtotal:0,keterangan:''}
            transaksi["trans"+noCount] = item;
            var tr = itemRow();
            $("#item_child").append(tr);
            new Cleave($("#jumlah_"+noCount), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalScale: 4
            })
            noChose = noCount;
            $("#kt_modal_search_produk").modal("show");
            $( "#barcode" ).focus();
            noCount++;
        })
        var itemRow = function(){
            var text = '<tr id="row_'+noCount+'">';
            text +=  '<td><div class="input-group col-12">'+
                '<input type="text" class="form-control" id="produk_nama_'+noCount+'" readonly="">'+
                '<div class="input-group-append"><button class="btn btn-primary item-search" type="button" data-no="'+noCount+'"><i class="flaticon-search"></i></button></div>'+
                '</div></td>';
            text +=  '<td>'+
                '<select class="col-12 m-select form-control sel-satuan" name="" data-no="'+noCount+'" id="satuan_nama_'+noCount+'">'+

                '</select>'+
                '</td>';
            text +=  '<td><input type="text" class="form-control input-numeral input-decimal" data-no="'+noCount+'" value="0" id="jumlah_'+noCount+'"></td>';
            text +=  '<td><span id="harga_'+noCount+'">0</span></td>';
            text +=  '<td><span id="subtotal_'+noCount+'">0</span></td>';
            text +=  '<td style="display:none"><span id="nilai_konversi_'+noCount+'"></span></td>';
            text +=  '<td><textarea class="form-control keterangan" data-no="'+noCount+'"></textarea></td>';
            text +=  '<td><button class="btn btn-danger delete-btn" data-no="'+noCount+'"><i class="flaticon2-trash"></i></button></td>';
            text += '</tr>';
            return text;
        }
        $("#item_child").on('keyup','.keterangan',function () {
            var no = $(this).data('no')
            transaksi['trans'+no].keterangan = $(this).val();
            getSubtotal(no)
            getTotal(no)
        })
        $("#item_child").on('change','.sel-satuan',function(){
            var harga_satuan = ($(this).find(':selected').data('harga'));
            var stock_ready = ($(this).find(':selected').data('stock_ready'));
            var nilai_konversi = ($(this).find(':selected').data('konversi'));
            var satuan_nama = ($(this).find(':selected').data('satuan_nama'));
            var satuan_id = ($(this).find(':selected').data('satuan_id'));
            var ar_min = ($(this).find(':selected').data('ar-min'));
            var ar_harga = ($(this).find(':selected').data('ar-harga'));
            var no = $(this).data('no');
            var reset_value = 0;
            transaksi["trans"+no].tipe_penjualan = $(this).val();
            transaksi["trans"+no].harga_eceran = harga_satuan;
            transaksi["trans"+no].satuan_nama = satuan_nama;
            transaksi["trans"+no].stock_ready = stock_ready;
            transaksi["trans"+no].ar_minimal_pembelian = ar_min;
            transaksi["trans"+no].ar_harga = ar_harga;
            console.log(ar_min)
            getSubtotal(no);
            $("#nilai_konversi_"+no).html(nilai_konversi);
            transaksi["trans"+no].nilai_konversi = nilai_konversi;
            transaksi["trans"+noChose].satuan_id = satuan_id;
            var harga_satuan_number_format = harga_satuan.toLocaleString(
                undefined, // leave undefined to use the browser's locale,
                // or use a string like 'en-US' to override it.
            );
            console.log(harga_satuan_number_format)
            if(transaksi["trans"+no].tipe_penjualan == "eceran"){
                $("#harga_"+no).html(harga_satuan_number_format );
                $("#jumlah_"+no).val(reset_value);
                $("#subtotal_"+no).html(reset_value);
                transaksi["trans"+no].jumlah = reset_value;
            } else{
                $("#harga_"+no).html(harga_satuan_number_format );
                $("#jumlah_"+no).val(reset_value);
                $("#subtotal_"+no).html(reset_value);
                transaksi["trans"+no].jumlah = reset_value;
            }
            getSubtotal(no);
            getTotal();
        })
        $('#item_child').on('click','.delete-btn',function(){
            var no = $(this).data("no");
            $("#row_"+no).remove();
            delete transaksi["trans"+no];
            getTotal()
        })
        $('#item_child').on('keyup','.input-numeral',function(){
            var no = $(this).data("no");
            var nilai_maxs =  transaksi["trans"+no].stock_ready;
            console.log(nilai_maxs)
            var nilai_input = $(this).val();
            var element = $(this);
            var id = $(this).attr('id');
            var no = id.replace("jumlah_","");

            if(transaksi["trans"+no].ar_minimal_pembelian != undefined){

                var ar_min = (transaksi["trans"+no].ar_minimal_pembelian+"").split("|");
                var ar_harga = (transaksi["trans"+no].ar_harga+"").split("|");
                $.each(ar_min,function(i,value){
                    if(element.val() >= value && value != 0){
                        transaksi["trans"+no].harga_eceran = ar_harga[i];
                        $("#harga_"+noChose).html( KTUtil.numberString(transaksi["trans"+no].harga_eceran));
                    }
                })
                transaksi["trans"+no].jumlah = intVal($(this).val());
                getSubtotal(no);
            } else {
                $(this).val("0");
            }

        })
        $('#item_child').on('change','input[type=radio]',function(){
            var id = $(this).attr('name');
            var no = id.replace("radio","");
            transaksi["trans"+no].tipe_penjualan = $(this).val();

            if(transaksi["trans"+no].tipe_penjualan == "eceran"){
                $("#harga_"+no).html(transaksi["trans"+no].harga_eceran);
            } else{
                $("#harga_"+no).html(transaksi["trans"+no].harga_grosir);
            }
            getSubtotal(no);
        })
        var getSubtotal = function(index){
            var total = 0;
            total = intVal(transaksi["trans"+index].jumlah) * intVal(transaksi["trans"+index].harga_eceran);
            transaksi["trans"+index].subtotal =  KTUtil.numberString(total.toFixed(0))
            $("#subtotal_"+index).html(transaksi["trans"+index].subtotal);
            getTotal(index);
        }
        var intVal = function(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };

        var getTotal = function(index){
            var previousTotal = previous.grand_total == undefined ? 0 : previous.grand_total
            totalItem = intVal(previousTotal);
            $.each(transaksi,function(i,data){
                totalItem += intVal(data.subtotal)
            })
            console.log(totalItem)
            $("#total-item").html(KTUtil.numberString(totalItem.toFixed(0)));
            $("#total-bayar").html(KTUtil.numberString(totalItem.toFixed(0)));
            grandTotal = (totalItem + tambahan) - potongan
            $("#grand-total").html(KTUtil.numberString(grandTotal.toFixed(0)));
            $("#grand-bayar").html(KTUtil.numberString(grandTotal.toFixed(0)));
            textTrans = {potongan:potongan,tambahan:tambahan,total_item:totalItem,grand_total:grandTotal,terbayar:terbayar,item:transaksi}
            $("#textTransaksi").html(JSON.stringify(textTrans));

        }
        getTotal(2)
        $("#potongan").keyup(function(){
            $("#potongan-bayar").html($(this).val());
            potongan = intVal($(this).val())
            getTotal()
        })
        $("#terbayar").keyup(function(){
            terbayar = intVal($(this).val())
            getTotal()
        })
        $("#tambahan").keyup(function(){
            $("#tambahan-bayar").html($(this).val());
            tambahan = intVal($(this).val())
            getTotal()
        })
        $("#process-button").click(function(){
            if(grandTotal == 0 ){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Total transaksi 0",
                    showConfirmButton: false,
                    timer: 1500
                });
                return
            }
            $("#terbayar").val(KTUtil.numberString(grandTotal.toFixed(0)));
            var form = $("#save-form");
            terbayar = grandTotal;
            if($("#tipe_pembayaran").find(':selected').data('kembalian') == 1){
                $("#terbayar").removeClass("readonly");
                $("#terbayar").val(0);
            }
            if($("#lokasi_id").val() != undefined && $("#lokasi_id").val() == "" ){
                form.validate().element("#lokasi_id");
                return
            }
            $("#kt_modal_pembayaran").modal("show")
        })
        $("#order-button").click(function(){
            var penjualan_id = $("#penjualan_id").val();
            if(penjualan_id!=''){
                save_order()
            }else{
                $("#kt_modal_order").modal("show")
            }

        })
        $("#btn-save").click(function(){
            var btn = $(this);
            var form = $("#save-form");
            var enough = true;
            if($("#terbayar").prop("disabled") == false){
                if(intVal($("#terbayar").val()) < grandTotal){
                    enough = false;
                }
            }
            if(!enough && ($("#tipe_pembayaran").find(':selected').data('jenis') != "kredit")){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Uang terbayar kurang",
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            if($("#tipe_pembayaran").find(':selected').data('jenis') == "kredit" && $("#tenggat_pelunasan").val() == ""){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Tenggat pembayaran tidak boleh kosong",
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            form.validate({});
            if (!form.valid()) {
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        console.log(data)
                        $("#kt_modal_pembayaran").modal("hide");
                        if($("#tipe_pembayaran").find(":selected").data("kembalian")==1){
                            Swal.fire({
                                type: 'success',
                                title: 'Rp. '+data.kembalian,
                                text: 'sisa kembalian'
                            }).then(function() {
                                form.clearForm();
                                form.validate().resetForm();
                                var url = data.url;
                                window.open(url,"_blank");
                                clearForm();
                            });
                        } else {
                            if($("#tipe_pembayaran").val()==-13){
                                snap.pay(data.token,{
                                    onSuccess: function(result){
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Success',
                                            text: 'Proses Pembayaran Berhasil'
                                        }).then(function() {
                                            form.clearForm();
                                            form.validate().resetForm();
                                            var url = data.url;
                                            window.open(url,"_blank");
                                            clearForm();
                                        });
                                    },
                                    onPending: function(result){
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Pending',
                                            text: 'Menunggu Pembayaran'
                                        }).then(function() {
                                            form.clearForm();
                                            form.validate().resetForm();
                                            var url = data.url;
                                            window.open(url,"_blank");
                                            clearForm();
                                        });
                                    },
                                    onError: function(result){
                                        Swal.fire({
                                            type: 'error',
                                            title: 'Error',
                                            text: 'Proses Pembayaran Gagal'
                                        })
                                    },
                                    onClose:function () {
                                        $.ajax({
                                            url:base_url+"/resto-pos/delete",
                                            data:{'id':data.id},
                                            type:'post',
                                            success:function (response) {

                                                console.log(response)
                                                console.log(data.id)
                                            }

                                        })
                                    }
                                })
                            }else{
                                Swal.fire({
                                    type: 'success',
                                    title: 'Transaksi Berhasil',
                                }).then(function() {
                                    form.clearForm();
                                    form.validate().resetForm();
                                    var url = data.url;
                                    window.open(url,"_blank");
                                    clearForm();
                                });
                            }


                        }
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })
        $("#input_order_name").keyup(function () {
            $("#order_name").val($(this).val())
        })
        $("#input_order_note").keyup(function () {
            $("#order_note").val($(this).val())
        })
        $("#btn-save-order").click(function(){
           save_order()
        })
        function save_order(){
            var url = base_url+"resto-pos/save-order"
            var btn = $(this);
            var form = $("#save-form");
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: url,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        $("#kt_modal_").modal("hide");
                        window.location.href = data.url
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        $("#btn-save-kas").on('click', function () {
            var btn = $(this);
            var form = $("#save-form");
            form.validate({});
            if (!form.valid()) {
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        swal.fire({
                            type: 'success',
                            title: 'Berhasil melakukan opening POS',
                        }).then(function() {
                            window.location.reload()
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });

        $(".form-save-kas").on('submit', function (e) {
            e.preventDefault();
            var btn = $(this);
            var method = btn.attr('method');
            var formData = btn.serialize();
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $.ajax({
                url: $(this).attr("action"),
                type: method,
                data: formData,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        swal.fire({
                            type: 'success',
                            title: 'Berhasil melakukan opening POS',
                        }).then(function() {
                            window.location.reload()
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
            return false;
        })
        $("#btn-closing").click(function(){

            $.ajax({
                type:'get',
                url:base_url+'resto-pos/info-closing',
                cache:false,
                success: function (response) {
                    var data = JSON.parse(response)
                    $.each(data,function (i,val) {
                        $("#kt_modal_closing [name="+i+"]").html(': '+val)
                    })
                    $("#kt_modal_closing").modal('show');
                }
            })

        })
        $("#kt_closing_submit").click(function () {
            $.ajax({
                type: "POST",
                url: base_url+'pos/closing',
                cache: false,
                success: function(response){
                    var data = JSON.parse(response)
                    if(data.success){
                        window.open(base_url+'pos/print-closing/'+data.log_kasir_id,'__blank')
                        window.location.reload()
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })
        var clearForm = function(){
            $(".input-numeral").val(0);
            $("#total-item").html(0);
            $("#total-bayar").html(0);
            $("#grand-total").html(0);
            $("#grand-bayar").html(0);
            $("#tambahan").html(0);
            $("#tambahan-bayar").html(0);
            $("#potongan").html(0);
            $("#potongan-bayar").html(0);
            $("#item_child").html("");
            noCount = 1;
            noChose = 0;
            totalItem = 0;
            potongan = 0;
            tambahan = 0;
            grandTotal = 0;
            transaksi = {};
            textTrans = {};
            terbayar = 0;
            //window.location.href = base_url+"resto-pos";
            window.location.reload();
        }
        $("#tipe_pembayaran").change(function(){
            $("#input_tipe_pembayaran").val($(this).val());
            $("#input_jenis_pembayaran").val($(this).find(':selected').data('jenis'));
            if($(this).find(':selected').data('kembalian') == 1){
                $("#terbayar").removeClass("read");
                $("#terbayar").addClass("input-numeral");
                $("#terbayar").val(0);
            } else {
                $("#terbayar").val(KTUtil.numberString(grandTotal.toFixed(0)));
                terbayar = grandTotal;
                getTotal()
                $("#terbayar").addClass("read");
                $("#terbayar").removeClass("input-numeral");
            }
            if($(this).find(':selected').data('jenis') == "kredit"){
                $("#kredit_container").css('display','block');
                $("#terbayar_container").css('display','none');

            } else {
                $("#kredit_container").css('display','none');
                $("#terbayar_container").css('display','flex');
            }
            if($(this).find(':selected').data('additional') == 1){
                $(".additional").removeClass("read");
                $("#additional_container").css('display','block');
            } else {
                console.log(321)
                $(".additional").addClass("read");
                $(".additional").val("");
                $("#additional_container").css('display','none');
            }
        })
        $('#tenggat_pelunasan').change(function(){
            $('#input_tenggat_pelunasan').val($(this).val());
        })
        $('#item-container').on('keydown','.readonly',function(e){
            e.preventDefault();
        });
        $(".readonly").keydown(function(e){
            if($("#tipe_pembayaran").find(":selected").data("kembalian")==0){
                e.preventDefault()
            }

        })
        $('.additional').change(function(){
            var name = $(this).attr("name");
            var value = $(this).val();
            $("input[name="+name+"]").val(value)
        })
        $("#lokasi_id").change(function(){
            var id = $(this).val();
            sess_change_lokasi(id)
            $(".input-numeral").val(0);
            $("#total-item").html(0);
            $("#total-bayar").html(0);
            $("#grand-total").html(0);
            $("#grand-bayar").html(0);
            $("#tambahan").html(0);
            $("#tambahan-bayar").html(0);
            $("#potongan").html(0);
            $("#potongan-bayar").html(0);
            $("#item_child").html("");
            noCount = 1;
            noChose = 0;
            totalItem = 0;
            potongan = 0;
            tambahan = 0;
            grandTotal = 0;
            transaksi = {};
            textTrans = {};
            terbayar = 0;
        })
        function sess_change_lokasi(key){
            $.ajax({
                type: "POST",
                url: base_url+'pos/utility/change-location',
                data:{'lokasi_id':key},
                cache: false,
                success: function(response){
                    console.log(response)
                    produk_table.ajax.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        $("#promo_child").on('click','.chose-btn',function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#kt_modal_promo").modal('hide');
            var produk_id = transaksi["trans"+noChose].produk_id
            if(promo_saved['promo_'+produk_id]!=undefined){
                promo_saved['promo_'+produk_id] = object
            }
            console.log(promo_saved)
        })
    }
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPOS.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
        })
    });
    $('.input-decimal').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralDecimalScale: 4
        })

    });
});
