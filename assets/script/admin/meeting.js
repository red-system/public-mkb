"use strict";

// Class Definition
var KTMeeting = function() {
    var general = function(){
        var base_url = $("#base_url").val();

        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        
        $("#kt_add_submit_kehadiran").click(function(){
            var btn = $(this);
            var form = $("#kt_add_kehadiran");
            
            form.validate({})
            if(!form.valid()){
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        swal.fire({
                            type: 'success',
                            title: 'Terima kasih karena sudah mengisi formulir kehadiran. Info selengkapnya akan diinformasikan melalui grup Whatsapp & Telegram.',
                        }).then(function() {
                            window.location.href = base_url+"meeting";
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })
        
        var url_produk = $("#list_produk").val();
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: url_produk,
            columns:[{data:'produk_kode'},{data:'produk_nama'},{data:'stock'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $('#child_data_ajax').on('click','.kehadiran-status-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.meeting_id;
            get_detail(id)
        });
        function get_detail(id,type = 0){
            $.ajax({
                type: "POST",
                url: base_url+'meeting/detail',
                data:{'meeting_id':id},
                cache: false,
                success: function(response){
                    if(type==0){
                        parseDetail(response);
                    } else {
                        // display_penerimaan(response)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        
        function parseDetail(response){
            var object = JSON.parse(response);
            $.each(object,function(key,value){
                $('#kt_modal_detail_notif label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_notif [name="' + key + '"]').val(value);
            })
            $("#kt_modal_detail_notif").modal("show");
        }
        

        

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTMeeting.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});

