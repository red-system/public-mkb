"use strict";

// Class Definition
var KTPoProduk = function() {
    var base_url = $("#base_url").val();
    var demo = $("#demo").length > 0 ? $("#demo").val() : 0;
    var is_list = $("#is_list").length>0?true:false;
    var is_cash = $("#is_cash").length>0?true:false;
    var general = function(){

        var min_qty = $("#minimal_po").val();
        var no = $("#last_number").length>0?$("#last_number").val():0;
        if($("#index_no").length>0){
            no = $("#index_no").val();
        }
        var produk_no = 0;
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $("#add_item").click(function(){

            if($("#reseller_id").val()==""){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Silakan Pilih Agen/Super Agen Terlebih dulu',
                    showConfirmButton: false,
                    timer: 1500
                });
            }else{
                $("#kt_modal_hutang").modal("show");
                var text2 = '<div class="row" id="produk_item_'+no+'">' +
                    '<div class="col-md-3">' +
                    '<div class="form-group row">' +
                    '<label for="example-text-input" class="col-3 col-form-label">No PO<b class="label--required">*</b></label>' +
                    '<div class="col-9">' +
                    '<label for="example-text-input" id="item_po_produk_no_'+no+'" class="col-12 col-form-label"></label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group row"><label for="example-text-input" class="col-6 col-form-label">Harga<b class="label--required">*'+
                    '</b></label>' +
                    '<label for="example-text-input" class="col-6 col-form-label" data-no="'+no+'"  id="item_produk_harga_'+no+'"></label>'+
                    '</div></div>' +
                    '<div class="col-md-2"><div class="form-group row"><label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b>'+
                    '</label><div class="col-6"><input type="text" class="form-control input-numeral jumlah-produk" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_jumlah_'+no+'" name="item_produk[produk_'+no+'][jumlah]">'+
                    '</div><label for="example-text-input" class="col-3 col-form-label" id="item_produk_satuan_'+no+'">'+
                    '</label></div></div><div class="col-md-2"><div class="form-group row"><label for="example-text-input" class="col-6 col-form-label">Subtotal</label><div class="col-6">'+
                    '<label class="col-form-label" id="item_produk_subtotal_display_'+no+'">0</label><input type="hidden" class="form-control input-numeral subtotal" autocomplete="off" data-no="'+no+'" value="0" id="item_produk_subtotal_'+no+'" name="item_produk[produk_'+no+'][subtotal]">'+
                    '</div></div></div><div class="col-md-1"><button type="button" class="btn btn-danger btn-sm delete-produk" data-no="'+no+'" data-item-produk="produk_item_'+no+'"><i class="flaticon2-trash"></i>'+
                    '</button></div></div>';
                var text = '<tr class="" id="produk_item_'+no+'">' +
                    '<td>' +
                    '<label for="example-text-input" id="item_po_produk_no_'+no+'" class="col-12 col-form-label"></label>' +
                    '</td>'+
                    '<td>' +
                    '<label for="example-text-input" id="item_voucher_no_'+no+'" class="col-12 col-form-label"></label>' +
                    '</td>'+
                    '<td>' +
                    '<label for="example-text-input" id="item_nama_produk_no_'+no+'" class="col-12 col-form-label"></label>' +
                    '</td>'+
                    '<td>' +
                    '<label for="example-text-input " id="item_jumlah_no_'+no+'" data-produk="" class="col-12 col-form-label jumlah-produk"></label>' +
                    '</td>'+
                    '<td style="text-align: center">' +
                    '<input type="hidden" name="hutang_produk_id[]" id="hutang_produk_id_no_'+no+'">'+
                    '<button type="button" class="btn btn-danger btn-sm delete-produk" data-no="'+no+'" data-item-produk="produk_item_'+no+'"><i class="flaticon2-trash"></i>'+
                    '</button> ' +
                    '<div id="check_no_'+no+'" style="display: none">' +

                    '</div>'+
                    '</td>'+
                    '</tr>';
                $("#item-container").append(text);
                produk_no = no;
                no++;
            }


        })
        $('#hutang_child').on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json)
            if($("#hutang_no_"+object.hutang_produk_id).length>0){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Tidak boleh memasukan record yang sama',
                    showConfirmButton: false,
                    timer: 1500
                });
            }else {
                console.log(object)
                $("#item_po_produk_no_" + produk_no).html(object.po_produk_no);
                $("#item_voucher_no_" + produk_no).html(object.voucher_code);
                $("#item_nama_produk_no_" + produk_no).html(object.produk_nama);
                $("#item_jumlah_no_" + produk_no).html(object.jumlah);
                $("#item_jumlah_no_" + produk_no).attr("data-produk",object.produk_id)
                $("#item_jumlah_no_" + produk_no).attr("data-min",object.min_wd);
                $("#hutang_produk_id_no_" + produk_no).val(object.hutang_produk_id);
                var text = "<div id='hutang_no_" + object.hutang_produk_id + "'></div>"
                $("#check_no_" + produk_no).html(text);

                $("#kt_modal_hutang").modal("hide");
            }
        });
        $("#kt_modal_hutang").on('hide.bs.modal',function () {
            if($("#item_po_produk_no_" + produk_no).html()==""){
            $("#produk_item_"+produk_no).remove();
            }
        })
        function cek(){
            var temp = 0;
            var treshold = 100;
            var temp_produk_id = 0;
            var hitung_jenis_produk = 0;
            $.each($("#item-container .jumlah-produk"),function (i,val) {
                var jumlah = intVal($(val).html())
                temp +=jumlah
                if(hitung_jenis_produk == 0){
                    hitung_jenis_produk  = hitung_jenis_produk+1;
                }else if(temp_produk_id!=$(val).attr("data-produk")){
                    hitung_jenis_produk  = hitung_jenis_produk+1;
                }else{
                    hitung_jenis_produk  = hitung_jenis_produk;
                }
                temp_produk_id = $(val).attr("data-produk");
                treshold = intVal($(val).attr("data-min"));
            })
            if(hitung_jenis_produk>1){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Harus 1 jenis produk',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            if((temp<treshold)){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Minimal jumlah produk adalah '+treshold,
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            } else {

                    return true;
            }
        }
        $("#kt_add_submit").click(function(e){
            e.preventDefault();
            var btn = $(this);
            var form = $("#kt_add")
            var is_zero = false;
            var ceks = cek()
            if(!ceks){
                return false;
            }
            $("#item-container .input-numeral").each(function(){
                if($(this).val()==0){
                    is_zero = true;
                }
            })
            if(is_zero){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Tidak boleh ada jumlah maupun harga yang bernilai 0',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            if($("#item-container").children().length == 0 ){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Order harus memiliki setidaknya satu produk',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            form.validate({})
            if(!form.valid()){
                return false;
            }
            // btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    //  $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        window.location.href = base_url+"request-pembayaran-piutang";
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });           if($("#item-container").children().length == 0 ){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Order harus memiliki setidaknya satu produk',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            form.validate({})
            if(!form.valid()){
                return false;
            }
            // btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    //  $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        //here
                        var tend = is_cash == true ? "withdraw-cash" : "request-pembayaran-piutang";
                        window.location.href = base_url+tend;
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })
        $("#item-container").on('keydown','.readonly',function(e){
            e.preventDefault();
        })
        $(".input-numeral").keyup(function(){
            if($(this).val()== ""){
                $(this).val(0)
            }
        })
        $("#item-container").on("click",".delete-produk",function(){
            var index = $(this).data("no");
            $("#produk_item_"+index).remove();
            get_subtotal();
        })
        function intVal(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        $("#item-container").on('keyup','.jumlah-produk',function(){

            var index = $(this).data('no');
            var subtotal = intVal($(this).val()) * intVal($("#item_produk_harga_"+index).html())
            $("#item_produk_subtotal_"+index).val(subtotal);
            $("#item_produk_subtotal_display_"+index).html(KTUtil.numberString(subtotal.toFixed(0)));
            get_subtotal();
        })
        $("#item-container").on('change','.jumlah-produk',function(){
            if(intVal($(this).val())<min_qty){
                $(this).val(min_qty)
            }
        })

        $("#item-container").on('keyup','.harga-produk',function(){
            var index = $(this).data('no');
            var subtotal = intVal($(this).val()) * intVal($("#item_produk_jumlah_"+index).val())
            $("#item_produk_subtotal_"+index).val(subtotal);
            $("#item_produk_subtotal_display_"+index).html(KTUtil.numberString(subtotal.toFixed(0)));
            get_subtotal();
        })
        function get_subtotal(){

            var total = 0;
            $( "#item-container .subtotal" ).each(function() {
                total += intVal($(this).val());
            })
            $("#total_item").html(KTUtil.numberString(total.toFixed(0)));
            $("#input_total_item").val(total);
            total = total + intVal($("#tambahan").val());
            total = total - intVal($("#potongan").val());
            $("#grand_total").html(KTUtil.numberString(total.toFixed(0)));
            $("#input_grand_total").val(total);

        }
        $("#potongan").keyup(function(){
            get_subtotal();
        })
        $("#tambahan").keyup(function(){
            get_subtotal();
        })
        $('#item-container').on('click','.produk-search',function(){
            produk_no = $(this).data('no');
            $("#kt_modal_produk").modal("show");
        });
        $(".reseller-search").click(function () {
            $("#kt_modal_reseller").modal("show");
        })

        $(".img-input").change(function(){
            var display = $(this).data('display');
            readURL(this,display);
        })
        function readURL(input,display) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+display).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        var url_hutang = $("#list_hutang").val();
        //use
        var hutang_table = $("#hutang-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            ajax: url_hutang,
            columns:[{data:'po_produk_no'},{data:'produk_nama'},{data:'jumlah'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        //use
        $('#child_data_ajax').on('click','.view-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.po_produk_id;
            get_detail(id)
        });
        //use

        function get_detail(id,type = 0){
            $.ajax({
                type: "POST",
                url: base_url+'po-produk/detail',
                data:{'po_produk_id':id},
                cache: false,
                success: function(response){
                    if(type==0){
                        parseDetail(response);
                    } else {
                        // display_penerimaan(response)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        $('#child_data_ajax').on('click','.penerimaan-status-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.po_produk_id;
            $("#po_produk_id_penerimaan").val(id);
            $("#kt_penerimaan").modal("show");
        });


        function parseDetail(response){
            var object = JSON.parse(response);
            $.each(object,function(key,value){
                $('#kt_modal_detail_po_produk label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_po_produk [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_po_produk .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_po_produk .img-preview').attr("src",base_url+value);
                }
            })
            $("#show_bukti").attr("data-url",object.bukti_url)

            $("#view_child_data").html("");
            $.each(object.item,function(key,value){
                var text = '<tr>'+
                    '<td >'+value.produk_nama+'</td>'+
                    '<td >'+value.harga+'</td>'+
                    '<td >'+value.jumlah+'</td>'+
                    '<td >'+value.sub_total+'</td></tr>';
                $("#view_child_data").append(text);
            })

            $("#kt_modal_detail_po_produk").modal("show");
        }
        $('#child_data_ajax').on('click','.accept-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            swal.fire({
                title: "Perhatian ...",
                text: "Apakah anda sudah memeriksa dengan seksama?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#0abb87",
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {

                    $.ajax({
                        url: base_url+"order-super-agen/accept",
                        type: "post",
                        data:{"id":object.row_id},
                        beforeSend: function () {
                            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                        },
                        success: function (response) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                                window.location.reload()
                            }else {
                                swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            $('.wrapper-loading').fadeOut().addClass('hidden');
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        });
        $('#child_data_ajax').on('click','.refuse-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#po_produk_id_refuse").val(object.row_id);
            $("#kt_refuse").modal("show");
        });
        $("#kt_produksi_penerimaan_submit").click(function(){
            var form = $("#kt_penerimaan_produksi") ;
            form.submit();
        })

        $('#kt_po_penerimaan_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    password:{
                        required: true,
                        minlength:5,
                    },
                    re_password:{
                        required: true,
                        minlength:5,
                        equalTo:'#password'
                    }
                }
            });
            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        window.location.reload();
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        $('#kt_po_refuse_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),

                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        // window.location.reload();
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        $.ajax({
            url: base_url+"suplier/option",
            type: "post",
            success: function (response) {
                states = jQuery.parseJSON(response);
                $('#kt_typeahead_1').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'states',
                    source: substringMatcher(states)
                });
                $('#kt_typeahead_2').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'states',
                    source: substringMatcher(states)
                });
            },
            error: function(request) {
            }
        });
    }
    var list = function () {
        $("#child_data_ajax").on('click','.view-btn',function () {
            var json = $(this).siblings("textarea").val()
            var object = JSON.parse(json)
            get_detail(object.pembayaran_hutang_produk_id)
        })
        $("#child_data_ajax").on('click','.submit-btn',function () {
            var json = $(this).siblings("textarea").val()
            var object = JSON.parse(json)
            $.ajax({
                type: "POST",
                url: base_url+'request-pembayaran-piutang/submit',
                data:{'pembayaran_hutang_produk_id':object.pembayaran_hutang_produk_id},
                cache: false,
                success: function(response){
                    var data = JSON.parse(response)
                    if(data.success){
                        window.location.reload()
                    }else{
                        swal.fire({
                            type: 'error',
                            title:'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        })
        function get_detail(id){
            $.ajax({
                type: "POST",
                url: base_url+'pembayaran-hutang-produk/detail',
                data:{'pembayaran_hutang_produk_id':id},
                cache: false,
                success: function(response){
                    parseDetail(response)
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function parseDetail(response){
            var object = JSON.parse(response);
            console.log(object)
            $.each(object,function(key,value){
                $('#kt_modal_detail_pembayaran_hutang label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_pembayaran_hutang [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_pembayaran_hutang .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_pembayaran_hutang .img-preview').attr("src",base_url+value);
                }
            })

            $("#view_child_data").html("");
            $("#table-item").DataTable().clear().destroy();
            $.each(object.item,function(key,value){
                var text = '<tr>'+
                    '<td >'+value.po_produk_no+'</td>'+
                    '<td >'+value.voucher_code+'</td>'+
                    '<td >'+value.nama_reseller_pemilik+'</td>'+
                    '<td >'+value.produk_nama+'</td>'+
                    '<td >'+value.jumlah+'</td></tr>';
                $("#view_child_data").append(text);

            })

            $('#kt_modal_detail_pembayaran_hutang [name="nama"]').html(object.item[0].nama_reseller)
            $("#table-item").DataTable({
                responsive: true,
                ordering: false,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            });
            $("#kt_modal_detail_pembayaran_hutang").modal("show");
        }
    }
    // Public Functions
    return {
        // public functions
        init: function() {

            if(is_list){
                list();
            }else{
                general();
            }
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPoProduk.init();

});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});