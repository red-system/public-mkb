console.log(123);
var url_hasil_survey = $("#hasil_survey_url").val();
var hasil_survey_table = $("#hasil-survey-table").DataTable({
    responsive: true,
    searching: false,
    processing: true,
    serverSide: true,
    ordering: false,
    lengthChange: false,
    bPaginate: false,
    info: false,
    pageLength: 5,
    ajax: url_hasil_survey,
    columns:[{data:'no'},{data:'nama'},{data:'leader_lama'},{data:'leader_baru'},{data:'alasan_pindah'}]
});