$(document).ready(function () {
    var base_url = $("#base_url").val();
   $("#child_data_ajax").on('click','.approve-btn',function () {
       var json = $(this).siblings('textarea').val();
       var data = JSON.parse(json)
       $.ajax({
           url:base_url+'management-reward-reseller/approve',
           method:'post',
           data:{'reward_reseller_id':data.reward_reseller_id},
           success:function (response) {
               var data = JSON.parse(response);
               if(data.success){
                   swal.fire({
                       type: 'success',
                       title: data.message,
                   }).then(function() {
                       window.location.reload();
                   });
               }
           }
       });
   });
    $("#child_data_ajax").on('click','.refuse-btn',function () {
        var json = $(this).siblings('textarea').val();
        var data = JSON.parse(json)
        $.ajax({
            url:base_url+'management-reward-reseller/refuse',
            method:'post',
            data:{'reward_reseller_id':data.reward_reseller_id},
            success:function (response) {
                var data = JSON.parse(response);
                if(data.success){
                    swal.fire({
                        type: 'success',
                        title: data.message,
                    }).then(function() {
                        window.location.reload();
                    });
                }
            }
        });
    });
});