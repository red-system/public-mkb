$(document).ready(function () {
    var base_url = $("#base_url").val();
    var city_url = base_url+'registrasi-city';
    var subdistrict_url = base_url+'registrasi-subdistrict';
    $('.province').change(function () {
        var province_ = $(this).val();
        var prefix = $(this).attr('prefix');
        var city_ = prefix+'city_id';
        var subdistrict_ = prefix+'subdistrict_id';
        if(province_!==''){
            getCity(province_,city_,subdistrict_)
        }
    })
    $('.city').change(function () {
        var city_ = $(this).val();
        var prefix = $(this).attr('prefix');
        var subdistrict_ = prefix+'subdistrict_id';
        if(city_!==''){
            getSubdistrict(city_,subdistrict_)
        }
    })
    function getCity(province_,city_,subdistrict_) {
        $.ajax({
            url:city_url,
            type:'post',
            data:{'province_id':province_},
            success:function (response) {
                var result = JSON.parse(response);
                $("#"+city_).html('<option value="">Pilih Kabupaten/Kota</option>');
                $.each(result.data,function (i,val) {
                    $("#"+city_).append('<option value="'+val.city_id+'">'+val.city_name+'</option>')
                })
                $("#"+subdistrict_).html('<option value="">Pilih Kecamatan</option>');
            }
        })
    }
    function getSubdistrict(city_,subdistrict_) {
        $.ajax({
            url:subdistrict_url,
            type:'post',
            data:{'city_id':city_},
            success:function (response) {
                var result = JSON.parse(response);
                $("#"+subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                $.each(result.data,function (i,val) {
                    $("#"+subdistrict_).append('<option value="'+val.subdistrict_id+'">'+val.subdistrict_name+'</option>')
                })

            }
        })
    }
    $(".img-input").change(function(){
        var display = $(this).data('display');
        readURL(this,display);
    })
    function readURL(input,display) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+display).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

})