$(document).ready(function () {
    var base_url = $("#base_url").val();

    $(".form-save-penukaran").on('submit', function (e) {

        e.preventDefault();
        var btn = $(this);
        var method = btn.attr('method');
        var formData = btn.serialize();
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        $.ajax({
            url: $(this).attr("action"),
            type: method,
            data: formData,
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    parseDetail(data.detail)
                } else {
                    swal.fire({
                        type: 'error',
                        title: 'Peringatan',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        return false;
    })
    function parseDetail(object){

        $.each(object,function(key,value){
            $('#kt_modal_detail_voucher label[name="' + key + '"]').html(value);
            $('#kt_modal_detail_voucher [name="' + key + '"]').val(value);
            var field = $('#kt_modal_detail_voucher .img-preview').data("field");
            if (field == key){
                $('#kt_modal_detail_voucher .img-preview').attr("src",base_url+value);
            }
        })
        $("#process_penukaran").attr("data-po_kode",object.voucher_code)

        $("#view_child_data").html("");
        $.each(object.item,function(key,value){
            console.log(key)
            var text = '<tr>'+
                '<td >'+value.produk_nama+'</td>'+
                '<td >'+value.jumlah_qty+'</td>'+
                '<td '+(value.indicator==1?'style="color:red"':'')+'>'+value.stock+'</td>' +
                '<input type="hidden" class="indicator" value="'+value.indicator+'">'+
                '</tr>';
            $("#view_child_data").append(text);
        })
        $("#kt_modal_detail_voucher").modal("show");
    }
    $("#process_penukaran").click(function () {
        var po_kode = $(this).data("po_kode");
        var indicator = 0;
        var btn = $(this)
        $.each($("#view_child_data .indicator"),function (key,value) {
            indicator += $(value).val();
        })
        if(indicator>0){
            swal.fire({
                type: 'error',
                title: 'Peringatan',
                text:"Stock Anda tidak memadai",
                showConfirmButton: false,
                timer: 1500
            });
        }else{
            var link = $("#penukaran_link").length>0?base_url+'penukaran-retur-agen-save':base_url+'penukaran-po-save';
            $.ajax({
                url: link,
                type: "post",
                data: {"po_kode":po_kode},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        swal.fire({
                            type: 'success',
                            title: 'Berhasil melakukan penukaran',
                        }).then(function() {
                            window.location.reload()
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    })
})