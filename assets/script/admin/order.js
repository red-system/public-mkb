"use strict";

// Class Definition
var KTOrder = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var numSelected = 0
        var tempData = null
        var total = 0;
        var grand_total = 0;
        var potongan = 0;
        var tambahan = 0;
        var terbayar = 0;

        $("table").on('click','.parent',function () {
            var parent = $(this)
           $.each($("table .child"),function (i,val) {
               $(val).prop('checked', $(parent).prop('checked'));
           })
            change_data_print();
        });
        $("table").on('click','.child',function () {
            var total = $("table .child").length
            var count = 0;
            $.each($("table .child"),function (i,val) {
                if($(val).prop('checked')==true){
                    count ++;
                }
            })
            if(count == total){
                $("table .parent").prop('checked',$(this).prop('checked'));
            } else {
                $("table .parent").prop('checked',false);
            }
            change_data_print()
        })
        function change_data_print() {
           var url = $('.child:checked').map(function () {
                return $(this).val()
            }).get().join('-');
           numSelected = url == '' ? 0 : 1;
           $("#print-button").data('url',$("#print-button").data('static')+url);
            $("#cancel-item-btn").data('item',url);
        }
        $("#cancel-item-btn").click(function () {
            var id = $(this).data('item');
            $.ajax({
                url:base_url+"order-resto-pos-cancel",
                data:{id:id},
                type:'post',
                success: function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        Swal.fire({
                            type: 'success',
                            title: data.message,
                        }).then(function() {
                           window.location.reload()
                        });
                    }
                },
                error: function(request) {
                }
            })
        })
        $("#print-button").click(function () {
            if(numSelected>0){
                var url = $(this).data('url')
                window.open(url,'__blank');
            }
        })
        $(".order-process-btn").click(function () {
            var penjualan_id = $("#penjualan_id").val();
            process(penjualan_id)
        })
        $("#child_data_ajax").on('click','.order-process-btn',function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            process(object.penjualan_id);
        })
        function process(penjualan_id) {
            tempData = null
            $.ajax({
                url: base_url+"order-resto-pos/get-item",
                type: "post",
                data:{'id':penjualan_id},
                success: function (response) {
                  tempData = JSON.parse(response);
                  $("#tambahan-bayar").val(KTUtil.numberString(tempData.tambahan));
                    $("#potongan-bayar").val(KTUtil.numberString(tempData.potongan));
                    $("#total-bayar").html(KTUtil.numberString(tempData.total));
                    $("#grand-bayar").html(KTUtil.numberString(tempData.grand_total));
                    tambahan = tempData.tambahan
                    potongan = tempData.potongan
                    total = tempData.total
                    grand_total = tempData.grand_total
                    terbayar = tempData.terbayar
                    $("#input_penjualan_id").val(penjualan_id)
                },
                error: function(request) {
                }
            });
            $("#kt_modal_payment").modal('show');
        }
        function get_total() {
            var tempGrand = (parseInt(total) - parseInt(potongan)) + parseInt(tambahan)
            grand_total = tempGrand
            $("#grand-bayar").html(KTUtil.numberString(grand_total))
            transaksi_json()
        }
        function transaksi_json(){
            var trans = {total_item:total,potongan:potongan,tambahan:tambahan,grand_total:grand_total,terbayar:terbayar,item:{}}
            $("#transaksi").val(JSON.stringify(trans))
            console.log( $("#transaksi").val())
            console.log(190)
        }
        $("#potongan-bayar").keyup(function () {
            potongan = parseInt(intVal($(this).val()))
            get_total()
        })
        $("#tambahan-bayar").keyup(function () {
            tambahan = parseInt(intVal($(this).val()))
            get_total()
        })
        $("#tambahan-bayar").keyup(function () {
            tambahan = parseInt(intVal($(this).val()))
            get_total()
        })
        $("#terbayar").keyup(function () {
            terbayar = parseInt(intVal($(this).val()))
            get_total()
        })
        $("#tipe_pembayaran").change(function(){
            $("#input_tipe_pembayaran").val($(this).val());
            $("#input_jenis_pembayaran").val($(this).find(':selected').data('jenis'));
            if($(this).find(':selected').data('kembalian') == 1){
                $("#terbayar").removeClass("read");
                $("#terbayar").addClass("input-numeral");
                $("#terbayar").val(0);
            } else {
                $("#terbayar").val(KTUtil.numberString(grand_total));
                terbayar = grand_total;
                get_total()
                $("#terbayar").addClass("read");
                $("#terbayar").removeClass("input-numeral");
            }
            if($(this).find(':selected').data('jenis') == "kredit"){
                $("#kredit_container").css('display','block');
                $("#terbayar_container").css('display','none');

            } else {
                $("#kredit_container").css('display','none');
                $("#terbayar_container").css('display','flex');
            }
            if($(this).find(':selected').data('additional') == 1){
                $(".additional").removeClass("read");
                $("#additional_container").css('display','block');
            } else {
                console.log(321)
                $(".additional").addClass("read");
                $(".additional").val("");
                $("#additional_container").css('display','none');
            }
        })
        $(".numeral-display").keyup(function () {
            var name = $(this).attr('name');
            $(".numeral-input[name="+name+"]").val(intVal($(this).val()))
        })
        $('#tenggat_pelunasan').change(function(){
            $('#input_tenggat_pelunasan').val($(this).val());
        })
        $('#item-container').on('keydown','.readonly',function(e){
            e.preventDefault();
        });
        $(".readonly").keydown(function(e){
            if($("#tipe_pembayaran").find(":selected").data("kembalian")==0){
                e.preventDefault()
            }

        })
        $('.additional').change(function(){
            var name = $(this).attr("name");
            var value = $(this).val();
            $("input[name="+name+"]").val(value)
        })
        $("#save-payment").click(function(){
            var btn = $(this);
            var form = $("#payment-form");
            var enough = true;
            if($("#terbayar").prop("disabled") == false){
                if(intVal($("#terbayar").val()) < grand_total){
                    enough = false;
                }
            }
            if(!enough && ($("#tipe_pembayaran").find(':selected').data('jenis') != "kredit")){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Uang terbayar kurang",
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            if($("#tipe_pembayaran").find(':selected').data('jenis') == "kredit" && $("#tenggat_pelunasan").val() == ""){
                swal.fire({
                    type: 'error',
                    title: 'Peringatan',
                    text:"Tenggat pembayaran tidak boleh kosong",
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;
            }
            form.validate({});
            if (!form.valid()) {
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        console.log(data)
                        $("#kt_modal_payment").modal("hide");
                        if($("#tipe_pembayaran").find(":selected").data("kembalian")==1){
                            Swal.fire({
                                type: 'success',
                                title: 'Rp. '+data.kembalian,
                                text: 'sisa kembalian'
                            }).then(function() {
                                form.clearForm();
                                form.validate().resetForm();
                                var url = data.url;
                                window.open(url,"_blank");
                                window.location.href = base_url+"order-resto-pos";
                            });
                        } else {
                            Swal.fire({
                                type: 'success',
                                title: 'Transaksi Berhasil',
                            }).then(function() {
                                form.clearForm();
                                form.validate().resetForm();
                                var url = data.url;
                                window.open(url,"_blank");
                                clearForm();
                            });
                        }
                    } else {
                        swal.fire({
                            type: 'error',
                            title: 'Peringatan',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })
        $(".modal").on("show.bs.modal",function () {
            $(".pos-floating-button").css('display','none')
            console.log(123)
        })
        $(".modal").on("hidden.bs.modal",function () {
            $(".pos-floating-button").css('display','block')
        })
       
    }

    var intVal = function(i) {
        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
    };

    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTOrder.init();
});
document.addEventListener('DOMContentLoaded', () => {
    if($("#detail-page").length>0){
        $('.input-numeral').toArray().forEach(function(field){
            new Cleave(field, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalScale: 4
            })

        });
        $('.input-decimal').toArray().forEach(function(field){
            new Cleave(field, {
                numeral: true,
                numeralDecimalScale: 4
            })

        });
    }

});