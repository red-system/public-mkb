$(document).ready(function () {
   var base_url = $("#base_url").val();
   $("#child_data_ajax").on('click','.approve-btn',function () {
       var data = JSON.parse($(this).siblings('textarea').val());
       var claim_reward_id = data.claim_reward_id;
      $.ajax({
          type:'post',
          url:base_url+'management-reward/approve',
          data:{'claim_reward_id':claim_reward_id},
          success:function (response) {
            var data = JSON.parse(response);
              if(data.success){
                  swal.fire({
                      type: 'success',
                      title: 'Berhasil',
                      text: data.message
                  }).then(function () {
                      window.location.reload();
                  });
              }else {
                  swal.fire({
                      type: 'error',
                      title: 'Gagal',
                      text: data.message
                  });
              }
          }
      });
   });
});