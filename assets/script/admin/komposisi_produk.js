"use strict";

// Class Definition
var KTKomposisi = function() {
    var general = function(){
        
        var base_url = $("#base_url").val();
        
        $('.input-numeral').keyup(function(){
            if($(this).val()==""){
                $(this).val(0);
            }
        })
        
        
        $('#child_data_ajax').on('click','.view-btn',function(){ 
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.produk_recipe_id;
            get_detail(id)
        });

        function get_detail(id,type = 0){
            $.ajax({
                type: "POST",
                url: base_url+'komposisi-produk-bahan/detail',
                data:{'produk_recipe_id':id},
                cache: false,
                success: function(response){

                    console.log(response);

                    parseDetail(response);
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function parseDetail(response){
            var object = JSON.parse(response);
            $.each(object.komposisi,function(key,value){
                $('#kt_modal_detail_komposisi label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_komposisi [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_komposisi .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_komposisi .img-preview').attr("src",base_url+value);
                }
            })
            $("#view_child_data").html("");
            $.each(object.komposisi.item,function(key,value){
                // var text = '<tr>'+
                // '<td>'+value.produk_nama+'</td>'+
                // '<td>'+value.takaran+'</td>'+
                // '<td><'+value.harga_pokok+'/td>';
                var text = '<tr>';
                var a = 0;
                $.each(value.item_bahan,function(key2,value2){
                    if(a == 0){
                        text +='<td>'+value2.bahan_nama+'</td><td>'+value2.takaran+' '+value2.satuan_nama+'</td><td>'+value2.harga_pokok+'</td></tr>';
                        a = 1;
                    } else {
                        text +='<tr><td>'+value2.bahan_nama+'</td><td>'+value2.takaran+' '+value2.satuan_nama+'</td><td>'+value2.harga_pokok+'</td></tr>';
                    }
                });
                $("#view_child_data").append(text);
            })
            $("#kt_modal_detail_komposisi").modal("show");            
        }        
        
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTKomposisi.init();
});
document.addEventListener('DOMContentLoaded', () => {
	$('.input-numeral').toArray().forEach(function(field){
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		})

	});
	$('.input-decimal').toArray().forEach(function(field){
		new Cleave(field, {
			numeral: true,
			numeralDecimalScale: 4
		})

	});
});
