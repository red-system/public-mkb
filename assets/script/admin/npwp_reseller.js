$(document).ready(function () {
    var base_url = $("#base_url").val();
    var npwp_ = "";
    var panel = "add";
    $("#child_data_ajax").on("click", '.accept-btn', function () {
        var data = $(this).siblings('textarea').val()
        var object = JSON.parse(data);
        npwp_ = object.npwp_id;
        $.each(object,function(key,value){
            $('#kt_modal_accept label[name="' + key + '"]').html(value);
            $('#kt_modal_accept [name="' + key + '"]').val(value);
            var field = $('#kt_modal_accept .img-preview').data("field");
            if($("#"+key)[0]!==undefined){
                $('#kt_modal_accept #'+key).attr("src",value);
            }

        })
        $("#kt_modal_accept").modal("show");
    });
    $("#child_data_ajax").on('click', '.refuse-btn', function () {
        var object = $(this).siblings('textarea').val()
        var data = JSON.parse(object);

        $("#reseller_id_refuse").val(data.npwp_id)
        $("#kt_modal_refuse").modal("show");
    })
    $(".case").click(function () {
        var case_ = $(this).data('case');

        $.ajax({
            url: base_url + 'npwp-reseller/approved',
            data: {'npwp_id': npwp_, 'case': case_},
            type: 'post',
            success: function (response) {
                var result = JSON.parse(response);
                if (result.success) {
                    swal.fire({
                        type: 'success',
                        title: 'Berhasil',
                        text: 'Berhasil menyimpan data ',

                    }).then((data) => {
                        window.location.reload();
                    })
                }
            }
        })
    })
    $("#kt_submit_approve").click(function (e) {
        e.preventDefault();

        var btn = $(this);
        var form = $("#kt_form_approve_npwp");

        form.validate({
            rules: {
                password: {
                    required: true,
                    minlength: 5,
                },
                re_password: {
                    required: true,
                    minlength: 5,
                    equalTo: '#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function (response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload();
                    $("input[type=number]").val(0);

                } else {
                    swal.fire({
                        type: 'error',
                        text: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#kt_refuse_submit_btn").click(function (e) {
        e.preventDefault();

        var btn = $(this);
        var form = $("#kt_form_refuse");

        form.validate({
            rules: {
                password: {
                    required: true,
                    minlength: 5,
                },
                re_password: {
                    required: true,
                    minlength: 5,
                    equalTo: '#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function (response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    window.location.reload();
                    $("input[type=number]").val(0);

                } else {
                    swal.fire({
                        type: 'error',
                        text: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    var url_reseller = $("#list_reseller").val();
    var reseller_table = $("#reseller-table").DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        ordering: false,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pageLength: 10,
        ajax: url_reseller,
        columns: [{data: 'nama'}, {data: 'type'}, {data: 'aksi'}],
        columnDefs: [{
            targets: -1,
            responsivePriority: 1,
            title: 'Actions',
            orderable: false,
            render: function (data, type, full, meta) {
                var temp = '<div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">'
                    + '<textarea style="display:none">' + JSON.stringify(full) + '</textarea>'
                    + '<button class="btn btn-success btn-icon btn-icon-sm chose-btn btn-select-reseller" title="Pilih" >'
                    + '<i class="fa fa-calendar-check"></i>&nbsp; Pilih'
                    + '</button>';
                temp += '</div>'
                return temp;
            }
        }]

    });
    $(".reseller-search").click(function () {
        var modal_ = $(this).data("modal");
        panel = modal_;
        $("#kt_modal_reseller").modal("show");
    })
    $('#kt_modal_reseller').on('hidden.bs.modal', function () {
        $(this).css('z-index', 1050);
    });
    $('#kt_modal_reseller').on('show.bs.modal', function () {
        $(this).css('z-index', 2050);
    });
    $("#customFile").change(function () {
        readURL(this);
    })
    $(".img-input").change(function () {
        var display = $(this).data('display');
        var name = $(this).attr("name");
        upload_image(this, name)
        readURL(this, display);
    })

    function readURL(input, display) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#' + display).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function upload_image(input, name) {

        var base_url = $("#base_url").val();
        var data = new FormData();
        jQuery.each(jQuery('[name=' + name + ']')[0].files, function (i, file) {
            data.append(name, file);
        });
        $.ajax({
            url: base_url + "profile/upload-npwp",
            type: "post",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                $('.error-message').remove();
            },
            success: function (response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                console.log(data.url)
                $('[name=input_' + name + ']').val(data.url)
                $('[name=' + name + ']').siblings('label').html(data.url);

            }, error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    modal_edit_open();
    btn_add();
    btn_edit();
    select_reseller();
    penghasilan_lain();
});

function btn_add() {
    $('#kt_modal_add').on('click', '.reseller-search', function () {
        var data = $(this).data('modal');
        $('#kt_modal_reseller').attr('data-modal-target', data);
    })
}

function btn_edit() {
    $('#kt_modal_edit').on('click', '.reseller-search', function () {
        var data = $(this).data('modal');
        $('#kt_modal_reseller').attr('data-modal-target', data);
    })
}
$("#select-case").on('change',function () {
    var value = $(this).val()
    if(value==1){
        $("#input_ptkp").val(0)
    }else{
        $("#input_ptkp").val($("#ptkp_lbl").val())
    }
})
function select_reseller() {
    $('#kt_modal_reseller .table-reseller').on('click', '.btn-select-reseller', function () {
        var target = $('#kt_modal_reseller').attr('data-modal-target');
        var data = $(this).siblings('textarea').val();
        data = JSON.parse(data);

        $('#'+target+'_reseller_id').val(data['reseller_id']);
        $('#'+target+'_reseller_nama').val(data['nama']);

        $('#kt_modal_reseller').modal('hide');
    })
}

function modal_edit_open() {
    $('.table-approved').on('click', '.edit-btn', function () {
        var data_foto = $('#kt_modal_edit [name="data_foto"]').val();
        var link_foto = $('#kt_modal_edit [name="link_foto"]').val();
        var input_penghasilan = $('#kt_modal_edit [name="input_penghasilan_lain_base"]').val();
        input_penghasilan = parseInt(input_penghasilan);
        $('#kt_modal_edit [name="input_foto_npwp"]').val(data_foto);
        if (!data_foto){
            data_foto = 'no_image.png';
            link_foto = $('#kt_modal_edit [name="link_base"]').val();
        }

        $('#kt_modal_edit #displayImg3').attr('src', link_foto+data_foto);
        $('#kt_modal_edit #1').val(1);
        $('#kt_modal_edit #3').val(3);
        if (input_penghasilan != 0){
            $('#kt_modal_edit [name="penghasilan_lain"]').prop('checked', true);
            var keterangan = $('#kt_modal_edit [name="input_keterangan_lain_base"]').val();
            $('#kt_modal_edit #penghasilan_text').show();
            $('#kt_modal_edit [name="keterangan_lain"]').prop('required', true).val(keterangan);
            $('#kt_modal_edit #penghasilan_val').val(1);
        }else {
            $('#kt_modal_edit [name="penghasilan_lain"]').prop('checked', false);
            $('#kt_modal_edit #penghasilan_text').hide();
            $('#kt_modal_edit [name="keterangan_lain"]').prop('required', false).val('');
            $('#kt_modal_edit #penghasilan_val').val(0);
        }
    })
}

function penghasilan_lain() {
    $('#kt_modal_add [name="penghasilan_lain"]').on('change', function () {
        if ($(this).is(':checked')){
            $('#kt_modal_add #penghasilan_text').show();
            $('#kt_modal_add [name="keterangan_lain"]').prop('required', true).val('');
            $('#kt_modal_add #penghasilan_val').val(1);
        }else {
            $('#kt_modal_add #penghasilan_text').hide();
            $('#kt_modal_add [name="keterangan_lain"]').prop('required', false).val('');
            $('#kt_modal_add #penghasilan_val').val(0);
        }
    });

    $('#kt_modal_edit [name="penghasilan_lain"]').on('change', function () {
        if ($(this).is(':checked')){
            var keterangan = $('#kt_modal_edit [name="input_keterangan_lain_base"]').val();
            $('#kt_modal_edit #penghasilan_text').show();
            $('#kt_modal_edit [name="keterangan_lain"]').prop('required', true).val(keterangan);
            $('#kt_modal_edit #penghasilan_val').val(1);
        }else {
            $('#kt_modal_edit #penghasilan_text').hide();
            $('#kt_modal_edit [name="keterangan_lain"]').prop('required', false).val('');
            $('#kt_modal_edit #penghasilan_val').val(0);
        }
    });
};