$(document).ready(function () {
    var base_url = $("#base_url").val()
    reload()
   function reload() {
       $.ajax({
           url:base_url+"table/list",
           type:'get',
           success:function (response) {
                var data = JSON.parse(response);
                if(data.status){
                    parseData(data.list)
                }
           }
       })
   }
   function parseData(data) {
       $(".table-container").html('')
       $.each(data,function (i,val) {
           var item = '<div class="table-m">' +
               '<span class="dropdown">' +
                    '<a href="javascript:;" class="trans-table" data-toggle="dropdown" aria-expanded="false">' +
                        '<div class="inner-table">' +
                            val.table_name +
                        '</div>' +
                    '</a>' +
               '<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(11px, 0px, 0px);">' +
                    '<textarea style="display:none">'+JSON.stringify(val)+'</textarea>' +
                    '<a href="javascript:;" class="dropdown-item  btn-icon btn-icon-sm edit-btn" title="Edit"><i class="flaticon2-edit"></i>&nbsp; Edit </a>' +
                    '<a href="javascript:;" class="dropdown-item  btn-icon btn-icon-sm delete-btn" data-route="'+base_url+'table/delete/" title="Delete"><i class="flaticon2-trash"></i>&nbsp; Hapus</a>' +
               '</div>' +
               '</span>' +
               '</div>';
           $(".table-container").append(item);
       });
   }
    $(".table-container").on('click','.edit-btn',function(){

        var json = $(this).siblings('textarea').val();

        var object = JSON.parse(json);
        $.each(object,function(key,value){
            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#kt_modal_edit .img-preview').attr('src', base_url+value);
            } else if (type == 'radio') {
                $('#kt_modal_edit [name="' + key + '"][value=' + value + ']').prop("checked", true);

            }
            else if (type == 'checkbox') {
                if(value == true || value==1){
                    $('#kt_modal_edit [name="' + key + '"]').prop("checked", true);
                } else {
                    $('#kt_modal_edit [name="' + key + '"]').prop("checked", false);
                }
            }else {
                if(key !="password"){
                    $('#kt_modal_edit [name="' + key + '"]').val(value);
                    $('#kt_modal_edit [name="' + key + '"]').trigger('change');
                    $(".input-numeral").trigger('change');
                }

            }
        })
        if (object.edit_url != undefined) {

        } else {
            $("#kt_modal_edit").modal("show");
        }
    })
    $(".table-container").on('click','.delete-btn',function(){
        var json = $(this).siblings('textarea').val();
        var object = JSON.parse(json);
        $.each(object,function(key,value){
            $('#kt_modal_delete [name="' + key + '"]').val(value);
        })
        var route = object.delete_url;
        swal.fire({
            title: "Perhatian ...",
            text: "Yakin hapus data ini ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#0abb87",
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {

                $.ajax({
                    url: route,
                    type: "delete",
                    data:{"id":object.row_id},
                    beforeSend: function () {
                        $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                    },
                    success: function (response) {
                        $('.wrapper-loading').fadeOut().addClass('hidden');
                        var data = jQuery.parseJSON(response);
                        if (data.success){
                            reload()
                        }else {
                            swal.fire({
                                type: 'error',
                                text:data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    },
                    error: function(request) {
                        $('.wrapper-loading').fadeOut().addClass('hidden');
                        swal.fire({
                            title: "Ada yang Salah",
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
    })
    $('#kt_add_submit').click(function(e) {
        e.preventDefault();
        var btn = $(this);
        var form = $(this).closest('form');
        form.validate({
            rules: {
                password:{
                    required: true,
                    minlength:5,
                },
                re_password:{
                    required: true,
                    minlength:5,
                    equalTo:'#password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    reload()
                    $(".modal").modal("hide");
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $('#kt_edit_submit').click(function(e) {
        e.preventDefault();
        var btn = $(this);
        var form = $(this).closest('form');
        form.validate({
            rules: {
                password:{
                    minlength:5,
                },
                re_password:{
                    minlength:5,
                    equalTo:'#edit_password'
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        form.ajaxSubmit({
            url: form.attr("action"),
            beforeSend: function () {
                $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            },
            success: function(response, status, xhr, $form) {
                $('.wrapper-loading').fadeOut().addClass('hidden');
                var data = jQuery.parseJSON(response);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                if (data.success) {
                    form.clearForm();
                    form.validate().resetForm();
                    $(".modal").modal("hide");
                    $("input[type=number]").val(0);
                    reload()
                } else {
                    swal.fire({
                        type: 'error',
                        text:data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }
        });
    });
});