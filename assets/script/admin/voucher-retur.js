$(document).ready(function () {
    var base_url = $("#base_url").val();
    $('#child_data_ajax').on('click','.view-btn',function(e){
        var json = $(this).siblings('textarea').val();
        var object = JSON.parse(json);
        var id = object.retur_agen_id;
        console.log(object)
        get_detail(id)
    });
    function get_detail(id){
        $.ajax({
            type: "POST",
            url: base_url+'voucher-retur/detail',
            data:{'retur_agen_id':id},
            cache: false,
            success: function(response){
                parseDetail(response);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
    function parseDetail(response){
        var object = JSON.parse(response);
        $.each(object,function(key,value){
            $('#kt_modal_detail_voucher label[name="' + key + '"]').html(value);
            $('#kt_modal_detail_voucher [name="' + key + '"]').val(value);
            var field = $('#kt_modal_detail_voucher .img-preview').data("field");
            if (field == key){
                $('#kt_modal_detail_voucher .img-preview').attr("src",base_url+value);
            }
        })

        $("#view_child_data").html("");
        $.each(object.item,function(key,value){
            var text = '<tr>'+
                '<td >'+value.produk_nama+'</td>'+
                '<td >'+value.jumlah+'</td>'+'</tr>';
            $("#view_child_data").append(text);
        })
        $("#kt_modal_detail_voucher").modal("show");
    }

})