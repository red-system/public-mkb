"use strict";

// Class Definition
var KTQuality = function() {
    var base_url = $("#base_url").val();
    var type = $("#type").val();
    function intVal(i) {
        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
    };
    var rugi = function(){

        $(".item-search").click(function () {
            $("#kt_modal_search_stock").modal('show');
        })
        $("#child_data_ajax").on('click','.chose-btn',function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#stock_produk_seri").val(object.stock_produk_seri)
            $("#jumlah_stock").html(KTUtil.numberString(object.stock_produk_qty))
            $("#stock_produk_lokasi_id").val(object.stock_produk_id)
            $("#hpp_stock").html(KTUtil.numberString(object.hpp))
            $("#input_hpp").val(object.hpp)
            $("#kt_modal_search_stock").modal('hide');
            $("#jumlah_kerugian").data('max',object.stock_produk_qty);
            $("#jumlah_kerugian").val(0);
            total();
        })
        $("#jumlah_kerugian").keyup(function () {
            var val = intVal($(this).val()),
                max = $(this).data('max')
            if(val>max){
                $(this).val(KTUtil.numberString(max))
                swal.fire({
                    title:'Error',
                    type:"error",
                    text:"Jumlah Melebihi Stock",
                    showConfirmButton:false,
                    timer:1000
                })
            }
            total();
        })
        function total() {
            var hpp = $("#hpp_stock").html();
            hpp =  typeof hpp === 'string' ? hpp.replace('Rp. ','') : hpp;
            hpp = intVal(hpp)
            $("#input_hpp").val(hpp)
            var jumlah = intVal($("#jumlah_kerugian").val())
            var total = hpp *jumlah
            $("#total_kerugian").html('Rp. '+KTUtil.numberString(total))
            $("#input_total_kerugian").val(total);
        }
    }
    var retur = function(){

        $(".po-search").click(function () {
            $("#kt_modal_search_po").modal('show');
        })
        $("#child_data_ajax").on('click','.chose-btn',function () {
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#po_produk_no").val(object.po_produk_no)
            $("[name=input_po_id]").val(object.po_produk_id)
            $("#suplier").html(object.suplier_nama)
            $("#jumlah_item_po").html(KTUtil.numberString(object.jumlah))
            $("#hpp_stock").html(KTUtil.numberString(object.harga))
            $("#input_hpp").val(object.harga)
            $("#kt_modal_search_po").modal('hide');
            $("#jumlah_retur").data('max2',object.jumlah);
            $("#jumlah_retur").val(0);
            total();
        })
        $("#jumlah_retur").keyup(function () {
            var val = intVal($(this).val()),
                maxPO = $(this).data('max1'),
                maxLok = $(this).data('max2'),
                max = maxLok < maxPO ? maxLok : maxPO
            if(val>maxPO||val>maxLok){
                $(this).val(KTUtil.numberString(max))
                swal.fire({
                    title:'Error',
                    type:"error",
                    text:"Jumlah Melebihi Jumlah pembelian atau jumlah stok saat ini",
                    showConfirmButton:false,
                    timer:1000
                })
            }
            total();
        })
        function total() {
            var hpp = $("#hpp_stock").html();
            hpp =  typeof hpp === 'string' ? hpp.replace('Rp. ','') : hpp;
            hpp = intVal(hpp)
            $("#input_hpp").val(hpp)
            var jumlah = intVal($("#jumlah_retur").val())
            var total = hpp *jumlah
            console.log(jumlah)
            console.log(hpp)
            $("#total_retur").html('Rp. '+KTUtil.numberString(total))
            $("#input_total_retur").val(total);
        }
    }
    var send = function () {
        $(".form-send").submit(function (e) {
            e.preventDefault();
            var self = $(this)
            var alert_show = self.data('alert-show');
            var alert_field_message = self.data('alert-field-message');
            if (!self.valid()) {
                return;
            }
            if(alert_show){
                swal.fire({
                    title: 'Apakah anda yakin?',
                    text: alert_field_message,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText:'Ya',
                    cancelButtonText:'Tidak'
                }).then((result) => {
                    if (result.value) {
                        form_send(self);
                    }
                })
            } else {
                form_send(self)
            }

        })
        function form_send(self) {
            var action = self.attr('action');
            var method = self.attr('method');
            var redirect = self.data('redirect');

            var data = self.serialize()
            $.ajax({
                url:action,
                type:method,
                data: data,
                success: function (response) {
                    var data = JSON.parse(response)
                    if(data.success){
                        swal.fire({
                            text:data.message,
                            type:'success',
                        }).then((result) => {
                            if(result.value){
                                if(redirect!=undefined){
                                    window.location.href = redirect
                                }else{
                                    window.location.href = redirect
                                }
                            }
                        })
                    } else {
                        swal.fire({
                            title:'Error',
                            text:data.message,
                            type:'error',
                            showConfirmButton: false,
                            timer:1000
                        })
                    }
                },
                error:function (response) {

                }
            })
        }

    }
    // Public Functions
    return {
        // public functions
        init: function() {

            if(type=='rugi'){
                rugi()

            }else {
                retur()
            }
            send()
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTQuality.init();
});