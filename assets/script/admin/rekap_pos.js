"use strict";

// Class Definition
var KTRekap = function() {
    var general = function(){
        $("#child_data_ajax").on('click','.view-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            getDetail(object.penjualan_id);
        })
        $("#child_data_ajax").on('click','.print-btn',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, "_blank");
        })


    }
    function getDetail(id){
            $.ajax({
            type: "POST",
            url: $("#detail_url").val()+"/"+id,
            cache: false,
            success: function(response){
               var res = jQuery.parseJSON(response);
               parseDetail(res.data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });        
    }
    function parseDetail(data){
        $("#item_child").html('');
        $.each(data,function(i,value){
            var temp = "<tr>"+
                        "<td>"+value.produk_kode+"</td>"+
                        "<td>"+value.produk_nama+"</td>"+
                        "<td>"+value.qty+"</td>"+
                        "<td>"+value.satuan_nama+"</td>"+
                        "<td>"+value.harga+"</td>"+
                        "<td>"+value.sub_total+"</td>"+
                       +"</tr>";
            $("#item_child").append(temp);
        })
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTRekap.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
          numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    })
   });
});