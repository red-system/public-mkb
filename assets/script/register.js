"use strict";

// Class definition
var KTWizard1 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    var base_url = $("#base_url").val();
    var city_url = base_url + 'registrasi-city';
    var subdistrict_url = base_url + 'registrasi-subdistrict';
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v1', {
            startStep: 1
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function (wizard) {
            setTimeout(function () {
                KTUtil.scrollTop();
            }, 500);
        });

        $('.province').change(function () {
            var province_ = $(this).val();
            var prefix = $(this).attr('prefix');
            var city_ = prefix + 'city_id';
            var subdistrict_ = prefix + 'subdistrict_id';
            if (province_ !== '') {

                getCity(province_, city_, subdistrict_)
            }

        })
        $('.city').change(function () {
            var city_ = $(this).val();
            var prefix = $(this).attr('prefix');
            var subdistrict_ = prefix + 'subdistrict_id';
            if (city_ !== '') {
                getSubdistrict(city_, subdistrict_)
            }
        })

        function getCity(province_, city_, subdistrict_) {
            $.ajax({
                url: city_url,
                type: 'post',
                data: {'province_id': province_},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var result = JSON.parse(response);
                    $("#" + city_).html('<option value="">Pilih Kabupaten/Kota</option>');
                    $.each(result.data, function (i, val) {
                        $("#" + city_).append('<option value="' + val.city_id + '">' + val.city_name + '</option>')
                    })

                    $("#" + subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                }
            })
        }

        function getSubdistrict(city_, subdistrict_) {
            $.ajax({
                url: subdistrict_url,
                type: 'post',
                data: {'city_id': city_},
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var result = JSON.parse(response);
                    $("#" + subdistrict_).html('<option value="">Pilih Kecamatan</option>');
                    $.each(result.data, function (i, val) {
                        $("#" + subdistrict_).append('<option value="' + val.subdistrict_id + '">' + val.subdistrict_name + '</option>')
                    })

                }
            })
        }

        $(".img-input").change(function () {
            var display = $(this).data('display');
            var name = $(this).attr("name");
            upload_image(this, name)
            // readURL(this, display);
            $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
            $('.error-message').remove();
        })

        function readURL(input, display) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#' + display).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function upload_image(input, name) {
            var test = $('[name="' + name + '"]')[0].files[0];
            console.log('image0');
            console.log(test);
            console.log(test.size);
            var label = $('[name="' + name + '"]').attr('data-display');
            var bagi = 2;

            if (parseInt(test.size) >= 1700000){
                bagi = 8;
            }else if(1700000 > parseInt(test.size) > 1000000){
                bagi = 4;
            }
            console.log(bagi);
            console.log(parseInt(bagi));
            var fileReader = new FileReader();
            var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

            fileReader.onload = function (event) {
                var image = new Image();

                image.onload = function () {
                    // document.getElementById("original-Img").src=image.src;
                    var canvas = document.createElement("canvas");
                    var img_h = image.height;
                    var img_w = image.width;

                    if (bagi > 2){
                        img_h = image.height/bagi;
                        img_w = image.width/bagi;
                    }
                    var context = canvas.getContext("2d");
                    canvas.width=img_w;
                    canvas.height=img_h;
                    context.drawImage(image,
                        0,
                        0,
                        image.width,
                        image.height,
                        0,
                        0,
                        canvas.width,
                        canvas.height
                    );

                    document.getElementById(label).src = canvas.toDataURL();


                    new_upload(name, canvas.toDataURL());
                }
                image.src = event.target.result;

            };

            var uploadImage = $('[name=' + name + ']');

            //check and retuns the length of uploded file.
            if (uploadImage[0].files.length === 0) {
                return;
            }

            //Is Used for validate a valid file.
            var uploadFile = $('[name=' + name + ']')[0].files[0];
            fileReader.readAsDataURL(uploadFile);


        }

        function new_upload(name, url) {
            console.log('image_resized');
            console.log(url);
            var blobBin = atob(url.split(',')[1]);
            var array = [];
            for(var i = 0; i < blobBin.length; i++) {
                array.push(blobBin.charCodeAt(i));
            }

            // console.log('file')
            // console.log(file)
            var data = new FormData();
            jQuery.each(jQuery('[name=' + name + ']')[0].files, function (i, file) {

                var file_new=new Blob([new Uint8Array(array)], {type: 'image/png'});

                data.append(name, file_new);
                data.append(name+'_base', file);
            });
            console.log(data);
            $.ajax({
                url: base_url + "/registrasi-upload",
                type: "post",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                    $('.error-message').remove();
                },
                success: function (response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    console.log(data.url)
                    $('[name=input_' + name + ']').val(data.url)
                    $('[name=' + name + ']').siblings('label').html(data.url);

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }

    }


    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                //= Step 1
                address1: {
                    required: true
                },
                postcode: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                country: {
                    required: true
                },

                //= Step 2
                package: {
                    required: true
                },
                weight: {
                    required: true
                },
                width: {
                    required: true
                },
                height: {
                    required: true
                },
                length: {
                    required: true
                },

                //= Step 3
                delivery: {
                    required: true
                },
                packaging: {
                    required: true
                },
                preferreddelivery: {
                    required: true
                },

                //= Step 4
                locaddress1: {
                    required: true
                },
                locpostcode: {
                    required: true
                },
                loccity: {
                    required: true
                },
                locstate: {
                    required: true
                },
                loccountry: {
                    required: true
                },
            },

            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                // swal({
                //     "title": "",
                //     "text": "There are some errors in your submission. Please correct them.",
                //     "type": "error",
                //     "confirmButtonClass": "btn btn-secondary"
                // });
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    }
    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');
        btn.on('click', function (e) {
            e.preventDefault();
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 5,
                    },
                    re_password: {
                        required: true,
                        minlength: 5,
                        equalTo: '#password'
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            if (!$('#checkbox').prop('checked')) {
                return;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: form.serialize(),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                    $('.error-message').remove();
                },
                success: function (response, status, xhr, $form) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    try {
                        var data = jQuery.parseJSON(response);
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        if (data.success) {
                            form.clearForm();
                            form.validate().resetForm();
                            swal.fire({
                                type: 'success',
                                text: data.message,
                            }).then(() => {
                                window.location.href = base_url + "registrasi-success";
                            });
                            $("input[type=number]").val(0);
                            if (btn.data('page') != undefined && btn.data('page') == "transfer-stock") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Dalam proses tranfer',
                                    text: 'Menunggu konfirmasi penerimaan'
                                });
                            }
                        } else {
                            var message = "";
                            var totalText = "";
                            var title = "Fill form completely";
                            var page = 4;
                            $.each(data.errors, function (i, val) {
                                $('[name=' + i + ']').parent().append(val)
                                if (message == "" && val != "") {
                                    message = $('[name=' + i + ']').siblings('.error-message').html();
                                    var container = $('[name=' + i + ']').closest('.kt-wizard-v1__content');
                                    page = container.data('num');
                                }
                                if (val != "") {
                                    totalText = val;
                                }
                            })
                            if (totalText == "") {
                                message = "Ops There's something wrong, please check your connection first";
                                title = "Bad Connection";
                            }
                            swal.fire({
                                type: 'error',
                                title: title,
                                text: message,
                            }).then(function (e) {
                                if (title == "Bad Connection") {

                                } else {
                                    wizard.goTo(page)
                                }
                            });

                        }
                    } catch (e) {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        swal.fire({
                            type: 'error',
                            title: 'Bad Connection',
                            text: "Ops There's something wrong, please check your connection first",
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
    }
    return {
        // public functions
        init: function () {
            wizardEl = KTUtil.get('kt_wizard_v1');
            formEl = $('#kt_add_staff_form');

            initWizard();
            initValidation();
            initSubmit();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizard1.init();
});
