"use strict";

// Class Definition
var KTStock = function() {
	var general = function(){
		var base_url = $("#base_url").val();
		$("#child_data_ajax").on('click','.barcode-btn',function(){
			var json = $(this).siblings('textarea').val();
			var object = JSON.parse(json);
			$("#barcode_seri").val(object.stock_produk_seri);
			$("#barcode_modal").modal("show");
		})
		$("#print_barcode").click(function () {
			var url = base_url+"stock-produk/print-barcode/"+$("#barcode_seri").val()+"/"+$("#jumlah_cetak").val();
			window.open(url, '_blank');
			$("#barcode_modal").modal("hide");
			$("#jumlah_cetak").val("0");
		})
	}
	// Public Functions
	return {
		// public functions
		init: function() {
			general();
		}
	};
}();

// Class Initialization
jQuery(document).ready(function() {
	KTStock.init();
	$('.input-numeral').toArray().forEach(function(field){
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		});
	});
});
