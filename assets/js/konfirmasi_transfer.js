"use strict";

// Class Definition
var KTKonfirm = function() {
    var general = function(){

        $('input[type=radio][name=status_history]').change(function() {
            if($(this).val()=="Diterima Sebagian"){
                $("#qty_terima_group").css("display","block");
                $("#qty_terima").prop("disabled",false);
                $("#qty_terima").val(1);
            } else {
                $("#qty_terima_group").css("display","none");
                $("#qty_terima").prop("disabled",true);
            }
        })
        $('.input-numeral').keyup(function(){
            if($(this).val()==""){
                $(this).val(0);
            }
        })
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTKonfirm.init();
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});