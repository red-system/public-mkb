"use strict";

// Class Definition
var KTUser = function() {
    var general = function(){
        $(".user-role").change(function(){
            if($(this).val() == "1"){
                $(".user_lokasi_container").css("display","none");
                $(".lokasi-select").prop('disabled', true);
            } else {
                $(".user_lokasi_container").css("display","block");
                $(".lokasi-select").prop('disabled', false);                
            }
        })

    }
    $(".modal").on('hidden.bs.modal', function () {
        $('#kt_modal_edit select').val($('#kt_modal_edit select option:first').val());
        $('#kt_modal_add select').val($('#kt_modal_edit select option:first').val());
        $(".user_lokasi_container").css("display","block");
        $(".lokasi-select").prop('disabled', false);         
    })
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTUser.init();
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
});
