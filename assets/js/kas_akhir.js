"use strict";

// Class Definition
var KTKas = function() {
    var type = $('#type-page').length>0?'list':'manipulate';
    var base_url = $('#base_url').val();
    var general = function () {
        $('.form-send').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            form_send(form);
        })
        function form_send(form) {
            var action = $(form).attr('action');
            var dataForm = form.serializeArray();
            var btn = $('#btn-save');
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            if (!form.valid()) {
                return;
            }
            $.ajax({
                type:'post',
                url : action,
                data: dataForm,
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    var responseData = JSON.parse(response);
                    if (responseData.success){
                        swal.fire({
                            type: 'success',
                            title: 'Pemeberitahuan',
                            text: responseData.message,
                        }).then(function() {
                            console.log(123);
                            window.location.reload();
                        });
                    }else{
                        swal.fire({
                            type: 'error',
                            text:responseData.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }

            });
        }
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTKas.init();
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalScale: 4
        })

    });
});