"use strict";

// Class Definition
var KTProduk = function() {
    var base_url = $("#base_url").val();
    var general = function(){
        var base_url = $("#base_url").val();
        $("#child_data_ajax").on('click','.view-btn',function(){

            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#kt_modal_detail_produk label[name="' + key + '"]').html(value);
                $('#kt_modal_detail_produk [name="' + key + '"]').val(value);
                var field = $('#kt_modal_detail_produk .img-preview').data("field");
                if (field == key){
                    $('#kt_modal_detail_produk .img-preview').attr("src",base_url+value);
                }
            })
            getdetail(object.produk_id);
            getdetail_item(object.produk_id)
            
        })
        function getdetail(id){
            $.ajax({
                type: "POST",
                url: base_url+'produk/harga/'+id,
                cache: false,
                success: function(response){
                    parseHarga(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function getdetail_item(id){
            $.ajax({
                type: "POST",
                url: base_url+'produk/item_produk/'+id,
                cache: false,
                success: function(response){
                    parseItem(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });            
        }
        function parseHarga(response){
            var object = JSON.parse(response);
            $("#view_child_data").html("");
            if(object.length > 0){
                $.each(object,function(i,value){
                    var text = "<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td>"+(value.minimal_pembelian)+"</td>"+
                                    "<td>"+(value.harga)+"</td>"+
                               "</tr>";
                    $("#view_child_data").append(text);
                })
            } else {
                $("#view_child_data").append('<tr><td colspan="3" style="text-align: center;">Tidak ada data</td></tr>');
            }
            $("#kt_modal_detail_produk").modal("show");
        }
        function parseItem(response){
            var object = JSON.parse(response);
            console.log(object);
            $("#view_child_data_item").html("");
            if(object.length > 0){
                $.each(object,function(i,value){
                    var text = "<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td width='60%'>"+(value.produk_nama)+"</td>"+
                                    "<td>"+(value.jumlah)+"</td>"+
                               "</tr>";
                    $('.item_produk_display').css('display','block');
                    $("#view_child_data_item").append(text);
                })
            } else {
                $('.item_produk_display').css('display','none');
                $("#view_child_data_item").append('<tr><td colspan="3" style="text-align: center;">Tidak ada data</td></tr>');
            }
            $("#kt_modal_detail_produk").modal("show");
        }
        $("#topperCheck").click(function () {
            var value = $(this).prop('checked');
            if(value){
                $("#topperContainer").css('display','block');
            } else {
                $("#topperContainer").css('display','none');
            }
            $("#topperContainer").val(0);
        })
        $("#child_data_ajax").on('click','.barcode-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $("#barcode_seri").val(object.produk_kode);
            $("#barcode_modal").modal("show");
        })
        $("#print_barcode").click(function () {
            var url = base_url+"stock-produk/print-barcode/"+$("#barcode_seri").val()+"/"+$("#jumlah_cetak").val();
            window.open(url, '_blank');
            $("#barcode_modal").modal("hide");
            $("#jumlah_cetak").val("0");
        })
    }
    var manipulate = function () {
        var add_size = {};
        var edit_size = {};
        var delete_size = [];
        var intVal = function(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        var no = $("#last_no").length > 1 ? $("#last_no").val() : 0;
        rewrite()
        rewrite_edible()
        $(".add-premium").click(function () {
            no++;
            var item = {size_id:null,harga_minimal:0,harga_maksimal:0}
            var index = get_no()
            var text = '<tr id="row-size-'+no+'" data-no="'+no+'">' +
                        '<td class="row-index">'+index+
                             '<input type="hidden" id="id_change_'+no+'" value="">'+
                         '</td>'+
                        '<td>' +
                            '<select class="form-control size-id" id="input-size-'+no+'"  data-no="'+no+'">' +
                                $("#option_size").html()+
                            '</select>' +
                        '</td>'+
                        '<td>' +
                            '<input type="text" class="form-control input-numeral harga-minimal" data-no="'+no+'" id="input-minimal-'+no+'"  value="0">'+
                        '</td>'+
                        '<td>' +
                            '<input type="text" class="form-control input-numeral harga-maksimal" data-no="'+no+'" id="input-maksimal-'+no+'"  value="0">'+
                        '</td>'+
                        '<td style="text-align: center">' +
                            '<button type="button" class="btn btn-danger delete-size" data-no="'+no+'" data-produk-size=""><i class="fa fa-trash"></i></button>'+
                        '</td>'+
                '</tr>';
            $(".size_table").append(text)
            new Cleave($("#input-minimal-"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            new Cleave($("#input-maksimal-"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            rewrite()
        })
        $(".add-edible").click(function () {
            no++;
            var item = {size_id:null,harga_minimal:0,harga_maksimal:0}
            var index = get_no_edible()
            var text = '<tr id="row-size-edible-'+no+'" data-no="'+no+'">' +
                '<td class="row-index">'+index+
                '<input type="hidden" id="id_change_edible_'+no+'" value="">'+
                '</td>'+
                '<td>' +
                '<select class="form-control size-id-edible" id="input-size-edible-'+no+'"  data-no="'+no+'">' +
                '<option value="">Tanpa Size</option>'+
                $("#option_size").html()+
                '</select>' +
                '</td>'+
                '<td>' +
                '<input type="text" class="form-control input-numeral harga-minimal-edible" data-no="'+no+'" id="input-minimal-edible-'+no+'"  value="0">'+
                '</td>'+
                '<td>' +
                '<input type="text" class="form-control input-numeral harga-maksimal-edible" data-no="'+no+'" id="input-maksimal-edible-'+no+'"  value="0">'+
                '</td>'+
                '<td style="text-align: center">' +
                '<button type="button" class="btn btn-danger delete-size-edible" data-no="'+no+'" data-produk-size=""><i class="fa fa-trash"></i></button>'+
                '</td>'+
                '</tr>';
            $(".size_table_edible").append(text)
            new Cleave($("#input-minimal-edible-"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            new Cleave($("#input-maksimal-edible-"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            rewrite_edible()
        })
        $(".size_table").on('keyup','.input-numeral',function () {
            $(this).val($(this).val()==''?0:$(this).val())
        })
        $(".size_table_edible").on('keyup','.input-numeral',function () {
            $(this).val($(this).val()==''?0:$(this).val())
        })
        $(".size_table").on('change','.size-id',function () {
            var index = $(this).data('no');
            rewrite()
        })
        $(".size_table_edible").on('change','.size-id-edible',function () {
            var index = $(this).data('no');
            rewrite_edible()
        })
        $(".size_table").on('keyup','.harga-minimal,.harga-maksimal',function () {
            var index = $(this).data('no');
            rewrite()
        })
        $(".size_table_edible").on('keyup','.harga-minimal-edible,.harga-maksimal-edible',function () {
            var index = $(this).data('no');
            rewrite_edible()
        })
        function get_no() {
            return  $("#size_table tr").length + 1
        }
        function get_no_edible() {
            return  $("#size_table tr").length + 1
        }
        $(".size_table").on('click','.delete-size',function () {
            var chose = $(this).data('no')
            $("#row-size-"+chose).remove()
            rewrite()
            re_arrange()
        })
        $(".size_table_edible").on('click','.delete-size-edible',function () {
            var chose = $(this).data('no')
            $("#row-size-edible-"+chose).remove()
            rewrite_edible()
            re_arrange_edible()
        })
        function rewrite() {
            var data = []
            $.each($(".size_table tr"),function (i,val) {
                var index = $(val).data('no')
                data.push({id_size:$("#input-size-"+index).val(),harga_minimal: intVal($("#input-minimal-"+index).val()),harga_maksimal: intVal($("#input-maksimal-"+index).val())})
            })
            $("[name=hiasan_premium]").val(JSON.stringify(data))
            console.log($("[name=hiasan_premium]").val())
        }
        function rewrite_edible() {
            var data = []
            $.each($(".size_table_edible tr"),function (i,val) {
                var index = $(val).data('no')
                console.log(intVal($("#input-minimal-edible-"+index).val()))
                data.push({id_size:$("#input-size-edible-"+index).val(),harga_minimal: intVal($("#input-minimal-edible-"+index).val()),harga_maksimal: intVal($("#input-maksimal-edible-"+index).val())})
            })
            $("[name=edible]").val(JSON.stringify(data))
        }
        function re_arrange() {
            $.each($('#size_table .row-index'),function (i,val) {
                $(val).html(i+1)
            })
        }
        function re_arrange_edible() {
            $.each($('#size_table_edible .row-index'),function (i,val) {
                $(val).html(i+1)
            })
        }
        $("#kt_add_submit").click(function(e){
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({});
            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        window.location.href = base_url+"produk";
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        })

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
            if($("#is_form").length>0){
                manipulate()
            }
        }
    };
}();
$('#child_data_ajax').on('click', '.edit-assembly-btn', function (e) {
    e.preventDefault();
    var json = $(this).siblings('textarea').val();
    var object = JSON.parse(json);
    var direct = $(this).data('direct');
    var check = $(this).data('check');
    var href = $(this).attr('href');
    console.log(direct)
    if (check != '' || check != 0) {
        swal.fire({
            title: "Perhatian",
            html: "Produk Ini Masih Memiliki Stock, Jika Anda Melakukan Pengeditan, Maka Akan Dilakukan Penghapusan Stock Saat Menekan Tombol Save. <br>Apakah anda yakin akan melanjutkan?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Tidak",
        }).then(function (e) {
            if (e.value) {
                window.location.href = direct;
            } else {

            }
        });
    } else {
        if (direct != "") {
            window.location.href = direct;
        }
    }
}); 

// Class Initialization
jQuery(document).ready(function() {
    KTProduk.init();
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalScale: 4
        })

    });
    $('.input-decimal').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralDecimalScale: 4
        })

    });
});