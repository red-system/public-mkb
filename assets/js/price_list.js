"use strict";

// Class Definition
var KTPrice = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var add_size = {};
        var edit_size = {};
        var delete_size = [];
        var no = $("#last_no").length > 1 ? $("#last_no").val() : 0;
        var intVal = function(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        $(".add-item").click(function () {
            var parent = $(this).closest('.modal').attr('id');
            no++;
            var item = {size_id:null,produk_size_id:null,harga_pokok:0,harga_jual:0}
            var index = get_no(parent)
            var text = '<tr id="row-size-'+no+'">' +
                '<td class="row-index">'+index+

                '</td>'+
                '<td>' +
                '<input type="hidden" class="id_change_'+no+'" value="">'+
                '<select class="form-control size-id" id="input-size-'+no+'"  data-no="'+no+'">' +
                $("#option_size").html()+
                '</select>' +
                '</td>'+
                '<td>' +
                '<input type="text" class="form-control input-numeral harga-pokok" data-no="'+no+'" id="input-pokok-'+no+'"  value="0">'+
                '</td>'+
                '<td>' +
                '<input type="text" class="form-control input-numeral harga-jual" data-no="'+no+'" id="input-jual-'+no+'"  value="0">'+
                '</td>'+
                '<td style="text-align: center">' +
                '<button type="button" class="btn btn-danger delete-size" data-no="'+no+'" data-produk-size=""><i class="fa fa-trash"></i></button>'+
                '</td>'+
                '</tr>';
            $("#"+parent+" .size_table").append(text)
            new Cleave($("#"+parent+" #input-pokok-"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            new Cleave($("#"+parent+" #input-jual-"+no), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
            add_size['row-'+no] = item
            add_size['row-'+no].size_id = $("#input-size-"+no).val()
            add_size['row-'+no].harga_pokok = intVal($("#input-pokok-"+no).val())
            add_size['row-'+no].harga_jual = intVal($("#input-jual-"+no).val())
            $("#"+parent+" .input_add_size").val(JSON.stringify(add_size))
        })
        function get_no(parent) {
            return  $("#"+parent+" .size_table tr").length + 1
        }
        $(".size_table").on('keyup','.input-numeral',function () {
            $(this).val($(this).val()==''?0:$(this).val())
        })
        $(".size_table").on('change','.size-id',function () {
            var index = $(this).data('no');
            var parent = $(this).closest('.modal').attr('id');
            edit_size_item(parent,index)
        })
        $(".size_table").on('keyup','.harga-pokok,.harga-jual',function () {
            var index = $(this).data('no');
            var parent = $(this).closest('.modal').attr('id');
            edit_size_item(parent,index)
        })
        function edit_size_item(parent,index) {
            var id_change = $("#"+parent+" .id_change_"+index).val()
            var item = {size_id:$("#input-size-"+index).val(),produk_size_id:null,harga_pokok:intVal($("#input-pokok-"+index).val()),harga_jual:intVal($("#input-jual-"+index).val())}
            if(id_change==''){
                add_size['row-'+index] = item
                $("#"+parent+" .input_add_size").val(JSON.stringify(add_size))
            }else{
                edit_size['row-'+index] = item
                edit_size['row-'+index].produk_size_id = id_change;
                $("#"+parent+" .input_edit_size").val(JSON.stringify(edit_size))
            }
            console.log("#"+parent+" .input_edit_size")
            console.log(edit_size)
        }
        $(".modal").on("hide.bs.modal",function () {
            $(".size_table").html('');
             add_size = {};
             edit_size = {};
             delete_size = [];
        })
        $("#kt_modal_edit").on('shown.bs.modal',function () {
            var id = $("#kt_modal_edit [name=id]").val()

            $.ajax( {
                type:'get',
                url:base_url+'price-list/detail/'+id,
                cache:false,
                success:function (response) {
                    var data = JSON.parse(response)
                    $.each(data,function (i,val) {
                        var text = '<tr id="row-size-'+val.id+'">' +
                            '<td class="row-index">'+(i+1)+
                            '</td>'+
                            '<td>' +
                            '<input type="hidden" class="id_change_'+val.id+'" value="'+val.id+'">'+
                            '<select class="form-control size-id" id="input-size-'+val.id+'"  data-no="'+val.id+'">' +
                            $("#option_size").html()+
                            '</select>' +
                            '</td>'+
                            '<td>' +
                            '<input type="text" class="form-control input-numeral harga-pokok" data-no="'+val.id+'" id="input-pokok-'+val.id+'"  value="'+KTUtil.numberString(val.harga_pokok)+'">'+
                            '</td>'+
                            '<td>' +
                            '<input type="text" class="form-control input-numeral harga-jual" data-no="'+val.id+'" id="input-jual-'+val.id+'"  value="'+KTUtil.numberString(val.harga_jual)+'">'+
                            '</td>'+
                            '<td style="text-align: center">' +
                            '<button type="button" class="btn btn-danger delete-size" data-no="'+val.id+'" data-produk-size="'+val.id+'"><i class="fa fa-trash"></i></button>'+
                            '</td>'+
                            '</tr>';
                        $("#kt_modal_edit .size_table").append(text)
                        $("#input-size-"+val.id).val(val.id_size)
                        no = val.id
                        new Cleave($("#kt_modal_edit #input-pokok-"+val.id), {
                            numeral: true,
                            numeralThousandsGroupStyle: 'thousand'
                        })
                        new Cleave($("#kt_modal_edit #input-jual-"+val.id), {
                            numeral: true,
                            numeralThousandsGroupStyle: 'thousand'
                        })
                    })
                }
            })
        })
        $(".size_table").on('click','.delete-size',function () {
            var chose = $(this).data('no'),
                 parent = $(this).closest('.modal').attr('id'),
                produk_size_id = $(this).data('produk-size');
            $("#"+parent+" #row-size-"+chose).remove()
            if (produk_size_id != ''){
                delete_size.push(produk_size_id)
                $(".input_delete_size").val(JSON.stringify(delete_size))
            }
            re_arrange(parent)
        })
        function re_arrange(parent) {
            $.each($('#'+parent+' .size_table .row-index'),function (i,val) {
                $(val).html(i+1)
            })
        }
        $("#kt_modal_detail").on('shown.bs.modal',function () {
            var id = $("#kt_modal_detail [name=id]").val()
            $.ajax( {
                type:'get',
                url:base_url+'price-list/detail/'+id,
                cache:false,
                success:function (response) {
                    var data = JSON.parse(response)
                    $.each(data,function (i,val) {
                        console.log(val)
                        var text = '<tr id="row-size-'+val.id+'">' +
                            '<td class="row-index">'+(i+1)+
                            '</td>'+
                            '<td>' +
                                val.size+
                            '</td>'+
                            '<td>' +
                            KTUtil.numberString(val.harga_pokok)+
                            '</td>'+
                            '<td>' +
                            KTUtil.numberString(val.harga_jual)+
                            '</td>'+
                            '</tr>';
                        $("#kt_modal_detail .size_table").append(text)
                        $("#input-size-"+val.id).val(val.id_size)
                    })
                }
            })
        })
    }

    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPrice.init();
});