"use strict";

// Class Definition
var KTPiutang = function() {
    var general = function(){
        var base_url = $("#base_url").val();

        $("#child_data_ajax").on('click','.posting-btn',function(e){
            e.preventDefault()
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "get",
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function (response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    var data = jQuery.parseJSON(response);
                    if (data.success){
                        window.location.reload();
                    }else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function(request) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    swal.fire({
                        title: "Ada yang Salah",
                        html: request.responseJSON.message,
                        type: "warning"
                    });
                }
            });
        })
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPiutang.init();
});