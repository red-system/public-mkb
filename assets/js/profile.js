"use strict";

// Class Definition
var KTProfileGeneral = function() {

    var login = $('#kt_profile');
    var email = $("#personal_email").val();
    var user_name = $("#personal_name").val();
    var phone = $("#personal_phone").val();
    var tempat_tanggal_lahir = $("#tempat_tanggal_lahir").val();
    var alamat = $("#staff_alamat").val();
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var noImage = baseUrl+"/assets/media/no_image.png";
    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }
    var generalForm = function(){
        if ($.fn.datepicker) {
            $('.tanggal').datepicker({
                    rtl: KTUtil.isRTL(),
                    todayHighlight: true,
                    orientation: "bottom left",
                    autoclose: true,
                    format: 'yyyy-mm-dd',
           });           
        }
        if ($.fn.select2) {
            $('#kt_select2_1, #kt_select2_2, .kt_select_2').select2({
                width:'100%'
            });
        }
       
    }
    var editPersonal = function(){
        $("#kt_tabs_1_1 > input").prop("readonly",false);
        console.log(123);
    }
    var handleSwitchPersonalInfo = function(index) {
        index = index || 0
        if (index == 0) {
            $("form#edit_personal_form input").prop("readonly",false);
            $("form#edit_personal_form textarea").prop("readonly",false);
            $("#personal_cancel_group").slideUp();
            $("#personal_edit_group").slideDown();
        } else {
            $("form#edit_personal_form input").prop("readonly",true);
            $("form#edit_personal_form textarea").prop("readonly",true);
            $("#personal_edit_group").slideUp();
            $("#personal_cancel_group").slideDown();
            $("#personal_email").val(email);
            $("#personal_name").val(user_name);
            $("#personal_phone").val(phone);
            $("#tempat_tanggal_lahir").val(tempat_tanggal_lahir);
            $("#staff_alamat").val(alamat);
        }
    }
    var handleFormSwitch = function() {
        $("#edit_personal_btn").click(function(){
            handleSwitchPersonalInfo(0);
        });
        $("#cancel_personal_btn").click(function(){
           handleSwitchPersonalInfo(1); 
        });
    }
    var handlePersonalInfoSubmit = function() {
        $('#kt_personal_info_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    user_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        $(".account_name").html($("#personal_name").val());
                        user_name = $("#personal_name").val()
                        email = $("#personal_email").val()
                        phone = $("#personal_phone").val()
                        handleSwitchPersonalInfo(1);
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                }
            });
        });
    }
    var handleChangePasswordSubmit = function() {
        $('#kt_password_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    old_pass: {
                        required: true,
                        minlength:5
                    },
                    new_pass: {
                        required: true,
                        minlength: 5
                    },
                    re_pass: {
                        required: true,
                        minlength : 5,
                        equalTo : "#new_pass"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        form.clearForm();
                        form.validate().resetForm();
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                }
            });
        });
    }
    var handleAvatarSubmit = function() {
        $('#kt_avatar_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    avatar: {
                        required: true,
                        minlength:5
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        $("#customFile").val("");
                        $("#labelCustomFile").html("");
                        $('#displayImg').attr('src', noImage);
                        $(".account_avatar").attr('src', data.url);
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                }
            });
        });
    }
    var handleAvatarChange = function(){
        $("#customFile").change(function(){
            readURL(this);

        })
    }
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#displayImg').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    // Public Functions
    return {
        // public functions
        init: function() {
            handleFormSwitch();
            generalForm();
            handlePersonalInfoSubmit();
            handleChangePasswordSubmit();
            handleAvatarChange();
            handleAvatarSubmit();
        }
    };
    
}();


// Class Initialization
jQuery(document).ready(function() {
    KTProfileGeneral.init();
    
});