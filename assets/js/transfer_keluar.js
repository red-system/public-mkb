"use strict";

// Class Definition
var KTTransfer = function() {
    var type = $('#type-page').length>0?'list':'manipulate';
    var base_url = $('#base_url').val();
    var list = function(){

    }
    var manipulate = function(){
        var no_row = $('#no_row').length>0 ? $('#no_row').val() : 1;
        $('.tanggal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $('#add_item').click(function () {
            var row = '<tr id="row-'+no_row+'" class="cal-row" data-no="'+no_row+'">' +
                '<td class="no-row" data-no="'+no_row+'"></td>'+
                '<td><input type="text" data-no="'+no_row+'" class="form-control nama_item" id="nama_item_'+no_row+'" name="item[_'+no_row+'][nama_item]"></td>'+
                '<td><input type="text" data-no="'+no_row+'" class="form-control harga_item input-numeral" id="harga_item_'+no_row+'" name="item[_'+no_row+'][harga_item]" value="0"></td>'+
                '<td><input type="text" data-no="'+no_row+'" class="form-control qty_item input-numeral" id="qty_item_'+no_row+'" name="item[_'+no_row+'][qty_item]" value="0"></td>'+
                '<td>' +
                '<label id="subtotal_label_'+no_row+'" data-no="'+no_row+'">Rp. 0</label>' +
                '<input type="hidden" data-no="'+no_row+'" id="subtotal_item_'+no_row+'" name="item[_'+no_row+'][subtotal_item]">'+
                '</td>'+
                '<td>' +
                '<textarea class="form-control" id="keterangan_item_'+no_row+'" data-no="'+no_row+'" name="item[_'+no_row+'][keterangan_item]"></textarea>'+
                '</td>'+
                '<td><button type="button" class="btn btn-danger delete-item" data-no="'+no_row+'"><i class="fa fa-trash"></i></button></td>'+
                '</tr>';
            $('#item_row').append(row);

            new Cleave($('#harga_item_'+no_row), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
            })
            new Cleave($('#qty_item_'+no_row), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
            })
            no_row++;
            reArange();
        });
        $('#item_row').on('keyup','.harga_item,.qty_item',function () {
            subtotal()
        })
        $('#item_row').on('keyup','.input-numeral',function () {
            var value = $(this).val()
            if(value==''){
                value = 0;
                $(this).val(value)
            }
        })
        $('#item_row').on('click','.delete-item',function () {
            var index = $(this).data('no');
            $('#row-'+index).remove();
            reArange();
            subtotal();
        })

        var intVal = function(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        function subtotal() {
            var total = 0;
            $.each($('.cal-row'),function (i,val) {
                var index = $(val).data('no');
                var harga = intVal($('#harga_item_'+index).val());
                var qty = intVal($('#qty_item_'+index).val());
                var subtotal = harga * qty;
                total +=subtotal;
                $('#subtotal_label_'+index).html('Rp. '+KTUtil.numberString(subtotal));
                $('#subtotal_item_'+index).val(subtotal);
            });
            $('#grand_total_lbl').html('Rp.'+KTUtil.numberString(total));
            $('#input_grand_total').val(KTUtil.numberString(total));
        }
        function reArange() {
            $.each($('.no-row'),function (i,val) {
                $(val).html(i+1);
            });
        }
        $('#form-send').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            form_send(form);
        })
        function form_send(form) {
            var act = $(form).attr('action');
            var dataForm = form.serializeArray();
            var btn = $('#btn-save');
            if (!form.valid()) {
                return;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            $.ajax({
                url:act,
                data:dataForm,
                type:'post',
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    var responseData = JSON.parse(response);
                    if (responseData.success){
                        swal.fire({
                            type: 'success',
                            title: 'Pemeberitahuan',
                            text: responseData.message,
                        }).then(function() {
                            console.log(123);
                            window.location.href = base_url+'transfer-keluar';
                        });
                    }else{
                        swal.fire({
                            type: 'error',
                            text:responseData.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }

            });
        }
    }

    // Public Functions
    return {
        // public functions
        init: function() {
            if (type === 'list'){
                list();
            }else{
                manipulate();
            }
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTTransfer.init();
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalScale: 4
        })

    });
});